//
//  AppDelegate.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 10/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import XMPPFramework
import CoreData
import Crashlytics
import Fabric
import AWSS3
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
//import PushKit

let App = UIApplication.shared.delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate/*,UNUserNotificationCenterDelegate, MessagingDelegate*/{

    var window: UIWindow?
    var xmppController: XMPPController!
    var selctedImage = UIImage()
    var strCaptionText = String()
    var selectedVideoURL: URL?
    var captureAudioUrl: URL?
    var videoThumbNailImage = UIImage()
    var locationTitleAddress =  String()
    var locationDetailddress =  String()
    var locationManager:CLLocationManager!
    var latitude = String()
    var longitude = String()
    var latitudeOfSendLocation = String()
    var longitudeOfSendLocation = String()
    var isCallFirstTimeGetMychatList = Bool()
    var strCreatePost = String()
    var selectedImageOfPostCreation = UIImage()
    var selectedVideoOfPostCreation: URL?
    var isTapOnNotificationFromNotRunApp =  Bool()
    var mediaUrl: String?
    var mediThumbUrl =  String()
    var imgprofileLogo = String()
    var companyName = String()
    var channelMaxColor = String()
    var mediaType = String()
    var AuthToken = String()
    var isLaunchFirstTime = Bool()
    var isInviteViewed = Bool()
    var strSelectedLanguage = String()

    var dashStoryBoard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // For left Drawer
        self.createMenuView()
        // Google map integration
        GMSServices.provideAPIKey("AIzaSyDX6FLbeI7r5eTY4LHNTz_IWowhvcWgNd0")
        //File location of core data

        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print("core data file location",urls[urls.count-1] as URL)
        let noti = UserDefaults.standard.value(forKey: "Notification")
        print(noti as Any)
        
        //set flage for call get mychat list api at first time on dashboard screen
        isCallFirstTimeGetMychatList = true
        isLaunchFirstTime = true
        isInviteViewed = false
        isTapOnNotificationFromNotRunApp = false

        determineMyCurrentLocation()
        //Reconnect to ejabberd
        if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
            if retriveState == StringConstants.NSUserDefauluterValues.loginSuccessfully{
                let ejabberdUserId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                let middlewareToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)
                self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
                
                
                
                //for gettig invite summary Response
             let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
              if(sessionId != nil  && sessionId != StringConstants.EMPTY){
                getInviteSummaryAPI()
                getAppStarterDataAPI()
              }else{
                    self.sighIntoGreenCloud()
              }
            
            }
        }
        // Fabric crash anylitics
        Fabric.with([Crashlytics.self])
        
        // VOIP Push Kit initialise
        // self.voipRegistration()
        // create document directory
        let documentDirectoryHelper = DocumentDirectoryHelper()
        documentDirectoryHelper.createDirectory()

        FirebaseApp.configure()
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().isAutoInitEnabled = true
        
        //clear custom notification flags
        UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)

        //For localization
        // Override point for customization after application launch.
        L102Localizer.DoTheMagic()
        UIApplication.shared.applicationIconBadgeNumber = 0

//        let localNotif = launchOptions![UIApplication.LaunchOptionsKey.remoteNotification] as? UILocalNotification
//        if localNotif != nil {
//            
//            print("load notifiation *******")
//            // Perform your actions
//            let alert = UIAlertView.init(title: "Notification", message:"receive msssage in kill state", delegate:(self as! UIAlertViewDelegate) , cancelButtonTitle:"ok", otherButtonTitles: "", "")
//            alert.show()
//        }
        
        
        //UNUserNotificationCenter.removeAllPendingNotificationRequests(self)

        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: "id-Kisan Test Event iOS",
            AnalyticsParameterItemName: "Kisan Test Event iOS",
            AnalyticsParameterContentType: "cont"
            ])
        
        return true
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        /*Store the completion handler.*/
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
    
    //Reconnect to ejabberd
    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
            do {
                try xmppController = XMPPController(hostName: server,
                                                    userJIDString: userJID,
                                                    password: userPassword)
                xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
                xmppController.connect()
                
            } catch {
                print("Something went wrong")
            }
    }

    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        latitude = String(userLocation.coordinate.latitude)
        longitude = String(userLocation.coordinate.longitude)
         //manager.stopUpdatingLocation()
    }
    

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error \(error)")
    }
    
    fileprivate func createMenuView() {
        // create viewController code...
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController //ViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary) //barTintColor
        leftViewController.dashboardViewController = nvc
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController:rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    // Notification
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[StringConstants.pushNotificationConstants.gcm_message_id] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print("Notification",userInfo)
        self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : false)
    }
    
    func setPushNotificationDataInLocalDb(userInfo: [AnyHashable: Any] , isTapped: Bool){
        
        // Check this condition for notification of received chat messages
        print("userInfo",userInfo)
        var action = String()
        if let actionValue = userInfo[StringConstants.pushNotificationConstants.action]  {
            action = String(describing:actionValue)
        }else{
            action = StringConstants.EMPTY
        }

        if action == StringConstants.PushNotificationAction.custom{
            let pushNotificationaAtion = String(describing:userInfo[StringConstants.PushNotificationAction.pushNotificationaAtion]!)
            if pushNotificationaAtion == StringConstants.PushNotificationAction.Followchannel {
                if isTapped{
                    UserDefaults.standard.set(StringConstants.PushNotificationAction.Followchannel, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(userInfo[StringConstants.PushNotificationAction.CommynityKey], forKey: StringConstants.PushNotificationAction.CommynityKey)
                }else{
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.CommynityKey)
                }
            }else if pushNotificationaAtion == StringConstants.PushNotificationAction.link{
                if isTapped{
                    UserDefaults.standard.set(StringConstants.PushNotificationAction.link, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(userInfo[StringConstants.PushNotificationAction.contentField1], forKey: StringConstants.PushNotificationAction.contentField1)
                }else{
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.contentField1)
                }
            }else if pushNotificationaAtion == StringConstants.PushNotificationAction.upgrade {
                if isTapped{
                    UserDefaults.standard.set(StringConstants.PushNotificationAction.upgrade, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                }else{
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                }
            }else if pushNotificationaAtion == StringConstants.PushNotificationAction.sponsored{
                var myChatList = userInfo as! Dictionary<String, Any>
                let communityKey = myChatList["communityKey"] as? String
                let id = myChatList["id"] as? String
                let isSponsoredMessage = String(describing: myChatList["isSponsoredMessage"]!)
                //sponsored message
                let checkCommunityKeyWithisSponsoredFlag = MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdTrue(communityKey: communityKey!, isSponsoredMessage: isSponsoredMessage, id: id!)
                if(checkCommunityKeyWithisSponsoredFlag.count == 0){
                    MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: myChatList, isFirstTime: true,iSCommingFromOneTOneinitiate:false)
                }else{
                    MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: myChatList)
                }
                if isTapped{
                    UserDefaults.standard.set(StringConstants.PushNotificationAction.sponsored, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    print("StringConstants.PushNotificationAction.communityId",userInfo[StringConstants.PushNotificationAction.CommynityKey] as Any); UserDefaults.standard.set(userInfo[StringConstants.PushNotificationAction.CommynityKey], forKey: StringConstants.PushNotificationAction.CommynityKey)
                    UserDefaults.standard.set(id, forKey: StringConstants.PushNotificationAction.sponsoredMessageId)
                    UserDefaults.standard.set(isSponsoredMessage, forKey: StringConstants.PushNotificationAction.isSponsoredMessage)
                }else{
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.CommynityKey)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.sponsoredMessageId)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.isSponsoredMessage)
                }
            }else if pushNotificationaAtion == StringConstants.PushNotificationAction.message{
                var message = String()
                var aps : Dictionary<String, Any> = [:]
                var alert : Dictionary<String, Any> = [:]
                var title : String = StringConstants.EMPTY

                if let apsValue = userInfo["aps"]{
                    aps = apsValue as! Dictionary<String, Any>
                    if let alertValue = aps["alert"]{
                        alert = alertValue as! Dictionary<String, Any>
                        if let titleValue = alert["title"]{
                            title = titleValue as! String
                        }
                        if let message1 = alert["body"]{
                            message =  message1 as! String
                        }else{
                            message =  StringConstants.EMPTY
                        }
                    }
                }
                setOverlayCustomMessagesPushNotificationDataInLocalDb(userInfo: userInfo, message: message)
                if isTapped{
                    UserDefaults.standard.set(StringConstants.PushNotificationAction.message, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(userInfo[StringConstants.PushNotificationAction.communityId], forKey: StringConstants.PushNotificationAction.CommynityKey)
                    UserDefaults.standard.set(userInfo[StringConstants.PushNotificationAction.senderId], forKey: StringConstants.PushNotificationAction.senderId)

//                        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
//                        let apptVC = dashboardStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                        let navigationController = UINavigationController.init(rootViewController: apptVC)
//                        self.window?.rootViewController = navigationController
//                        self.window?.makeKeyAndVisible()
            
                    createMenuView()

                }else{
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.CommynityKey)
                }
            }
        }else{
            var aps : Dictionary<String, Any> = [:]
            var alert : Dictionary<String, Any> = [:]
            var title : String = StringConstants.EMPTY
            if let apsValue = userInfo["aps"]{
                aps = apsValue as! Dictionary<String, Any>
                if let alertValue = aps["alert"]{
                    alert = alertValue as! Dictionary<String, Any>
                    if let titleValue = alert["title"]{
                        title = titleValue as! String
                    }
                }
            }
            
            if let actionValue = userInfo[StringConstants.pushNotificationConstants.action] {
                let action = String(describing: actionValue)
                if action == StringConstants.pushNotificationConstants.joinCommunity{
                    if let jsonString = userInfo[StringConstants.pushNotificationConstants.myChat] as? String {
                        let objectData = jsonString.data(using: String.Encoding.utf8)
                        do {
                            let followerJoin = try JSONSerialization.jsonObject(with: objectData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            var followerJoinValue = followerJoin as! Dictionary<String, Any>
                            followerJoinValue.updateValue(StringConstants.CommunityType.CHANNEL, forKey: "type")
                            followerJoinValue.updateValue(URLConstant.c_OneToOne, forKey: "featured")
                            followerJoinValue.updateValue(StringConstants.ONE, forKey: "isMember")
                            followerJoinValue.updateValue(StringConstants.CommunityPrivacy.PUBLIC, forKey: "privacy")
                            //set message time messageAt
                            let currentTime = DateTimeHelper.getCurrentMillis()
                            followerJoinValue.updateValue(currentTime, forKey: "messageAt")
                            
                            print("followerJoinValue",followerJoinValue)
                            let communityJabberId = followerJoinValue["communityJabberId"] as? String
                            let communityDetailsCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityJabberId!)
                            if(communityDetailsCommunityKey.count == 0){
                                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: followerJoinValue, isFirstTime: false,iSCommingFromOneTOneinitiate:true)
                            }else if (communityDetailsCommunityKey.count == 1){
                                MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: followerJoinValue)
                            }else{
                                print("Not allowed")
                            }
                            //self.subscribeNode(communityJabberId: followerJoinValue["communityKey"] as! String)
                            let notificationName = Notification.Name(StringConstants.pushNotificationConstants.joinPushNotificationCenter)
                            // Post notification
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        } catch {
                            // Handle error
                            print(error)
                        }
                    }
                }else if action == StringConstants.pushNotificationConstants.appPushNotification{
                    
                    if let jsonString = userInfo[StringConstants.pushNotificationConstants.notification] as? String {
                        let objectData = jsonString.data(using: String.Encoding.utf8)
                        do {
                            let followerJoin = try JSONSerialization.jsonObject(with: objectData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            let followerJoinValue = followerJoin as! Dictionary<String, Any>
                            print("followerJoinValue",followerJoinValue)
                            //let sso_id = followerJoinValue["sso_id"] as? String
                            //userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                            let notificationKey = followerJoinValue["notificationKey"] as? String
                            let notificationDetailsNotificationKey = NotificationServices.sharedInstance.checkNotificationKey(notificationKey: notificationKey!)
                            if(notificationDetailsNotificationKey.count == 0){
                                NotificationServices.sharedInstance.sendNotiAtNotificationTable(notficationDetails: followerJoinValue)
                            }else if (notificationDetailsNotificationKey.count == 1){
                                NotificationServices.sharedInstance.updateNotiAtNotificationTable(notficationDetails: followerJoinValue)
                            }else{
                                print("Not allowed")
                            }
                            // Get notification data
                            let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.fromJoinNotification]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
                        } catch {
                            // Handle error
                            print(error)
                        }
                    }
                }else if action == StringConstants.pushNotificationConstants.block_member || action == StringConstants.pushNotificationConstants.unblock_member || action == StringConstants.pushNotificationConstants.remove_member {
                    // Block channel member and remove member from channel
                    if let jsonString = userInfo[StringConstants.pushNotificationConstants.myChat] as? String {
                        let objectData = jsonString.data(using: String.Encoding.utf8)
                        do {
                            let followerJoin = try JSONSerialization.jsonObject(with: objectData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            var followerJoinValue = followerJoin as! Dictionary<String, Any>
                            let communityKey = followerJoinValue["communityKey"] as? String
                            let communityDetailsOnCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey!)
                            if(communityDetailsOnCommunityKey.count == 0){
                                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: followerJoinValue, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
                            }else if (communityDetailsOnCommunityKey.count == 1){
                                let currentTime = DateTimeHelper.getCurrentMillis()
                                var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: communityDetailsOnCommunityKey)
                                dict.updateValue(String(currentTime), forKey: "messageAt")
                                
                                /* if action == StringConstants.pushNotificationConstants.block_member || action == StringConstants.pushNotificationConstants.unblock_member {
                                 dict.updateValue(followerJoinValue["messageText"] ?? String(), forKey: "messageText")
                                 }else{
                                 dict.updateValue(StringConstants.EMPTY, forKey: "messageText")
                                 }*/
                                
                                dict.updateValue(followerJoinValue["communityKey"] ?? String(), forKey: "communityKey")
                                if action == StringConstants.pushNotificationConstants.block_member || action == StringConstants.pushNotificationConstants.remove_member {
                                    dict.updateValue(StringConstants.ZERO, forKey: "isMember")
                                    CommunityDetailService.sharedInstance.updateisMemberFromComminityDetailsTbl(isMemberValue:StringConstants.ZERO,communityKey:followerJoinValue["communityKey"] as! String)
                                    
                                }else{
                                    dict.updateValue(StringConstants.ONE, forKey: "isMember")
                                    CommunityDetailService.sharedInstance.updateisMemberFromComminityDetailsTbl(isMemberValue:StringConstants.ONE,communityKey:followerJoinValue["communityKey"] as! String)
                                }
                                
                                MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                                
                                let userId = followerJoinValue["ownerId"] as? String
                                if action == StringConstants.pushNotificationConstants.remove_member{
                                    // Remove data from member table on userId/OwnerId
                                    MembersServices.sharedInstance.deleteRemoveMemberFromChannel(userId:userId!)
                                }else if action == StringConstants.pushNotificationConstants.block_member {
                                    MembersServices.sharedInstance.changeStatusOfBlockAndUnBlock(userId:userId!,isBlocked:"t")
                                }else if action == StringConstants.pushNotificationConstants.unblock_member {
                                    MembersServices.sharedInstance.changeStatusOfBlockAndUnBlock(userId:userId!,isBlocked:"f")
                                }
                                
                            }else{
                                print("Not allowed")
                            }
                            //Used this notification center on Dashboard Screen
                            let notificationName = Notification.Name(StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnDashboard)
                            // Post notification
                            NotificationCenter.default.post(name: notificationName, object: nil)
                            if action == StringConstants.pushNotificationConstants.block_member || action == StringConstants.pushNotificationConstants.unblock_member {
                                let operationDataDict = ["operation": StringConstants.pushNotificationConstants.block_member]
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnChannelFolloweList), object: nil, userInfo: operationDataDict)
                            }else if action == StringConstants.pushNotificationConstants.remove_member {
                                let operationDataDict = ["operation": StringConstants.pushNotificationConstants.remove_member]
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnChannelFolloweList), object: nil, userInfo: operationDataDict)
                            }
                        } catch {
                            // Handle error
                            print(error)
                        }
                    }
                    
                }else if action == StringConstants.pushNotificationConstants.messageDeleted{
                    
                    if let jsonString = userInfo[StringConstants.pushNotificationConstants.messages] as? String {
                        let objectData = jsonString.data(using: String.Encoding.utf8)
                        do {
                            let followerJoin = try JSONSerialization.jsonObject(with: objectData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            let followerJoinValue = followerJoin as! Dictionary<String, Any>
                            print("followerJoinValue",followerJoinValue)
                            //let sso_id = followerJoinValue["sso_id"] as? String
                            //userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                            let communityId = followerJoinValue["communityId"] as? String
                            let messageId = followerJoinValue["id"] as? String
                            
                            if messageId?.count != nil && communityId?.count != nil{
                                if UserDefaults.standard.bool(forKey: "MessageDeleteStatus") == true{
                                    let operationDataDict = ["communityId": communityId,
                                                             "id":messageId]
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.messageDeleted), object: operationDataDict, userInfo: nil )
                                }else{
                                    let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: Messages(), messageId: messageId!, communityKey: communityId!)
                                    MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey: communityId!, messageId: messageId!)
                                    print(status)
                                    
                                }
                            }
                        } catch {
                            // Handle error
                            print(error)
                        }
                    }
                }
            }else if title == "Kisan.Net" {
                var message = String()
                if let message1 = alert["body"]{
                    message =  message1 as! String
                }else{
                    message =  StringConstants.EMPTY
                }
                self.setOverlayCustomMessagesPushNotificationDataInLocalDb(userInfo: userInfo, message:message )
                
            }
        }
    }
    
    
    func setOverlayCustomMessagesPushNotificationDataInLocalDb(userInfo: [AnyHashable: Any],message: String){
        
        var dict: Dictionary<String, Any> = [:]
        
        if let hiddenDatetime = userInfo["hiddenDatetime"] {
            dict["hiddenDatetime"] = hiddenDatetime as? String
        }else{
            dict["hiddenDatetime"] = StringConstants.EMPTY
        }
        
        if let contentField2 = userInfo["contentField2"] {
            dict["contentField2"] = contentField2 as? String
        }else{
            dict["contentField2"] = StringConstants.EMPTY
        }
        
        if let contentField6 = userInfo["contentField6"] {
            dict["contentField6"] = contentField6 as? String
        }else{
            dict["contentField6"] = StringConstants.EMPTY
        }
        
        if let messageStatus = userInfo["messageStatus"] {
            dict["messageStatus"] = messageStatus as? String
        }else{
            dict["messageStatus"] = StringConstants.EMPTY
        }
        
        if let contentField10 = userInfo["contentField10"] {
            dict["contentField10"] = contentField10 as? String
        }else{
            dict["contentField10"] = StringConstants.EMPTY
        }
        
        if let hiddenbyUserId = userInfo["hiddenbyUserId"] {
            dict["hiddenbyUserId"] = hiddenbyUserId as? String
        }else{
            dict["hiddenbyUserId"] = StringConstants.EMPTY
        }
        
        if let contentField8 = userInfo["contentField8"] {
            dict["contentField8"] = contentField8 as? String
        }else{
            dict["contentField8"] = StringConstants.EMPTY
        }
        
        if let deletedbyUserId = userInfo["deletedbyUserId"] {
            dict["deletedbyUserId"] = deletedbyUserId as? String
        }else{
            dict["deletedbyUserId"] = StringConstants.EMPTY
        }
        
        if let contentField7 = userInfo["contentField7"] {
            dict["contentField7"] = contentField7 as? String
        }else{
            dict["contentField7"] = StringConstants.EMPTY
        }
        
        if let contentField3 = userInfo["contentField3"] {
            dict["contentField3"] = contentField3 as? String
        }else{
            dict["contentField3"] = StringConstants.EMPTY
        }
        
        if let deletedDatetime = userInfo["deletedDatetime"] {
            dict["deletedDatetime"] = deletedDatetime as? String
        }else{
            dict["deletedDatetime"] = StringConstants.EMPTY
        }
        
        if let contentField9 = userInfo["contentField9"] {
            dict["contentField9"] = contentField9 as? String
        }else{
            dict["contentField9"] = StringConstants.EMPTY
        }
        
        if let jabberId = userInfo["jabberId"] {
            dict["jabberId"] = jabberId as? String
        }else{
            dict["jabberId"] = StringConstants.EMPTY
        }
        
        if let createdDatetime = userInfo["createdDatetime"] {
            dict["createdDatetime"] = createdDatetime as? String
        }else{
            dict["createdDatetime"] = StringConstants.EMPTY
        }
        
        if let contentField5 = userInfo["contentField5"] {
            dict["contentField5"] = contentField5 as? String
        }else{
            dict["contentField5"] = StringConstants.EMPTY
        }
        
        if let mediaType = userInfo["mediaType"] {
            dict["mediaType"] = mediaType as? String
        }else{
            dict["mediaType"] = StringConstants.EMPTY
        }
        
        if let messageType = userInfo["messageType"] {
            dict["messageType"] = messageType as? String
        }else{
            dict["messageType"] = StringConstants.EMPTY
        }
        
        if let id = userInfo["id"] {
            dict["id"] = id as? String
        }else{
            dict["id"] = StringConstants.EMPTY
        }
        
        if let communityId = userInfo["communityId"] {
            dict["communityId"] = communityId as? String
        }else{
            dict["communityId"] = StringConstants.EMPTY
        }
        
        if let contentField1 = userInfo["contentField1"] {
            dict["contentField1"] = contentField1 as? String
        }else{
            dict["contentField1"] = StringConstants.EMPTY
        }
        
        if let senderId = userInfo["senderId"] {
            dict["senderId"] = senderId as? String
        }else{
            dict["senderId"] = StringConstants.EMPTY
        }
        
        if let contentField4 = userInfo["contentField4"] {
            dict["contentField4"] = contentField4 as? String
        }else{
            dict["contentField4"] = StringConstants.EMPTY
        }
        
        let id = dict["id"]
        let messageKeyAvaiblity = MessagesServices.sharedInstance.checkMessagePresentInDB(messageKey: id as! String )
//        if let message1 = alert["body"]{
//            message =  message1 as! String
//        }else{
//            message =  StringConstants.EMPTY
//        }
        if(messageKeyAvaiblity.count == 0){
            let mediaType = dict["mediaType"] as! String
            let messageType = dict["messageType"] as! String
            let  messages = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: dict, saveDataAtDB: true)
            print("messages",messages)
            
            var strLatestMessage : String = StringConstants.EMPTY
            if mediaType == StringConstants.MediaType.image{
                strLatestMessage = StringConstants.MediaType.image
            }else if mediaType == StringConstants.MediaType.video{
                strLatestMessage = StringConstants.MediaType.video
            }else if mediaType == StringConstants.MediaType.audio{
                strLatestMessage = StringConstants.MediaType.audio
            }else if messageType == StringConstants.MessageType.mapLocation{
                strLatestMessage = StringConstants.MessageType.mapLocation
            }else if messageType == StringConstants.MessageType.feed{
                strLatestMessage = message
            }else if mediaType == StringConstants.MediaType.document{
                strLatestMessage = message
            }else if (messageType == StringConstants.MessageType.chatMessage || messageType == StringConstants.MessageType.banner) && (mediaType  == StringConstants.EMPTY || mediaType  == StringConstants.MessageType.text) {
                strLatestMessage = message
            }else{
                strLatestMessage = message
            }
            
            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: strLatestMessage, latestMessageTime: String(describing: dict["createdDatetime"]!), communityKey: String(describing: dict["communityId"]!),mediaType:dict["mediaType"] as! String,messageType:String(describing: dict["messageType"]!), lastMessageId: dict["id"] as? String ?? "")
            
            let messagesValue = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: dict)
            
            let communityId  = dict["communityId"] as! String
            if (communityId.contains("_")){
                let nitificationDataDict:[String: Messages] = [StringConstants.NSUserDefauluterKeys.receivedMessages: messagesValue]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receivedMessagesForChannelOneToOne"), object: nil, userInfo: nitificationDataDict)
            }else{
                let nitificationDataDict:[String: Messages] = [StringConstants.NSUserDefauluterKeys.receivedMessages: messagesValue]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receivedMessagesForChannelBroadCast"), object: nil, userInfo: nitificationDataDict)
            }
            
        }else{
            MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: dict)
        }
    }

    
    
    func subscribeNode(communityJabberId:String) {
        let host = URLConstant.hostName
        let serviceJID = XMPPJID(string: "pubsub.\(host)")
        print("serviceJID",serviceJID ?? String())
        let xmppPubSub = XMPPPubSub(serviceJID: serviceJID, dispatchQueue: DispatchQueue.main)
        xmppPubSub.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppPubSub.activate(xmppController.xmppStream)
        xmppPubSub.subscribe(toNode: communityJabberId)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[StringConstants.pushNotificationConstants.gcm_message_id] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("Notification",userInfo)
        self.setPushNotificationDataInLocalDb(userInfo: userInfo , isTapped : false)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs token: \(deviceTokenString)")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
        if Messaging.messaging().fcmToken != nil {
            Messaging.messaging().subscribe(toTopic: StringConstants.global) { error in
                print("Subscribed to global topic")
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        //clear notification from notification tray
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        application.cancelAllLocalNotifications()
        center.removeAllPendingNotificationRequests()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let saveStatus =   self.saveContext()
        print("saveStatus",saveStatus)
    }
    
     // MARK: - Core Data stack
   lazy var persistentContainerTemp: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail. KisanChat
         */
        let container = NSPersistentContainer(name: "KISANNetTemp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    
   /* lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail. KisanChat
         */
        let container = NSPersistentContainer(name: "KISANNet")
        //New code of data migration ------------------------------//
        let description = NSPersistentStoreDescription()
        description.shouldMigrateStoreAutomatically = true
        description.shouldInferMappingModelAutomatically = true
        container.persistentStoreDescriptions = [description]
        //--------------------------------------------------------//
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        var saveFlag = false
        if context.hasChanges {
            do {
                try context.save()
                saveFlag = true
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                saveFlag = false
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                
            }
        }
        
        return saveFlag
    }*/
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "KISANNet")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        var saveFlag = false
        if context.hasChanges {
            do {
                try context.save()
                saveFlag = true
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                saveFlag = false
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return saveFlag
    }
    
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.pendingInvitationCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            self.getInviteSummaryAPI()
                                                            self.getAppStarterDataAPI()
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }

//API call to get Invite Summary
func getInviteSummaryAPI(){
    
    AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
    let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
    let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
    
    //set Source and filter for both exhibitor and Kisan Mitra
    let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
    var resource = String()
    var  source = String()
    if accountType == StringConstants.ONE {
        resource = RequestName.kisanMita
        source = RequestName.loginSourceName
    }else{
        resource = RequestName.ExhibitorInvite
        source = RequestName.loginSourceExhibitorName
    }
    
    let filter = [URLConstant.filter1: resource, URLConstant.filter2: userName]
    let params = InviteSummuryRequest.convertToDictionary(inviteSummuryRequest:InviteSummuryRequest.init(eventCode:URLConstant.eventCode , source: source, pagesize: 3, currentpage: 0, sort_col: "", app_key: URLConstant.app_key, sessionId: sessionId, filter: filter as [String : AnyObject]))
    print("params",params)
    let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.guestList
    _ = InviteSummaryServices().getInviteSummary(apiURL,postData: params as [String : AnyObject],
                                                 withSuccessHandler: { (userModel) in
                                                    let model = userModel as! InviteSummaryResponse
                                                    print("model",model)
                                                    
                                                    let records = model.records
                                                    let inviteSummary = records[0]
                                                    
                                                    let settings = model.settings
                                                    let inviteSetting = settings[0]

                                                    var standard =  Int()
                                                    var extra = Int()
                                                    var sent = Int()
                                                    
                                                    let medialink =  inviteSummary.media_link
                                                    let mediatype = inviteSummary.media_type
                                                    let skip_media = inviteSummary.skip_media
                                                    let channelName = inviteSummary.channel_name
                                                    let channelColour = inviteSummary.channel_max_color
                                                    let mediathumb = inviteSummary.media_thumbnail
                                                    let imagechannelProfile = inviteSummary.channel_big_thumb
                                                    let issueGreenpasslastDate = inviteSetting.issue_greenpass_last_date
                                                    let greenpassAcceptLast_date = inviteSetting.greenpass_accept_last_date
                                                    
                                                    if(accountType == StringConstants.TWO){
                                                        if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 1) {
                                                            standard =  inviteSummary.unlimited_standard!
                                                            extra = inviteSummary.unlimited_extra!
                                                            sent = inviteSummary.unlimited_sent!
                                                        }else if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 0){
                                                            standard =  inviteSummary.quota_standard!
                                                            extra = inviteSummary.quota_extra!
                                                            sent = inviteSummary.quota_sent!
                                                        }
                                                    }else{
                                                        standard =  inviteSummary.unlimited_standard!
                                                        extra = inviteSummary.unlimited_extra!
                                                        sent = inviteSummary.unlimited_sent!
                                                    }
                                                    
                                                    
                                                    let show_issue_unlimited_greenpass = inviteSetting.show_issue_unlimited_greenpass

                                                    let show_issue_greenpass = inviteSetting.show_issue_greenpass
                                                    
                                                    UserDefaults.standard.set(show_issue_greenpass, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                                                    
                                                    if inviteSetting.show_issue_unlimited_greenpass != nil {
                                                        UserDefaults.standard.set(inviteSetting.show_issue_unlimited_greenpass!, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                    }else{
                                                        UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                    }

                                                    
                                                    UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                    
                                                    UserDefaults.standard.set(extra, forKey:
                                                        StringConstants.NSUserDefauluterKeys.extra)
                                                    
                                                    UserDefaults.standard.set(sent, forKey:
                                                        StringConstants.NSUserDefauluterKeys.sent)
                                                    
                                                    UserDefaults.standard.set(issueGreenpasslastDate, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                    
                                                    UserDefaults.standard.set(greenpassAcceptLast_date, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)


                                                    UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                    
                                                    UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                    
                                                    UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                    
                                                    
                                                    UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                    
                                                    UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                    
                                                    UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                    
                                                    UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                    
                                                    
                                                    
                                                    
    }, withFailureHandlere: { (error: String) in
        print("error",error)
        //            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
        //                },{action2 in
        //                }, nil])
    })
}
    
    
    //API call to get Invite Summary
    func getAppStarterDataAPI(){
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginSourceName
        let params = AppstarterRequest.convertToDictionary(appstarterRequest:AppstarterRequest.init(app_key: URLConstant.app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.appStarterdata
        _ = AppStarterServices().getAppStarterData(apiURL,postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                    let model = userModel as! AppStarterResponse
                                                    print("model",model)
                                                    
                                                    let data = model.data[0]
                                                    let pendingInvite = data.pending_invitation_count
                                                    let myGreenPassCount = data.greenpass_count
                                                    
                                                    UserDefaults.standard.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
                                                    
                                                    UserDefaults.standard.set(myGreenPassCount, forKey:
                                                        StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                    
                                                    let isMitra = data.isMitra
                                                    let defaults=UserDefaults.standard
                                                    defaults.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                    defaults.set(myGreenPassCount, forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount)
                                                    defaults.synchronize()
                                                    /*if pendingInvite! > 0{
                                                     if ((UserDefaults.standard.value(forKey: "isNotFirstTime")) != nil) && UserDefaults.standard.value(forKey: "isNotFirstTime") as! Bool == true{
                                                     print("not First time show congratulation")
                                                     UserDefaults.standard.set(false, forKey: "isNotFirstTime")
                                                     }else{
                                                     print("First time show congratulation")
                                                     UserDefaults.standard.set(true, forKey: "isNotFirstTime")
                                                     let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                                                     self.navigationController?.pushViewController(vc, animated: false)
                                                     }
                                                     }*/
                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }
    

    
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
      //  completionHandler([.alert, .sound])
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[StringConstants.pushNotificationConstants.gcm_message_id] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print("Notification",userInfo)
        // Change this to your preferred presentation option
        
        var action = String()
        if let actionValue = userInfo[StringConstants.pushNotificationConstants.action]  {
            action = String(describing:actionValue)
        }else{
            action = StringConstants.EMPTY
        }
        //for overlay messages
        var pushnotificationAction = String()
        if let actionValue = userInfo[StringConstants.PushNotificationAction.pushNotificationaAtion]  {
            pushnotificationAction = String(describing:actionValue)
        }else{
            pushnotificationAction = StringConstants.EMPTY
        }

        print("action",action)
        if action == StringConstants.PushNotificationAction.custom{
            if pushnotificationAction == StringConstants.PushNotificationAction.message{
                self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : false)
                let senderId = userInfo["senderId"] as? String
                let communityId = userInfo["communityId"] as? String
                if (communityId?.contains("_"))!{
                    if let checkAvaiblity = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne) {
                        print("checkAvaiblity",checkAvaiblity)
                        if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                            if senderId == userId {
                                completionHandler([])
                            }else if checkAvaiblity == communityId {
                                completionHandler([])
                            }else{
                                completionHandler(.alert)
                            }
                        }
                    }else{
                        if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                            if senderId == userId {
                                completionHandler([])
                            }else{
                                completionHandler(.alert)
                            }
                        }
                    }
                    
                }else{
                    if let checkAvaiblity = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad) {
                        print("checkAvaiblity",checkAvaiblity)
                        if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                            if senderId == userId {
                                completionHandler([])
                            }else if checkAvaiblity == communityId {
                                completionHandler([])
                            }else{
                                completionHandler(.alert)
                            }
                        }
                    }else{
                        if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                            if senderId == userId {
                                completionHandler([])
                            }else{
                                completionHandler(.alert)
                            }
                        }
                    }
                }            }else{
                self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : false)
            }
        }else if action == StringConstants.PushNotificationAction.sponsored{
            self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : false)
        }else{
            self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : false)
            let senderId = userInfo["senderId"] as? String
            let communityId = userInfo["communityId"] as? String
            if (communityId?.contains("_"))!{
                if let checkAvaiblity = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne) {
                    print("checkAvaiblity",checkAvaiblity)
                    if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                        if senderId == userId {
                            completionHandler([])
                        }else if checkAvaiblity == communityId {
                            completionHandler([])
                        }else{
                            completionHandler(.alert)
                        }
                    }
                }else{
                    if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                        if senderId == userId {
                            completionHandler([])
                        }else{
                            completionHandler(.alert)
                        }
                    }
                }
                
            }else{
                if let checkAvaiblity = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad) {
                    print("checkAvaiblity",checkAvaiblity)
                    if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                        if senderId == userId {
                            completionHandler([])
                        }else if checkAvaiblity == communityId {
                            completionHandler([])
                        }else{
                            completionHandler(.alert)
                        }
                    }
                }else{
                    if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId){
                        if senderId == userId {
                            completionHandler([])
                        }else{
                            completionHandler(.alert)
                        }
                    }
                }
            }
        }
    }
    
    
    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0

    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[StringConstants.pushNotificationConstants.gcm_message_id] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print("Notification",userInfo)
        isTapOnNotificationFromNotRunApp = true
        self.setPushNotificationDataInLocalDb(userInfo: userInfo ,isTapped : true)
        completionHandler()
    }
   
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        Messaging.messaging().subscribe(toTopic: StringConstants.global) { error in
            print("Subscribed to global topic")
        }

    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        Messaging.messaging().subscribe(toTopic: StringConstants.global) { error in
            print("Subscribed to global topic")
        }
        
    }
    // [END ios_10_data_message]
}

extension AppDelegate : XMPPPubSubDelegate{
    func xmppPubSub(_ sender: XMPPPubSub, didSubscribeToNode node: String, withResult iq: XMPPIQ) {
        print("node",node)
    }
    
    func xmppPubSub(_ sender: XMPPPubSub, didNotSubscribeToNode node: String, withError iq: XMPPIQ) {
        print("node",node)
    }
}


//VOIP for receive notification in close app state
/*
 extension AppDelegate:PKPushRegistryDelegate{
    // Register for VoIP notifications
    func voipRegistration() {
        DispatchQueue.main.async {
            let voipRegistry: PKPushRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
            // Set the registry's delegate to self
            voipRegistry.delegate = self
            // Set the push type to VoIP
            voipRegistry.desiredPushTypes = [PKPushType.voIP]
        }
        // Create a push registry object
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        // Register VoIP push token (a property of PKPushCredentials) with server
        print("PUSH Token: \(pushCredentials.token)")
        let deviceTokenString = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs token retrieved: \(deviceTokenString)")

    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("Invalid Push notification")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        let payloadDict = payload.dictionaryPayload["aps"] as? Dictionary<String, AnyObject>
        let message = payloadDict?["alert"]
        UserDefaults.standard.set(message, forKey: "Notification")
        NSLog("incoming voip notfication: \(payload.dictionaryPayload)")
        
        let noti = UserDefaults.standard.value(forKey: "Notification")
        print(noti)

    }
}*/


extension String{
    
    mutating func getJSONFormatedString(){
        
        self = self.replacingOccurrences(of: "(", with: "[")
        self = self.replacingOccurrences(of: ")", with: "]")
        self = self.replacingOccurrences(of: "\n ", with: "\n \"")
        self = self.replacingOccurrences(of: ",", with: "\",")
        self = self.replacingOccurrences(of: "\"\"", with: "\"")
        self = self.replacingOccurrences(of: "\",\"", with: "\",\n \"")
        self = self.replacingOccurrences(of: "[\"", with: "[\n \"")
        self = self.replacingOccurrences(of: "\"]", with: "\"\"\n]")
        self = self.replacingOccurrences(of: "\"\"", with: "\"")
    }
}
