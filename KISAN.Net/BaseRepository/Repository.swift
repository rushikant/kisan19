
//
//  Repository.swift22
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

typealias  CompleteionHandler   = ((_ responseObject:Any?) -> Void)
typealias  FailureHandler       = ((_ errorMessage: String) -> Void)

class Repository: NSObject {
    
    var completeionBlock: CompleteionHandler?
    var failureBlock: FailureHandler?
    
    // Declare Headers for visitor login and
    func setHeadersWithoutXAPIKey(_ mutableURLRequest: NSMutableURLRequest,isExhibitor:Bool,isExhibitorAdvertise:Bool,isCommingFromIdKisanLab:Bool = false) {
        if isExhibitor == true {
            mutableURLRequest.addValue(URLConstant.EXHIBITOR_X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            mutableURLRequest.addValue(URLConstant.EXHIBITOR_X_API_SECRET, forHTTPHeaderField: "X-API-SECRET")
            mutableURLRequest.addValue(URLConstant.EXHIBITOR_X_REQUEST_ID, forHTTPHeaderField: "X-REQUEST-ID")
            mutableURLRequest.addValue(URLConstant.EXHIBITOR_APPLICATION_JSON, forHTTPHeaderField: "Content-Type")
        }else if isExhibitorAdvertise == true{
            mutableURLRequest.addValue(URLConstant.EXHIBITOR_APPLICATION_JSON, forHTTPHeaderField: "Content-Type")
        }else if isCommingFromIdKisanLab == true{
            mutableURLRequest.addValue(URLConstant.APPLICATION_JSON, forHTTPHeaderField: "Content-Type")
        }else{
            let strUUID = UUIDHelper.getUUID()
            let requestid = URLConstant.ios + strUUID
            mutableURLRequest.addValue(URLConstant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            mutableURLRequest.addValue(URLConstant.X_API_SECRET, forHTTPHeaderField: "X-API-SECRET")
            if let middlewareToken = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.middlewareToken) {
                mutableURLRequest.addValue(URLConstant.BEARER + " " + middlewareToken, forHTTPHeaderField: "Authorization")
            }else{
                mutableURLRequest.addValue(URLConstant.BEARER, forHTTPHeaderField: "Authorization")
            }
            mutableURLRequest.addValue(requestid, forHTTPHeaderField: "X-REQUEST-ID")
            mutableURLRequest.addValue(URLConstant.APPLICATION_JSON, forHTTPHeaderField: "Content-Type")
        }
    }
    
    // Set  Authorization with Bearer token for edit profile
    
    func setAuthorizationWithBearerToken(_ mutableURLRequest: NSMutableURLRequest){
        let token = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.token)
         mutableURLRequest.addValue("Bearer " + token, forHTTPHeaderField: "Authorization")
    }
    
    // Create Complete URL
    func getCompleteUrl(_ url: String) -> String {
        
        let baseUrl: String = "" //URLConstant.BaseUrl
        
        var completedURL: String = ""
        let url1 = url
        
        if !url.isEmpty && !baseUrl.isEmpty {
            
            let imageURL = url[url.startIndex] == "/" ? String(url1.substring(from: url1.characters.index(after: url1.startIndex))) : url1
            completedURL = baseUrl[baseUrl.characters.index(before: baseUrl.endIndex)] != "/" ? String(baseUrl + "/") : baseUrl
            completedURL.append(imageURL)
            
        }
        return url1
    }
    
    // GET
    func GET(_ url: String,
             postData: [String: AnyObject],
             requestName: String) -> DataRequest? {
        print("postData",postData.count)
        let modifiedUrlString = getUrlFromDictionary(postData)
        let finalUrl = url + modifiedUrlString
        print(finalUrl)
        let requestUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let mutableURLRequest: NSMutableURLRequest
        mutableURLRequest = NSMutableURLRequest(url: URL(string: requestUrl!)!)
        mutableURLRequest.httpMethod = "GET"
        self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        return self.callAPI(mutableURLRequest, requestName: requestName,postData:postData)
    }
    
    func GET_FROM_WIRHOUT_PARAMETER_API(_ requestUrl: String,postData: [String: AnyObject], requestName: String) -> DataRequest? {
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: requestUrl)!)
        mutableURLRequest.httpMethod = "GET"
        self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        return self.callAPIWithoutHeaderInSuccessCallBack(mutableURLRequest, requestName: requestName, postData: postData)
    }
    
    // get method to call api with no heades for getting yioutube data
    
    func GET_FROM_WIRHOUT_PARAMETER_WITHOUT_HEADER_API(_ requestUrl: String,postData: [String: AnyObject], requestName: String) -> DataRequest? {
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: requestUrl)!)
        mutableURLRequest.httpMethod = "GET"
        return self.callAPIWithoutHeaderInSuccessCallBack(mutableURLRequest, requestName: requestName, postData: postData)
    }
    
    func GET_FROM_PARAMETERWITHURL_API(_ url: String,
             postData: [String: AnyObject],
             requestName: String) -> DataRequest? {
        print("postData",postData.count)
        let params = ["":""] as [String : AnyObject]
        let modifiedUrlString = getUrlFromDictionary(params)
        let finalUrl = url + modifiedUrlString
        print(finalUrl)
        let requestUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let mutableURLRequest: NSMutableURLRequest
        mutableURLRequest = NSMutableURLRequest(url: URL(string: requestUrl!)!)
        mutableURLRequest.httpMethod = "GET"
        self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        return self.callAPIWithoutHeaderInSuccessCallBack(mutableURLRequest, requestName: requestName, postData: postData)
    }
    
    // POST
    func POST(_ requestUrl: String, postData: [String: AnyObject], requestName: String) -> DataRequest? {
        
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: requestUrl)!)
        mutableURLRequest.httpMethod = "POST"
        if requestUrl.range(of:"/exhibitor/") != nil{
            // Check of exhibitor word for exhibitor login
            self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:true,isExhibitorAdvertise:false)
        }else if requestUrl.range(of:"greencloud.") != nil{
            self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:true)
        }else if requestUrl.range(of:URLConstant.checkRequestFrom) != nil{
          // id.kisan.in/   id.kisanlab.com
            self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:true,isCommingFromIdKisanLab: true)
        }else{
            self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        }
        do {
            mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: postData, options: .prettyPrinted)

        } catch {
            print(error)
            return nil
        }
        return self.callAPI(mutableURLRequest, requestName: requestName,postData:postData)
    }
    
    // PUT
    func PUT(_ requestUrl: String, postData: [String: AnyObject], requestName: String) -> DataRequest? {
        
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: self.getCompleteUrl(requestUrl))!)
        mutableURLRequest.httpMethod = "PUT"
        self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        //self.setAuthorizationWithBearerToken(mutableURLRequest)
        print("mutableURLRequest",mutableURLRequest)
        do {
            mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: postData, options: .prettyPrinted)
        } catch {
            print(error)
            return nil
        }
        return self.callAPI(mutableURLRequest, requestName: requestName,postData:postData)
    }
    
    
    
    // DELETE
    func DELETE(_ requestUrl: String, deleteData: NSDictionary, requestName: String) -> DataRequest? {
        
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: self.getCompleteUrl(requestUrl))!)
        mutableURLRequest.httpMethod = "DELETE"
        self.setHeadersWithoutXAPIKey(mutableURLRequest,isExhibitor:false,isExhibitorAdvertise:false)
        do {
            mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: deleteData, options: .prettyPrinted)
        } catch {
            print(error)
            return nil
        }
        return self.callAPI(mutableURLRequest, requestName: requestName,postData:["":"" as AnyObject])
    }
    
    func callAPI(_ mutableURLRequest: NSMutableURLRequest, requestName: String,postData: [String: AnyObject]) -> DataRequest {
        
        print("******** Headers: \(String(describing: mutableURLRequest.allHTTPHeaderFields))")
        return  Alamofire.request((mutableURLRequest as URLRequest) as URLRequestConvertible)
            
            .responseJSON { (response) -> Void in
                
                guard response.result.isSuccess else {
                    print("Error: NOT SUCCESS \(String(describing: response.result.error))")
                    self.failureCallback(response.result.error! as NSError, requestName: requestName)
                    return
                }
                
                guard let value = response.result.value else {
                    print("Error: \(String(describing: response.result.error))")
                    self.failureCallback(response.result.error! as NSError, requestName: requestName)
                    
                    return
                }
                print("Response: \(value)")
                self.successCallback(value as AnyObject, headers: (response.response?.allHeaderFields)!, requestName: requestName,postData:postData)
        }
    }
    
    
    // call API WithoutHeader In Success CallBack
    func callAPIWithoutHeaderInSuccessCallBack(_ mutableURLRequest: NSMutableURLRequest, requestName: String,postData: [String: AnyObject]) -> DataRequest {
        print("******** Headers: \(String(describing: mutableURLRequest.allHTTPHeaderFields))")
        return  Alamofire.request((mutableURLRequest as URLRequest) as URLRequestConvertible)
            
            .responseJSON { (response) -> Void in
                
                guard response.result.isSuccess else {
                    print("Error: NOT SUCCESS \(String(describing: response.result.error))")
                    self.failureCallback(response.result.error! as NSError, requestName: requestName)
                    return
                }
                
                guard let value = response.result.value else {
                    print("Error: \(String(describing: response.result.error))")
                    self.failureCallback(response.result.error! as NSError, requestName: requestName)
                    
                    return
                }
                print("Response: \(value)")
                self.successCallback(value as AnyObject, requestName: requestName,postData:postData)
        }
    }
    
    
    func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
    }
    
    func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
    }
    
    func failureCallback(_ error: NSError, requestName: String) {
    }
    
    func failureCallbackWithSatusMessage(_ error: NSError, requestName: String, _ response: AnyObject) {
    }
    
    func callPostAPI(_ mutableURLRequest: NSMutableURLRequest, requestName: String) {
        
        //Print.Log.info("******** Headers: \(String(describing: mutableURLRequest.allHTTPHeaderFields))")
        print("******** Headers: \(String(describing: mutableURLRequest.allHTTPHeaderFields))")
        Alamofire.request((mutableURLRequest as URLRequest) as URLRequestConvertible)
            .responseString { response in
               // Print.Log.info("Success: \(response.result.isSuccess)")
                print("Success: \(response.result.isSuccess)")
              //  Print.Log.info("Response String: \(String(describing: response.result.value))")
                print("Response String: \(String(describing: response.result.value))")

        }
        
    }
    
    func callResponseStringAPI(_ mutableURLRequest: NSMutableURLRequest, requestName: String) {
        
        guard let url = mutableURLRequest as? URLRequestConvertible else {fatalError("Cannot to convert to URLRequestConvertible")}
        
        Alamofire.request(url)
            
            .responseString { (response) -> Void in
                //Print.Log.info("Response: \(response)")
                print("Response: \(response)")
        }
    }
    
    func requestPostURLForUploadImage(_ strURL : String,
                                      isImageSelect : Bool,
                                      fileName : String,
                                      params : [String : AnyObject]?,
                                      image : UIImage,
                                      successHandler: @escaping (UploadLogotoGreenCloudResponse) -> Void,
                                      failureHandler: @escaping (Bool?, String) -> Void){
        var imgName = ""
        let timeStamp = NSNumber(value: Date().timeIntervalSince1970)
        imgName = fileName + "." +  String(describing: timeStamp) + ".png"
        Alamofire.upload(multipartFormData: { multipartFormData in
            if isImageSelect == true{
                let imageHelper = ImageHelper()
                let imageData = imageHelper.compressImage(image: image, compressQuality: 0.4)!
               // let imgData = UIImageJPEGRepresentation(image, 0.2)!
                multipartFormData.append(imageData, withName: fileName,fileName:imgName, mimeType: "image/jpg")
            }
            for (key, value) in params! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:strURL)     { (result) in
                            switch result {
                            case .success(let upload, _ ,_ ):
                                upload.uploadProgress(closure: { (progress) in
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                })
                                
                                upload.responseJSON { response in
                                    let resJson = response.result.value
                                    if resJson != nil{
                                        
                                        let res = resJson as! Dictionary<String, Any>
                                        var loginRespo: UploadLogotoGreenCloudResponse = UploadLogotoGreenCloudResponse.initializeUploadLogotoGreenCloudResponse()
                                        loginRespo = UploadLogotoGreenCloudResponse.uploadLogotoGreenCloudResponseFromDictionary(res as [String : AnyObject?])
                                        
                                        
                                        if res.count > 0 {
                                            if res["success"] as? Int == 1 {
                                                successHandler(loginRespo)
                                            }
                                        }
                                    }
                                }
                            case .failure(let error):
                                failureHandler(false, error as! String)
                            }
        }
    }
    
    
    
    func requestPostURLForUploadMediaImageToGreenCloud(_ strURL : String,
                                      isImageSelect : Bool,
                                      fileName : String,
                                      params : [String : AnyObject]?,
                                      image : UIImage,imageData : NSData,
                                      successHandler: @escaping (UploadSetInvitationMediaImageToGreenCloud) -> Void,
                                      failureHandler: @escaping (Bool?, String) -> Void){
      
        var imgName = ""
        let timeStamp = NSNumber(value: Date().timeIntervalSince1970)
        Alamofire.upload(multipartFormData: { multipartFormData in
            if isImageSelect{
                imgName =  String(describing: timeStamp) + ".png"
                multipartFormData.append(imageData as Data, withName: fileName,fileName:imgName, mimeType: "image/jpg")
            }else{
                imgName =  String(describing: timeStamp) + ".pdf"
                multipartFormData.append(imageData as Data, withName: fileName,fileName:imgName, mimeType: "application/pdf")
            }
            for (key, value) in params! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to:strURL)     { (result) in
                            switch result {
                            case .success(let upload, _ ,_ ):
                                upload.uploadProgress(closure: { (progress) in
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                })
                                
                                upload.responseJSON { response in
                                    let resJson = response.result.value
                                    if resJson != nil{
                                        
                                        let res = resJson as! Dictionary<String, Any>
                                        var mediaUploadRespo: UploadSetInvitationMediaImageToGreenCloud = UploadSetInvitationMediaImageToGreenCloud.initializeUploadSetInvitationMediaImageToGreenCloud()
                                        mediaUploadRespo = UploadSetInvitationMediaImageToGreenCloud.uploadSetInvitationMediaImageToGreenCloudFromDictionary(res as [String : AnyObject?])
                                        
                                        
                                        if res.count > 0 {
                                            if res["success"] as? Int == 1 {
                                                successHandler(mediaUploadRespo)
                                            }else{
                                                failureHandler(false, mediaUploadRespo.message!)
                                            }
                                        }
                                    }
                                }
                            case .failure(let error):
                                failureHandler(false, error as! String)
                            }
        }
        }
    
    func getUrlFromDictionary(_ dictionary: [String : AnyObject]) -> String {
        var strURL = ""
        for (key, value) in dictionary {
            strURL=strURL+"&"+key+"="+(value as! String)
        }
        strURL = String(strURL.dropFirst(1))
        return strURL
    }
    
}
