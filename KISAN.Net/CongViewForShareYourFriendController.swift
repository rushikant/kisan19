//
//  CongViewForShareYourFriendController.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class CongViewForShareYourFriendController: UIViewController {

    @IBOutlet weak var lblSentCount: UILabel!
    
    @IBOutlet weak var lblGoToHome: UILabel!
    
    var count = Int()
    
   
    @IBAction func inviteMoreFriendsClicked(_ sender: Any) {
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "InviteFriendBaseVC")as!InviteFriendBaseVC
        vc.isComingFromGreenpass=false
        vc.comingFrom = StringConstants.flags.shareYourFriendsVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    
    @IBAction func btnGoToHomeClicked(_ sender: Any) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let attributedString = NSMutableAttributedString.init(string: "Go to Home Screen")
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        lblGoToHome.attributedText = attributedString
        if(count==1){
        lblSentCount.text = "Successfully shared with " + String(count) + " contact"
        }else{
        lblSentCount.text = "Successfully shared with " + String(count) + " contacts"
        }
        // Do any additional setup after loading the view.
    }
    

}
