//
//  CongratulationsViewController.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 09/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class CongratulationsViewController: UIViewController {

    @IBOutlet weak var lblGoToHome: UILabel!
    @IBOutlet weak var lblSentCount: UILabel!
    var strSenntInviteCout :Int = 0
    var comingFrom = String()
    
    @IBOutlet weak var lblLastdate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString = NSMutableAttributedString.init(string: "Go to Home Screen")
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        lblGoToHome.attributedText = attributedString
        let issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)

        getDateInFormat(dateStr: issueInviteLastDate)
        if(comingFrom == StringConstants.flags.exhIssuePass){
            lblSentCount.isHidden = true
        }else{
            lblSentCount.isHidden = false
            let valueOfSenntInviteCout = strSenntInviteCout
            let intSenntInviteCout = valueOfSenntInviteCout.formattedWithSeparator
            lblSentCount.text = intSenntInviteCout + StringConstants.singleSpace + "Greenpasses sent successfully."
        }
        
    }
    

    @IBAction func btnInviteMoreFrndsClicked(_ sender: Any) {
      
//        let  inviteFriendBaseVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
//        self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
        if(comingFrom == StringConstants.flags.exhIssuePass){
            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "InviteFriendBaseVC")as!InviteFriendBaseVC
            vc.isComingFromGreenpass=false
            vc.comingFrom = StringConstants.flags.exhIssuePass
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is InviteFriendBaseVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                nav.pushViewController(vc, animated: false)
            }
        }
    }
    
    
    func getDateInFormat(dateStr:String){
        
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: dateStr)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        lblLastdate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
        
    }
    
    
    @IBAction func btnGoToHomeClicked(_ sender: Any) {
        
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
}
