//
//  ColorConstants.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 20/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

struct ColorConstants{
    //static let colorValue = 0x114455
    static let colorPrimary = 0x25B35A
    static let colordarkPrimary = 0x008940
    static let colorPrimaryYellow = 0xdaae00
    static let colorWhite = 0xFFFFFF
    static let colorViewBanerOfChannel = 0xB1B1B1
    static let colorPrimaryString = "0x25B35A"
    static let selectedCellOfLeftDrawer = 0x199248
    static let selectedCellOfRightDrawer =  0x66666C
    static let backroundcolorOfOneToOneChatCell =  0xEEEEEE
    static let textColorOnAllFollowerChannelListPopUp = 0xF26C6F
    static let setCaptionBackViewColor = 0x312E34
    static let blueDocMediaColor = 0x24308a
    static let redPdfMediaColor = 0xff0000
    static let marunColor = "0x905020"
}

