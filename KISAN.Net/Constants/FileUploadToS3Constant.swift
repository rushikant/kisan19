//
//  FileUploadToS3Constant.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import AWSCore


#if TARGET_TESTING
let S3BucketName: String = "kisannet-channelmedias-dev/"
#elseif TARGET_DEV
let S3BucketName: String = "kisannet-channelmedias-dev/"
#else
let S3BucketName: String = "kisannet-channelmedias-prod/" 
#endif


let S3DownloadKeyName: String = "mediaPart"
var S3UploadKeyName: String = "mediaPart"
let channelMedia:String = "ChannelMedia/"
let post:String = "post/"
let channelProfiles:String = "ChannelProfileImages/"
let images:String = "images"
let documents = "documents"
let videos:String = "videos"
let audios:String = "audios"
let pdf : String = "pdf"
let doc : String = "doc"
let docx : String = "docx"
let imagePrefix = "IMG_"
let documentPrefix = "DOC_"
let pdfPrfiex = "PDF_"
let imagethumbPrefix = "THUMB_"
let videoPrefix = "Video_"
let audioPrefix = "Audio_"
let videoThumbPrefix = "VideoTHUMB_"
let pdfThumbPrefix = "pdfTHUMB_"
let docThumbPrefix = "docTHUMB_"
let thumbnail = "Thumbnail"
let s3BaseURL = "https://s3.ap-south-1.amazonaws.com/"
let imageContentType = "image/png"
let videoContentType = "video/mp4"
let audioContentType =  "audio/mp3"
let pdfContentType = "file/pdf"
let docContentType = "file/doc"



