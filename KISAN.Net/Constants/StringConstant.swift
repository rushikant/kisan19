//
//  StringConstant.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 13/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

struct StringConstants {

    struct EnglishConstant {
        static let languageName = "ENGLISH"
        // Community Profile (Menu Options)
        static let EditProfile = "Edit Profile"
        static let Invite = "Invite to channel"
        static let BlockedFollowers = "Blocked Followers"
        static let DeleteChannel = "Delete Channel"
        static let LeaveChannel = "Unfollow Channel"
        static let Mute = "Mute"
        static let UnMute = "UnMute"
        static let Chooselanguage = "Choose_your_language"

        
    }
    struct MarathiConstant {
        static let languageName = "मराठी"
    }
    
    struct HindiConstant {
        static let languageName = "हिंदी"
    }
    
    
    struct ImageNames {
        // Pending Pass screen
        static let kisan_web_logo_white_x50 = "kisan_web_logo_white_x50"
        // CreateCommunityStep2ViewController Screen
        static let privacyRadiobtnChecked = "ic_radio_button_checked"
        static let privacyRadiobtnUnChecked = "ic_radio_button_unchecked"
       
        //Navigation Bar
        static let backArrowImage = "ic_navigate_before_white_36pt"
        //User Profile screen
        static let userProfilePlaceHolderImg = "photoPlaceHolder"
      
        //User Area interst screen
        static let categotyPlaceholderImg = "defaultChannel"
        //DiscoverViewController
        static let communityPlaceholderImg = "defaultChannel"
        static let communityPlaceholderRSSfeedImg = "RSSFeedDefault"

        static let LeftDrawerProfilePlaceholderImg = "step1_avatar"
        static let AcceptStatusIcon = "AcceptStatusicon"
        static let AttendedStatusIcon = "attendedicon"
        //Create community1 screen
        static let communityTypeGroupImg = "access_contacts"
        static let communityTypeChannelImg = "channel_black"
        
        //Channel Dash Board VC
        static let notificationsWhiteImg = "ic_notifications_none_white_24dp"
        
        // ChannelSendBroadCastMsgVC
        static let sendChatMesgGreenBtn = "send_green"
        static let sendChatMesgGrayBtn = "send"
        static let pendingMessage = "pending_white_16"
        static let image_icon = "image_icon"
        static let ic_videocam_white = "ic_videocam_white"
        static let ic_play_circle_outline_white = "ic_play_circle_outline_white"
        static let play_audio = "play_audio"
        static let ic_file_download = "ic_file_download"
        static let baseline_delete_black_18dp = "baseline_delete_black_18dp"
        static let ic_file_download_white_36dp = "ic_file_download_white_36dp"
        static let clock = "clock"
         static let Pavilion = "Pavilion filter icon"
        static let pdfDefaultImage = "PDF app default"
        static let post = "Post button icon – 36"
        static let postBlack = "Post button icon – 36_black"

        //Community profile screen
        static let optionMenu = "ic_more_vert_white"
//        static let senderBubble = "sender_bubble.9"
//        static let receiverBubble = "receiver_bubble.9"
        static let senderBubble = "send-bubble"
        static let receiverBubble = "receive-bubble"
        static let attachFile = "ic_attach_file_white_24dp"
        static let moreOptions = "ic_more_vert_white"
        static let ic_arrow_forward_black_24dp = "ic_arrow_forward_black_24dp"
        static let plain_white_chat = "plain_white_chat"
        static let video_image = "video-image"
        static let doc_image = "doc-image"
        
        // Attachement option list images
        static let camera = "camera_new.png"
        static let image = "gallery_new"
        static let video = "video_new"
        static let audio = "audio_new"
        static let location = "location_new"
        static let doc = "Document icon"
        
        //Home screen
        static let homescreen_pageritem_one = "homescreen_pageritem_one"
        static let homescreen_pageritem_two = "homescreen_pageritem_two"
        static let homescreen_pageritem_three = "homescreen_pageritem_three"
        
        static let homescreen_pageritem_new_one = "homescreen_pageritem_new_one"
        static let homescreen_pageritem_new_two = "homescreen_pageritem_new_two"
        static let homescreen_pageritem_new_three = "homescreen_pageritem_new_three"
        
        //User Area interest screen
        static let unchecked = "newunchecked-checkbox"
        static let checked = "checked"
        
        //Discover Channel
        static let searchIcon = "ic_search_white"
        static let filterChannel = "filterfinal"
        static let closeSearchIcon = "ic_close_white"
        static let refreshIcon = "ic_autorenew_white"
        
       // Left Drawer
        static let chat = "chat"
        static let channel = "channel"
        static let kisanlogo = "kisanlogo_white"
        static let greenpassicon = "nav_greenpass_icon"
        static let exhibitor = "exhibitor"
        static let language = "ic_language_black_36dp"
        static let share = "ic_share_white_24dp"
        static let help = "ic_help_white_24dp"
        static let logout = "logout_36_white"
        static let Layout = "layout"
        static let InviteFriends = "invite_new"
        static let IssueGreenPass = "issuegreenpass_new"
        static let badgeIssue = "badgeIssue"
        static let guestlist_icon = "guestlist_icon"

        

        //Onetoonechat screen
        static let helpNav = "ic_info_outline_white_24dp"
        static let forword = "ic_forward_white_24dp"
        static let delete = "ic_delete_white_24dp"
        static let copy = "ic_content_copy_white_24dp"
        static let hide = "hide_white_36px"
        static let reply = "ic_reply_white"
    
        //AllFollowerChannelList scren fron ShowUserProfile screen
         static let channelblack = "channel_black"
         static let sms = "sms"
         static let clear = "clear"
        
        //DashBoard Screen
        static let menuOption = "abc_ic_menu_moreoverflow_mtrl_alpha"
        static let notificationBell = "new_notification_white"
        static let notificationReceivedBellIcon  = "notificationReceivedBellIcon"
        static let discover = "discover"
        static let ic_camera_dark_gray_16dp = "ic_camera_dark_gray_16dp"
        static let videoImage = "videoImage"
        static let headphone_16px = "headphones-24x24"
        static let locationDash = "locationDash"
        static let ic_album = "ic_album"
        static let bannerEnglish = "banner English"
        static let bannerhindi = " banner hindi"
        static let bannerMarathi = "banner marathi"
        static let rssFeedIcon = "rssFeedIcon"
        static let aboutApp = "AboutApp"
        //ChannelSendBroadcastMsgVC camera pop up
        static let cameraBlack = "cameraBlack"
        static let videoBlack = "videoBlack"
        static let document = "Document small"

        //ChannelSendBroadcastMsgVC audio pop up
        static let audioRecordGallery = "audio-record-gallery"
        static let audioRecord = "audio-record"
        static let imageDefaultMediaPartPlaceHolderImg = "image_default"
        static let rssFeedDefault = "default_rssfeed"
        static let docDefaultImage = "Doc app default"
        static let pdfRedIcon = "PDF red 36"
        static let docRedIcon = "Doc red 36"
        static let documentThumbnail = "documentThumbnail"
        static let docwhite70 = "Doc white 70"
        static let pdfWhite70 = "PDF white 70"


        
        //RecordAudioViewController Screen
        static let stopRecording = "stop-recording"
        static let playRecording = "play-recording"
        
        //Navigation bar of RSSFeed Details
        static let kisan_smoll_logo = "kisan_smoll_logo"
        
        //createpost vc
        static let yellow_bubble_circle = "yellow_bubble_circle"
        
        
        //Media Popup
        static let videoGray = "upload_video_gray"
        static let videoGreen = "uploadvideo_green"
        static let imageGray = "upload_image_gray"
        static let imageGreen = "upload_image_green"
        static let docGray = "uploaddoc_gray"
        static let docGreen = "upload_doc_green"

        
        //Greenpassbannerr
        static let greenArrowGo = "rightArrowOnDashboard"
        static let arrowGoGreen = "arrowGoGreen"
        static let arrowGoYellow = "arrowGoYellow"
        
        
        //MygreenPass
        static let defaultuser = "default_user"
        static let rotate = "rotate-24"


        //ExhBadges
        static let uncheck_gray = "uncheck_gray"
        static let checkboxBrown = "checkboxBrown"
        static let exhBadge = "exhBadge"


    }
    struct PushNotificationAction {
        static let Followchannel = "followChannel"
        static let link = "link"
        static let upgrade = "update"
        static let message = "message"
        static let title = "title"
        static let description = "description"
        static let companyName = "communtyName"
        static let mediaUrl = "mediaUrl"
        static let custom = "custom"
        static let pushNotificationaAtion = "pushNotificationAction"
        static let CommynityKey = "communityKey"
        static let communityProfileImageUrl = "communityProfileImageUrl"
        static let contentField1 = "contentField1"
        static let sponsored = "sponsored"
        static let communityImageBigThumbUrl = "communityImageBigThumbUrl"
        static let sponsoredMessageTitle = "messageText"
        static let sponsoredMessageDescription = "lastMessageContentField2"
        static let sponsoredMessageId = "sponsoredMessageId"
        static let mediaType = "mediaType"
        static let sponsoredMsgcompanyName = "communityName"
        static let lastMessageContentField3 = "lastMessageContentField3"
        static let lastMessageContentField8 = "lastMessageContentField8"
        static let isSponsoredMessage = "isSponsoredMessage"
        
        
        //push notification action Mapping
        static let action = "action"
        static let text = "text"
        static let id = "id"
        static let communtyName = "communityName"
        static let messageType = "messageType"
        static let contentField2 = "contentField2"
        static let contentField3 = "contentField3"
        static let contentField4 = "contentField4"
        static let contentField5 = "contentField5"
        static let contentField6 = "contentField6"
        static let contentField7 = "contentField7"
        static let contentField8 = "contentField8"
        static let contentField9 = "contentField9"
        static let contentField10 = "contentField10"
        static let senderId = "senderId"
        static let communityId = "communityId"
        static let jabberId = "jabberId"
        static let createdDatetime = "createdDatetime"
        static let hiddenDatetime = "hiddenDatetime"
        static let deletedDatetime = "deletedDatetime"
        static let hiddenbyUserId = "hiddenbyUserId"
        static let deletedbyUserId = "deletedbyUserId"
        static let communityProfileUrl = "communityProfileUrl"
        
        
        //ovelay notification
        static let imgDocLogo = "document_big"
        static let imgLocationLogo = "Location_big"
        
        
        //message types
        static let photo = "Photo"
        static let audio = "Audio"
        static let video = "Video"
        static let location = "Location"
        static let Document = "Document"
        static let post = "Post"

    }
    struct Validation {
        //User Profile screen
        static let nameText_cannotBlank = "Name_cannot_be_blank"
        static let nameRequired_minTwoChar = "At_least_2_characters_required"
        static let surNameText_cannotBlank = "Surname_cannot_be_blank"
        static let surNameRequired_minTwoChar = "At_least_2_characters_required"
        static let pinCode_cannotBlank = "Pincode_cannot_be_blank"
        static let sixDigit_PinCode = "Please_enter_a_valid_digit_pincode_number"
        static let notnumber = "Name_can_not_contains_number"
        static let profile_Image_mandatory = "Profile image is mandatory"

        
        // SelectLocation screen
        static let selectState = "Please select state!"
        static let selectCity = "Please select city!"
        static let SelectDistricttitl = "-- Select District --"
        static let selectStateTitl =  "-- Select State --"
        //UserAreaInterest Screen
        static let selectsAtLeastCategory = "Please_select_at_least_one_category"
        static let selectMaxCategory = "Please_select_maximum_three_categories"
        
        //Select Members ViewController
        static let selectOneContact = "Please_select_at_least_one_contact"
        static let numbervalidation = "mobile_number_cannot_be_blank"

        //Dashboard and ChannelSendBroadcastMsgVC
        static let noLongerFollower = "You can not send & receive message to this channel. Because you are no longer a follower."
        static let noLongerFollwerOfoneToOne = "noLongerFollwerOfoneToOne"
        static let PleaseWait = "loading please wait"

        //Exhibitor Login
        static let userNameCantBlank = "Username cannot be blank"
        static let passwordCantBlank = "Password cannot be blank"
        static let userNamePasswordNotMatch = "Username and Password did not match"
        
        //channelSendBroadcast  For create post
        static let promotepostPopUptext = "Promotrposttext"
        static let promotepost = "Promote this Post"
        
        //set Invite Summary
        static let validYoutubeLink = "Please enter a valid youtube url"
        static let EmptyText = "please enter a youtube url"
        
        static let nameValidation = "At least 2 characters required in first name. No digits and special characters allowed"
        static let lastNameValidation = "At least 2 characters required in first name. No digits and special characters allowed"

        static let phoneNumValidation = "Please enter a valid phone number"


    }
    
    struct NavigationTitle {
        static let back = "Back"
        static let aboutApp = "About App"

        static let next = "Next"
        // Create goup screen
        static let createGroup = "Group"
        static let createChannel = "Create Channel"
        static let done = "Done"
        // User Profile
        static let yourProfile = "Your Profile"
        
        // User Area Interest
        static let myInterests = "My Interests"
        
        // User Area Interest (Comming from create community)
        static let selectCategory = "Select_Categories"

        //Select location screen  
        static let selectLocation = "Select_Location"
        
        //Dashboard screen
        static let dashboard = "KISAN.Net"
        static let forwardTo = "Forward_to..."
        
        //Discover Channel
        static let discover = "Discover"
        
        // Introducing channels screen
        static let introChannelsScreenNaviTitle = "Introducing channels"
        
        //Select members screen
         static let selectMembers = "Invite People"
         static let doneBtn = "DONE"
        
        // Invite manually screen
         static let invite = "INVITE"
         static let inviteManually = "Invite Manually"
        
        //Block Members List Screen
        static let blockedMembers = "Blocked Followers"
        
        //Message Information
        static let messageInformation = "Message Information"
        
        //AllFollowChannelList Screen
        static let allFollowChannelList = "CHANNELS YOU FOLLOW"
        
        // Support VC
        static let support = "Support"
        
        //Exhibitor login
        static let LoginAsExhibitior = "Login as Organization"
        
        //create post Title
        static let CreatePost = "NEW POST"
        
        
        //set Invite Summary
        static let seyInviteSummary = "Personalised Invitation Message"

        static let knowMoreTitl = "Invite Friends to KISAN 2019"
        
        static let listBrochures = "List"
        static let scannedResult = "Scanned Result"
        static let listWithRecords = "List({{VALUE}} records)"
        static let scan = "Scan"
        static let balance = "Balance"
        static let greenpassQuota = "Greenpass Quota"

        static let guestList = "Invited Guests"


    }
    
    struct TextOnAllScreen {
        //User Profile screen (Camera), CreateCommunityStep1ViewController
        static let camera = "Camera"
        static let photoLibrary = "Photo Library"
        static let cancel = "Cancel"
    }
    
    struct StoryBoardName {
        static let main = "Main"
        static let dashboard = "Dashboard"
    }
    
    struct placeHolder {
        // DiscoverChannelViewController
        static let search = "Search"
        //CreateCommunityStep1ViewController
        static let groupName = "Group Name"
        static let channelName = "Channel Name"
        //SetCaptionViewController
        static let writeCaption = "Write_Caption"
        //Edit User Profile
        static let aboutYou = "Max. 140 characters"
        //Channel Profile
        static let noCommunityDesc = "No_channel_description"
        //Select MEmber Vc
        static let SelectContact = "select_contacts"
        static let NoEnterManually = "Enter_Number"
        //create post Vc
        static let EnterpostTitl = "maxTitl"
        static let EntertPostMessage = "maxmsg"
    }
    
    struct DrawerMenuNames{
        static let Home = "Home"
        static let Discover = "Discover"
        static let Start_your_channel = "Start your channel"
        static let Kisan_2018 = "Kisan 2019"
        static let My_Greenpass = "My Greenpass"
        static let Exhibitor = "Exhibitor"
        static let Language = "Language"
        static let Share = "Share"
        static let Support = "Support"
        static let Logout = "Logout"
        static let leftDrawerbuttonIcon = "ic_menu_white"
        static let MyChannel = "My Channel"
        static let FairLayout = "FairLayout"
        static let Invite = "Invite Friends"
        static let IssueGreenPass = "Issue Greenpass"
        static let AboutApp = "About App"
        static let Issue_badges = "Issue badges"
        static let guest_list = "Guest List"

    }
    
    struct rssFeed {
        static let readMore = "READ MORE"
    }
    
    
    struct popUpMenuName {
        //ChannelSendBroadcastMsgVC
        // Attachment List
        static let camera = "Camera"
        static let image = "Image"
        static let video = "Video"
        static let audio = "Audio"
        static let location = "Location"
        static let doc = "doc"
        static let post = "post"
        
        //MenuList
        static let channelprofile = "Channel profile"
        static let viewProfile = "View Profile"
        static let search = "Search"
        static let invite = "Invite"
        static let mute = "Mute"
        
        //ChannelFollower list
        static let removeFromChannle = "Remove from channel"
        static let blockFollower = "Block follower"
        
        //AllFollowerChannelList scren fron ShowUserProfile screen
        static let sendaMessage = "Send a Message"
        static let leaveChannel = "Unfollow Channel"
        
        //Dashboard Screen
        static let inviteToKisan = "Invite_to_App"
        static let changeLanguage = "Change Language"
        
         //ChannelSendBroadcastMsgVC camera pop up
        static let takeApicture = "Take a picture"
        static let recordavideo = "Record a video"
        
        //ChannelSendBroadcastMsgVC audio pop up
        static let recordAudio = "Record audio"
        static let audioFrmGallary = "Choose audio from gallery"
    }
    
     struct NSUserDefauluterKeys{
        static let commingFromScreen = "commingFromScreen"
        static let middlewareToken = "middlewareToken"
        static let token = "token"
        static let userFirstName = "userFirstName"
        static let userLastName = "userLastName"
        static let username = "username"
        static let userId = "userId"
        static let mobileNum = "mobileNum"
        static let ejabberdServerUsreName = "ejabberdServerUsreName"
        static let signUpResponseCode = "signUpResponseCode"
        static let loginResponseCode = "loginResponseCode"
        static let checkedLogin = "checkedLogin"
        static let oAuthToken = "oAuthToken"
        static let interests = "interests"
        static let about = "about"
        static let pin = "pin"
        static let accountType = "accountType"
        static let country = "country"
        //Discover channel screen - key for dictionary
        static let messageAt = "messageAt"
        static let showOverlayOnDiscover = "showOverlayOnDiscover"
        static let last_activity_datetime = "last_activity_datetime"
        
        //Home Screen & User profile screen for login & sign up
        static let imageBigthumbUrl = "imageBigthumbUrl"
        static let imageSmallthumbUrl = "imageSmallthumbUrl"
        static let imageUrl = "imageUrl"
        static let state = "state"
        static let city = "city"
        static let categories = "categories"
        
        // Appdelegate, HomeViewController, UserProfileViewController
        static let  FCMToken = "FCM_Token"
        //Filter screen
        static let communityListData = "communityListData"
        //ChannelSendBroadcastMsgVC & OneToOneFromAdminVC screen  DontPlaySoundForThisCommunityKey
        static let DontPlaySoundForThisCommunityKey = "DontPlaySoundForThisCommunityKey"
        //
        static let setNotificationReceivedValue = "setNotificationReceivedValue"
        //Set CommunityKey for notification  setCommunityKeyForCheckUserAvailableOnSameScreenForPushNoti
        static let setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad = "setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad"
        static let setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne = "setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne"

        //receivedMessages in push notification
        static let receivedMessages  = "receivedMessages"
        static let isSponsoredMessage = "isSponsoredMessage"
        
        
        //GreenCloud data
        static let isMitra  = "isMitra"
        static let pendingInvitationCount  = "pendingInvitationCount"
        static let sessionId  = "SessionId"
        
        //Quota Available count
         static let standard =  "unlimited_standard"
         static let extra = "unlimited_extra"
         static let sent = "unlimited_sent"
         static let  skip_media  = "skip_media"
         static let  media_type  = "media_type"
         static let  media_link = "media_link"
        static let  channelName  = "channelName"
        static let  channelColour  = "channelColour"
        static let  mediathumb = "mediathumb"
        static let  channelBigThumb = "channelBigThumb"
        static let channel_min_color = "channel_min_color"
        static let imageurl = "imageurl"
        static let pendingInviteCount = "pending_invite"
        static let myGreenPassCount = "myGreenpass_count"
        static let issueGreenpasslastDate = "lastDate"
        static let greenpassAcceptLast_date = "greenpassAcceptLast_date"
        static let responseCode = "responseCode"
        static let isViewdInvite = "isViewed"
        static let eventStartDateTime = "eventStartDateTime"
        static let show_issue_greenpass = "show_issue_greenpass"
        static let show_issue_unlimited_greenpass = "show_issue_unlimited_greenpass"

        
        
        //manage count of passe
        static let  PendingInviteCount = "pending_invite_count"
        static let MyGreenpassCount = "my_greenpass_count"
        static let IsNotFirstTime = "isNotFirstTime"
        
        //
        static let exh_badge_enddatetime = "exh_badge_enddatetime"
        static let exh_badge_enddatetime_original = "exh_badge_enddatetime_original"

        }
    
    struct NSUserDefauluterValues{
        static let CreateCommunityStep2ViewController = "CreateCommunityStep2ViewController"
        static let UserAreaInterestViewController = "UserAreaInterestViewController"
        static let EditChannelProfileVC = "EditChannelProfileVC"
        static let EditUserProfileVC = "EditUserProfileVC"
        static let loginSuccessfully = "loginToejabberd"
        static let TRUE = "true"
        static let FALSE = "false"
        static let sendto = "SEND_TO"
        static let sendtoLowercase = "Send_to"


    }

    struct flags {
        // ChannelSendBroadcastMsgVC  "attachment"
        static let moreoptions = "moreoptions"
        static let attachment = "attachment"
        //Left Drawer Screen
        static let leftDrawerShare = "leftDrawerShare"
        static let leftDrawerLogout = "leftDrawerLogout"
        static let leftDrawerLanguage = "leftDrawerLanguage"
        static let leftDrawerKisan2018 = "leftDrawerKisan2018"

        //DashboardViewController screen
        static let dashboardViewController = "DashboardViewController"
        //ChannelSendBroadcastMsgVC Screen & OneToOneFromAdminVC screen
        static let channelSendBroadcastMsgVC = "ChannelSendBroadcastMsgVC"
        static let oneToOneFromAdminVC = "OneToOneFromAdminVC"
        //PIP Mode
        static let toggle = "toggle"
        //RightViewController
        static let channelDashboardViewController = "ChannelDashboardViewController"
        static let discoverChannelViewController = "DiscoverChannelViewController"
        static let ApplyFilter = "ApplyFilter"
        static let ApplyPavilionFilter = "Apply Pavilion Filter"

        //ChannelFollowerList
        static let ChannelFollowerList = "ChannelFollowerList"
        //ShowUserProfileVC
        static let showUserProfileVC = "ShowUserProfileVC"
        //ChannelProfileVC
        static let channelProfileVC = "ChannelProfileVC"
        //Appdelegate
        static let fromJoinNotification = "fromJoinNotification"
        //UserAreaInterestViewController screen
        static let userAreaInterestViewController = "UserAreaInterestViewController"
        //selectMemberVc
        static let Invitemanually = "invitemanually"
        static let InviteFromContact = "InvitefromContact"
        //Discover
        static let FromDiscoverCategoryFilter = "CategoryFilter"
        static let FromDiscoverPavilionFilter = "PavilionFilter"
        
        static let FromUserProfile = "userprofile"
        static let FromAdmin = "admin"

        //CreatePostViewController
        static let CreatePostViewController = "CreatePostViewController"
        
        //Sponsered detail screen
        static let SponseredDetailCOntroller = "SponseredDetailCOntroller"
        
        //SetInviteSummary
        static let profileTypeImage = "profilePic"
        static let MediaTypeImage = "mediaImage"
        
        static let shareYourFriendsVc = "shareYourFriendsVc"
        static let issueBadges = "issueBadges"
        static let guestList = "guestList"

        static let exhIssuePass = "exhIssuePass"

    }
    
    struct IDS {
       static let ownerId = "baban@kisan.net"
    }
    
    struct AlertMessage {
        static let deleteMsgConfirmation = "Are you sure, you want to delete the message"
        static let logoutConfirmation = "Are_you_sure,_you_want_to_logout"
        static let pdfsendConfirmation = "Do_you_want_to_send_pdf"
        static let btnYes = "YES"
        static let btnNo = "NO"
        static let btnOK = "OK"
        static let btnCancle = "CANCLE"
        static let btnConfirm = "CONFIRM"
        static let locationNotFound = "Location_not_found"
        static let errorOccured = "Unknow error occured"
        static let leaveChannelConfirmation = "channel_do_you_want_to_leave"
        static let deleteNotofocation = "Are you sure, you want to delete the notification?"
        static let forwardMesgConfirmation = "Forward_this_message_to_new_channel"
        static let blockMembersConfirmation = "are_you_sure_you_want_to_block_member"
        static let unBlockMembersConfirmation = "are_you_sure_you_want_to_unblock_member"
        static let removeMembersConfirmation = "are_you_sure_you_want_to_remove_member"
        static let noBlockedFollower = "No blocked followers found"
        static let contactsPemission = "Unable to access contacts"
        static let LoadMore = "Load More +"
        static let enableContactPremissionText = "does not have access to contacts. Kindly enable it in privacy settings "
        static let alreadySent = "You have already requested a call back for this message"

        
    }
    

    
    //Create channel
    struct CommunityType {
        static let G = "g"
        static let C = "channel"
        static let GROUP = "Group"
        static let CHANNEL = "Channel"
        static let keyValueForCHANNEL = "createChannel"
        static let keyValueForGROUP = "createGroup"
        static let groupNaviTitle = "New Group"
        static let channelNaviTitle = "New Channel"
        static let groupNaviTitleForGroupInfo = "Group Info"
        static let channelNaviTitleForChannelInfo = "Channel Info"
        static let txtPlaceHolderForGroupInfo = "Describe your group"
        static let txtPlaceHolderForChannelInfo = "Describe your channel"
        
        static let txtPlaceHolderForTypeMessage = "Type a message"
        
        
        static let groupAboutCommunityTitle = "ABOUT THIS GROUP"
        static let groupCommunityTypeTitle = "GROUP TYPE"
        static let grouppublicTypeOfCommunity = "Public group"
        static let groupPublicDescriOfCommunity = "This group will be visible to all-members and non-members. Anyone can join this group."
        static let groupPrivateTypeOfCommunity = "Private group"
        static let groupPrivateDescriOfCommunity = "In private group, only people you invite can join this group."
        
        static let channelAboutCommunityTitle = "ABOUT THIS CHANNEL"
        static let channelCommunityTypeTitle = "CHANNEL TYPE"
        static let channelpublicTypeOfCommunity = "Public channel"
        static let channelPublicDescriOfCommunity = "This channel will be visible to all only when you have 20 Followers."
        static let channelPrivateTypeOfCommunity = "Private channel"
        static let channelPrivateDescriOfCommunity = "In Private channel, only people you invite can follow this channel"
        
    }
     //Create channel
    struct CommunityPrivacy {
        // GroupInfo Screen
        static let publicPrivacy = "public"
        static let privatePrivacy = "private"
        static let PUBLIC = "Public"
        static let PRIVATE = "Private"
    }
    
    struct DiscoverChannel {
        static let follower = " Follower"
        static let following = "FOLLOWING"
        static let follow = "FOLLOW"
        static let zeroFollowerCount = "0"
        static let NoFollower = "NO FOLLOWER"
        static let recommended = "recommended"
        static let searchResult = "searchResult"
        static let Colorcode = "F"

    }
    
    struct RightViewController {
      static let apply =   "APPLY "
      static let filters = " FILTERS"
      static let Requested = "requested_to_you_follow_their_channel"
      static let SelectAll =   "SELECT CATEGORIES"
       static let SelectPavilion =  "SELECT PAVILION"
      static let ClearAll = "CLEAR ALL"
      static let NoNoti = "No_notifications"

    }
    
    struct RecordAudioViewController {
        static let TapToRecord =  "Tap_to_start_recording"
        static let talk = "Talk now. Tap"
        static let Stoprecording = "to stop recording"

    }
    struct CreatePostViewController {
        static let titleofpost = "TITLE OF THE POST"
        static let messageOfPost = "YOUR MESSAGE"
        static let PhotoOrVideo = "PHOTO OR VIDEO"
        static let validationTitl = "Please enter the title"
        static let validationMessage = "Please enter your message"
        static let post = "post"
        static let Message = "Message"
        static let receiveUpdateandStartDialog = "Receive updates or Start a Dialogue"
        static let FollowChannel = "Follow Channel"
        static let Attach = "Attach"
        static let Followed = "Followed"
        
        //Sponsore Message Details screen
        static let thankYouMsg = "Thank you! Your contact number has been shared with "
    }
    
    struct DashboardViewController{
        static let follwStatus = "channel_banner_join"
        static let unfollowStatus = "channel_banner_left"
        static let channelCreation = "YOU CREATED THE CHANNEL"
        static let updateChannel = "channel details updated"
        //Show message time status on dashboard
        static let now = "NOW"
        static let aMinuteAgo = "a minute ago"
        static let minutesAgo = "minutes ago"
        static let anHourAgo = "an hour ago"
        static let hoursAgo = " hours ago"
        static let yesterday = "yesterday"
        static let  daysAgo = " days ago"
        //Show latest message status
        static let image = "latest_msg_Image"
        static let video = "latest_msg_video"
        static let audio = "latest_msg_audio"
        static let location = "latest_msg_location"
        static let MailSubject = " KISAN.Net Beta App"
        
        //New User Overlay view
        
        static let Hi = "Hi"
        static let Welcome1 = "Welcome1"
        static let Welcome2 = "Welcome2"
        static let Discover = "DiscoverFirst"
    }
    struct ExhibitorAdvertiseScreen{
       
        static let eventDate = "event_date_2018"
        static let eventTime = "Event_time"
        static let eventAddress = "Event_Address"
        static let makeSure  = "make_sure_you_are_there"
        static let Register  = "Register"
        static let SkipNow  = "skip_now"

       //exhibitor login
        static let ExhibitorLogin  = "Organization Login"
        static let Initialtext  = "Initialtext"
        static let emailplaceholder  = "Username"
        static let passwordplaceholder  = "Password"
        static let navtitle  = "navtitl"
        static let Login  = "Login as Member"


    }
    
    struct channelProfile {
        static let followers = "FOLLOWERS"
        static let leaveChannelOperation = "leave"
        static let members = "MEMBERS"
        //channel follower list screen
        static let block = "block"
        static let unblock = "unblock"
        static let remove = "remove"
        static let allFollower = "Show_all_followers"
        static let AboutChannel = "ABOUT THIS CHANNEL"
        static let Categories = "CATEGORIES"
        static let productAndServices = "productAndServices"
        static let stallNo = "stallNo"
        static let addressTitle = "addressTitle"
        static let eventName = "eventName"
    }
    
    struct showUserProfile{
        static let channelsYouFollows = "CHANNELS_YOU_FOLLOW"
        static let commonChannels = "COMMON_CHANNELS"
        static let viewAll = "View All"
        static let info = "INFO"
        static let thatsYou = "THAT'S YOU"
                //Edit User Profile Vc
        
        static let aboutYou = "ABOUT YOU"
        static let Interests = "INTERESTS"
        static let AddMore = "ADD MORE +"
        static let name = "First Name"
        static let surname = "Last Name"
        static let pincode = "Pin Code"
        static let Photo = "Photo"


    }
    
    struct supportViewController{
        static let SupportInfo = "string_content1"
        static let contactNo = "contactNo"
        static let Email = "Email"
        static let Supporttextpart2 = "string_content2"

        static let Namaskar = "Namaskar"
        static let Regards = "Regards"
        static let TeamKisan = "TeamKisan"
        static let helptext = "we_would_be_happy_to_help_you"

    }
    
    struct setInviteSummaryViewController{
        static let send = "Send"
        static let FreeGreenPass = "Free Greenpasses"
    }
    
    struct kisanMitra {
        //Contact list
        static let Ji = "Ji"
        static let acceptGreenpass = "Accept FREE Greepass"
        static let viewGreenPass = "View My Greenpass"
        static let buyGreenpass = "Buy Greenpass"

        static let openBrace = "("
        static let closeBrace = ")"
        static let kisanYoutubeUrl = "https://www.youtube.com/watch?v=GdFJtdMupfI&feature=youtu.be"
        static let before = "Before"
        static let shareWithFriends = "Share with Friends Now!"
        static let next = "Next"
        static let exhBadge = "Exhibitor Badge"



    }
    
    static let groupNameText_cannotBlank = "Group name cannot be blank"
    static let channelNameText_cannotBlank = "Channel name cannot be blank"
    static let EMPTY = ""
    static let ShowMore = ".."
    static let AND = "&"
    static let commaspace = ", "
    static let TRUE = "true"
    static let FALSE = "false"
    static let ZERO = "0"
    static let ONE = "1"
    static let TWO = "2"
    static let country = "India"
    static let appLoginParameter = "chat"
    static let singleSpace = " "
    static let urlRemoveSpace = "%20"
    static let comma = ","
    static let no = "NO"
    static let null = "null"
    static let AppLink = "https://itunes.apple.com/in/app/kisan-net/id1297223018?mt=8"
    static let global = "global"
    static let global_ios = "global_ios"
    static let global_prod = "global_prod"
    static let global_ios_prod = "global_ios_prod"
    static let Sponsered = "Sponsored"
    static let less = "Less"
    static let More = "more"
    static let file = "file"
    static let Approved = "Approved"

    //commenteddd by ankit
    struct PassType {
        static let MitraIvite = "mitrainvite"
        static let UnlimitedExhibitorInvite = "unlimitedexhibitorinvite"
        static let Mitra="mitra"
        static let onlinebadge="onlineexhibitorbadge"
        static let exhibitorinvite = "exhibitorinvite"
    }
    static let EventType = "Single Entry"
    static let MultiEntry = "Multi Entry"
    
    struct Status {
        static let pending = "pending"
        static let sent = "sent"
        static let deliveredAckPending = "deliveredAckPending"
        static let delivered = "delivered"
        static let read = "read"
        static let readAckPending = "readAckPending"
    }
    
    struct MessageType {
        static let chatMessage = "chat"
        static let mapLocation = "location"
        static let banner = "banner"
        static let text = "text"
        static let feed = "feed"
        static let chat = "chatmessage"
        static let post = "post"

    }
    
    struct MediaType{
        static let image = "image"
        static let video = "video"
        static let audio = "audio"
        static let text = "text"
        static let document = "document"
        static let documentsdoc = "documentsdoc"
        static let documentspdf = "documentspdf"
        static let location = "location"
        //static let documents = "xml"
        static let doc = "doc"
        static let docx = "docx"
        static let pdf = "pdf"
        static let post = "post"
        static let imageOfPostCreation = "imageOfPostCreation"
        static let link = "link"
        static let externallink = "externallink"

    }
    struct SelectLocation{
        static let currentlocation = "CURRENT LOCATION"
        static let AroundYouLocation = "AROUND YOU"
    }
    
    
    struct documentDirectory {
       static let documentDirectoryName = "multiMediaDirectory"
    }
    
    struct DiscoverViewController {
      static let following = "FOLLOWING"
      static let tap = "Tap_Filter"
      static let gotIt = "GOT IT"
    }

    
    struct issueExhBadgeViewController {
        static let editContactsNote1 = "Please ensure names are correct before issuing the badge. Tap on name to make changes. \n"
        static let editContactsNote2 = "You are currently issuing the badges for \n "
        static let Badgelastdate = "The facility to issue badged is available till"
        static let issueBadges = "ISSUE BADGES"
    }
    
    struct coreDataEntityName {
        static let communityDetailTable = "CommunityDetailTable"
        static let messages = "Messages"
        static let myChatsTable = "MyChatsTable"
        static let membersTable = "MembersTable"
        static let notificationTable = "NotificationTable"
    }
    
    struct ToastViewComments {
        //Discover screen
        static let followChannel = "You_have_followed_the_channel"
        //Dashboard Screen
        static let logout = "User_successfully_logged_out."
        //User Area interest screen
        static let channelCreat = "Channel has been created successfully."
        //channel profile
        static let channelCanNotleave = "Channel owner can not leave channel"
        static let channelNameNotBlank = "Channel_name_can_not_be_blank"
        //Right drawer viewcontroller
        static let applyeFilterValidation = "Tap_on_categories_to_apply_filter"
        static let applyeFilterSuccess = "Filter appiled successfully"
        //Edit User Profile
        static let selectAtleastOneInterest = "Please select atleast one interest"
        static let updatedUserProfile = "Profile Updated Successfully"
        //OneTOne
        static let imageNotFormat = "Image not in proper format"
        static let videoNotFormat = "Video not in proper format"
        static let resultNotFound = "No Search Result Found"
        //ShowUserProfileVC
        static let noInterNet = "No Internet Connection. Please try again!"
        // Channel profile
        static let mute = "Channel Muted"
        static let unMute = "Channel UnMuted"
        //Edit Channel profile
        static let channelProfileUpdated = "Channel_Profile_Updated_Successfully"
        static let InvitationSent = "Invitation sent successfully"
        //chennel send broadcast
        static let deleteConfirmation = "Message deleted successfully"
        static let failureMsg = "failureMsg"
        
        //Kisan Mitra
        static let All_feildsare_mandatory = "All feilds are mandatory"
        static let incomplete_information = "Information is incomplete!"
        static let select_only = "You can select only"
        static let remaining_count = "remaining Contacts"
        static let Insufficientquota_for_stall = "Insufficient quota for stall"


    }
    
    struct pushNotificationConstants{
        static let action = "action"
        static let gcm_message_id = "gcm.message_id"
        static let aps = "aps"
        static let myChat = "myChat"
        static let joinCommunity = "joinCommunity"
        static let joinPushNotificationCenter = "joinPushNotificationCenter"
        static let showNotificationOnRightDrawer = "showNotificationOnRightDrawer"
        static let appPushNotification = "appPushNotification"
        static let notification = "notification"
        static let block_member = "block_member"
        static let unblock_member = "unblock_member"
        static let remove_member = "remove_member"
        static let blockUnBlockRemoveMemberPushNotificationCenterOnDashboard = "blockUnBlockRemoveMemberPushNotificationCenterOnDashboard"
        static let blockUnBlockRemoveMemberPushNotificationCenterOnChannelFolloweList = "blockUnBlockRemoveMemberPushNotificationCenterOnChannelFolloweList"
        static let shaoutMyChatData = "shaoutMyChatData"
        // Right Drawer
        static let channelProfileFrmNotification = "channelProfileFrmNotification"
        static let myChatData = "myChatData"
        
        //Channel notification (SelectMembers screen)
        static let notificationLanguageType = "en"
        static let appInvitation = "appInvitation"
        static let invite = "invite"
        static let created = "created"
        static let messageDeleted = "message_deleted"
        static let messages = "messages"
        static let sponserMessages = "sponseredMessage"
        static let appUpdate = "Update"
        static let isSponsored = "isSponsored"
        static let customNotificationForAllUser = "CustomNoti"
        static let appUpdateLink = "link"

    
    }
    
    struct KnowMoreViewController {
        static let issueGreenPassDateText = "1. The last date of sending invitation is "
        static let acceptGreenPassDateText = "2. Invited guest has to accept the invite before "
    }
    
    struct HomeViewController {
        static let Intro_titleText_one = "Discover channels"
        static let Intro_titleText_two = "Start a dialogue"
        static let Intro_titleText_three = "Register for KISAN 2020"
        
        static let Intro_DescriptionText_one = "Follow the channels of your interests. \n Stay tuned with the latest trends."
        static let Intro_DescriptionText_two = "Voice your opinions. Start a constructive \n communication with leading brands."
        static let Intro_DescriptionText_three = "Book your Greenpass. Avoid standing in \n a long queue, Save time & money."

    }
    
    
    struct MobileOTPVerificationVC {
        static let change_mobile_number = "Change mobile number"
        static let resend_OTP = "Resend OTP"
    }
    
    struct MobileOtpSendViewController {
        static let lbl_privacy_and_terms = "By clicking 'NEXT', you are indicating that you are agree to \n the, privacy policy and Terms."
        
        static let lbl_privacy_and_termsValue1 = "By clicking 'NEXT', you are indicating that you agree to "
        static let lbl_privacy_and_termsValue2 = "the, Privacy policy and Terms."


    }
    
}

