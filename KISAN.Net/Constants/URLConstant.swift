//
//  URLConstant.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

struct URLConstant {
    
    // oAuth
    // static let BaseUrl = "http://27.109.19.234/lynkd/personal/backend/web/api2/"
    // Test
//    static let authBaseUrl =  "http://kisanoauth-test.ap-south-1.elasticbeanstalk.com/" //"https://id.kisan.co.in/"
    //Dev
    // Test
    static let clientId = "3a037a6d-be9a-43f8-8a30-6918fc93ad9e"  //"1c714eee-0db2-431a-a941-9b27b2c4d64b"
    static let client_secret = "68eeec35-275e-42ed-8b4c-f062d53e3877" //"61678097-4baa-472c-9fa7-ea628a5d2675"
    static let sendCustomOtp = "profiles/send_custom_otp_mobile/"
    static let api_token_custom_otp_auth = "api-token-custom-otp-auth/"

    //Dev
    //static let clientId = "3a037a6d-be9a-43f8-8a30-6918fc93ad9e"
    //static let client_secret = "68eeec35-275e-42ed-8b4c-f062d53e3877"
    
    //Jersey  Test
    static let JerseyBaseURL = "http://testing.izpxtirkev.ap-south-1.elasticbeanstalk.com/webapi/"
    static let appKey = "diuvwipXMHh7pigOT5Xp494642P7w6lm"
    static let appSecret = "B42DCBC99843E928F9A784D61A9AE"
    
    //End points auth
    static let SignUp = "/users/"
   // static let Login = "/social-auth/facebook/"
    static let Login = "/social-auth/custom-auth/kisanid"

    static let LoginType = "otp"
    static let editUserProfile = "/users/"
    
    //link detection
    static let http = "http"
    static let https = "https"

    //source append for request id
    static let ios = "ios_"

    //Fair Layout Url
    static let layout = "http://layout.kisan.in/"

    
        //End points Jersy ws
    //static let discoverChannelList = "/communities?page_number=1&page_size=1000&search_text=&user_field_value=&user_field_name=&filter_category_ids="
    static let discoverChannelList1 = "/communities?page_number="
    static let discoverChannelList2 = "&page_size="
    //static let discoverChannelList3 = "&search_text=&user_field_value=&user_field_name=&filter_category_ids=&CURRENT_TIMESTAMP="
    static let discoverChannelList3 = "&search_text="
    static let discoverChannelList4 = "&user_field_value=&user_field_name=&filter_category_ids="
    static let discoverChannelList5 = "&CURRENT_TIMESTAMP="
    
    static let logoutUser = "/users/logout"
    static let createCommunity = "/communities"
    static let followCommunity1 = "/communities/"
    static let followCommunity2 = "/members/"
    static let getMyachatList = "/users/current/mychats/v2?page_size=100&page_number=1&search_text=&CURRENT_TIMESTAMP="

    static let getOneToOneMyChatList = "/users/current/onetoone/"
    static let getOneToOneMyChatList2 = "?page_size=100&page_number=1&search_text=&CURRENT_TIMESTAMP="
    //static let createOneToOneChat = "xmpp_community/createoneonone"
    static let getFollowerList1 = "/communities/"
    static let getFollowerList2 = "/members?"
    static let getFollowerList3 = "page_size="
    static let getFollowerList4 = "&page_number="
    static let getFollowerList5 = "&search_text= &show-blocked= &show-all= &country= &state= &city= &userid= &show-removed=&CURRENT_TIMESTAMP="
    static let getBlockFollowerList = "&search_text=s&show-blocked=true&show-all=&country= &state= &city= &userid= &show-removed=&CURRENT_TIMESTAMP="
    static let greenpassToken = "?token="
   // static let getBlockFollowerList = "page_size=100&page_number=1&search_text=s&show-blocked=true&show-all=&country= &state= &city= &userid= &show-removed=&CURRENT_TIMESTAMP="
    
    static let getUserDetails1 = "/users/12?field_value="
    static let getUserDetails2 = "&field_name&accnt_type"

    static let getCommonChannelList1 = "/communities/members/"
    static let getCommonChannelList2  = "?page_number=1&page_size=1000&search_text="
    static let leaveCommunity1 = "/communities/"
    static let leaveCommunity2 = "/members/"
    static let blockUnBlock1 = "/communities/"
    static let blockUnBlock2 = "/members/"
    static let usersCurrentOnetoOne = "/users/current/onetoone"
    
    static let getOldMessages1 =  "/communities/"
    static let getOldMessages2 =  "/messages?page_size="
    static let getOldMessages3 =  "&page_number="
    static let getOldMessages4 =  "&userId="
    static let getOldMegOneToOneTimpStmp = "&CURRENT_TIMESTAMP="
    static let getOldMessageOfBroadcast = "&userId=&CURRENT_TIMESTAMP="
    
    static let deleteMessage1 =  "/communities/"
    static let deleteMessage2 = "/messages/"
    static let c_OneToOne = "c_OneToOne"
    static let muteUnmute = "/users/current/mychats/"
    static let invitation = "/invitation"
    
    //sendMessageInDB
    
    static let sendMessageInDB = "/communities/message"

    //Get Chaanel Profile data
    static let getChannelProfile = "/communities/"
    static let getChannelProfileTimeStamp = "?CURRENT_TIMESTAMP="
  
    //Exhibitor advertise
    static let exhibitionDetails = "v1/geteventdetails.php"

    
    //GreenCloud API
    
    static let greenCloudLogin = "v1/signin.php"
    static let inviteSummary = "v1/getinvitesummary.php"
    static let uploadProfilePicToGreenCloud1 = "v1/upload_file_exhibitor_logo.php?"
    static let uploadProfilePicToGreenCloud2 = "app_key="
    static let uploadProfilePicToGreenCloud3 = "&sessionId="
    static let uploadProfilePicToGreenCloud4 = "&type="
    static let guestList = "v1/getinvitationlist.php"
    static let appStarterdata = "v1/gcappstarter.php"
    static let sharewithYourFriend = "v1/sendsms.php"
    static let issuebadges = "v1/issuestallpasses.php"
    static let getbadgeslist = "v1/report_stall_passes.php"
    static let getexhibitordetails = "v1/getexhibitordetailsbyusername.php"
    static let getstalldetails = "v1/getstalldetails.php"
    
    //callbackAPI
    static let callback1 = "/callback_request?messageId="
    static let callback2 = "&sponsoredMessageId="




    //commented by Ankit
    static let pendingInvitationList = "v1/getpendinginvitationlist.php"
    static let acceptInvitation = "v1/acceptinvitation.php"
    static let getMyGreenpasses = "v1/getmygreenpasses.php"
    
    static let updateMyGreenpasses = "v1/update_greenpass.php"
    static let shareGreenPass = "v1/sharegreenpass.php"

    static let uploadMediaToGreenCloud1 = "v1/upload_file_exhibitor_greenpass_invite_media.php?"
    static let uploadMediaToGreenCloud2 = "app_key="
    static let uploadMediaToGreenCloud3 = "&sessionId="
    static let uploadMediaToGreenCloud4 = "&type="

    static let scanBrochureCode = "v1/scan_exhibitor_broucher_code.php"
    static let getScannedBrochures = "v1/get_scanned_broucher_by_visitor.php"
    
    
    //commented by ankit
    static let uploadFileVisitor = "v1/upload_file_visitor.php"
    static let uploadVisitorProfileToImage = "v1/upload_profile_image.php"

    static let issueInvites = "v1/issueinvites.php"
    static let getInvitationList = "v1/getinvitationlist"
    //greenCloud Request Parametre
    static let eventCode = "KISAN19"
    static let app_key = "nLyJTPX30kvsyuX5K7KvpQD2xB091F65"
    static let filter1 = "t1.type"
    static let filter2 = "t2.username"
    
    static let filter4 = "t5.type"
    static let filter5 = "t2.username"

    //kisan mitra
    static let updateinvitesummaryinfo = "v1/updateinvitesummaryinfo.php"
    
    //Term and condition URL
    static let termAndConditionURL = "http://kisan.net/t&c/#one"
    static let privacyPolicyURL = "http://kisan.net/t&c/#second"


    #if TARGET_TESTING
    // New Constant 192.168.0.80
    static let authBaseUrl = "https://id.kisanlab.com/"
    static let BASE_URL = "https://middleware.kisanlab.com/webapi"
    // This flag for check request from which server of kisan id
    static let checkRequestFrom = "id.kisanlab.com"
    //ejabberd server host name - Test
    static let hostName = "kisan-test.m.in-app.io"
    //GreenPass URL
    static let greenPassbaseUrl = "https://greenpass.kisanlab.com/ios/#/"
    //Visitor login headers
    static let X_API_KEY = "8b88e789-d091-4379-be3b-8107cce27c24"
    static let X_API_SECRET = "0221ee84-c793-418a-bbaa-1441d4200933"
    static let APPLICATION_JSON = "application/json"
    static let BEARER = "bearer"
    
    //Ebhitor login
    static let exhibitor_client_id = "3a037a6d-be9a-43f8-8a30-6918fc93ad9e"
    static let exhibitor_client_secret = "68eeec35-275e-42ed-8b4c-f062d53e3877"
    //Ebhitor login headers
    static let EXHIBITOR_X_API_KEY = "bb50938f-acdd-4cdf-8e56-289fb8d70e52"
    static let EXHIBITOR_X_API_SECRET = "eab633ae-b671-4ea7-8b17-929092405b6e"
    static let EXHIBITOR_APPLICATION_JSON = "application/json"
    static let EXHIBITOR_X_REQUEST_ID = "ios_exhibitor"
    static let ExhibitorLogin = "/exhibitor/orglogin"
    
    static let SHARING_URL =  "Check out KISAN.Net app//" + "https://itunes.apple.com/in/app/kisan-net/id1297223018?mt=8"
    
    //Exhibitor advertise
    static let GREENPASS_APPKEY = "nLyJTPX30kvsyuX5K7KvpQD2xB091F65"
    static let GREENPASS_BASE_URL = "http://greencloud.kisanlab.com/"
    static let GREENPASS_EXHIAPPKEY = "9E1E6222E53CF2316B1AA51D57345"
    static let EVENT_CODE = "KISAN18"
    
    #elseif TARGET_DEV
    static let authBaseUrl = "https://id.kisanlab.com/"
    static let BASE_URL = "https://middleware.kisanlab.com/webapi"
    // This flag for check request from which server of kisan id
   static let checkRequestFrom = "id.kisanlab.com"
   // static let BASE_URL = "http://192.168.0.130:8080/kisanchat/webapi"
    //ejabberd server host name - Dev
    static let hostName = "kisan-test.m.in-app.io"
    //GreenPass URL
    static let greenPassbaseUrl = "https://greenpass.kisanlab.com/ios/#/"
    static let SHARING_URL =  "Check out KISAN.Net app//" + "https://itunes.apple.com/in/app/kisan-net/id1297223018?mt=8"
    
    //Visitor login headers
    static let X_API_KEY = "8b88e789-d091-4379-be3b-8107cce27c24"
    static let X_API_SECRET = "0221ee84-c793-418a-bbaa-1441d4200933"
    static let APPLICATION_JSON = "application/json"
    static let BEARER = "bearer"
    
    //Ebhitor login
    static let exhibitor_client_id = "3a037a6d-be9a-43f8-8a30-6918fc93ad9e"
    static let exhibitor_client_secret = "68eeec35-275e-42ed-8b4c-f062d53e3877"
    //Ebhitor login headers
    static let EXHIBITOR_X_API_KEY = "bb50938f-acdd-4cdf-8e56-289fb8d70e52"
    static let EXHIBITOR_X_API_SECRET = "eab633ae-b671-4ea7-8b17-929092405b6e"
    static let EXHIBITOR_APPLICATION_JSON = "application/json"
    static let EXHIBITOR_X_REQUEST_ID = "ios_exhibitor"
    static let ExhibitorLogin = "/exhibitor/orglogin"
    
    //Exhibitor advertise
    static let GREENPASS_APPKEY = "nLyJTPX30kvsyuX5K7KvpQD2xB091F65"
    static let GREENPASS_BASE_URL = "http://greencloud.kisanlab.com/"
    static let GREENPASS_EXHIAPPKEY = "9E1E6222E53CF2316B1AA51D57345"
    static let EVENT_CODE = "KISAN18"

    #else
    // do something else
    static let authBaseUrl = "https://id.kisan.in/"
    static let BASE_URL = "https://middleware.kisan.in/webapi"
    // This flag for check request from which server of kisan id
    static let checkRequestFrom = "id.kisan.in"
    //ejabberd server host name - Prod
    static let hostName = "kisan.m.in-app.io"
    static let greenPassbaseUrl = "https://greenpass.kisan.in/ios/#/"
    static let SHARING_URL =  "Check out KISAN.Net app//" + "https://itunes.apple.com/in/app/kisan-net/id1297223018?mt=8"
    
    //Visitor login headers
    static let X_API_KEY = "8b88e789-d091-4379-be3b-8107cce27c24"
    static let X_API_SECRET = "0221ee84-c793-418a-bbaa-1441d4200933"
    static let APPLICATION_JSON = "application/json"
    static let BEARER = "bearer"
    
    //Ebhitor login
    static let exhibitor_client_id = "3a037a6d-be9a-43f8-8a30-6918fc93ad9e"
    static let exhibitor_client_secret = "68eeec35-275e-42ed-8b4c-f062d53e3877"
    //Ebhitor login headers
    static let EXHIBITOR_X_API_KEY = "bb50938f-acdd-4cdf-8e56-289fb8d70e52"
    static let EXHIBITOR_X_API_SECRET = "eab633ae-b671-4ea7-8b17-929092405b6e"
    static let EXHIBITOR_APPLICATION_JSON = "application/json"
    static let EXHIBITOR_X_REQUEST_ID = "ios_exhibitor"
    static let ExhibitorLogin = "/exhibitor/orglogin"
    
    //Exhibitor advertise
    static let GREENPASS_APPKEY = "nLyJTPX30kvsyuX5K7KvpQD2xB091F65"
    static let GREENPASS_BASE_URL = "http://greencloud.kisan.in/"
    static let GREENPASS_EXHIAPPKEY = "9E1E6222E53CF2316B1AA51D57345"
    static let EVENT_CODE = "KISAN18"
    
    #endif
    
}



struct RequestName {
    static let SignUpUser = "signup"
    static let LoginUser = "Login"
    static let LoginExhibitor = "LoginExhibitor"
    static let ExhibitionAdvertise = "ExhibitionAdvertise"
    static let GetUserAreaInterest = "Area_Interest"
    static let GetPavilionFilterList = "Pavilion_Filter"
    static let EditUserProfile = "EditUserProfile"
    static let GetStateList = "GetStateList"
    static let GetDistrictList = "GetDistrictList"
    static let GetDiscoverList = "GetDiscoverList"
    static let GetMessageList = "GetMessageList"
    static let resourceName = "iOSJersey"
    static let loginSourceName = "ios"
    static let loginSourceExhibitorName = "exhibitor"
    static let createCommunity = "createCommunity"
    static let followChannel = "followChannel"
    static let GetMyChatList = "GetMyChatList"
    static let GetOneToOneMyChatList = "GetOneToOneMyChatList"
    static let createOneToOneChat = "createOneToOneChat"
    static let GetCommunityMembersList = "GetCommunityMembersList"
    static let GetCommonChannelList = "GetCommonChannelList"
    static let updateCommunity = "updateCommunity"
    static let logOutUser = "logOutUser"
    static let leaveChannel = "leaveChannel"
    static let sendInvitation = "sendInvitation"
    static let blockUnBlockMembers = "blockUnBlockMembers"
    static let GetUserDetails = "GetUserDetails"
    static let  deleteMessage = "deleteMesage"
    static let GetChannelProfileDetails = "GetChannelProfileDetails"
    static let SendMessageInDB = "SendMessageInDB"
    static let GetYoutubeurlData = "Youtube_data"
    static let loginExhibitorSourceName = "ios_exhibitor"
    static let sendOtp = "SendOtp"
    //Commented by Ankit
    static let GetPendingInviteList = "getPendingInviteList"
    static let AcceptInvitation = "acceptInvitation"
    static let GetMyGreenpassList = "getMyGreenpassList"
    //Exhibitor login
    static let account_type = "2"
    static let typeEmail = "email"
    static let typeMobile = "mobile"
    static let app = "admin"
    
    //greemnCloud
    static let GreenCloudLoginUser = "GreenCloudLogin"
    static let InviteSummary = "InviteSummary"
    static let UploadLogoToGreenCloud = "UploadLogo"

    //LoginTypes
    static let kisanMita = "mitrainvite"
    static let ExhibitorInvite = "exhibitor"
    static let exhPassType = "unlimitedexhibitorinvite"
    static let shareInviteToFrnds = "invite_friends_visitor"

    
    //Kisan Mitra
    static let UpdateInviteSummary = "UpdateInviteSummary"
    static let GetGuestList = "getGuestList"
    static let IssueInvites = "issueInvites"
    static let AppStarter = "AppStarter"

    
    //callback APi
    static let CallBack = "CallBack"
    static let shareInviteWithFriends = "shareInviteWithFriends"
    static let updateGreenPass = "updateGreenPass"
    static let ShareGreenpass = "shareGreenpass"
    static let IssueBadges = "issueBadges"
    static let GetBadgesList = "getBadgesList"
    static let GetStallPasses = "getStallPasses"
    static let GetExhibitorDetails = "getExhibitorDetails"

    
    //Scan Exibitor Brochure specific APIs
    static let scanExhibitorBrochureCode = "scan_exhibitor_broucher_code"
    static let getScannedBrochuersByvisitor = "get_scanned_broucher_by_visitor"
    
    //show badge list
    static let onlineexhibitorbadge = "onlineexhibitorbadge"

}

struct ResponseCode {
    static let loginResponseCode = 1001
    static let signUpResponseCode = 1002
    static let customOtpSignUpResponseCode = 2016
}

struct SelectCityURLConstant {
    //SelectLocationPopUpView
   static let stateList = "stateList"
   static let stateDistrictListEng = "stateDistrictListEng"
   static let jsonType = "json"
}
