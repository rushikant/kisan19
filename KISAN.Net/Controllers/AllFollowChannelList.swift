//
//  AllFollowChannelList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 13/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class AllFollowChannelList: UIViewController {
    var arrOfMenusAdmin = [(NSLocalizedString(StringConstants.popUpMenuName.channelprofile, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.sendaMessage, comment: StringConstants.EMPTY))]
    var arrOfMenuImagesAdmin  = [StringConstants.ImageNames.channelblack,StringConstants.ImageNames.sms]
    var arrOfMenusFollower = [(NSLocalizedString(StringConstants.popUpMenuName.channelprofile, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.sendaMessage, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.leaveChannel, comment: StringConstants.EMPTY))]
    var arrOfMenuImagesFollower  = [StringConstants.ImageNames.channelblack,StringConstants.ImageNames.sms,StringConstants.ImageNames.clear]
    @IBOutlet weak var tblOfFollowChannelList: UITableView!
    @IBOutlet weak var viewOfPopUp: UIView!
    @IBOutlet weak var viewOfBackPopUp: UIView!
    @IBOutlet weak var tblOfMenuOption: UITableView!
    var myChatsList = [MyChatsTable]()
    var userId = String()
    var myChatsForPassNext = MyChatsTable()
    var checkAdmin : Bool = false
    var commingFrom = String()
    @IBOutlet weak var heightOfViewOnPopUpView: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
    }

    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        tblOfFollowChannelList.separatorColor = UIColor.clear
        tblOfFollowChannelList.tableFooterView = UIView()
        tblOfMenuOption.separatorColor = UIColor.clear
        tblOfMenuOption.tableFooterView = UIView()
        tblOfMenuOption.isScrollEnabled = false
        
        let  lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.showUserProfile.channelsYouFollows, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AllFollowChannelList.hideMenuOptionPopUp))
        tap.cancelsTouchesInView = false
        self.viewOfPopUp.addGestureRecognizer(tap)
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
       // self.myChatsList = MyChatServices.sharedInstance.getIsMemberTypeTrueMyChatDetails()
        tblOfFollowChannelList.reloadData()
    }
    
     @objc func hideMenuOptionPopUp() {
        self.viewOfPopUp.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
     @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}

extension AllFollowChannelList : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOfMenuOption{
            if checkAdmin == true{
                return arrOfMenusAdmin.count
            }else{
                return arrOfMenusFollower.count
            }
        }else{
            return self.myChatsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblOfMenuOption{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
            cell.selectionStyle = .none
            if checkAdmin == true{
                if indexPath.row == 2 {
                    cell.lblMenuTitle.textColor = UIColorFromRGB.init(rgb: ColorConstants.textColorOnAllFollowerChannelListPopUp)
                }else{
                    cell.lblMenuTitle.textColor = UIColor.darkGray
                }
                cell.lblMenuTitle.text = arrOfMenusAdmin[indexPath.row]
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImagesAdmin[indexPath.row])
            }else{
                cell.lblMenuTitle.textColor = UIColor.darkGray
                cell.lblMenuTitle.text = arrOfMenusFollower[indexPath.row]
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImagesFollower[indexPath.row])
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelListOfYouFollowCell", for: indexPath) as! ChannelListOfYouFollowCell
            cell.selectionStyle = .none
            cell.imageviewOfUserProfile.layer.cornerRadius = cell.imageviewOfUserProfile.frame.size.width / 2
            cell.imageviewOfUserProfile.clipsToBounds = true
            let myChats = myChatsList[indexPath.row]
            if let communityName = myChats.communityName, !communityName.isEmpty {
                cell.lblChannelName.text = StringUTFEncoding.UTFEncong(string: communityName)
            }
            if let channelProfilePic = myChats.communityImageBigThumbUrl {
                cell.imageviewOfUserProfile.sd_setImage(with: URL(string: channelProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
            }
            
            if myChats.ownerId == userId{
                cell.lblUserRole.isHidden = false
            }else{
                cell.lblUserRole.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       /* if tableView == tblOfMenuOption{
            navigationController?.setNavigationBarHidden(false, animated: false)
            if indexPath.row == 0 {
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelProfileVC = storyboard.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
                channelProfileVC.myChatsData = myChatsForPassNext
               // channelProfileVC.dominantColour = myChatsForPassNext.communityDominantColour!
                viewOfPopUp.isHidden = true
                self.navigationController?.pushViewController(channelProfileVC, animated: true)
            }else if indexPath.row == 1 {
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelSendBroadcastMsgVC = storyboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                channelSendBroadcastMsgVC.communityName = myChatsForPassNext.communityName!
                channelSendBroadcastMsgVC.communityKey = myChatsForPassNext.communityKey!
                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsForPassNext.communityImageSmallThumbUrl!
                channelSendBroadcastMsgVC.myChatsData = myChatsForPassNext
                viewOfPopUp.isHidden = true
                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
            }else{
                viewOfPopUp.isHidden = true
                popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.leaveChannelConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                        self.leaveChannel(myChatData:self.myChatsForPassNext)
                        
                    }, nil])
            }
            
        }else{
            navigationController?.setNavigationBarHidden(true, animated: false)
            myChatsForPassNext = myChatsList[indexPath.row]
            if myChatsForPassNext.ownerId == userId{
                checkAdmin = true
                heightOfViewOnPopUpView.constant = CGFloat(44 * arrOfMenusAdmin.count)
            }else{
                checkAdmin = false
                heightOfViewOnPopUpView.constant = CGFloat(44 * arrOfMenusFollower.count)
            }
            tblOfMenuOption.reloadData()
            viewOfPopUp.isHidden = false
        }*/
        
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let channelProfileVC = storyboard.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
        channelProfileVC.myChatsData = myChatsList[indexPath.row]
        // channelProfileVC.dominantColour = myChatsForPassNext.communityDominantColour!
        viewOfPopUp.isHidden = true
        self.navigationController?.pushViewController(channelProfileVC, animated: true)
    }
    
    
    func leaveChannel(myChatData:MyChatsTable){
        
        let params = LeaveChannelRequest.convertToDictionary(leaveChannelDetails: LeaveChannelRequest.init(operation:StringConstants.channelProfile.leaveChannelOperation , communityJabberId: myChatData.communityJabberId!, new_admin_user_id: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.leaveCommunity1 + myChatData.communityKey! + URLConstant.leaveCommunity2 + userId
        _ = LeaveChannelServices().leaveChannelServices(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! LeaveChannelResponse
                                                            print("model",model)
                                                            CommunityDetailService.sharedInstance.updateisMemberFromComminityDetailsTbl(isMemberValue: StringConstants.ZERO, communityKey: myChatData.communityKey!)
                                                            let currentTime = DateTimeHelper.getCurrentMillis()
                                                            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: StringConstants.DashboardViewController.unfollowStatus, latestMessageTime:String(currentTime), communityKey:myChatData.communityKey!,mediaType:StringConstants.EMPTY,messageType:StringConstants.MessageType.banner, lastMessageId: "")
                                                            MyChatServices.sharedInstance.updateisMemberFromMyChatTbl(isMemberValue: StringConstants.ZERO, communityKey: myChatData.communityKey!)
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            
                                                            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                            if let nav = self.navigationController {
                                                                for controller in nav.viewControllers as Array {
                                                                    if controller is DashboardViewController {
                                                                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                                        break
                                                                    }
                                                                }
                                                                nav.pushViewController(vc, animated: false)
                                                                return
                                                            }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOfFollowChannelList {
            return 70
        }
        return 44
    }
}
