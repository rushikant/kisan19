//
//  BlockMembersListVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 01/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class BlockMembersListVC: UIViewController {

    @IBOutlet weak var lblOfRecordNotFound: UILabel!
    @IBOutlet weak var tblBlockMembersList: UITableView!
    @IBOutlet weak var heightOflblNoRecordFound: NSLayoutConstraint!
    var myChatsData = MyChatsTable()
    var membersList = [MembersTable]()
    
    //For Pagination
    var isDataLoading:Bool=false
    var page_number : Int = 1
    var page_size : Int = 10
    var fetchOffSet :Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        
       /* self.membersList = MembersServices.sharedInstance.getBlockedMembersList(communityKey:myChatsData.communityKey!)
        if self.membersList.isEmpty{
            SVProgressHUD.show()
            self.callFetchBlockedCommunityMembersList(page_number:String(page_number),page_size:String(page_size))
        }else{
            if self.membersList.count > 0 {
                lblOfRecordNotFound.isHidden = true
                heightOflblNoRecordFound.constant = 0
                tblBlockMembersList.reloadData()
            } else {
                lblOfRecordNotFound.isHidden = false
                heightOflblNoRecordFound.constant = 30
            }
        }*/
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.callFetchBlockedCommunityMembersList(page_number:String(page_number),page_size:String(page_size))
        }else{
            self.membersList = MembersServices.sharedInstance.getBlockedMembersList(communityKey:myChatsData.communityKey!)
            if self.membersList.count > 0 {
                lblOfRecordNotFound.isHidden = true
                heightOflblNoRecordFound.constant = 0
                tblBlockMembersList.reloadData()
            } else {
                lblOfRecordNotFound.isHidden = false
                heightOflblNoRecordFound.constant = 30
                lblOfRecordNotFound.text = (NSLocalizedString(StringConstants.AlertMessage.noBlockedFollower, comment: StringConstants.EMPTY))
            }
        }
        
    }
 
    func callFetchBlockedCommunityMembersList(page_number:String,page_size:String) {
        let params = ["communityKey":myChatsData.communityKey!,"isBlocked":"t"]
        print("params",params)
        let currentTime = DateTimeHelper.getCurrentMillis()
        //let apiURL = URLConstant.BASE_URL + URLConstant.getFollowerList1 + myChatsData.communityKey! + URLConstant.getFollowerList2 + URLConstant.getBlockFollowerList + String(currentTime)
        let apiURL = URLConstant.BASE_URL + URLConstant.getFollowerList1 + myChatsData.communityKey! + URLConstant.getFollowerList2 + URLConstant.getFollowerList3 + page_size + URLConstant.getFollowerList4 + page_number + URLConstant.getBlockFollowerList + String(currentTime)
        
        _ = GetCommunityMembersListServices().getCommunityMembersList(apiURL,
                                                                      postData: params as [String : AnyObject],
                                                                      withSuccessHandler: { (userModel) in
                                                                        
                                                                        let model = userModel as! CommunityMembersListResponse
                                                                        print("model",model)
                                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                                        
                                                                        if self.membersList.count == 0 {
                                                                            self.membersList = MembersServices.sharedInstance.getBlockedMembersList(communityKey:self.myChatsData.communityKey!)
                                                                        }else{
                                                                            //Local Pagination
                                                                            
                                                                            /*// Refer this logic from DiscoverChannelViewController. but not used in also DiscoverChannelViewController screen.
                                                                             let communityListLastVal = self.communityListTemp.last
                                                                             let tempList = CommunityDetailService.sharedInstance.fetchChannelAfterSentTimeStamp(communityCreatedTimestamp:(communityListLastVal?.communityCreatedTimestamp!)!)*/
                                                                            
                                                                            self.fetchOffSet = self.fetchOffSet + 10;
                                                                            let tempList = MembersServices.sharedInstance.getMembersDetailsRecord(self.fetchOffSet,communityKey:self.myChatsData.communityKey!,isBlocked:"t")
                                                                            for i in 0 ..< tempList.count {
                                                                                let list = tempList[i]
                                                                                if !self.membersList.contains(list){
                                                                                    self.membersList.append(list)
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        if self.membersList.count > 0 {
                                                                            self.lblOfRecordNotFound.isHidden = true
                                                                            self.heightOflblNoRecordFound.constant = 0
                                                                            self.tblBlockMembersList.reloadData()
                                                                        } else {
                                                                            self.lblOfRecordNotFound.isHidden = false
                                                                            self.lblOfRecordNotFound.text = (NSLocalizedString(StringConstants.AlertMessage.noBlockedFollower, comment: StringConstants.EMPTY))
                                                                            self.heightOflblNoRecordFound.constant = 30
                                                                        }
                                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func initialDesign()   {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.blockedMembers, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        tblBlockMembersList.separatorColor = UIColor.clear
        
        tblBlockMembersList.delegate=self //To enable scrollviewdelegate
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension BlockMembersListVC : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.membersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockMembersListCell", for: indexPath) as! BlockMembersListCell
        cell.selectionStyle = .none
        let membersList = self.membersList[indexPath.row]
        var name = membersList.firstName!
        name = name + StringConstants.singleSpace + membersList.lastName!
        cell.lblBlockMemName.text = name
        cell.imageViewOfBlockMemProfile.layer.cornerRadius = cell.imageViewOfBlockMemProfile.frame.size.width / 2
        cell.imageViewOfBlockMemProfile.clipsToBounds = true
        cell.imageViewOfBlockMemProfile.sd_setImage(with: URL(string: membersList.imageUrl!), placeholderImage: UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg), options: .refreshCached)
        
        cell.btnUnBlock.tag = indexPath.row + 10
        cell.btnUnBlock.addTarget(self,action:#selector(btnUnBlockClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnUnBlockClicked(sender:UIButton){
        let point = tblBlockMembersList.convert(sender.center, from: sender.superview)
        let indexPath = tblBlockMembersList.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        DispatchQueue.main.async {
            self.popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.unBlockMembersConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.unBockMembers(indexPathValue:indexPath!)
                }, nil])
        }
    }
    
    func unBockMembers(indexPathValue: IndexPath){
        let membersData = membersList[(indexPathValue.row)]
        let params = BlockUnBlockChannelRequest.convertToDictionary(blockUnBlockChannelRequest:BlockUnBlockChannelRequest.init(operation: StringConstants.channelProfile.unblock, communityJabberId: myChatsData.communityJabberId!, newAdminUserId: StringConstants.EMPTY))
        let apiURL = URLConstant.BASE_URL + URLConstant.blockUnBlock1 + myChatsData.communityKey! + URLConstant.blockUnBlock2 + membersData.userId!
        _ = BolckUnBlockChannelServices().bolckUnBlockChannelServices(apiURL,
                                                                      postData: params as [String : AnyObject],
                                                                      withSuccessHandler: { (userModel) in
                                                                        let model = userModel as! BlockUnBlockMemberResponse
                                                                        print("model",model)
                                                                        MembersServices.sharedInstance.changeStatusOfBlockAndUnBlock(userId:membersData.userId!,isBlocked:"f")
                                                                        self.membersList = MembersServices.sharedInstance.getBlockedMembersList(communityKey:self.myChatsData.communityKey!)
                                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                                        self.tblBlockMembersList.reloadData()
                                                                        
                                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 65
    }
}


extension  BlockMembersListVC :UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    //Pagination
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>){
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let endScrolling: Float = Float(scrollView.contentOffset.y + scrollView.frame.size.height)
            var scrollContentSize = Float(scrollView.contentSize.height)
            scrollContentSize = (scrollContentSize / 100) * 50
            if (scrollView is UITableView) {
                if velocity.y > 0 {
                    if endScrolling >= scrollContentSize {
                        if !isDataLoading{
                            isDataLoading = true
                            self.page_number = self.page_number + 1
                            self.page_size =  10
                            self.callFetchBlockedCommunityMembersList(page_number:String(page_number),page_size:String(page_size))
                        }
                    }
                }
            }
        }else{
            //Local Pagination
            fetchOffSet = fetchOffSet + 10;
            let tempList = MembersServices.sharedInstance.getMembersDetailsRecord(fetchOffSet,communityKey:self.myChatsData.communityKey!,isBlocked:"t")
            for i in 0 ..< tempList.count {
                let list = tempList[i]
                if !self.membersList.contains(list){
                    self.membersList.append(list)
                }
            }
            if self.membersList.count > 0 {
                self.lblOfRecordNotFound.isHidden = true
                self.heightOflblNoRecordFound.constant = 0
                self.tblBlockMembersList.reloadData()
            } else {
                self.lblOfRecordNotFound.isHidden = false
                 self.lblOfRecordNotFound.text = (NSLocalizedString(StringConstants.AlertMessage.noBlockedFollower, comment: StringConstants.EMPTY))
                self.heightOflblNoRecordFound.constant = 30
            }
        }
    }
}
