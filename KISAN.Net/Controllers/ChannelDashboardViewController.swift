//
//  ChannelDashboardViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 17/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import XMPPFramework
import MBProgressHUD

class ChannelDashboardViewController: UIViewController {
    
    @IBOutlet weak var tblFollowerList: UITableView!
    var refreshControl = UIRefreshControl()
    var ownerId = String()
    @IBOutlet weak var lblOfChannelActivityText: UILabel!
    var messageAt: String?
    var userId: String?
    var dominantColour = String()
    var arrOfUserName = [String]()
    var myChatsData = MyChatsTable()
    var myChatsList = [MyChatsTable]()
    var appDelegate = AppDelegate()
    var page_number : Int = 1
    var page_size : Int = 10
    let fetOldMessageGroup = DispatchGroup()
    var overlayView: ChannelBroadcastOverlay!
    var isFromcreateChannel = false
    var buttonIsSelected = false

    @IBOutlet var widthConstraintForMediaImg: NSLayoutConstraint!
    //@IBOutlet var widthconstraintImageiconFormeassage: NSLayoutConstraint!
    @IBOutlet weak var btnChannelBroadcastMsg: UIButton!
    @IBOutlet var BtnBroadcastMessage: UIButton!
    @IBOutlet var BtnBroadcastPost: UIButton!
    @IBOutlet var LblPostToolTip: UILabel!
    @IBOutlet var lblMessageToolTip: UILabel!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblFollowerList.isHidden = true
        initialDesign()
        if isFromcreateChannel {
            self.initializeOverlayView()
        }
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        //Push Notification handle
        //Register to receive notification
        let notificationName = Notification.Name(StringConstants.pushNotificationConstants.joinPushNotificationCenter)
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelDashboardViewController.methodOfReceivedNotification), name: notificationName, object: nil)
        
        //refreshcontrol
        /*refreshControl.attributedTitle = NSAttributedString(string: StringConstants.EMPTY)
        refreshControl.addTarget(self, action: #selector(refreshMychat), for: .valueChanged)
        refreshControl.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        tblFollowerList.addSubview(refreshControl)*/
        
        LblPostToolTip.text = (NSLocalizedString(StringConstants.CreatePostViewController.post, comment: StringConstants.EMPTY))
        lblMessageToolTip.text = (NSLocalizedString(StringConstants.CreatePostViewController.Message, comment: StringConstants.EMPTY))
        
        //Get data from local db
        self.myChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: self.myChatsData.communityKey!)

        //If local db is empty then call ws
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.getOneToOneMyChatList(communityKey:self.myChatsData.communityKey!)
        }else{
            if  self.myChatsList.isEmpty{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.getOneToOneMyChatList(communityKey:self.myChatsData.communityKey!)
            }else{
                self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
                self.tblFollowerList.isHidden = false
                self.tblFollowerList.reloadData()
            }
        }
    }
    
    @objc func refreshMychat() {
       let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
       if checkInternetConnection == true{
        self.getOneToOneMyChatList(communityKey:self.myChatsData.communityKey!)
       }else{
        self.refreshControl.endRefreshing()
        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
        }
    }
    
    @objc func methodOfReceivedNotification() {
        self.myChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: self.myChatsData.communityKey!)
        self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
        if 0 < self.myChatsList.count{
            self.tblFollowerList.isHidden = false
        }else{
            self.tblFollowerList.isHidden = true
        }
        tblFollowerList.reloadData()
    }
    
    func initializeOverlayView()  {
      
        self.lblOfChannelActivityText.isHidden = true
        self.overlayView = ChannelBroadcastOverlay.loadNib()
        self.view.addSubview(self.overlayView)
        self.overlayView.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFromcreateChannel {
            let height = 185.0
            let width  = 240.0
            overlayView.frame = CGRect.init(x: (Double(view.frame.size.width/2.0) - (width/2)), y: (Double(view.frame.origin.y + view.frame.size.height) - (height + 100)), width: 240.0, height: 185.0)
        }
        self.view.layoutIfNeeded()
    }
    
    func getOneToOneMyChatList(communityKey:String){
        let params = ["":""]
        print("params",params)
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOneToOneMyChatList + communityKey + URLConstant.getOneToOneMyChatList2 + String(currentTime)
        _ = OneToOneMyChatListService().getOneToOneMyChatList(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! MyChatListResponse
                                                                print("model",model)
                                                                self.myChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: self.myChatsData.communityKey!)
                                                                self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
                                                                //self.myChatsList.sort(by:{$0.last_activity_datetime! < $1.last_activity_datetime!} )
                                                                DispatchQueue.main.async {
                                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                                    if 0 < self.myChatsList.count{
                                                                        self.tblFollowerList.isHidden = false
                                                                    }else{
                                                                        self.tblFollowerList.isHidden = true
                                                                    }
                                                                    self.tblFollowerList.reloadData()
                                                                }
                                                                self.refreshControl.endRefreshing()
                                                                for i in 0 ..< self.myChatsList.count {
                                                                    let mychatData = self.myChatsList[i]
                                                                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                                                                    spinnerActivity.isUserInteractionEnabled = false
                                                                    self.fetOldMessageGroup.enter()
                                                                    self.getOldMessage(oneToOneCommunityKey: mychatData.communityKey!)
                                                                }
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    func getOldMessage(oneToOneCommunityKey:String){
        
        let params = ["communityKey":oneToOneCommunityKey]
        var simpleComunityKey = oneToOneCommunityKey
        if let dotRange = simpleComunityKey.range(of: "_") {
            simpleComunityKey.removeSubrange(dotRange.lowerBound..<simpleComunityKey.endIndex)
        }
        var apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + simpleComunityKey + URLConstant.getOldMessages2 + String(self.page_size)
        apiURL = apiURL + URLConstant.getOldMessages3 + String(self.page_number) + URLConstant.getOldMessages4 + oneToOneCommunityKey
        print("apiURL",apiURL)
        
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            self.fetOldMessageGroup.leave()
                                                            DispatchQueue.main.async {
                                                                 MBProgressHUD.hide(for: self.view, animated: true);
                                                            }

        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        self.setNavigationBarItem()
        
        //broadcast message Button
        self.BtnBroadcastPost.isHidden = true
        self.BtnBroadcastMessage.isHidden = true
        self.LblPostToolTip.isHidden = true
        self.lblMessageToolTip.isHidden = true
//        btnChannelBroadcastMsg.setBackgroundImage(UIImage(named: StringConstants.ImageNames.yellow_bubble_circle), for: .normal)
        buttonIsSelected = false
        // For open right drawer (Hide notification otion for phase 1)
        // self.setRightDrawerForNotification()
        
        //Get data from local db
       /* self.myChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: self.myChatsData.communityKey!)
        //If local db is empty then call ws
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.getOneToOneMyChatList(communityKey:self.myChatsData.communityKey!)
        }else{
            if  self.myChatsList.isEmpty{
             let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
             spinnerActivity.isUserInteractionEnabled = false
             self.getOneToOneMyChatList(communityKey:self.myChatsData.communityKey!)
            }else{
                 self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
                self.tblFollowerList.isHidden = false
                self.tblFollowerList.reloadData()
            }
        }*/
        
        if !self.myChatsList.isEmpty {
            self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
        }
        // userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: "username")
        if let communityDominantColour = myChatsData.communityDominantColour{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }
    
    }
    
    func initialDesign() {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
       
        //removed notification list option for phase 1
        //navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.notificationBell), style: .plain, target: self, action: #selector(btnNotificationDrawer))
        //navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem?.tintColor = .clear
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        tblFollowerList.tableFooterView = UIView()
        let btnNavTitleClick =  UIButton(type: .custom)
        btnNavTitleClick.frame = CGRect(x: 0, y: 0, width:view.frame.width - 32, height: view.frame.height)
        btnNavTitleClick.backgroundColor = UIColor.clear
        btnNavTitleClick.titleLabel?.font =  UIFont.boldSystemFont(ofSize: 16.0)
        btnNavTitleClick.setTitle(StringUTFEncoding.UTFEncong(string:myChatsData.communityName!), for: .normal)
        btnNavTitleClick.addTarget(self, action: #selector(self.btnNavTitleClick), for: .touchUpInside)
        btnNavTitleClick.titleLabel?.textAlignment = .left
        btnNavTitleClick.contentHorizontalAlignment = .left
        self.navigationItem.titleView = btnNavTitleClick
        
        // Set flag for open notifiation drawer from right drawer
        let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.channelDashboardViewController]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
    }
    
    @objc func btnNavTitleClick(button: UIButton) {
        print("btnNavTitleClick")
        let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
        channelProfileVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelProfileVC, animated: true)
    }
    
    
    @objc func btnNotificationDrawer(sender: UIBarButtonItem) {
        // For open right drawer
        self.setRightDrawerForNotification()
    }
    
    func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChannelBroadcastMsgClick(_ sender: Any) {
//        buttonIsSelected = !buttonIsSelected
//        if buttonIsSelected {
//            self.BtnBroadcastPost.isHidden = false
//            self.BtnBroadcastMessage.isHidden = false
//            self.LblPostToolTip.isHidden = false
//            self.lblMessageToolTip.isHidden = false
//            btnChannelBroadcastMsg.setBackgroundImage(UIImage(named: "Close"), for: .normal)
//        }else{
//            self.BtnBroadcastPost.isHidden = true
//            self.BtnBroadcastMessage.isHidden = true
//            self.LblPostToolTip.isHidden = true
//            self.lblMessageToolTip.isHidden = true
//            btnChannelBroadcastMsg.setBackgroundImage(UIImage(named: StringConstants.ImageNames.yellow_bubble_circle), for: .normal)
//        }
        if isFromcreateChannel {
            isFromcreateChannel = false
            self.overlayView.isHidden = true
            self.lblOfChannelActivityText.isHidden = false
        }
        let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
        channelSendBroadcastMsgVC.ownerId = ownerId
        //channelSendBroadcastMsgVC.dominantColour = self.myChatsData.communityDominantColour!
        channelSendBroadcastMsgVC.ownerId = myChatsData.ownerId!
        channelSendBroadcastMsgVC.communityName = myChatsData.communityName!
        channelSendBroadcastMsgVC.communityKey = myChatsData.communityKey!
        channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsData.communityImageSmallThumbUrl!
        channelSendBroadcastMsgVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
    }

    @IBAction func btnPostMessageClicked(_ sender: Any) {
        let createpostVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
        createpostVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(createpostVC, animated: true)
    }
    
    @IBAction func btnMessageBroadcastClick(_ sender: Any) {
        if isFromcreateChannel {
            isFromcreateChannel = false
            self.overlayView.isHidden = true
            self.lblOfChannelActivityText.isHidden = false
        }
        let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
        channelSendBroadcastMsgVC.ownerId = ownerId
        //channelSendBroadcastMsgVC.dominantColour = self.myChatsData.communityDominantColour!
        channelSendBroadcastMsgVC.ownerId = myChatsData.ownerId!
        channelSendBroadcastMsgVC.communityName = myChatsData.communityName!
        channelSendBroadcastMsgVC.communityKey = myChatsData.communityKey!
        channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsData.communityImageSmallThumbUrl!
        channelSendBroadcastMsgVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ChannelDashboardViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myChatsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelDashboardCell", for: indexPath) as! ChannelDashboardCell
        cell.selectionStyle = .none
        let myChats = myChatsList[indexPath.row]
        cell.imageViewOfFollower.layer.cornerRadius = cell.imageViewOfFollower.frame.size.width / 2
        cell.imageViewOfFollower.clipsToBounds = true
        let name = myChats.communityName
//        if let lastName = myChats.lastName{
//            name = name! + StringConstants.singleSpace + lastName
//        }
        cell.lblNameOfFollower.text = StringUTFEncoding.UTFEncong(string: name!)
        
        if let communityImageBigThumbUrl = myChats.communityImageBigThumbUrl{
            let imgurl = communityImageBigThumbUrl
            let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            cell.imageViewOfFollower.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
        }else{
            cell.imageViewOfFollower.image = UIImage(named:StringConstants.ImageNames.communityPlaceholderImg)
        }
        
        cell.lblTextMessage?.text = StringConstants.EMPTY
        if let messageText: String = myChats.messageText {
            if myChats.ownerId == userId {
                cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
            }else{
                cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
            }
        }
        
        if (myChats.messageAt != nil) && !(myChats.messageAt?.isEmpty)! {
            let time = DateTimeHelper.getTimeAgo(time: myChats.messageAt!)
            cell.lblOfDateTime.isHidden = false
            cell.lblOfDateTime?.text = time
        }else{
            cell.lblOfDateTime.isHidden = true
        }
        
        cell.lblTextMessage.isHidden = false
        cell.imageOfShowMessageTypeLatestMsg.isHidden = true
        if myChats.mediaType == StringConstants.MediaType.image{
            cell.lblTextMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.image, comment: "")
            cell.imageOfShowMessageTypeLatestMsg.isHidden = false
            cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.ic_camera_dark_gray_16dp)
            cell.leadingOflblLatestMsg.constant = 30
            cell.widtContraintForMediaImg.constant = 18
            cell.heightconstraintforMediaimg.constant = 16
        }else if myChats.mediaType == StringConstants.MediaType.video{
            cell.lblTextMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.video, comment: "")
            cell.imageOfShowMessageTypeLatestMsg.isHidden = false
            cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.videoImage)
            cell.leadingOflblLatestMsg.constant = 30
            cell.widtContraintForMediaImg.constant = 20
            cell.heightconstraintforMediaimg.constant = 12
        }else if myChats.mediaType == StringConstants.MediaType.audio{
            cell.lblTextMessage?.text = NSLocalizedString( StringConstants.DashboardViewController.audio, comment: "")
            cell.imageOfShowMessageTypeLatestMsg.isHidden = false
            cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.headphone_16px)
            cell.leadingOflblLatestMsg.constant = 30
            cell.widtContraintForMediaImg.constant = 18
            cell.heightconstraintforMediaimg.constant = 18
        }else if myChats.messageType == StringConstants.MessageType.mapLocation{
            cell.lblTextMessage?.text = NSLocalizedString(  StringConstants.DashboardViewController.location, comment: "")  
            cell.imageOfShowMessageTypeLatestMsg.isHidden = false
            cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.locationDash)
            cell.leadingOflblLatestMsg.constant = 30
            cell.widtContraintForMediaImg.constant = 17
            cell.heightconstraintforMediaimg.constant = 20
            //widthconstraintImageiconFormeassage.constant = 17
            updateViewConstraints()
        }else if myChats.messageType == StringConstants.MessageType.chatMessage && myChats.mediaType  == StringConstants.EMPTY {
            if let messageText: String = myChats.messageText {
                print("messageText1",messageText)
                cell.imageOfShowMessageTypeLatestMsg.isHidden = true
                if myChats.ownerId == userId {
                    cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
                }else{
                    //                        if let messageBy = myChats.messageBy{
                    //                            cell.lblLatestMessage?.text = messageBy + StringConstants.singleSpace + StringUTFEncoding.UTFEncong(string:messageText)
                    //                        }else{
                    cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
                    //                        }
                }
                cell.leadingOflblLatestMsg.constant = 7
            }
        }else{
            if let messageText: String = myChats.messageText {
                cell.imageOfShowMessageTypeLatestMsg.isHidden = true
                if myChats.ownerId == userId {
                    cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
                }else{
                    //                        if let messageBy = myChats.messageBy{
                    //                            cell.lblLatestMessage?.text = messageBy + StringConstants.singleSpace + StringUTFEncoding.UTFEncong(string:messageText)
                    //                        }else{
                    cell.lblTextMessage?.text = StringUTFEncoding.UTFEncong(string:messageText)
                    //                       }
                }
                cell.leadingOflblLatestMsg.constant = 7
            }
        }
        
        return cell
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myChats = self.myChatsList[indexPath.row]
        let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
        oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: self.myChatsData.communityName!)
        oneToOneFromAdminVC.communityKey = myChats.communityKey!
        oneToOneFromAdminVC.communityImageSmallThumbUrl = self.myChatsData.communityImageSmallThumbUrl!
        oneToOneFromAdminVC.dominantColour = myChatsData.communityDominantColour!
        oneToOneFromAdminVC.ownerId = myChatsData.ownerId!
        oneToOneFromAdminVC.myChatsData = self.myChatsData
        oneToOneFromAdminVC.myChatsData = myChats //myChatsData
        //oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
        self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 77
    }
}

extension ChannelDashboardViewController: XMPPStreamDelegate {
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage){
        if message.childCount > 1{
            //comment getMyChat list code when notification is working
            self.myChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: self.myChatsData.communityKey!)
            self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
            tblFollowerList.reloadData()
        }
    }
}

extension ChannelDashboardViewController : XMPPPubSubDelegate{
    func xmppPubSub(_ sender: XMPPPubSub, didSubscribeToNode node: String, withResult iq: XMPPIQ) {
        print("node",node)
    }
    
    func xmppPubSub(_ sender: XMPPPubSub, didNotSubscribeToNode node: String, withError iq: XMPPIQ) {
        print("node",node)
    }
}


