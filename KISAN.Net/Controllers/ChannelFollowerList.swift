//
//  ChannelFollowerList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class ChannelFollowerList: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var viewBackOfPopUp: UIView!
    @IBOutlet weak var viewOfPopUp: UIView!
    @IBOutlet weak var tblOfPopUp: UITableView!
    @IBOutlet weak var tblFollowersList: UITableView!
    @IBOutlet var lblfollowers: UILabel!
    var userId = String()

    var arrOfPopUpList = [(NSLocalizedString(StringConstants.popUpMenuName.viewProfile, comment: StringConstants.EMPTY))
        ,(NSLocalizedString(StringConstants.popUpMenuName.removeFromChannle, comment: StringConstants.EMPTY))
        ,(NSLocalizedString(StringConstants.popUpMenuName.blockFollower, comment: StringConstants.EMPTY))]
    var longPressIndexPath:NSIndexPath?
    var myChatsData = MyChatsTable()
    var communityMembersList: [CommunityMembersList] = []
    var communityTempMembersList: [CommunityMembersList] = []
    var userIdValue = String()
    var membersList = [MembersTable]()
    var membersListTemp = [MembersTable]()
    
    //For Pagination
    var isDataLoading:Bool=false
    var page_number : Int = 1
    var page_size : Int = 10
    var fetchOffSet :Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        intialDesign()
        userIdValue = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        lblfollowers.text = (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
       /* self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:myChatsData.communityKey!)
        self.membersListTemp = self.membersList
        
        if self.membersList.isEmpty{
            SVProgressHUD.show()
            self.callFetchCommunityMembersList()
        }else{
            self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
        }*/
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.callFetchCommunityMembersList(page_number:String(page_number),page_size:String(page_size))
        }else{
            self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:myChatsData.communityKey!)
            self.membersListTemp = self.membersList
            self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
        }

        //  Register to receive notification in your class (NotificationCenter from appdelegate of channel block and remove)
        NotificationCenter.default.addObserver(self, selector: #selector(self.operationFilter(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnChannelFolloweList), object: nil)
        
        //Get UserId
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)

        
    }
    
    @objc func operationFilter(_ notification: NSNotification) {
        let operation = notification.userInfo?["operation"] as! String
        self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:myChatsData.communityKey!)
        self.membersListTemp = self.membersList
        self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
    }
    
    
    func callFetchCommunityMembersList(page_number:String,page_size:String) {
       
        let params = ["communityKey":myChatsData.communityKey!,"isBlocked":"f"]
        print("params",params)
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getFollowerList1 + myChatsData.communityKey! + URLConstant.getFollowerList2 + URLConstant.getFollowerList3 + page_size + URLConstant.getFollowerList4 + page_number + URLConstant.getFollowerList5 + String(currentTime)
            
        _ = GetCommunityMembersListServices().getCommunityMembersList(apiURL,
                                                   postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                    let model = userModel as! CommunityMembersListResponse
                                                    print("model",model)
                                                    
                                                    
                                                    if self.membersListTemp.count == 0{
                                                        self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:self.myChatsData.communityKey!)
                                                        self.membersListTemp = self.membersList
                                                        self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
                                                    }else{
                                                        //Local Pagination
                                                       
                                                        /*// Refer this logic from DiscoverChannelViewController. but not used in also DiscoverChannelViewController screen.
                                                         let communityListLastVal = self.communityListTemp.last
                                                         let tempList = CommunityDetailService.sharedInstance.fetchChannelAfterSentTimeStamp(communityCreatedTimestamp:(communityListLastVal?.communityCreatedTimestamp!)!)*/
                                                        
                                                        self.fetchOffSet = self.fetchOffSet + 10;
                                                        let tempList = MembersServices.sharedInstance.getMembersDetailsRecord(self.fetchOffSet,communityKey:self.myChatsData.communityKey!,isBlocked:"f")
                                                        for i in 0 ..< tempList.count {
                                                            let list = tempList[i]
                                                            if !self.membersList.contains(list){
                                                                self.membersList.append(list)
                                                            }
                                                        }
                                                        self.membersListTemp = self.membersList
                                                        self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
                                                    }
                                                    
                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
           MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func handleFollowerList(membersList:[MembersTable],membersListTemp:[MembersTable]) {
        // Remove Admin from follow list
        if self.myChatsData.ownerId == self.userIdValue{
            let index =   self.membersList.index { (memberList) -> Bool in
                memberList.userId == self.userIdValue
            }
            if let inadexValue = index{
                self.membersList.remove(at: inadexValue)
            }
        }else{
            
            for i in 0 ..< self.membersListTemp.count {
                let members = self.membersListTemp[i]
                if members.userId == self.myChatsData.ownerId{
                    let index =   self.membersList.index { (memberList) -> Bool in
                        memberList.userId == self.myChatsData.ownerId
                    }
                    if let inadexValue = index{
                        self.membersList.remove(at: inadexValue)
                    }
                }
            }
        }
        
        MBProgressHUD.hide(for: self.view, animated: true);
        self.tblFollowersList.reloadData()
        
    }
    
   
    func intialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        
        
        if let communityDominantColour = myChatsData.communityDominantColour{
            var correctedCommunityDominantColour = String()
            if communityDominantColour.length < 7{
                if communityDominantColour.length == 6{
                    correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode
                }else{
                    correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode + StringConstants.DiscoverChannel.Colorcode
                }
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
            }
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }
        
    
        
        //tblFollowersList.showsVerticalScrollIndicator = false
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = self.myChatsData.communityName
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        tblFollowersList.separatorColor = UIColor.clear
        tblOfPopUp.separatorColor = UIColor.clear
        
        // long press recognizer
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        //self.tblFollowersList.addGestureRecognizer(longPressGesture)
        
        // For pop up hidden
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChannelFollowerList.hideViewBackOfPopUp))
        tap.cancelsTouchesInView = false
        self.viewBackOfPopUp.addGestureRecognizer(tap)
        
        tblFollowersList.delegate=self //To enable scrollviewdelegate
    }
    
    @objc func hideViewBackOfPopUp() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        viewBackOfPopUp.isHidden = true
    }
    
    @objc func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        if self.myChatsData.ownerId == self.userIdValue{
            let p = longPressGesture.location(in: self.tblFollowersList)
            let indexPath = self.tblFollowersList.indexPathForRow(at: p)
            navigationController?.setNavigationBarHidden(true, animated: true)
            viewBackOfPopUp.isHidden = false
            if indexPath == nil {
                print("Long press on table view, not row.")
            }
            else if (longPressGesture.state == UIGestureRecognizerState.began) {
                print("Long press on row, at \(indexPath!.row)")
                longPressIndexPath = indexPath! as NSIndexPath
                tblOfPopUp.reloadData()
            }
        }
    }
    
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension ChannelFollowerList : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblFollowersList{
            return membersList.count
        }else{
            return arrOfPopUpList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        if tableView == tblFollowersList{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelFollowerListCell", for: indexPath) as! ChannelFollowerListCell
            cell.btnInitializeOnetoOneChat.tag = indexPath.row
            cell.btnInitializeOnetoOneChat.isHidden = true
            /*if userId == myChatsData.ownerId{
                cell.btnInitializeOnetoOneChat.isHidden = false
                cell.btnInitializeOnetoOneChat.addTarget(self,action:#selector(btnInitializeOnetoOneChatClicked(sender:)), for: .touchUpInside)
            }else{
                cell.btnInitializeOnetoOneChat.isHidden = true
            }*/
            cell.selectionStyle = .none
            let membersList = self.membersList[indexPath.row]
            var name = membersList.firstName!
            name = name + StringConstants.singleSpace + membersList.lastName!
            cell.lblOfFollowerName.text = name
            cell.imageViewOfFollowerProfile.layer.cornerRadius = cell.imageViewOfFollowerProfile.frame.size.width / 2
            cell.imageViewOfFollowerProfile.clipsToBounds = true
            cell.imageViewOfFollowerProfile.sd_setImage(with: URL(string: membersList.imageUrl!), placeholderImage: UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg), options: .refreshCached)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpOnChannelFollowerListVC", for: indexPath) as! PopUpOnChannelFollowerListVC
            cell.selectionStyle = .none
            cell.lblOfText.text = arrOfPopUpList[indexPath.row]
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOfPopUp{
            navigationController?.setNavigationBarHidden(false, animated: false)
            viewBackOfPopUp.isHidden = true
            if indexPath.row == 0{
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let showUserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "ShowUserProfileVC") as! ShowUserProfileVC
                //showUserProfileVC.communityMembersData = [communityMembersList[(longPressIndexPath?.row)!]]
                showUserProfileVC.membersListData = membersList[indexPath.row]
                showUserProfileVC.commingFrom = StringConstants.flags.ChannelFollowerList
                self.navigationController?.pushViewController(showUserProfileVC, animated: true)
            }else if indexPath.row == 1 {
                DispatchQueue.main.async {
                    self.popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.removeMembersConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                        },{action2 in
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                            self.bockAndRemoveMembers(indexPathValue: self.longPressIndexPath! as IndexPath,operation:StringConstants.channelProfile.remove)
                        }, nil])
                }
            }else if indexPath.row == 2 {
                DispatchQueue.main.async {
                    self.popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.blockMembersConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                        },{action2 in
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                            self.bockAndRemoveMembers(indexPathValue: self.longPressIndexPath! as IndexPath,operation:StringConstants.channelProfile.block)
                        }, nil])
                }
            }
        }else{
            //            if self.myChatsData.ownerId == self.userIdValue{
            //                navigationController?.setNavigationBarHidden(true, animated: true)
            //                viewBackOfPopUp.isHidden = false
            //                longPressIndexPath = indexPath as NSIndexPath
            //
            //                tblOfPopUp.reloadData()
            //            }else{
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let showUserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "ShowUserProfileVC") as! ShowUserProfileVC
            // showUserProfileVC.communityMembersData = [communityMembersList[indexPath.row]]
            showUserProfileVC.membersListData = membersList[indexPath.row]
            showUserProfileVC.commingFrom = StringConstants.flags.ChannelFollowerList
            self.navigationController?.pushViewController(showUserProfileVC, animated: true)
            // }
            
        }
    }
    
    @objc func btnInitializeOnetoOneChatClicked(sender:UIButton) {
        let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
        SessionDetailsForCommunityType.shared.globalMyChatsData = myChatsData
        oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
        oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: self.myChatsData.communityName!)
        oneToOneFromAdminVC.communityKey = self.myChatsData.communityKey!
        oneToOneFromAdminVC.communityImageSmallThumbUrl = self.myChatsData.communityImageSmallThumbUrl!
        oneToOneFromAdminVC.dominantColour = myChatsData.communityDominantColour!
        oneToOneFromAdminVC.ComingFrom = StringConstants.flags.ChannelFollowerList
        let membersList = self.membersList[sender.tag]
        let userId = membersList.userId!
        oneToOneFromAdminVC.FollowerUserId = userId
        oneToOneFromAdminVC.ownerId = self.myChatsData.ownerId!
        let communityJavbberId = self.myChatsData.communityKey! + "_" + userId
        oneToOneFromAdminVC.myChatsData = self.myChatsData
        oneToOneFromAdminVC.communityJabberIDFromlistOfFollower = communityJavbberId
        //oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
        self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)
    }
    
    
    func bockAndRemoveMembers(indexPathValue: IndexPath,operation:String){
        let membersData = membersList[indexPathValue.row]
        let params = BlockUnBlockChannelRequest.convertToDictionary(blockUnBlockChannelRequest:BlockUnBlockChannelRequest.init(operation: operation, communityJabberId: myChatsData.communityJabberId!, newAdminUserId: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.blockUnBlock1 + myChatsData.communityKey! + URLConstant.blockUnBlock2 + membersData.userId!
        print("apiURL",apiURL)
        _ = BolckUnBlockChannelServices().bolckUnBlockChannelServices(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! BlockUnBlockMemberResponse
                                                            print("model",model)
                                                            if operation == StringConstants.AlertMessage.removeMembersConfirmation{
                                                               MembersServices.sharedInstance.deleteRemoveMemberFromChannel(userId:membersData.userId!)
                                                                self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:self.myChatsData.communityKey!)
                                                                self.membersListTemp = self.membersList
                                                                self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)

                                                            }else{
                                                           MembersServices.sharedInstance.changeStatusOfBlockAndUnBlock(userId:membersData.userId!,isBlocked:"t")
                                                                self.membersList = MembersServices.sharedInstance.getMembersDetails(communityKey:self.myChatsData.communityKey!)
                                                                self.membersListTemp = self.membersList
                                                                self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
                                                            }
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
         if tableView == tblFollowersList{
            return 65
         }else{
            return 44
        }
    }
}

extension  ChannelFollowerList :UIScrollViewDelegate {
   
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    //Pagination
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>){
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let endScrolling: Float = Float(scrollView.contentOffset.y + scrollView.frame.size.height)
            var scrollContentSize = Float(scrollView.contentSize.height)
            scrollContentSize = (scrollContentSize / 100) * 50
            if (scrollView is UITableView) {
                if velocity.y > 0 {
                    if endScrolling >= scrollContentSize {
                        if !isDataLoading{
                            isDataLoading = true
                            self.page_number = self.page_number + 1
                            self.page_size =  10
                            self.callFetchCommunityMembersList(page_number:String(page_number),page_size:String(page_size))
                        }
                    }
                }
            }
        }else{
            //Local Pagination
            fetchOffSet = fetchOffSet + 10;
            let tempList = MembersServices.sharedInstance.getMembersDetailsRecord(fetchOffSet,communityKey:self.myChatsData.communityKey!,isBlocked:"f")
            for i in 0 ..< tempList.count {
                let list = tempList[i]
                if !self.membersList.contains(list){
                    self.membersList.append(list)
                }
            }
            self.membersListTemp = self.membersList
            self.handleFollowerList(membersList: self.membersList, membersListTemp: self.membersListTemp)
        }
    }
}
