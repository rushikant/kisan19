//
//  ChannelProfileVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices
import SafariServices

class ChannelProfileVC: UIViewController,SFSafariViewControllerDelegate {
    
    @IBOutlet var lblofAdressVewSeperator: UILabel!
    @IBOutlet var topConstraintOfChannelFollower: NSLayoutConstraint!
    @IBOutlet var heightConstraintOfAdreessTitl: NSLayoutConstraint!
    @IBOutlet weak var viewOfBackProfilePic: UIView!
    @IBOutlet weak var imageViewOfCommunitiyProfile: UIImageView!
    @IBOutlet weak var lblOfCommunityName: UILabel!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var ViewOfBackbtnBroadCastVC: UIView!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblDescriprtion: UILabel!
    @IBOutlet weak var btnMoveToBroadCastVC: UIButton!
    var dominantColour = String()
    @IBOutlet weak var ViewOfBackTblMenuOption: UIView!
    @IBOutlet weak var tblMenuOption: UITableView!
    @IBOutlet weak var heightOfMenuOptionView: NSLayoutConstraint!
    @IBOutlet weak var viewOnScroll: UIView!
    var tapGesture = UITapGestureRecognizer()
    var tapGesture1 = UITapGestureRecognizer()
    //    var itemsMyChat = [CommunityDetails]()
    //    var communityKey = String()
    var userId = String()
    //    var itemsMyChatLocal = [MyChatsTable]()
    var dictOfCategories = [String: AnyObject?]()
    var arrOfCategories = NSMutableArray()
    var arrOfCategoryName = NSMutableArray()
    var valueOfCategories = String()
    var strCompleteAddress = String()
    var strOfCategoriesName = String()
    var arrOfCategoryDataForPassNextView = NSMutableArray()
    var items = [String]()
    var flag : Bool = false
    var myChatsData = MyChatsTable()
    var categories_list: [CategoriesList] = []
    @IBOutlet weak var imageViewOFBackBtnBroadCastClick: UIImageView!
    @IBOutlet weak var btnFollower: UIButton!
    var commingFrom = String()
    var CommynityKeyFromPushNoti = String()
    @IBOutlet weak var productServicesCollectionView: UICollectionView!
    @IBOutlet weak var viewOfBackChannelProfilePic: UIView!
    @IBOutlet weak var lblStatllNumber: UILabel!
    @IBOutlet weak var lblOfPavillionName: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblContactPersonDesignation: UILabel!
    @IBOutlet weak var contactPersonName: UILabel!
    @IBOutlet weak var lblOfAddress: UILabel!
    @IBOutlet weak var imageViewOfPavillion: UIImageView!
    @IBOutlet weak var lblOfEventName: UILabel!
    @IBOutlet weak var bottomOfCollectionViewProductAndServices: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfAddress: UIImageView!
    @IBOutlet weak var imageViewOfWebLink: UIImageView!
    @IBOutlet weak var lblOfProductServices: UILabel!
    @IBOutlet weak var lblOfAddressTitle: UILabel!
    
    var arrOfProductService: [ProductMedias] = []
    var dictOfCommunityLocation: [String:String] = [:]

    @IBOutlet weak var heightOfProductServicesCollectionView: NSLayoutConstraint!
    @IBOutlet weak var heightOfViewOfBackAddress: NSLayoutConstraint!
    @IBOutlet weak var heightOfViewProductServices: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfLabelAddress: NSLayoutConstraint!
    @IBOutlet weak var topOfLableAdreess: NSLayoutConstraint!
    @IBOutlet weak var heightOfLabelName: NSLayoutConstraint!
    
    @IBOutlet weak var topOfLblDesignation: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfLableDesignation: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfImageViewOfAddress: NSLayoutConstraint!
    @IBOutlet weak var heightOfImageViewOfName: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfImageViewOfWebLink: NSLayoutConstraint!
    @IBOutlet weak var heightOfLableWebLink: NSLayoutConstraint!
    @IBOutlet weak var topOfLabelWebLink: NSLayoutConstraint!
    @IBOutlet weak var topOfEventCode: NSLayoutConstraint!
    @IBOutlet weak var topOfProductServices: NSLayoutConstraint!
    @IBOutlet weak var heightOfProductServices: NSLayoutConstraint!
    @IBOutlet weak var lblOfProductServicesSeprator: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //callGetUserAreaInterestAPI()
        btnFollower.setTitle((NSLocalizedString(StringConstants.channelProfile.allFollower, comment: StringConstants.EMPTY)), for: .normal)
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        self.initialDesign()

        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if commingFrom == StringConstants.PushNotificationAction.Followchannel{
            if checkInternetConnection == true{
                self.getProfileData()
            }else{
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
            }
        }else{
            if checkInternetConnection == true{
                if commingFrom == StringConstants.pushNotificationConstants.channelProfileFrmNotification{
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.getProfileData()
                }else{
                    //self.setProfileData()
                    //callGetUserAreaInterestAPI()
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.getProfileData()
                }
            }else{
                let communityData = CommunityDetailService.sharedInstance.checkMyChatCommunityKey(communityKey: myChatsData.communityKey!)
                if communityData.count == 0{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                }else{
                    let  dict  = ConvertToDicCommunityDetailsObject.convertToDictionary(communityDetail: communityData)
                    let myChatConvertedData = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict)
                    self.myChatsData  = myChatConvertedData
                    self.setProfileData()
                    callGetUserAreaInterestAPI()
                }
            }
        }
    }
    
    func getProfileData(){
        let params = ["":""]
        let currentTime = DateTimeHelper.getCurrentMillis()
        print("params",params)
        var apiURL = String()
        if commingFrom == StringConstants.PushNotificationAction.Followchannel{
          apiURL = URLConstant.BASE_URL + URLConstant.getChannelProfile + CommynityKeyFromPushNoti + URLConstant.getChannelProfileTimeStamp + String(currentTime)
        }else{
        apiURL = URLConstant.BASE_URL + URLConstant.getChannelProfile + myChatsData.communityKey! + URLConstant.getChannelProfileTimeStamp + String(currentTime)
        }
        print("apiURL",apiURL)
        _ = ChannelProfileService().getChannelProfile(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let communityDetails = userModel as! CommunityDetails
                                                                
                                                                self.arrOfProductService = communityDetails.productMedias
                                                                self.dictOfCommunityLocation = communityDetails.location

                                                                let  dict  = ConvertToDictOfChannelProfileObj.convertToDictOfChannelProfileObj(myChatDetails: [communityDetails])
                                                                let  myChatListRespo = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict)
                                                                
                                                                self.myChatsData = myChatListRespo
                                                                
                                                                if !(self.myChatsData.logged_user_blocked_timestamp == StringConstants.EMPTY){
                                                                    self.ViewOfBackbtnBroadCastVC.isHidden = true
                                                                }else{
                                                                    self.ViewOfBackbtnBroadCastVC.isHidden = false
                                                                }
                                                                if self.myChatsData.isMember! == StringConstants.ONE{
                                                                    self.imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.plain_white_chat)
                                                                }else{
                                                                    self.imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.ic_arrow_forward_black_24dp)
                                                                }
                                                                
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                                self.setProfileData()
                                                                self.callGetUserAreaInterestAPI()
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))
], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    func setProfileData() {
        if commingFrom == StringConstants.PushNotificationAction.Followchannel{
            //to update the channel profile basic data when comes from notification
            self.showInitialDesign(StrComingWithmychatData: true)
        }else{
        }
        self.lblOfCommunityName.text = self.myChatsData.communityName
        if let followerCount = self.myChatsData.memberCount {
            var totalMembers = Int(followerCount)
            if totalMembers! != 0{
                totalMembers = totalMembers! - 1
                if totalMembers == 0 {
                  //  self.lblFollowersCount.text = (NSLocalizedString(StringConstants.no, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                       self.lblFollowersCount.text =  (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
                }else{
                    self.lblFollowersCount.text =  String(describing: totalMembers!) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                }
            }else{
               // self.lblFollowersCount.text = (NSLocalizedString(StringConstants.no, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                 self.lblFollowersCount.text =  (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
            }
        }else{
            //self.lblFollowersCount.text = (NSLocalizedString(StringConstants.no, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
             self.lblFollowersCount.text =  (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
        }
        
        if let communityDesc = self.myChatsData.communityDesc{
            if communityDesc == StringConstants.EMPTY{
                self.lblDescriprtion.text = (NSLocalizedString(StringConstants.placeHolder.noCommunityDesc, comment: StringConstants.EMPTY))

            }else{
                self.lblDescriprtion.text = communityDesc
            }
        }else{
           self.lblDescriprtion.text = (NSLocalizedString(StringConstants.placeHolder.noCommunityDesc, comment: StringConstants.EMPTY))

        }
        
        if arrOfProductService.count > 0 {
            heightOfProductServicesCollectionView.constant = 128
           if let pavilion_name = myChatsData.pavilion_name,let alloted_stall_number = myChatsData.alloted_stall_number {
                imageViewOfPavillion.isHidden = false
                self.bottomOfCollectionViewProductAndServices.constant = 95
                if alloted_stall_number.isEmpty {
                    lblStatllNumber.isHidden = true
                }else{
                    lblStatllNumber.isHidden = false
                    lblStatllNumber.text = (NSLocalizedString(StringConstants.channelProfile.stallNo, comment: StringConstants.EMPTY)) + alloted_stall_number
                }
                lblOfPavillionName.text = pavilion_name
            }else{
                lblStatllNumber.text = StringConstants.EMPTY
                lblOfPavillionName.text = StringConstants.EMPTY
                imageViewOfPavillion.isHidden = true
                self.bottomOfCollectionViewProductAndServices.constant = 18
            }
            //Comment this code for remove stall number and stall name
            lblStatllNumber.text = StringConstants.EMPTY
            lblOfPavillionName.text = StringConstants.EMPTY
            imageViewOfPavillion.isHidden = true
            lblOfProductServicesSeprator.isHidden = false
            self.bottomOfCollectionViewProductAndServices.constant = 18
            
        }else{
            heightOfProductServicesCollectionView.constant = 0
            topOfEventCode.constant = 0 //10
            topOfProductServices.constant = 0
            heightOfProductServices.constant = 0
            if let pavilion_name = myChatsData.pavilion_name,let alloted_stall_number = myChatsData.alloted_stall_number {

                if pavilion_name.isEmpty && alloted_stall_number.isEmpty{
                    
                    heightOfViewProductServices.priority = UILayoutPriority(rawValue: 999)
                    heightOfViewProductServices.isActive = true
                    heightOfViewProductServices.constant = 0
                    self.view.layoutIfNeeded()
                }else{
                    imageViewOfPavillion.isHidden = false
                    self.bottomOfCollectionViewProductAndServices.constant = 95
                    if alloted_stall_number.isEmpty {
                        lblStatllNumber.isHidden = true
                    }else{
                        lblStatllNumber.isHidden = false
                        lblStatllNumber.text = (NSLocalizedString(StringConstants.channelProfile.stallNo, comment: StringConstants.EMPTY)) + alloted_stall_number
                    }
                    lblOfPavillionName.text = pavilion_name
                }
                
                
            }else{
                heightOfViewProductServices.priority = UILayoutPriority(rawValue: 999)
                heightOfViewProductServices.constant = 0
                self.view.layoutIfNeeded()
            }
         
            //Comment this code for remove stall number and stall name
            lblStatllNumber.text = StringConstants.EMPTY
            lblOfPavillionName.text = StringConstants.EMPTY
            imageViewOfPavillion.isHidden = true
            lblOfProductServicesSeprator.isHidden = true
            self.bottomOfCollectionViewProductAndServices.constant = -10
        }
        if self.myChatsData.feed == StringConstants.ZERO{
            // if arrOfCommunityLocation.count>0{
            if let city = dictOfCommunityLocation["city"], let state = dictOfCommunityLocation["state"]{
                if let address = myChatsData.address{
                    if address.isEmpty{
                        strCompleteAddress = city + StringConstants.commaspace + state
                    }else{
                        strCompleteAddress = address + StringConstants.commaspace + "\n" + city + StringConstants.commaspace + state
                    }
                }else{
                    strCompleteAddress = city + StringConstants.commaspace + state
                }
            }else{
                if let address = myChatsData.address{
                    strCompleteAddress = address
                }
            }
            
            if  !strCompleteAddress.isEmpty{
                /*if let address = myChatsData.address{
                 heightOfLabelAddress.priority = UILayoutPriority(rawValue: 999)
                 heightOfLabelAddress.constant = 0
                 heightOfImageViewOfAddress.constant = 0
                 topOfLableAdreess.constant = -5
                 lblOfAddress.isHidden = true
                 imageViewOfAddress.isHidden = true
                 self.view.layoutIfNeeded()
                 }else{*/
                lblOfAddress.text = strCompleteAddress
                lblOfAddress.isHidden = false
                imageViewOfAddress.isHidden = false
                //}
            }else{
                lblOfAddress.text = StringConstants.EMPTY
                heightOfLabelAddress.priority = UILayoutPriority(rawValue: 999)
                heightOfLabelAddress.constant = 0
                heightOfImageViewOfAddress.constant = 0
                topOfLableAdreess.constant = -5
                lblOfAddress.isHidden = true
                imageViewOfAddress.isHidden = true
                heightConstraintOfAdreessTitl.constant = 0
                self.view.layoutIfNeeded()
            }
            
            if let contact_person_first_name = myChatsData.contact_person_first_name, let contact_person_last_name = myChatsData.contact_person_last_name {
                contactPersonName.text = contact_person_first_name + StringConstants.singleSpace + contact_person_last_name
            }else{
                contactPersonName.text = StringConstants.EMPTY
            }
            
            
            if let contact_person_designation = myChatsData.contact_person_designation {
                if contact_person_designation.isEmpty{
                    heightOfLableDesignation.constant = 0
                    topOfLblDesignation.constant = -5
                }else{
                    lblContactPersonDesignation.text = contact_person_designation
                }
            }else{
                lblContactPersonDesignation.text = StringConstants.EMPTY
                heightOfLableDesignation.constant = 0
                topOfLblDesignation.constant = -5
            }
            
            if let company_website = myChatsData.company_website {
                if company_website.isEmpty{
                    heightOfLableWebLink.constant = 0
                    heightOfImageViewOfWebLink.constant = 0
                    topOfLabelWebLink.constant = -5
                    imageViewOfWebLink.isHidden = true
                    lblWebsite.isHidden = true
                }else{
                    lblWebsite.text = company_website
                    imageViewOfWebLink.isHidden = false
                    lblWebsite.isHidden = false
                }
            }else{
                lblWebsite.text = StringConstants.EMPTY
                heightOfLableWebLink.constant = 0
                heightOfImageViewOfWebLink.constant = 0
                topOfLabelWebLink.constant = -5
                imageViewOfWebLink.isHidden = true
                lblWebsite.isHidden = true
            }
            
        }else{
            lblOfAddress.text = StringConstants.EMPTY
            heightOfLabelAddress.priority = UILayoutPriority(rawValue: 999)
            heightOfLabelAddress.constant = 0
            heightOfImageViewOfAddress.constant = 0
            topOfLableAdreess.constant = -5
            lblOfAddress.isHidden = true
            imageViewOfAddress.isHidden = true
            heightConstraintOfAdreessTitl.constant = 0
            heightOfLabelAddress.constant = 0
            heightOfViewOfBackAddress.constant = 0
            heightOfLabelName.constant = 0
            lblOfAddressTitle.isHidden = true
            heightOfImageViewOfName.constant = 0
            contactPersonName.isHidden = true
            self.view.layoutIfNeeded()
            
            lblContactPersonDesignation.text = StringConstants.EMPTY
            heightOfLableDesignation.constant = 0
            topOfLblDesignation.constant = -5
            
            lblWebsite.text = StringConstants.EMPTY
            heightOfLableWebLink.constant = 0
            heightOfImageViewOfWebLink.constant = 0
            topOfLabelWebLink.constant = -5
            imageViewOfWebLink.isHidden = true
            lblWebsite.isHidden = true
            
            topConstraintOfChannelFollower.constant = -18
            lblofAdressVewSeperator.isHidden = true
        }
        productServicesCollectionView.reloadData()
    }
    

    func callGetUserAreaInterestAPI() {
        let params = ["":""]
        print("params",params)
        let path = Bundle.main.path(forResource: "categoriesList", ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        
        let apiURL = String(describing: fileUrl)
        _ = UserAreaOfInterestServices().getInterest(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! UserAreaOfInterestResponse
                                                        let categoriesList = model.categories_list
                                                        let categoryData = categoriesList.filter( { (list: CategoriesList) -> Bool in
                                                            return list.is_occupation == false
                                                        })
                                                        
                                                        if let categoryId = self.myChatsData.communityCategories{
                                                            var arrOfId = categoryId.components(separatedBy: ",")
                                                            arrOfId = arrOfId.filter { $0 != "" }
                                                            var strCategoryName = String()
                                                            self.categories_list.removeAll()
                                                            for  i in (0..<arrOfId.count){
                                                                let strId = arrOfId[i]
                                                                let categories_list = categoryData.filter { ($0.id)?.range(of: strId, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                                                                
                                                                if categories_list.count > 0 {
                                                                    let category = categories_list[0]
                                                                    self.categories_list.append(category)
                                                                    if strCategoryName.isEmpty{
                                                                        strCategoryName = category.name!
                                                                    }else{
                                                                        strCategoryName = strCategoryName + "," + category.name!
                                                                    }
                                                                    self.lblCategories.text = strCategoryName
                                                                }
                                                            }
                                                        }
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func initialDesign(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        ViewOfBackbtnBroadCastVC.layer.cornerRadius = ViewOfBackbtnBroadCastVC.frame.size.width/2
        ViewOfBackbtnBroadCastVC.clipsToBounds = true
        
        btnMoveToBroadCastVC.layer.cornerRadius = btnMoveToBroadCastVC.frame.size.width/2
        btnMoveToBroadCastVC.clipsToBounds = true
        imageViewOfCommunitiyProfile.layer.cornerRadius = imageViewOfCommunitiyProfile.frame.size.width/2
        imageViewOfCommunitiyProfile.clipsToBounds = true
        viewOfBackChannelProfilePic.layer.cornerRadius = viewOfBackChannelProfilePic.frame.size.width/2
        viewOfBackChannelProfilePic.clipsToBounds = true
        lblOfProductServices.text = (NSLocalizedString(StringConstants.channelProfile.productAndServices, comment: StringConstants.EMPTY))
        lblOfAddressTitle.text = (NSLocalizedString(StringConstants.channelProfile.addressTitle, comment: StringConstants.EMPTY))
        lblOfEventName.text = (NSLocalizedString(StringConstants.channelProfile.eventName, comment: StringConstants.EMPTY))
        tblMenuOption.tableFooterView = UIView()
        self.tblMenuOption.separatorColor = UIColor.clear
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChannelProfileVC.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        viewOnScroll.addGestureRecognizer(tapGesture)
        viewOnScroll.isUserInteractionEnabled = true
        
        tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ChannelProfileVC.myviewTapped(_:)))
        tapGesture1.numberOfTapsRequired = 1
        tapGesture1.numberOfTouchesRequired = 1
        viewOfBackProfilePic.addGestureRecognizer(tapGesture1)
        viewOfBackProfilePic.isUserInteractionEnabled = true
    
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChannelProfileVC.dismissPopUp))
        tap.cancelsTouchesInView = false
        ViewOfBackTblMenuOption.addGestureRecognizer(tap)
        
        //Setup of colletionview
        self.productServicesCollectionView.delegate = self
        self.productServicesCollectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 145, height: 128)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        self.productServicesCollectionView.collectionViewLayout = layout
        
        //Comment this code for remove stall number and stall name
        lblStatllNumber.text = StringConstants.EMPTY
        lblOfPavillionName.text = StringConstants.EMPTY
        imageViewOfPavillion.isHidden = true
        lblOfProductServicesSeprator.isHidden = false
        self.bottomOfCollectionViewProductAndServices.constant = 18
        
        //set channel profile data when comes from notification and without notification
        if commingFrom == StringConstants.PushNotificationAction.Followchannel{
            self.showInitialDesign(StrComingWithmychatData: false)
        }else{
            self.showInitialDesign(StrComingWithmychatData: true)
        }
    }
    
    
    func showInitialDesign(StrComingWithmychatData:Bool){
        if !StrComingWithmychatData{
            //do nothing if coming from channel profile for first time
        }else{
            
            //Correct the dominant color
            if let communityDominantColour = myChatsData.communityDominantColour{
                var correctedCommunityDominantColour = String()
                if communityDominantColour.length < 7{
                    if communityDominantColour.length == 6{
                        correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode
                    }else{
                        correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode + StringConstants.DiscoverChannel.Colorcode
                    }
                    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                    self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    self.lblCategories.textColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    self.btnFollower.titleLabel?.textColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    viewOfBackProfilePic.backgroundColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    
                }else{
                    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                    self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
                    self.lblCategories.textColor = hexStringToUIColor(hex: communityDominantColour)
                    self.btnFollower.titleLabel?.textColor = hexStringToUIColor(hex: communityDominantColour)
                    viewOfBackProfilePic.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                }
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                self.lblCategories.textColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                self.btnFollower.titleLabel?.textColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                viewOfBackProfilePic.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
            
            
            if let channelProfilePic = myChatsData.communityImageBigThumbUrl {
                if channelProfilePic == StringConstants.EMPTY{
                    if myChatsData.feed == StringConstants.ONE{
                        imageViewOfCommunitiyProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                    }else{
                        imageViewOfCommunitiyProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                    }
                }else{
                    let imgurl = channelProfilePic
                    let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                    imageViewOfCommunitiyProfile.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
                }
            }
            self.setMenuOption()
        }
        self.tblMenuOption.reloadData()
    }
    
    func setMenuOption(){
        if userId == myChatsData.ownerId{
            items.append((NSLocalizedString(StringConstants.EnglishConstant.EditProfile, comment: StringConstants.EMPTY)))
            //                items.append((NSLocalizedString(StringConstants.EnglishConstant.Invite, comment: StringConstants.EMPTY)))
            //                items.append((NSLocalizedString(StringConstants.EnglishConstant.BlockedFollowers, comment: StringConstants.EMPTY)))
            
            
            // Remove this two options for phase 1
            //items.append(StringConstants.EnglishConstant.DeleteChannel)
            //items.append(StringConstants.EnglishConstant.LeaveChannel)
            heightOfMenuOptionView.constant = CGFloat(44 * items.count) // + 70
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.optionMenu), style: .plain, target: self, action: #selector(btnMenuOptionTapped))
            navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.plain_white_chat)
        }else{
            //Remove this comment at time of you want to enable invite feature
            //                items.append((NSLocalizedString(StringConstants.EnglishConstant.Invite, comment: StringConstants.EMPTY))
            //                )
            //  if (myChatsData.isMuted?.isEmpty)!{
            //      items.append(StringConstants.EnglishConstant.Mute)
            //  }else{
            //      items.append(StringConstants.EnglishConstant.UnMute)
            //  }
            items.append((NSLocalizedString(StringConstants.EnglishConstant.LeaveChannel, comment: StringConstants.EMPTY)))
            heightOfMenuOptionView.constant = CGFloat(44 * items.count) // + 70
            self.view.updateConstraints()
            self.ViewOfBackTblMenuOption.updateConstraints()
            if !(self.myChatsData.logged_user_blocked_timestamp == StringConstants.EMPTY){
                self.ViewOfBackbtnBroadCastVC.isHidden = true
            }else{
                self.ViewOfBackbtnBroadCastVC.isHidden = false
            }
            
            if myChatsData.isMember! == StringConstants.ONE{
                navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.optionMenu), style: .plain, target: self, action: #selector(btnMenuOptionTapped))
                navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.plain_white_chat)
            }else{
                if commingFrom == "userprofile"{
                    imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.plain_white_chat)
                }else{
                    imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.ic_arrow_forward_black_24dp)
                }
            }
        }
    }
    
    @objc func dismissPopUp() {
        self.view.endEditing(true)
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if commingFrom == StringConstants.PushNotificationAction.Followchannel{
        }else{
            
            if let communityDominantColour = myChatsData.communityDominantColour{
                var correctedCommunityDominantColour = String()
                if communityDominantColour.length < 7{
                    if communityDominantColour.length == 6{
                        correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode
                    }else{
                        correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode + StringConstants.DiscoverChannel.Colorcode
                    }
                    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                    self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    self.lblCategories.textColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    self.btnFollower.titleLabel?.textColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                    viewOfBackProfilePic.backgroundColor = hexStringToUIColor(hex: correctedCommunityDominantColour)

                }else{
                    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                    self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
                    self.lblCategories.textColor = hexStringToUIColor(hex: communityDominantColour)
                    self.btnFollower.titleLabel?.textColor = hexStringToUIColor(hex: communityDominantColour)
                    viewOfBackProfilePic.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                }
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                self.lblCategories.textColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                self.btnFollower.titleLabel?.textColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                viewOfBackProfilePic.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
        }
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfilePicClick(_ sender: Any) {
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
        
        if let channelProfilePic = myChatsData.communityImageUrl {
            let fullViewProfilePicVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewProfilePicVC") as! FullViewProfilePicVC
            fullViewProfilePicVC.commingFrom = StringConstants.flags.channelProfileVC
            fullViewProfilePicVC.imageData = channelProfilePic
            fullViewProfilePicVC.navName  = self.myChatsData.communityName!
            self.navigationController?.pushViewController(fullViewProfilePicVC, animated: false)
        }
    }
    
    
    @objc func btnMenuOptionTapped(sender: UIBarButtonItem) {
        if flag == false {
            flag = true
            ViewOfBackTblMenuOption.isHidden = false
        }else{
            flag = false
            ViewOfBackTblMenuOption.isHidden = true
        }
    }
    
    @IBAction func btnMoveToBroadCastVcClick(_ sender: Any) {
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
        if userId == myChatsData.ownerId{
            self.moveToChannelBroadCast()
        }else{
            if myChatsData.isMember == StringConstants.ONE{
                self.moveToChannelBroadCast()
            }else{
                if commingFrom == StringConstants.flags.FromUserProfile{
                    self.moveToChannelBroadCast()
                }else{
                //let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                //spinnerActivity.isUserInteractionEnabled = false
                self.buttonFollowClicked()
                }
            }
        }
    }
    
    
    func buttonFollowClicked(){
        
        //let followingDetails = self.communityList[buttonRow]
        let communityKey = myChatsData.communityKey!
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            
            var myChats = [MyChatsTable]()
            myChats.append(myChatsData)
            //---------------- Update isMember  value as true for follow channel & also at database----------//
            myChats.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
            myChats.filter({$0.communityKey == communityKey}).first?.messageText = NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
            
            // Update MyChat List
            //let followingData = myChats[0]
            var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: myChats)
            // Set follower count after following channel
            var totalMembers = Int()
            if let followerCount = self.myChatsData.memberCount {
                var totalMembers = Int(followerCount)
                if totalMembers! != 0{
                    totalMembers = totalMembers! + 1
                    dict.updateValue(totalMembers ?? Int(), forKey: "memberCount")
                    self.lblFollowersCount.text =  String(describing: totalMembers! - 1) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                    
                }else{
                    totalMembers = 1
                    self.lblFollowersCount.text =  String(describing: totalMembers) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                    dict.updateValue(totalMembers ?? Int(), forKey: "memberCount")
                }
            }else{
                totalMembers = 1
                self.lblFollowersCount.text =  String(describing: totalMembers) + StringConstants.singleSpace + (NSLocalizedString(StringConstants.channelProfile.followers, comment: StringConstants.EMPTY))
                dict.updateValue(totalMembers , forKey: "memberCount")
            }
            
            
            CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
            //let myChatCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
            // dict.updateValue(followingData.communityWithRssFeed ?? String(), forKey: "feed")
            dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
            dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
            
            let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
            if checkCommunityKeyWithisNormalMychatFlag.count == 0{
                MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
                dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
            }else{
                MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
            }
            /* if(myChatCommunityKey.count == 0){
             MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
             }else if (myChatCommunityKey.count == 1){
             MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
             }else{
             print("Not allowed")
             }*/
            // Change follow button staus to admin broad cast
            self.myChatsData = myChats[0]
            if !(self.myChatsData.logged_user_blocked_timestamp == StringConstants.EMPTY){
                self.ViewOfBackbtnBroadCastVC.isHidden = true
            }else{
                self.ViewOfBackbtnBroadCastVC.isHidden = false
            }
            if self.myChatsData.isMember! == StringConstants.ONE{
                self.imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.plain_white_chat)
            }else{
                self.imageViewOFBackBtnBroadCastClick.image = UIImage(named: StringConstants.ImageNames.ic_arrow_forward_black_24dp)
            }
            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi()
            }
        }else{
            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi()}
            }
      }
    
    func followChannelApi(){
        
        let communityKey = myChatsData.communityKey!
        let communityJabberId = myChatsData.communityJabberId!

        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            DispatchQueue.main.async {
                                                                self.setMenuOption()
                                                            } //---------------------------------------------------------------------------//
                                                            // Get old messages after follow channel
                                                            DispatchQueue.global(qos: .background).async {
                                                            self.getOldMessage(communityKey:communityKey)
                                                            }
                                                            //---------------------------------------------------------------------------//
                                                            
                                                            //---------------- Update isMember  value as true for follow channel & also at
                                                            //MBProgressHUD.hide(for: self.view, animated: true);
                                                            //self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.followChannel, comment: StringConstants.EMPTY)))
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    func getOldMessage (communityKey:String){
        let params = ["communityKey":communityKey]
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + "10" + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func moveToChannelBroadCast(){
        let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
        //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
        channelSendBroadcastMsgVC.commingFrom = StringConstants.flags.channelProfileVC
        channelSendBroadcastMsgVC.communityName = myChatsData.communityName!
        channelSendBroadcastMsgVC.communityKey = myChatsData.communityKey!
        channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsData.communityImageSmallThumbUrl!
        channelSendBroadcastMsgVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
    }
    
    @IBAction func btnShowFollowersListClick(_ sender: Any) {
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let channelFollowerList = storyboard.instantiateViewController(withIdentifier: "ChannelFollowerList") as! ChannelFollowerList
        channelFollowerList.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelFollowerList, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension ChannelProfileVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
        cell.selectionStyle = .none
        cell.lblMenuTitle.text = StringUTFEncoding.UTFEncong(string: items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        flag = false
        ViewOfBackTblMenuOption.isHidden = true
        if userId == myChatsData.ownerId {
            if indexPath.row == 0 {
                //            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                //            if checkInternetConnection == true {
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let editChannelProfileVC = storyboard.instantiateViewController(withIdentifier: "EditChannelProfileVC") as! EditChannelProfileVC
                editChannelProfileVC.dominantColour = dominantColour
                editChannelProfileVC.myChatsData = myChatsData
                editChannelProfileVC.categories_list =  self.categories_list
                self.navigationController?.pushViewController(editChannelProfileVC, animated: true)
                //            }else{
                //                SCLAlertView().showError("", subTitle: "No Intenet Connection. Please try again!")
                //            }
            }else if indexPath.row == 1{
                //Invite channel
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectMembersVC = storyboard.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                selectMembersVC.strComingFrom = "ChannelProfileVC"
                selectMembersVC.communityKey = myChatsData.communityKey!
                self.navigationController?.pushViewController(selectMembersVC, animated: true)
            }else if indexPath.row == 2{
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let blockMembersListVC = storyboard.instantiateViewController(withIdentifier: "BlockMembersListVC") as! BlockMembersListVC
                blockMembersListVC.myChatsData = myChatsData
                self.navigationController?.pushViewController(blockMembersListVC, animated: true)
            }else if indexPath.row == 4{
                self.navigationController?.view.makeToast(StringConstants.ToastViewComments.channelCanNotleave)
            }
        }else{
            /*if indexPath.row == 0{
                //Invite channel
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectMembersVC = storyboard.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                selectMembersVC.strComingFrom = "ChannelProfileVC"
                selectMembersVC.communityKey = myChatsData.communityKey!
                self.navigationController?.pushViewController(selectMembersVC, animated: true)
            }else*/ if indexPath.row == 0{
                DispatchQueue.main.async {
                    self.popupAlert(title: (NSLocalizedString(StringConstants.AlertMessage.leaveChannelConfirmation, comment: StringConstants.EMPTY))
                        , message: StringConstants.EMPTY, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                        },{action2 in
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                            DispatchQueue.global(qos: .background).async {
                                self.leaveChannel()
                            }
                        }, nil])
                }
                /*SVProgressHUD.show()
                if (myChatsData.isMuted?.isEmpty)!{
                    self.muteAndUnMuteChannel(muteStatus:StringConstants.TRUE,communityKey:self.myChatsData.communityKey!)
                }else{
                    self.muteAndUnMuteChannel(muteStatus:StringConstants.FALSE,communityKey:self.myChatsData.communityKey!)
                }*/
            }/*else if indexPath.row == 2{
                DispatchQueue.main.async {
                    self.popupAlert(title: StringConstants.AlertMessage.leaveChannelConfirmation, message: StringConstants.EMPTY, actionTitles: [StringConstants.AlertMessage.btnNo,StringConstants.AlertMessage.btnYes], actions:[{action1 in
                        },{action2 in
                            SVProgressHUD.show()
                            DispatchQueue.global(qos: .background).async {
                                self.leaveChannel()
                            }
                        }, nil])
                }
            }*/
        }
    }
    
    func muteAndUnMuteChannel(muteStatus:String,communityKey:String){
        
        let params = MuteUnMuteRequest.convertToDictionary(muteUnMuteDetails: MuteUnMuteRequest.init(muteStatus: muteStatus, communityKey: communityKey))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.muteUnmute + communityKey
        _ = MuteUnMuteServices().muteUnMuteServices(apiURL,
                                                    postData: params as [String : AnyObject],
                                                    withSuccessHandler: { (userModel) in
                                                        let model = userModel as! MuteUnMuteResponse
                                                        print("model",model)
                                                        
                                                        let communityDetailsOnCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                        var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: communityDetailsOnCommunityKey)
                                                        if muteStatus == StringConstants.FALSE{
                                                            dict.updateValue(StringConstants.EMPTY, forKey: "isMuted")
                                                            MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                                                            self.items.remove(at: 1)
                                                            self.items.insert(StringConstants.EnglishConstant.Mute, at: 1)
                                                            self.navigationController?.view.makeToast(StringConstants.ToastViewComments.unMute)
                                                            let mycCHat = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                            self.myChatsData = mycCHat[0]
                                                        }else{
                                                            dict.updateValue(muteStatus, forKey: "isMuted")
                                                            MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                                                            self.items.remove(at: 1)
                                                            self.items.insert(StringConstants.EnglishConstant.UnMute, at: 1)
                                                            self.navigationController?.view.makeToast(StringConstants.ToastViewComments.mute)
                                                            let mycCHat = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                            self.myChatsData = mycCHat[0]
                                                        }
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        self.tblMenuOption.reloadData()
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    func leaveChannel(){
        
        let params = LeaveChannelRequest.convertToDictionary(leaveChannelDetails: LeaveChannelRequest.init(operation:StringConstants.channelProfile.leaveChannelOperation , communityJabberId: self.myChatsData.communityJabberId!, new_admin_user_id: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.leaveCommunity1 + self.myChatsData.communityKey! + URLConstant.leaveCommunity2 + userId
        _ = LeaveChannelServices().leaveChannelServices(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! LeaveChannelResponse
                                                            print("model",model)
                                                            CommunityDetailService.sharedInstance.updateisMemberFromComminityDetailsTbl(isMemberValue: StringConstants.ZERO, communityKey: self.myChatsData.communityKey!)
                                                               let currentTime = DateTimeHelper.getCurrentMillis()
                                                            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg:StringConstants.DashboardViewController.unfollowStatus /*NSLocalizedString(StringConstants.DashboardViewController.unfollowStatus, comment: "")*/, latestMessageTime:String(currentTime), communityKey:self.myChatsData.communityKey!,mediaType:StringConstants.EMPTY,messageType:StringConstants.MessageType.banner, lastMessageId: "")
                                                            MyChatServices.sharedInstance.updateisMemberFromMyChatTbl(isMemberValue: StringConstants.ZERO, communityKey: self.myChatsData.communityKey!)

                                                            DispatchQueue.main.async {
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                                let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                                if let nav = self.navigationController {
                                                                    for controller in nav.viewControllers as Array {
                                                                        if controller is DashboardViewController {
                                                                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                                            break
                                                                        }
                                                                    }
                                                                    nav.pushViewController(vc, animated: false)
                                                                    return
                                                                }
                                                            }
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
}

extension  ChannelProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: UICollectionViewDelegate & UICollectionViewDataSource Functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfProductService.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ChannelProductServicesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelProductServicesCell", for: indexPath) as! ChannelProductServicesCell
        
        let productMedias = arrOfProductService[indexPath.row]
        if productMedias.type == StringConstants.MediaType.image {
            cell.imageViewOfShowPlayIcon.isHidden = true
            let imgurl = productMedias.url
            let imageUrlByRemovingSpace = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            cell.imageViewOfShowThumbImg.sd_setImage(with: URL(string: imageUrlByRemovingSpace), placeholderImage: UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
            cell.imageViewOfShowThumbImg.contentMode = UIViewContentMode.scaleAspectFit
        }else if productMedias.type == StringConstants.MediaType.video {
            cell.imageViewOfShowPlayIcon.isHidden = true
            cell.imageViewOfShowThumbImg.image = UIImage(named: StringConstants.ImageNames.video_image)
            cell.imageViewOfShowThumbImg.contentMode = UIViewContentMode.scaleAspectFill
        }else {
            cell.imageViewOfShowPlayIcon.isHidden = true
            cell.imageViewOfShowThumbImg.image = UIImage(named:StringConstants.ImageNames.doc_image)
            cell.imageViewOfShowThumbImg.contentMode = UIViewContentMode.scaleAspectFill
        }
        cell.btnOpenMediaClicked.addTarget(self,action:#selector(btnOpenMediaClicked(sender:)), for: .touchUpInside)
        cell.btnOpenMediaClicked.tag = indexPath.row
        return cell
    }
    
    @objc func btnOpenMediaClicked(sender:UIButton) {
        let buttonRow = sender.tag
        print("buttonRow",buttonRow)
        let productMedias = arrOfProductService[buttonRow]

        if productMedias.type == StringConstants.MediaType.image {
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            let imgurl = productMedias.url
            let imageUrlByRemovingSpace = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            fullViewOfImageFromMessagesVC.imageDataFromServer = imageUrlByRemovingSpace
            self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
        }else{
            let docUrl = productMedias.url
            let finalUrl = docUrl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let url = URL(string: finalUrl)
            let controller = SVWebViewController(url: url)
            self.navigationController?.pushViewController(controller!, animated: true)
           /* if let url = URL(string:finalUrl ) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                vc.preferredBarTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                vc.preferredControlTintColor = UIColor.white
                vc.delegate = self
                vc.dismissButtonStyle = .close
                vc.title = StringConstants.EMPTY
                present(vc, animated: true)
            }*/ 
//            let url = URL(string: finalUrl)
//            let controller = SVWebViewController(url: url)
//            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    //Sfsafari Delegate
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
 
}
