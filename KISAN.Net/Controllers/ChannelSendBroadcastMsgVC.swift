//
//  ChannelSendBroadcastMsgVC.swift
//  KisanChat
//
//  Created by KISAN TEAM on 18/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices
import XMPPFramework
import AWSS3
import ReverseExtension
import MBProgressHUD
import youtube_ios_player_helper_swift
import UserNotifications


class ChannelSendBroadcastMsgVC: UIViewController,UIGestureRecognizerDelegate,PassMultiMediaResponse,UIDocumentPickerDelegate{
    
    var communityName = String()
    @IBOutlet weak var bottomOfmessageComposingView: NSLayoutConstraint!
    @IBOutlet weak var tblChannelSendBroadcastMsgList: UITableView!
    @IBOutlet weak var txtViewOfChatMessage: UITextView!
    @IBOutlet weak var topOfmessageComposingView: NSLayoutConstraint!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var topOfTblChannelSendBroadcastMsgList: NSLayoutConstraint!
    @IBOutlet weak var viewOfBackSendMsg: UIView!
    @IBOutlet weak var btnOneToOneChat: UIButton!
    @IBOutlet weak var heightConstantOfViewBackChatTextview: NSLayoutConstraint!
    @IBOutlet weak var heightConstantOftxtViewOfChatMessage: NSLayoutConstraint!
    @IBOutlet weak var bottomOfTblChannelBroadCastMsg: NSLayoutConstraint!
    var userId = String()
    var dominantColour = String()
    var commingFrom = String()
    @IBOutlet weak var tblOfMenuOnPopup: UITableView!
    @IBOutlet weak var ViewOfBacktblMenuPopUp: UIView!
    @IBOutlet weak var btnLoadMore: UIButton!
    @IBOutlet weak var heightOfBtnMore: NSLayoutConstraint! //30
    @IBOutlet weak var btnNewMessage: UIButton!
    @IBOutlet var heightConstarintforAttchTable: NSLayoutConstraint!
    var titleOfCreatePost = String()
    var mesaageOfCreatePost = String()
    
    var arrOfMenuImages = [StringConstants.ImageNames.camera,StringConstants.ImageNames.image,StringConstants.ImageNames.video,/*StringConstants.ImageNames.audio,StringConstants.ImageNames.location,*/StringConstants.ImageNames.doc/*,StringConstants.ImageNames.post*/]
    var arrOfAttachmentList = [(NSLocalizedString(StringConstants.popUpMenuName.camera, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.image, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.video, comment: StringConstants.EMPTY)),/*(NSLocalizedString(StringConstants.popUpMenuName.audio, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.location, comment: StringConstants.EMPTY)),*/(NSLocalizedString(StringConstants.popUpMenuName.doc, comment: StringConstants.EMPTY)),/*(NSLocalizedString(StringConstants.popUpMenuName.post, comment: StringConstants.EMPTY))*/]
    var arrOfMenuList = [StringConstants.popUpMenuName.channelprofile,StringConstants.popUpMenuName.search,StringConstants.popUpMenuName.invite,StringConstants.popUpMenuName.mute]
    var flagOfCheckPopup = String()
    var ownerId = String()
    var navView = UIView()
    var flagOfCheckLongPressRegonizer = Bool()
    var longPressIndexPath:NSIndexPath?
    var previouslongPressIndexPath:NSIndexPath?

    var flagOfClickOnMoreOption = Bool()
    var flagOfClickOnAttachmentOption = Bool()
    var appDelegate = AppDelegate()
    var imagePicker = UIImagePickerController()
    var imagePickerForOnlyImages = UIImagePickerController()
    var capturImage = UIImage()
    var videoURL: URL?
    lazy   var searchBar:UISearchBar = UISearchBar()
    var btnCloseSearch = UIButton()
    var flagForCheckOpenSerachBar = Bool()
    var arrOfOriginalCopy = NSMutableArray()
    var communityKey = String()
    var messageList = [Messages]()
    var messageListTemp = [Messages]()
    var filteredObjects: [Messages]?
    var forwardedmessageDetails: Messages?
    var createSendMessageRequestInDB = Dictionary<String, Any>()
    var communityImageSmallThumbUrl = String()
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var thumbnailImage = UIImage()
    var imageData = Data()
    var pdfDocData = Data()
    var pdfThumbData = Data()
    var thumbVideoData = Data()
    var messageKeyForSendMultiMedia = String()
    var messageTimeForSendMultiMedia = String()
    var originalMediaBucketName = String()
    var thumbMediaBucketName = String()
    let documentDirectoryHelper = DocumentDirectoryHelper()
    var mediaSize = Double()
    var myChatsData = MyChatsTable()
    var S3UploadKeyNameWithImagePrefix = String()
    var S3UploadKeyNameWithThumbPrefix = String()
    var documentName = String()
    
    //For Pagination
    var isDataLoading:Bool = false
    var page_number : Int = 1
    var page_size : Int = 10
    var fetchOffSet :Int = 0
    var isFirstRowIsVisible:Bool = false
    @IBOutlet weak var lblOfLeaveMsgToast: UILabel!
    
    //PIP Mode
    var videoPlayer = PIPVideoPlayer()
    var strToggle = String()
    //  ------------------------------//
    private var playerView: PlayerView!
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    override var shouldAutorotate: Bool{
        return false
    }
    // -----------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let indexPath = IndexPath(row: 0, section: 0)
        //previouslongPressIndexPath = indexPath as NSIndexPath

       //register a post message cell
        self.tblChannelSendBroadcastMsgList.register(UINib(nibName: "PostMesaageViewCell", bundle:nil), forCellReuseIdentifier: "PostMesaageViewCell")
        //register a PIPViewCell
        self.tblChannelSendBroadcastMsgList.register(UINib(nibName: "PIPViewCell", bundle:nil), forCellReuseIdentifier: "PIPViewCell")
        
        // Save my chat data globally for update value of userCommunityJabberId after one to one chat init
        SessionDetailsForCommunityType.shared.globalMyChatsData = myChatsData
        setReverseExtension()
        // Get Data from local db
        let messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
        // messages = messages.sorted(by: {$0.createdDatetime! < $1.createdDatetime!} )
        messageList.removeAll()
        messageListTemp.removeAll()
        messageList = messages
        messageListTemp = messages
        btnLoadMore.setTitle((NSLocalizedString(StringConstants.AlertMessage.LoadMore, comment: StringConstants.EMPTY)), for: .normal)
        
        if messages.count > 0 {
            // check local database
            self.isDataLoading = messages.count > 0 ? true : false
            self.page_number =   self.page_number + 1
            self.fetchOffSet =   self.fetchOffSet + 10
        }
        
        if messages.count < 10 {
            isDataLoading = false
        }
        
        heightConstarintforAttchTable.constant = 184
        
        initialDesign()
        self.addKeyboardNotifications()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChannelSendBroadcastMsgVC.dismissPopUp))
        tap.cancelsTouchesInView = false
        self.ViewOfBacktblMenuPopUp.addGestureRecognizer(tap)
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        flagForCheckOpenSerachBar = false
        setTblScrollAtBottom()
        intialiseBtnMore()
        hideBtnMore()
        
        
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)

        // set notification center for update the message list when message deleted from admin
        if myChatsData.ownerId != userId{
            NotificationCenter.default.addObserver(self, selector: #selector(self.refreshMessageFromNotification(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.messageDeleted), object: nil)
        }
        
        //self.getOldMessage()
        
        // Receive messages from notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMessages(_:)), name: NSNotification.Name(rawValue: "receivedMessagesForChannelBroadCast"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if strToggle == StringConstants.flags.toggle{
            videoPlayer.addPlayerView(in:videoPlayer.videoView)
        }
    }

    
    @objc func receiveMessages(_ notification: NSNotification) {
        
        let messages = notification.userInfo?[StringConstants.NSUserDefauluterKeys.receivedMessages] as? Messages
        if messageList.contains(where: { $0.id == messages!.id }){
            print("Dont append")
        }else{
            if communityKey != messages?.communityId {
                print("Dont append")
            }else{
            // messageList.append(messages)
            messageList.insert(messages!, at: 0)
            messageListTemp = messageList
            if messageList.count < 10 {
                isDataLoading = false
            }
            }
        }
        // Change unreadMessagesCount to zero
        MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: communityKey)
        //For show new message button
        if userId == myChatsData.ownerId{
            btnNewMessage.isHidden = true
            setTblScrollAtBottom()
            self.tblChannelSendBroadcastMsgList.reloadData()
        }else{
            if isFirstRowIsVisible == true{
                setTblScrollAtBottom()
                btnNewMessage.isHidden = true
                self.tblChannelSendBroadcastMsgList.reloadData()
            }else{
                btnNewMessage.isHidden = false
            }
        }
    }
    
    func setReverseExtension()  {
        tblChannelSendBroadcastMsgList.re.delegate = self
        tblChannelSendBroadcastMsgList.re.scrollViewDidReachTop = { scrollView in
            print("scrollViewDidReachTop")
        }
        tblChannelSendBroadcastMsgList.re.scrollViewDidReachBottom = { scrollView in
            print("scrollViewDidReachBottom")
        }
        tblChannelSendBroadcastMsgList.estimatedRowHeight = 44
        tblChannelSendBroadcastMsgList.rowHeight = UITableViewAutomaticDimension
    }
    
    func intialiseBtnMore()  {
        self.btnLoadMore.layer.cornerRadius = 6.0
        self.btnLoadMore.layer.masksToBounds = true
    }
    
    // ** Pavan
    func setBtnMore ()  {
        self.btnLoadMore.isHidden = false
        self.heightOfBtnMore.constant = 30.0
        if myChatsData.feed == StringConstants.ONE{
            self.view.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            if let communityDominantColour = myChatsData.communityDominantColour{
                self.view.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                self.view.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
        }
    }
    // ** Pavan
    func hideBtnMore() {
        self.heightOfBtnMore.constant = 0.0
        self.btnLoadMore.isHidden = true
    }
    // ** Pavan
    @IBAction func btnLoadMore(_ sender: Any) {
        //Local Pagination
        self.hideBtnMore()
        var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
        if messages.count > 0 {
            // check local database
            let preveousCount = self.messageList.count
            
            self.isDataLoading = messages.count > 0 ? true : false
            self.page_number =   self.page_number + 1
            self.fetchOffSet =   self.fetchOffSet + 10
            for i in 0 ..< messages.count {
                let list = messages[i]
                self.isDataLoading = true
                if !messageList.contains(list){
                    self.messageList.append(list)
                    self.messageListTemp.append(list)
                }
            }
            self.tblChannelSendBroadcastMsgList.reloadData()
             self.tblChannelSendBroadcastMsgList.scroll(to: .bottom, animated: false)
        }else{
            // check internet and fire api
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.getOldMessage()
        }
    }
    
    @objc func refreshMessageFromNotification(_ notification: Notification) {
        // Logic for only deleted message delete from list
        var index = -1
        if let obj = notification.object as? [String:Any]{
            let noti_msg_id =  obj["id"] as! String
            let noti_community_id =  obj["communityId"] as! String
            for i in 0..<self.messageList.count {
                let model = self.messageList[i]
                let msgId = model.id ?? ""
                _ = model.communityId
                if msgId == noti_msg_id{ //&& noti_community_id == communityId{
                    index = i
                    break
                }
            }
            if index >= 0{
                if self.messageList.count > index{
                    self.messageList.remove(at: index)
                    if self.messageListTemp.count > index{
                        self.messageListTemp.remove(at: index) }
                    self.tblChannelSendBroadcastMsgList.reloadData()
                    // delete from local database
                    let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: Messages(), messageId:noti_msg_id , communityKey: noti_community_id)
                    print(status)
                    MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey:noti_community_id, messageId: noti_msg_id)
                }
            }else{
                let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: Messages(), messageId:noti_msg_id , communityKey: noti_community_id)
                print(status)
                MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey:noti_community_id, messageId: noti_msg_id)
            }
        }
  }
    
    func getOldMessage(){
        let params = ["communityKey":communityKey]
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + String(self.page_size) + URLConstant.getOldMessages3 + String(self.page_number) + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            DispatchQueue.main.async {
                                                                var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(self.fetchOffSet, communityKey: self.communityKey)
                                                                self.isDataLoading = messages.count > 0 ? true : false
                                                                self.page_number =  messages.count > 0 ? self.page_number + 1 : self.page_number
                                                                self.fetchOffSet =  messages.count > 0 ? self.fetchOffSet + 10 : self.fetchOffSet
                                                                let preveousCount = self.messageList.count
                                                                for i in 0 ..< messages.count {
                                                                    let list = messages[i]
                                                                    if !self.messageList.contains(list){
                                                                        self.messageList.append(list)
                                                                        self.messageListTemp.append(list)
                                                                    }
                                                                }
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                                self.tblChannelSendBroadcastMsgList.reloadData()
                                                            }
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.isDataLoading = true
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    func deleteMessage(messageDetails:Messages){
        let params = ["communityKey":self.communityKey]
        let simpleComunityKey = self.communityKey
        
        let apiURL = URLConstant.BASE_URL + URLConstant.deleteMessage1 + simpleComunityKey + URLConstant.deleteMessage2 + messageDetails.id!
        print("apiURL",apiURL)
        _ = DeleteMessageServices().deleteMessage(apiURL, postData: params as [String : AnyObject], withSuccessHandler: { (userModel) in
            let model = userModel as! DeleteMessageResponse
            print("model",model)
            self.deleteMessageLocally()
            MBProgressHUD.hide(for: self.view, animated: true);
            self.view.makeToast(model.message)
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.isDataLoading = true
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Save community key in session for mute sound of comming message
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey)
        //Show notification if user leave this screen
        UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UserDefaults.standard.set(true, forKey: "MessageDeleteStatus")
        //Not allowed show message notification for on same screen
        UserDefaults.standard.set(communityKey, forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromChannelSendBroad)

        flagOfCheckLongPressRegonizer = false
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        flagOfCheckPopup = ""
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        lblOfLeaveMsgToast.text = (NSLocalizedString(StringConstants.Validation.noLongerFollower, comment: StringConstants.EMPTY))
        
        
        // Save community key in session for mute sound of comming message
        SessionManagerForSaveData.saveDataInSessionManager(valueString:communityKey , keyString: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey)
        // Set Mute and Unmute menu option
        if userId == myChatsData.ownerId{
            if (myChatsData.isMuted?.isEmpty)!{
                self.arrOfMenuList.remove(at: 3)
                self.arrOfMenuList.insert(StringConstants.EnglishConstant.Mute, at: 3)
            }else{
                self.arrOfMenuList.remove(at: 3)
                self.arrOfMenuList.insert(StringConstants.EnglishConstant.UnMute, at: 3)
            }
        }
        // Set Navigation Bar
        self.setNavigationBarAtScreenLoadTime()
        if myChatsData.feed == StringConstants.ONE{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            //self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
            self.view.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            //tblChannelSendBroadcastMsgList.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
            tblChannelSendBroadcastMsgList.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            if let communityDominantColour = myChatsData.communityDominantColour{
                print("communityDominantColour",communityDominantColour)
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
                //self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                self.view.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                // tblChannelSendBroadcastMsgList.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                tblChannelSendBroadcastMsgList.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                //self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                self.view.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                //tblChannelSendBroadcastMsgList.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                tblChannelSendBroadcastMsgList.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
        }
        
        //Set Image selected from camera
        setMultiMediaPart()
    }
    
    
    
    func setMultiMediaPart(){
        
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let size = CGSize(width: 0, height: 0)
        if appDelegate.selctedImage.size.width != size.width{
            self.uploadImageToAws(currentTime:currentTime,strUUID:strUUID,selectedImage:appDelegate.selctedImage,messageType:channelMedia)
        }else if appDelegate.selectedVideoURL != nil{

          self.uploadVideoToAws(currentTime:currentTime,strUUID:strUUID,selectedVideoURL:appDelegate.selectedVideoURL!,messageType:channelMedia)
            
        }else if appDelegate.captureAudioUrl != nil {
            
            do {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                let audio = try NSData(contentsOf: appDelegate.captureAudioUrl!, options: .mappedIfSafe)
                // Total Size in KB
                mediaSize = Double(audio.length) / 1024.0
                let formatter = NumberFormatter()
                formatter.numberStyle = .none
                formatter.maximumFractionDigits = 0
                let formattedAmount = formatter.string(from: mediaSize as NSNumber)!
                print(mediaSize)
                mediaSize = Double(formattedAmount)!
                
                print(mediaSize)
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original audio file to Amzon S3
                originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + audios + "/" + messageKeyForSendMultiMedia
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save audio file at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + audioPrefix + String(self.messageTimeForSendMultiMedia)
                let audioFileName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioFileName , sourceType: StringConstants.MediaType.audio,imageData:audio as NSData)
                uploadMediaPartToS3.uploadImage(with: audio as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.audio,callAgain:false,contentType:audioContentType, uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            } catch {
                print("Error")
            }
            
        }else if !(appDelegate.locationTitleAddress.isEmpty) && !appDelegate.locationDetailddress.isEmpty{
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            let strUUID = UUIDHelper.getUUID()
            let messageKey = strUUID
            let createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent, communityName:  myChatsData.communityName!, communityProfileUrl: myChatsData.communityImageBigThumbUrl!)
            print("createSendMessageRequest",createSendMessageRequest)
            
            createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
            
            
            // Show message on same screen
            var  messages = Messages()
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
                self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: strUUID, communityJabberId: myChatsData.communityJabberId!)
            }else{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            }
            
            //  messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            setTblScrollAtBottom()
            tblChannelSendBroadcastMsgList.reloadData()
            //Save message in core data as local DB with pending status
            let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
            
            if checkInternetConnection == true{
                self.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityJabberId: myChatsData.communityJabberId!)
            }
            txtViewOfChatMessage.text = StringConstants.EMPTY
            
            // Make nill object
            appDelegate.strCaptionText = ""
            appDelegate.selctedImage = UIImage()
            appDelegate.selectedVideoURL = nil
            appDelegate.captureAudioUrl = nil
            appDelegate.locationTitleAddress = ""
            appDelegate.locationDetailddress = ""
            appDelegate.longitudeOfSendLocation = ""
            appDelegate.latitudeOfSendLocation = ""
        }else if appDelegate.strCreatePost == StringConstants.flags.CreatePostViewController {
            let size = CGSize(width: 0, height: 0)
            if appDelegate.selectedImageOfPostCreation.size.width != size.width{
            self.uploadImageToAws(currentTime:currentTime,strUUID:strUUID,selectedImage:appDelegate.selectedImageOfPostCreation,messageType:post)

            }else{
                self.uploadVideoToAws(currentTime:currentTime,strUUID:strUUID,selectedVideoURL:appDelegate.selectedVideoOfPostCreation!,messageType:post)
            }

        }
    }
    
    func uploadImageToAws(currentTime:Int64,strUUID:String,selectedImage:UIImage,messageType:String){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        // Compress image
        let imageHelper = ImageHelper()
        imageData = imageHelper.compressImage(image: selectedImage, compressQuality: 0.4)!
        messageKeyForSendMultiMedia = strUUID
        messageTimeForSendMultiMedia = String(currentTime)
        //Send original image to Amzon S3
        originalMediaBucketName = S3BucketName + messageType + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia
        let uploadMediaPartToS3  = UploadMediaPartToS3()
        uploadMediaPartToS3.delegate = self
        //Save Image at docoument directory
        S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + imagePrefix + String(messageTimeForSendMultiMedia)
        let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
        documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imageData as NSData)
       
        if messageType == post{
             uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.imageOfPostCreation,callAgain:true,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithImagePrefix)
        }else{
            uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:true,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithImagePrefix)
        }
    
    }
    
    func uploadVideoToAws(currentTime:Int64,strUUID:String,selectedVideoURL:URL,messageType:String){
        let imageHelper = ImageHelper()
        let MyUrl = selectedVideoURL
        let fileAttributes = try! FileManager.default.attributesOfItem(atPath: MyUrl.path)
        let fileSizeNumber = fileAttributes[FileAttributeKey.size] as! NSNumber
        let fileSize = fileSizeNumber.int64Value
        var sizeMB = Double(fileSize / 1024)
        sizeMB = Double(sizeMB / 1024)
        print(String(format: "%.2f", sizeMB) + " MB")
        
        if sizeMB > 5{
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4")
            imageHelper.compressVideo(inputURL: (selectedVideoURL), outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {
                        return
                    }
                    print("File size after compression: \(Double(compressedData.length / 1024)) kb")
                    
                    DispatchQueue.main.async {
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                        //let video = try NSData(contentsOf: selectedVideoURL, options: .mappedIfSafe)
                        let video = compressedData
                        // Total Size in KB
                        self.mediaSize = Double(video.length) / 1024.0
                        print("mediaSize",self.mediaSize)
                        let imageThumb = imageHelper.getThumbnailImage(forUrl: selectedVideoURL)
                        self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                        self.messageKeyForSendMultiMedia = strUUID
                        self.messageTimeForSendMultiMedia = String(currentTime)
                        //Send original image to Amzon S3
                        self.originalMediaBucketName = S3BucketName + messageType + self.communityKey + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                        let uploadMediaPartToS3  = UploadMediaPartToS3()
                        uploadMediaPartToS3.delegate = self
                        //Save Image at docoument directory
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                        let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                        
                        if messageType == post{
                             uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.imageOfPostCreation,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                        }else{
                            uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                        }
                    }
                    
                case .failed:
                    break
                case .cancelled:
                    break
                }
            }
        }else{
            guard let compressedData = NSData(contentsOf: selectedVideoURL) else {
                return
            }
            print("File size after compression: \(Double(compressedData.length / 1024)) kb")
            
            DispatchQueue.main.async {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                //let video = try NSData(contentsOf: selectedVideoURL, options: .mappedIfSafe)
                let video = compressedData
                // Total Size in KB
                self.mediaSize = Double(video.length) / 1024.0
                print("mediaSize",self.mediaSize)
                let imageThumb = imageHelper.getThumbnailImage(forUrl: selectedVideoURL)
                self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                if messageType == post{
                    let croppedImage = self.cropToBounds(image: imageThumb!, width: 120, height: 120)
                    self.thumbVideoData = imageHelper.compressImage(image: croppedImage, compressQuality:  0.25)!
                }
                self.messageKeyForSendMultiMedia = strUUID
                self.messageTimeForSendMultiMedia = String(currentTime)
                //Send original image to Amzon S3
                self.originalMediaBucketName = S3BucketName + messageType + self.communityKey + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save Image at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                if messageType == post{
                    uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.imageOfPostCreation,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                }else{
                    uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)

                }
                //StringConstants.MediaType.imageOfPostCreatio
                
//                uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            }
        }
        
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
        if callAgain == true{
            //Send Thumb image to Amzon S3
            let uploadMediaPartToS3  = UploadMediaPartToS3()
            uploadMediaPartToS3.delegate = self
            
            if mediaSource == StringConstants.MediaType.image{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.video{
                
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: thumbVideoData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + ".pdf"
                uploadMediaPartToS3.uploadImage(with: pdfThumbData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:false,contentType:pdfContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.doc{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.doc,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.docx{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName =  S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.docx,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.imageOfPostCreation {
                let size = CGSize(width: 0, height: 0)
                if appDelegate.selectedImageOfPostCreation.size.width != size.width{
                    thumbMediaBucketName = S3BucketName + post + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                    uploadMediaPartToS3.uploadImage(with: imageData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.imageOfPostCreation,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
                }else{
                    thumbMediaBucketName = S3BucketName + post + communityKey + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                    uploadMediaPartToS3.uploadImage(with: thumbVideoData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.imageOfPostCreation,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
                }
            }
        }else{
            
            let strUUID = UUIDHelper.getUUID()
            var strChatMessage = String()
            var mediaName = String()
            var mediaType = String()
            var contentField9 = String()
            if mediaSource == StringConstants.MediaType.image{
                let imageHelper = ImageHelper()
                // Total Size in KB
                mediaSize = imageHelper.getImageSize(imgData: imageData as NSData)
                print("mediaSize",mediaSize)
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                // Bucket name after adding Base URL & Image name
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.image
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.video{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp4"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.video
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.audio{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.audio
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".pdf"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + ".pdf"
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix + ".pdf"
                mediaType = StringConstants.MediaType.document
                contentField9 = pdf
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + ".pdf"
            }else if mediaSource == StringConstants.MediaType.doc{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }else if mediaSource == StringConstants.MediaType.docx{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }else if mediaSource == StringConstants.MediaType.imageOfPostCreation {
                
                let size = CGSize(width: 0, height: 0)
                if appDelegate.selectedImageOfPostCreation.size.width != size.width{
                    let imageHelper = ImageHelper()
                    // Total Size in KB
                    mediaSize = imageHelper.getImageSize(imgData: imageData as NSData)
                    print("mediaSize",mediaSize)
                    mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                    // Bucket name after adding Base URL & Image name
                    originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                    S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                    thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                    mediaType = StringConstants.MediaType.image
                    contentField9 = StringConstants.EMPTY
                    strChatMessage = appDelegate.strCaptionText
                }else{
                    mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp4"
                    originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                    S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                    thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                    mediaType = StringConstants.MediaType.video
                    contentField9 = StringConstants.EMPTY
                    strChatMessage = appDelegate.strCaptionText
                }
            }
        
            MBProgressHUD.hide(for: self.view, animated: true);
          
            var createSendMessageRequest = Dictionary<String, Any>()
            if appDelegate.strCreatePost == StringConstants.flags.CreatePostViewController {
             createSendMessageRequest  = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.post, contentField1: titleOfCreatePost, contentField2: mesaageOfCreatePost, contentField3: originalMediaBucketName, contentField4: myChatsData.communityImageUrl!, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: myChatsData.communityName!, contentField8: thumbMediaBucketName, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
                print("createSendMessageRequest",createSendMessageRequest)
                
                createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.post, contentField1: titleOfCreatePost, contentField2: mesaageOfCreatePost, contentField3: originalMediaBucketName, contentField4: myChatsData.communityImageUrl!, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: myChatsData.communityName!, contentField8: thumbMediaBucketName, contentField9: contentField9, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)

            }else{
                createSendMessageRequest  = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: StringConstants.EMPTY, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: String(mediaSize), contentField9: contentField9, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
                print("createSendMessageRequest",createSendMessageRequest)
                
                createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: mediaName, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: String(mediaSize), contentField9: contentField9, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
            }
            
            // Make nill object
            appDelegate.strCaptionText = ""
            appDelegate.selctedImage = UIImage()
            appDelegate.selectedVideoURL = nil
            appDelegate.captureAudioUrl = nil
            appDelegate.locationTitleAddress = ""
            appDelegate.locationDetailddress = ""
            appDelegate.longitudeOfSendLocation = ""
            appDelegate.latitudeOfSendLocation = ""
            appDelegate.strCreatePost = ""
            appDelegate.selectedImageOfPostCreation = UIImage()
            appDelegate.selectedVideoOfPostCreation = nil
            //appDelegate
            
            //send message at server DB
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: strUUID, communityJabberId: myChatsData.communityJabberId!)
            }
            
            // Show message on same screen
            let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            // messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            setTblScrollAtBottom()
            tblChannelSendBroadcastMsgList.reloadData()
            //Save message in core data as local DB with pending status
            let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
            
            if checkInternetConnection == true{
                self.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityJabberId: myChatsData.communityJabberId!)
            }
            txtViewOfChatMessage.text = StringConstants.EMPTY
        }
    }
    
    
    func PassMultiMediaFail(_ errorMsg: String) {
        MBProgressHUD.hide(for: self.view, animated: true);
        self.popupAlert(title: "", message: errorMsg, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    func setTblScrollAtBottom()  {

        let when = DispatchTime.now() + 0.000001 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.messageList.count  > 0 {
                self.scrollToBottom()
            }
        }
        
    }
    
    func scrollToBottom(){
        let indexPath = IndexPath(row: self.messageList.count - 1, section: 0)
        if self.messageList.count > indexPath.row {
            // self.tblChannelSendBroadcastMsgList.scrollToRow(at: indexPath, at: .bottom, animated: false)
            tblChannelSendBroadcastMsgList.re.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
        
    }
    
    func scrollToLoadMorePossition(messagesCount:Int){
        let count  = self.messageList.count - messagesCount
        if count > 0 {
            if self.messageList.count > count - 1  {
                let indexPath = IndexPath(row: count - 1, section: 0)
                // self.tblChannelSendBroadcastMsgList.scrollToRow(at: indexPath, at: .bottom, animated: false)
                tblChannelSendBroadcastMsgList.re.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    @objc func dismissPopUp() {
        self.view.endEditing(true)
        txtViewOfChatMessage.resignFirstResponder()
        ViewOfBacktblMenuPopUp.isHidden = true
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
    }
    
    @IBAction func btnCaptureImgClick(_ sender: Any) {
        
    }
    
    @IBAction func btnSendMessageClick(_ sender: Any) {
        
        if myChatsData.ownerId == userId {
            txtViewOfChatMessage.text = txtViewOfChatMessage.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            let strChatMessage = txtViewOfChatMessage.text //StringUTFDecoding.UTFDecoding(string: txtViewOfChatMessage.text)
            
            if (txtViewOfChatMessage.text != StringConstants.CommunityType.txtPlaceHolderForTypeMessage) && !txtViewOfChatMessage.text.isEmpty{
                let text = txtViewOfChatMessage.text
                let types: NSTextCheckingResult.CheckingType = .link
                let detector = try? NSDataDetector(types: types.rawValue)
                guard let detect = detector else {
                    return
                }
                
                let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                if checkInternetConnection == true{
                    let matches = detect.matches(in: text!, options: .reportCompletion, range: NSMakeRange(0, text!.characters.count))
                    if matches.count > 0{
                        var urlString = String()
                        for match in matches {
                            print(match.url!)
                            urlString = match.url!.absoluteString
                        }
                        if let youTubeId = getYoutubeId(youtubeUrl: urlString) {
                            //call API
                            if youTubeId == StringConstants.EMPTY{
                                //If link is not a youtube link
                                //send Normal Message
                                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                            }else{
                                
                                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                                spinnerActivity.isUserInteractionEnabled = false
                                self.getDetailsFromYouTubeAPI(youTubeId:youTubeId,strChatMessage:strChatMessage!)
                            }
                        }
                    }else{
                        //send Normal Message
                        //if the message send by user does not contains a link
                        self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                    }
                }else{
                    //send Normal Message
                    //offline mode send link message as a text message
                    self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                }
            }
        }else{
            if myChatsData.isMember == StringConstants.ZERO {
                //User has been leave channel & allow to delete channel
            }
        }
        
    }
    
    
    
    func getDetailsFromYouTubeAPI(youTubeId:String,strChatMessage:String) {
        let params = ["":""]
        let baseUrl = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + youTubeId + "&key=AIzaSyARI7c1_rmH9yN6N8_dqgWtcOAcg6AGvcs"
        let apiURL = String(describing: baseUrl)
        _ = GetYoutubeurlDataService().getyoutubeurldata(apiURL,
                                                         postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetYoutubeurlDataResponse
                                                            let youTubeData = model.Youtubedata as [YoutubeGetData]
                                                            
                                                            if youTubeData.count > 0 {
                                                                let datadict = youTubeData[0]
                                                                print(datadict.title as Any)
                                                                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage,mediaType:StringConstants.MediaType.link,contentField2:datadict.thumnailImgUrl1!,contentField3:datadict.title!,contentField4:datadict.descreption!)
                                                            }else{
                                                                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)

                                                            }
                                                            
                                                            
                                                           
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }

    
    
    
    func sendTextMessageWithUrlMessage(strChatMessage:String,mediaType:String,contentField2:String,contentField3:String,contentField4:String){
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let messageKey = strUUID

        let createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
        print("createSendMessageRequest",createSendMessageRequest)
        
        createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending, communityName:  myChatsData.communityName!, communityProfileUrl:  myChatsData.communityImageBigThumbUrl!)
        
        // Show message on same screen
        var  messages = Messages()
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
            //Send Message to Server DB
            self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: strUUID, communityJabberId: self.myChatsData.communityJabberId!)
        }else{
            messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
        }
        
        //                messageList.append(messages)
        messageList.insert(messages, at: 0)
        messageListTemp = messageList
        // setTblScrollAtBottom()
        MBProgressHUD.hide(for: self.view, animated: true);
        tblChannelSendBroadcastMsgList.reloadData()
        //Save message in core data as local DB with pending status
        let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
        print("messageStatusTime",messagesDB.messageStatusTime!)
        MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
        
        if checkInternetConnection == true{
            self.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityJabberId:self.myChatsData.communityJabberId! )
        }
        txtViewOfChatMessage.text = StringConstants.EMPTY
        heightConstantOftxtViewOfChatMessage.constant = 33
        heightConstantOfViewBackChatTextview.constant = 53
        self.view.layoutIfNeeded()
        
    }
    
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        let url = youtubeUrl
        var youtubrID = String()
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            if ((match) != nil){
            let range = match?.range(at: 0)
            youtubrID = (url as NSString).substring(with: range!)
            print(youtubrID)
            }else{
                youtubrID = StringConstants.EMPTY
            }
        } catch {
            youtubrID = StringConstants.EMPTY
        }
        return youtubrID
    }
    

    func buildRequestForSendMessage(id: String, mediaType: String, messageType: String, contentField1: String, contentField2: String, contentField3: String, contentField4: String, contentField5: String, contentField6: String, contentField7: String, contentField8: String, contentField9: String, contentField10: String, senderId: String, communityId: String, jabberId: String, createdDatetime: String, hiddenDatetime: String, deletedDatetime: String, hiddenbyUserId: String, deletedbyUserId: String,messageStatus:String,communityName:String,communityProfileUrl:String)-> Dictionary<String, Any>{
        
        let  createSendMessageRequest = MessageRequest.convertToDictionary(sendMessage: MessageRequest.init(id: id, mediaType: mediaType, messageType: messageType, contentField1: contentField1, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: contentField5, contentField6: contentField6, contentField7: contentField7, contentField8: contentField8, contentField9: contentField9, contentField10: contentField10, senderId: senderId, communityId: communityId, jabberId: jabberId, createdDatetime: createdDatetime, hiddenDatetime: hiddenDatetime, deletedDatetime: deletedDatetime, hiddenbyUserId: hiddenbyUserId, deletedbyUserId: deletedbyUserId,messageStatus:messageStatus,communityName:communityName,communityProfileUrl:communityProfileUrl))
        print("createSendMessageRequest",createSendMessageRequest)
        return createSendMessageRequest
    }
    
    func groupChat(createSendMessageRequest:Dictionary<String, Any>,messageGuid:String,communityJabberId:String) {
        
       let host = URLConstant.hostName
        let serviceJID = XMPPJID(string: "pubsub.\(host)")
        print("serviceJID",serviceJID ?? String())
        let xmppPubSub = XMPPPubSub(serviceJID: serviceJID, dispatchQueue: DispatchQueue.main)
        xmppPubSub.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppPubSub.activate(appDelegate.xmppController.xmppStream)
        let body = XMLElement.element(withName: "title") as? XMLElement // New
        let from = XMLElement.element(withName: "from") as? XMLElement
        
        var userString = String()
        do{
            if let userOneData = try JSONSerialization.data(withJSONObject: createSendMessageRequest, options: JSONSerialization.WritingOptions.prettyPrinted) as Optional {
                let  jsonString = NSString(data: userOneData , encoding: String.Encoding.utf8.rawValue)! as String
                userString = jsonString.replacingOccurrences(of: "\\/", with: "/"
                    , options:  NSString.CompareOptions.caseInsensitive, range: nil)
                print("userString",userString)
            } }catch {
                print(error.localizedDescription)
        }
        
        let resource = RequestName.resourceName
        body?.stringValue = userString
        from?.stringValue = resource
        txtViewOfChatMessage.text = StringConstants.EMPTY
        let messageBody = XMLElement.element(withName: "book") as? XMLElement
        messageBody?.setXmlns("pubsub:test:book") //New
        messageBody?.addChild(body ?? XMLNode())
        messageBody?.addChild(from ?? XMLNode())
        print("messageBody",messageBody as Any)
        xmppPubSub.publish(toNode: communityJabberId, entry: messageBody!, withItemID: messageGuid, options: ["pubsub#access_model": "open"])
        
        // Message status  Use this code for acknowledge
        let xmppMessageDeliveryRecipts = XMPPMessageDeliveryReceipts(dispatchQueue: DispatchQueue.main)
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = true
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = true
        xmppMessageDeliveryRecipts.activate(appDelegate.xmppController.xmppStream)
    }
    
    func SendMessageInDB(createSendMessageRequest:Dictionary<String, Any>,messageuid:String,communityJabberId:String) {
        _ = RequestName.SendMessageInDB
        let params = SendMessageInDbRequest.convertToDictionary(sendInvitationRequest: SendMessageInDbRequest.init(message: createSendMessageRequest))
        print("Param:",params)

        let apiURL = URLConstant.BASE_URL + URLConstant.sendMessageInDB
        print("Api",apiURL)
        _ = SendMessageInDBService().SendMessageInDB(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! SendMessageInDbResponse
                                                        print(model)
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
        
    }
    
    func setTextview(){
        
        txtViewOfChatMessage.isScrollEnabled = false
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 999)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 999)
        heightConstantOftxtViewOfChatMessage.constant = 33
        heightConstantOfViewBackChatTextview.constant = 53
        self.view.layoutIfNeeded()
    }
    
    func initialDesign() {
        
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        txtViewOfChatMessage.returnKeyType = UIReturnKeyType.next
        
        tblChannelSendBroadcastMsgList.rowHeight = UITableViewAutomaticDimension
        tblChannelSendBroadcastMsgList.estimatedRowHeight = 90.0
        tblChannelSendBroadcastMsgList.tableFooterView = UIView()
        tblChannelSendBroadcastMsgList.separatorColor = UIColor.clear
        tblOfMenuOnPopup.separatorColor = UIColor.clear
        
        txtViewOfChatMessage.text = (NSLocalizedString(StringConstants.CommunityType.txtPlaceHolderForTypeMessage, comment: StringConstants.EMPTY))
        txtViewOfChatMessage.textColor = UIColor.lightGray
        txtViewOfChatMessage.selectedTextRange = txtViewOfChatMessage.textRange(from: txtViewOfChatMessage.beginningOfDocument, to: txtViewOfChatMessage.beginningOfDocument)
        self.tblChannelSendBroadcastMsgList.scrollsToTop = false
        
        if  UIScreen.main.bounds.size.height <= 568{
            lblOfLeaveMsgToast.font = UIFont.systemFont(ofSize: 12)
        }else{
            lblOfLeaveMsgToast.font = UIFont.systemFont(ofSize: 15)
        }
        
        // long press recognizer
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        //self.tblChannelSendBroadcastMsgList.addGestureRecognizer(longPressGesture)
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtViewOfChatMessage.inputAccessoryView = toolbar
        self.searchBar.inputAccessoryView = toolbar
        txtViewOfChatMessage.isScrollEnabled = false
    }
    
    
    
  @objc func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.tblChannelSendBroadcastMsgList)
        let indexPath = self.tblChannelSendBroadcastMsgList.indexPathForRow(at: p)
        longPressIndexPath = indexPath! as NSIndexPath
        let messageInfo = messageList[(longPressIndexPath?.row)!]
        if messageInfo.messageType != StringConstants.MessageType.feed && messageInfo.messageType != StringConstants.MessageType.post {
            ViewOfBacktblMenuPopUp.isHidden = true
            if indexPath == nil {
                print("Long press on table view, not row.")
            }
            else if (longPressGesture.state == UIGestureRecognizerState.began) {
                print("Long press on row, at \(indexPath!.row)")
                flagOfCheckLongPressRegonizer = true
                forwardedmessageDetails = messageList[(longPressIndexPath?.row)!]
                let strMessageType =  forwardedmessageDetails?.messageType
                var strMediaType = forwardedmessageDetails?.mediaType
                let cell = tblChannelSendBroadcastMsgList.cellForRow(at:indexPath!)
                cell!.backgroundColor = UIColor.gray
                if previouslongPressIndexPath == nil{
                    
                }else{
                    let cellRect = tblChannelSendBroadcastMsgList.rectForRow(at: previouslongPressIndexPath! as IndexPath)
                    let completelyVisible = tblChannelSendBroadcastMsgList.bounds.contains(cellRect)
                    if completelyVisible || (self.isRowZeroVisible(indexPath: previouslongPressIndexPath! as IndexPath)){
                        let cell1 = tblChannelSendBroadcastMsgList.cellForRow(at:previouslongPressIndexPath! as IndexPath)
                        if myChatsData.feed == StringConstants.ONE{
                            cell1!.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }else{
                            if let communityDominantColour = myChatsData.communityDominantColour{
                                cell1!.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                            }else{
                                cell1!.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                            }
                        }
                    }
                }
                previouslongPressIndexPath = longPressIndexPath
               // tblChannelSendBroadcastMsgList.reloadData()
                hideSearchBar()
                setNavigationBarAtScreenLoadTime()
                navigationItem.rightBarButtonItems = []
                if strMediaType == nil{
                    strMediaType = "Location"
                }
                self.addFuncOnNavForSelectedText(strMessageType: strMessageType!,strMediaType:strMediaType!)
            }
        }
    }
    
    
    //for checking the previously selected row is visible or not
    func isRowZeroVisible(indexPath:IndexPath) -> Bool {
        let indexes = tblChannelSendBroadcastMsgList.indexPathsForVisibleRows
        for index in indexes ?? [] {
            if index == indexPath {
                return true
            }
        }
        return false
    }

        func setLongPress(indexPath:IndexPath,longPressGesture:UILongPressGestureRecognizer){
        longPressIndexPath = indexPath as NSIndexPath
        ViewOfBacktblMenuPopUp.isHidden = true
        if indexPath == nil {
            print("Long press on table view, not row.")
        }else if (longPressGesture.state == UIGestureRecognizerState.began) {
            print("Long press on row, at \(indexPath.row)")
            flagOfCheckLongPressRegonizer = true
            forwardedmessageDetails = messageList[(longPressIndexPath?.row)!]
            let strMessageType =  forwardedmessageDetails?.messageType
            var strMediaType = forwardedmessageDetails?.mediaType
            let cell = tblChannelSendBroadcastMsgList.cellForRow(at:indexPath)
            cell!.backgroundColor = UIColor.gray
            tblChannelSendBroadcastMsgList.reloadData()
            hideSearchBar()
            setNavigationBarAtScreenLoadTime()
            navigationItem.rightBarButtonItems = []
            if strMediaType == nil{
                strMediaType = "Location"
            }
            self.addFuncOnNavForSelectedText(strMessageType: strMessageType!,strMediaType:strMediaType!)
        }
    }
    
    func addFuncOnNavForSelectedText(strMessageType:String,strMediaType:String) {
        
        let messageInfo = messageList[(longPressIndexPath?.row)!]
        navView.isHidden = true
        let btnHelp   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.helpNav), style: .plain, target: self, action: #selector(btnHelpClick))
        let btnCopy = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.copy), style: .plain, target: self, action: #selector(btnCopyClick))
        let btnForword   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.forword), style: .plain, target: self, action: #selector(btnForwordClick))
        let btnDelete = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.delete), style: .plain, target: self, action: #selector(btnDeleteClick))
        let btnHideUnHide = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.hide), style: .plain, target: self, action: #selector(btnHideUnHideClick))
        let btnReply = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.reply), style: .plain, target: self, action: #selector(btnReplyClick))
        
        btnHelp.tintColor = UIColor.white
        btnCopy.tintColor = UIColor.white
        btnForword.tintColor = UIColor.white
        btnDelete.tintColor = UIColor.white
        btnHideUnHide.tintColor = UIColor.white
        btnReply.tintColor = UIColor.white
        
        if strMessageType == StringConstants.MessageType.chat && strMediaType == ""{
                navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,*/btnForword,btnDelete]
            }else{
                if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.image{
                    
                    let messageId = messageInfo.id
                    let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                    print(isExist)
                    if isExist{
                        print("yes exist")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    }else{
                        if myChatsData.ownerId == userId {
                           navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                            print("yes exist same user")
                        }else{
                            print("not downloaded")
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                        }
                    }
                }
                    
                else if strMessageType == StringConstants.MessageType.chatMessage && (strMediaType  == StringConstants.EMPTY || strMediaType == StringConstants.MediaType.text){
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,*/btnCopy,btnForword,btnDelete]
                }
                else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.audio{
                    //new code for undownloaded media
                    let messageId = messageInfo.id
                    let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
                    print(isExist)
                    if isExist{
                         navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        print("yes exist")
                    }else{
                        if myChatsData.ownerId == userId {
                            print("yes exist same user")
                             navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        }else{
                            print("not downloaded")
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                        }
                    }
                }
                else  if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.video{
                    
                    let messageId = messageInfo.id
                    self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                    let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                    print("cellpath:,\(videoNameFrmDB)")
                    
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
                    print(isExist)
                    if isExist{
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    }else{
                        if myChatsData.ownerId == userId {
                            print("yes exist same user")
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        }else{
                            print("not downloaded")
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                        }
                    }
                }else if messageInfo.mediaType == StringConstants.MediaType.document{
                    
                    let messageId = messageInfo.id
                    let contentField9 =  messageInfo.contentField9!
                    var imageName1 = String()
                    if  contentField9 == doc {
                        imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                    }else{
                        imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
                    }
                    print("imageName1",imageName1)
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                    print("isExist",isExist)
                    if isExist{
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        print("yes exist")
                    }else{
                        if myChatsData.ownerId == userId {
                            print("yes exist same user")
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        }else{
                            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                            print("not downloaded")
                        }
                    }
                
                }else{
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                }
            }
    }
    
    @objc func btnHelpClick(sender: UIBarButtonItem) {
        print("btnHelpClick")
        let indexPath = IndexPath(item: (longPressIndexPath?.row)!, section: 0)
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil

        tblChannelSendBroadcastMsgList.reloadRows(at: [indexPath], with: .none)
        self.setTblScrollAtBottom()
        let forwordMessageVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageInfoVC") as! MessageInfoVC
        self.navigationController?.pushViewController(forwordMessageVC, animated: true)
    }
    
    @objc func btnCopyClick(sender: UIBarButtonItem) {
        print("btnCopyClick")
        let board = UIPasteboard.general
        let messageData = messageList[(longPressIndexPath?.row)!]
        board.string = messageData.contentField1
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil
        self.tblChannelSendBroadcastMsgList.reloadData()
        self.setNavigationBarAtScreenLoadTime()
    }
    
    @objc func btnForwordClick(sender: UIBarButtonItem) {
        print("btnForwordClick")
        let indxValue = self.longPressIndexPath
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil
        flagOfCheckLongPressRegonizer = false
        let indexPath = IndexPath(item: (indxValue?.row)!, section: 0)
        tblChannelSendBroadcastMsgList.reloadRows(at: [indexPath], with: .none)
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        dashboardViewController.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
        dashboardViewController.forwardedmessageData = forwardedmessageDetails
        self.navigationController?.pushViewController(dashboardViewController, animated: false)
    }
    
    @objc func btnDeleteClick(sender: UIBarButtonItem) {
        print("btnDeleteClick")
        popupAlert(title: "", message: (NSLocalizedString(StringConstants.AlertMessage.deleteMsgConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
                if self.myChatsData.ownerId == self.userId{  // Admin
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.deleteMessage(messageDetails: self.messageList[(self.longPressIndexPath?.row)!])
                }else{
                    self.deleteMessageLocally()
                }
            }, nil])
    }
    
    func deleteMessageLocally()  {
        if self.messageList.count > self.longPressIndexPath!.row  {
            let messageInfo = self.messageList[(self.longPressIndexPath?.row)!]
            let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: self.messageList[(self.longPressIndexPath?.row)!], messageId: messageInfo.id!, communityKey: self.communityKey)
            print(status)
            print("delete locally")
            self.messageList.remove(at: (self.longPressIndexPath?.row)!)
            self.messageListTemp.remove(at: (self.longPressIndexPath?.row)!)
            self.longPressIndexPath = nil
            self.previouslongPressIndexPath = nil
            MyChatServices.sharedInstance.fetchFirstMessageOnCommunityKey(communityKey: self.communityKey)
            self.tblChannelSendBroadcastMsgList.reloadData()

            //  self.setTblScrollAtBottom()
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.deleteConfirmation, comment: StringConstants.EMPTY)))
        }
        self.setNavigationBarAtScreenLoadTime()
        
    }
    
    @objc func btnHideUnHideClick(sender: UIBarButtonItem) {
        print("btnHideUnHideClick")
    }
    
    @objc func btnReplyClick(sender: UIBarButtonItem) {
        print("btnReplyClick")
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
        if txtViewOfChatMessage.text.isEmpty || txtViewOfChatMessage.text == nil || txtViewOfChatMessage.text == StringConstants.CommunityType.txtPlaceHolderForTypeMessage {
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGrayBtn), for: .normal)
            btnSendMessage.isEnabled = false
        }else{
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGreenBtn), for: .normal)
            btnSendMessage.isEnabled = true
        }
    }
    
    func setNavigationBarAtScreenLoadTime(){
        // Create a navView to add to the navigation bar
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navView.isHidden = false
        
        if myChatsData.ownerId == userId {
            if UIScreen.main.bounds.size.height <= 568{
                navView = UIView(frame: CGRect(x: 0, y: 0, width: 205, height: 50))
            }else{
                navView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
            }
        }else{
            navView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
        }
        // Create the image view
        var imageOfNavTitle = UIImageView()
        imageOfNavTitle = UIImageView(frame: CGRect(x: -10, y:5, width: 35, height: 35))
       
        if let channelProfilePic = myChatsData.communityImageSmallThumbUrl {
            if channelProfilePic == StringConstants.EMPTY{
                if myChatsData.feed == StringConstants.ONE{
                    imageOfNavTitle.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                }else{
                    imageOfNavTitle.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                }
            }else{
                let imgurl = channelProfilePic
                let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                imageOfNavTitle.sd_setImage(with: URL(string: rep2), placeholderImage:UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
            }
        }
        imageOfNavTitle.layer.cornerRadius = imageOfNavTitle.frame.height / 2
        imageOfNavTitle.clipsToBounds = true
        
        // Create the label
        var lblOfNavTitle = UILabel()
        if myChatsData.ownerId == userId {
            if UIScreen.main.bounds.size.height <= 568{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 102, height: 21))
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
            }else{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 150, height: 21))
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(16)
            }
        }else{
            lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 150, height: 21))
            lblOfNavTitle.font = lblOfNavTitle.font.withSize(16)
        }
        lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
        lblOfNavTitle.text = StringUTFEncoding.UTFEncong(string:communityName)
        lblOfNavTitle.textColor = UIColor.white
        lblOfNavTitle.textAlignment = NSTextAlignment.left
        navView.addSubview(lblOfNavTitle)
        navView.addSubview(imageOfNavTitle)
        // Set the navigation bar's navigation item's titleView to the navView
        self.navigationItem.titleView = navView
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChannelSendBroadcastMsgVC.navTitleClick))
        navView.addGestureRecognizer(tap)
        
        if myChatsData.ownerId == userId {
            let moreOptionButton   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.moreOptions), style: .plain, target: self, action: #selector(btnMoreOptionClick))
            let attachMentButton = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.attachFile), style: .plain, target: self, action: #selector(btnAttachmentClick))
            moreOptionButton.tintColor = UIColor.white
            attachMentButton.tintColor = UIColor.white
            navigationItem.rightBarButtonItems = [/*moreOptionButton*/attachMentButton]
        }else{
            navigationItem.rightBarButtonItems = []
        }
        
        self.navigationController?.navigationBar.isTranslucent = true
        
        // For channel admin broadcast
        if myChatsData.feed == StringConstants.ONE{
            viewOfBackSendMsg.isHidden = true
            btnOneToOneChat.isHidden = true
            bottomOfTblChannelBroadCastMsg.constant = -55
            self.view.layoutIfNeeded()
        }else{
            
            if myChatsData.ownerId == userId {
                viewOfBackSendMsg.isHidden = false
                btnOneToOneChat.isHidden = true
                bottomOfTblChannelBroadCastMsg.constant = 2
                txtViewOfChatMessage.isHidden = false
                lblOfLeaveMsgToast.isHidden = true
                viewOfBackSendMsg.backgroundColor = UIColor.white
                btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGrayBtn), for: .normal)
                btnSendMessage.isEnabled = false
                btnSendMessage.isHidden = false
                
            }else{
                //If user has leave channel
                if myChatsData.isMember == StringConstants.ZERO {
                    viewOfBackSendMsg.isHidden = false
                    btnOneToOneChat.isHidden = true
                    bottomOfTblChannelBroadCastMsg.constant = 2
                    txtViewOfChatMessage.isHidden = true
                    lblOfLeaveMsgToast.isHidden = false
                    viewOfBackSendMsg.backgroundColor = UIColor.clear
                    btnSendMessage.setImage(UIImage(named: StringConstants.EMPTY), for: .normal)
                    let image = UIImage(named:  StringConstants.ImageNames.baseline_delete_black_18dp)
                    btnSendMessage.setBackgroundImage(image, for: .normal)
                    btnSendMessage.isHidden = true
                }else{
                    viewOfBackSendMsg.isHidden = true
                    btnOneToOneChat.isHidden = false
                    bottomOfTblChannelBroadCastMsg.constant = -55
                    btnSendMessage.isHidden = false
                }
                self.view.layoutIfNeeded()
            }
            
        }
        //Remove this code for oneTone chat messages
        btnOneToOneChat.isHidden = true
    }
    
    @objc func navTitleClick() {
        print("navTitleClick")
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        ViewOfBacktblMenuPopUp.isHidden = true
        let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
        channelProfileVC.myChatsData = myChatsData
        self.navigationController?.pushViewController(channelProfileVC, animated: true)
    }
    
    @objc func btnMoreOptionClick(sender: UIBarButtonItem) {
        print("More")
        txtViewOfChatMessage.resignFirstResponder()
        if flagOfClickOnMoreOption == false{
            flagOfClickOnMoreOption = true
            ViewOfBacktblMenuPopUp.isHidden = false
            flagOfCheckPopup = StringConstants.flags.moreoptions
            tblOfMenuOnPopup.reloadData()
        }else{
            flagOfClickOnMoreOption = false
            ViewOfBacktblMenuPopUp.isHidden = true
        }
        flagOfClickOnAttachmentOption = false
    }
    
    
    @objc func btnAttachmentClick(sender: UIBarButtonItem) {
        print("flagOfClickOnAttachmentOption",flagOfClickOnAttachmentOption)
        
        //Hide pip mode video
        if strToggle == StringConstants.flags.toggle{
            strToggle = StringConstants.EMPTY
            videoPlayer.handleCloseTapped((Any).self)
        }
        
        txtViewOfChatMessage.resignFirstResponder()
        if flagOfClickOnAttachmentOption == false{
            flagOfClickOnAttachmentOption = true
            ViewOfBacktblMenuPopUp.isHidden = false
            flagOfCheckPopup = StringConstants.flags.attachment
            tblOfMenuOnPopup.reloadData()
        }else{
            flagOfClickOnAttachmentOption = false
            ViewOfBacktblMenuPopUp.isHidden = true
        }
        flagOfClickOnMoreOption = false
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        
        //Hide pip mode video
        if strToggle == StringConstants.flags.toggle{
            strToggle = StringConstants.EMPTY
            videoPlayer.handleCloseTapped((Any).self)
        }
        
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC {
            commingFrom = StringConstants.EMPTY
            let dashboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            _ = dashboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is ChannelDashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                //nav.pushViewController(vc, animated: false)
                return
            }
            
        }else if commingFrom == StringConstants.flags.SponseredDetailCOntroller{
            commingFrom = StringConstants.EMPTY
            let dashboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            _ = dashboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is DashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                //nav.pushViewController(vc, animated: false)
                return
            }
        }else{
            UserDefaults.standard.set(false, forKey: "MessageDeleteStatus")
            self.navigationController?.popViewController(animated: true)
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelSendBroadcastMsgVC.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelSendBroadcastMsgVC.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK:- Notification
    @objc func keyboardWillShow(_ notification: Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            //self.buttomLayoutConstraint = keyboardFrame.size.height
            self.bottomOfmessageComposingView.constant = keyboardFrame.size.height
            let when = DispatchTime.now() + 0.007 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.messageList.count > 0 {
                    self.setTblScrollAtBottom()
                }
            }
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
            if self.messageList.count > 0 {
                self.setTblScrollAtBottom()
            }
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.bottomOfmessageComposingView.constant = 0.0
            
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
            self.setTblScrollAtBottom()
            
        })
    }
    
    
    @IBAction func btnOneToOneChatClick(_ sender: Any) {
        //Hide pip mode video
        if strToggle == StringConstants.flags.toggle{
            strToggle = StringConstants.EMPTY
            videoPlayer.handleCloseTapped((Any).self)
        }
        
        let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
        oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: communityName)
        oneToOneFromAdminVC.communityKey = communityKey
        oneToOneFromAdminVC.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        oneToOneFromAdminVC.dominantColour = dominantColour
        oneToOneFromAdminVC.ownerId = SessionDetailsForCommunityType.shared.globalMyChatsData.ownerId! //myChatsData.ownerId!
        oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
        self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)
    }
    
    @IBAction func btnNewMessageClick(_ sender: Any) {
        isFirstRowIsVisible = true
        btnNewMessage.isHidden = true
        setTblScrollAtBottom()
        tblChannelSendBroadcastMsgList.reloadData()
    }
    
    //pick doc and pdf file from documents
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        var strconfirmation = StringConstants.EMPTY
        if preferredLanguage == "en" {
             strconfirmation = (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY)) /*+ myChatsData.communityName!*/
        } else if preferredLanguage == "mr" {
            strconfirmation =  (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY))
        }else{
            strconfirmation = /*myChatsData.communityName! +*/ (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY))
        }
        popupAlert(title: "", message:strconfirmation , actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnCancle, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnConfirm, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
                do{
                    self.pdfDocData = try NSData(contentsOf: myURL, options: .mappedIfSafe) as Data
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    
                    self.messageKeyForSendMultiMedia = strUUID
                    self.messageTimeForSendMultiMedia = String(currentTime)
                    //Send original image to Amzon S3
                    self.originalMediaBucketName = S3BucketName + channelMedia + self.communityKey + "/" + documents + "/" + self.messageKeyForSendMultiMedia
                    print("self.originalMediaBucketName",self.originalMediaBucketName)
                    let uploadMediaPartToS3  = UploadMediaPartToS3()
                    uploadMediaPartToS3.delegate = self
                    //Selected document name
                    print("document name",myURL.lastPathComponent)
                    self.documentName = myURL.lastPathComponent
                    //Save Image at docoument directory  pdfPrfiex
                    var docName = String()
                    if myURL.pathExtension == pdf {
                        //Generate PDF Thumb data (Used third party for generate thubnail of pdf)
                        let thumbnail = ROThumbnail.sharedInstance
                        let documentURL = myURL
                        let pdfThumbImage = thumbnail.getThumbnail(documentURL)
                        self.pdfThumbData = UIImagePNGRepresentation(pdfThumbImage) ?? Data()
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + pdfPrfiex  + String(self.messageTimeForSendMultiMedia)
                         docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(self.messageKeyForSendMultiMedia) + ".pdf"
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: StringConstants.MediaType.documentspdf,imageData:self.pdfDocData as NSData)
                        let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + ".pdf"
                        uploadMediaPartToS3.uploadImage(with: self.pdfDocData as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:true,contentType:pdfContentType,uploadKeyName:uploadKeyName)
                    }else{
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + documentPrefix + String(self.messageTimeForSendMultiMedia)
                        let documentType = myURL.pathExtension
                        docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(self.messageKeyForSendMultiMedia) + "." + documentType
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: documentType,imageData:self.pdfDocData as NSData)
                        let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + "." + documentType
                        uploadMediaPartToS3.uploadImage(with: self.pdfDocData as Data, bucketName:self.originalMediaBucketName,mediaSource:documentType,callAgain:true,contentType:docContentType,uploadKeyName: uploadKeyName)
                        
                    }
                    
                }catch{
                }
                
            }, nil])
        
        
      
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}


extension ChannelSendBroadcastMsgVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblChannelSendBroadcastMsgList{
            return messageList.count
        }else{
            if flagOfCheckPopup == StringConstants.flags.moreoptions{
                return arrOfMenuList.count
            }else if  flagOfCheckPopup == StringConstants.flags.attachment{
                return arrOfAttachmentList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblChannelSendBroadcastMsgList{
           
           let messageInfo = messageList[indexPath.row]
            if messageInfo.messageType == StringConstants.MessageType.chatMessage && (messageInfo.mediaType  == StringConstants.EMPTY || messageInfo.mediaType  ==  StringConstants.MessageType.text ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelSendBroadcastMsgCell", for: indexPath) as! ChannelSendBroadcastMsgCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                if dominantColour.isEmpty {
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }else{
                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
                }
                // cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                cell.txtViewOfMessageText.text = StringConstants.EMPTY
                cell.txtViewOfMessageText.text = messageInfo.contentField1! //StringUTFEncoding.UTFEncong(string:messageInfo.contentField1! )
                var msgtext = messageInfo.contentField1!
                msgtext = msgtext.lowercased()
                if msgtext.contains(URLConstant.http) ||  msgtext.contains(URLConstant.https){
                    cell.txtViewOfMessageText.isSelectable = true
                }else{
                    cell.txtViewOfMessageText.isSelectable = false
                }
                cell.txtViewOfMessageText.dataDetectorTypes = UIDataDetectorTypes.link
                cell.txtViewOfMessageText.linkTextAttributes = [ NSAttributedStringKey.foregroundColor.rawValue: UIColor.init(hexString: ColorConstants.colorPrimaryString) ]
                cell.txtViewOfMessageText.delegate = self
               
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = strTime

                if (cell.txtViewOfMessageText.text?.isEmpty)! ||  cell.txtViewOfMessageText.text == nil{
                    cell.txtViewOfMessageText.isHidden = true
                    cell.heightOfViewOnCell.priority = UILayoutPriority(rawValue: 999)
                    cell.heightOfViewOnCell.constant = 50
                }else{
                    cell.txtViewOfMessageText.isHidden = false
                    cell.heightOfViewOnCell.priority = UILayoutPriority(rawValue: 250)
                }
                if myChatsData.ownerId == userId {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfViewBackChatMsg.constant = 100 //65
                    cell.rightConstantOfViewBackChatMsg.constant = 15
                    if messageInfo.messageStatus == StringConstants.Status.sent || messageInfo.messageStatus == StringConstants.EMPTY || messageInfo.messageStatus == nil{
                        cell.imageViewOfStatus.isHidden = true
                        cell.widthOfStatusImageView.constant = 0
                    }else{
                        cell.imageViewOfStatus.isHidden = false
                        cell.widthOfStatusImageView.constant = 15
                        cell.imageViewOfStatus.image = UIImage(named: StringConstants.ImageNames.clock)
                        cell.lblOfDateTime?.text = StringConstants.EMPTY

                    }
                } else {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfViewBackChatMsg.constant = 15
                    cell.rightConstantOfViewBackChatMsg.constant = 100 //65
                    cell.imageViewOfStatus.isHidden = true
                    cell.widthOfStatusImageView.constant = 0
                }
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                        }else{
                            
                            cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                
                return cell
                
            }else if  messageInfo.messageType == StringConstants.MessageType.chatMessage && messageInfo.mediaType  == StringConstants.MediaType.audio {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetAudioMediaPartCell", for: indexPath) as! SetAudioMediaPartCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }else{
                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
                }
                cell.btnAudioPlay.tag = indexPath.row + 10
                cell.btnAudioPlay.addTarget(self,action:#selector(btnPlayAudio(sender:)), for: .touchUpInside)
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }

               // let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = strTime
                
               // let imageName = messageInfo.contentField4
                //new code
               // let messageTime = messageInfo.createdDatetime
                let messageId = messageInfo.id
                let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
                print(isExist)
                if isExist
                {
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)
                    
                }else{
                    if myChatsData.ownerId == userId {
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)
                    }else{
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download)
                    }
                }
                
               /* if !(imageName?.isEmpty)!{
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)
                }else{
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download)
                }*/
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                        cell.backgroundColor =  UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                        }else{
                            cell.backgroundColor =  UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                if myChatsData.ownerId == userId {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.widthConstantOfStatusImg.constant  = 0//19
                    cell.leftConstantOfBackViewOfBubleImage.constant = 100
                    cell.rightConstantOfBackViewOfBubleImage.constant = 15
                    cell.imageViewOfStatus.isHidden = false
                } else {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.widthConstantOfStatusImg.constant  = 0
                    cell.leftConstantOfBackViewOfBubleImage.constant = 15
                    cell.rightConstantOfBackViewOfBubleImage.constant = 100
                    cell.imageViewOfStatus.isHidden = true
                }
                
              return cell
                
            }else if messageInfo.messageType == StringConstants.MessageType.mapLocation /*&& messageInfo.mediaType == StringConstants.EMPTY*/ {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetLocationPartCell", for: indexPath) as! SetLocationPartCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }else{
                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
                }
                cell.lblTitleAddress.text = messageInfo.contentField4
                cell.lblDetailAddress.text = messageInfo.contentField5
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = strTime
                
                cell.btnOpenGoogleMap.tag = indexPath.row + 10
                cell.btnOpenGoogleMap.addTarget(self,action:#selector(btnOpenGooleMap(sender:)), for: .touchUpInside)
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                        }else{
                            cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                
                if myChatsData.ownerId == userId {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.widthOfStatusImage.constant  = 0 //19
                    cell.leftConstantViewOfBackBubbleImg.constant = 100
                    cell.rightConstantViewOfBackBubbleImg.constant = 15
                    cell.imageViewOfStastus.isHidden = false
                } else {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.widthOfStatusImage.constant  = 0
                    cell.leftConstantViewOfBackBubbleImg.constant = 15
                    cell.rightConstantViewOfBackBubbleImg.constant = 100
                    cell.imageViewOfStastus.isHidden = true
                }
                //color
                return cell
            }else if messageInfo.messageType == StringConstants.MessageType.feed {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "RSSFeedTblCell", for: indexPath) as! RSSFeedTblCell
                cell.selectionStyle = .none
                
                //cell view corner radious
                cell.viewOnCell.layer.cornerRadius = 8
                cell.viewOnCell.clipsToBounds = true
                
                
                cell.lblReadMore.text = (NSLocalizedString(StringConstants.rssFeed.readMore, comment: StringConstants.EMPTY))
                cell.btnreadMore.addTarget(self,action:#selector(btnReadMOre(sender:)), for: .touchUpInside)
                cell.btnreadMore.tag = indexPath.row

                if let contentField5 = messageInfo.contentField5{
                    cell.lblSourceName.text = contentField5
                }else{
                    cell.lblSourceName.text = StringConstants.EMPTY
                }
                
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                  cell.lblFeedTime.isHidden = false
                cell.lblFeedTime?.text = strTime
                if let contentField1 = messageInfo.contentField1{
                    cell.lblFeedTitle.text = contentField1 /*StringUTFEncoding.UTFEncong(string:contentField1)*/
                }else{
                    cell.lblFeedTitle.text = StringConstants.EMPTY
                }
                
                cell.imgSourceIMage.layer.cornerRadius = cell.imgSourceIMage.frame.size.width / 2
                cell.imgSourceIMage.clipsToBounds = true
                
                if let contentField2 = messageInfo.contentField2{
                    //cell.lblFeedMsg.text = StringUTFEncoding.UTFEncong(string:contentField2Value)
                    cell.lblFeedMsg.text = contentField2
                }else{
                    cell.lblFeedMsg.text = StringConstants.EMPTY
                }
                if let contentField6 = messageInfo.contentField6{
                    let contentField3Value = contentField6.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    cell.imgSourceIMage.sd_setImage(with: URL(string: contentField3Value ?? String()), placeholderImage:UIImage(named: StringConstants.ImageNames.rssFeedIcon), options: .refreshCached)

                }else{
                    cell.imgSourceIMage.image = UIImage(named: StringConstants.ImageNames.rssFeedIcon)
                }
                
                
                if let contentField3 = messageInfo.contentField3{
                    let contentField3Value = contentField3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    cell.imageViewOfFeed.sd_setImage(with: URL(string: contentField3Value ?? String()), placeholderImage:UIImage(named: StringConstants.ImageNames.rssFeedDefault), options: .refreshCached)
                }else{
                    cell.imageViewOfFeed.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
                }
                
                cell.backgroundColor =  UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                
                return cell
            }else if messageInfo.messageType == StringConstants.MessageType.post{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostMesaageViewCell", for: indexPath) as! PostMesaageViewCell
                cell.videIcon.isHidden = true
                cell.txtviewOfPostDescription.text = messageInfo.contentField2
                cell.lblOfChannelName.text = messageInfo.contentField7
                
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
               // let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.text  = strTime
                cell.lblPOstTitl.text = messageInfo.contentField1
                
                cell.ViewOncell.layer.cornerRadius = 8
                cell.ViewOncell.clipsToBounds = true
                cell.imgChannelProfile.layer.cornerRadius = cell.imgChannelProfile.frame.size.width / 2
                cell.imgChannelProfile.clipsToBounds = true
                cell.selectionStyle = .none
                cell.btnPromotePost.setTitle((NSLocalizedString(StringConstants.Validation.promotepost, comment: StringConstants.EMPTY)), for: .normal)
                //channel profile image
                let imgurl = messageInfo.contentField4
                let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                let url = NSURL(string: rep2)
                cell.imgChannelProfile.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                
                //postimageOrVideo
                if messageInfo.mediaType == StringConstants.MediaType.video{
                    cell.videIcon.isHidden = false
                    cell.btnPlayVideo.isHidden = false
                    if let imgurl = messageInfo.contentField8 {
                        let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                        let url = NSURL(string: rep2)
                        //cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.rssFeedDefault), options: .refreshCached)
                        let imageView = UIImageView()
                        imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let size = CGSize(width: 248, height: 248)
                        let imageHelper = ImageHelper()
                        cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
                    }else{
                        cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
                    }
                }else{
                    cell.videIcon.isHidden = true
                    cell.btnPlayVideo.isHidden = true
                    if let imgurl = messageInfo.contentField3 {
                        let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                        let url = NSURL(string: rep2)
                        //cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.rssFeedDefault), options: .refreshCached)
                        let imageView = UIImageView()
                        imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let size = CGSize(width: 248, height: 248)
                        let imageHelper = ImageHelper()
                        cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
                    }else{
                        cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
                    }
                }
                cell.btnPlayVideo.tag = indexPath.row + 10
                cell.btnPlayVideo.addTarget(self,action:#selector(btnPlayVideoFromPost(sender:)), for: .touchUpInside)
                
                cell.btnPromotePost.tag = indexPath.row
                cell.btnPromotePost.addTarget(self,action:#selector(btnPramotrPostClicked(sender:)), for: .touchUpInside)

                if myChatsData.feed == StringConstants.ONE{
                    cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                }else{
                    if let communityDominantColour = myChatsData.communityDominantColour{
                        cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                    }else{
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }
                }
                
                if myChatsData.ownerId == userId {
                    /*cell.heightConstantOfBtnPromoPost.constant = 38
                    cell.btnPromotePost.isHidden = false
                    cell.lblOfSeprator.isHidden = false*/
                    cell.heightConstantOfBtnPromoPost.constant = 0
                    cell.heightOfViewOfBackRequestCallBack.constant = 0
                    cell.btnPromotePost.isHidden = true
                    cell.viewOfBackRequestCallBack.isHidden = true
                    cell.lblOfSeprator.isHidden = true
                    cell.ViewOncell.backgroundColor = UIColor(red: 254.0/255.0, green: 253.0/255.0, blue: 199.0/255.0, alpha: 1.0)
                }else{
                    cell.heightConstantOfBtnPromoPost.constant = 0
                    cell.heightOfViewOfBackRequestCallBack.constant = 0
                    cell.btnPromotePost.isHidden = true
                    cell.viewOfBackRequestCallBack.isHidden = true
                    cell.lblOfSeprator.isHidden = true
                    cell.ViewOncell.backgroundColor = UIColor.white
                }
                
                if  UIScreen.main.bounds.size.height <= 568{
                    cell.HeightConstraintForPostimage.constant = 200
                }
                return cell
            }else if messageInfo.mediaType == StringConstants.MediaType.link  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PIPViewCell", for: indexPath) as! PIPViewCell
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.text = strTime
                if let communityDominantColour = myChatsData.communityDominantColour{
                    cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                }else{
                    cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                }
                cell.txtViewOfLink.text = messageInfo.contentField1
                cell.lblOfTitle.text = messageInfo.contentField3
                cell.lblOfDescription.text = messageInfo.contentField4
                
                //channel profile image
                if let imgurl = messageInfo.contentField2{
                    let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                    let url = NSURL(string: rep2)
                    cell.imageViewOfThumb.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                }else{
                    
                }
                 let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                if myChatsData.ownerId == userId {
                    cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leadingOfViewOnCell.constant  = 100
                    cell.trailingOfViewOnCell.constant = 17
                    cell.leadingOfThumbImg.constant = 5
                    cell.trailingOfthumbImage.constant = 10
                    cell.leadingOfDescriptionView.constant = 0
                    
                } else {
                    cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leadingOfViewOnCell.constant  = 17
                    cell.trailingOfViewOnCell.constant = 100
                    cell.leadingOfThumbImg.constant = 10
                    cell.trailingOfthumbImage.constant = 5
                    cell.leadingOfDescriptionView.constant = 10
                    
                }
                
                //Open video in PIP mode
                cell.btnOpenVideoInPipModeClick.tag = indexPath.row + 10
                cell.btnOpenVideoInPipModeClick.addTarget(self,action:#selector(btnOpenVideoInPIPMode(sender:)), for: .touchUpInside)
                //--------------------------------------------------------------------//
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                        }else{
                            
                            cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                return cell
                
            } else if messageInfo.mediaType == StringConstants.MediaType.image || messageInfo.mediaType == StringConstants.MediaType.video {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetCameraMediaPartCell", for: indexPath) as! SetCameraMediaPartCell
                cell.selectionStyle = .none
                if messageInfo.mediaType == StringConstants.MediaType.image{
                    //new code
                    //let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id
                    let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                    print(isExist)
                    if isExist{
                        let image = documentDirectoryHelper.getImage(imageName: imageName1)
                        cell.imgViewOfSelectedImage.image = nil
                        cell.imgViewOfSelectedImage.image = image
                        cell.imageViewOfShowMediaStatus.isHidden = true
                        // New code ------------
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.EMPTY)
                        //----------------
                        cell.btnPlayVideo.isHidden = false
                        // cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.image_icon)
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                    }else{
                        
                        let imgurl = messageInfo.contentField3
                        let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                        let url = NSURL(string: rep2)
                        cell.imgViewOfSelectedImage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        
                        //New code ------------
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download_white_36dp)
                        //----------------
                        cell.btnPlayVideo.isHidden = false
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.image_icon)
                        
                        if myChatsData.ownerId == userId {
                            cell.imageViewOfShowMediaStatus.isHidden = true
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                            
                        }else{
                            cell.imageViewOfShowMediaStatus.isHidden = false
                        }
                    }
                    
                }else{
                    _ = messageInfo.contentField3
                    cell.imgViewOfSelectedImage.image = UIImage(named:StringConstants.EMPTY)
                    cell.imgViewOfSelectedImage.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage: UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                    
                    // new code
                    //let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id
                    self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                    let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                    print("cellpath:,\(videoNameFrmDB)")
                    
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
                    print(isExist)
                    if isExist
                    {
                        cell.imageViewOfShowMediaStatus.isHidden = false
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_play_circle_outline_white)
                        cell.btnPlayVideo.isHidden = false
                        //cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                    }else{
                        
                        if myChatsData.ownerId == userId {
                            cell.imageViewOfShowMediaStatus.isHidden = false
                            cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_play_circle_outline_white)
                            cell.btnPlayVideo.isHidden = false
                            //cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                        }else{
                            cell.btnPlayVideo.isHidden = false
                            //New code ------------
                            cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download_white_36dp)
                            //----------------
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                        }
                    }
                }
                // ------------------------- Download & Open image and Download video & Play video --------------- //
                cell.btnPlayVideo.tag = indexPath.row + 10
                cell.btnPlayVideo.addTarget(self,action:#selector(btnPlayVideo(sender:)), for: .touchUpInside)
                // -----------------------------------------//----------------------------------------------- //
                
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                
                if let captiontxt = messageInfo.contentField1{
                    if captiontxt == StringConstants.null{
                        cell.lblCaptionText.text = StringConstants.EMPTY
                    }else{
                        cell.lblCaptionText.text = StringUTFEncoding.UTFEncong(string:messageInfo.contentField1! )
                    }
                }else{
                    cell.lblCaptionText.text = StringConstants.EMPTY
                }
                
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = strTime
                
                if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }else{
                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
                }
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                        }else{
                            cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                
                if myChatsData.ownerId == userId {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfBubbleImgView.constant = 97
                    cell.rightConstantOfBubbleImgView.constant = 12
                    cell.widthOfStatusImg.constant = 0 //19
                    cell.imageViewOfSatus.isHidden = false
                    cell.imageViewOfShowMediaType.isHidden = true
                    //Set selected image in bubble chat box
                    cell.rightConstantOfSelectedImage.constant = 8
                    cell.leftConstantOfSelectedImage.constant = 2
                    
                } else {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfBubbleImgView.constant = 12
                    cell.rightConstantOfBubbleImgView.constant = 97
                    ;           cell.imageViewOfSatus.isHidden = true
                    cell.widthOfStatusImg.constant = 0
                    cell.imageViewOfShowMediaType.isHidden = false
                    //Set selected image in bubble chat box
                    cell.rightConstantOfSelectedImage.constant = 2
                    cell.leftConstantOfSelectedImage.constant = 8
                }
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetPDFDOCCell", for: indexPath) as! SetPDFDOCCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                var strTime = String()
                if let time = messageInfo.createdDatetime{
                    if time == StringConstants.EMPTY{
                    }else{
                        strTime = DateTimeHelper.getTimeAgo(time: String(describing: time))
                    }
                }
                //let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime?.text = strTime
                let contentField9 =  messageInfo.contentField9!
                
                if let contentField1 = messageInfo.contentField1 {
                    cell.lblOfMediaName.text =  contentField1
                }else{
                    cell.lblOfMediaName.text = StringConstants.EMPTY
                }
                
                if  contentField9 == doc ||  contentField9 == docx{
                    if myChatsData.ownerId == userId {
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfMediaTypeImage.constant = 8
                        cell.leftConstantOfMediaTypeImage.constant = 3
                        cell.imgeViewOfDownloadIcon.isHidden = true
                        cell.btnofDownloadMedia.isHidden = false
                        //For Thumbnail Image
                        cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                        cell.documentTypeImageOnBanerImg.isHidden = false
                        cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.docwhite70)
                        
                    }else{
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfMediaTypeImage.constant = 3
                        cell.leftConstantOfMediaTypeImage.constant = 8
                        cell.btnofDownloadMedia.isHidden = false
                        let messageId = messageInfo.id
                        var imageName1 = ""
                        if  contentField9 == doc{
                            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                        }else{
                            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
                        }
                        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                        print(isExist)
                        if isExist{
                            cell.imgeViewOfDownloadIcon.isHidden = true
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = false
                            cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.docwhite70)
                            
                        }else{
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.imgeViewOfDownloadIcon.isHidden = false
                            cell.documentTypeImageOnBanerImg.isHidden = true
                        }
                    }
                    cell.lblOfMediaName.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                    cell.documentTypeImageView.image = nil
                    cell.documentTypeImageView.image = UIImage(named: StringConstants.ImageNames.docRedIcon)
                }else{
                    
                    if myChatsData.ownerId == userId {
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfMediaTypeImage.constant = 8
                        cell.leftConstantOfMediaTypeImage.constant = 3
                        cell.imgeViewOfDownloadIcon.isHidden = true
                        cell.btnofDownloadMedia.isHidden = false
                        //  cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                        //  cell.imageViewOfThumbOfMedia.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        
                        //For thumbnail
                        cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                        cell.documentTypeImageOnBanerImg.isHidden = false
                        cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.pdfWhite70)
                        
                    }else{
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfMediaTypeImage.constant = 3
                        cell.leftConstantOfMediaTypeImage.constant = 8
                        //cell.imageViewOfThumbOfMedia.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        cell.btnofDownloadMedia.isHidden = false
                        let messageId = messageInfo.id
                        let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
                        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                        print(isExist)
                        if isExist{
                            cell.imgeViewOfDownloadIcon.isHidden = true
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = false
                            cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.pdfWhite70)
                        }else{
                            cell.imgeViewOfDownloadIcon.isHidden = false
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = true
                        }
                    }
                    cell.lblOfMediaName.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                    cell.documentTypeImageView.image = nil
                    cell.documentTypeImageView.image = UIImage(named: StringConstants.ImageNames.pdfRedIcon)
                }
                cell.btnofDownloadMedia.tag = indexPath.row + 10
                cell.btnofDownloadMedia.addTarget(self,action:#selector(btnOpenDocument(sender:)), for: .touchUpInside)
    
                if longPressIndexPath?.row == indexPath.row{
                    cell.viewOnCell.backgroundColor = UIColor.gray
                }else{
                    if myChatsData.feed == StringConstants.ONE{
                       // cell.viewOnCell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                    }else{
                        if let communityDominantColour = myChatsData.communityDominantColour{
                           // cell.viewOnCell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                            cell.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
                            
                        }else{
                            //cell.viewOnCell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                            cell.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                        }
                    }
                }
                
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
            cell.selectionStyle = .none
            if flagOfCheckPopup == StringConstants.flags.moreoptions{
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImages[indexPath.row])
                cell.lblMenuTitle.text = StringUTFEncoding.UTFEncong(string: arrOfMenuList[indexPath.row])
                cell.imageViewOfOption.isHidden = true
                cell.widthOfMenuImageView.constant = 0
            }else if  flagOfCheckPopup == StringConstants.flags.attachment{
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImages[indexPath.row])
                cell.lblMenuTitle.text = StringUTFEncoding.UTFEncong(string: arrOfAttachmentList[indexPath.row])
                if arrOfAttachmentList[indexPath.row] == (NSLocalizedString(StringConstants.popUpMenuName.doc, comment: StringConstants.EMPTY)){
                    cell.widthOfMenuImageView.constant = 25
                    cell.heightOfOptionMenuImg.constant = 25
                    cell.leadingOfOptinMenuImgView.constant = 15
                    cell.topOfOptionImageView.constant = 11

                }else if arrOfAttachmentList[indexPath.row] == (NSLocalizedString(StringConstants.popUpMenuName.post, comment: StringConstants.EMPTY)){
                    cell.widthOfMenuImageView.constant = 22
                    cell.heightOfOptionMenuImg.constant = 22
                    cell.leadingOfOptinMenuImgView.constant = 18
                    cell.topOfOptionImageView.constant = 11
                
                }else{
                    cell.widthOfMenuImageView.constant = 30
                    cell.heightOfOptionMenuImg.constant = 30
                    cell.leadingOfOptinMenuImgView.constant = 10
                    cell.topOfOptionImageView.constant = 6
                }
                cell.imageViewOfOption.isHidden = false
            }
            
            return cell
        }
    }
    
    @objc func btnOpenVideoInPIPMode(sender:UIButton){
        
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        
        let text = messageInfo.contentField1
        let types: NSTextCheckingResult.CheckingType = .link
        let detector = try? NSDataDetector(types: types.rawValue)
        guard let detect = detector else {
            return
        }
        let matches = detect.matches(in: text!, options: .reportCompletion, range: NSMakeRange(0, text!.characters.count))
        if matches.count > 0{
            var urlString = String()
            for match in matches {
                print(match.url!)
                urlString = match.url!.absoluteString
            }
            if let youTubeId = getYoutubeId(youtubeUrl: urlString) {
                //call API
                videoPlayer.showVideoUrl(in: self.view,youTubeId:youTubeId)
                strToggle = StringConstants.flags.toggle
            }
        }
    }
    
    @objc func btnOpenDocument(sender:UIButton){
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        
        if messageInfo.mediaType == StringConstants.MediaType.document{
            let messageId = messageInfo.id
            let contentField9 =  messageInfo.contentField9!
            var imageName1 = String()
            if  contentField9 == doc || contentField9 == docx{
                if contentField9 == doc{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                }else{
                    imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
                }
            }else{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
            }
            print("imageName1",imageName1)
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            print("isExist",isExist)
            if isExist{
                if flagOfCheckLongPressRegonizer == true{
                    flagOfCheckLongPressRegonizer = false
                    longPressIndexPath = nil
                    tblChannelSendBroadcastMsgList.reloadData()
                    // Set Navigation Bar
                    if flagForCheckOpenSerachBar == false{
                        self.setNavigationBarAtScreenLoadTime()
                    }else{
                        addSearchBar()
                    }
                }else{
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.openPDFInFullScreen(messageInfo:messageInfo)
                }
            }else{
                if myChatsData.ownerId == userId {
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.getOpenDocument(messageInfo: messageInfo, indexPath: indexPath ?? IndexPath())
                }else{
                    DispatchQueue.main.async {
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                    }
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.getOpenDocument(messageInfo: messageInfo, indexPath: indexPath ?? IndexPath())
                }
                
            }
        }
    }
    
    func getOpenDocument(messageInfo:Messages,indexPath:IndexPath){
        let keys = Array(messageInfo.entity.attributesByName.keys)
        var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
        DispatchQueue.global(qos: .background).async {
            let messageId = messageInfo.id
            var imageName = String()
            let contentField9 =  messageInfo.contentField9!
            if  contentField9 == doc || contentField9 == docx{
                if  contentField9 == doc{
                imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                }else{
                     imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
                }
            }else{
                imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
            }
            print("Directoryname:,\(imageName)")
            let messageUrl = messageInfo.contentField2
            //let url = URL(string:messageUrl!)
            let imgurl = messageUrl
            let finalurl = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)

            if let url = URL(string:finalurl){
                if let imgData = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imgData as NSData)
                        messagedict.updateValue(imageName, forKey: "contentField4")
                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                        // Update local mediaLocalPath value
                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                        self.messageList.remove(at: (indexPath.row))
                        self.messageList.insert(messages, at: (indexPath.row))
                        self.messageListTemp.remove(at: (indexPath.row))
                        self.messageListTemp.insert(messages, at: (indexPath.row))
                        let indexPathValue = IndexPath(item: (indexPath.row), section: 0)
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                            if self.myChatsData.ownerId == self.userId {
                                let messageInfo = self.messageList[(indexPath.row)]
                                self.openPDFInFullScreen(messageInfo:messageInfo)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func openPDFInFullScreen(messageInfo:Messages){
        
        let messageId = messageInfo.id
        let contentField9 =  messageInfo.contentField9!
        var imageName1 = String()
        if  contentField9 == doc || contentField9 == docx{
            if  contentField9 == doc{
            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
            }else{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
            }
        }else{
            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
        }
        
        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
        print(isExist)
        let fullViewDocumentFileVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewDocumentFileVC") as! FullViewDocumentFileVC
        
        fullViewDocumentFileVC.documentDataFromLocal = imageName1
        fullViewDocumentFileVC.documentDataFromServer = StringConstants.EMPTY
        
        if  contentField9 == doc || contentField9 == docx{
            if  contentField9 == doc{
                fullViewDocumentFileVC.mediaType = ".doc"
            }else{
                fullViewDocumentFileVC.mediaType = ".docx"
            }
        }else{
            fullViewDocumentFileVC.mediaType = ".pdf"
        }
        
        self.navigationController?.pushViewController(fullViewDocumentFileVC, animated: false)
    }
    
    
    
    @objc func btnOpenGooleMap(sender:UIButton){
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        let messageInfo = messageList[(indexPath?.row)!]
        let lat = messageInfo.contentField2
        let lon = messageInfo.contentField3
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            if let latvalue = lat, let latitude = Double(latvalue),let longvalue = lon, let longitude = Double(longvalue) {
                UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&zoom=14&views=traffic&q=loc:\(latitude),\(longitude)&directionsmode=driving")!, options: [:], completionHandler: nil)
            }
        } else {
            print("Can't use comgooglemaps://")
            if let latvalue = lat, let latitude = Double(latvalue),let longvalue = lon, let longitude = Double(longvalue) {
                UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(latitude),\(longitude)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
            }
        }
    }
    
    
    @objc func btnPlayAudio(sender:UIButton){
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        let messageInfo = messageList[(indexPath?.row)!]
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
        var audioURL = NSURL()
       // let audioName = messageInfo.contentField4
        //new code -----
        //let messageTime = messageInfo.createdDatetime
        let messageId = messageInfo.id
        let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
        print(isExist)
        if isExist
        {
            audioURL = documentDirectoryHelper.getVideoPath(videoName:audioNameForDB) as NSURL
            print("audioURL",audioURL)
            let player = AVPlayer(url: audioURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            
            if myChatsData.ownerId == userId {
                let audiourlstr =  messageInfo.contentField2
                audioURL = NSURL(string:audiourlstr!)!
                    let keys = Array(messageInfo.entity.attributesByName.keys)
                    var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
                    //Save audio at docoument directory
                    //let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id
                    let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                    let messageUrl = messageInfo.contentField2
                    let url = URL(string:messageUrl!)
                    if let videoData = try? Data(contentsOf: url!){
                        documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioNameForDB, sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                        messagedict.updateValue(audioNameForDB, forKey: "contentField4")
                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                        // Update local mediaLocalPath value
                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                        messageList.remove(at: (indexPath?.row)!)
                        messageList.insert(messages, at: (indexPath?.row)!)
                        messageListTemp.remove(at: (indexPath?.row)!)
                        messageListTemp.insert(messages, at: (indexPath?.row)!)
                        let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                        tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                    }
                    
                    let player = AVPlayer(url: audioURL as URL)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                
                }
            }else{
                
                let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                if checkInternetConnection == true{
                    let audiourlstr =  messageInfo.contentField2
                    audioURL = NSURL(string:audiourlstr!)!
                    let keys = Array(messageInfo.entity.attributesByName.keys)
                    var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
                    //Save audio at docoument directory
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                    DispatchQueue.global(qos: .background).async {
                        let messageId = messageInfo.id
                        let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                        let messageUrl = messageInfo.contentField2
                        let url = URL(string:messageUrl!)
                        if let videoData = try? Data(contentsOf: url!){
                            DispatchQueue.main.async {
                                self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioNameForDB, sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                                messagedict.updateValue(audioNameForDB, forKey: "contentField4")
                                MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                // Update local mediaLocalPath value
                                let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                self.messageList.remove(at: (indexPath?.row)!)
                                self.messageList.insert(messages, at: (indexPath?.row)!)
                                self.messageListTemp.remove(at: (indexPath?.row)!)
                                self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                MBProgressHUD.hide(for: self.view, animated: true);
                                self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                            }
                        }
                    }
                }else{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                }
            }
    }
    }
    
    func openImageInFullScreen(messageInfo:Messages){
        //Open image in full screen
        if messageInfo.mediaType == StringConstants.MediaType.image{
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            fullViewOfImageFromMessagesVC.channelName = communityName
            /*if let contentField4 = messageInfo.contentField4{
                fullViewOfImageFromMessagesVC.imageDataFromLocalDB = contentField4
            }*/
            let messageId = messageInfo.id
            let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            print(isExist)
            if let contentField3 = messageInfo.contentField3{
                fullViewOfImageFromMessagesVC.imageDataFromServer = contentField3
            }
            
            if let messageText = messageInfo.contentField1{
                if messageText == StringConstants.null{
                    fullViewOfImageFromMessagesVC.captionText  = StringConstants.EMPTY
                }else{
                    fullViewOfImageFromMessagesVC.captionText  = messageText
                }
            }else{
                fullViewOfImageFromMessagesVC.captionText  = StringConstants.EMPTY
            }
            
            if isExist{
                fullViewOfImageFromMessagesVC.imageDataFromLocalDB = imageName1
                self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
            }else{
                if userId == messageInfo.senderId{
                    fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.contentField3!
                    fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                }else{
                    if messageInfo.messageType != StringConstants.MediaType.post{
                        fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    }else{
                        fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.contentField3!
                        fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                        self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                    }
                }
            }
        }
    }
    
    @objc func btnPramotrPostClicked(sender:UIButton){
        self.popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.Validation.promotepostPopUptext, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    
    @objc func btnReadMOre(sender:UIButton){
        let messageInfo = messageList[sender.tag]
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let rssFeedDetailVC = storyboard.instantiateViewController(withIdentifier: "RSSFeedDetailVC") as! RSSFeedDetailVC
        rssFeedDetailVC.rssFeedDetails = messageInfo
        self.navigationController?.pushViewController(rssFeedDetailVC, animated: true)
    }
    
    
    @objc func btnPlayVideoFromPost(sender:UIButton){
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        let keys = Array(messageInfo.entity.attributesByName.keys)
        var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
        let messageId = messageInfo.id
        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
        let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
        print(isExist)
        if isExist
        {    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            var videoURL = NSURL()
            
            if isExist{
                videoURL = documentDirectoryHelper.getVideoPath(videoName:videoNameFrmDB) as NSURL
            }else{
                let imgurl = messageInfo.contentField3
                let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                videoURL = NSURL(string: rep2)!
            }
            let player = AVPlayer(url: videoURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            let messageUrl = messageInfo.contentField3
            let imgurl = messageInfo.contentField3
            let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            videoURL = NSURL(string: rep2)! as URL
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
               playerViewController.player!.play()
            }
           DispatchQueue.global(qos: .background).async {
                if let url = URL(string:messageUrl!){
                    if let videoData = try? Data(contentsOf: url){
                        DispatchQueue.main.async {
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                        messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                        // Update local mediaLocalPath value
                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                            self.messageList.remove(at: (indexPath?.row)!)
                            self.messageList.insert(messages, at: (indexPath?.row)!)
                            self.messageListTemp.remove(at: (indexPath?.row)!)
                            self.self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                            let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                            self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                        }
                    }
                }
            }
        }
    }
    
    @objc func btnPlayVideo(sender:UIButton){
        let point = tblChannelSendBroadcastMsgList.convert(sender.center, from: sender.superview)
        let indexPath = tblChannelSendBroadcastMsgList.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        let keys = Array(messageInfo.entity.attributesByName.keys)
        var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
        
        if messageInfo.mediaType == StringConstants.MediaType.image{
            
            //new code
            //let messageTime = messageInfo.createdDatetime
            let messageId = messageInfo.id
            let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            if isExist{
                if flagOfCheckLongPressRegonizer == true{
                    flagOfCheckLongPressRegonizer = false
                    longPressIndexPath = nil
                    tblChannelSendBroadcastMsgList.reloadData()
                    // Set Navigation Bar
                    if flagForCheckOpenSerachBar == false{
                        self.setNavigationBarAtScreenLoadTime()
                    }else{
                        addSearchBar()
                    }
                }else{
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.openImageInFullScreen(messageInfo:messageInfo)
                }
            }else{
                
                if myChatsData.ownerId == userId {
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.openImageInFullScreen(messageInfo:messageInfo)
                }else{
                    let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                    if checkInternetConnection == true{
                        DispatchQueue.main.async {
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                        }
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                        //let messageTime = messageInfo.createdDatetime
                        DispatchQueue.global(qos: .background).async {
                            let messageId = messageInfo.id
                            let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                            print("Directoryname:,\(imageName)")
                            let messageUrl = messageInfo.contentField2
                            let imgurl = messageUrl
                            let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)

                            //let url = URL(string:messageUrl!)
                            if let url = URL(string:rep2){
                                if let imgData = try? Data(contentsOf: url){
                                    DispatchQueue.main.async {
                                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imgData as NSData)
                                        messagedict.updateValue(imageName, forKey: "contentField4")
                                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                        // Update local mediaLocalPath value
                                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                        self.messageList.remove(at: (indexPath?.row)!)
                                        self.messageList.insert(messages, at: (indexPath?.row)!)
                                        self.messageListTemp.remove(at: (indexPath?.row)!)
                                        self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                        let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                        DispatchQueue.main.async {
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                                        }
                                    }
                                }
                            }/*else{
                             MBProgressHUD.hide(for: self.view, animated: true);
                             self.navigationController?.view.makeToast(StringConstants.ToastViewComments.imageNotFormat)
                             }*/
                        }
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }
            }
            
        }else{
            //New Code------
            //let messageTime = messageInfo.createdDatetime
            let messageId = messageInfo.id
            self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
            let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
            print(isExist)
            if isExist
            {    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
                var videoURL = NSURL()

                if isExist{
                    videoURL = documentDirectoryHelper.getVideoPath(videoName:videoNameFrmDB) as NSURL
                }else{
                    let imgurl = messageInfo.contentField2
                    let rep2 = imgurl!.replacingOccurrences(of:" ", with: "%20")
                    videoURL = NSURL(string: rep2)!
                }
                let player = AVPlayer(url: videoURL as URL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else{
                
                if myChatsData.ownerId == userId {
                    
                    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
                    var videoURL = NSURL()
                    
                    if isExist{
                        videoURL = documentDirectoryHelper.getVideoPath(videoName:videoNameFrmDB) as NSURL
                    }else{
                        let imgurl = messageInfo.contentField2
                        let rep2 = imgurl!.replacingOccurrences(of:" ", with: "%20")
                        videoURL = NSURL(string: rep2)!
                    }
                    
                    let messageUrl = messageInfo.contentField2
                    if let url = URL(string:messageUrl!){
                        if let videoData = try? Data(contentsOf: url){
                            documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                            messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                            MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                            // Update local mediaLocalPath value
                            let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                            messageList.remove(at: (indexPath?.row)!)
                            messageList.insert(messages, at: (indexPath?.row)!)
                            messageListTemp.remove(at: (indexPath?.row)!)
                            messageListTemp.insert(messages, at: (indexPath?.row)!)
                            let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true);
                                self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                            }
                        }
                        let player = AVPlayer(url: videoURL as URL)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                        
                    }
                }else{
                    let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                    if checkInternetConnection == true{
                        DispatchQueue.main.async {
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                        }
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                        DispatchQueue.global(qos: .background).async {
                            let messageId = messageInfo.id
                            self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                            let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                            //let messageUrl = messageInfo.contentField2
                            let imgurl = messageInfo.contentField2
                            let rep2 = imgurl!.replacingOccurrences(of:" ", with: "%20")
                            if let url = URL(string:rep2){
                                if let videoData = try? Data(contentsOf: url) {
                                    DispatchQueue.main.async {
                                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                                        messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                        // Update local mediaLocalPath value
                                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                        self.messageList.remove(at: (indexPath?.row)!)
                                        self.messageList.insert(messages, at: (indexPath?.row)!)
                                        self.messageListTemp.remove(at: (indexPath?.row)!)
                                        self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                        let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        self.tblChannelSendBroadcastMsgList.reloadRows(at: [indexPathValue], with: .none)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))

                    }
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOfMenuOnPopup{
            ViewOfBacktblMenuPopUp.isHidden = true
            flagOfClickOnMoreOption = false
            flagOfClickOnAttachmentOption = false
            
            if  flagOfCheckPopup == StringConstants.flags.attachment{
                let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                if indexPath.row == 0{
                    if checkInternetConnection == true {
                        let selectMultiMediaPopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectMultiMediaPopUpVC") as! SelectMultiMediaPopUpVC
                        selectMultiMediaPopUpVC.modalPresentationStyle = .overCurrentContext
                        let backgroundScreenShot = captureScreen()
                        selectMultiMediaPopUpVC.checkMediaType = "Camera"
                        selectMultiMediaPopUpVC.backroundImage = backgroundScreenShot!
                        selectMultiMediaPopUpVC.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                        self.navigationController?.pushViewController(selectMultiMediaPopUpVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 1{
                    if checkInternetConnection == true {
                        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            imagePickerForOnlyImages.delegate = self
                            imagePickerForOnlyImages.sourceType = .savedPhotosAlbum
                            imagePickerForOnlyImages.allowsEditing = false
                            self.present(imagePickerForOnlyImages, animated: true, completion: nil)
                        }
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 2{
                    if checkInternetConnection == true {
                        imagePicker.sourceType = .savedPhotosAlbum
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = [kUTTypeMovie as String]
                        present(imagePicker, animated: true, completion: nil)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))                    }
                }/*else if indexPath.row == 3{
                    if checkInternetConnection == true {
                        let selectMultiMediaPopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectMultiMediaPopUpVC") as! SelectMultiMediaPopUpVC
                        selectMultiMediaPopUpVC.modalPresentationStyle = .overCurrentContext
                        let backgroundScreenShot = captureScreen()
                        selectMultiMediaPopUpVC.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                        selectMultiMediaPopUpVC.checkMediaType = "Audio"
                        selectMultiMediaPopUpVC.backroundImage = backgroundScreenShot!
                        self.navigationController?.pushViewController(selectMultiMediaPopUpVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 4 {
                    if checkInternetConnection == true {
                        let selectLocForSendLocVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocForSendLocVC") as! SelectLocForSendLocVC
                        selectLocForSendLocVC.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                        selectLocForSendLocVC.strchannelname = StringUTFEncoding.UTFEncong(string:communityName)
                        print(StringUTFEncoding.UTFEncong(string:communityName))
                        selectLocForSendLocVC.myChatsData = self.myChatsData
                        self.navigationController?.pushViewController(selectLocForSendLocVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }*/else if indexPath.row == 3 {
                    if checkInternetConnection == true {
                        let pdf                                 = String(kUTTypePDF)
                        let docs                                = String(kUTTypeCompositeContent)
                        let types                               = [pdf, docs]
                        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
                        documentPicker.delegate = self
                        documentPicker.modalPresentationStyle = .formSheet
                        self.present(documentPicker, animated: true, completion: nil)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }/*else if indexPath.row == 6 {
                    if checkInternetConnection == true {
                        let createpostVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                        createpostVC.myChatsData = myChatsData
                        self.navigationController?.pushViewController(createpostVC, animated: true)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }*/
            }else if flagOfCheckPopup == StringConstants.flags.moreoptions{
                
                if indexPath.row == 0{
                    flagOfClickOnMoreOption = false
                    flagOfClickOnAttachmentOption = false
                    ViewOfBacktblMenuPopUp.isHidden = true
                    let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
                    channelProfileVC.myChatsData = myChatsData
                    self.navigationController?.pushViewController(channelProfileVC, animated: true)
                }else if indexPath.row == 1 {
                    addSearchBar()
                }else if indexPath.row == 2 {
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let selectMembersVC = storyboard.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                    selectMembersVC.strComingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                    self.navigationController?.pushViewController(selectMembersVC, animated: true)
                }else if indexPath.row == 3 {
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    if (myChatsData.isMuted?.isEmpty)!{
                        self.muteAndUnMuteChannel(muteStatus:StringConstants.TRUE,communityKey:self.myChatsData.communityKey!)
                    }else{
                        self.muteAndUnMuteChannel(muteStatus:StringConstants.FALSE,communityKey:self.myChatsData.communityKey!)
                    }
                }
            }
        }else{
            
            let messageInfo = messageList[indexPath.row]
            if messageInfo.messageType == StringConstants.MessageType.feed {
//                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
//                let rssFeedDetailVC = storyboard.instantiateViewController(withIdentifier: "RSSFeedDetailVC") as! RSSFeedDetailVC
//                rssFeedDetailVC.rssFeedDetails = messageInfo
//                self.navigationController?.pushViewController(rssFeedDetailVC, animated: true)
            }else{
                if flagOfCheckLongPressRegonizer == true{
                    flagOfCheckLongPressRegonizer = false
                    longPressIndexPath = nil
                    tblChannelSendBroadcastMsgList.reloadData()
                    // Set Navigation Bar
                    if flagForCheckOpenSerachBar == false{
                        self.setNavigationBarAtScreenLoadTime()
                    }else{
                        addSearchBar()
                    }
                }else{
                    print("Selected Image")
                    let messageInfo = messageList[(indexPath.row)]
                    //Open image in full screen
                    self.openImageInFullScreen(messageInfo:messageInfo)
                }
            }
        }
    }
    
    func muteAndUnMuteChannel(muteStatus:String,communityKey:String){
        let params = MuteUnMuteRequest.convertToDictionary(muteUnMuteDetails: MuteUnMuteRequest.init(muteStatus: muteStatus, communityKey: communityKey))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.muteUnmute + communityKey
        _ = MuteUnMuteServices().muteUnMuteServices(apiURL,
                                                    postData: params as [String : AnyObject],
                                                    withSuccessHandler: { (userModel) in
                                                        let model = userModel as! MuteUnMuteResponse
                                                        print("model",model)
                                                        
                                                        let communityDetailsOnCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                        var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: communityDetailsOnCommunityKey)
                                                        if muteStatus == StringConstants.FALSE{
                                                            dict.updateValue(StringConstants.EMPTY, forKey: "isMuted")
                                                            MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                                                            self.arrOfMenuList.remove(at: 3)
                                                            self.arrOfMenuList.insert(StringConstants.EnglishConstant.Mute, at: 3)
                                                            self.navigationController?.view.makeToast(StringConstants.ToastViewComments.unMute)
                                                            let mycCHat = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                            self.myChatsData = mycCHat[0]
                                                        }else{
                                                            dict.updateValue(muteStatus, forKey: "isMuted")
                                                            MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                                                            self.arrOfMenuList.remove(at: 3)
                                                            self.arrOfMenuList.insert(StringConstants.EnglishConstant.UnMute, at: 3)
                                                            self.navigationController?.view.makeToast(StringConstants.ToastViewComments.mute)
                                                            let mycCHat = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
                                                            self.myChatsData = mycCHat[0]
                                                        }
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        self.tblOfMenuOnPopup.reloadData()
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    func addSearchBar()  {
        navView.isHidden = true
        flagForCheckOpenSerachBar = true
        searchBar.placeholder = (NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.becomeFirstResponder()
        searchBar.sizeToFit()
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
        
        
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        searchBar.delegate = self
        btnCloseSearch = UIButton(frame: CGRect(x: self.searchBar.frame.origin.x  + self.searchBar.frame.size.width, y: 5, width: 40, height: 25))
        btnCloseSearch  = UIButton(type: .custom)
        btnCloseSearch.setImage(UIImage(named: StringConstants.ImageNames.closeSearchIcon), for: .normal)
        btnCloseSearch.addTarget(self, action: #selector(btnCloseSearchClick), for: .touchUpInside)
        navigationItem.rightBarButtonItems = []
        let rightNavBarButton = UIBarButtonItem(customView:btnCloseSearch)
        self.navigationItem.rightBarButtonItem = rightNavBarButton
    }
    
    
    @objc func btnCloseSearchClick(sender: UIButton!) {
        hideSearchBar()
        flagForCheckOpenSerachBar = false
        sender.isHidden = true
        setNavigationBarAtScreenLoadTime()
        self.messageList = self.messageListTemp
        tblChannelSendBroadcastMsgList.reloadData()
        self.setTblScrollAtBottom()
    }
    
    func hideSearchBar(){
        btnCloseSearch.isHidden = true
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.removeFromSuperview()
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] as? UIWindow
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
}


extension ChannelSendBroadcastMsgVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true
        
        let currentText = textView.text
        let updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        let trimmedString = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if updatedText.isEmpty {
            
            textView.text = ""
            txtViewOfChatMessage.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGrayBtn), for: .normal)
            btnSendMessage.isEnabled = false
            return false
        }else if txtViewOfChatMessage.textColor == UIColor.lightGray && !text.isEmpty && trimmedString != "" && !trimmedString.isEmpty && trimmedString != StringConstants.CommunityType.txtPlaceHolderForTypeMessage  {
            textView.text = nil
            txtViewOfChatMessage.textColor = UIColor.black
            btnSendMessage.isEnabled = true
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGreenBtn), for: .normal)
        }
        if !trimmedString.isEmpty {
            btnSendMessage.isEnabled = true
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGreenBtn), for: .normal)
        }
        
        //Line number limitation for message textview
        var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        let boundingRect = sizeOfString(string: updatedText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        
        if numberOfLines >= 5  {
            txtViewOfChatMessage.isScrollEnabled = true
        } else {
            txtViewOfChatMessage.isScrollEnabled = false

        }
        
        return true
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedStringKey.font: font],
                                                 context: nil).size
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        ViewOfBacktblMenuPopUp.isHidden = true
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        setTblScrollAtBottom()
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if textView.text.isEmpty {
            textView.text = StringConstants.CommunityType.txtPlaceHolderForTypeMessage
            textView.textColor = UIColor.lightGray
            txtViewOfChatMessage.resignFirstResponder()
            txtViewOfChatMessage.isScrollEnabled = false
            heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.constant = 33
            heightConstantOfViewBackChatTextview.constant = 53
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView == self.txtViewOfChatMessage{
        }else{
        if !NSEqualRanges(textView.selectedRange, NSRange(location: 0, length: 0)) {
            textView.selectedRange = NSRange(location: 0, length: 0)
        }
      }
    }
}

extension ChannelSendBroadcastMsgVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        capturImage = UIImage()
        videoURL = nil
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                dismiss(animated: true, completion: nil)
                let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                setCaptionViewController.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                capturImage = image
                setCaptionViewController.selectedImage = capturImage
                setCaptionViewController.navigationTitle = StringUTFEncoding.UTFEncong(string:communityName)
                self.navigationController?.pushViewController(setCaptionViewController, animated: false)
            }else{
                videoURL = info[UIImagePickerControllerMediaURL] as? URL
                self.dismiss(animated: true, completion: nil)
                let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                setCaptionViewController.selectedVideoURL = videoURL
                setCaptionViewController.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
                setCaptionViewController.navigationTitle = StringUTFEncoding.UTFEncong(string:communityName)
                self.navigationController?.pushViewController(setCaptionViewController, animated: false)
            }
        }
    }
}

extension  ChannelSendBroadcastMsgVC :UISearchBarDelegate {
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        let searchText = String(searchBar.text!)
        self.filteredObjects = messageListTemp.filter { ($0.contentField1)?.range(of: searchText, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        if self.filteredObjects?.count == 0{
            messageList.removeAll()
            messageList = messageListTemp
        }else{
            messageList.removeAll()
            messageList = filteredObjects!
        }
        
        self.tblChannelSendBroadcastMsgList.reloadData()
        self.setTblScrollAtBottom()
    }
    
}


extension ChannelSendBroadcastMsgVC: XMPPStreamDelegate {
    
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage){
        print("group chat message",message)
        if message.childCount > 1{
            let createSendMessageRequest = XmppBroadCastMessageReceiveResponse.adminBroadCastMsgReceiveResponse(message: message)
            let messageData = createSendMessageRequest.0 as NSDictionary
            if let receivedMsgCommunityKey = messageData.value(forKey: "communityId") as? String {
                // let  from = createSendMessageRequest.1
                let  senderId = messageData.value(forKey: "senderId") as? String
                if receivedMsgCommunityKey == communityKey && userId !=  senderId /*from != RequestName.resourceName*/ {
                    let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest.0)
                    if messageList.contains(where: { $0.id == messages.id }){
                        print("Dont append")
                    }else{
                        // messageList.append(messages)
                        messageList.insert(messages, at: 0)
                        messageListTemp = messageList
                        if messageList.count < 10 {
                            isDataLoading = false
                        }
                    }
                    // Change unreadMessagesCount to zero
                    MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: communityKey)
                    //For show new message button
                    if userId == myChatsData.ownerId{
                        btnNewMessage.isHidden = true
                        setTblScrollAtBottom()
                        self.tblChannelSendBroadcastMsgList.reloadData()
                    }else{
                        if isFirstRowIsVisible == true{
                            setTblScrollAtBottom()
                            btnNewMessage.isHidden = true
                            self.tblChannelSendBroadcastMsgList.reloadData()
                        }else{
                            btnNewMessage.isHidden = false
                        }
                    }
                }else if SessionDetailsForCommunityType.shared.changePendingMessageStatus == true {
                    SessionDetailsForCommunityType.shared.changePendingMessageStatus = false
                    self.fetchOffSet = 0
                    let messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
                    // messages = messages.sorted(by: {$0.createdDatetime! < $1.createdDatetime!} )
                    messageList.removeAll()
                    messageListTemp.removeAll()
                    messageList = messages
                    messageListTemp = messages
                    if messages.count < 10 {
                        isDataLoading = false
                    }
                    tblChannelSendBroadcastMsgList.reloadData()
                }
            }
        }
    }
}

extension ChannelSendBroadcastMsgVC : XMPPPubSubDelegate{
    func xmppPubSub(_ sender: XMPPPubSub, didPublishToNode node: String, withResult iq: XMPPIQ) {
        createSendMessageRequestInDB.updateValue(StringConstants.Status.sent, forKey: "messageStatus")
        print("didPublishToNode",createSendMessageRequestInDB)
        //Update message in core data as local DB with sent status
        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: createSendMessageRequestInDB)
    }
    
    func xmppPubSub(_ sender: XMPPPubSub, didNotPublishToNode node: String, withError iq: XMPPIQ) {
        print("node",node)
    }
}


// ** Pavan
extension  ChannelSendBroadcastMsgVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblChannelSendBroadcastMsgList{
            if messageList.count > 0 {
                if (tblChannelSendBroadcastMsgList.indexPathsForVisibleRows?.count)! > 0{
                    if let firstVisibleIndexPath: IndexPath? = tblChannelSendBroadcastMsgList.indexPathsForVisibleRows?[0]{
                        // let firstVisibleIndexPath: IndexPath? = tblChannelSendBroadcastMsgList.indexPathsForVisibleRows?[0]
                        if firstVisibleIndexPath?.row == 0 {
                            isFirstRowIsVisible = true
                        }else{
                            isFirstRowIsVisible = false
                        }
                    }
                }
            }
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
       //  isDataLoading = false
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>){
        
        self.hideBtnMore()
        let endScrolling: Float = Float(scrollView.contentOffset.y + scrollView.frame.size.height)
        var scrollContentSize = Float(scrollView.contentSize.height)
        scrollContentSize = (scrollContentSize / 100) * 80
        
        if (scrollView is UITableView) {
            if velocity.y > 0 {
                if endScrolling >= scrollContentSize {
                    var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
                    if messages.count > 0 {
                        // check local database
                        let preveousCount = self.messageList.count
                        self.isDataLoading = messages.count > 0 ? true : false
                        self.page_number =   self.page_number + 1
                        self.fetchOffSet =   self.fetchOffSet + 10
                        for i in 0 ..< messages.count {
                            let list = messages[i]
                            self.isDataLoading = true
                            if !messageList.contains(list){
                                self.messageList.append(list)
                                self.messageListTemp.append(list)
                            }
                        }
                        // self.messageList.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                        //  self.messageListTemp.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                        self.tblChannelSendBroadcastMsgList.reloadData()
                        // self.tblChannelSendBroadcastMsgList.setContentOffset(.zero, animated: true)
                       // self.tblChannelSendBroadcastMsgList.scroll(to: .bottom, animated: false)
                        //  self.scrollToLoadMorePossition(messagesCount: preveousCount)
                        
                    }else{
                        self.getOldMessage()
                    }
                }
            }
        }
    }

}
extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: false)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-10, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}
