//
//  ChatViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import XMPPFramework

class ChatViewController: UIViewController {
    
    var xmppController: XMPPController!
    var stream:XMPPStream!
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtViewOfSendMsg: UITextView!
    var arrOfChatData = NSMutableArray()
    var appDelegate = AppDelegate()
    @IBOutlet weak var bottomOfmessageComposingView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardNotifications()
        txtViewOfSendMsg.text = ""
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.tblChat.tableFooterView = UIView()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.dismissKeyboard))
        self.tblChat.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        self.InitialDesign() //
        
        if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
            print("retriveState",retriveState)
            self.didTouchLogIn(userJID: "1649485378460274@kisan-test.m.in-app.io", userPassword: "+919730674642", server: "kisan-test.m.in-app.io")
        }
        
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)

    }
    
    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
        do {
            try appDelegate.xmppController = XMPPController(hostName: server,
                                                            userJIDString: userJID,
                                                            password: userPassword)
            appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
            appDelegate.xmppController.connect()
            
        } catch {
            print("Something went wrong")
        }
    }
    
    func InitialDesign() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc func logout(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        appDelegate.xmppController.xmppStream.disconnect()
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        txtViewOfSendMsg.resignFirstResponder()
        //setTblScrollAtBottom()
    }
    
    
    func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //self.buttomLayoutConstraint = keyboardFrame.size.height
            self.bottomOfmessageComposingView.constant = keyboardFrame.size.height
            let when = DispatchTime.now() + 0.001 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.arrOfChatData.count > 0 {
                    self.setTblScrollAtBottom()
                }
            }
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
        })
    }
    
    func scrollToBottom(){
        let indexPath = IndexPath(row: self.arrOfChatData.count - 1, section: 0)
        self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.bottomOfmessageComposingView.constant = 0.0
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
            self.setTblScrollAtBottom()
            
        })
    }
    
    
    func setTblScrollAtBottom()  {
        
        let numSections = tblChat.numberOfSections
        //var contentInsetTop = tblChannelSendBroadcastMsgList.bounds.size.height -
        //    (self.navigationBar?.frame.size.height ?? 0)
        //var contentInsetTop = tblOneToOneChat.bounds.size.height - (64)
        var contentInsetTop = tblChat.bounds.size.height
        for section in 0..<numSections {
            let numRows = tblChat.numberOfRows(inSection: section)
            let sectionHeaderHeight = tblChat.rectForHeader(inSection: section).size.height
            let sectionFooterHeight = tblChat.rectForFooter(inSection: section).size.height
            contentInsetTop -= sectionHeaderHeight + sectionFooterHeight
            for i in 0..<numRows {
                let rowHeight = tblChat.rectForRow(at: IndexPath(item: i, section: section)).size.height
                contentInsetTop -= rowHeight
                if contentInsetTop <= 0 {
                    contentInsetTop = 0
                    break
                }
            }
            // Break outer loop as well if contentInsetTop == 0
            if contentInsetTop == 0 {
                break
            }
        }
        tblChat.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)
        let when = DispatchTime.now() + 0.000001 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.arrOfChatData.count > 0 {
                self.scrollToBottom()
            }
        }
        
    }
    
    @IBAction func btnSendMessageClick(_ sender: Any) {
      
        //txtViewOfSendMsg.resignFirstResponder()
       /* let body = DDXMLElement(name: "body", stringValue: txtViewOfSendMsg.text)
        let messageID = appDelegate.xmppController.xmppStream.generateUUID
        let completeMessage = DDXMLElement.element(withName: "message") as! DDXMLElement
        completeMessage.addAttribute(withName: "id", stringValue: messageID)
        completeMessage.addAttribute(withName: "type", stringValue: "chat")
        completeMessage.addAttribute(withName: "to", stringValue: "test_kisan_test_7@kisan-test.m.in-app.io")
        completeMessage.addChild(body)
        let active = DDXMLElement.element(withName: "active", stringValue:
            "http://jabber.org/protocol/chatstates") as! DDXMLElement
        completeMessage.addChild(active)
        arrOfChatData.add(txtViewOfSendMsg.text)
        txtViewOfSendMsg.text = ""
        tblChat.reloadData()
        if self.arrOfChatData.count > 0 {
            self.setTblScrollAtBottom()
        }
        appDelegate.xmppController.xmppStream.send(completeMessage)*/
        
        /* let image: UIImage = UIImage(named: "Good-Day-Messages-for-Friend")!
         
         let dataF = UIImagePNGRepresentation(image)
         let imgStr = dataF?.base64EncodedString()
         
         let body = DDXMLElement(name: "body", stringValue: imgStr) //"Good-Day-Messages-for-Friend"
         let messageID = appDelegate.xmppController.xmppStream.generateUUID
         let completeMessage = DDXMLElement.element(withName: "message") as! DDXMLElement
         completeMessage.addAttribute(withName: "id", stringValue: messageID)
         completeMessage.addAttribute(withName: "type", stringValue: "chat")
         completeMessage.addAttribute(withName: "to", stringValue: "test_kisan_6@kisan.m.in-app.io")
         completeMessage.addChild(body)
         let active = DDXMLElement.element(withName: "active", stringValue:
         "http://jabber.org/protocol/chatstates") as! DDXMLElement
         completeMessage.addChild(active)
         arrOfChatData.add(txtViewOfSendMsg.text)
         txtViewOfSendMsg.text = ""
         tblChat.reloadData()
         if self.arrOfChatData.count > 0 {
         self.setTblScrollAtBottom()
         }
         appDelegate.xmppController.xmppStream.send(completeMessage)*/
        
        sendOneToOneMsg()
       // groupChat()
    }
    
    func sendOneToOneMsg(){
        // Use this code for acknowledge
        //self.xsm = XMPPStreamManagement()
        /* xsm?.addDelegate(self, delegateQueue: DispatchQueue.main)
         xsm?.activate(appDelegate.xmppController.xmppStream)
         let enable = XMLElement(name: "enable", xmlns: "urn:xmpp:sm:3")
         xsm?.xmppStream?.send(enable)
         xsm?.requestAck()
         xsm?.enable(withResumption: true, maxTimeout: 0)*/
        
        // one To one Chat
        //txtViewOfSendMsg.resignFirstResponder()
        let body = DDXMLElement(name: "body", stringValue: txtViewOfSendMsg.text)
        let messageID = appDelegate.xmppController.xmppStream.generateUUID
        let completeMessage = DDXMLElement.element(withName: "message") as! DDXMLElement
        completeMessage.addAttribute(withName: "id", stringValue: messageID)
        completeMessage.addAttribute(withName: "type", stringValue: "chat")
        //completeMessage.addAttribute(withName: "to", stringValue: "test_kisan_3@kisan.m.in-app.io")
        completeMessage.addAttribute(withName: "to", stringValue: "1649485378460274@kisan-test.m.in-app.io")
        completeMessage.addChild(body)
        let active = DDXMLElement.element(withName: "active", stringValue:
            "http://jabber.org/protocol/chatstates") as! DDXMLElement
        // Use this code for acknowledge
        /* let a = XMLElement(name: "request", xmlns: "urn:xmpp:receipts")
         completeMessage.addChild(a)*/
        completeMessage.addChild(active)
        arrOfChatData.add(txtViewOfSendMsg.text)
        txtViewOfSendMsg.text = ""
        tblChat.reloadData()
        if self.arrOfChatData.count > 0 {
            self.setTblScrollAtBottom()
        }
        
        appDelegate.xmppController.xmppStream.send(completeMessage)
    }
    
    
    func groupChat() {
        
        //Group chat (channel)
        let host = "xmpp.kisan.in"
        let serviceJID = XMPPJID(string: "pubsub.\(host)") //XMPPJID.jid(with: "pubsub.\(host)")
        print("serviceJID",serviceJID ?? String())
        let xmppPubSub = XMPPPubSub(serviceJID: serviceJID, dispatchQueue: DispatchQueue.main)
        xmppPubSub.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppPubSub.activate(appDelegate.xmppController.xmppStream)
        let body = XMLElement.element(withName: "body") as? XMLElement
        //body?.stringValue = "This is last mesg from iOS to agri channel"
        body?.stringValue = txtViewOfSendMsg.text
        txtViewOfSendMsg.text = ""
        let messageBody = XMLElement.element(withName: "message") as? XMLElement
        messageBody?.setXmlns("jabber:client")
        messageBody?.addChild(body ?? XMLNode())
        xmppPubSub.publish(toNode: "testForiOS", entry: messageBody!, withItemID: nil, options: ["pubsub#access_model": "open"])
        
        // Message status  Use this code for acknowledge
        let xmppMessageDeliveryRecipts = XMPPMessageDeliveryReceipts(dispatchQueue: DispatchQueue.main)
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = true
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = true
        xmppMessageDeliveryRecipts.activate(appDelegate.xmppController.xmppStream)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}

extension ChatViewController: XMPPStreamDelegate {
    
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        print("message",message.body ?? String())
        arrOfChatData.add(message.body ?? String())
        if self.arrOfChatData.count > 0 {
            self.setTblScrollAtBottom()
        }
        tblChat.reloadData()
    }
}


extension ChatViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfChatData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageCell", for: indexPath) as! ChatMessageCell
        cell.lblChatMsgText.text = arrOfChatData[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}


extension ChatViewController: UITextViewDelegate{
    
}

