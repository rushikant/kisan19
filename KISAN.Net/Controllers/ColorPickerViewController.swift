//
//  ColorPickerViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 08/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here
protocol PassSelectedColor: class {
    func passSelectedColor(_ color: String)
}

class ColorPickerViewController: UIViewController {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var colorPicker: SwiftHSVColorPicker!
     var selectedColor = "#FF2930"
    //MARK: step 2 Create a delegate property here, don't forget to make it weak!
    weak var delegate: PassSelectedColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        colorPicker.setViewColor(hexStringToUIColor(hex: selectedColor))
        
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.clipsToBounds = true
        
        btnOk.layer.cornerRadius = 5.0
        btnOk.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func hexString(color: UIColor?) -> String? {
        let components = color?.cgColor.components
        var red = Float((components?[0])!)
        var green = Float((components?[1])!)
        var blue = Float((components?[2])!)
        
        red = fabs(red)
        green = fabs(green)
        blue = fabs(blue)
        return String(format: "#%02lX%02lX%02lX", lroundf(red * 255), lroundf(green * 255), lroundf(blue * 255))
    }
    
    @IBAction func btnCancelClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnOkClick(_ sender: Any) {
        let selectedColor = colorPicker.color
        print(selectedColor!)
        let colorValue = self.hexString(color: colorPicker.color)
        print("colorValue",colorValue!)
        delegate?.passSelectedColor(colorValue!)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
