//
//  CreateCommunityStep1ViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 01/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import AWSS3
import MBProgressHUD

class CreateCommunityStep1ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate,PassMultiMediaResponse,PassSelectedColor,CropViewControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var basicBool : Bool = false
    @IBOutlet weak var imageViewOfUserProfile: UIImageView!
    @IBOutlet weak var viewOfBackProfilePic: UIView!
    var profileImage: UIImage!
    var groupNameTxtLength: Int! = 0
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var viewOfBackGroupName: UIView!
    @IBOutlet weak var txtGroupName: UITextField!
    var retriveDataOfCommunityTypeChannel = String()
    @IBOutlet weak var btnNext: UIButton!
    var data = NSData()
    var dominantColour = String()
    var dominantColourForPassNextView = String()
    @IBOutlet weak var imgViewOfCommunityType: UIImageView!
    var originalMediaBucketName = String()
    var communityJabberId = String()
    var communityImageUrl = String()
    @IBOutlet weak var viewOnScroll: UIView!
    var firstLoad = true
    var base64String: String = ""
    private let picker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        checkTypeOfCommunity()
        
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: StringConstants.NavigationTitle.next, style: .plain, target: self, action: #selector(btnNextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        viewOfBackGroupName.layer.cornerRadius = 5.0
        scrollView.isScrollEnabled = false
        imageViewOfUserProfile.layer.cornerRadius = imageViewOfUserProfile.frame.size.width/2
        imageViewOfUserProfile.clipsToBounds = true
        viewOfBackProfilePic.layer.cornerRadius = viewOfBackProfilePic.frame.size.width/2
        viewOfBackProfilePic.clipsToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11.2, *) {
            navigationController?.navigationBar.tintAdjustmentMode = .normal
            navigationController?.navigationBar.tintAdjustmentMode = .automatic
        }
    }
    
    func checkTypeOfCommunity(){
        
        guard let retriveDataOfCommunityType =  SessionDetailsForCommunityType.shared.communityTypeChannel else {
            return
        }
        retriveDataOfCommunityTypeChannel = retriveDataOfCommunityType
        if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
            self.title = StringConstants.CommunityType.channelNaviTitle
            imgViewOfCommunityType.image = UIImage(named:StringConstants.ImageNames.communityTypeChannelImg)
            txtGroupName.placeholder = StringConstants.placeHolder.channelName
        } else {
            self.title = StringConstants.CommunityType.groupNaviTitle
            imgViewOfCommunityType.image = UIImage(named:StringConstants.ImageNames.communityTypeGroupImg)
            txtGroupName.placeholder = StringConstants.placeHolder.groupName
        }
        
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
            return
        }
    }
    
    @objc func btnNextTapped(sender: UIBarButtonItem) {
        
        clickNextButton()
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        //clickNextButton()
        txtGroupName.resignFirstResponder()
        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ColorPickerViewController") as! ColorPickerViewController
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func passSelectedColor(_ color:String) {
        print("color123",color)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: color)
        viewOnScroll.backgroundColor = hexStringToUIColor(hex: color)
        dominantColourForPassNextView = color
    }
    
    func clickNextButton(){
        
        self.txtGroupName.resignFirstResponder()
        
        if ((txtGroupName.text?.isEmpty)! || (txtGroupName.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            
            txtGroupName.text!  = (txtGroupName.text?.replacingOccurrences(of: " ", with: ""))!
            
            if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL {
                
                lblGroupName.isHidden = false
                lblGroupName.text = StringConstants.channelNameText_cannotBlank
                txtGroupName.becomeFirstResponder()
                
                
            }else{
                lblGroupName.isHidden = false
                lblGroupName.text = StringConstants.groupNameText_cannotBlank
                txtGroupName.becomeFirstResponder()
                
            }
            
        }
        else{

            let currentTime = DateTimeHelper.getCurrentMillis()
            let strUUID = UUIDHelper.getUUID()
            communityJabberId = String(currentTime) + "-" + String(strUUID)
            if (imageViewOfUserProfile.image?.isEqual(UIImage(named: StringConstants.ImageNames.userProfilePlaceHolderImg)))!{
                MBProgressHUD.hide(for: self.view, animated: true);
                lblGroupName.isHidden = true
                let createCumminity2VC = self.storyboard?.instantiateViewController(withIdentifier: "CreateCommunityStep2ViewController") as! CreateCommunityStep2ViewController
                createCumminity2VC.groupName = txtGroupName.text!
                createCumminity2VC.dominantColour = self.dominantColourForPassNextView
                createCumminity2VC.communityJabberId = communityJabberId
                createCumminity2VC.communityImageUrl = StringConstants.EMPTY
                self.navigationController?.pushViewController(createCumminity2VC, animated: true)
                
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                // Compress image
                let imageHelper = ImageHelper()
                let imageData = imageHelper.compressImage(image: imageViewOfUserProfile.image!, compressQuality: 0.4)!
                //Send original image to Amzon S3
                originalMediaBucketName = S3BucketName + channelProfiles + communityJabberId
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyName)
            }
        }
        
    }
    
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
        // Bucket name after adding Base URL & Image name
        MBProgressHUD.hide(for: self.view, animated: true);
        originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyName
        print("originalMediaBucketName",originalMediaBucketName)
        lblGroupName.isHidden = true
        let createCumminity2VC = self.storyboard?.instantiateViewController(withIdentifier: "CreateCommunityStep2ViewController") as! CreateCommunityStep2ViewController
        createCumminity2VC.groupName = txtGroupName.text!
        createCumminity2VC.dominantColour = self.dominantColourForPassNextView
        createCumminity2VC.communityJabberId = communityJabberId
        createCumminity2VC.communityImageUrl = originalMediaBucketName
        self.navigationController?.pushViewController(createCumminity2VC, animated: true)
    }
    
    
    func PassMultiMediaFail(_ errorMsg: String) {
        MBProgressHUD.hide(for: self.view, animated: true);
        self.popupAlert(title: "", message: errorMsg, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSelectPicClick(_ sender: Any) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: StringConstants.TextOnAllScreen.camera, style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: StringConstants.TextOnAllScreen.photoLibrary, style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: StringConstants.TextOnAllScreen.cancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewOfUserProfile.contentMode = .scaleAspectFit
            imageViewOfUserProfile.image = pickedImage
            viewOfBackProfilePic.isHidden = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                self.openEditor(nil)
            }
            
        }
    }
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self as! UIImageCropperProtocol
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
        
       /* let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)*/
    }
    
    // MARK: - CropView New Library delegate
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        // imageViewOfProfilePic.image = image
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        imageViewOfUserProfile.image = image
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfUserProfile.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension CreateCommunityStep1ViewController: UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtGroupName {
            
            var basicBool : Bool = false
            groupNameTxtLength = (txtGroupName.text! as NSString).length + (string as NSString).length - range.length as Int
            
            if  (!(txtGroupName.text?.isEmpty)!) {
                
                if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL {
                    
                    if groupNameTxtLength == 0 {
                        lblGroupName.text = StringConstants.channelNameText_cannotBlank
                        lblGroupName.isHidden = false
                        
                    }else{
                        lblGroupName.isHidden = true
                    }
                    
                }else{
                    
                    if groupNameTxtLength == 0 {
                        lblGroupName.text = StringConstants.groupNameText_cannotBlank
                        lblGroupName.isHidden = false
                        
                    }else{
                        lblGroupName.isHidden = true
                    }
                }
                
                
            }else{
                
                if basicBool == false {
                    
                    
                    if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL {
                        basicBool = true
                        if groupNameTxtLength == 0 {
                            lblGroupName.text = StringConstants.channelNameText_cannotBlank
                            lblGroupName.isHidden = false
                            
                            
                        }else{
                            lblGroupName.isHidden = true
                            
                        }
                    }else{
                        
                        basicBool = true
                        if groupNameTxtLength == 0 {
                            lblGroupName.text = StringConstants.groupNameText_cannotBlank
                            lblGroupName.isHidden = false
                            
                            
                        }else{
                            lblGroupName.isHidden = true
                            
                        }
                    }
                }
            }
        }
        
        return groupNameTxtLength <= 25
        
    }
    
}

extension CreateCommunityStep1ViewController: UIImageCropperProtocol {
  
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imageViewOfUserProfile.image = croppedImage
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfUserProfile.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
