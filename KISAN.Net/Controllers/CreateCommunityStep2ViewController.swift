//
//  CreateCommunityStep2ViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 01/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class CreateCommunityStep2ViewController: UIViewController {

    @IBOutlet weak var txtViewOfGroupInfo: UITextView!
    @IBOutlet weak var lblOfTextCount: UILabel!
    @IBOutlet weak var imgOfPublicGroupRdbtn: UIImageView!
    @IBOutlet weak var imgOfPrivateGroupRdbtn: UIImageView!
    var groupName = String()
    var communityPrivacy = String()
    var retriveDataOfCommunityTypeChannel = String()
    var createCommunityRequest = Dictionary<String, Any>()
    var userDetailsRequest = Dictionary<String, Any>()
    var index = 0 as Int
    @IBOutlet weak var lblAboutCommunityTitle: UILabel!
    @IBOutlet weak var lblCommunityTypeTitle: UILabel!
    @IBOutlet weak var lblpublicTypeOfCommunity: UILabel!
    @IBOutlet weak var lblPublicDescriOfCommunity: UILabel!
    @IBOutlet weak var lblPrivateTypeOfCommunity: UILabel!
    @IBOutlet weak var lblPrivateDescriOfCommunity: UILabel!
    var dominantColour = String()
    var communityJabberId = String()
    var communityImageUrl = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        checkTypeOfCommunity()
    }
    
    func initialDesign() {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: StringConstants.NavigationTitle.next, style: .plain, target: self, action: #selector(btnNextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        lblOfTextCount.isHidden = false
        print("dominantColour",dominantColour)
        if dominantColour.isEmpty {
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: dominantColour)
        }
        
        if UIScreen.main.bounds.size.height <= 568{
            lblPublicDescriOfCommunity.font = lblPublicDescriOfCommunity.font.withSize(10)
            lblPrivateDescriOfCommunity.font = lblPublicDescriOfCommunity.font.withSize(11)
        }else{
            lblPublicDescriOfCommunity.font = lblPublicDescriOfCommunity.font.withSize(14)
            lblPrivateDescriOfCommunity.font = lblPublicDescriOfCommunity.font.withSize(14)
        }
    }
    
    func checkTypeOfCommunity(){
        guard let retriveDataOfCommunityType =  SessionDetailsForCommunityType.shared.communityTypeChannel else {
            return
        }
        retriveDataOfCommunityTypeChannel = retriveDataOfCommunityType
        print("retriveDataOfCommunityTypeChannel",retriveDataOfCommunityTypeChannel)
        if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
            self.title = StringConstants.CommunityType.channelNaviTitleForChannelInfo
            txtViewOfGroupInfo.text = StringConstants.CommunityType.txtPlaceHolderForChannelInfo
            txtViewOfGroupInfo.textColor = UIColor.lightGray
            
            lblAboutCommunityTitle.text = StringConstants.CommunityType.channelAboutCommunityTitle
            lblCommunityTypeTitle.text = StringConstants.CommunityType.channelCommunityTypeTitle
            lblpublicTypeOfCommunity.text = StringConstants.CommunityType.channelpublicTypeOfCommunity
            lblPublicDescriOfCommunity.text = StringConstants.CommunityType.channelPublicDescriOfCommunity
            lblPrivateTypeOfCommunity.text = StringConstants.CommunityType.channelPrivateTypeOfCommunity
            lblPrivateDescriOfCommunity.text = StringConstants.CommunityType.channelPrivateDescriOfCommunity
            
            
        } else {
            self.title = StringConstants.CommunityType.groupNaviTitleForGroupInfo
            txtViewOfGroupInfo.text = StringConstants.CommunityType.txtPlaceHolderForGroupInfo
            txtViewOfGroupInfo.textColor = UIColor.lightGray
            
            lblAboutCommunityTitle.text = StringConstants.CommunityType.groupAboutCommunityTitle
            lblCommunityTypeTitle.text = StringConstants.CommunityType.groupCommunityTypeTitle
            lblpublicTypeOfCommunity.text = StringConstants.CommunityType.grouppublicTypeOfCommunity
            lblPublicDescriOfCommunity.text = StringConstants.CommunityType.groupPublicDescriOfCommunity
            lblPrivateTypeOfCommunity.text = StringConstants.CommunityType.groupPrivateTypeOfCommunity
            lblPrivateDescriOfCommunity.text = StringConstants.CommunityType.groupPrivateDescriOfCommunity

        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.2, *) {
            navigationController?.navigationBar.tintAdjustmentMode = .normal
            navigationController?.navigationBar.tintAdjustmentMode = .automatic
        }
        
        txtViewOfGroupInfo.selectedTextRange = txtViewOfGroupInfo.textRange(from: txtViewOfGroupInfo.beginningOfDocument, to: txtViewOfGroupInfo.beginningOfDocument)
        communityPrivacy = StringConstants.CommunityPrivacy.publicPrivacy
        lblOfTextCount.text = String(144)
        imgOfPublicGroupRdbtn.image = imgOfPublicGroupRdbtn.image!.withRenderingMode(.alwaysTemplate)
        imgOfPublicGroupRdbtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        imgOfPrivateGroupRdbtn.tintColor = UIColor.black

    }
    
    @IBAction func btnPublicGroupClick(_ sender: Any) {
        imgOfPrivateGroupRdbtn?.image = UIImage(named:StringConstants.ImageNames.privacyRadiobtnUnChecked)
        imgOfPublicGroupRdbtn?.image = UIImage(named:StringConstants.ImageNames.privacyRadiobtnChecked)
        imgOfPublicGroupRdbtn.image = imgOfPublicGroupRdbtn.image!.withRenderingMode(.alwaysTemplate)
        imgOfPublicGroupRdbtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        imgOfPrivateGroupRdbtn.tintColor = UIColor.black
        communityPrivacy = StringConstants.CommunityPrivacy.publicPrivacy
        txtViewOfGroupInfo .resignFirstResponder()
    }
    
    @IBAction func btnPrivateGroupClick(_ sender: Any) {
        imgOfPublicGroupRdbtn?.image = UIImage(named:StringConstants.ImageNames.privacyRadiobtnUnChecked)
        imgOfPrivateGroupRdbtn?.image = UIImage(named:StringConstants.ImageNames.privacyRadiobtnChecked)
        imgOfPrivateGroupRdbtn.image = imgOfPrivateGroupRdbtn.image!.withRenderingMode(.alwaysTemplate)
        imgOfPrivateGroupRdbtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        imgOfPublicGroupRdbtn.tintColor = UIColor.black
        communityPrivacy = StringConstants.CommunityPrivacy.privatePrivacy
        txtViewOfGroupInfo .resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnNextTapped(sender: UIBarButtonItem) {
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
        let communityDescprionData = checkDescriptionData(description: txtViewOfGroupInfo.text)
        let userAreaInterestVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
        userAreaInterestVC.groupName = groupName
        userAreaInterestVC.communityPrivacy = communityPrivacy
        userAreaInterestVC.communityDescprionData = communityDescprionData
        userAreaInterestVC.communityJabberId = communityJabberId
        userAreaInterestVC.communityImageUrl = communityImageUrl
        userAreaInterestVC.dominantColour = dominantColour
        self.navigationController?.pushViewController(userAreaInterestVC, animated: true)
    }
    
    
    func checkDescriptionData(description: String) -> String {
        var checkDescriptionData =  String()
        if description == StringConstants.CommunityType.txtPlaceHolderForChannelInfo {
            checkDescriptionData = ""
        } else {
            checkDescriptionData = description
        }
        return checkDescriptionData
    }
    
}

extension CreateCommunityStep2ViewController: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var currentText = textView.text
        var updatedText = ""
        if !(currentText?.isEmpty)! {
            updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        }
        
        if text == "" && range.location > 0 {
            index = index - 1
        }else if text == "" && range.location == 0{
            index = 0
        }else {
            index = index + 1
        }
        
        
        if updatedText.isEmpty && text.isEmpty{
            
            if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
                textView.text = StringConstants.CommunityType.txtPlaceHolderForChannelInfo
                textView.textColor = UIColor.lightGray
            } else {
                textView.text = StringConstants.CommunityType.txtPlaceHolderForGroupInfo
                textView.textColor = UIColor.lightGray
            }
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
            if currentText == StringConstants.CommunityType.txtPlaceHolderForChannelInfo{
                currentText = ""
            }
        } else {
            
            if currentText == StringConstants.CommunityType.txtPlaceHolderForGroupInfo{
                currentText = ""
            }
        }
        
        if index > 0 {
            
            let  newTaxt = (currentText! as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newTaxt.characters.count // for Swift use count(newText)
            let count = 144 - numberOfChars
            
            if count >= 0 {
                lblOfTextCount.text =  String(count)
                
                if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
                    if textView.text == StringConstants.CommunityType.txtPlaceHolderForChannelInfo && textView.textColor == UIColor.lightGray{
                        lblOfTextCount.text =  String(144)
                    }
                } else {
                    
                    if textView.text == StringConstants.CommunityType.txtPlaceHolderForGroupInfo && textView.textColor == UIColor.lightGray{
                        lblOfTextCount.text =  String(144)
                    }
                }
                
                return numberOfChars <= 144;
            }else{
                
                return numberOfChars <= 144;

            }
        }
        return true
        
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
             if textView.textColor == UIColor.lightGray  {
                //textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
                if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
                    if textView.text == StringConstants.CommunityType.txtPlaceHolderForChannelInfo && textView.textColor == UIColor.lightGray{
                        lblOfTextCount.text =  String(144)
                    }
                } else {
                    if textView.text == StringConstants.CommunityType.txtPlaceHolderForGroupInfo && textView.textColor == UIColor.lightGray{
                        lblOfTextCount.text =  String(144)
                    }
                }
                
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            
            if retriveDataOfCommunityTypeChannel == StringConstants.CommunityType.CHANNEL && !(retriveDataOfCommunityTypeChannel.isEmpty) {
                textView.text = StringConstants.CommunityType.txtPlaceHolderForChannelInfo

            } else {
                textView.text = StringConstants.CommunityType.txtPlaceHolderForGroupInfo
            }
            
            textView.textColor = UIColor.lightGray
        }
    }
    
}

