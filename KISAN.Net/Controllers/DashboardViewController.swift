//
//  DashboardViewController.swift
//  KisanChat
//
//  Created by Kisan-MAC on 09/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import AccountKit
import XMPPFramework
import SDWebImage
import MBProgressHUD
import SafariServices
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import Contacts

class DashboardViewController: UIViewController,UIActivityItemSource,SFSafariViewControllerDelegate,UIViewControllerTransitioningDelegate,PassMultiMediaResponse{
    
    @IBOutlet weak var tblMyChatList: UITableView!
    var refreshControl = UIRefreshControl()
    var arrOfMenus = NSMutableArray()
    var lblOfseprator = UILabel()
    var lblOfTitle = UILabel()
    var commingFrom = String()
    var pendingInvitecount = Int()
    var myGreenpasCoint = Int()

    var flagOfCheckMoreOptionClick = Bool()
    lazy   var searchBar:UISearchBar = UISearchBar()
    var appDelegate = AppDelegate()
    @IBOutlet weak var tblMoreOption: UITableView!
    @IBOutlet weak var viewOfBackMoreOptionTbl: UIView!
    var myChatsList = [MyChatsTable]()
    var myChatsListTemp = [MyChatsTable]()
    var filteredObjects: [MyChatsTable]?
    var userId = String()
    var AuthToken = String()
    var token = String()
    var accountKit: AKFAccountKit!
    var arrOfEntityName = [StringConstants.coreDataEntityName.communityDetailTable, StringConstants.coreDataEntityName.messages, StringConstants.coreDataEntityName.myChatsTable,StringConstants.coreDataEntityName.membersTable,StringConstants.coreDataEntityName.notificationTable]
    @IBOutlet weak var viewOfStartDiscovering: UIView!
    var forwardedmessageData:Messages?
    var createSendMessageRequestInDB = Dictionary<String, Any>()
    var createSendMessageRequest = Dictionary<String, Any>()
    let fetOldMessageGroup = DispatchGroup()
    @IBOutlet weak var btnBannerShow: UIButton!
    @IBOutlet weak var lblOfUserNameOnDiscoverView: UILabel!
    @IBOutlet var lblWelcomeText: UILabel!
    @IBOutlet var btnstartdiscovering: UIButton!
    @IBOutlet weak var topOfTblMayChat: NSLayoutConstraint!
    @IBOutlet weak var heightOfBannerButton: NSLayoutConstraint!
    @IBOutlet var heightofAttachmenutable: NSLayoutConstraint!
    var imageData = Data()
    var messageKeyForSendMultiMedia = String()
    var messageTimeForSendMultiMedia = String()
    var originalMediaBucketName = String()
    var thumbMediaBucketName = String()
    let documentDirectoryHelper = DocumentDirectoryHelper()
    var S3UploadKeyNameWithImagePrefix = String()
    var S3UploadKeyNameWithThumbPrefix = String()
    var myChatsOfForwordData = MyChatsTable()
    var mediaSize = Double()
    var thumbVideoData = Data()
    var pdfDocData = Data()
    var documentName = String()
    var pdfThumbData = Data()
    
    @IBOutlet weak var topOfTblMychListToSelfView: NSLayoutConstraint!
    
    //Greenpass Banners
    
    @IBOutlet weak var viewOfGreenpassBanner: UIView!
    @IBOutlet weak var lblBuyAndMYGreenpass: UILabel!
    @IBOutlet weak var imgArrowGoBtn: UIImageView!
    @IBOutlet weak var lblPendingInviteCount: UILabel!
    @IBOutlet weak var viewOfPendingInvites: UIView!
    @IBOutlet weak var lblGreenPassissueLastDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if TARGET_TESTING
        self.subScribeToTopicForCustomNotification(toTopic1:StringConstants.global,toTopic2:StringConstants.global_ios)
        #elseif TARGET_DEV
        self.subScribeToTopicForCustomNotification(toTopic1:StringConstants.global,toTopic2:StringConstants.global_ios)
        #else
        self.subScribeToTopicForCustomNotification(toTopic1:StringConstants.global_prod,toTopic2:StringConstants.global_ios_prod)
        #endif
        
        
        initialDesign()
        flagOfCheckMoreOptionClick = true
        handleCustomNotificationsRedirect()
        NotificationCenter.default.addObserver(self,selector:#selector(applicationWillEnterForeground(_:)),name:NSNotification.Name.UIApplicationWillEnterForeground,object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(applicationDidActive(_:)),name:NSNotification.Name.UIApplicationDidBecomeActive,object: nil)
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        definesPresentationContext = true
        //Register to receive notification in your class (NotificationCenter from RightViewController) move to channel profile
        NotificationCenter.default.addObserver(self, selector: #selector(self.setChannelProfileFrmNotification(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.channelProfileFrmNotification), object: nil)
        
        //Register to receive notification in your class (NotificationCenter from appdelegate of channel block and unblock)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMyChatAftergettingNotification(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnDashboard), object: nil)
        
        //shaout myChat list from MyChatServices class ...... When get new myChat or update mychat
        NotificationCenter.default.addObserver(self, selector: #selector(self.shaoutMyChatDataAfterGetNewOrUpdateMychat(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.shaoutMyChatData), object: nil)
        
        //Update mychat list when get rss feed notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMyChatAftergettingNotification(_:)), name: NSNotification.Name(rawValue: "receivedMessagesForChannelBroadCast"), object: nil)
        
        
        //heightofAttachmenutable.constant = 150
        heightofAttachmenutable.constant = 50
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
            if accountType == StringConstants.ONE {
                btnBannerShow.isHidden = false
                btnBannerShow.layer.cornerRadius = 20
                if UIScreen.main.bounds.size.width <= 320 {
                    //heightOfBannerButton.constant = 0 //60
                    // topOfTblMayChat.constant = 15//81
                }else{
                    //heightOfBannerButton.constant = 0 //70
                    //topOfTblMayChat.constant = 15//91
                }
                
            }else{
                btnBannerShow.isHidden = false
                //heightOfBannerButton.constant = 0
                //topOfTblMayChat.constant = 0
            }
        }
        
        //Register cell for sponserd message
        self.tblMyChatList.register(UINib(nibName: "PostMesaageViewCell", bundle:nil), forCellReuseIdentifier: "PostMesaageViewCell")
        
        //clear the all remote notification from notificatio tray
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        UIApplication.shared.cancelAllLocalNotifications()
        center.removeAllPendingNotificationRequests()
        
        
        //sign in to GrrenCloud
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        if(sessionId != nil  && sessionId != StringConstants.EMPTY){
            getInviteSummaryAPI()
        }else{
            self.sighIntoGreenCloud()
        }
        
        Analytics.logEvent("share_image", parameters: [
            "name": "Kisan Forum" as NSObject,
            "full_text": "Kisan Exhibition 2019" as NSObject
            ])
        
        getAppStarterDataAPI()
        getEventDetails()
    }
    
    
    
    func openPopUpConditionally(){
        
        /*appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)*/

        /*let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)

        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
            var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
            if(isMitra && show_issue_pass == 0){
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }else{
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }else if(remaining>0){
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
            
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
                // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
        }*/
    }
    
    func getEventDetails() {
        
        let params = ExhibitionAdvertiseDetailsRequest.convertToDictionary(exhibitorAdvertiseDetails: ExhibitionAdvertiseDetailsRequest.init(app_key: URLConstant.GREENPASS_APPKEY, eventCode: URLConstant.eventCode, source: RequestName.loginSourceName, discount_name: "online", referrer_code: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.exhibitionDetails
        _ = ExhibitionAdvertiseServices().exhibitionAdvertise(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! ExhibitionAdvertiseResponse
                                                                let userData = model.exhibitiondetails
                                                                
                                                                let exh_badge_enddatetime = userData["exh_badge_enddatetime"]
                                                                let sepratedexh_badge_enddatetime = exh_badge_enddatetime?!.components(separatedBy: " ")
                                                                
                                                                let exh_badge_endDate = sepratedexh_badge_enddatetime?[0]
                                                                let exh_badge_endTime = sepratedexh_badge_enddatetime?[1]
                                                                let convertedEndDate = self.dateConverter(date: exh_badge_endDate ?? String())
                                                              //  let convertedEndTime = self.timeConverter(time:exh_badge_endTime ?? String())
                                                                
                                                                MBProgressHUD.hide(for: self.view, animated: true)
                                                                let badge_enddatetime = convertedEndDate.components(separatedBy: [","])
                                                                if badge_enddatetime.count > 0 {
                                                                    let exh_badge_enddatetimeValue = badge_enddatetime[0]
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:exh_badge_enddatetimeValue , keyString: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:exh_badge_enddatetime as! String , keyString: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime_original)


                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime)

                                                                }
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    

    
    
    func dateConverter(date:String) -> String{
        var convertedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM,  yyyy"
        if let convertdate = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: convertdate))
            convertedDate = dateFormatterPrint.string(from: convertdate)
        } else {
            print("There was an error decoding the string")
        }
        return convertedDate
    }
    

    func timeConverter(time:String) -> String{
        
        var convertedTime = String()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date: Date? = dateFormatter.date(from: time )
        dateFormatter.dateFormat = "hh:mm a"
        if let aDate = date {
            convertedTime = dateFormatter.string(from: aDate)
        }
        
        return convertedTime
    }
    

    func subScribeToTopicForCustomNotification(toTopic1:String,toTopic2:String){
        
        Messaging.messaging().subscribe(toTopic: toTopic1) { error in
            print("Subscribed to global topic")
        }
        Messaging.messaging().subscribe(toTopic: toTopic2) { error in
            print("Subscribed to global topic")
        }
    }
    
    
    func handleCustomNotificationsRedirect(){
        let comingFrom = UserDefaults.standard.string(forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
        appDelegate.isTapOnNotificationFromNotRunApp = false
        if comingFrom == StringConstants.PushNotificationAction.upgrade{
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
            guard let url = URL(string: StringConstants.AppLink) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }else if comingFrom == StringConstants.PushNotificationAction.Followchannel{
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
            let communitykey = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.CommynityKey)
            let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
            channelProfileVC.commingFrom = StringConstants.PushNotificationAction.Followchannel
            channelProfileVC.CommynityKeyFromPushNoti = communitykey ?? String()
            self.navigationController?.pushViewController(channelProfileVC, animated: true)
        }else if comingFrom == StringConstants.PushNotificationAction.link{
            let link = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.contentField1)
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
            guard let url = URL(string: link!) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        }else if comingFrom == StringConstants.PushNotificationAction.sponsored{
           
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
            let sponsoredMessageId = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.sponsoredMessageId)
            let communitykey = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.CommynityKey)
            let isSponsoredMessage = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.isSponsoredMessage)

            myChatsList = MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdTrue(communityKey: communitykey!, isSponsoredMessage: isSponsoredMessage!, id: sponsoredMessageId!)
            let myChats = myChatsList[0]
            
            if myChats.isSponsoredMessage == StringConstants.ONE{
                if myChats.isMember == StringConstants.ONE {
                    let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                    //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                    channelSendBroadcastMsgVC.communityName = myChats.communityName!
                    channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                    channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                    channelSendBroadcastMsgVC.myChatsData = myChats
                    // Change unreadMessagesCount to zero
                    MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                    self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                }else{
                    let SponseredMessageDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "SponseredMessageDetailController") as! SponseredMessageDetailController
                    SponseredMessageDetailVc.myChatsData = myChats
                    self.navigationController?.pushViewController(SponseredMessageDetailVc, animated: true)
                }
            }else{
                let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                channelSendBroadcastMsgVC.communityName = myChats.communityName!
                channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                channelSendBroadcastMsgVC.myChatsData = myChats
                // Change unreadMessagesCount to zero
                MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                
                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
            }

            
        }else if comingFrom == StringConstants.PushNotificationAction.message{
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
            let communitykey = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.CommynityKey)
            let senderId = UserDefaults.standard.string(forKey: StringConstants.PushNotificationAction.senderId)

            if (communitykey!.contains("_")){

                var communitykeyNew = String()
               if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
                    if accountType == StringConstants.ONE{
                        let communitykeyArr : [String] = communitykey!.components(separatedBy: "_")
                        communitykeyNew = communitykeyArr[0]
                    }else{
                        if senderId == userId{
                           communitykeyNew = communitykey!
                        }else{
                            let communitykeyArr : [String] = communitykey!.components(separatedBy: "_")
                            communitykeyNew = communitykeyArr[0]
                        }
                    }
                }
                myChatsList = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communitykeyNew)
                let myChats = myChatsList[0]
                let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
                oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: myChats.communityName!)
                oneToOneFromAdminVC.communityKey = myChats.communityKey!
                oneToOneFromAdminVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                //oneToOneFromAdminVC.dominantColour = myChats.dominantColour!
               // oneToOneFromAdminVC.ownerId = myChats.ownerId! //myChatsData.ownerId!
                oneToOneFromAdminVC.myChatsData = myChats
                self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: false)

            }else{
                myChatsList = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communitykey!)
                let myChats = myChatsList[0]
                let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                channelSendBroadcastMsgVC.communityName = myChats.communityName!
                channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                channelSendBroadcastMsgVC.myChatsData = myChats
                // Change unreadMessagesCount to zero
                MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                //UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.PushNotificationAction.CommynityKey)
                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: false)
            }
            
        }else{
            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
        }
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
       // handleCustomNotificationsRedirect()
        lblGreenPassissueLastDate.layer.removeAllAnimations()
        lblGreenPassissueLastDate.alpha = 1
        lblBuyAndMYGreenpass.layer.removeAllAnimations()
        lblBuyAndMYGreenpass.alpha = 1
        print("Received Notification")
        //NotificationCenter.default.removeObserver(self)
        ButtonAnimationHelper().blinkView(view: lblGreenPassissueLastDate)
        ButtonAnimationHelper().blinkView(view: lblBuyAndMYGreenpass)
    }
    
    @objc func applicationDidActive(_ notification: NSNotification) {
         handleCustomNotificationsRedirect()
        print("Received Notification in applicationDidActive")
        //NotificationCenter.default.removeObserver(self)
    }
    
    

    @objc func refreshMychat() {
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            self.getMyChatList(isFullToRefresh:true)
        }else{
        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
            self.refreshControl.endRefreshing()
        }
    }
    
    
    @objc func shaoutMyChatDataAfterGetNewOrUpdateMychat(_ notification: NSNotification) {
        let myChatValue = notification.userInfo?["myChats"] as! MyChatsTable
        if !self.myChatsList.contains(myChatValue){
            self.myChatsList.append(myChatValue)
            self.tblMyChatList.beginUpdates()
            self.tblMyChatList.insertRows(at: [IndexPath(row: self.myChatsList.count - 1, section: 0)], with: .automatic)
            self.tblMyChatList.endUpdates()
        }
        
        fetOldMessageGroup.enter()
        self.getOldMessage(communityKey:myChatValue.communityKey!)
        fetOldMessageGroup.notify(queue: .main) {
            self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
           // self.myChatsList.sort(by:{$0.last_activity_datetime! > $1.last_activity_datetime!} )
            self.myChatsListTemp = self.myChatsList
//            for i in (0..<self.myChatsListTemp.count){
//                let mychat = self.myChatsListTemp[i]
//                if  mychat.isSponsoredMessage == StringConstants.ONE{
//                    if self.userId == mychat.ownerId{
//                        if  mychat.isSponsoredMessage == StringConstants.ONE {
//                            let index =    self.myChatsList.index { (myChatValue) -> Bool in
//                                myChatValue.isSponsoredMessage == mychat.isSponsoredMessage
//                            }
//                            self.myChatsList.remove(at: index!)
//                        }
//                        //  self.myChatsList.remove(at: i)
//                    }
//                }
//            }

            self.hideShowDiscoverView()
            self.tblMyChatList.reloadData()
        }
    }
    
    @objc func updateMyChatAftergettingNotification(_ notification: NSNotification) {
        self.myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
        self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
        //self.myChatsList.sort(by:{$0.last_activity_datetime! > $1.last_activity_datetime!} )
//        for i in (0..<self.myChatsListTemp.count){
//            let mychat = self.myChatsListTemp[i]
//            if  mychat.isSponsoredMessage == StringConstants.ONE{
//                if self.userId == mychat.ownerId{
//                    if  mychat.isSponsoredMessage == StringConstants.ONE {
//                        let index =    self.myChatsList.index { (myChatValue) -> Bool in
//                            myChatValue.isSponsoredMessage == mychat.isSponsoredMessage
//                        }
//                        self.myChatsList.remove(at: index!)
//                    }
//                    //self.myChatsList.remove(at: i)
//                }
//            }
//        }
        tblMyChatList.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
    }
    
    func initialDesign(){
        
        tblMyChatList.tableFooterView = UIView()
        //arrOfMenus.add((NSLocalizedString(StringConstants.popUpMenuName.inviteToKisan, comment: StringConstants.EMPTY)))
        //arrOfMenus.add(StringConstants.popUpMenuName.changeLanguage)
        //arrOfMenus.add(StringConstants.popUpMenuName.search)
        // arrOfMenus.add(NSLocalizedString(StringConstants.popUpMenuName.changeLanguage, comment: StringConstants.EMPTY))
        arrOfMenus.add(NSLocalizedString(StringConstants.popUpMenuName.search, comment: StringConstants.EMPTY))
        //btnBannerShow
        setNavigation()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DashboardViewController.dismisaPopUp))
        tap.cancelsTouchesInView = false
        self.viewOfBackMoreOptionTbl.addGestureRecognizer(tap)
        
        //overlay view
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 20
        paragraphStyle.alignment = .center
        let attrs = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize:20),NSAttributedStringKey.paragraphStyle :paragraphStyle]
        let attributedString2 = NSMutableAttributedString(string:(NSLocalizedString(StringConstants.DashboardViewController.Welcome1, comment: StringConstants.EMPTY)), attributes:attrs)
        let attributedString3 = NSMutableAttributedString(string:"\n", attributes:attrs)
        let attrs2 = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize:16),NSAttributedStringKey.paragraphStyle :paragraphStyle]
        let attrString = NSMutableAttributedString(string: (NSLocalizedString(StringConstants.DashboardViewController.Welcome2, comment: StringConstants.EMPTY)), attributes: attrs2)
        attributedString2.append(attributedString3)
        attributedString2.append(attrString)
        lblWelcomeText.attributedText = attributedString2
        lblWelcomeText.textAlignment = .center
        btnstartdiscovering.setTitle((NSLocalizedString(StringConstants.DashboardViewController.Discover, comment: StringConstants.EMPTY)), for: .normal)
        
        
        
        //for Greenpass banners
        viewOfGreenpassBanner.layer.cornerRadius = 8
        viewOfGreenpassBanner.clipsToBounds = true
        
//        viewOfGreenpassBanner.isHidden = true
//        heightOfBannerButton.constant = 0
//        topOfTblMychListToSelfView.constant = 0

        /*//
        viewOfGreenpassBanner.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        imgArrowGoBtn.image = UIImage(named: StringConstants.ImageNames.greenArrowGo)*/
        
    }
    
    @objc func dismisaPopUp() {
        self.view.endEditing(true)
        viewOfBackMoreOptionTbl.isHidden = true
        flagOfCheckMoreOptionClick = true
    }
    
    func setNavigation(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC || commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.forwardTo, comment: ""))
            navigationItem.titleView = lblOfTitle
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
            navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            for i in (0..<myChatsListTemp.count){
                let mychat = myChatsListTemp[i]
                let isMember = mychat.isMember
               /* if  mychat.isSponsoredMessage == StringConstants.ONE {
                    let index =    self.myChatsList.index { (myChatValue) -> Bool in
                        myChatValue.isSponsoredMessage == mychat.isSponsoredMessage
                    }
                    myChatsList.remove(at: index!)
                }
                else*/ if  isMember == StringConstants.ZERO {
                    let index =    self.myChatsList.index { (myChatValue) -> Bool in
                        myChatValue.isMember == isMember
                    }
                    myChatsList.remove(at: index!)
                }else if mychat.feed == StringConstants.ONE {
                    let index =    self.myChatsList.index { (myChatValue) -> Bool in
                        myChatValue.feed == mychat.feed
                    }
                    myChatsList.remove(at: index!)
                }
            }
        }else{
            
            //refreshcontrol
            //            refreshControl.attributedTitle = NSAttributedString(string: StringConstants.EMPTY)
            //            refreshControl.addTarget(self, action: #selector(refreshMychat), for: .valueChanged)
            //            refreshControl.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
            //tblMyChatList.addSubview(refreshControl)
            
            //SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.FALSE , keyString: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue)
            if let isCommingFromNotification = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue) {
                self.setNavigationBarButtons(isCommingFromNotification:isCommingFromNotification)
            }else{
                self.setNavigationBarButtons(isCommingFromNotification:StringConstants.EMPTY)
            }
        }
        //        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        //        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        
    }
    
    func setNavigationBarButtons(isCommingFromNotification:String){
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.dashboard, comment: ""))
        navigationItem.titleView = lblOfTitle
        tblMoreOption.separatorColor = UIColor.clear
        
        let moreOtionImage = UIImage(named: StringConstants.ImageNames.menuOption)!
        var notificationImage = UIImage()
        if isCommingFromNotification == StringConstants.TRUE{
            notificationImage = UIImage(named: StringConstants.ImageNames.notificationReceivedBellIcon)!
        }else{
            notificationImage = UIImage(named: StringConstants.ImageNames.notificationBell)!
        }
        let discoverImage = UIImage(named: StringConstants.ImageNames.discover)!
        let moreOtionButton: UIButton = UIButton(type: UIButtonType.custom)
        moreOtionButton.setImage(moreOtionImage, for: UIControlState.normal)
        moreOtionButton.addTarget(self, action: #selector(DashboardViewController.btnMoreOtionClick(_:)), for:.touchUpInside)
        moreOtionButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)        
        let moreOtionBtn = UIBarButtonItem(customView: moreOtionButton)
        
        let notificationButton: UIButton = UIButton(type: UIButtonType.custom)
        notificationButton.setImage(notificationImage, for: UIControlState.normal)
        notificationButton.addTarget(self, action: #selector(DashboardViewController.btnNotificationClick(_:)), for:.touchUpInside)
        notificationButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        let notificationBtn = UIBarButtonItem(customView: notificationButton)
        
        let  discoverButton: UIButton = UIButton(type: UIButtonType.custom)
        discoverButton.setImage(discoverImage, for: UIControlState.normal)
        discoverButton.addTarget(self, action: #selector(DashboardViewController.btnDiscoverClick(_:)), for:.touchUpInside)
        discoverButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        let  discoverBtn = UIBarButtonItem(customView:  discoverButton)
        self.navigationItem.setRightBarButtonItems([moreOtionBtn, notificationBtn, discoverBtn], animated: false)
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnMoreOtionClick(_ sender : UIButton) {
        if flagOfCheckMoreOptionClick == true{
            flagOfCheckMoreOptionClick = false
            viewOfBackMoreOptionTbl.isHidden = false
            tblMoreOption.reloadData()
        }else{
            viewOfBackMoreOptionTbl.isHidden = true
            flagOfCheckMoreOptionClick = true
        }
    }
    
    @objc func btnNotificationClick(_ sender : UIButton) {
        viewOfBackMoreOptionTbl.isHidden = true
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.FALSE , keyString: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue)
        let isCommingFromNotification = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue)
        self.setNavigationBarButtons(isCommingFromNotification:isCommingFromNotification)
        // For open right drawer
        // self.setRightDrawerForNotification()
        openRightDrawerOnDashBoard()
    }
    
    @objc func btnDiscoverClick(_ sender : UIButton) {
        viewOfBackMoreOptionTbl.isHidden = true
        let discoverChannelVC = self.storyboard?.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
        self.navigationController?.pushViewController(discoverChannelVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        MBProgressHUD.hide(for: self.view, animated: true);
        // Get data from local MyChat DB
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)

        self.myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
        self.myChatsListTemp =  self.myChatsList
        tblMyChatList.reloadData()
        self.hideShowDiscoverView()
        lblGreenPassissueLastDate.layer.removeAllAnimations()
        lblGreenPassissueLastDate.alpha = 1
        lblBuyAndMYGreenpass.layer.removeAllAnimations()
        lblBuyAndMYGreenpass.alpha = 1

        if appDelegate.isCallFirstTimeGetMychatList == true && appDelegate.isTapOnNotificationFromNotRunApp == false{
            appDelegate.isCallFirstTimeGetMychatList = false
            //call this function after launching app
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.myChatsList.removeAll()
                self.getMyChatList(isFullToRefresh:false)
        }else{
            self.myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
            if  self.myChatsList.isEmpty{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.myChatsList.removeAll()
                self.getMyChatList(isFullToRefresh:false)
            }else{
                self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
                //self.myChatsList.sort(by:{$0.last_activity_datetime! > $1.last_activity_datetime!} )
                
                self.myChatsListTemp = self.myChatsList
               // let myChatsListLocal = self.myChatsList
//                for i in (0..<myChatsListLocal.count){
//                    let mychat = myChatsListLocal[i]
//                    if  mychat.isSponsoredMessage == StringConstants.ONE{
//                        if self.userId == mychat.ownerId{
//                            self.myChatsList.remove(at: i)
//                            self.self.myChatsListTemp.remove(at: i)
//                        }
//                    }
//                }
                tblMyChatList.reloadData()
                self.hideShowDiscoverView()
            }
        }
        //Set yellow icon on bell to indication of new notification is received
        NotificationCenter.default.addObserver(self, selector: #selector(self.showYellowIconOnNotiBell(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        // Set flag for open notifiation drawer from right drawer
        let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.dashboardViewController]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
        
        // For open right drawer
        //self.setRightDrawerForNotification()
        //Left Drawer Actions & forword messages to channel
        setLeftDrawerActions()
        //For left Drawer
        self.setNavigationBarItem()
        setNavigation()
        
        //banner image localization
       /* let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        if preferredLanguage == "en" {
            btnBannerShow.setImage(UIImage(named: StringConstants.ImageNames.bannerEnglish), for: .normal)
        } else if preferredLanguage == "mr" {
            btnBannerShow.setImage(UIImage(named: StringConstants.ImageNames.bannerMarathi), for: .normal)
        }else{
            btnBannerShow.setImage(UIImage(named: StringConstants.ImageNames.bannerhindi), for: .normal)
        }*/
        
        
        let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)

        self.showGreenPassTilesAccordingly(pendingInviteCount: pendingInviteCount, myGreenPassCount: myGreenPassCount, isMitra: isMitra)

       // openPopUpConditionally()
        ButtonAnimationHelper().blinkView(view: lblGreenPassissueLastDate)
        ButtonAnimationHelper().blinkView(view: lblBuyAndMYGreenpass)
    }
    
    @objc func showYellowIconOnNotiBell(_ notification: NSNotification) {
        if let commingFrom = notification.userInfo?[StringConstants.NSUserDefauluterKeys.commingFromScreen] as? String {
            if  commingFrom == StringConstants.flags.fromJoinNotification{
                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.TRUE , keyString: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue)
                let isCommingFromNotification = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.setNotificationReceivedValue)
                self.setNavigationBarButtons(isCommingFromNotification:isCommingFromNotification ?? String())
            }
        }
    }
    
    @objc func setChannelProfileFrmNotification(_ notification: NSNotification) {
        if let myChatData = notification.userInfo?[StringConstants.pushNotificationConstants.myChatData] as? MyChatsTable {
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let channelProfileVC = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
            channelProfileVC.myChatsData = myChatData
            channelProfileVC.commingFrom = StringConstants.pushNotificationConstants.channelProfileFrmNotification
            self.navigationController?.pushViewController(channelProfileVC, animated: true)
        }
    }
    

    func getMyChatList(isFullToRefresh:Bool){
        let params = ["":""]
        print("params",params)
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getMyachatList + String(currentTime)
        _ = MyChatListServices().getMyChatList(apiURL,
                                               postData: params as [String : AnyObject],
                                               withSuccessHandler: { (userModel) in
                                                let model = userModel as! MyChatListResponse
                                                print("model",model)
                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                self.hideShowDiscoverView()
                                                self.refreshControl.endRefreshing()
                                                
                                                if isFullToRefresh == true{
                                                    self.myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
                                                    //self.myChatsList.sort(by:{$0.last_activity_datetime! > $1.last_activity_datetime!} )
                                                    self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
                                                    self.myChatsListTemp = self.myChatsList
                                                    self.tblMyChatList.reloadData()
                                                    self.hideShowDiscoverView()
                                                }

                                               /*self.myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
                                                 DispatchQueue.global(qos: .background).async {
                                                 for i in 0 ..< self.myChatsList.count {
                                                 let myChat = self.myChatsList[i]
                                                 self.getOldMessage(communityKey:myChat.communityKey!)
                                                 }
                                                 DispatchQueue.main.async {
                                                 self.myChatsList.[sort](by:{$0.messageAt! > $1.messageAt!} )
                                                 self.myChatsListTemp = self.myChatsList
                                                 self.hideShowDiscoverView()
                                                 self.tblMyChatList.reloadData()
                                                 }
                                                 }*/
                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    func getOldMessage (communityKey:String){
        let params = ["communityKey":communityKey]
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + "10" + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            self.fetOldMessageGroup.leave()
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            /*self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
             },{action2 in
             }, nil])*/
        })
    }
    
    

    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
       
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
               resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)

        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                               withSuccessHandler: { (userModel) in
                                                let model = userModel as! GreenCloudLoginResponse
                                                print("model",model)
                                              
                                                let pendingInviattionCount = model.pending_invitation_count
                                                let isMitra = model.isMitra
                                            
                                                let ProfileDetails = model.profileDetails[0]
                                                let sessionId = ProfileDetails.sessionId

                                                let eventStartDate = model.eventstartdatetime
                                                
                                                UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)

                                                UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.pendingInvitationCount)
                                                
                                                UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                
                                                
                                                self.getInviteSummaryAPI()

        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
//            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
//                },{action2 in
//                }, nil])
        })
    }
    
    
    
    //API call to get Invite Summary
    func getInviteSummaryAPI(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        
        //set Source and filter for both exhibitor and Kisan Mitra
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            var resource = String()
            var  source = String()
            if accountType == StringConstants.ONE {
                resource = RequestName.kisanMita
                source = RequestName.loginSourceName
            }else{
                resource = RequestName.ExhibitorInvite
                source = RequestName.loginSourceExhibitorName
            }
    
        let filter = [URLConstant.filter1: resource, URLConstant.filter2: userName]
        let params = InviteSummuryRequest.convertToDictionary(inviteSummuryRequest:InviteSummuryRequest.init(eventCode:URLConstant.eventCode , source: source, pagesize: 3, currentpage: 0, sort_col: "", app_key: URLConstant.app_key, sessionId: sessionId, filter: filter as [String : AnyObject]))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.inviteSummary
        _ = InviteSummaryServices().getInviteSummary(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! InviteSummaryResponse
                                                            print("model",model)
                                                            
                                                            let records = model.records
                                                            let inviteSummary = records[0]
                                                           
                                                            let settings = model.settings
                                                            let inviteSetting = settings[0]

                                                            var standard =  Int()
                                                            var extra = Int()
                                                            var sent = Int()
                                                            
                                                            let medialink =  inviteSummary.media_link
                                                            let mediatype = inviteSummary.media_type
                                                            let skip_media = inviteSummary.skip_media
                                                            let channelName = inviteSummary.channel_name
                                                            let channelColour = inviteSummary.channel_max_color
                                                            let mediathumb = inviteSummary.media_thumbnail
                                                            let imagechannelProfile = inviteSummary.channel_big_thumb
                                                            let issueGreenpasslastDate = inviteSetting.issue_greenpass_last_date
                                                            let greenpassAcceptLast_date = inviteSetting.greenpass_accept_last_date
                                                            
                                                            if(accountType == StringConstants.TWO){
                                                            if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 1) {
                                                                standard =  inviteSummary.unlimited_standard!
                                                                extra = inviteSummary.unlimited_extra!
                                                                sent = inviteSummary.unlimited_sent!
                                                            }else if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 0){
                                                                standard =  inviteSummary.quota_standard!
                                                                extra = inviteSummary.quota_extra!
                                                                sent = inviteSummary.quota_sent!
                                                            }
                                                            }else{
                                                                standard =  inviteSummary.unlimited_standard!
                                                                extra = inviteSummary.unlimited_extra!
                                                                sent = inviteSummary.unlimited_sent!
                                                            }
                                                            let show_issue_greenpass = inviteSetting.show_issue_greenpass

                                                            UserDefaults.standard.set(show_issue_greenpass, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                                                        
                                                            if inviteSetting.show_issue_unlimited_greenpass != nil {
                                                                UserDefaults.standard.set(inviteSetting.show_issue_unlimited_greenpass!, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                            }else{
                                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                            }
                                                            
                                                            UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                            
                                                            UserDefaults.standard.set(extra, forKey:
                                                                StringConstants.NSUserDefauluterKeys.extra)
                                                            
                                                            UserDefaults.standard.set(sent, forKey:
                                                                StringConstants.NSUserDefauluterKeys.sent)

                                                            
                                                            UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                            
                                                            UserDefaults.standard.set(issueGreenpasslastDate, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                                                                                        UserDefaults.standard.set(greenpassAcceptLast_date, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
 
                                                            UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                            
                                                            UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                            
                                                            
                                                            UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                            
                                                            UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                            
                                                            UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                            
                                                            UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)



                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
//            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
//                },{action2 in
//                }, nil])
        })
    }
    
    
    //API call to get Invite Summary
    func getAppStarterDataAPI(){
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginSourceName
        let params = AppstarterRequest.convertToDictionary(appstarterRequest:AppstarterRequest.init(app_key: URLConstant.app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.appStarterdata
        _ = AppStarterServices().getAppStarterData(apiURL,postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! AppStarterResponse
                                                        print("model",model)
                                                        
                                                        let data = model.data[0]
                                                        let pendingInvite = data.pending_invitation_count
                                                        self.pendingInvitecount =  data.pending_invitation_count!
                                                        let myGreenPassCount = data.greenpass_count
                                                        self.myGreenpasCoint =  data.greenpass_count!
                                                        
                                                        UserDefaults.standard.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
                                                        
                                                        UserDefaults.standard.set(myGreenPassCount, forKey:
                                                            StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                        
                                                        self.openPopUpConditionally()

                                                        let isMitra = data.isMitra
                                                        self.showGreenPassTilesAccordingly(pendingInviteCount: pendingInvite!, myGreenPassCount: myGreenPassCount!, isMitra: isMitra!)
                                                        let defaults=UserDefaults.standard
                                                        defaults.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                        defaults.set(myGreenPassCount, forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount)
                                                        defaults.synchronize()
                                                        /*if pendingInvite! > 0{
                                                            if ((UserDefaults.standard.value(forKey: "isNotFirstTime")) != nil) && UserDefaults.standard.value(forKey: "isNotFirstTime") as! Bool == true{
                                                                print("not First time show congratulation")
                                                                UserDefaults.standard.set(false, forKey: "isNotFirstTime")
                                                            }else{
                                                                print("First time show congratulation")
                                                                UserDefaults.standard.set(true, forKey: "isNotFirstTime")
                                                                let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                                                                self.navigationController?.pushViewController(vc, animated: false)
                                                            }
                                                        }*/
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }
    
    
    func showGreenPassTilesAccordingly(pendingInviteCount:Int,myGreenPassCount:Int,isMitra:Bool){
      
        viewOfGreenpassBanner.isHidden = true
        heightOfBannerButton.constant = 0
        topOfTblMychListToSelfView.constant = 0

        
      /*  let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        if accountType == StringConstants.ONE {
            viewOfGreenpassBanner.isHidden = false
            heightOfBannerButton.constant = 62
            topOfTblMychListToSelfView.constant = 90

            if(pendingInviteCount>0){
                viewOfGreenpassBanner.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimaryYellow)
                imgArrowGoBtn.image = UIImage(named: StringConstants.ImageNames.arrowGoYellow)
                viewOfPendingInvites.isHidden = false
                lblBuyAndMYGreenpass.isHidden = true
                lblPendingInviteCount.text = StringConstants.kisanMitra.acceptGreenpass + StringConstants.singleSpace + StringConstants.kisanMitra.openBrace +  String(pendingInviteCount) + StringConstants.kisanMitra.closeBrace
                
                let greenpassAcceptLast_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
                
                if !(greenpassAcceptLast_date.isEmpty){
                    getDateInFormat(dateStr: greenpassAcceptLast_date)
                }

            }else if (myGreenPassCount>0){
                viewOfGreenpassBanner.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colordarkPrimary)
                imgArrowGoBtn.image = UIImage(named: StringConstants.ImageNames.arrowGoGreen)
                viewOfPendingInvites.isHidden = true
                lblBuyAndMYGreenpass.isHidden = false
                lblBuyAndMYGreenpass.text = StringConstants.kisanMitra.viewGreenPass + StringConstants.singleSpace + StringConstants.kisanMitra.openBrace +  String(myGreenPassCount) + StringConstants.kisanMitra.closeBrace

                
            }else{
                viewOfGreenpassBanner.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colordarkPrimary)
                imgArrowGoBtn.image = UIImage(named: StringConstants.ImageNames.arrowGoGreen)
                viewOfPendingInvites.isHidden = true
                lblBuyAndMYGreenpass.isHidden = false
                lblBuyAndMYGreenpass.text = StringConstants.kisanMitra.buyGreenpass

            }
        }else{
            viewOfGreenpassBanner.isHidden = true
            heightOfBannerButton.constant = 0
            topOfTblMychListToSelfView.constant = 0
        }*/
    }
    
    func getDateInFormat(dateStr:String){
        
        /* let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
         let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
         let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
         let remaining = (standard + extra) - sent*/
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: dateStr)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        lblGreenPassissueLastDate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
    }
    
    func hideShowDiscoverView(){
        if self.myChatsList.count == 0 {
            self.viewOfStartDiscovering.isHidden = false
            if let firstName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName) {
                self.lblOfUserNameOnDiscoverView.text = (NSLocalizedString(StringConstants.DashboardViewController.Hi, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + firstName + StringConstants.singleSpace + "!"
            }
            self.tblMyChatList.isHidden = true
        }else{
            self.viewOfStartDiscovering.isHidden = true
            self.tblMyChatList.isHidden = false
        }
    }
    
    
    func setLeftDrawerActions() {
        if commingFrom == StringConstants.flags.leftDrawerShare{
            commingFrom = ""
            self.shareActionFromDawer()
        }else if commingFrom == StringConstants.flags.leftDrawerLogout{
            commingFrom = ""
            popupAlert(title: "", message:  (NSLocalizedString(StringConstants.AlertMessage.logoutConfirmation, comment: StringConstants.EMPTY)), actionTitles: [ (NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.logoutActionFromDawer()
                }, nil])
        }
        else if commingFrom == StringConstants.flags.leftDrawerKisan2018
        {
            commingFrom = StringConstants.EMPTY
            let greenpassfinalurl = URLConstant.greenPassbaseUrl + URLConstant.greenpassToken + AuthToken
            let url = URL(string: greenpassfinalurl)
            let controller = SVWebViewController(url: url)
            self.navigationController?.pushViewController(controller!, animated: true)
            /*if let url = URL(string: greenpassfinalurl) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                vc.preferredBarTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                vc.preferredControlTintColor = UIColor.white
                vc.transitioningDelegate = self
                vc.dismissButtonStyle = .close
                vc.view.tintColor = UIColor.red
                present(vc, animated: false)
            }*/
            
        }
    }
    
    func logoutCallAPI(){
        
        let userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let params = LogOutFromAppRequest.convertToDictionary(louOutUser: LogOutFromAppRequest.init(userID: userId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.logoutUser
        _ = LogoutUserServices().logOutUser(apiURL,postData: params as [String : AnyObject],
                                            withSuccessHandler: { (userModel) in
                                                print("model",userModel as Any)
                                                
                                                //Clear the local DB
                                                for  j in (0..<self.arrOfEntityName.count){
                                                    let strEntityName = self.arrOfEntityName[j]
                                                    self.resetAllRecords(in: strEntityName)
                                                }
                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                // Clear session
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.accountType)

                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.token)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.username)
                                                
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.userId)
                                                
                                                UserDefaults.standard.set(false, forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.channel_min_color)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.imageurl)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)

                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.pendingInvitationCount)
                                                
                                                UserDefaults.standard.set(0, forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)

                                                UserDefaults.standard.set(false, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.extra)

                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.sent)
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                
                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)


                                                UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.pin)

                                                //Logout from Ejabberd
                                                self.appDelegate.xmppController.xmppStream.disconnect()
                                                
                                                self.appDelegate.isLaunchFirstTime = true
                                                //Logout from fb account kit
                                                //accountKit.logOut()
                                                self.navigationController?.view.makeToast( (NSLocalizedString(StringConstants.ToastViewComments.logout, comment: StringConstants.EMPTY)))
                                                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                                if let nav = self.navigationController {
                                                    for controller in nav.viewControllers as Array {
                                                        if controller is HomeViewController {
                                                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                            break
                                                        }
                                                    }
                                                    nav.pushViewController(vc, animated: false)
                                                    return
                                                }
                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    
    
    //Sfsafari Delegate
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    //--------------------------- Forword messages -------------------------------------//
    func forwardMessage(indexPathValue: IndexPath){
        self.setMultiMediaPart(mediaType:(forwardedmessageData?.mediaType)!,indexPathValue: indexPathValue)
    }
    
    func setMultiMediaPart(mediaType:String,indexPathValue: IndexPath){
        
        myChatsOfForwordData = myChatsList[indexPathValue.row]
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        if forwardedmessageData?.mediaType == StringConstants.MediaType.image{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            let url = URL(string:forwardedmessageData!.contentField2!)
            if let data = try? Data(contentsOf: url!){
                imageData = (data as NSData) as Data
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original image to Amzon S3
                if myChatsOfForwordData.ownerId == userId{
                    originalMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + images + "/" + messageKeyForSendMultiMedia
                }else{
                    if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                        originalMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + images + "/" + messageKeyForSendMultiMedia
                    }else{
                        originalMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + images + "/" + messageKeyForSendMultiMedia
                    }
                }
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save Image at docoument directory
                S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + imagePrefix + String(messageTimeForSendMultiMedia)
                let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imageData as NSData)
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:true,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithImagePrefix)
            }
            
        }else if forwardedmessageData?.mediaType == StringConstants.MediaType.video {
            
            let imageHelper = ImageHelper()
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            guard let compressedData = NSData(contentsOf: URL(string:forwardedmessageData!.contentField2!)!) else {
                return
            }
            print("File size after compression: \(Double(compressedData.length / 1024)) kb")
                DispatchQueue.main.async {
                //let video = try NSData(contentsOf: self.appDelegate.selectedVideoURL!, options: .mappedIfSafe)
                let video = compressedData
                // Total Size in KB
                self.mediaSize = Double(video.length) / 1024.0
                print("mediaSize",self.mediaSize)
                let imageThumb = imageHelper.getThumbnailImage(forUrl: URL(string:self.forwardedmessageData!.contentField2!)!)
                self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                self.messageKeyForSendMultiMedia = strUUID
                self.messageTimeForSendMultiMedia = String(currentTime)
                //Send original image to Amzon S3
                if self.myChatsOfForwordData.ownerId == self.userId{
                    self.originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                }else{
                    if let myChatsValue = self.myChatsOfForwordData.userCommunityJabberId{
                        self.originalMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                    }else{
                        self.originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                    }
                }
                
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save Image at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            }
            
        }else if forwardedmessageData?.mediaType == StringConstants.MediaType.audio {
            
            do {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                let url = URL(string:forwardedmessageData!.contentField2!)
                let audio = try NSData(contentsOf: url!, options: .mappedIfSafe)
                // Total Size in KB
                mediaSize = Double(audio.length) / 1024.0
                let formatter = NumberFormatter()
                formatter.numberStyle = .none
                formatter.maximumFractionDigits = 0
                let formattedAmount = formatter.string(from: mediaSize as NSNumber)!
                print(mediaSize)
                mediaSize = Double(formattedAmount)!
                
                print(mediaSize)
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original audio file to Amzon S3
                // originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + audios + "/" + messageKeyForSendMultiMedia
                if self.myChatsOfForwordData.ownerId == self.userId{
                    self.originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + audios + "/" + messageKeyForSendMultiMedia
                }else{
                    if let myChatsValue = self.myChatsOfForwordData.userCommunityJabberId{
                        self.originalMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + audios + "/" + messageKeyForSendMultiMedia
                    }else{
                        self.originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + audios + "/" + messageKeyForSendMultiMedia
                    }
                }
                
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save audio file at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + audioPrefix + String(self.messageTimeForSendMultiMedia)
                let audioFileName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioFileName , sourceType: StringConstants.MediaType.audio,imageData:audio as NSData)
                uploadMediaPartToS3.uploadImage(with: audio as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.audio,callAgain:false,contentType:audioContentType, uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            } catch {
                print("Error")
            }
            
        }else if forwardedmessageData?.messageType == StringConstants.MediaType.location{
            self.forwordLocationAndTextMessage()
        }else if (forwardedmessageData?.mediaType  == StringConstants.EMPTY || forwardedmessageData?.mediaType  == StringConstants.MessageType.text){
            self.forwordLocationAndTextMessage()
        }else if forwardedmessageData?.mediaType == StringConstants.MediaType.link {
            self.forwordLocationAndTextMessage()
        }else if forwardedmessageData?.mediaType == StringConstants.MediaType.document {
            
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            let myURL = URL(string: (forwardedmessageData?.contentField2)!)
            let currentTime = DateTimeHelper.getCurrentMillis()
            let strUUID = UUIDHelper.getUUID()
            do{
                pdfDocData = try NSData(contentsOf: myURL!, options: .mappedIfSafe) as Data
               // let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
              //  spinnerActivity.isUserInteractionEnabled = false
                
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original image to Amzon S3
                
                if self.myChatsOfForwordData.ownerId == self.userId{
                    originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia
                }else{
                    if let myChatsValue = self.myChatsOfForwordData.userCommunityJabberId{
                        originalMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + documents + "/" + messageKeyForSendMultiMedia
                    }else{
                        originalMediaBucketName = S3BucketName + channelMedia + self.myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia
                    }
                }
                
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Selected document name
                print("document name",myURL!.lastPathComponent)
                documentName = myURL!.lastPathComponent
                //Save Image at docoument directory  pdfPrfiex
                var docName = String()
                if myURL!.pathExtension == pdf {
                    //Generate PDF Thumb data (Used third party for generate thubnail of pdf)
                    let thumbnail = ROThumbnail.sharedInstance
                    let documentURL = myURL
                    let pdfThumbImage = thumbnail.getThumbnail(documentURL!)
                    pdfThumbData = UIImagePNGRepresentation(pdfThumbImage) ?? Data()
                    S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + pdfPrfiex  + String(messageTimeForSendMultiMedia)
                    docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".pdf"
                    documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: StringConstants.MediaType.documentspdf,imageData:pdfThumbData as NSData)
                    let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + ".pdf"
                    uploadMediaPartToS3.uploadImage(with: pdfDocData as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:true,contentType:pdfContentType,uploadKeyName:uploadKeyName)
                }else{
                    S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + documentPrefix + String(messageTimeForSendMultiMedia)
                    let documentType = myURL!.pathExtension
                    docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + documentType
                    documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: documentType,imageData:pdfDocData as NSData)
                    let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + "." + documentType
                    uploadMediaPartToS3.uploadImage(with: pdfDocData as Data, bucketName:originalMediaBucketName,mediaSource:documentType,callAgain:true,contentType:docContentType,uploadKeyName:uploadKeyName)
                    
                }
                
            }catch{
            }
        }
    }
    
    func forwordLocationAndTextMessage(){
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let messageKey = strUUID
        
        if myChatsOfForwordData.ownerId == userId {
            //Forwordmessage At channelBroadcast
            createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
            
            createSendMessageRequest = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
            // Send Message to local db and xmpp
            self.forwordMessageToDBandXMPP(myChats:myChatsOfForwordData,messageKey:messageKey)
        }else{
            
            if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                //Forword message at one to one
                createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsValue, jabberId: myChatsValue, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                
                createSendMessageRequest = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsValue, jabberId: myChatsValue, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                
                // Send Message to local db and xmpp
                self.forwordMessageToDBandXMPP(myChats:myChatsOfForwordData,messageKey:messageKey)
            }else{
                
                // forward message at oneToone, This is first one to one chat message. So, initiate oneto one chat message
                createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityKey!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                
                createSendMessageRequest = buildRequestForSendMessage(id: messageKey, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: forwardedmessageData?.contentField2, contentField3: forwardedmessageData?.contentField3, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: forwardedmessageData?.contentField6, contentField7: forwardedmessageData?.contentField7, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityKey!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                
                MBProgressHUD.hide(for: self.view, animated: true);
                /* SessionDetailsForCommunityType.shared.globalMyChatsData = myChatsOfForwordData
                 let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
                 oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: myChatsOfForwordData.communityName!)
                 oneToOneFromAdminVC.communityKey = myChatsOfForwordData.communityKey!
                 oneToOneFromAdminVC.communityImageSmallThumbUrl = myChatsOfForwordData.communityImageSmallThumbUrl!
                 //oneToOneFromAdminVC.dominantColour = myChats.dominantColour
                 oneToOneFromAdminVC.ownerId = SessionDetailsForCommunityType.shared.globalMyChatsData.ownerId! //myChatsData.ownerId!
                 oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
                 self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)*/
                
                
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendMessage(createSendMessageRequestData: createSendMessageRequest, createSendMessageRequestInDB: createSendMessageRequestInDB,currentTime:currentTime,messageKey:messageKey,myChats:myChatsOfForwordData)
            }
        }
    }
    
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
        if callAgain == true{
            //Send Thumb image to Amzon S3
            let uploadMediaPartToS3  = UploadMediaPartToS3()
            uploadMediaPartToS3.delegate = self
            
            if mediaSource == StringConstants.MediaType.image{
                if myChatsOfForwordData.ownerId == userId{
                    thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                }else{
                    if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    }else{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    }
                }
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.video{
                
                if myChatsOfForwordData.ownerId == userId{
                    thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                }else{
                    if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                        
                    }else{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    }
                }
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: thumbVideoData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                
                if myChatsOfForwordData.ownerId == userId{
                    thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                }else{
                    if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsValue + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    }else{
                        thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                    }
                }
                
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + ".pdf"
                uploadMediaPartToS3.uploadImage(with: pdfThumbData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:false,contentType:pdfContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.doc{
                thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.doc,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.docx{
                thumbMediaBucketName = S3BucketName + channelMedia + myChatsOfForwordData.communityKey! + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName =  S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.docx,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }
            
        }else{
            
            var strChatMessage = String()
            var mediaName = String()
            var mediaType = String()
            var contentField9 = String()
            let currentTime = DateTimeHelper.getCurrentMillis()
            
            if mediaSource == StringConstants.MediaType.image{
                let imageHelper = ImageHelper()
                // Total Size in KB
                mediaSize = imageHelper.getImageSize(imgData: imageData as NSData)
                print("mediaSize",mediaSize)
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                // Bucket name after adding Base URL & Image name
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.image
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.video{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp4"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.video
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.audio{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.audio
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".pdf"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + ".pdf"
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix + ".pdf"
                mediaType = StringConstants.MediaType.document
                contentField9 = pdf
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + ".pdf"
            }else if mediaSource == StringConstants.MediaType.doc{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }else if mediaSource == StringConstants.MediaType.docx{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }
            
            
            if myChatsOfForwordData.ownerId == userId {
                //Forwordmessage At channelBroadcast
                createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                
                createSendMessageRequest = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                // Send Message to local db and xmpp
                self.forwordMessageToDBandXMPP(myChats:myChatsOfForwordData,messageKey:messageKeyForSendMultiMedia)
            }else{
                
                if let myChatsValue = myChatsOfForwordData.userCommunityJabberId{
                    //Forword message at one to one
                    createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsValue, jabberId: myChatsValue, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                    
                    createSendMessageRequest = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsValue, jabberId: myChatsValue, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                    
                    // Send Message to local db and xmpp
                    self.forwordMessageToDBandXMPP(myChats:myChatsOfForwordData,messageKey:messageKeyForSendMultiMedia)
                }else{
                    
                    // forward message at oneToone, This is first one to one chat message. So, initiate oneto one chat message
                    createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: forwardedmessageData?.contentField4, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityKey!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.pending, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                    
                    createSendMessageRequest = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: forwardedmessageData?.mediaType, messageType: forwardedmessageData?.messageType, contentField1: forwardedmessageData?.contentField1, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: StringConstants.EMPTY, contentField5: forwardedmessageData?.contentField5, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: forwardedmessageData?.contentField8, contentField9: forwardedmessageData?.contentField9, contentField10: forwardedmessageData?.contentField10, senderId: userId, communityId: myChatsOfForwordData.communityKey!, jabberId: myChatsOfForwordData.communityKey!, createdDatetime: String(currentTime), hiddenDatetime: forwardedmessageData?.hiddenDatetime, deletedDatetime: forwardedmessageData?.deletedDatetime, hiddenbyUserId: forwardedmessageData?.hiddenbyUserId, deletedbyUserId: forwardedmessageData?.deletedbyUserId, messageStatus: StringConstants.Status.sent, communityName: forwardedmessageData?.communityName, communityProfileUrl: forwardedmessageData?.communityProfileUrl)
                    
                    MBProgressHUD.hide(for: self.view, animated: true);
                    //MBProgressHUD.hide(for: self.view, animated: true);
                   /* SessionDetailsForCommunityType.shared.globalMyChatsData = myChatsOfForwordData
                    let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
                    oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: myChatsOfForwordData.communityName!)
                    oneToOneFromAdminVC.communityKey = myChatsOfForwordData.communityKey!
                    oneToOneFromAdminVC.communityImageSmallThumbUrl = myChatsOfForwordData.communityImageSmallThumbUrl!
                    //oneToOneFromAdminVC.dominantColour = myChats.dominantColour
                    oneToOneFromAdminVC.ownerId = SessionDetailsForCommunityType.shared.globalMyChatsData.ownerId! //myChatsData.ownerId!
                    oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
                    self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)*/
                    
                    
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.sendMessage(createSendMessageRequestData: createSendMessageRequest, createSendMessageRequestInDB: createSendMessageRequestInDB,currentTime:currentTime,messageKey:messageKeyForSendMultiMedia,myChats:myChatsOfForwordData)
                }
            }
            
            
        }
    }
    
    
    func PassMultiMediaFail(_ errorMsg: String) {
        MBProgressHUD.hide(for: self.view, animated: true);
        self.popupAlert(title: "", message: errorMsg, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    
    
    func forwordMessageToDBandXMPP(myChats:MyChatsTable,messageKey:String){
        //save message in core data as local DB with pending status
        let messagesDB =  MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
        print("messagesDB",messagesDB)
        MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let sendMessagesServices  = SendMessagesServices()
            if  myChats.ownerId == userId {
                sendMessagesServices.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: messageKey, communityJabberId:myChats.communityJabberId! )
            }else{
                sendMessagesServices.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: messageKey, communityJabberId:myChats.userCommunityJabberId! )
            }
        }
        
        if myChats.ownerId == userId {
            MBProgressHUD.hide(for: self.view, animated: true);
            MBProgressHUD.hide(for: self.view, animated: true);
            let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
            channelSendBroadcastMsgVC.communityName = myChats.communityName!
            channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
            channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
            channelSendBroadcastMsgVC.myChatsData = myChats
            self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: false)
        }else{
            
            MBProgressHUD.hide(for: self.view, animated: true);
            MBProgressHUD.hide(for: self.view, animated: true);
            SessionDetailsForCommunityType.shared.globalMyChatsData = myChats
            let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
            oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: myChats.communityName!)
            oneToOneFromAdminVC.communityKey = myChats.communityKey!
            oneToOneFromAdminVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
            //oneToOneFromAdminVC.dominantColour = myChats.dominantColour!
            oneToOneFromAdminVC.ownerId = SessionDetailsForCommunityType.shared.globalMyChatsData.ownerId! //myChatsData.ownerId!
            oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
            self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)
        }
    }
    
    // Intitated to one to one chat message
    func sendMessage(createSendMessageRequestData:Dictionary<String, Any>,createSendMessageRequestInDB:Dictionary<String, Any>,currentTime:Int64,messageKey:String,myChats:MyChatsTable){
        
        let firstName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        let lastName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        let username = firstName + StringConstants.singleSpace + lastName
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
        let country = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.country)
        let imageBigthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
        let imageSmallthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
        let imageUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageUrl)
        let userDetails = CreateOneToOneChatRequest.convertToDictionary(createOneToOneChatRequest: CreateOneToOneChatRequest.init(username: username, accountType: Int(accountType)!, city: city, country: country, imageBigthumbUrl: imageBigthumbUrl, imageSmallthumbUrl: imageSmallthumbUrl, imageUrl: imageUrl))
        let currentTime = DateTimeHelper.getCurrentMillis()
        let oneToOneKey = RequestName.loginSourceName + String(currentTime)
        let params = CreateOneToOneChatDetailRequest.convertToDictionary(CreateOneToOneChatDetailRequest: CreateOneToOneChatDetailRequest.init(userDetails: userDetails, communityKey: myChats.communityKey!, oneToOneKey: oneToOneKey, memberId: myChats.ownerId!))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.usersCurrentOnetoOne
        _ = CreateOneToOneChatServices().createOneToOneChat(apiURL,
                                                            postData: params as [String : AnyObject],
                                                            withSuccessHandler: { (userModel) in
                                                                let model = userModel as! MyChatListResponse
                                                                let message = model.message
                                                                let oneToOneCommunityKey = model.communityKey!
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                                
                                                                
                                                                //send to one to one initiate by forwarding message
                                                                
                                                                SessionDetailsForCommunityType.shared.globalMyChatsData = self.myChatsOfForwordData
                                                                 let  oneToOneFromAdminVC = self.storyboard?.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
                                                                oneToOneFromAdminVC.communityName = StringUTFEncoding.UTFEncong(string: self.myChatsOfForwordData.communityName!)
                                                                oneToOneFromAdminVC.communityKey = self.myChatsOfForwordData.communityKey!
                                                                oneToOneFromAdminVC.communityImageSmallThumbUrl = self.myChatsOfForwordData.communityImageSmallThumbUrl!
                                                                 //oneToOneFromAdminVC.dominantColour = myChats.dominantColour
                                                                 oneToOneFromAdminVC.ownerId = SessionDetailsForCommunityType.shared.globalMyChatsData.ownerId! //myChatsData.ownerId!
                                                                 oneToOneFromAdminVC.myChatsData = SessionDetailsForCommunityType.shared.globalMyChatsData //myChatsData
                                                                 self.navigationController?.pushViewController(oneToOneFromAdminVC, animated: true)
                                                                
                                                                //send to one to one initiate by forwarding message

                                                                
                                                                print("message",message ?? (Any).self)
                                                                //Update value of userCommunityJabberId after oneToone chat init
                                                                SessionDetailsForCommunityType.shared.globalMyChatsData.setValue(oneToOneCommunityKey, forKey: "userCommunityJabberId")
                                                                
                                                                //Save message in Message Tbl of local device
                                                                var createSendMesgRequestInDB = createSendMessageRequestInDB
                                                                createSendMesgRequestInDB.updateValue(oneToOneCommunityKey, forKey: "communityId")
                                                                createSendMesgRequestInDB.updateValue(oneToOneCommunityKey, forKey: "jabberId")
                                                                self.createSendMessageRequestInDB.removeAll()
                                                                self.createSendMessageRequestInDB = createSendMesgRequestInDB
                                                                print("self.createSendMessageRequestInDB",self.createSendMessageRequestInDB)
                                                                let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMesgRequestInDB, saveDataAtDB: true)
                                                                print("messagesDB.communityId",messagesDB.communityId!)
                                                                //Save message in core data as local DB with pending status
                                                                MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
                                                                
                                                                //Change communityId & communityJabberId for send message to ejjaberd
                                                                var createOneToOneSendMessageRequest = createSendMessageRequestData
                                                                createOneToOneSendMessageRequest.updateValue(oneToOneCommunityKey, forKey: "communityId")
                                                                createOneToOneSendMessageRequest.updateValue(oneToOneCommunityKey, forKey: "jabberId")
                                                                print("createOneToOneSendMessageRequest",createOneToOneSendMessageRequest)
                                                                
                                                                let sendMessagesServices  = SendMessagesServices()
                                                                sendMessagesServices.groupChat(createSendMessageRequest: createOneToOneSendMessageRequest, messageGuid: messageKey, communityJabberId:oneToOneCommunityKey )
                                                                
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    func buildRequestForSendMessage(id: String, mediaType: String?, messageType: String?, contentField1: String?, contentField2: String?, contentField3: String?, contentField4: String?, contentField5: String?, contentField6: String?, contentField7: String?, contentField8: String?, contentField9: String?, contentField10: String?, senderId: String?, communityId: String?, jabberId: String?, createdDatetime: String?, hiddenDatetime: String?, deletedDatetime: String?, hiddenbyUserId: String?, deletedbyUserId: String?,messageStatus:String?,communityName:String?,communityProfileUrl:String?)-> Dictionary<String, Any>{
        let  createSendMessageRequest = MessageRequest.convertToDictionary(sendMessage: MessageRequest.init(id: id, mediaType: mediaType, messageType: messageType, contentField1: contentField1, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: contentField5, contentField6: contentField6, contentField7: contentField7, contentField8: contentField8, contentField9: contentField9, contentField10: contentField10, senderId: senderId, communityId: communityId, jabberId: jabberId, createdDatetime: createdDatetime, hiddenDatetime: hiddenDatetime, deletedDatetime: deletedDatetime, hiddenbyUserId: hiddenbyUserId, deletedbyUserId: deletedbyUserId,messageStatus:messageStatus, communityName: communityName, communityProfileUrl: communityProfileUrl))
        print("createSendMessageRequest",createSendMessageRequest)
        return createSendMessageRequest
    }
    
    //-------------------------------------------------------------------------------------//
    
    func shareActionFromDawer(){
        
        DispatchQueue.main.async {
            // text to share
            let text =  URLConstant.SHARING_URL
            // set up activity view controller
            let textToShare = [ text ]
            let items = [self]
            let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            // activityViewController.setValue("This is my title", forKey: "Subject")
            // activityViewController.setValue("This is my title", forKey: "Subject")
            //activityViewController.setValue("" , forKey: "subject") ;
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.assignToContact ]
            
            
            // present the view controller
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return StringConstants.EMPTY
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType?) -> Any? {
        let text =  URLConstant.SHARING_URL
        return text
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
        return StringConstants.DashboardViewController.MailSubject
    }
    
    
    func logoutActionFromDawer(){
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
             logoutCallAPI()
        }else{
         self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
           MBProgressHUD.hide(for: self.view, animated: true);

        }
    }
    
    func resetAllRecords(in entity : String){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    @objc func btnNextTapped(sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let DiscoverChannelVC = storyboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as UIViewController
        self.navigationController?.pushViewController(DiscoverChannelVC, animated: true)
    }
    
    
    @IBAction func btnDisciverClickOnDiscView(_ sender: Any) {
        let discoverChannelVC = self.storyboard?.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
        self.navigationController?.pushViewController(discoverChannelVC, animated: true)
    }
    
    @IBAction func btnBanerShowClick(_ sender: Any) {
        
        
        if(self.pendingInvitecount>0){
            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "PassBaseVC")as!PassBaseVC
            vc.isFromViewAccept=true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (self.myGreenpasCoint>0){
            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "PassBaseVC")as!PassBaseVC
            vc.isFromViewAccept=false
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let greenpassfinalurl = URLConstant.greenPassbaseUrl + URLConstant.greenpassToken + AuthToken
            let url = URL(string: greenpassfinalurl)
            let controller = SVWebViewController(url: url)
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    
    }

}

extension DashboardViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblMyChatList{
            return self.myChatsList.count
        }else{
            return self.arrOfMenus.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblMyChatList{
            let myChats = myChatsList[indexPath.row]
            var communityType = String()
            if let communityTypeVal = myChats.type{
                communityType = communityTypeVal
            }
            var communityPrivacy = String()
            if let checkPrivacy = myChats.privacy, !checkPrivacy.isEmpty {
                communityPrivacy = checkPrivacy
            }
            var type = StringConstants.CommunityType.GROUP
            switch communityType {
            case StringConstants.CommunityType.G:
                type = StringConstants.CommunityType.GROUP
            case StringConstants.CommunityType.C:
                type = StringConstants.CommunityType.CHANNEL
            default:break
            }
            var privacy = StringConstants.CommunityPrivacy.publicPrivacy
            switch communityPrivacy {
            case StringConstants.CommunityPrivacy.privatePrivacy:
                privacy = StringConstants.CommunityPrivacy.PRIVATE
            case StringConstants.CommunityPrivacy.publicPrivacy:
                privacy = StringConstants.CommunityPrivacy.PUBLIC
            default:break
            }
          

            if myChats.isSponsoredMessage == StringConstants.ZERO || myChats.isSponsoredMessage == nil{
                 let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatListTblCell", for: indexPath) as! MyChatListTblCell
                 cell.selectionStyle = .none
                 cell.imgCommunityProfile.layer.cornerRadius = cell.imgCommunityProfile.frame.size.width / 2
                 cell.imgCommunityProfile.clipsToBounds = true
                 if let communityName = myChats.communityName, !communityName.isEmpty {
                 cell.lblCommunityName.text = communityName
                 }
                 if let communityImageBigThumbUrl = myChats.communityImageBigThumbUrl{
                 if communityImageBigThumbUrl == StringConstants.EMPTY{
                 if myChats.feed == StringConstants.ONE{
                 cell.imgCommunityProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                 }else{
                 cell.imgCommunityProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                 }
                 }else{
                 let imgurl = communityImageBigThumbUrl
                 let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                 cell.imgCommunityProfile.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
                 }
                 }else{
                 if myChats.feed == StringConstants.ONE{
                 cell.imgCommunityProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                 }else{
                 cell.imgCommunityProfile.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                 }
                 }
                 cell.lblCommunityType.text = String(format: "%@ %@", privacy, type)
                 if (myChats.messageAt != nil) && !(myChats.messageAt?.isEmpty)! {
                 let time = DateTimeHelper.getTimeAgo(time: myChats.messageAt!)//last_activity_datetime
                 cell.lblMessageDateTime.isHidden = false
                 cell.lblMessageDateTime?.text = time
                 }else{
                 cell.lblMessageDateTime.isHidden = true
                 }
                 cell.lblLatestMessage.isHidden = false
                 
                 if myChats.messageType == StringConstants.MessageType.post{
                 cell.lblLatestMessage?.text = NSLocalizedString(StringConstants.popUpMenuName.post, comment: "")
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.postBlack)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 7
                 cell.widthOfLatestMsgTypeImgView.constant = 15
                 cell.heightOfLatestMsgTypeImgView.constant = 15
                 cell.topOfLatestMesgLabel.constant = 12
                 }else if myChats.mediaType == StringConstants.MediaType.image{
                 cell.lblLatestMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.image, comment: "")
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.ic_camera_dark_gray_16dp)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 7
                 cell.widthOfLatestMsgTypeImgView.constant = 20
                 cell.heightOfLatestMsgTypeImgView.constant = 20
                 cell.topOfLatestMesgLabel.constant = 12
                 }else if myChats.mediaType == StringConstants.MediaType.video{
                 cell.lblLatestMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.video, comment: "")
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.videoImage)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 7
                 cell.widthOfLatestMsgTypeImgView.constant = 20
                 cell.heightOfLatestMsgTypeImgView.constant = 12
                 cell.topOfLatestMesgLabel.constant = 16
                 }else if myChats.mediaType == StringConstants.MediaType.audio{
                 cell.lblLatestMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.audio, comment: "")
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.headphone_16px)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 9
                 cell.widthOfLatestMsgTypeImgView.constant = 18
                 cell.heightOfLatestMsgTypeImgView.constant = 18
                 cell.topOfLatestMesgLabel.constant = 12
                 }else if myChats.messageType == StringConstants.MessageType.mapLocation{
                 cell.lblLatestMessage?.text = NSLocalizedString(StringConstants.DashboardViewController.location, comment: "")
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.locationDash)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 9
                 cell.widthOfLatestMsgTypeImgView.constant = 13
                 cell.heightOfLatestMsgTypeImgView.constant = 19
                 cell.topOfLatestMesgLabel.constant = 12
                 }else if (myChats.messageType == StringConstants.MessageType.chatMessage || myChats.messageType == StringConstants.MessageType.banner) && (myChats.mediaType  == StringConstants.EMPTY || myChats.mediaType  == StringConstants.MessageType.text) {
                 if let messageText: String = myChats.messageText {
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = true
                 if myChats.ownerId == userId {
                 cell.lblLatestMessage?.text = messageText // StringUTFEncoding.UTFEncong(string:messageText)
                 }else{
                 cell.lblLatestMessage?.text = messageText
                 }
                 //new code
                 if messageText == StringConstants.DashboardViewController.follwStatus{
                 cell.lblLatestMessage?.text =  NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
                 }else if messageText == StringConstants.DashboardViewController.unfollowStatus{
                 cell.lblLatestMessage?.text =  NSLocalizedString(StringConstants.DashboardViewController.unfollowStatus, comment: "")
                 }else if messageText == StringConstants.DashboardViewController.updateChannel{
                 cell.lblLatestMessage?.text = (NSLocalizedString(StringConstants.DashboardViewController.updateChannel, comment: StringConstants.EMPTY))
                 }else{
                 cell.lblLatestMessage?.text = messageText
                 }
                 cell.leadingOflblLatestMsg.constant = 7
                 }else{
                 cell.lblLatestMessage?.text = StringConstants.EMPTY
                 cell.topOfLatestMesgLabel.constant = 12
                 }
                 }else if (myChats.mediaType == StringConstants.MediaType.document ) {
                 if let messageText: String = myChats.messageText {
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.document)
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 9
                 cell.widthOfLatestMsgTypeImgView.constant = 18
                 cell.heightOfLatestMsgTypeImgView.constant = 17
                 cell.topOfLatestMesgLabel.constant = 12
                 if myChats.ownerId == userId {
                 cell.lblLatestMessage?.text = messageText // StringUTFEncoding.UTFEncong(string:messageText)
                 }else{
                 cell.lblLatestMessage?.text = messageText
                 }
                 cell.topOfLatestMesgLabel.constant = 12
                 }
                 }else if (myChats.mediaType == StringConstants.MediaType.link ) {
                 if let messageText: String = myChats.messageText {
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = true
                 if myChats.ownerId == userId {
                 cell.lblLatestMessage?.text = messageText // StringUTFEncoding.UTFEncong(string:messageText)
                 }else{
                 cell.lblLatestMessage?.text = messageText
                 }
                 cell.leadingOflblLatestMsg.constant = 9
                 cell.topOfLatestMesgLabel.constant = 12
                 }
                 }else if myChats.messageType == StringConstants.MessageType.feed {
                 if let messageText: String = myChats.messageText {
                 cell.lblLatestMessage?.text = messageText
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 cell.imageOfShowMessageTypeLatestMsg.image = UIImage(named: StringConstants.ImageNames.rssFeedIcon)
                 cell.leadingOflblLatestMsg.constant = 35
                 cell.leftConstantOfLatestMsgTypeImgView.constant = 9
                 cell.widthOfLatestMsgTypeImgView.constant = 19
                 cell.heightOfLatestMsgTypeImgView.constant = 19
                 cell.topOfLatestMesgLabel.constant = 12
                 }else{
                 cell.lblLatestMessage?.text = StringConstants.EMPTY
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = false
                 }
                 }else{
                 cell.lblLatestMessage.isHidden = true
                 cell.imageOfShowMessageTypeLatestMsg.isHidden = true
                 }

                //This feature is removed for now
                 /*if let unreadMessagesCount = myChats.unreadMessagesCount{
                 if Int(unreadMessagesCount)! >= Int(StringConstants.ONE)!{
                 if myChats.ownerId == userId{
                 cell.imgOfMsgUnReadIndicator.isHidden = true
                 }else{
                 cell.imgOfMsgUnReadIndicator.isHidden = false
                 }
                 }else{
                 cell.imgOfMsgUnReadIndicator.isHidden = true
                 }
                 }*/
                 return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostMesaageViewCell", for: indexPath) as! PostMesaageViewCell
                cell.heightConstantOfBtnPromoPost.constant = 0
                cell.heightOfViewOfBackRequestCallBack.constant = 0
                cell.videIcon.isHidden = true
                cell.btnPlayVideo.isHidden = true
                cell.lblPOstTitl.text = myChats.messageText
                cell.txtviewOfPostDescription.text = myChats.lastMessageContentField2
                let time = DateTimeHelper.getTimeAgo(time: String(describing: myChats.messageAt!))
                cell.lblOfDateTime.text  = time
                cell.lblsponsored.text = NSLocalizedString(StringConstants.Sponsered, comment: StringConstants.EMPTY)
                //set channel profile Image
                let imgurl = myChats.communityImageBigThumbUrl
                let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                let url = NSURL(string: rep2)
                cell.imgChannelProfile.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                if let communityName = myChats.communityName, !communityName.isEmpty {
                    let attrs = [
                        NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),
                        NSAttributedStringKey.foregroundColor : UIColor.black] as [NSAttributedStringKey : Any] as [NSAttributedStringKey : Any]
                    let attributedString = NSMutableAttributedString(string:communityName, attributes:attrs)
                    cell.lblOfChannelName.attributedText = attributedString
                    cell.heightConstraintOfLblSponsored.constant = 18
                }
                
                cell.imgChannelProfile.layer.cornerRadius = cell.imgChannelProfile.frame.size.width / 2
                cell.imgChannelProfile.clipsToBounds = true
                cell.selectionStyle = .none
                cell.imgpostimage.backgroundColor = UIColor.lightGray
                //postimageOrVideo
                if myChats.mediaType == StringConstants.MediaType.video{
                    cell.videIcon.isHidden = false
                    cell.btnPlayVideo.isHidden = true
                    if let imgurl = myChats.lastMessageContentField8 {
                        let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                        let url = NSURL(string: rep2)
                        //cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let imageView = UIImageView()
                        imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let size = CGSize(width: 248, height: 248)
                        let imageHelper = ImageHelper()
                        cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
                    }else{
                        cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg)
                    }
                }else{
                    cell.videIcon.isHidden = true
                    cell.btnPlayVideo.isHidden = true
                    if let imgurl = myChats.lastMessageContentField3 {
                        let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                        let url = NSURL(string: rep2)
                        //cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let imageView = UIImageView()
                        imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        let size = CGSize(width: 248, height: 248)
                        let imageHelper = ImageHelper()
                        cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
                    }else{
                        cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg)
                    }
                }
                
                if  UIScreen.main.bounds.size.height <= 568{
                    cell.HeightConstraintForPostimage.constant = 200
                }

                // cell.HeightConstraintForPostimage.constant = 200
                cell.btnPromotePost.isHidden = true
                cell.lblOfSeprator.isHidden = true
                cell.viewOfBackRequestCallBack.isHidden = true
                return cell
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
            cell.selectionStyle = .none
            cell.lblMenuTitle.text = arrOfMenus[indexPath.row] as? String
            cell.widthOfMenuImageView.constant = 0
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblMyChatList{
            if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC || commingFrom == StringConstants.flags.oneToOneFromAdminVC{
                let myChats = myChatsList[indexPath.row]
                if myChats.isMember == StringConstants.ZERO {
                    DispatchQueue.main.async {
                        self.popupAlert(title: "", message:(NSLocalizedString(StringConstants.Validation.noLongerFollower, comment: StringConstants.EMPTY)) , actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                            },{action2 in
                            }, nil])
                    }
                }else{
                    DispatchQueue.main.async {
                        self.popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.forwardMesgConfirmation, comment: StringConstants.EMPTY)) , actionTitles: [ (NSLocalizedString(StringConstants.AlertMessage.btnCancle, comment: StringConstants.EMPTY)), (NSLocalizedString(StringConstants.AlertMessage.btnConfirm, comment: StringConstants.EMPTY))], actions:[{action1 in
                            },{action2 in
                                // let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                                // spinnerActivity.isUserInteractionEnabled = false
                                self.commingFrom = StringConstants.EMPTY
                                self.forwardMessage(indexPathValue:indexPath)
                            }, nil])
                    }
                }
            }else{
                viewOfBackMoreOptionTbl.isHidden = true
                let myChats = myChatsList[indexPath.row]
                if myChats.ownerId == userId {    // For Admin
                    let channelDashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                    channelDashboardVC.myChatsData = myChats
                    self.navigationController?.pushViewController(channelDashboardVC, animated: true)
                } else {
                    if myChats.isSponsoredMessage == StringConstants.ONE{
                          if myChats.isMember == StringConstants.ONE {

                            if((myChats.request_callback_id?.isEmpty)!){
                                let SponseredMessageDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "SponseredMessageDetailController") as! SponseredMessageDetailController
                                SponseredMessageDetailVc.myChatsData = myChats
                                self.navigationController?.pushViewController(SponseredMessageDetailVc, animated: true)
                            }else{

                                let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                                channelSendBroadcastMsgVC.communityName = myChats.communityName!
                                channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                                channelSendBroadcastMsgVC.myChatsData = myChats
                                // Change unreadMessagesCount to zero
                                MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                            }
                            /*
                             let messageId = myChats.id!
                             let strArray = messageId.components(separatedBy: ":")

                             if(strArray.count == 1){
                                let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                                channelSendBroadcastMsgVC.communityName = myChats.communityName!
                                channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                                channelSendBroadcastMsgVC.myChatsData = myChats
                                // Change unreadMessagesCount to zero
                                MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                            }else if (strArray.count == 2) {
                                let SponseredMessageDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "SponseredMessageDetailController") as! SponseredMessageDetailController
                                SponseredMessageDetailVc.myChatsData = myChats
                                self.navigationController?.pushViewController(SponseredMessageDetailVc, animated: true)
                            }else{
                                let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                                channelSendBroadcastMsgVC.communityName = myChats.communityName!
                                channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                                channelSendBroadcastMsgVC.myChatsData = myChats
                                // Change unreadMessagesCount to zero
                                MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                            }*/

                        }else{
                            let SponseredMessageDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "SponseredMessageDetailController") as! SponseredMessageDetailController
                            SponseredMessageDetailVc.myChatsData = myChats
                            self.navigationController?.pushViewController(SponseredMessageDetailVc, animated: true)
                        }
                    }else{
                        let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                        //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                        channelSendBroadcastMsgVC.communityName = myChats.communityName!
                        channelSendBroadcastMsgVC.communityKey = myChats.communityKey!
                        channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChats.communityImageSmallThumbUrl!
                        channelSendBroadcastMsgVC.myChatsData = myChats
                        // Change unreadMessagesCount to zero
                        MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: myChats.communityKey!)
                        
                        self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                    }
                }
            }
        }else{
            viewOfBackMoreOptionTbl.isHidden = true
            setSearchBar()
          /*  if indexPath.row == 0 {
                
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectMembersVC = storyboard.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                selectMembersVC.strComingFrom = StringConstants.flags.dashboardViewController
                selectMembersVC.communityKey = StringConstants.EMPTY
                self.navigationController?.pushViewController(selectMembersVC, animated: true)
                
            }else if indexPath.row == 1 {
                
                // setSearchBar()
                /* let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                 let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                 vc.strCommingFrom = StringConstants.flags.dashboardViewController
                 if let nav = self.navigationController {
                 for controller in nav.viewControllers as Array {
                 if controller is ViewController {
                 let _ = nav.popToViewController(controller as UIViewController, animated: false)
                 break
                 }
                 }
                 nav.pushViewController(vc, animated: false)
                 return
                 }*/
                
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                vc.strCommingFrom = StringConstants.flags.dashboardViewController
                if let nav = self.navigationController {
                    for controller in nav.viewControllers as Array {
                        if controller is ViewController {
                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                            break
                        }
                    }
                    nav.pushViewController(vc, animated: false)
                    return
                }
            }else{
                setSearchBar()
            }*/
        }
        
    }
    
    
    func setSearchBar(){
        searchBar.placeholder = (NSLocalizedString(StringConstants.placeHolder.search, comment: StringConstants.EMPTY))
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.becomeFirstResponder()
        searchBar.sizeToFit()
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
            if let z = ob as? UITextField {
                let txtview: UITextField = z
                txtview.tintColor =  UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
            }
        }
        
        
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        searchBar.delegate = self
        var btnCloseSearch = UIButton(frame: CGRect(x: self.searchBar.frame.origin.x  + self.searchBar.frame.size.width, y: 5, width: 40, height: 25))
        btnCloseSearch.backgroundColor = .red
        btnCloseSearch  = UIButton(type: .custom)
        btnCloseSearch.setImage(UIImage(named: StringConstants.ImageNames.closeSearchIcon), for: .normal)
        btnCloseSearch.addTarget(self, action: #selector(btnCloseSearchClick), for: .touchUpInside)
        // let rightNavBarButton = UIBarButtonItem(customView:btnCloseSearch)
        let closeBtn = UIBarButtonItem(customView: btnCloseSearch)
        self.navigationItem.setRightBarButtonItems([closeBtn], animated: false)
        //  self.navigationItem.rightBarButtonItem = rightNavBarButton
        
        //Done button for hide  key pad
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)) , style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        searchBar.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonAction() {
        searchBar.resignFirstResponder()
    }
    
    @objc func btnCloseSearchClick(sender: UIButton!) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.removeFromSuperview()
        sender.isHidden = true
        setNavigation()
        self.setNavigationBarItem()
        self.myChatsList = self.myChatsListTemp
//        for i in (0..<self.myChatsListTemp.count){
//            let mychat = self.myChatsListTemp[i]
//            if  mychat.isSponsoredMessage == StringConstants.ONE{
//                if self.userId == mychat.ownerId{
//                    if  mychat.isSponsoredMessage == StringConstants.ONE {
//                        let index =    self.myChatsList.index { (myChatValue) -> Bool in
//                            myChatValue.isSponsoredMessage == mychat.isSponsoredMessage
//                        }
//                        self.myChatsList.remove(at: index!)
//                    }
//                    //self.myChatsList.remove(at: i)
//                }
//            }
//        }
        tblMyChatList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblMyChatList{
            let myChats = myChatsList[indexPath.row]
            if myChats.isSponsoredMessage == StringConstants.ZERO || myChats.isSponsoredMessage == nil {
                return 83
            }else{
                if UIScreen.main.bounds.size.width <= 320{
                    return /*320*/ 360
                }else{
                    return 400
                }
            }
        }
        return 50
    }
}

extension  DashboardViewController :UISearchBarDelegate {
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        let searchText = String(searchBar.text!)
        self.filteredObjects = myChatsListTemp.filter { ($0.communityName)?.range(of: searchText, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        if self.filteredObjects?.count == 0{
            myChatsList.removeAll()
            myChatsList = myChatsListTemp
        }else{
            myChatsList.removeAll()
            myChatsList = filteredObjects!
        }
        
        tblMyChatList.reloadData()
    }
}

extension DashboardViewController: XMPPStreamDelegate {
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        print("Aunthenticated at viewcontroller")
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Wrong password or username")
    }
    
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage){
        if message.childCount > 1{
            self.myChatsList.sort(by:{$0.messageAt! > $1.messageAt!} )
            //self.myChatsList.sort(by:{$0.last_activity_datetime! > $1.last_activity_datetime!} )
            tblMyChatList.reloadData()
        }
    }
}

extension DashboardViewController : XMPPPubSubDelegate{
    func xmppPubSub(_ sender: XMPPPubSub, didPublishToNode node: String, withResult iq: XMPPIQ) {
        createSendMessageRequestInDB.updateValue(StringConstants.Status.sent, forKey: "messageStatus")
        print("didPublishToNode",createSendMessageRequestInDB)
        //Update message in core data as local DB with sent status
        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: createSendMessageRequestInDB)
    }
    
    func xmppPubSub(_ sender: XMPPPubSub, didNotPublishToNode node: String, withError iq: XMPPIQ) {
        print("node",node)
    }
}





