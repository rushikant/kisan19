//
//  DiscoverChannelViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 10/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import SDWebImage
import XMPPFramework
import MBProgressHUD

class DiscoverChannelViewController: UIViewController{
    
    @IBOutlet weak var collectioViewOfChannel: UICollectionView!
    var arrOfCheckfollower = NSMutableArray()
    lazy   var searchBar:UISearchBar = UISearchBar()//frame: CGRect(x: 0, y: 0, width: 500 , height: 20)
    var container = UIView()
    var communityList = [CommunityDetailTable]()
    var followingData = CommunityDetailTable()
    var communityListTemp = [CommunityDetailTable]()
    var filteredObjects = [CommunityDetailTable]()
    var userId = String()
    //var communityDetailsList: [CommunityDetailsList] = []
    var appDelegate = AppDelegate()
    var filter: [CommunityDetailTable] = []
    @IBOutlet weak var viewOfBackPopUpView: UIView!
    var btnSearchClick : Bool = false
    @IBOutlet weak var lblSearchingResultFor: UILabel!
    var isComingFromGreepass=false
    //For Pagination
    var isDataLoading:Bool=false
    var page_number : Int = 1
    var page_size : Int = 10
    var fetchOffSet :Int = 0
    var page_sizeForSearch : Int = 1000
    var isSarchingFilter : Bool =  false
    var isGetDiscoverList : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //------ setOverLay ---------//
        self.setOverLay()
        //---------------------------//
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        
        initialDesign()
        self.communityList.removeAll()
        self.communityListTemp.removeAll()
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.callFetchDiscoverChannelList(page_number:String(page_number),page_size:String(page_sizeForSearch),searchText:StringConstants.EMPTY,filterCategory:StringConstants.EMPTY,isSearching:StringConstants.FALSE)
        }else{
            // Set flag for open category drawer from right drawer
           // let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.discoverChannelViewController]
            self.communityList = CommunityDetailService.sharedInstance.getCommunityDetailsRecord(fetchOffSet)
           // self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
            self.communityListTemp = self.communityList
            //Set follow status
            self.setFollowStatus()
            
           // let nitificationDataDict:[String: [CommunityDetailTable]] = [StringConstants.NSUserDefauluterKeys.communityListData: self.communityList]
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
            
        }

        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFilter(_:)), name: NSNotification.Name(rawValue: StringConstants.flags.ApplyFilter), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyPavilionFilter(_:)), name: NSNotification.Name(rawValue: StringConstants.flags.ApplyPavilionFilter), object: nil)
    }
    
    func setOverLay(){
        let showOverlayOnDiscover = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
        if showOverlayOnDiscover == StringConstants.NSUserDefauluterValues.TRUE{
            MBProgressHUD.hide(for: self.view, animated: true);
            let overlayViewOnDiscoverVC = self.storyboard?.instantiateViewController(withIdentifier: "OverlayViewOnDiscoverScreen") as! OverlayViewOnDiscoverScreen
            self.navigationController?.pushViewController(overlayViewOnDiscoverVC, animated: false)
        }
    }
    
    // handle notification
    @objc func applyFilter(_ notification: NSNotification) {
        isSarchingFilter = true
        
        let arrofcategoryData = notification.userInfo?["categories"] as! Dictionary<String,[String]>
        let categoriesId = arrofcategoryData["categoriesId"] as! [String]
        let categoriesNames = arrofcategoryData["categoriesName"]  as! [String]

        communityList.removeAll()
        communityListTemp.removeAll()
        var arrOfCategoriesFromList  = [String]()
        communityListTemp = CommunityDetailService.sharedInstance.getCommunityDetailsForRightVC()
        for  i in (0..<communityListTemp.count){
            let temp = communityListTemp[i]
            let categoryValue = temp.communityCategories
            arrOfCategoriesFromList = (categoryValue?.components(separatedBy: ","))!
            for  i in (0..<categoriesId.count){
                let searchText = categoriesId[i]
                if arrOfCategoriesFromList.contains(searchText){
                    if !communityList.contains(temp){
                        communityList.append(temp)
                    }
                }
            }
        }

        self.communityList.sort(by:{$0.community_suggestion_score_for_user! as! Int > $1.community_suggestion_score_for_user! as! Int} )

        
        lblSearchingResultFor.text = StringConstants.EMPTY
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        
        if communityList.count == 0 {
            lblSearchingResultFor.isHidden = true
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.resultNotFound, comment: StringConstants.EMPTY)), duration: 3.0, position: .center)
        }else{
            lblSearchingResultFor.isHidden = false
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterSuccess, comment: StringConstants.EMPTY)))
        }
        
        self.communityList = (communityList as NSArray).sortedArray(using: [NSSortDescriptor(key: "communityScore", ascending: false)]) as [AnyObject] as! [CommunityDetailTable]
        print(self.communityList)

        //call this method for set follw and folling channel
        arrOfCheckfollower.removeAllObjects()
        self.setFollowStatus()
        
        collectioViewOfChannel.reloadData()
        
       var categoryName = String()
        for  i in (0..<categoriesNames.count){
            let searchText = categoriesNames[i]
            if categoryName.isEmpty{
                categoryName =  categoryName + searchText
            }else{
                categoryName =  categoryName + StringConstants.comma + searchText
            }
        }
        
        let strFirstAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
        let strSecondAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
        
        let strFirstString = NSMutableAttributedString(string: (NSLocalizedString(StringConstants.DiscoverChannel.searchResult, comment: StringConstants.EMPTY)), attributes: strFirstAttributes)
        let strSecondString = NSMutableAttributedString(string: categoryName, attributes: strSecondAttributes)
        
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        if preferredLanguage == "en" {
            strFirstString.append(strSecondString)
            lblSearchingResultFor.attributedText = strFirstString
        }else if preferredLanguage == "mr" {
            strSecondString.append(strFirstString)
            lblSearchingResultFor.attributedText = strSecondString
        } else{
            strSecondString.append(strFirstString)
            lblSearchingResultFor.attributedText = strSecondString
        }
        
        
    }
    
    @objc func applyPavilionFilter(_ notification: NSNotification) {
        isSarchingFilter = true
        let categories = notification.userInfo?["Pavilions"] as! [String]
        communityList.removeAll()
        communityListTemp.removeAll()
        var arrOfPavilionFromList  = [String]()
        communityListTemp = CommunityDetailService.sharedInstance.getCommunityDetailsForRightVC()
        //self.communityListTemp.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
        for  i in (0..<communityListTemp.count){
            let temp = communityListTemp[i]
            let categoryValue = temp.pavilion_name
            //print(categoryValue as Any)
            arrOfPavilionFromList = (categoryValue?.components(separatedBy: ","))!
            for  i in (0..<categories.count){
                let searchText = categories[i]
                let str = arrOfPavilionFromList[0]
                if str.contains(searchText){
                    if !communityList.contains(temp){
                        communityList.append(temp)
                        //print(temp.communityName)
                        //print(temp.community_suggestion_score_for_user)
                    }
                }
            }
        }
        
        self.communityList.sort(by:{$0.community_suggestion_score_for_user! as! Int > $1.community_suggestion_score_for_user! as! Int} )

        lblSearchingResultFor.text = StringConstants.EMPTY
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        
        if communityList.count == 0 {
            lblSearchingResultFor.isHidden = true
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.resultNotFound, comment: StringConstants.EMPTY)), duration: 3.0, position: .center)
        }else{
            lblSearchingResultFor.isHidden = false
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterSuccess, comment: StringConstants.EMPTY)))
        }
        //self.communityList.sort(by:{$0.communityScore! > $1.communityScore!})
        //call this method for set follw and folling channel
        arrOfCheckfollower.removeAllObjects()
        self.setFollowStatus()
        collectioViewOfChannel.reloadData()
        
        var pavalionName = String()
        for  i in (0..<categories.count){
            let searchText = categories[i]
            if pavalionName.isEmpty{
                pavalionName =  pavalionName + searchText
            }else{
                pavalionName =  pavalionName + StringConstants.comma + searchText
            }
        }
        
        let strFirstAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
        let strSecondAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
        
        let strFirstString = NSMutableAttributedString(string: (NSLocalizedString(StringConstants.DiscoverChannel.searchResult, comment: StringConstants.EMPTY)), attributes: strFirstAttributes)
        let strSecondString = NSMutableAttributedString(string: pavalionName, attributes: strSecondAttributes)
        
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        if preferredLanguage == "en" {
            strFirstString.append(strSecondString)
            lblSearchingResultFor.attributedText = strFirstString
        }else if preferredLanguage == "mr" {
            strSecondString.append(strFirstString)
            lblSearchingResultFor.attributedText = strSecondString
        } else{
            strSecondString.append(strFirstString)
            lblSearchingResultFor.attributedText = strSecondString
        }
        
    }
    
    func callFetchDiscoverChannelList(page_number:String,page_size:String,searchText:String,filterCategory:String,isSearching:String) {
        
        let params = ["isSearchig":isSearching]
        let currentTime = DateTimeHelper.getCurrentMillis()
        //let apiURL = URLConstant.BASE_URL + URLConstant.discoverChannelList1 + page_number + URLConstant.discoverChannelList2 + page_size + URLConstant.discoverChannelList3 + String(currentTime)
        let apiURL = URLConstant.BASE_URL + URLConstant.discoverChannelList1 + page_number + URLConstant.discoverChannelList2 + page_size + URLConstant.discoverChannelList3 + searchText + URLConstant.discoverChannelList4 + filterCategory + URLConstant.discoverChannelList5 + String(currentTime)

        print("apiURL",apiURL)
        _ = DiscoverListServices().getDiscoverList(apiURL,
                                                   postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                   // let model = userModel as! DiscoverChannelResponse
                                                   // self.communityDetailsList = model.communityDetailsList
                                                    if isSearching == StringConstants.TRUE{
                                                              self.communityList = userModel as! [CommunityDetailTable]
                                                        print("self.communityList 512",self.communityList.count)
                                                        //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
                                                        self.communityListTemp = self.communityList
                                                        
                                                        //Set follow status
                                                        self.setFollowStatus()
                                                        // Set flag for open category drawer from right drawer
                                                        let nitificationDataDict:[String: [CommunityDetailTable]] = [StringConstants.NSUserDefauluterKeys.communityListData: self.communityList]
                                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
                                                        
                                                        if self.communityListTemp.count == 0{
                                                    //self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.resultNotFound, comment: StringConstants.EMPTY)))
                                                    self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.resultNotFound, comment: StringConstants.EMPTY)), duration: 3.0, position: .center)
                                                        }

                                                    }else{
                                                        if self.communityListTemp.count == 0 {
                                                            self.communityList = CommunityDetailService.sharedInstance.getCommunityDetails()
                                                            //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
                                                            //self.communityList.sort(by:{$0.communityScore! < $1.communityScore!} )
                                                            self.communityListTemp = self.communityList
                                                            
                                                        }else{
                                                            //Local Pagination
                                                            self.getChannelListOnPagination()
                                                        }
                                                        // Set flag for open category drawer from right drawer
                                                        let nitificationDataDict:[String: [CommunityDetailTable]] = [StringConstants.NSUserDefauluterKeys.communityListData: self.communityList]
                                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
                                                        
                                                        //Used for pagination
                                                        //Set follow status
                                                        self.setFollowStatus()
                                                        
                                                        //Hide & show navigation icons
                                                        if self.isGetDiscoverList == false{
                                                            self.isGetDiscoverList = true
                                                            self.setNavigation()
                                                        }
                                                    }
                                           
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            /*self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
             },{action2 in
             }, nil])*/
            
        })
    }
    //Local Pagination
    func getChannelListOnPagination(){
       /*// This logic is based on last object communityCreatedTimestamp. Getting next 10 channel after this communityCreatedTimestamp.
        let communityListLastVal = self.communityListTemp.last
        let tempList = CommunityDetailService.sharedInstance.fetchChannelAfterSentTimeStamp(communityCreatedTimestamp:(communityListLastVal?.communityCreatedTimestamp!)!)*/
        
        fetchOffSet = fetchOffSet + 10;
        let tempList = CommunityDetailService.sharedInstance.getCommunityDetailsRecord(fetchOffSet)
        for i in 0 ..< tempList.count {
            let list = tempList[i]
            if !self.communityList.contains(list){
                self.communityList.append(list)
            }
        }
       // self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
        self.communityListTemp = self.communityList
    }
    
    func setFollowStatus(){
        for  i in (0..<self.communityList.count){
            let communityDetails = communityList[i]
            if communityDetails.isMember! == StringConstants.ONE{
                self.arrOfCheckfollower.add("1")
            }else{
                self.arrOfCheckfollower.add("0")
            }
        }
        self.view.isUserInteractionEnabled = true
        //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
        MBProgressHUD.hide(for: self.view, animated: true);
        self.collectioViewOfChannel.reloadData()
    }
    
    func initialDesign() {
        setNavigation()
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        collectioViewOfChannel.collectionViewLayout = layout
        collectioViewOfChannel.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchBar.inputAccessoryView = toolbar
        //Set for pagination
        collectioViewOfChannel.delegate=self //To enable scrollviewdelegate
    }
    
    @objc func doneButtonAction() {
        searchBar.resignFirstResponder()
    }
    
    @objc func btnFilterChannel(sender: UIBarButtonItem) {
        // For open right drawer
        let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.FromDiscoverCategoryFilter]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
        self.setRightDrawer()
    }
    
    @objc func BtnPavilionFilterChannel(sender: UIBarButtonItem) {
        // For open right drawer
        let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.FromDiscoverPavilionFilter]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
        self.setRightDrawerForPavilion()
    }
    
    @objc func btnRefreshChannel(sender: UIBarButtonItem) {
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        lblSearchingResultFor.textColor = UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1.0)
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.view.isUserInteractionEnabled = false
            self.communityList.removeAll()
            self.communityListTemp.removeAll()
            fetchOffSet = 0
            page_number  = 1
            page_size = 10
            isSarchingFilter = false
            self.callFetchDiscoverChannelList(page_number:String(page_number),page_size:String(page_sizeForSearch),searchText:StringConstants.EMPTY,filterCategory:StringConstants.EMPTY,isSearching:StringConstants.FALSE)
        }else{
            // Set flag for open category drawer from right drawer
            // let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.discoverChannelViewController]
            self.communityList = CommunityDetailService.sharedInstance.getCommunityDetailsRecord(fetchOffSet)
            //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
            self.communityListTemp = self.communityList
            //Set follow status
            self.setFollowStatus()
            
            let nitificationDataDict:[String: [CommunityDetailTable]] = [StringConstants.NSUserDefauluterKeys.communityListData: self.communityList]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
        }
    }
    
    @objc func btnSearchChannel(sender: UIBarButtonItem) {
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        lblSearchingResultFor.textColor = UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1.0)
        self.SearchChannelClick()
    }
    
    func SearchChannelClick(){
        btnSearchClick = true
        searchBar.placeholder = (NSLocalizedString(StringConstants.placeHolder.search, comment: StringConstants.EMPTY))
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.becomeFirstResponder()
        searchBar.sizeToFit()
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
            if let z = ob as? UITextField {
                let txtview: UITextField = z
                txtview.tintColor =  UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
            }
        }
        
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        searchBar.delegate = self
        var btnCloseSearch = UIButton(frame: CGRect(x: self.searchBar.frame.origin.x  + self.searchBar.frame.size.width-10, y: 2, width: 50, height: 35))
    
        self.navigationItem.rightBarButtonItems = []
        btnCloseSearch  = UIButton(type: .custom)
        btnCloseSearch.setImage(UIImage(named: StringConstants.ImageNames.closeSearchIcon), for: .normal)
        btnCloseSearch.addTarget(self, action: #selector(btnCloseSearchClick), for: .touchUpInside)
        let rightNavBarButton = UIBarButtonItem(customView:btnCloseSearch)
        self.navigationItem.rightBarButtonItem = rightNavBarButton
    }
    
    func setNavigation(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        if self.isGetDiscoverList == true{
            /*  let filterButton   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.filterChannel), style: .plain, target: self, action: #selector(btnFilterChannel))
             let PavilionFilterButton  = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.Pavilion), style: .plain, target: self, action: #selector(BtnPavilionFilterChannel))
             let searchButton = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.searchIcon), style: .plain, target: self, action: #selector(btnSearchChannel))
             let refreshButton = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.refreshIcon), style: .plain, target: self, action: #selector(btnRefreshChannel))
             
             filterButton.tintColor = UIColor.white
             searchButton.tintColor = UIColor.white
             refreshButton.tintColor = UIColor.white
             navigationItem.rightBarButtonItems = [filterButton,PavilionFilterButton,searchButton,refreshButton]*/
            
            
            let filterBtn: UIButton = UIButton(type: UIButtonType.custom)
            filterBtn.setImage(UIImage(named: StringConstants.ImageNames.filterChannel), for: [])
            filterBtn.addTarget(self, action: #selector(btnFilterChannel), for: UIControlEvents.touchUpInside)
            filterBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let filterButton = UIBarButtonItem(customView: filterBtn)
            
            let PavilionFilterBtn: UIButton = UIButton(type: UIButtonType.custom)
            PavilionFilterBtn.setImage(UIImage(named: StringConstants.ImageNames.Pavilion), for: [])
            PavilionFilterBtn.addTarget(self, action: #selector(BtnPavilionFilterChannel), for: UIControlEvents.touchUpInside)
            PavilionFilterBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let PavilionFilterButton = UIBarButtonItem(customView: PavilionFilterBtn)
            
            let searchBtn: UIButton = UIButton(type: UIButtonType.custom)
            searchBtn.setImage(UIImage(named: StringConstants.ImageNames.searchIcon), for: [])
            searchBtn.addTarget(self, action: #selector(btnSearchChannel), for: UIControlEvents.touchUpInside)
            searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let searchButton = UIBarButtonItem(customView: searchBtn)
            
            let refreshBtn: UIButton = UIButton(type: UIButtonType.custom)
            refreshBtn.setImage(UIImage(named: StringConstants.ImageNames.refreshIcon), for: [])
            refreshBtn.addTarget(self, action: #selector(btnRefreshChannel), for: UIControlEvents.touchUpInside)
            refreshBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let refreshButton = UIBarButtonItem(customView: refreshBtn)
            self.navigationItem.rightBarButtonItems = [filterButton, /*PavilionFilterButton,*/ searchButton,refreshButton]
        }
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.text =  (NSLocalizedString(StringConstants.NavigationTitle.discover, comment: StringConstants.EMPTY))
        navigationItem.titleView = lblOfTitle

        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
    }
    
    @objc func btnCloseSearchClick(sender: UIButton!) {
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        lblSearchingResultFor.textColor = UIColor(red: 101/255, green: 101/255, blue: 101/255, alpha: 1.0)
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.removeFromSuperview()
        sender.isHidden = true
        btnSearchClick = false
        isSarchingFilter = false
        /*let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            SVProgressHUD.show()
            self.communityList.removeAll()
            self.communityListTemp.removeAll()
            fetchOffSet = 0
            page_number  = 1
            page_size = 10
            self.callFetchDiscoverChannelList(page_number:String(page_number),page_size:String(page_size),searchText:StringConstants.EMPTY,filterCategory:StringConstants.EMPTY,isSearching:StringConstants.FALSE)
            
        }else{*/
         fetchOffSet = 0
        self.communityList = CommunityDetailService.sharedInstance.getCommunityDetailsRecord(fetchOffSet)
        //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
        self.communityListTemp = self.communityList
        //Set follow status
        self.setFollowStatus()
        
        //}
        setNavigation()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        lblSearchingResultFor.text = StringConstants.EMPTY
        lblSearchingResultFor.text = (NSLocalizedString(StringConstants.DiscoverChannel.recommended, comment: StringConstants.EMPTY))
        // For open right drawer
        //self.setRightDrawer()
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        if btnSearchClick == true {
            self.SearchChannelClick()
        }
        collectioViewOfChannel.reloadData()
    }
    
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        if isComingFromGreepass{
            self.navigationController?.popViewController(animated: true)
        }else{
            let nitificationDataDict:[String: String] = [StringConstants.NSUserDefauluterKeys.commingFromScreen: StringConstants.flags.dashboardViewController]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nitificationDataDict)
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is DashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                nav.pushViewController(vc, animated: false)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
  
    }
    
}


extension  DiscoverChannelViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: UICollectionViewDelegate & UICollectionViewDataSource Functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return communityList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : DiscoverChannelCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverChannelCollectionViewCell", for: indexPath) as! DiscoverChannelCollectionViewCell
        let communityDetails = communityList[indexPath.row]
        if let communityName = communityDetails.communityName{
            cell.lblChannelName.text = communityName //StringUTFEncoding.UTFEncong(string:communityName)
        }
        cell.imageViewOfChannel.layer.cornerRadius = cell.imageViewOfChannel.frame.size.width/2
        cell.imageViewOfChannel.clipsToBounds = true
        cell.viewOfBackChannelImg.layer.cornerRadius = cell.viewOfBackChannelImg.frame.size.width/2
        cell.viewOfBackChannelImg.clipsToBounds = true
        
        if let communityDominantColour = communityDetails.communityDominantColour{
            var correctedCommunityDominantColour = String()
            print("communityDominantColour:::",communityDominantColour.length)
            if communityDominantColour.length < 7{
                if communityDominantColour.length == 6{
                    correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode
                }else{
                    correctedCommunityDominantColour = communityDominantColour + StringConstants.DiscoverChannel.Colorcode + StringConstants.DiscoverChannel.Colorcode
                }
                cell.viewOfBaner.backgroundColor = hexStringToUIColor(hex: correctedCommunityDominantColour)
                
            }else{
                cell.viewOfBaner.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }
        }else{
            cell.viewOfBaner.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }

        if let communityImageUrl = communityDetails.communityImageBigThumbUrl{
            if communityImageUrl == StringConstants.EMPTY{
                if communityDetails.communityWithRssFeed == StringConstants.ONE
                {
                    cell.imageViewOfChannel.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                }else{
                    cell.imageViewOfChannel.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                }
            }else{
                let imgurl = communityDetails.communityImageBigThumbUrl
                let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                 cell.imageViewOfChannel.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
                }
        }else{
            if communityDetails.communityWithRssFeed == StringConstants.ONE
            {
                cell.imageViewOfChannel.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                
            }else{
                cell.imageViewOfChannel.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
            }
        }
        
      
        cell.viewOnCell.layer.cornerRadius = 2
        cell.viewOnCell.clipsToBounds = true
        if communityDetails.totalMembers == nil{
            cell.lblFollwersCount.text = (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
        }else{
            
            var totalMembers = Int(communityDetails.totalMembers!)
            if totalMembers == 0 {
                cell.lblFollwersCount.text = (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
            }else{
                totalMembers = totalMembers! - 1
                if totalMembers == 0 {
                    cell.lblFollwersCount.text = (NSLocalizedString(StringConstants.DiscoverChannel.NoFollower, comment: StringConstants.EMPTY))
                }else{
                    cell.lblFollwersCount.text =  String(describing: totalMembers!) + (NSLocalizedString(StringConstants.DiscoverChannel.follower, comment: StringConstants.EMPTY))
                }
            }
        }

        if communityDetails.ownerId == userId {
             cell.lblOfOwnership.isHidden = false
             cell.btnFollow.isHidden = true
        }else{
            cell.lblOfOwnership.isHidden = true
            cell.btnFollow.isHidden = false
        }
        cell.btnFollow.addTarget(self,action:#selector(buttonFollowClicked(sender:)), for: .touchUpInside)
        cell.btnFollow.tag = indexPath.row
        if  communityDetails.isMember! == StringConstants.ONE{
            cell.btnFollow.setTitle((NSLocalizedString(StringConstants.DiscoverChannel.following, comment: StringConstants.EMPTY)), for: .normal)
            cell.btnFollow.isUserInteractionEnabled = false
            cell.btnFollow.setTitleColor(UIColor.lightGray, for: UIControlState())
        }else{
            cell.btnFollow.setTitle((NSLocalizedString(StringConstants.DiscoverChannel.follow, comment: StringConstants.EMPTY)), for: .normal)
            cell.btnFollow.isUserInteractionEnabled = true
            cell.btnFollow.setTitleColor(UIColorFromRGB.init(rgb: ColorConstants.colorPrimary), for: UIControlState())
        }
        return cell
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hexStr)
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    @objc func buttonFollowClicked(sender:UIButton) {
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{

        let buttonRow = sender.tag
        let checkFollower = arrOfCheckfollower[buttonRow] as! String
        arrOfCheckfollower.removeObject(at: buttonRow)
        arrOfCheckfollower.insert("1", at: buttonRow)
        if checkFollower == "0"{
            
            let followingDetails = self.communityList[buttonRow]
            let communityKey = followingDetails.communityKey!
            self.communityList.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
            self.communityList.filter({$0.communityKey == communityKey}).first?.messageText = StringConstants.DashboardViewController.follwStatus
            
            // Update MyChat List
            self.followingData = self.communityList[buttonRow]
            var  dict  = ConvertToDicCommunityDetailsObject.convertToDictionary(communityDetail: [self.followingData])
            
            // Set follower count after following channel
            var totalMembers = Int()
            if let followerCount = self.followingData.totalMembers {
                var totalMembers = Int(followerCount)
                if totalMembers! != 0{
                    totalMembers = totalMembers! + 1
                    dict.updateValue(totalMembers ?? Int(), forKey: "totalMembers")
                    dict.updateValue(totalMembers ?? Int(), forKey: "memberCount")
                }else{
                    totalMembers = 1
                    dict.updateValue(totalMembers ?? Int(), forKey: "totalMembers")
                    dict.updateValue(totalMembers ?? Int(), forKey: "memberCount")
                }
            }else{
                totalMembers = 1
                dict.updateValue(totalMembers , forKey: "totalMembers")
                dict.updateValue(totalMembers , forKey: "memberCount")
            }
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
            dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.last_activity_datetime)
            CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)
            _ = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
            dict.updateValue(self.followingData.communityWithRssFeed ?? String(), forKey: "feed")
            
            dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
            dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
            
            let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
            if checkCommunityKeyWithisNormalMychatFlag.count == 0{
                MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
                    dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
                    MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
            }else{
                MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
            }
            
            
          /*  if(myChatCommunityKey.count == 0){
                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
            }else /*if (myChatCommunityKey.count == 1)*/{
                MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
            }*/
            let indexPath = IndexPath(item: buttonRow, section: 0)
            sender.setTitleColor(UIColor.lightGray, for: UIControlState())
            sender.setTitle((NSLocalizedString(StringConstants.DiscoverChannel.following, comment: StringConstants.EMPTY)), for: .normal)
            sender.isUserInteractionEnabled = false

            self.collectioViewOfChannel.reloadItems(at: [indexPath])

            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi(CommunityIndex: buttonRow)
            }
            
        }else{
            arrOfCheckfollower.removeObject(at: buttonRow)
            arrOfCheckfollower.insert("0", at: buttonRow)
        }
        
        let indexPath = IndexPath(item: buttonRow, section: 0)
        self.collectioViewOfChannel.reloadItems(at: [indexPath])
        }else{
            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi(CommunityIndex: sender.tag)
            }
        }
    }
    
    
    //follow Api
    func followChannelApi (CommunityIndex:Int){
        
        let followingDetails = self.communityList[CommunityIndex]
        let communityKey = followingDetails.communityKey!
        let communityJabberId = followingDetails.communityJabberId!
        
        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            //---------------- Update isMember  value as true for follow channel & also at database----------//
                                                            self.communityList.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
                                                            self.communityList.filter({$0.communityKey == communityKey}).first?.messageText = StringConstants.DashboardViewController.follwStatus
                                                            //---------------------------------------------------------------------------//
                                                            // Get old messages after follow channel
                                                            DispatchQueue.global(qos: .background).async {
                                                                self.getOldMessage(communityKey:communityKey)
                                                            }
                                                            //---------------------------------------------------------------------------//
                                                           /* DispatchQueue.main.async {
                                                                let indexPath = IndexPath(item: CommunityIndex, section: 0)
                                                                self.collectioViewOfChannel.reloadItems(at: [indexPath])
                                                                //self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.followChannel, comment: StringConstants.EMPTY)))
                                                            }*/
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
                MBProgressHUD.hide(for: self.view, animated: true);
                self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])}
        })
        
    }
    
    
    
    
    func getOldMessage (communityKey:String){
      
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + "10" + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        let params = ["communityKey":communityKey]
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func collectionView(_ didSelectItemAtcollectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let communityData = self.communityList[indexPath.row]
        var  dict  = ConvertToDicCommunityDetailsObject.convertToDictionary(communityDetail: [communityData])
        self.followingData = self.communityList[indexPath.row]
        dict.updateValue(self.followingData.communityWithRssFeed ?? String(), forKey: "feed")
        print("dict",dict)
        let myChatData = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict)
        let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
        channelProfileVC.myChatsData = myChatData
        self.navigationController?.pushViewController(channelProfileVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //device screen size
        let width = UIScreen.main.bounds.size.width
        //calculation of cell size
        return CGSize(width: ((width / 2) - 15)   , height: 177)
    }
    
}

extension  DiscoverChannelViewController :UISearchBarDelegate {
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        let searchbartext = String(searchBar.text!)
        //let searchText = String(searchBar.text!)

        let searchText = searchbartext.trimmingCharacters(in: .whitespaces)
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false;
            page_number  = 1
            self.callFetchDiscoverChannelList(page_number:String(page_number),page_size:String(page_sizeForSearch),searchText:searchText,filterCategory:StringConstants.EMPTY,isSearching:StringConstants.TRUE)
            
        }else{
            self.filteredObjects = communityListTemp.filter { ($0.communityName)?.range(of: searchText, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            if self.filteredObjects.count == 0{
                if searchBar.text?.count == 0 {
                    communityList.removeAll()
                    communityList = communityListTemp
                }else{
                    communityList.removeAll()
                    self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.resultNotFound, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
                }
            }else{
                communityList.removeAll()
                communityList = filteredObjects
            }
            
            collectioViewOfChannel.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        isSarchingFilter = true
        return true
    }

}

extension  DiscoverChannelViewController :UIScrollViewDelegate {
   
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>){
        
        if isSarchingFilter == false{
            /* let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
             if checkInternetConnection == true {
             let endScrolling: Float = Float(scrollView.contentOffset.y + scrollView.frame.size.height)
             var scrollContentSize = Float(scrollView.contentSize.height)
             scrollContentSize = (scrollContentSize / 100) * 50
             if (scrollView is UICollectionView) {
             if velocity.y > 0 {
             if endScrolling >= scrollContentSize {
             if !isDataLoading{
             isDataLoading = true
             self.page_number = self.page_number + 1
             self.page_size =  10
             self.callFetchDiscoverChannelList(page_number:String(page_number),page_size:String(page_size),searchText:StringConstants.EMPTY,filterCategory:StringConstants.EMPTY,isSearching:StringConstants.FALSE)
             }
             }
             }
             }
             }else{*/
            //Local Pagination
            fetchOffSet = fetchOffSet + 10;
            let tempList = CommunityDetailService.sharedInstance.getCommunityDetailsRecord(fetchOffSet)
            for i in 0 ..< tempList.count {
                let list = tempList[i]
                if !communityList.contains(list){
                    self.communityList.append(list)
                }
            }
            //self.communityList.sort(by:{$0.communityCreatedTimestamp! > $1.communityCreatedTimestamp!} )
            self.communityListTemp = self.communityList
            self.setFollowStatus()
            // }
        }
    }
}
