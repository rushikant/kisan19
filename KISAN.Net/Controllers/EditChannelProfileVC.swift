//
//  EditChannelProfileVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import AWSS3
import SDWebImage
import MBProgressHUD


class EditChannelProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate,PassSelectedCategory,PassMultiMediaResponse,PassSelectedColor,CropViewControllerDelegate {

    @IBOutlet weak var viewOfBackProfilePic: UIView!
    @IBOutlet weak var imageViewOfProfilePic: UIImageView!
    @IBOutlet weak var txtOfCommunityName: UITextField!
    @IBOutlet weak var txtViewOfAboutCommunity: UITextView!
    var dominantColour = String()
    @IBOutlet weak var tblCatogory: UITableView!
    @IBOutlet weak var heightOfTblCatagoty: NSLayoutConstraint!
    @IBOutlet weak var lblCategoryCount: UILabel!
    @IBOutlet var lblAboutChannel: UILabel!
    @IBOutlet var lblcategories: UILabel!
    @IBOutlet var btnAddMore: UIButton!
    var profileImage: UIImage!
    var editCommunityRequest = Dictionary<String, Any>()
    var myChatsData = MyChatsTable()
    var categories_list: [CategoriesList] = []
    var originalMediaBucketName = String()
    var categoryIds = String()
    @IBOutlet weak var viewOfBackProfilePick: UIView!
    var communityDominantColourValue = String()
    var isSetDominontColor = Bool()
    var firstLoad = true
    var base64String: String = ""
    @IBOutlet weak var collectionViewOfProductServices: UICollectionView!
    @IBOutlet weak var heightOfProductCollectionView: NSLayoutConstraint!
    var strChannelName = String()
    var strChannelDescription = String()

    private let picker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        txtViewOfAboutCommunity.text = StringConstants.EMPTY
        
        lblAboutChannel.text = (NSLocalizedString(StringConstants.channelProfile.AboutChannel, comment: StringConstants.EMPTY))
        lblcategories.text = (NSLocalizedString(StringConstants.channelProfile.Categories, comment: StringConstants.EMPTY))
        btnAddMore.setTitle((NSLocalizedString(StringConstants.showUserProfile.AddMore, comment: StringConstants.EMPTY)), for: .normal)
        isSetDominontColor = true
        self.initialDesign()
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        if isSetDominontColor == true{
            communityDominantColourValue = myChatsData.communityDominantColour!
            if let communityDominantColour = myChatsData.communityDominantColour{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
                viewOfBackProfilePick.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                viewOfBackProfilePick.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
            
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColourValue)
            viewOfBackProfilePick.backgroundColor = hexStringToUIColor(hex: communityDominantColourValue)
        }
        
        lblCategoryCount.text = String(categories_list.count)
        tblCatogory .reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.updateViewConstraints()
        heightOfTblCatagoty.constant = CGFloat(self.categories_list.count * 75)
        heightOfProductCollectionView.constant =  114 * (10/3)
        self.updateViewConstraints()
    }
    
    
    func initialDesign() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(btnDoneTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        imageViewOfProfilePic.layer.cornerRadius = imageViewOfProfilePic.frame.size.width/2
        imageViewOfProfilePic.clipsToBounds = true
        
        if let channelProfilePic = myChatsData.communityImageBigThumbUrl {
            imageViewOfProfilePic.sd_setImage(with: URL(string: channelProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
            viewOfBackProfilePic.isHidden = true
        }else{
            viewOfBackProfilePic.isHidden = false
        }
        
        viewOfBackProfilePic.layer.cornerRadius = viewOfBackProfilePic.frame.size.width/2
        viewOfBackProfilePic.clipsToBounds = true
        txtViewOfAboutCommunity.text = myChatsData.communityDesc
        txtOfCommunityName.text = myChatsData.communityName
        tblCatogory.separatorColor = UIColor.clear

    }

    @objc func btnDoneTapped(sender: UIBarButtonItem) {
        print("Done Tapped")
        txtOfCommunityName.text =  txtOfCommunityName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if  (txtOfCommunityName.text?.isEmpty)! {
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.channelNameNotBlank, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }else if categories_list.count == 0 {
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.selectsAtLeastCategory, comment: StringConstants.EMPTY)))
        }else{
            if (imageViewOfProfilePic.image?.isEqual(UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)))!{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.callEditChannelProfile(communityImageUrl:StringConstants.EMPTY)
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                // Compress image
                let imageHelper = ImageHelper()
                let imageData = imageHelper.compressImage(image: imageViewOfProfilePic.image!, compressQuality: 0.4)!
                //Send original image to Amzon S3
                originalMediaBucketName = S3BucketName + channelProfiles + myChatsData.communityJabberId!
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyName)
            }
        }
    }
    
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
        // Bucket name after adding Base URL & Image name
        originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyName
        self.callEditChannelProfile(communityImageUrl:originalMediaBucketName)
    }
    
    
    func PassMultiMediaFail(_ errorMsg: String) {
        MBProgressHUD.hide(for: self.view, animated: true);
        self.popupAlert(title: "", message: errorMsg, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    func callEditChannelProfile(communityImageUrl:String){
        
        for i in (0..<categories_list.count){
            let categories = categories_list[i]
            let categoriesId = categories.id
            
            if categoryIds.isEmpty{
                categoryIds = categoriesId!
            }else{
                categoryIds = categoryIds + StringConstants.comma + categoriesId!
            }
        }
        

          let communityDetilas = EditChannelProfileRequest.convertToDictionary(editChannelProfileDetails: EditChannelProfileRequest.init(communityCategories: String(categoryIds), communityDesc: String(txtViewOfAboutCommunity.text), communityDominantColour: String(communityDominantColourValue), communityName: String(txtOfCommunityName.text!), communityImageSmallThumbUrl: String(communityImageUrl), communityImageUrl: String(communityImageUrl), communityJabberId: String(myChatsData.communityJabberId!), communityKey: String(myChatsData.communityKey!), isCommunityWithRssFeed: false, isDefault: false, ownerId: String(myChatsData.ownerId!), ownerName: String(myChatsData.ownerName!), privacy: String(myChatsData.privacy!), type: String(myChatsData.type!), communityImageBigThumbUrl: String(communityImageUrl)))

        let params = EditChannelProfileDetailRequest.convertToDictionary(editChannelProfileDetailRequest: EditChannelProfileDetailRequest.init(communityDetails: communityDetilas))
        print("params",params)
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.createCommunity + "/" + myChatsData.communityKey! + StringConstants.AND + String(currentTime)
        _ = UpdateCommunityServices().updateCommunity(apiURL,
                                                      postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let model = userModel as! UpdateChannelResponse
                                                        print("model",model)
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        self.txtOfCommunityName.resignFirstResponder()
                                                        self.txtViewOfAboutCommunity.resignFirstResponder()
                                                        
                                                        //Update image in image cache
                                                        SDImageCache.shared().removeImage(forKey: communityImageUrl, withCompletion: nil)
                                                        let url = NSURL(string:communityImageUrl)
                                                        let  data = NSData(contentsOf:url! as URL)
                                                        if data != nil {
                                                            SDImageCache.shared().storeImageData(toDisk: data! as Data, forKey: communityImageUrl)
                                                        }
                                                   self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.channelProfileUpdated, comment: StringConstants.EMPTY)))
                                                        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                        if let nav = self.navigationController {
                                                            for controller in nav.viewControllers as Array {
                                                                if controller is DashboardViewController {
                                                                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                                    break
                                                                }
                                                            }
                                                            nav.pushViewController(vc, animated: false)
                                                            return
                                                        }
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        txtOfCommunityName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCaptureProfilePic(_ sender: Any) {
        
        txtOfCommunityName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.photoLibrary, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewOfProfilePic.contentMode = .scaleAspectFit
            imageViewOfProfilePic.image = pickedImage
            viewOfBackProfilePic.isHidden = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                self.openEditor(nil)
            }
        }
    }
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        // Use view controller
        
        /*  let controller = CropViewController()
         controller.delegate = self
         controller.image = image
         let navController = UINavigationController(rootViewController: controller)
         present(navController, animated: true, completion: nil)*/
        
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
        
        
    }
  
    
    // MARK: - CropView New Library delegate
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        // imageViewOfProfilePic.image = image
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        imageViewOfProfilePic.image = image
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfProfilePic.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnColorPickerClick(_ sender: Any) {
        txtOfCommunityName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ColorPickerViewController") as! ColorPickerViewController
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnAddMoreCategoryClick(_ sender: Any) {
        txtOfCommunityName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.EditChannelProfileVC, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let userAreaInterestVC = storyboard.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
        userAreaInterestVC.delegate = self
        userAreaInterestVC.selectedCategories_list = categories_list
        self.navigationController?.pushViewController(userAreaInterestVC, animated: true)
    }
    
    func PassSelectedCategory(_ category: [CategoriesList]) {
        categories_list.removeAll()
        categories_list = category
        lblCategoryCount.text = String(categories_list.count)
        tblCatogory.reloadData()
        heightOfTblCatagoty.constant = CGFloat(self.categories_list.count * 75)
        self.updateViewConstraints()
    }
    
    func passSelectedColor(_ color:String) {
        print("color123",color)
        communityDominantColourValue = color
        isSetDominontColor = false
    }
}


extension EditChannelProfileVC : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserAreaInterestTblCell", for: indexPath) as! UserAreaInterestTblCell
        cell.selectionStyle = .none
        let category = categories_list[indexPath.row]
        cell.lblofDetailsTxt.text = category.name
        cell.imageViewOfInterest.sd_setImage(with: URL(string: category.image_url!), placeholderImage: UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
        cell.btnCheckBox.isHidden = false
        cell.btnCheckBox.tag = indexPath.row + 10
        cell.btnCheckBox.addTarget(self,action:#selector(btnCloseClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnCloseClicked(sender:UIButton){
        txtOfCommunityName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        let point = tblCatogory.convert(sender.center, from: sender.superview)
        let indexPath = tblCatogory.indexPathForRow(at: point)
        //let cell = tblCatogory!.cellForRow(at: indexPath!)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        // let button = cell?.viewWithTag((indexPath?.row)!) as? UIButton
        //let button = cell?.viewWithTag((indexPath?.row)! + 10) as? UIButton
        // [self.categories.removeObject(at: (indexPath?.row)!)]
        // [self.categoriesName.removeObject(at: (indexPath?.row)!)]
        categories_list.remove(at: (indexPath?.row)!)
        lblCategoryCount.text = String(categories_list.count)
        tblCatogory.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 75
    }
}

extension  EditChannelProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: UICollectionViewDelegate & UICollectionViewDataSource Functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : EditChannelProductServicesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditChannelProductServicesCell", for: indexPath) as! EditChannelProductServicesCell
        cell.viewOfBackEditButton.layer.cornerRadius = cell.viewOfBackEditButton.frame.size.width/2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //If you want your cell should be square in size the return the equal height and width, and make sure you deduct the Section Inset from it.
        return CGSize(width: (view.frame.size.width / 3) - 20 , height: (view.frame.size.width / 3) - 45)
    }
   
}


extension EditChannelProfileVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == txtViewOfAboutCommunity{
    
            let currentText = txtViewOfAboutCommunity.text ?? StringConstants.EMPTY
            guard let stringRange = Range(range, in: currentText) else { return false }
            let changedText = currentText.replacingCharacters(in: stringRange, with: text)
            strChannelDescription = changedText
            if text.isEmpty {
                if changedText.count < 1 {
                    txtViewOfAboutCommunity.text = StringConstants.EMPTY
                }
            }else{
                txtViewOfAboutCommunity.text = textView.text
                return changedText.count <= 100
            }
        }
        return true
     }

}

extension EditChannelProfileVC : UITextFieldDelegate{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtOfCommunityName){
//        guard let textFieldText = textField.text,
//            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
//                return false
//        }
//        let substringToReplace = textFieldText[rangeOfTextToReplace]
//        let countValue = textFieldText.count - substringToReplace.count + string.count
        //return count <= 25
            
            let currentText = textField.text ?? StringConstants.EMPTY
            var updatedText = String()
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                updatedText = currentText.replacingCharacters(in: textRange, with: string)
            }
            
            let validString = NSCharacterSet(charactersIn: "!@#$%^*_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
            if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                print(range)
                return false
            }
            if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                return false
            } else {
                guard range.location == 0 else {
                    return updatedText.count <= 25
                }
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                if newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location == 0 {
                    return false
                }else{
                    return updatedText.count <= 25
                }
            }
        }
        return true
    }
}


extension EditChannelProfileVC: UIImageCropperProtocol {
    
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imageViewOfProfilePic.image = croppedImage
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfProfilePic.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
