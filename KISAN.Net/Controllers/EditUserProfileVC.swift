//
//  EditUserProfileVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 13/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class EditUserProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate,PassSelectedCategory,CropViewControllerDelegate{
   
    
    
    @IBOutlet weak var viewOfBackProfilePic: UIView!
    @IBOutlet weak var imageViewOfProfilePic: UIImageView!
    @IBOutlet weak var txtViewOfAboutCommunity: UITextView!
    var dominantColour = String()
    @IBOutlet weak var tblCatogory: UITableView!
    @IBOutlet weak var heightOfTblCatagoty: NSLayoutConstraint!
    @IBOutlet weak var lblCategoryCount: UILabel!
    var profileImage: UIImage!
    var editCommunityRequest = Dictionary<String, Any>()
    var arrOFCatName =  [String]()
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var viewOfBackLastName: UIView!
    @IBOutlet weak var viewOfBackFirstName: UIView!
    @IBOutlet var lblInterest: UILabel!
    @IBOutlet var btnAddMore: UIButton!
    @IBOutlet var lblAboutYou: UILabel!
    @IBOutlet var lblphoto: UILabel!
    
    var categories_list: [CategoriesList] = []
    var userId = String()
    var categoryIds = String()
    var base64String: String = ""
    @IBOutlet weak var lblSurnameCannotBlank: UILabel!
    @IBOutlet weak var lblOfNameCannotBlank: UILabel!
    var nameTxtLength: Int! = 0
    var lastNameTxtLength: Int! = 0
    let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.'"
    var firstLoad = true
    private let picker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        lblInterest.text = (NSLocalizedString(StringConstants.showUserProfile.Interests, comment: StringConstants.EMPTY))
        lblAboutYou.text = (NSLocalizedString(StringConstants.showUserProfile.aboutYou, comment: StringConstants.EMPTY))
        btnAddMore.setTitle((NSLocalizedString(StringConstants.showUserProfile.AddMore, comment: StringConstants.EMPTY)), for: .normal)
        txtFirstName.placeholder = (NSLocalizedString(StringConstants.showUserProfile.name, comment: StringConstants.EMPTY))
        txtLastName.placeholder = (NSLocalizedString(StringConstants.showUserProfile.surname, comment: StringConstants.EMPTY))
        lblphoto.text = (NSLocalizedString(StringConstants.showUserProfile.Photo, comment: StringConstants.EMPTY))

        self.initialDesign()
        
    }
   
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(btnDoneTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        imageViewOfProfilePic.layer.cornerRadius = imageViewOfProfilePic.frame.size.width/2
        imageViewOfProfilePic.clipsToBounds = true
        viewOfBackProfilePic.layer.cornerRadius = viewOfBackProfilePic.frame.size.width/2
        viewOfBackProfilePic.clipsToBounds = true
        tblCatogory.separatorColor = UIColor.clear
        
        viewOfBackFirstName.layer.cornerRadius = 6.0
        viewOfBackFirstName.clipsToBounds = true
        
        viewOfBackLastName.layer.cornerRadius = 6.0
        viewOfBackLastName.clipsToBounds = true
        
        let  lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.yourProfile, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        lblCategoryCount.text = String(categories_list.count)
        txtFirstName.text = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        txtLastName.text = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        if let userProfilePic = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl) {
            if userProfilePic.isEmpty{
                viewOfBackProfilePic.isHidden = false
            }else{
                self.imageViewOfProfilePic.sd_setImage(with: URL(string: userProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.userProfilePlaceHolderImg), options: .refreshCached)
                viewOfBackProfilePic.isHidden = true
                if imageViewOfProfilePic.image != UIImage(named: StringConstants.ImageNames.userProfilePlaceHolderImg) {
                    let imageData:NSData = UIImageJPEGRepresentation(imageViewOfProfilePic.image!, 0.7)! as NSData
                    base64String = imageData.base64EncodedString()
                }else{
                    base64String = StringConstants.EMPTY
                }
            }
        }
        
        //txtview placeholder
        txtViewOfAboutCommunity.returnKeyType = UIReturnKeyType.next
         if let about = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.about) {
            txtViewOfAboutCommunity.text = about
         }else{
            txtViewOfAboutCommunity.text = StringConstants.placeHolder.aboutYou
            txtViewOfAboutCommunity.textColor = UIColor.lightGray
        }
        
        txtViewOfAboutCommunity.selectedTextRange = txtViewOfAboutCommunity.textRange(from: txtViewOfAboutCommunity.beginningOfDocument, to: txtViewOfAboutCommunity.beginningOfDocument)
        
        tblCatogory .reloadData()
    }
    
    @objc func btnDoneTapped(sender: UIBarButtonItem) {
        print("Done Tapped")
        if (txtFirstName.text?.isEmpty)! {
            lblOfNameCannotBlank.isHidden = false
            lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))

            txtFirstName.becomeFirstResponder()
        }else if (!(txtFirstName.text?.isEmpty)!) {
            let str = txtFirstName.text
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = str!.rangeOfCharacter(from: decimalCharacters)
            if nameTxtLength == 1 {
                lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                lblOfNameCannotBlank.isHidden = false
                txtFirstName.becomeFirstResponder()
            }else if decimalRange != nil  {
              lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.notnumber, comment: StringConstants.EMPTY))
              self.lblOfNameCannotBlank.isHidden = false
             txtFirstName.becomeFirstResponder()
        }else if (txtLastName.text?.isEmpty)! {
                lblSurnameCannotBlank.isHidden = false
                lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                txtLastName.becomeFirstResponder()
            } else if (!(txtLastName.text?.isEmpty)!) {
                let str = txtLastName.text
                let decimalCharacters2 = CharacterSet.decimalDigits
                let decimalRange2 = str!.rangeOfCharacter(from: decimalCharacters2)
                if lastNameTxtLength == 1 {
                    lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    lblSurnameCannotBlank.isHidden = false
                    txtLastName.becomeFirstResponder()
                }else if decimalRange2 != nil  {
                    lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.notnumber, comment: StringConstants.EMPTY))
                    self.lblSurnameCannotBlank.isHidden = false
                    txtLastName.becomeFirstResponder()
                }else if categories_list.count == 0{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.selectAtleastOneInterest, comment: StringConstants.EMPTY)))
                }else{
                    self.callEditUserProfile()
                }
            }
        }
        
    }
    
    func callEditUserProfile(){
        
        for i in (0..<categories_list.count){
            let categories = categories_list[i]
            let categoriesId = categories.id
            
            if categoryIds.isEmpty{
                categoryIds = categoriesId!
            }else{
                categoryIds = categoryIds + StringConstants.comma + categoriesId!
            }
        }
        let strCategories = categoryIds
        var about = String()
        if txtViewOfAboutCommunity.text == StringConstants.placeHolder.aboutYou {
            about = StringConstants.EMPTY
        }else{
            about = txtViewOfAboutCommunity.text
        }
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let oAuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let mobileNum = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
        let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
     
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)
        
        txtFirstName.text =  txtFirstName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtLastName.text =  txtLastName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        let params = EditUserDetailsRequest.convertToDictionary(userDetails: EditUserDetailsRequest.init(firstName: txtFirstName.text!, lastName: txtLastName.text!, mobile: mobileNum, accountType: account_Type ?? Int(), password: userId, country: StringConstants.country, state: state, city: city, profile_status: about, image_url: StringConstants.EMPTY, big_thumb_url: StringConstants.EMPTY, small_thumb_url: StringConstants.EMPTY, categories: strCategories, email: StringConstants.EMPTY, about: about, imageBlob: base64String, oAuthToken: oAuthToken,source:RequestName.loginSourceName))
        print("params",params)

        let apiURL = URLConstant.BASE_URL + URLConstant.editUserProfile + userId
        print("apiURL",apiURL)
        _ = EditUserProfileServices().editUserProfileServices(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! EditUserProfileResponse
                                                                let userData = model.user_details[0]
                                                                let userProfile = userData.userProfile[0]
                                                                let interests = userProfile.interests!
                                                                
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                                                
                                                                if let pin = userProfile.pin {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }

                                                                
                                                                let strInterests = interests.joined(separator: ",")
                                                                if interests.count > 0 {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }
                                                                
                                                                if let imageUrl = userProfile.imageUrl{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                    
                                                                    SDImageCache.shared().removeImage(forKey: userProfile.imageBigthumbUrl!, withCompletion: nil)
                                                                    
                                                                    let url = NSURL(string:userProfile.imageBigthumbUrl!)
                                                                    let  data = NSData(contentsOf:url! as URL)
                                                                    if data != nil {
                                                                        SDImageCache.shared().storeImageData(toDisk: data! as Data, forKey: userProfile.imageBigthumbUrl!)
                                                                    }
                                                                
                                                                    

                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                }
                                                                
                                                                self.txtViewOfAboutCommunity.resignFirstResponder()
                                                                self.txtFirstName.resignFirstResponder()
                                                                self.txtLastName.resignFirstResponder()
                                                            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.updatedUserProfile, comment: StringConstants.EMPTY)))

                                                                self.navigationController?.popViewController(animated: false)
                                                                
                                                                MBProgressHUD.hide(for: self.view, animated: true);

                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* if dominantColour.isEmpty {
         UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
         self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
         viewOfBackProfilePic.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
         
         }else{
         UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
         self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: dominantColour)
         viewOfBackProfilePic.backgroundColor = UIColor.init(hexString: dominantColour)
         }*/
    }
    
    override func viewDidLayoutSubviews() {
        super.updateViewConstraints()
        heightOfTblCatagoty.constant = CGFloat(self.categories_list.count * 75)
        self.updateViewConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCaptureProfilePic(_ sender: Any) {
        
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.photoLibrary, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewOfProfilePic.contentMode = .scaleAspectFit
            imageViewOfProfilePic.image = pickedImage
            viewOfBackProfilePic.isHidden = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                self.openEditor(nil)
            }
        }
    }
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        
       /* let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)*/
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
      
    }
    
    
    
    // MARK: - CropView New Library delegate
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
       // imageViewOfProfilePic.image = image
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        imageViewOfProfilePic.image = image
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfProfilePic.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        controller.dismiss(animated: true, completion: nil)

    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
 
    
    @IBAction func btnColorPickerClick(_ sender: Any) {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
    }
    
    @IBAction func btnAddMoreCategoryClick(_ sender: Any) {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.EditUserProfileVC, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let userAreaInterestVC = storyboard.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
        userAreaInterestVC.delegate = self
        userAreaInterestVC.selectedCategories_list = categories_list
        self.navigationController?.pushViewController(userAreaInterestVC, animated: true)
    }
    
    func PassSelectedCategory(_ category: [CategoriesList]) {
        categories_list.removeAll()
        categories_list = category
        lblCategoryCount.text = String(categories_list.count)
        tblCatogory.reloadData()
        heightOfTblCatagoty.constant = CGFloat(self.categories_list.count * 75)
        self.updateViewConstraints()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EditUserProfileVC : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserAreaInterestTblCell", for: indexPath) as! UserAreaInterestTblCell
        cell.selectionStyle = .none
        let category = categories_list[indexPath.row]
        cell.lblofDetailsTxt.text = category.name
        cell.imageViewOfInterest.sd_setImage(with: URL(string: category.image_url!), placeholderImage: UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
        cell.btnCheckBox.isHidden = false
        cell.btnCheckBox.tag = indexPath.row + 10
        cell.btnCheckBox.addTarget(self,action:#selector(btnCloseClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnCloseClicked(sender:UIButton){
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtViewOfAboutCommunity.resignFirstResponder()
        let point = tblCatogory.convert(sender.center, from: sender.superview)
        let indexPath = tblCatogory.indexPathForRow(at: point)
        //let cell = tblCatogory!.cellForRow(at: indexPath!)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        // let button = cell?.viewWithTag((indexPath?.row)!) as? UIButton
        //let button = cell?.viewWithTag((indexPath?.row)! + 10) as? UIButton
        // [self.categories.removeObject(at: (indexPath?.row)!)]
        // [self.categoriesName.removeObject(at: (indexPath?.row)!)]
        categories_list.remove(at: (indexPath?.row)!)
        lblCategoryCount.text = String(categories_list.count)
        tblCatogory.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 75
    }
}


extension EditUserProfileVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            
            txtLastName .becomeFirstResponder()
            
        }else if textField == txtLastName{
            
            txtLastName .resignFirstResponder()
            
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            
            txtFirstName.returnKeyType = UIReturnKeyType.next
            
        }else if textField == txtLastName{
            
            txtLastName.returnKeyType = UIReturnKeyType.done
            
        }
        return true;
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtFirstName {
            
            var basicBool : Bool = false
            nameTxtLength = (txtFirstName.text! as NSString).length + (string as NSString).length - range.length as Int
            
            if  (!(txtFirstName.text?.isEmpty)! || !(string.isEmpty)) {
                
                if nameTxtLength == 1 {
                    lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    lblOfNameCannotBlank.isHidden = false
                    
                } else if nameTxtLength == 0 {
                    lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))
                    lblOfNameCannotBlank.isHidden = false
                    
                }else{
                    lblOfNameCannotBlank.isHidden = true
                }
                
                //Not allowed type emoji
                if #available(iOS 7, *){
                    if textField.isFirstResponder {
                        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                            // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                            return false
                        }
                    }
                }
                //Not all special character
//                let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                return (string == filtered)
                let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
                //Not allowed digits
                if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                    return false
                } else {
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                    //return true
                }
                
            }else{
                
                if basicBool == false {
                    
                    basicBool = true
                    lblOfNameCannotBlank.isHidden = false
                    // Not allow space in beginning of textfield
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (txtFirstName.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    let trimmedString = string.trimmingCharacters(in: .whitespaces)
                    if (trimmedString.isEmpty || trimmedString == "" ) {
                        if nameTxtLength == 1{
                            nameTxtLength = nameTxtLength - 1
                        }
                    }else if ACCEPTABLE_CHARACTERS.range(of: newString as String) != nil{
                        print("Got the string")
                    }else{
                        if nameTxtLength == 1{
                            nameTxtLength = nameTxtLength - 1
                        }
                    }
                    
                    if nameTxtLength == 1 {
                        lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    } else if nameTxtLength == 0 {
                        lblOfNameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))
                    }
                    
                    //Not allowed type emoji
                    if #available(iOS 7, *){
                        if textField.isFirstResponder {
                            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                                // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                                return false
                            }
                        }
                    }
                    
                    //Not all special character & white space
                    //                    let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                    //                    let filtered: String = (newString.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
                    //                    let whiteSpaceValue = newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
                    //                    if (string == filtered && whiteSpaceValue == true){
                    //                        return true
                    //                    }else{
                    //                        return false
                    //                    }
                    
                    let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                    if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                        print(range)
                        return false
                    }
                    //Not allowed digits
                    if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                        return false
                    } else {
                        guard range.location == 0 else {
                            return true
                        }
                        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                        //return true
                    }
                    
                }
                
            }
            
            
        }else if textField == txtLastName{
            
            var basicBool : Bool = false
            lastNameTxtLength  = (txtLastName.text! as NSString).length + (string as NSString).length - range.length as Int
            
            if  (!(txtLastName.text?.isEmpty)! || !(string.isEmpty)) {
                
                if lastNameTxtLength == 1 {
                    lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    lblSurnameCannotBlank.isHidden = false
                    
                } else if lastNameTxtLength == 0 {
                    lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                    lblSurnameCannotBlank.isHidden = false
                    
                }else{
                    lblSurnameCannotBlank.isHidden = true
                }
                
                //Not allowed type emoji
                if #available(iOS 7, *){
                    if textField.isFirstResponder {
                        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                            // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                            return false
                        }
                    }
                }
                
                //Not all special character
//                let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                return (string == filtered)
                
                let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
                //Not allowed digits
                if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                    return false
                } else {
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                    //return true
                }
                
            }else{
                
                if basicBool == false {
                    basicBool = true
                    lblSurnameCannotBlank.isHidden = false
                    
                    // Not allow space in beginning of textfield
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (txtLastName.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    let trimmedString = string.trimmingCharacters(in: .whitespaces)
                    if (trimmedString.isEmpty || trimmedString == "" ) {
                        if lastNameTxtLength == 1{
                            lastNameTxtLength = lastNameTxtLength - 1
                        }
                    }else if ACCEPTABLE_CHARACTERS.range(of: newString as String) != nil{
                        print("Got the string")
                    }else{
                        if lastNameTxtLength == 1{
                            lastNameTxtLength = lastNameTxtLength - 1
                        }
                    }
                    
                    if lastNameTxtLength == 1 {
                        lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    } else if nameTxtLength == 0 {
                        lblSurnameCannotBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                    }
                    
                    //Not allowed type emoji
                    if #available(iOS 7, *){
                        if textField.isFirstResponder {
                            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                                // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                                return false
                            }
                        }
                    }
                    
                    //Not all special character & white space
//                    let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                    let filtered: String = (newString.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                    let whiteSpaceValue = newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
//                    if (string == filtered && whiteSpaceValue == true){
//                        return true
//                    }else{
//                        return false
//                    }
                    
                    let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                    if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                        print(range)
                        return false
                    }
                    //Not allowed digits
                    if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                        return false
                    } else {
                        guard range.location == 0 else {
                            return true
                        }
                        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                        //return true
                    }
                }
            }
        }
        return true
    }
    
}

extension EditUserProfileVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        let currentText = textView.text
        let updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        let trimmedString = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if updatedText.isEmpty {
            textView.text = ""
            txtViewOfAboutCommunity.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }else if txtViewOfAboutCommunity.textColor == UIColor.lightGray && !text.isEmpty && trimmedString != "" && !trimmedString.isEmpty && trimmedString != StringConstants.placeHolder.aboutYou  {
            textView.text = nil
            txtViewOfAboutCommunity.textColor = UIColor.black
        }
        let  newTaxt = (currentText! as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newTaxt.characters.count
        return numberOfChars <= 144
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedStringKey.font: font],
                                                 context: nil).size
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if textView.text.isEmpty {
            textView.text = StringConstants.placeHolder.aboutYou
            textView.textColor = UIColor.lightGray
            txtViewOfAboutCommunity.resignFirstResponder()
        }
    }

}

extension EditUserProfileVC: UIImageCropperProtocol {
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imageViewOfProfilePic.image = croppedImage
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfProfilePic.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
