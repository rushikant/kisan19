//
//  ExhibitionAdvertiseVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 20/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import SafariServices

class ExhibitionAdvertiseVC: UIViewController,SFSafariViewControllerDelegate {

    @IBOutlet weak var lblOfDate: UILabel!
    @IBOutlet weak var lblOfTime: UILabel!
    @IBOutlet weak var lblOfPlace: UILabel!
    @IBOutlet var lblMakeSure: UILabel!
    @IBOutlet var btnregister: UIButton!
    @IBOutlet var lblregister: UILabel!
    @IBOutlet var btnSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnregister.layer.cornerRadius = 8
        btnregister.clipsToBounds = true
        self.SetData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func SetData(){
        btnSkip.setTitle((NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.SkipNow, comment: StringConstants.EMPTY)), for: .normal)
        //let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        //spinnerActivity.isUserInteractionEnabled = false
        //self.exhibitionAds()
    }
    
    
    func exhibitionAds(){
        
        let params = ExhibitionAdvertiseDetailsRequest.convertToDictionary(exhibitorAdvertiseDetails: ExhibitionAdvertiseDetailsRequest.init(app_key: URLConstant.GREENPASS_APPKEY, eventCode: URLConstant.EVENT_CODE, source: RequestName.loginSourceName, discount_name: "online", referrer_code: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.exhibitionDetails
        _ = ExhibitionAdvertiseServices().exhibitionAdvertise(apiURL,
                                                    postData: params as [String : AnyObject],
                                                    withSuccessHandler: { (userModel) in
                                                        let model = userModel as! ExhibitionAdvertiseResponse
                                                        let userData = model.exhibitiondetails
                                                        
                                                        let eventstartdatetime1 = userData["eventstartdatetime"]
                                                        let speprateStartDateTime = eventstartdatetime1?!.components(separatedBy: " ")
                                                        let startDate = speprateStartDateTime?[0]
                                                        let startTime = speprateStartDateTime?[1]
                                                        let lastTwoCharOfStartDate = String(startDate!.suffix(2))

                                                        let eventenddatetime = userData["eventenddatetime"] // eventenddatetime
                                                        let speprateEndDateTime = eventenddatetime?!.components(separatedBy: " ")
                                                        let endDate = speprateEndDateTime?[0]
                                                        let endTime = speprateEndDateTime?[1]
                                                        
                                                        let convertedDate = self.dateConverter(date: endDate ?? String())
                                                        let finalDate = lastTwoCharOfStartDate + StringConstants.singleSpace + "-" + StringConstants.singleSpace + convertedDate
                                                        
                                                        let convrtedStartTime = self.timeConverter(time:startTime ?? String())
                                                        let convertedEndTime = self.timeConverter(time:endTime ?? String())
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        self.lblOfDate.text = finalDate
                                                        self.lblOfTime.text = convrtedStartTime + " to " + convertedEndTime

                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: StringConstants.Validation.userNamePasswordNotMatch, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    func dateConverter(date:String) -> String{
        var convertedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM,  yyyy"
        if let convertdate = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: convertdate))
            convertedDate = dateFormatterPrint.string(from: convertdate)
        } else {
            print("There was an error decoding the string")
        }
        return convertedDate
    }
    
    func timeConverter(time:String) -> String{
        
        var convertedTime = String()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date: Date? = dateFormatter.date(from: time )
        dateFormatter.dateFormat = "hh:mm a"
        if let aDate = date {
            convertedTime = dateFormatter.string(from: aDate)
        }
        
        return convertedTime
    }
    
    
    @IBAction func btnCloseClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(DashboardVC, animated: false)
    }
    @IBAction func btnRegisterClick(_ sender: Any) {
       let AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let greenpassfinalurl = URLConstant.greenPassbaseUrl + URLConstant.greenpassToken + AuthToken
        let url = URL(string: greenpassfinalurl)
        let controller = SVWebViewController(url: url)
        self.navigationController?.pushViewController(controller!, animated: true)

    }
    
    @IBAction func btnSkipClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(DashboardVC, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
