//
//  ExhibitorLoginVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import SVProgressHUD
import XMPPFramework
import MBProgressHUD

class ExhibitorLoginVC: UIViewController {
    var lblOfTitle = UILabel()
    @IBOutlet weak var viewOfBackUserName: UIView!
    @IBOutlet weak var viewOfBackPassword: UIView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet var lblexhibitorLogin: UILabel!
    @IBOutlet var lblinitialtext: UILabel!
    var AuthToken = String()

    
    public var interests = [String]()
    var state = String()
    var appDelegate = AppDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialDesign()
    }
    
    func initialDesign() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.navtitle, comment: StringConstants.EMPTY)
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        txtUserName.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        txtPassword.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        viewOfBackUserName.layer.cornerRadius = 5.0
        viewOfBackPassword.layer.cornerRadius = 5.0
        btnLogin.layer.cornerRadius = 5.0
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        lblexhibitorLogin.text = NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.ExhibitorLogin, comment: StringConstants.EMPTY)
         lblinitialtext.text = NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.Initialtext, comment: StringConstants.EMPTY)
        txtUserName.placeholder = NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.emailplaceholder, comment: StringConstants.EMPTY)
        txtPassword.placeholder = NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.passwordplaceholder, comment: StringConstants.EMPTY)
        btnLogin.setTitle(NSLocalizedString(StringConstants.NavigationTitle.LoginAsExhibitior, comment: StringConstants.EMPTY), for: .normal)

    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnLoginClick(_ sender: Any) {
        if (txtUserName.text?.isEmpty)! {
            self.navigationController?.view.makeToast(StringConstants.Validation.userNameCantBlank)
        }else if(txtPassword.text?.isEmpty)!{
            self.navigationController?.view.makeToast(StringConstants.Validation.passwordCantBlank)
        }else{
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.loginToExhibitor()
            }else{
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))

            }
        }
    }
    
    func loginToExhibitor(){
        
        let fcmToken = Messaging.messaging().fcmToken
        //let strUUID = UUIDHelper.getUUID()
        var type = String()
        let userName = txtUserName.text
        if userName!.range(of:"@") != nil {
            type = RequestName.typeEmail
        }else{
            type = RequestName.typeMobile
        }
        
        let params = ExhibitorLoginDetailsRequest.convertToDictionary(exhibitorLoginDetails: ExhibitorLoginDetailsRequest.init(client_id: URLConstant.exhibitor_client_id, client_secret: URLConstant.exhibitor_client_secret, source: RequestName.loginSourceName, account_type: RequestName.account_type, type: type, username: txtUserName.text ?? String(), password: txtPassword.text ?? String(), resource: RequestName.resourceName, fcmDeviceId: fcmToken!, app: RequestName.app))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.ExhibitorLogin
        _ = ExhibitorLoginServices().loginExhibitor(apiURL,
                                                    postData: params as [String : AnyObject],
                                                    withSuccessHandler: { (userModel) in
                                                        let model = userModel as! ExhibitorLoginResponse
                                                        let userData = model.data[0]
                                                        let userProfile = userData.user_profile[0]
                                    
                                                        self.interests = userProfile.interests!
                                                        let middlewareToken = userData.token
                                                        print("middlewareToken",middlewareToken ?? String())
                                                        let oAuthToken = userData.oAuthToken
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:middlewareToken! , keyString: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString: oAuthToken!, keyString: StringConstants.NSUserDefauluterKeys.oAuthToken)
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.first_name! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                                    
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.last_name! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.username! , keyString: StringConstants.NSUserDefauluterKeys.username)
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString: userProfile.id!, keyString: StringConstants.NSUserDefauluterKeys.userId)
                                                        //SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                                        let accountType = String(userProfile.account_type!)
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:accountType , keyString: StringConstants.NSUserDefauluterKeys.accountType)
                                                        //***
                                                        if let country = userProfile.country{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:country , keyString: StringConstants.NSUserDefauluterKeys.country)
                                                        }
                                                        //Show overlay on discover screen
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.TRUE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
                                                        
                                                        let strInterests = self.interests.joined(separator: ",")
                                                        if self.interests.count > 0 {
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                        }else{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                        }
                                                        
                                                        if let imageUrl = userProfile.image_url{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.image_bigthumb_url!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.image_smallthumb_url!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                        }else{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                        }
                                                        
                                                        var mobileNum = String()
                                                        if let state = userProfile.state{
                                                            self.state = state  //state city
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: state, keyString: StringConstants.NSUserDefauluterKeys.state)
                                                        }else{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
                                                        }
                                                        
                                                        if let city = userProfile.city{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: city, keyString: StringConstants.NSUserDefauluterKeys.city)
                                                        }else{
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
                                                        }
                                                        
                                                        if let mobile1 = userProfile.mobile1{
                                                            mobileNum = mobile1
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                                        }else{
                                                            if let mobile2 = userProfile.mobile2{
                                                                mobileNum = mobile2
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                                            }
                                                        }
                                                        
                                                        var ejabberdUserId = userProfile.id
                                                        ejabberdUserId = ejabberdUserId! + "@" + URLConstant.hostName + "/" + RequestName.resourceName
                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId! , keyString:StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                        
                                                        self.didTouchLogIn(userJID: ejabberdUserId!, userPassword: middlewareToken!, server: URLConstant.hostName)
                                                        print("ejabberdUserId",ejabberdUserId ?? String())
                                                        print("Password as middlewareToken",middlewareToken ?? String())
                                                        print("hostName",URLConstant.hostName)
                                                        
                                                        self.sighIntoGreenCloud()
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: StringConstants.Validation.userNamePasswordNotMatch, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.pendingInvitationCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
//            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
//                },{action2 in
//                }, nil])
        })
    }
    
    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
        do {
            try appDelegate.xmppController = XMPPController(hostName: server,
                                                            userJIDString: userJID,
                                                            password: userPassword)
            appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
            appDelegate.xmppController.connect()
            
        } catch {
            print("Something went wrong")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ExhibitorLoginVC: XMPPStreamDelegate {
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        // Move to chat controller
        print("Aunthenticated at viewcontroller")
        
        if (self.interests.count) > 0 {
            MBProgressHUD.hide(for: self.view, animated: true);
            if (state.isEmpty){
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectLocationVC = storyboard.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                self.navigationController?.pushViewController(selectLocationVC, animated: true)
                
            }else{
                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
                    print("retriveState",retriveState)
                }
                
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                self.navigationController?.pushViewController(DashboardVC, animated: true)
            }
            
        }else{
            MBProgressHUD.hide(for: self.view, animated: true);
            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.UserAreaInterestViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
            let userAreaVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
            self.navigationController?.pushViewController(userAreaVC, animated: true)
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Wrong password or username")
    }
}

extension ExhibitorLoginVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
