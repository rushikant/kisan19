//
//  ForwordMessageViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ForwordMessageViewController: UIViewController {

    @IBOutlet weak var tblMyChatList: UITableView!
    var arrOfChannelName = ["Kisan Hinjewadi","Kisan kothrud","Kisan Koplhapur", "Kisan Sangali","Kisan Mumbai"]
    var lblOfseprator = UILabel()
    var lblOfTitle = UILabel()
    var forwordMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         initialDesign()
    }

    
    func initialDesign(){
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Discover", style: .plain, target: self, action: #selector(btnNextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        tblMyChatList.tableFooterView = UIView()
        
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.dashboard, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setNavigationBarItem()
        self.navigationItem.hidesBackButton = true
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
    }
    
    @objc func btnNextTapped(sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let DiscoverChannelVC = storyboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as UIViewController
        self.navigationController?.pushViewController(DiscoverChannelVC, animated: true)
    }
    
    func btnLogout(sender: UIBarButtonItem) {
        print("Right button click")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ForwordMessageViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOfChannelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatListTblCell", for: indexPath) as! MyChatListTblCell
        cell.selectionStyle = .none
        cell.imgCommunityProfile.layer.cornerRadius = cell.imgCommunityProfile.frame.size.width / 2
        cell.imgCommunityProfile.clipsToBounds = true
        cell.lblCommunityName.text = arrOfChannelName[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            // For Admin
            let channelDashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
            channelDashboardVC.ownerId = StringConstants.IDS.ownerId
            
            //            var dominantColour = String(describing: myChatCommunity.communityDominantColour!)
            //            if dominantColour.isEmpty{
            //                channelDashboardVC.dominantColour = ""
            //            }else {
            //                dominantColour = dominantColour.replacingOccurrences(of: "\"", with: "", options:  NSString.CompareOptions.literal, range: nil)
            //                dominantColour.remove(at:  (dominantColour.startIndex))
            //                dominantColour = "0x" + dominantColour
            //                channelDashboardVC.dominantColour = dominantColour
            //
            //            }
            self.navigationController?.pushViewController(channelDashboardVC, animated: true)
        } else {
            let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            channelSendBroadcastMsgVC.ownerId = ""
            
            //            var dominantColour = String(describing: myChatCommunity.communityDominantColour!)
            //            if dominantColour.isEmpty{
            //                channelSendBroadcastMsgVC.dominantColour = ""
            //            }else {
            //                dominantColour = dominantColour.replacingOccurrences(of: "\"", with: "", options:  NSString.CompareOptions.literal, range: nil)
            //                dominantColour.remove(at:  (dominantColour.startIndex))
            //                dominantColour = "0x" + dominantColour
            //                channelSendBroadcastMsgVC.dominantColour = dominantColour
            //            }
            
            self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 90
    }
}
