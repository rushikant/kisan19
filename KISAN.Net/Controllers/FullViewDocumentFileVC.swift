//
//  FullViewDocumentFileVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 01/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class FullViewDocumentFileVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var documentDataFromLocal = String()
    var documentDataFromServer = String()
    var mediaType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialDesign()
        self.setData()
    }
    
    func intialDesign(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setData(){
        if (!(documentDataFromLocal.isEmpty)  && !(documentDataFromLocal == "") && !(documentDataFromLocal == "null")){
            var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            pdfURL = pdfURL.appendingPathComponent(documentDataFromLocal) as URL
            let data = try! Data(contentsOf: pdfURL)
            print("data",data)
            if mediaType == ".doc"{
                self.webView.load(data, mimeType: "application/msword", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
            }else if mediaType == ".docx"{
                self.webView.load(data, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
            }
            else{
                self.webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
            }
        }else{
            
            let documentData =  NSURL(string:documentDataFromServer)!
            print("import result : \(documentData)")
            do{
                let docData = try NSData(contentsOf: documentData as URL, options: .mappedIfSafe)
                if mediaType == ".doc"{
                    webView.load(docData as Data, mimeType: "application/msword", textEncodingName:"", baseURL: documentData.deletingLastPathComponent!)
                }else{
                    webView.load(docData as Data, mimeType: "application/pdf", textEncodingName:"", baseURL: documentData.deletingLastPathComponent!)
                }
                
            }catch{
                print("Error occures")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
