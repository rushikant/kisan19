//
//  FullViewOfImageFromMessagesVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 21/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class FullViewOfImageFromMessagesVC: UIViewController {
    
    @IBOutlet weak var heightOfSelectedImg: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfSelectedImg: UIImageView!
    @IBOutlet weak var lblOfChannelName: UILabel!
    @IBOutlet weak var lblOfCaptionText: UILabel!
    var channelName = String()
    var captionText = String()
    var imageDataFromLocalDB = String()
    var imageDataFromServer = String()
    let documentDirectoryHelper = DocumentDirectoryHelper()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)

        if (!(imageDataFromLocalDB.isEmpty)  && !(imageDataFromLocalDB == "") && !(imageDataFromLocalDB == "null")){
            let image = documentDirectoryHelper.getImage(imageName: imageDataFromLocalDB)
           imageViewOfSelectedImg.image = image
        }else{
            imageViewOfSelectedImg.sd_setImage(with: URL(string: imageDataFromServer), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
        }
        
        lblOfChannelName.text = channelName
        lblOfCaptionText.text = captionText
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIScreen.main.bounds.size.height <= 568{
            heightOfSelectedImg.constant = 400
        }else{
            heightOfSelectedImg.constant = 498
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
