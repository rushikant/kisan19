//
//  FullViewProfilePicVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 10/05/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class FullViewProfilePicVC: UIViewController {

    @IBOutlet weak var imageViewOfProfilePic: UIImageView!
    @IBOutlet weak var lblOfNavTitle: UILabel!
    @IBOutlet weak var heightOfProfilePicConstant: NSLayoutConstraint!
    var commingFrom = String()
    var imageData = String()
    var navName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        if UIScreen.main.bounds.size.height <= 568{
            heightOfProfilePicConstant.constant = 472
        }else if UIScreen.main.bounds.size.height == 812{
            heightOfProfilePicConstant.constant = 682
        }else{
            heightOfProfilePicConstant.constant = 570
        }
        
        if commingFrom == StringConstants.flags.showUserProfileVC{
            let imgurl = imageData
            let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                self.imageViewOfProfilePic.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg), options: .refreshCached)
            lblOfNavTitle.text = StringConstants.EMPTY
        }else if commingFrom == StringConstants.flags.channelProfileVC{
            let imgurl = imageData
            let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
         imageViewOfProfilePic.sd_setImage(with: URL(string: rep2), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
            lblOfNavTitle.text = navName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnBackClick(_ sender: Any) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: false)
    }
    
}
