//
//  GreenPassViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 17/09/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class GreenPassViewController: UIViewController,WKNavigationDelegate {
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(false, animated: false)
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        //create an instance of webview
        self.webView = WKWebView(frame: view.bounds, configuration: configuration)
        // self.webView = WKWebView(frame: CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height-64), configuration: configuration)
        //let AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        //URLConstant.greenPassbaseUrl + URLConstant.greenpassToken + AuthToken
        
        
        //For layout of exebution
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let greenpassfinalurl =  URLConstant.layout
            print("greenPassbaseUrl",greenpassfinalurl)
            view.addSubview(webView)
            webView.navigationDelegate = self
            webViewLoadURL(urlString: greenpassfinalurl)
        }else{
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Left Drawer
        self.setNavigationBarItem()
    }
    
    func webViewLoadURL(urlString: String){
        if let url = URL(string: urlString) {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            webView.load(URLRequest(url: url))
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        MBProgressHUD.hide(for: self.view, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark:- WKNevigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url,
                let host = url.host, !host.hasPrefix("https://play.google.com/"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                print(url)
                print("Redirected to browser. No need to open it locally")
                decisionHandler(.cancel)
            } else {
                print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        MBProgressHUD.hide(for: self.view, animated: true);
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        MBProgressHUD.hide(for: self.view, animated: true);
        print("Failed loading webview due to: ", error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        MBProgressHUD.hide(for: self.view, animated: true);
        print("Failed provisional loading webview due to: ", error)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
