//
//  CongRecievedPassVC.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 15/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class CongRecievedPassVC: UIViewController {
    @IBOutlet weak var infoLbl: UILabel!
    var appDelegate = AppDelegate()    
    @IBOutlet weak var lblCongoTitle: UILabel!
    @IBOutlet weak var viewAndAcceptGreenPassButton: UIButton!
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        appDelegate.isInviteViewed = false
        appDelegate.isLaunchFirstTime = false
        
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
    @IBAction func btnKisanVideoPlay(_ sender: Any) {
        let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
            UIApplication.shared.openURL(youtubeUrl as URL)
        } else{
            let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
            UIApplication.shared.openURL(youtubeUrl as URL)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.navigationController?.isNavigationBarHidden=true
        ButtonAnimationHelper().animateButton(button: viewAndAcceptGreenPassButton)
//        let titleStr="FREE"
//        let attributedString = NSMutableAttributedString(string: "You have received a \(titleStr) Greenpass.", attributes: [
//            .font: UIFont.systemFont(ofSize: 23.0, weight: .regular),
//            .foregroundColor: UIColor.white,
//            .kern: 0.0
//            ])
//        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 23.0, weight: .bold), range: NSRange(location: 20, length: titleStr.count))
//        infoLbl.attributedText=attributedString
        // Do any additional setup after loading the view.
        
        if UIScreen.main.bounds.size.height <= 568 {
            infoLbl.font = UIFont.boldSystemFont(ofSize: 21)
            infoLbl.font = UIFont.systemFont(ofSize: 13)
        }
    }
    
    
    //MARK:IBAction Methods
    @IBAction func viewAndAcceptBtnAction(_ sender: UIButton) {
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "PassBaseVC")as!PassBaseVC
        vc.isFromViewAccept=true
        appDelegate.isInviteViewed = true
        appDelegate.isLaunchFirstTime = false
        UserDefaults.standard.set(true, forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)

        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
