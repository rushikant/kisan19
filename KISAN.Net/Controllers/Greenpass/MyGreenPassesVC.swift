//
//  MyGreenPassesVC.swift
//  KISAN.Net
//
//  Created by MacMini on 10/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import Alamofire

class MyGreenPassesVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var myGrepassTblView: UITableView!
    @IBOutlet weak var sendGreenpassBtnView: UIView!
    @IBOutlet weak var buyGreenpassBtnView: UIView!
    @IBOutlet weak var topLbl: UILabel!
    var profileImage: UIImage!
    private let picker = UIImagePickerController()
    var selectedIndex = Int()
    var comingFrom = String()
    @IBOutlet weak var lblissueConstantLastDate: UILabel!
    @IBOutlet weak var lblSendFreenGreenpass: UILabel!
    var myGreenpassArray=NSMutableArray()
    var show_issue_pass = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleStr="KISAN 2019 EXHIBITORS"
        var attributedString = NSMutableAttributedString()
        if UIScreen.main.bounds.size.height <= 568{
            attributedString = NSMutableAttributedString(string: "Find out \(titleStr). ", attributes: [
                .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                .foregroundColor: UIColor.white,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize:13.0, weight: .semibold), range: NSRange(location: 9, length: titleStr.count))
        }else{
            attributedString = NSMutableAttributedString(string: "Find out \(titleStr). ", attributes: [
                .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                .foregroundColor: UIColor.white,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16.0, weight: .semibold), range: NSRange(location: 9, length: titleStr.count))
        }
        topLbl.attributedText=attributedString
        // Do any additional setup after loading the view.
        
        self.myGrepassTblView.register(UINib(nibName: "InviteFriendsViewCell", bundle:nil), forCellReuseIdentifier: "InviteFriendsViewCell")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)

        let isKisanMitra = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.isMitra)
        if(isKisanMitra){
            
            if(isKisanMitra && show_issue_pass == 0){
                //show buy greenpass option
                sendGreenpassBtnView.isHidden=true
                buyGreenpassBtnView.isHidden=false
           }else{
                let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
                let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
                let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
                let remaining = (standard + extra) - sent
                if remaining > 0{
                    //show send greenpass option
                    sendGreenpassBtnView.isHidden=false
                    buyGreenpassBtnView.isHidden=true
                }else{
                    //show buy greenpass option
                    sendGreenpassBtnView.isHidden=true
                    buyGreenpassBtnView.isHidden=false
                }
            }
        }else{
            //show buy greenpass option
            sendGreenpassBtnView.isHidden=true
            buyGreenpassBtnView.isHidden=false
        }
        
        if(comingFrom.isEmpty){
         self.myGreenpassArray.removeAllObjects()
         myGrepassTblView.reloadData()
         showMyGreenpssListAPi()
        }else{
            comingFrom = StringConstants.EMPTY
        }
        
        let issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
        if !(issueInviteLastDate.isEmpty) {
            getDateInFormat(dateStr: issueInviteLastDate)
        }
    }
    
    
    func getDateInFormat(dateStr:String){
        
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: dateStr)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        lblissueConstantLastDate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
        
        lblSendFreenGreenpass.text = StringConstants.setInviteSummaryViewController.send + StringConstants.singleSpace + String(remaining) + StringConstants.singleSpace + StringConstants.setInviteSummaryViewController.FreeGreenPass
    }
    
    
    //MARK:IBAction Methods
    @IBAction func startConnectingAction(_ sender: UIButton) {
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "DiscoverChannelViewController")as!DiscoverChannelViewController
        vc.isComingFromGreepass=true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buyGreenpassAction(_ sender: UIButton) {
        let vc = App.dashStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        vc.commingFrom = StringConstants.flags.leftDrawerKisan2018
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sendGreenpassAction(_ sender: UIButton) {
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "InviteFriendBaseVC")as!InviteFriendBaseVC
        vc.isComingFromGreenpass=true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // API FOR GET PENDING INVITE LIST
    
    func showMyGreenpssListAPi(){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
        source = RequestName.loginExhibitorSourceName
        }

        let params = ["app_key":String(URLConstant.app_key),
                      "sessionId":String(sessionId),
                      "eventCode":String(URLConstant.eventCode),
                      "source":String(source)] as [String : Any]
        
        print(params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getMyGreenpasses
        _ = ShowMyGreenpassService().getMyGreenpassList(apiURL,postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            let model = userModel as! ShowMyGreenpassResponse
                                                            print("model",model)
                                                            
                                                            
                                                            let myGreenpassList = model.myGreenpassList
                                                            
                                                            print("myGreenpassList",myGreenpassList)
                                                            
                                                            self.myGreenpassArray.addObjects(from: myGreenpassList)
                                                            NotificationCenter.default.post(name: Notification.Name("updateMyGreenpassCount"), object: myGreenpassList.count)
                                                            self.myGrepassTblView.reloadData()
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            print("error",error)
            NotificationCenter.default.post(name: Notification.Name("updateMyGreenpassCount"), object: 0)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    
    func updateMyGreenPassAPI(){
       
        let dictDataValue = self.myGreenpassArray[self.selectedIndex] as! MyGreenpassListDetails
        let imgUrl = dictDataValue.fullimageurl
        let barcode = dictDataValue.barcode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)

        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        
        let params = UpdateGreenPassRequest.convertToDictionary(updateGreenPassRequest: UpdateGreenPassRequest.init(app_key: URLConstant.app_key, imageurl: imgUrl, eventCode: URLConstant.eventCode, sessionId : sessionId ,barcode: barcode, first_name: dictDataValue.first_name!, last_name: dictDataValue.last_name!, source: RequestName.loginSourceName))
        
        
        print(params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.updateMyGreenpasses
        _ = UpdateMygreenpassServices().UpdateMygreenpass(apiURL,postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            let model = userModel as! UpdateGreenpassResponse
                                                            print("model",model)
                                                            self.popupAlert(title: "", message: model.message, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                                                                },{action2 in
                                                                }, nil])

                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            print("error",error)
            NotificationCenter.default.post(name: Notification.Name("updateMyGreenpassCount"), object: 0)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func btnEditPhotoClick(sender:UIButton){
        comingFrom = "crop"
        let greenpass = myGreenpassArray[sender.tag] as!MyGreenpassListDetails
        print(greenpass.first_name as Any)
        selectedIndex = sender.tag
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.photoLibrary, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                self.openEditor(nil)
            }
        }
    }
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        /* let controller = CropViewController()
         controller.delegate = self
         controller.image = image
         let navController = UINavigationController(rootViewController: controller)
         present(navController, animated: true, completion: nil)*/
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        comingFrom = "crop"
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
        
    }
    
    func isHideEditgreenpass()->Bool{
        var currentdate = Date()
        var eventStartDateTime = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.eventStartDateTime)
    
        
        if (eventStartDateTime.isEmpty){
          eventStartDateTime = "2019-12-11 00:00:00"
        }

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateEvent: Date? = dateFormatterGet.date(from: eventStartDateTime)! as Date
        
        let datecurrent = dateFormatterGet.string(from: Date())
        currentdate = dateFormatterGet.date(from: datecurrent)! as Date
        
        // Compare them
        switch currentdate.compare(dateEvent!) {
        case .orderedAscending     :   print("currentdate is earlier than date eventStart Date")
                                       return false
            
        case .orderedDescending    :   print("currentdate is later than eventStart Date")
                                       return true

        case .orderedSame          :   print("currentdate dates are eventStart Date")
                                       return true
        }
    }
}

extension MyGreenPassesVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(myGreenpassArray.count>0){
        return myGreenpassArray.count + 1
        }else{
         return myGreenpassArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(myGreenpassArray.count == indexPath.row){
            
           let cell=tableView.dequeueReusableCell(withIdentifier: "InviteFriendsViewCell") as! InviteFriendsViewCell
            cell.viewOfbackground.layer.cornerRadius = 8
            cell.viewOfbackground.clipsToBounds = true
            
            cell.viewShareWithFriends.layer.cornerRadius = 8
            cell.viewShareWithFriends.clipsToBounds = true
            cell.selectionStyle = .none

            return cell
            
        }else{
        let data=myGreenpassArray[indexPath.row]as!MyGreenpassListDetails
        if data.passtype_name == StringConstants.PassType.Mitra{
            //type C
            let cell=tableView.dequeueReusableCell(withIdentifier: "MyGreenPassTVC2") as! MyGreenPassTVC2
            cell.nameLbl.text="\(data.first_name ?? "") \(data.last_name ?? "")"
            cell.addressLbl.text="\(data.district ?? ""), \(data.state ?? "")"
            cell.qrCodeLbl.text=data.barcode
            cell.typeCView.roundCorners(corners: [.bottomLeft], radius: 40.0)
            if data.fullimageurl != "" && data.fullimageurl != nil{
                cell.userImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
            }else{
                cell.userImg.image = UIImage(named:StringConstants.ImageNames.defaultuser)
            }
            cell.qrCodeImg.image=generateQRCode(from: data.barcode!, imgQRCode: cell.qrCodeImg)
            cell.selectionStyle = .none
            
            if(isHideEditgreenpass()){
              cell.btnEditPhoto.isHidden = true
              cell.imgUpdatePic.isHidden = true
            }
            
            cell.btnEditPhoto.tag = indexPath.row
            cell.btnEditPhoto.addTarget(self, action: #selector(btnEditPhotoClick(sender:)), for: .touchUpInside)
            
            if((data.badge_barcode?.isEmpty)!){
                cell.imgStamp.isHidden  = true
            }else{
                cell.imgStamp.isHidden  = false
            }

            return cell
        }else if data.entrytype_name == StringConstants.MultiEntry{
            //type A
            let cell=tableView.dequeueReusableCell(withIdentifier: "MyGreenPassTVC") as! MyGreenPassTVC
            cell.nameLbl.text="\(data.first_name ?? "") \(data.last_name ?? "")"
            cell.addressLbl.text="\(data.district ?? ""), \(data.state ?? "")"
            cell.qrCodeLbl.text=data.barcode
            cell.multientryImgView.isHidden=false
            cell.smallView.isHidden=true
            cell.bottonLineLbl.backgroundColor=UIColor(red: 160.0/255.0, green: 119.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            cell.bgView.backgroundColor=UIColor(red: 201.0/255.0, green: 162.0/255.0, blue: 9.0/255.0, alpha: 1.0)
            cell.smallView.backgroundColor=UIColor(red: 160.0/255.0, green: 119.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            if data.fullimageurl != "" && data.fullimageurl != nil{
                cell.userImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
            }else{
                cell.userImg.image = UIImage(named:StringConstants.ImageNames.defaultuser)
            }
            cell.qrCodeImg.image=generateQRCode(from: data.barcode!, imgQRCode: cell.qrCodeImg)
            cell.selectionStyle = .none
            
            if(isHideEditgreenpass()){
                cell.btnEditPhoto.isHidden = true
                cell.imgUpdatepic.isHidden = true
            }
            
            cell.btnEditPhoto.tag = indexPath.row
            cell.btnEditPhoto.addTarget(self, action: #selector(btnEditPhotoClick(sender:)), for: .touchUpInside)
           
            
            if((data.badge_barcode?.isEmpty)!){
                cell.imgStamp.isHidden  = true
            }else{
                cell.imgStamp.isHidden  = false
            }

            return cell
        }else{
            if data.channel_name != "" && data.channel_name != nil{
                //type D
                let cell=tableView.dequeueReusableCell(withIdentifier: "MyGreenPassTVC3") as! MyGreenPassTVC3
                if(data.passtype_name == StringConstants.PassType.UnlimitedExhibitorInvite){
                    cell.kisanMitralImg.isHidden = true
                }else{
                    cell.kisanMitralImg.isHidden = false
                }
                cell.nameLbl.text="\(data.first_name ?? "") \(data.last_name ?? "")"
                cell.addressLbl.text="\(data.district ?? ""), \(data.state ?? "")"
                cell.qrCodeLbl.text=data.barcode
                if data.channel_max_color != "" && data.channel_max_color != nil{
                    cell.bgView.backgroundColor = hexStringToUIColor(hex: data.channel_max_color!)
                }
                
                cell.channelNameLbl.text=data.channel_name
                if data.channel_big_thumb != "" && data.channel_big_thumb != nil{
                    cell.channelImg.sd_setImage(with: URL.init(string: data.channel_big_thumb!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
                }else if data.fullimageurl != "" && data.fullimageurl != nil{
                    cell.channelImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
                }else{
                    cell.channelImg.image = UIImage(named:StringConstants.ImageNames.defaultuser)
                }
                if data.fullimageurl != "" && data.fullimageurl != nil{
                    cell.userImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
                }else{
                    cell.userImg.image = UIImage(named:StringConstants.ImageNames.defaultuser)
                }
                cell.qrCodetImg.image=generateQRCode(from: data.barcode!, imgQRCode: cell.qrCodetImg)
                cell.selectionStyle = .none
                
                if(isHideEditgreenpass()){
                    cell.btnEditPhoto.isHidden = true
                    cell.imgUpdatePic.isHidden = true
                }

                cell.btnEditPhoto.tag = indexPath.row
                cell.btnEditPhoto.addTarget(self, action: #selector(btnEditPhotoClick(sender:)), for: .touchUpInside)

                if((data.badge_barcode?.isEmpty)!){
                    cell.imgStamp.isHidden  = true
                }else{
                    cell.imgStamp.isHidden  = false
                }

                return cell
            }else{
                //type B
                let cell=tableView.dequeueReusableCell(withIdentifier: "MyGreenPassTVC") as! MyGreenPassTVC
                cell.nameLbl.text="\(data.first_name ?? "") \(data.last_name ?? "")"
                cell.addressLbl.text="\(data.district ?? ""), \(data.state ?? "")"
                cell.qrCodeLbl.text=data.barcode
                cell.multientryImgView.isHidden=true
                cell.smallView.isHidden=false
                cell.bottonLineLbl.backgroundColor=UIColor(red: 36.0/255.0, green: 119.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                cell.bgView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                cell.smallView.backgroundColor=UIColor(red: 36.0/255.0, green: 119.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                if data.fullimageurl != "" && data.fullimageurl != nil{
                    cell.userImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.defaultuser))
                }else{
                    cell.userImg.image = UIImage(named:StringConstants.ImageNames.defaultuser)
                }
                cell.qrCodeImg.image=generateQRCode(from: data.barcode!, imgQRCode: cell.qrCodeImg)
                cell.selectionStyle = .none
                
                if(isHideEditgreenpass()){
                    cell.btnEditPhoto.isHidden = true
                    cell.imgUpdatepic.isHidden = true
                }

                if((data.badge_barcode?.isEmpty)!){
                    cell.imgStamp.isHidden  = true
                }else{
                    cell.imgStamp.isHidden  = false
                }

                cell.btnEditPhoto.tag = indexPath.row
                cell.btnEditPhoto.addTarget(self, action: #selector(btnEditPhotoClick(sender:)), for: .touchUpInside)
                return cell
            }
            
        }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(myGreenpassArray.count == indexPath.row){
           /* let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "ContactListVC")as!ContactListVC
            vc.comingFrom = StringConstants.flags.shareYourFriendsVc
            self.navigationController?.pushViewController(vc, animated: true)*/
            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "InviteFriendBaseVC")as!InviteFriendBaseVC
            vc.isComingFromGreenpass=true
            vc.comingFrom = StringConstants.flags.shareYourFriendsVc
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            let data=myGreenpassArray[indexPath.row]as!MyGreenpassListDetails

            if((data.badge_barcode?.isEmpty)!){
                print(indexPath.row)
                let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "MyGreenpassDetailsVC")as!MyGreenpassDetailsVC
                vc.passDetails = data
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
            
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(myGreenpassArray.count == indexPath.row){
            return 234.0
        }else{
        let data=myGreenpassArray[indexPath.row]as!MyGreenpassListDetails
        if data.passtype_name == StringConstants.PassType.Mitra{
            //type C
            return 261.0
        }else if data.entrytype_name == StringConstants.MultiEntry{
            //type A
            return 229.0
        }else{
            if data.channel_name != "" && data.channel_name != nil{
                //type D
                return 297.0
            }else{
                return 229.0
            }
        }
    }
    }
    
    
    
    func uploadContactImg(croppedImage: UIImage?){
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
            let appKey =  URLConstant.app_key
            _ = StringConstants.MediaType.image
            let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadVisitorProfileToImage //URLConstant.uploadFileVisitor
            print("ApiUrl",apiURL)
            let params = ["app_key":appKey,
                          "sessionId":sessionId]
            let fileName = StringConstants.file
            let imageData:Data = UIImageJPEGRepresentation(croppedImage!, 0.7)! as Data

           Alamofire.upload(multipartFormData: { multipartFormData in
            for (key,value) in params {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            multipartFormData.append(imageData, withName: fileName, fileName: "invite_contact_img.png", mimeType: "image/jpeg")
           },to: apiURL,method: .post,encodingCompletion: { encodingResult in
                                                        switch encodingResult{
                
                                                          case .success(let upload, _, _):
                                                            upload.responseJSON { response in
                                                            print(response)
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            guard let responseJSON = response.result.value as? [String:Any],
                                                            let _ = responseJSON["message"] as? String,
                                                            let status = responseJSON["success"] as? Bool else {
                                                            return
                                                                }
                                                            if(status == false){
                                                                 MBProgressHUD.hide(for: self.view, animated: true);
                                                            }else{
                                                                  let data=responseJSON["imageDetails"]as!NSDictionary
                                                                  let dictDataValue = self.myGreenpassArray[self.selectedIndex] as! MyGreenpassListDetails
                                                                  let imgFullUrl = String(describing: data["imgFullUrl"]!)
                                                                  let imgUrl = String(describing: data["imgUrl"]!)
                        
                                                                  dictDataValue.imageurl = imgUrl
                                                                  dictDataValue.fullimageurl = imgFullUrl
                                                                  self.myGreenpassArray.removeObject(at: self.selectedIndex)
                                                                  self.myGreenpassArray.insert(dictDataValue, at: self.selectedIndex)
                                                                  let indexPath = IndexPath(item: self.selectedIndex, section: 0)
                                                                  self.myGrepassTblView.reloadRows(at: [indexPath], with:.automatic)
                                                                  self.updateMyGreenPassAPI()
                                                            }
                                                                
                                                           }
                
        case .failure( _ ):
                MBProgressHUD.hide(for: self.view, animated: true);
                self.popupAlert(title: "", message: "Something went wrong", actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }
        })
    }
    
}


extension UIViewController{
    func generateQRCode(from string: String, imgQRCode:UIImageView) -> UIImage? {
        let data = string.data(using: String.Encoding.utf8)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
            
            filter.setValue(data, forKey: "inputMessage")
            
            filter.setValue("H", forKey: "inputCorrectionLevel")
            colorFilter.setValue(filter.outputImage, forKey: "inputImage")
            colorFilter.setValue(CIColor.white, forKey: "inputColor1") // Background white
            colorFilter.setValue(CIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0), forKey: "inputColor0") // Foreground or the barcode RED
            guard let qrCodeImage = colorFilter.outputImage
                else {
                    return nil
            }
            let scaleX = imgQRCode.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = imgQRCode.frame.size.height / qrCodeImage.extent.size.height
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            
            
            if let output = colorFilter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    //status bar
    func showStatusBar(_ view: UIView) {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 45.0/255.0, green: 144.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        //        let statusBarColor = UIColor.white
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension MyGreenPassesVC: UIImageCropperProtocol {
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        
        self.uploadContactImg(croppedImage: croppedImage)
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
