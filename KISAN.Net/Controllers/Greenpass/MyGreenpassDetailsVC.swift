//
//  MyGreenpassDetailsVC.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 14/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class MyGreenpassDetailsVC: UIViewController {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var nameLbl2: UILabel!
    @IBOutlet weak var channelNameLbl: UILabel!
    @IBOutlet weak var qrCodeLbl: UILabel!
    @IBOutlet weak var qrCodeImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userImg2: UIImageView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressLbl2: UILabel!
    @IBOutlet weak var channelImg: UIImageView!
    @IBOutlet weak var kissanMitraImg: UIImageView!
    @IBOutlet weak var multientryImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var singleEntryView: UIView!
    @IBOutlet weak var subBgView: UIView!
    @IBOutlet weak var typeDView: UIView!
    @IBOutlet weak var bottonLineLbl: UILabel!
    @IBOutlet weak var bottonBtnView: UIView!
    
    @IBOutlet weak var imgKisanMitra: UIImageView!
    var passDetails:MyGreenpassListDetails?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        bottonBtnView.cornerRadiusWithShadow(shadowRadius: 5.0, cornerRadius: 0.0)
        self.showStatusBar(self.view)
        kissanMitraImg.isHidden=true
        multientryImg.isHidden=true
        smallView.isHidden=true
        typeDView.isHidden=true
        singleEntryView.isHidden=false
        if passDetails?.passtype_name == StringConstants.PassType.Mitra{
            //type C
            nameLbl.text="\(passDetails?.first_name ?? "") \(passDetails?.last_name ?? "")"
            addressLbl.text="\(passDetails?.district ?? ""), \(passDetails?.state ?? "")"
            qrCodeLbl.text=passDetails?.barcode
            smallView.isHidden=false
            kissanMitraImg.isHidden=false
            bottonLineLbl.backgroundColor=UIColor(red: 18.0/255.0, green: 71.0/255.0, blue: 21.0/255.0, alpha: 1.0)
            bgView.backgroundColor=UIColor(red: 33.0/255.0, green: 111.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            subBgView.backgroundColor=UIColor(red: 33.0/255.0, green: 111.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            bottonBtnView.backgroundColor=UIColor(red: 33.0/255.0, green: 111.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            singleEntryView.backgroundColor=UIColor(red: 18.0/255.0, green: 71.0/255.0, blue: 21.0/255.0, alpha: 1.0)
            if passDetails?.fullimageurl != "" && passDetails?.fullimageurl != nil{
                userImg.sd_setImage(with: URL.init(string: (passDetails?.fullimageurl!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
            }else{
                userImg.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
            }
            qrCodeImg.image=generateQRCode(from: (passDetails?.barcode!)!, imgQRCode: qrCodeImg)
        }else if passDetails?.entrytype_name == StringConstants.MultiEntry{
            //type A
            nameLbl.text="\(passDetails?.first_name ?? "") \(passDetails?.last_name ?? "")"
            addressLbl.text="\(passDetails?.district ?? ""), \(passDetails?.state ?? "")"
            qrCodeLbl.text=passDetails?.barcode
            multientryImg.isHidden=false
            singleEntryView.isHidden=true
            smallView.isHidden=false
            bottonLineLbl.backgroundColor=UIColor(red: 160.0/255.0, green: 119.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            bgView.backgroundColor=UIColor(red: 201.0/255.0, green: 162.0/255.0, blue: 9.0/255.0, alpha: 1.0)
            subBgView.backgroundColor=UIColor(red: 201.0/255.0, green: 162.0/255.0, blue: 9.0/255.0, alpha: 1.0)
            bottonBtnView.backgroundColor=UIColor(red: 201.0/255.0, green: 162.0/255.0, blue: 9.0/255.0, alpha: 1.0)
            singleEntryView.backgroundColor=UIColor(red: 160.0/255.0, green: 119.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            if passDetails?.fullimageurl != "" && passDetails?.fullimageurl != nil{
                userImg.sd_setImage(with: URL.init(string: (passDetails?.fullimageurl!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
            }else{
                userImg.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
            }
            qrCodeImg.image=generateQRCode(from: (passDetails?.barcode!)!, imgQRCode: qrCodeImg)
        }else{
            if passDetails?.channel_name != "" && passDetails?.channel_name != nil{
                //type D
               
                if(passDetails?.passtype_name == StringConstants.PassType.UnlimitedExhibitorInvite){
                    kissanMitraImg.isHidden = true
                    imgKisanMitra.isHidden = true
                }else{
                    kissanMitraImg.isHidden = false
                    imgKisanMitra.isHidden = false
                }
                
                nameLbl2.text="\(passDetails?.first_name ?? "") \(passDetails?.last_name ?? "")"
                addressLbl2.text="\(passDetails?.district ?? ""), \(passDetails?.state ?? "")"
                qrCodeLbl.text=passDetails?.barcode
                if passDetails?.channel_max_color != "" && passDetails?.channel_max_color != nil{
                    typeDView.backgroundColor = hexStringToUIColor(hex: (passDetails?.channel_max_color!)!)
                    bottonBtnView.backgroundColor = hexStringToUIColor(hex: (passDetails?.channel_max_color!)!)
                    
                }
                channelNameLbl.text=passDetails?.channel_name
                typeDView.isHidden=false
                bottonLineLbl.backgroundColor=UIColor(red: 37.0/255.0, green: 121.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                bgView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                subBgView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                singleEntryView.backgroundColor=UIColor(red: 37.0/255.0, green: 121.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                if passDetails?.channel_big_thumb != "" && passDetails?.channel_big_thumb != nil{
                    channelImg.sd_setImage(with: URL.init(string: (passDetails?.channel_big_thumb!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
                }else if passDetails?.fullimageurl != "" && passDetails?.fullimageurl != nil{
                    channelImg.sd_setImage(with: URL.init(string: (passDetails?.fullimageurl!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
                }else{
                    channelImg.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
                }
                if passDetails?.fullimageurl != "" && passDetails?.fullimageurl != nil{
                    userImg2.sd_setImage(with: URL.init(string: (passDetails?.fullimageurl!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
                }else{
                    userImg2.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
                }
                qrCodeImg.image=generateQRCode(from: (passDetails?.barcode!)!, imgQRCode: qrCodeImg)
            }else{
                //type B
                nameLbl.text="\(passDetails?.first_name ?? "") \(passDetails?.last_name ?? "")"
                addressLbl.text="\(passDetails?.district ?? ""), \(passDetails?.state ?? "")"
                qrCodeLbl.text=passDetails?.barcode
                multientryImg.isHidden=true
                
                smallView.isHidden=false
                bottonLineLbl.backgroundColor=UIColor(red: 36.0/255.0, green: 119.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                bgView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                subBgView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                bottonBtnView.backgroundColor=UIColor(red: 48.0/255.0, green: 151.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                singleEntryView.backgroundColor=UIColor(red: 36.0/255.0, green: 119.0/255.0, blue: 60.0/255.0, alpha: 1.0)
                if passDetails?.fullimageurl != "" && passDetails?.fullimageurl != nil{
                    userImg.sd_setImage(with: URL.init(string: (passDetails?.fullimageurl!)!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
                }else{
                    userImg.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
                }
                qrCodeImg.image=generateQRCode(from: (passDetails?.barcode!)!, imgQRCode: qrCodeImg)
            }
            
        }
        // Do any additional setup after loading the view.
    }
    //MARK:IBAction Methods
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        
        if let image = takeScreenshot(false) {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            self.shareMyGreenPassAPI()
            self.present(vc, animated: true)
        }
    }
    @IBAction func downloadAction(_ sender: UIButton) {
        _=takeScreenshot()
        
    }
    /// - Returns: (Optional)image captured as a screenshot
    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.subBgView.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        
        self.subBgView.drawHierarchy(in: self.subBgView.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        if let image = screenshotImage, shouldSave {
            //            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            
        }
        return screenshotImage
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error, Please allow app to save photo from setting!", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            self.popupAlert(title: "", message: "GreenPass Download Successfully!", actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        }
    }

    func shareMyGreenPassAPI(){
        let dictDataValue = self.passDetails as! MyGreenpassListDetails
        let barcode = dictDataValue.barcode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let toEmails = "rushikant0204@gmail.com"
        let activity = "sharepassemail"

        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        
        let params = ShareGreenPassTrackingRequest.convertToDictionary(
            shareGreenPassTrackingRequest: ShareGreenPassTrackingRequest.init(app_key: URLConstant.app_key,
                                                                              toEmails: toEmails,
                                                                              eventCode: URLConstant.eventCode,
                                                                              sessionId : sessionId ,
                                                                              barcode: barcode,
                                                                              activity: activity,
                                                                              source: RequestName.loginSourceName))
        
        
        print(params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.shareGreenPass
        _ = ShareGreenPassTrackingServices().shareGreenpass(apiURL,postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            let model = userModel as! ShareGreenPasstrackingResponse
                                                            print("model",model)
                                                            print("shareGreenPassTrackingSuccessful")
                                                            
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            print("error",error)
            NotificationCenter.default.post(name: Notification.Name("shareGreenPassTracking"), object: 0)
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
