//
//  PassBaseVC.swift
//  KISAN.Net
//
//  Created by MacMini on 10/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class PassBaseVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet private weak var btnTab1: UIButton!
    @IBOutlet private weak var btnTab2: UIButton!
    
    //    @IBOutlet private weak var viewLine: UIView!
    @IBOutlet private weak var constantViewLeft: NSLayoutConstraint!
    @IBOutlet weak var lblCountOfMyGreenPass: UILabel!
    @IBOutlet weak var lblCountOfPendingInvite: UILabel!
    var tab1VC:MyGreenPassesVC! = nil
    var tab2VC:PendingInvitesVC! = nil
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    var isFromViewAccept=false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showStatusBar(self.view)
        lblCountOfPendingInvite.isHidden=true
        lblCountOfMyGreenPass.isHidden=true
        self.navigationController?.isNavigationBarHidden=true
        if isFromViewAccept{
            currentPage=1
        }else{
            currentPage = 0
        }
        
        createPageViewController()
        //set inital count on tab buttons
        if ((UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount)) != nil) && UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount) as! Int > 0{
            lblCountOfMyGreenPass.isHidden=false
            lblCountOfMyGreenPass.text=String(UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount) as! Int)
        }else{
            lblCountOfMyGreenPass.isHidden=true
        }
        if ((UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)) != nil) && UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount) as! Int > 0{
            lblCountOfPendingInvite.isHidden=false
            lblCountOfPendingInvite.text=String(UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount) as! Int)
        }else{
            lblCountOfPendingInvite.isHidden=true
        }
        
        if UIScreen.main.bounds.size.height <= 568{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
            lblCountOfPendingInvite.font=UIFont.systemFont(ofSize: 13.0)
            lblCountOfMyGreenPass.font=UIFont.systemFont(ofSize: 13.0)
        }else{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 19.0)
            lblCountOfPendingInvite.font=UIFont.systemFont(ofSize: 14.0)
            lblCountOfMyGreenPass.font=UIFont.systemFont(ofSize: 14.0)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToMyGreenPass(notification:)), name: Notification.Name("moveToMyGreenPass"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatePendingInviteCount(notification:)), name: Notification.Name("updatePendingInviteCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMyGreenpassCount(notification:)), name: Notification.Name("updateMyGreenpassCount"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    @objc func moveToMyGreenPass(notification: NSNotification){
        let count=notification.object as!Int
        if count>0{
            lblCountOfPendingInvite.isHidden=false
            lblCountOfPendingInvite.text=String(count)
        }else{
            lblCountOfPendingInvite.isHidden=true
        }
        pageController.setViewControllers([arrVC[btnTab1.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        resetTabBarForTag(tag: btnTab1.tag-1)
    }
    @objc func updatePendingInviteCount(notification: NSNotification){
        let count=notification.object as!Int
        if count>0{
            lblCountOfPendingInvite.isHidden=false
            lblCountOfPendingInvite.text=String(count)
        }else{
            lblCountOfPendingInvite.isHidden=true
        }
        
    }
    @objc func updateMyGreenpassCount(notification: NSNotification){
        let count=notification.object as!Int
        if count>0{
            lblCountOfMyGreenPass.isHidden=false
            lblCountOfMyGreenPass.text=String(count)
        }else{
            lblCountOfMyGreenPass.isHidden=true
        }
        
    }
    
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.white, for: .normal)
        constantViewLeft.constant = btn.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }
    
    @IBAction private func btnOptionClicked(btn: UIButton) {
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    
    //MARK: - CreatePagination
    private func createPageViewController() {
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        if UIDevice.current.hasNotch {
            //... consider notch
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.pageController.view.frame = CGRect(x: 0, y:125, width: self.view.frame.size.width, height: self.view.frame.size.height-125)
            }
        } else {
            //... don't have to consider notch
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.pageController.view.frame = CGRect(x: 0, y:100, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
            }
        }
        
        //        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        tab1VC = (App.dashStoryBoard.instantiateViewController(withIdentifier: "MyGreenPassesVC") as! MyGreenPassesVC)
        tab2VC = (App.dashStoryBoard.instantiateViewController(withIdentifier: "PendingInvitesVC") as! PendingInvitesVC)
        arrVC = [tab1VC, tab2VC]
        if isFromViewAccept{
            pageController.setViewControllers([tab2VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
            resetTabBarForTag(tag: btnTab2.tag-1)
        }else{
            pageController.setViewControllers([tab1VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.firstIndex(of: viewCOntroller)!
        }
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index - 1
        }
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index + 1
        }
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.firstIndex(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    private func resetTabBarForTag(tag: Int) {
        var sender: UIButton!
        
        if(tag == 0) {
            sender = btnTab1
        }
        else if(tag == 1) {
            sender = btnTab2
        }
        currentPage = tag
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        selectedButton(btn: sender)
    }
    
    
    //MARK: - UIScrollView Delegate Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        //        let xCoor: CGFloat = CGFloat(viewLine.frame.size.width) * CGFloat(currentPage)
        //        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        //        constantViewLeft.constant = xPosition
    }
    //MARK:IBAction Methods
    @IBAction func backAction(_ sender: UIButton) {
        //        self.navigationController?.isNavigationBarHidden=false
        //        self.navigationController?.popViewController(animated: true)
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
