//
//  PendingInvitesVC.swift
//  KISAN.Net
//
//  Created by MacMini on 10/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class PendingInvitesVC: UIViewController {
    @IBOutlet weak var lblSendFreeGreenPass: UILabel!
    @IBOutlet weak var lblIssueGreenpassDate: UILabel!
    @IBOutlet weak var pendingInviteTblView: UITableView!
    @IBOutlet weak var sendGreenpassBtnView: UIView!
    @IBOutlet weak var buyGreenpassBtnView: UIView!
    var pendingInvitesArray=NSMutableArray()
    var issueInviteLastDate =  String()
    var greenpassAcceptLast_date =  String()
    var show_issue_pass = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
     
        greenpassAcceptLast_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
        NotificationCenter.default.addObserver(self,selector:#selector(applicationWillEnterForeground(_:)),name:NSNotification.Name.UIApplicationWillEnterForeground,object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.pendingInvitesArray.removeAllObjects()
        pendingInviteTblView.reloadData()
        super.viewWillAppear(true)
        let isKisanMitra = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.isMitra)
        show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)

        if(isKisanMitra){
            if(isKisanMitra && show_issue_pass == 0){
                //show buy greenpass option
                sendGreenpassBtnView.isHidden=true
                buyGreenpassBtnView.isHidden=false
            }else{
                let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
                let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
                let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
                let remaining = (standard + extra) - sent
                if remaining > 0{
                    //show send greenpass option
                    sendGreenpassBtnView.isHidden=false
                    buyGreenpassBtnView.isHidden=true
                }else{
                    //show buy greenpass option
                    sendGreenpassBtnView.isHidden=true
                    buyGreenpassBtnView.isHidden=false
                }
                
                issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                
                if !(issueInviteLastDate.isEmpty){
                    getDateInFormat(dateStr: issueInviteLastDate)
                }
            }
            
        }else{
            //show buy greenpass option
            sendGreenpassBtnView.isHidden=true
            buyGreenpassBtnView.isHidden=false
        }
        showPendingInviteListAPi()
        
    }
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        self.pendingInviteTblView.reloadData()
    }
    
    func setAcceptGreenPassDate(strDate:String) -> String {
        var formattedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: strDate)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
       formattedDate =  dateFormatterPrint.string(from: date! as Date)
        
        return formattedDate
    }
    
    
    
    func getDateInFormat(dateStr:String){
      
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: issueInviteLastDate)! as Date

        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))

        lblIssueGreenpassDate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
        
        lblSendFreeGreenPass.text = StringConstants.setInviteSummaryViewController.send + StringConstants.singleSpace + String(remaining) + StringConstants.singleSpace + StringConstants.setInviteSummaryViewController.FreeGreenPass
    }
    
    // API FOR GET PENDING INVITE LIST
    
    func showPendingInviteListAPi(){
       // self.pendingInvitesArray.removeAllObjects()
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
            source = RequestName.loginExhibitorSourceName
        }
        let params = ["app_key":String(URLConstant.app_key),
                      "sessionId":String(sessionId),
                      "eventCode":String(URLConstant.eventCode),
                      "source":String(source)] as [String : Any]
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.pendingInvitationList
        _ = ShowPendingInvitesService().getPendingInvitetList(apiURL,postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                                let model = userModel as! ShowPendingInvitesResponse
                                                                print("model",model)
                                                                
                                                                
                                                                let inviteInfo = model.invitationsInfo[0]
                                                                let invitationList = inviteInfo.invitation_list
                                                                print("invitationList",invitationList)
                                                                
                                                                self.pendingInvitesArray.addObjects(from: invitationList)
                                                                NotificationCenter.default.post(name: Notification.Name("updatePendingInviteCount"), object: inviteInfo.total_count)
                                                                self.pendingInviteTblView.reloadData()
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            print("error",error)
            NotificationCenter.default.post(name: Notification.Name("updatePendingInviteCount"), object: 0)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    //MARK:IBAction Methods
    @IBAction func buyGreenpassAction(_ sender: UIButton) {
        let vc = App.dashStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        vc.commingFrom = StringConstants.flags.leftDrawerKisan2018
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sendGreenpassAction(_ sender: UIButton) {
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "InviteFriendBaseVC")as!InviteFriendBaseVC
        vc.isComingFromGreenpass=true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PendingInvitesVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendingInvitesArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "PendingInvitesTVC") as! PendingInvitesTVC
        let data=pendingInvitesArray[indexPath.row]as!GuestListDetails
        cell.lblAcceptDate.layer.removeAllAnimations()
        cell.lblAcceptDate.alpha = 1
        cell.invitedNameLbl.text = data.channel_name
        if UIScreen.main.bounds.size.height <= 568{
            cell.infoLbl1.font = UIFont.systemFont(ofSize: 12.0)
            cell.infoLbl2.font = UIFont.boldSystemFont(ofSize: 16.0)
            cell.invitedNameLbl.font = UIFont.boldSystemFont(ofSize: 16.0)
            
            
        }else{
            cell.infoLbl1.font = UIFont.systemFont(ofSize: 15.0)
            cell.infoLbl2.font = UIFont.boldSystemFont(ofSize: 18.0)
            cell.invitedNameLbl.font = UIFont.boldSystemFont(ofSize: 18.0)
        }
        if data.channel_max_color != "" && data.channel_max_color != nil{
            cell.bgView.backgroundColor = hexStringToUIColor(hex: data.channel_max_color!)
        }
        if data.imageurl != "" && data.imageurl != nil{
            cell.logoImg.sd_setImage(with: URL.init(string: data.imageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
        }else{
            cell.logoImg.image = UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50)
        }
        if data.channel_key != "" && data.channel_key != nil{
            cell.bottomConstraintConstantOfCenterView.constant=50.0
            cell.heightConstraintConstantOfBottomView.constant=380.0
            cell.bottomView.isHidden=false
            
        }else{
            cell.bottomView.isHidden=true
            cell.bottomConstraintConstantOfCenterView.constant=10.0
            cell.heightConstraintConstantOfBottomView.constant=340.0
        }
        cell.bottomLbl.text = "By Accepting this invie, you follow it's channel and agree to Terms and Conditions."
        cell.bottomLbl.underlineMyText(range: "Terms and Conditions")
        cell.acceptBtn.tag=indexPath.row
        cell.acceptBtn.addTarget(self, action: #selector(PendingInvitesVC.acceptBtnAction), for: .touchUpInside)
        cell.termConditionBtn.tag=indexPath.row
        cell.termConditionBtn.addTarget(self, action: #selector(PendingInvitesVC.termConditionBtnBtnAction), for: .touchUpInside)
        cell.selectionStyle = .none
        
        if !( data.last_accept_datetime!.isEmpty){
            cell.lblAcceptDate.text  = "Before " + setAcceptGreenPassDate(strDate: data.last_accept_datetime!)
        }else{
            cell.lblAcceptDate.isHidden = true
        }
        ButtonAnimationHelper().blinkView(view:  cell.lblAcceptDate)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data=pendingInvitesArray[indexPath.row]as!GuestListDetails
        if data.channel_key != "" && data.channel_key != nil{
            return 390.0
        }else{
            return 351.0
        }
    }
    @objc func termConditionBtnBtnAction(sender: UIButton!) {
        
    }
    
    @objc func acceptBtnAction(sender: UIButton!) {
        
        let value = sender.tag;
        let data=pendingInvitesArray[value]as!GuestListDetails
        if data.invitation_code != "" && data.invitation_code != nil{
            self.acceptInvitationApi(data: data)
        }else{
            print("invitation_code is empty")
        }
        
    }
    // API FOR ACCEPT INVITATION
    
    func acceptInvitationApi(data:GuestListDetails){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        
        var  source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
            source = RequestName.loginExhibitorSourceName
        }
        let params = ["app_key":String(URLConstant.app_key),
                      "sessionId":String(sessionId),
                      "eventCode":String(URLConstant.eventCode),
                      "source":String(source),
                      "invitationCode":String(data.invitation_code!)] as [String : Any]
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.acceptInvitation
        _ = AcceptInvitationServices().acceptInvitation(apiURL,postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            let model = userModel as! AcceptInvitationResponse
                                                            print("model",model)
                                                            if self.pendingInvitesArray.count > 1{
                                                                NotificationCenter.default.post(name: Notification.Name("moveToMyGreenPass"), object: self.pendingInvitesArray.count-1)
                                                            }else{
                                                                NotificationCenter.default.post(name: Notification.Name("moveToMyGreenPass"), object: 0)
                                                            }
                                                            
                                                            if data.channel_key != "" && data.channel_key != nil{
                                                                DispatchQueue.global(qos: .background).async {
                                                                    self.getProfileData(channelKey: data.channel_key!)
                                                                }
                                                            }
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }
    
    func getProfileData(channelKey:String){
        let params = ["":""]
        let currentTime = DateTimeHelper.getCurrentMillis()
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.getChannelProfile + channelKey + URLConstant.getChannelProfileTimeStamp + String(currentTime)
        
        print("apiURL",apiURL)
        _ = ChannelProfileService().getChannelProfile(apiURL,
                                                      postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let communityDetails = userModel as! CommunityDetails
                                                        //communityDetails.communityJabberId
                                                        DispatchQueue.global(qos: .background).async {
                                                            self.followChannelApi(communityKey: channelKey, communityJabberId: communityDetails.communityJabberId!,communityDetails:communityDetails)
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))
                ], actions:[{action1 in
                    },{action2 in
                    }, nil])
        })
        
    }
    
    func followChannelApi(communityKey:String,communityJabberId:String,communityDetails:CommunityDetails){
        updateIsMemberInLocalDB(communityKey: communityKey, CommunityDetails: communityDetails)
        
        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
                self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])}
        })
        
    }
    
    
    func updateIsMemberInLocalDB(communityKey:String,CommunityDetails:CommunityDetails){
        
        let  dict1  = ConvertToDictOfChannelProfileObj.convertToDictOfChannelProfileObj(myChatDetails: [CommunityDetails])
        let  myChatListRespo = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict1)
        var myChats = [MyChatsTable]()
        myChats.append(myChatListRespo)
        //---------------- Update isMember  value as true for follow channel & also at database----------//
        myChats.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
        myChats.filter({$0.communityKey == communityKey}).first?.messageText = NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
        
        var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: myChats)
        CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)
        let currentTime = DateTimeHelper.getCurrentMillis()
        dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
        dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
        dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
        
        let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
        if checkCommunityKeyWithisNormalMychatFlag.count == 0{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
            dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
            MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
        }else{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
        }
        
    }
    
}
extension UILabel {
    func underlineMyText(range:String) {
        if let textString = self.text {
            
            let str = NSString(string: textString)
            let firstRange = str.range(of: range)
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: firstRange)
            attributedText = attributedString
        }
    }
}


