//
//  HomeViewController.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 13/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//string

import UIKit
import AccountKit
import XMPPFramework
import  Firebase
import FirebaseMessaging
import MBProgressHUD

class HomeViewController: UIViewController {
    
    var strLanguageType : String!
    //var localization = LocalizationModel()
    @IBOutlet weak var viewPager: ViewPager!
    var arrOfTitleText = [String]()
    var arrOfDescriptionText = [String]()
    var arrOfImages = [String]()
    var state = String()
    var imageUrlValue = String()
    @IBOutlet weak var topOfDescLabel: NSLayoutConstraint!
    @IBOutlet weak var topOfViewPagerView: NSLayoutConstraint!
    @IBOutlet weak var viewOfBackbtnPhoneAuth: UIView!
    @IBOutlet weak var btnPhoneAuth: UIButton!
    var accountKit: AKFAccountKit!
    var appDelegate = AppDelegate()
    var fbAccessToken = String()
    public var interests = [String]()
    @IBOutlet var btnexhibitorLogin: UIButton!
    var AuthToken = String()
    var strCommingFrom = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
//            if retriveState == StringConstants.NSUserDefauluterValues.loginSuccessfully{
//                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
//                let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
//                self.navigationController?.pushViewController(DashboardVC, animated: false)
//            }
//        }
        
        if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
            if strCommingFrom == StringConstants.flags.leftDrawerLanguage{
                strCommingFrom = StringConstants.EMPTY
            }else if  strCommingFrom == StringConstants.flags.dashboardViewController{
                strCommingFrom = StringConstants.EMPTY
            }else if retriveState == StringConstants.NSUserDefauluterValues.loginSuccessfully{
                /* let comingFrom = UserDefaults.standard.string(forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                 if comingFrom == StringConstants.pushNotificationConstants.appUpdate{
                 let urlstring = UserDefaults.standard.string(forKey: StringConstants.pushNotificationConstants.appUpdateLink)
                 UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                 if let url = URL(string: urlstring!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!), UIApplication.shared.canOpenURL(url) {
                 UIApplication.shared.openURL(url)
                 }
                 }else{*/
                strCommingFrom = StringConstants.EMPTY
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                self.navigationController?.pushViewController(DashboardVC, animated: false)
                //}
            }
        }

        initialDesign()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        // initialize Account Kit
        self.navigationController?.isNavigationBarHidden = true
        if UIScreen.main.bounds.size.width <= 320{
            topOfViewPagerView.constant = 20
            //topOfDescLabel.constant = 27
        }
    }
    
    func initialDesign() {
        
        //English - en, Marathi - mr,  Hindi - hi
        //let labelText: String = "HELLO_WORLD".localized(lang:strLanguageType)
        //print("labelText ==",labelText)
        print("HELLO_WORLD",NSLocalizedString("HELLO_WORLD", comment: ""))
        
       // self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        // Pager Design
        viewPager.dataSource = self;
       // viewPager.animationNext()
        viewOfBackbtnPhoneAuth.layer.cornerRadius = 5.0

        arrOfImages.append(StringConstants.ImageNames.homescreen_pageritem_new_one)
        arrOfImages.append(StringConstants.ImageNames.homescreen_pageritem_new_two)
        arrOfImages.append(StringConstants.ImageNames.homescreen_pageritem_new_three)
        
//        arrOfTitleText.append(NSLocalizedString("Intro_titleText_one", comment: ""))
//        arrOfTitleText.append(NSLocalizedString("Intro_titleText_two", comment: ""))
//        arrOfTitleText.append(NSLocalizedString("Intro_titleText_three", comment: ""))
        
        arrOfTitleText.append(StringConstants.HomeViewController.Intro_titleText_one)
        arrOfTitleText.append(StringConstants.HomeViewController.Intro_titleText_two)
        arrOfTitleText.append(StringConstants.HomeViewController.Intro_titleText_three)
        
        arrOfDescriptionText.append(StringConstants.HomeViewController.Intro_DescriptionText_one)
        arrOfDescriptionText.append(StringConstants.HomeViewController.Intro_DescriptionText_two)
    arrOfDescriptionText.append(StringConstants.HomeViewController.Intro_DescriptionText_three)

       // btnPhoneAuth.setTitle(NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.Login, comment: StringConstants.EMPTY), for: .normal)
       // btnexhibitorLogin.setTitle(NSLocalizedString(StringConstants.ExhibitorAdvertiseScreen.navtitle, comment: StringConstants.EMPTY), for: .normal)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        viewPager.scrollToPage(0)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnPhoneAuthClicked(_ sender: Any) {
//        appDelegate = UIApplication.shared.delegate as! AppDelegate
//        if accountKit == nil {
//            // may also specify AKFResponseTypeAccessToken
//            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
//        }
//        let inputState: String = UUID().uuidString
//        let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState)  as AKFViewController
//        viewController.enableSendToFacebook = true
//        viewController.enableGetACall = true
//        self.prepareLoginViewController(viewController)
//        self.present(viewController as! UIViewController, animated: true, completion: nil)
        
        
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "MobileOtpSendViewController") as UIViewController  //MobileOtpSendViewController MobileOTPVerificationVC
        self.navigationController?.pushViewController(dashboardViewController, animated: true)
    }

    func callLoginAPI(fbAccessToken:String) {
        let fcmToken = Messaging.messaging().fcmToken
        let resource = RequestName.loginSourceName
        let params = LoginDetailsRequest.convertToDictionary(loginDetails: LoginDetailsRequest.init(facebookToken: fbAccessToken, resource: resource, fcmDeviceId: fcmToken!,app:StringConstants.appLoginParameter,source:RequestName.loginSourceName, verificationCode: ""))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.Login
        _ = LoginServices().loginUser(apiURL,
                                      postData: params as [String : AnyObject],
                                      withSuccessHandler: { (userModel) in
                                        let model = userModel as! LoginResponse
                                        if model.responseCode == ResponseCode.loginResponseCode{
                                            let model = userModel as! LoginResponse
                                            let userData = model.user_details[0]
                                            let userProfile = userData.userProfile[0]
                                            self.interests = userProfile.interests!
                                            let middlewareToken = model.middlewareToken
                                            print("middlewareToken",middlewareToken ?? String())
                                            let oAuthToken = model.oAuthToken
                                            let token = model.token

                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:middlewareToken! , keyString: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                             SessionManagerForSaveData.saveDataInSessionManager(valueString: oAuthToken!, keyString: StringConstants.NSUserDefauluterKeys.oAuthToken)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.username! , keyString: StringConstants.NSUserDefauluterKeys.username)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: userData.userId!, keyString: StringConstants.NSUserDefauluterKeys.userId)
                                             SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                            
                                            if let pin = userProfile.pin {
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }

                                            
                                            let accountType = String(userProfile.accountType!)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:accountType , keyString: StringConstants.NSUserDefauluterKeys.accountType)
                                            //***
                                            if let country = userProfile.country{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:country , keyString: StringConstants.NSUserDefauluterKeys.country)
                                            }
                                            //Show overlay on discover screen
                                           SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.TRUE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)

                                            
                                            let strInterests = self.interests.joined(separator: ",")
                                            if self.interests.count > 0 { 
                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }else{
                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }
                                            
                                            if let imageUrl = userProfile.imageUrl{
                                                self.imageUrlValue = imageUrl
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }
                                            
                                            var mobileNum = String()
                                            if let state = userProfile.state{
                                                self.state = state  //state city
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: state, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }

                                            if let city = userProfile.city{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: city, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }else{
                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }
                                            
                                             if let mobile1 = userProfile.mobile1{
                                                mobileNum = mobile1
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                            }else{
                                                if let mobile2 = userProfile.mobile2{
                                                    mobileNum = mobile2
                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                                }
                                            }
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.loginResponseCode)

                                           
                                            /*var ejabberdUserId = userData.userId
                                            ejabberdUserId = ejabberdUserId! + "@" + URLConstant.hostName + "/" + resource
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId! , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                            self.didTouchLogIn(userJID: ejabberdUserId!, userPassword: middlewareToken!, server: URLConstant.hostName)
                                            
                                            print("ejabberdUserId",ejabberdUserId ?? String())
                                            print("Password as middlewareToken",middlewareToken ?? String())
                                            print("hostName",URLConstant.hostName)*/
                                         }else if model.responseCode == ResponseCode.signUpResponseCode{
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.signUpResponseCode)

                                            
                                            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                                            print("registrationToken",model.registrationToken!)
                                            userProfileVC.registration_token = model.registrationToken!
                                            self.navigationController?.pushViewController(userProfileVC, animated: true)
                                         }else{
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            self.popupAlert(title: "", message: StringConstants.AlertMessage.errorOccured, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                                                },{action2 in
                                                }, nil])
                                        }
                                        
                                         if model.responseCode == ResponseCode.loginResponseCode{
                                            self.sighIntoGreenCloud()
                                        }
                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            if model.inviteSummary.count > 0{
                                                              
                                                                let inviteSummary = model.inviteSummary[0]
                                                                let standard =  inviteSummary.unlimited_standard
                                                                let extra = inviteSummary.unlimited_extra
                                                                let sent = inviteSummary.unlimited_sent
                                                                let medialink =  inviteSummary.media_link
                                                                let mediatype = inviteSummary.media_type
                                                                let skip_media = inviteSummary.skip_media
                                                                let channelName = inviteSummary.channel_name
                                                                let channelColour = inviteSummary.channel_max_color
                                                                let mediathumb = inviteSummary.media_thumbnail
                                                                let imagechannelProfile = inviteSummary.channel_big_thumb
                                                                let eventStartDate = model.eventstartdatetime

                                                                UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                                UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                                
                                                                UserDefaults.standard.set(extra, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.extra)
                                                                
                                                                UserDefaults.standard.set(sent, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.sent)
                                                                
                                                                
                                                                UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                                
                                                                
                                                                UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                                
                                                                UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                                
                                                                
                                                                UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                                
                                                                UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                                
                                                                UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                                
                                                                UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                                

                                                            }
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)

                                                            let greenPassArray = model.greenpasses

                                                            let myGreenPassCount = greenPassArray.count
                                                           
                                                            UserDefaults.standard.set(myGreenPassCount, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                            

                                                            
                                                            
                                                            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                                                            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)

                                                            //signInTo Ejjaberd
                                                            var ejabberdUserId = userId
                                                            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
                                                            
                                                            print("ejabberdUserId",ejabberdUserId ?? String())
                                                            print("Password as middlewareToken",middlewareToken ?? String())
                                                            print("hostName",URLConstant.hostName)
                                                            self.getInviteSummaryAPI()
                                                            
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
           /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])*/
            //signInTo Ejjaberd
            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)

            var ejabberdUserId = userId
            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
            
            print("ejabberdUserId",ejabberdUserId )
            print("Password as middlewareToken",middlewareToken )
            print("hostName",URLConstant.hostName)
        })
    }
    
    
    //API call to get Invite Summary
    func getInviteSummaryAPI(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        
        //set Source and filter for both exhibitor and Kisan Mitra
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var resource = String()
        var  source = String()
        if accountType == StringConstants.ONE {
            resource = RequestName.kisanMita
            source = RequestName.loginSourceName
        }else{
            resource = RequestName.ExhibitorInvite
            source = RequestName.loginSourceExhibitorName
        }
        
        let filter = [URLConstant.filter1: resource, URLConstant.filter2: userName]
        let params = InviteSummuryRequest.convertToDictionary(inviteSummuryRequest:InviteSummuryRequest.init(eventCode:URLConstant.eventCode , source: source, pagesize: 3, currentpage: 0, sort_col: "", app_key: URLConstant.app_key, sessionId: sessionId, filter: filter as [String : AnyObject]))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.inviteSummary
        _ = InviteSummaryServices().getInviteSummary(apiURL,postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! InviteSummaryResponse
                                                        print("model",model)
                                                        
                                                        let records = model.records
                                                        let inviteSummary = records[0]
                                                        
                                                        let settings = model.settings
                                                        let inviteSetting = settings[0]
                                                        
                                                        let medialink =  inviteSummary.media_link
                                                        let mediatype = inviteSummary.media_type
                                                        let skip_media = inviteSummary.skip_media
                                                        let channelName = inviteSummary.channel_name
                                                        let channelColour = inviteSummary.channel_max_color
                                                        let mediathumb = inviteSummary.media_thumbnail
                                                        let imagechannelProfile = inviteSummary.channel_big_thumb
                                                        let issueGreenpasslastDate = inviteSetting.issue_greenpass_last_date
                                                        let greenpassAcceptLast_date = inviteSetting.greenpass_accept_last_date
                                                        
                                                        var standard =  Int()
                                                        var extra = Int()
                                                        var sent = Int()

                                                        let show_issue_greenpass = inviteSetting.show_issue_greenpass
                                                        
                                                        UserDefaults.standard.set(show_issue_greenpass, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                                                        
                                                        let show_issue_unlimited_greenpass = inviteSetting.show_issue_unlimited_greenpass
                                                        print("show_issue_unlimited_greenpass",show_issue_unlimited_greenpass)
                                                        
                                                        if(accountType == StringConstants.TWO){
                                                            if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 1) {
                                                                standard =  inviteSummary.unlimited_standard!
                                                                extra = inviteSummary.unlimited_extra!
                                                                sent = inviteSummary.unlimited_sent!
                                                            }else if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 0){
                                                                standard =  inviteSummary.quota_standard!
                                                                extra = inviteSummary.quota_extra!
                                                                sent = inviteSummary.quota_sent!
                                                            }
                                                        }else{
                                                            standard =  inviteSummary.unlimited_standard!
                                                            extra = inviteSummary.unlimited_extra!
                                                            sent = inviteSummary.unlimited_sent!
                                                        }
                                                        
                                                        UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                        
                                                        UserDefaults.standard.set(extra, forKey:
                                                            StringConstants.NSUserDefauluterKeys.extra)
                                                        
                                                        UserDefaults.standard.set(sent, forKey:
                                                            StringConstants.NSUserDefauluterKeys.sent)
                                                        
                                                        
                                                        UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                        
                                                        UserDefaults.standard.set(issueGreenpasslastDate, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                        UserDefaults.standard.set(greenpassAcceptLast_date, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
                                                        
                                                        UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                        
                                                        UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                        
                                                        
                                                        UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                        
                                                        UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                        
                                                        UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                        
                                                        UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                        
                                                        
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            //            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            //                },{action2 in
            //                }, nil])
        })
    }
    
    
    
    @IBAction func btnLoginAsExhibitor(_ sender: Any) {
//        let exhibitorLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "ExhibitorLoginVC") as! ExhibitorLoginVC
//        self.navigationController?.pushViewController(exhibitorLoginVC, animated: true)
        
        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let exhibitorLoginVC = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitorLoginVC") as! ExhibitorLoginVC
        self.navigationController?.pushViewController(exhibitorLoginVC, animated: true)
    }
    
}

extension HomeViewController:ViewPagerDataSource{
    func numberOfItems(_ viewPager:ViewPager) -> Int {
        return 3;
    }
    
    func viewAtIndex(_ viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        
        var newView = view;
        var lblTitle:UILabel?
        var lblDescrption:UILabel?
        var  imageOfBaner:UIImageView?
        
        if(newView == nil){
            
            if UIScreen.main.bounds.size.width <= 320{
                newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  300))
                
                lblTitle = UILabel(frame: CGRect(x: (newView?.frame.width)!/2 - 150, y: viewPager.frame.origin.x + 5, width: 300, height: 21)) //y = 5
                lblTitle?.numberOfLines = 0
                lblTitle!.tag = 1
                // label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
                lblTitle!.textAlignment = .center
                lblTitle!.font =  lblTitle!.font.withSize(20)
                newView?.addSubview(lblTitle!)
                
                lblDescrption = UILabel(frame: CGRect(x: (newView?.frame.width)!/2 - 150, y: (lblTitle?.frame.origin.y)! + (lblTitle?.frame.height)! + 5, width: 300, height: 60)) //y = 5
                lblDescrption?.numberOfLines = 0
                lblDescrption?.textColor = UIColor(red: 124/255.0, green: 124/255.0, blue: 124/255.0, alpha: 1.0)
                lblDescrption!.tag = 1
                // label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
                lblDescrption!.textAlignment = .center
                lblDescrption!.font =  lblDescrption!.font.withSize(14)
                newView?.addSubview(lblDescrption!)
                
                if index == 0 {
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4 - 15, y:100 , width: 180, height:180)) //250 y=65

                }else if index == 1{
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4, y:100 , width: 180, height:180)) //250 y=65
                }else if index == 2{
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4 + 16, y:100 , width: 164, height:180)) //250 y=65
                }
    
                imageOfBaner?.tag = 1
                newView?.addSubview(imageOfBaner!)
            }else{
                newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  300))
                
                lblTitle = UILabel(frame: CGRect(x: (newView?.frame.width)!/2 - 150, y: viewPager.frame.origin.x + 5, width: 300, height: 21)) //y = 5
                lblTitle?.numberOfLines = 0
                lblTitle!.tag = 1
                // label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
                lblTitle!.textAlignment = .center
                lblTitle!.font =  lblTitle!.font.withSize(20)
                newView?.addSubview(lblTitle!)
                
                lblDescrption = UILabel(frame: CGRect(x: (newView?.frame.width)!/2 - 150, y: (lblTitle?.frame.origin.y)! + (lblTitle?.frame.height)! + 5, width: 300, height: 60)) //y = 5
                lblDescrption?.numberOfLines = 0
                lblDescrption?.textColor = UIColor(red: 124/255.0, green: 124/255.0, blue: 124/255.0, alpha: 1.0)
                lblDescrption!.tag = 1
                // label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
                lblDescrption!.textAlignment = .center
                lblDescrption!.font =  lblDescrption!.font.withSize(14)
                newView?.addSubview(lblDescrption!)
            
                if index == 0 {
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4 - 15, y:120 , width: 180, height:180)) //250 y=65
                    
                }else if index == 1{
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4, y:120 , width: 180, height:180)) //250 y=65
                }else if index == 2{
                    imageOfBaner = UIImageView(frame: CGRect(x: viewPager.frame.width/4 + 16, y:120 , width: 164, height:180)) //250 y=65
                }

                imageOfBaner?.tag = 1
                newView?.addSubview(imageOfBaner!)
            }
  
        }else{
            lblDescrption = newView?.viewWithTag(1) as? UILabel
            lblTitle = newView?.viewWithTag(1) as? UILabel
            imageOfBaner = newView?.viewWithTag(1) as? UIImageView
        }
        
        lblDescrption?.text = arrOfDescriptionText[index]
        lblTitle?.text = arrOfTitleText[index]
        imageOfBaner?.image = UIImage(named:arrOfImages[index])
        return newView!
        
    }

    func didSelectedItem(_ index: Int) {
        print("select index \(index)")
    }
    
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}


extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}




extension HomeViewController:AKFViewControllerDelegate {
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!,
                        didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        print("Login succcess with AccessToken",accessToken.tokenString)
        fbAccessToken = accessToken.tokenString

        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        self.callLoginAPI(fbAccessToken:fbAccessToken)
//        let dictOfVerify = VerifyUoW.verify(token:accessToken.tokenString)
//        print("dictOfVerify",dictOfVerify)
//        let checkStatus = dictOfVerify["success"] as! Bool
//        print("checkStatus",checkStatus)
//        if checkStatus == true {
//            print("userJID",dictOfVerify["userJID"] ?? String())
//            print("userPassword",dictOfVerify["userPassword"] ?? String())
//
//        }
    }

    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
        do {
            try appDelegate.xmppController = XMPPController(hostName: server,
                                                            userJIDString: userJID,
                                                            password: userPassword)
            appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
            appDelegate.xmppController.connect()
            
        } catch {
            print("Something went wrong")
        }
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("Login succcess with AuthorizationCode",code)
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        print("We have an error \(String(describing: error))")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        print("canceled")
    }
    
    func prepareLoginViewController(_ loginViewController: AKFViewController) {
        loginViewController.delegate = self
        loginViewController.setAdvancedUIManager(nil)
        //Costumize the theme
        let theme:AKFTheme = AKFTheme.default()
        theme.inputBackgroundColor  = UIColor(red: 193.0/255.0, green: 237.0/255.0, blue: 213.0/255.0, alpha: 1)
        theme.headerBackgroundColor = UIColorFromRGB.init(rgb: 0x25B35A)
        theme.buttonBackgroundColor = UIColorFromRGB.init(rgb: 0x25B35A)
        theme.buttonDisabledBackgroundColor = UIColor(red: 193.0/255.0, green: 237.0/255.0, blue: 213.0/255.0, alpha: 1)
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = UIColorFromRGB.init(rgb: 0x25B35A)
        theme.inputTextColor = UIColor(white: 0.4, alpha: 1.0)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        loginViewController.setTheme(theme)
        
    }
}

extension HomeViewController: XMPPStreamDelegate {
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        // Move to chat controller
        print("Aunthenticated at viewcontroller")
        
        if (imageUrlValue.isEmpty){
            MBProgressHUD.hide(for: self.view, animated: true);
            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            self.navigationController?.pushViewController(userProfileVC, animated: true)
            
        }else if (self.interests.count) > 0 {
            MBProgressHUD.hide(for: self.view, animated: true);
            if (state.isEmpty){
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectLocationVC = storyboard.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                self.navigationController?.pushViewController(selectLocationVC, animated: true)
                
            }else{
                
                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
                    print("retriveState",retriveState)
                }
                //                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                //                let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as UIViewController
                //                self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
               /* let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                self.navigationController?.pushViewController(dashboardViewController, animated: true)*/
                self.openPopUpConditionally()
                //self.getAppStarterDataAPI()
            }
            
        }else{
            MBProgressHUD.hide(for: self.view, animated: true);
            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.UserAreaInterestViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
            let userAreaVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
            self.navigationController?.pushViewController(userAreaVC, animated: true)
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Wrong password or username")
    }
    
    
    func openPopUpConditionally(){
        
        
        appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)

      /*  let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
        
        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
            var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
            if(isMitra && show_issue_pass == 0){
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }else{

            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    appDelegate.isLaunchFirstTime = false
                    let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }else if(remaining>0){
                appDelegate.isLaunchFirstTime = false
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
            }else{
                if(myGreenPassCount>0){
                    appDelegate.isLaunchFirstTime = false
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                    self.navigationController?.pushViewController(dashboardViewController, animated: true)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }
            }
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
            self.navigationController?.pushViewController(dashboardViewController, animated: true)
        }*/
    }

    
    
    //API call to get Invite Summary
    func getAppStarterDataAPI(){
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginSourceName
        let params = AppstarterRequest.convertToDictionary(appstarterRequest:AppstarterRequest.init(app_key: URLConstant.app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.appStarterdata
        _ = AppStarterServices().getAppStarterData(apiURL,postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                    let model = userModel as! AppStarterResponse
                                                    print("model",model)
                                                    
                                                    let data = model.data[0]
                                                    let pendingInvite = data.pending_invitation_count
                                                    let myGreenPassCount = data.greenpass_count
                                                    
                                                    UserDefaults.standard.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
                                                    
                                                    UserDefaults.standard.set(myGreenPassCount, forKey:
                                                        StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                    
                                                    self.openPopUpConditionally()
                                                    
                                                    //let isMitra = data.isMitra
                                                    let defaults=UserDefaults.standard
                                                    defaults.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                    defaults.set(myGreenPassCount, forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount)
                                                    defaults.synchronize()
                                                    /*if pendingInvite! > 0{
                                                     if ((UserDefaults.standard.value(forKey: "isNotFirstTime")) != nil) && UserDefaults.standard.value(forKey: "isNotFirstTime") as! Bool == true{
                                                     print("not First time show congratulation")
                                                     UserDefaults.standard.set(false, forKey: "isNotFirstTime")
                                                     }else{
                                                     print("First time show congratulation")
                                                     UserDefaults.standard.set(true, forKey: "isNotFirstTime")
                                                     let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                                                     self.navigationController?.pushViewController(vc, animated: false)
                                                     }
                                                     }*/
                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }
    
    
}

extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
