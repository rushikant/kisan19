//
//  IntroducingChannelViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 09/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class IntroducingChannelViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = StringConstants.NavigationTitle.introChannelsScreenNaviTitle
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }

    @IBAction func btnStartYourChannelClick(_ sender: Any) {
        SessionDetailsForCommunityType.shared.communityTypeChannel = StringConstants.CommunityType.CHANNEL
        let CreateCommunityStep1VC = self.storyboard?.instantiateViewController(withIdentifier: "CreateCommunityStep1ViewController") as! CreateCommunityStep1ViewController
        self.navigationController?.pushViewController(CreateCommunityStep1VC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
