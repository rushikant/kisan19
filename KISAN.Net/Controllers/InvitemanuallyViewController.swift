//
//  InvitemanuallyViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 24/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here
protocol PassSelectedContactDetails: class {
    func passSelectedContactDetails(_ contact: String)
}

class InvitemanuallyViewController: UIViewController {

    @IBOutlet weak var lblOfValidation: UILabel!
    @IBOutlet weak var txtOfEnterMobNum: UITextField!
    weak var delegate: PassSelectedContactDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.invite, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(nextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        self.title = (NSLocalizedString(StringConstants.NavigationTitle.inviteManually, comment: StringConstants.EMPTY))
        lblOfValidation.isHidden = true
        txtOfEnterMobNum.becomeFirstResponder()
        lblOfValidation.text = (NSLocalizedString(StringConstants.Validation.numbervalidation, comment: StringConstants.EMPTY))
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtOfEnterMobNum.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func nextTapped(sender: UIBarButtonItem) {
        
        if (txtOfEnterMobNum.text?.count)! < 10 {
            lblOfValidation.isHidden = false
        }else{
            lblOfValidation.isHidden = true
            delegate?.passSelectedContactDetails(txtOfEnterMobNum.text!)
            self.navigationController?.popViewController(animated: false)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InvitemanuallyViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charsLimit = 10
        let startingLength = textField.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace =  range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        
        return newLength <= charsLimit
    }
}
