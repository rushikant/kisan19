//
//  IssueBadgesCongratulationsViewController.swift
//  KISAN.Net
//
//  Created by Rushikant on 18/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class IssueBadgesCongratulationsViewController: UIViewController {

    @IBOutlet weak var lblGoToHome: UILabel!
    
    @IBOutlet weak var lblSentCount: UILabel!
    
    @IBOutlet weak var lblIssueDate: UILabel!
    var strSenntInviteCout :Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString = NSMutableAttributedString.init(string: "Go to Home Screen")
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        lblGoToHome.attributedText = attributedString
        
        let valueOfSenntInviteCout = strSenntInviteCout
        let intSenntInviteCout = valueOfSenntInviteCout.formattedWithSeparator
        lblSentCount.text = intSenntInviteCout + StringConstants.singleSpace + "Badges sent successfully."
        
        //Before 7th December
        var  exh_badge_enddatetimeValue = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime)
        exh_badge_enddatetimeValue = "Before " +  exh_badge_enddatetimeValue
        lblIssueDate.text = exh_badge_enddatetimeValue
    }
    

    @IBAction func btnIssueMoreBadgesClicked(_ sender: Any) {
      
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
        vc.comingFrom = StringConstants.flags.issueBadges
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is InviteFriendBaseVC {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
    @IBAction func btnGoToHomeClicked(_ sender: Any) {
        
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
}

