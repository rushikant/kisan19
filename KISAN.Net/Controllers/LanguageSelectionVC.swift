//
//  LanguageSelectionVC.swift
//  KISAN.Net
//
//  Created by Rushikant on 25/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here
protocol PassSelectedLanguage: class {
    func passSelectedLanguage(_ language: String)
}

class LanguageSelectionVC: UIViewController {
    
    @IBOutlet weak var englishLanguage: UIButton!
    @IBOutlet weak var hindiLanguage: UIButton!
    @IBOutlet weak var marathiLanguage: UIButton!
    @IBOutlet weak var backgroundCaptureImage: UIImageView!
    weak var delegate: PassSelectedLanguage?
    var backImageCapturedValue = UIImage()
    var comingFrom = String()
    
    @IBOutlet weak var lblpopupTitl: UILabel!
    @IBOutlet weak var lblpopUpDescription: UILabel!
    @IBOutlet weak var imgpopUpTitl: UIImageView!
    
    
    @IBOutlet weak var viewOfBackPopUp: UIView!
    override func viewDidLoad() {
        backgroundCaptureImage.image = backImageCapturedValue
        viewOfBackPopUp.layer.cornerRadius = 8
        viewOfBackPopUp.clipsToBounds = true
        print(comingFrom)
        if(comingFrom == StringConstants.flags.shareYourFriendsVc){
            lblpopupTitl.text = "Select language for SMS"
            lblpopUpDescription.text = "Select the language in which you would like to share it with Friends."
            imgpopUpTitl.isHidden = true

        }else{
            lblpopupTitl.text = "Invite in your own language"
            lblpopUpDescription.text = "Select the language in which you would like to invitation SMS"
            imgpopUpTitl.isHidden = false

        }
    }
    
    @IBAction func englishLanguageSelected(_ sender: Any) {
        delegate?.passSelectedLanguage("english")
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func hindiLanguageSelected(_ sender: Any) {
        delegate?.passSelectedLanguage("hindi")
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func marathiLanguageSelected(_ sender: Any) {
        delegate?.passSelectedLanguage("marathi")
        self.dismiss(animated: false, completion: nil)
    }
}
