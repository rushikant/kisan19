//
//  MapViewForSendLocVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewForSendLocVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblSendToChannelName: UILabel!
    @IBOutlet weak var lblDetailAddress: UILabel!
    @IBOutlet weak var lblTitleAddress: UILabel!
    var appDelegate = AppDelegate()
    var commingFrom = String()
    var strTitleAddress = String()
    var strDetailAddress = String()
    var strLatitude = String()
    var strLongitude = String()
    var strchannelname = String()
    var myChatsData = MyChatsTable()
    var channelname =  String()

    @IBOutlet var btnsendlocation: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        initialDesign()
        
        print(strchannelname)
        //Google map
        let target = CLLocationCoordinate2D(latitude: Double(strLatitude)!, longitude: Double(strLongitude)!)
        mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: 14.0)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(strLatitude)!, longitude: Double(strLongitude)!)
        marker.title = "U R Locaton."
        //marker.snippet = "Australia"
        marker.map = mapView
        
        //Set Address
        lblTitleAddress.text = strTitleAddress
        lblDetailAddress.text = strDetailAddress
        btnsendlocation.setTitle(StringConstants.EMPTY, for: .normal)
        lblSendToChannelName.text = strchannelname
        lblSendToChannelName.text = channelname

    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(strchannelname)
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        if preferredLanguage == "en" {
            print("this is English")
            let str =  (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendto, comment: StringConstants.EMPTY)) + channelname
            lblSendToChannelName.text = str.uppercased()
        } else if preferredLanguage == "mr" {
            print("this is Ukrainian")
            let str = channelname + (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendto, comment: StringConstants.EMPTY))
            lblSendToChannelName.text = str.uppercased()
        }
        else{
            let str = channelname + (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendto, comment: StringConstants.EMPTY))
            lblSendToChannelName.text = str.uppercased()
        }
      

    }
    
    func initialDesign()  {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
    }

    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSendClick(_ sender: Any) {
      
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            
            appDelegate.locationTitleAddress = lblTitleAddress.text!
            appDelegate.locationDetailddress = lblDetailAddress.text!
            appDelegate.latitudeOfSendLocation = strLatitude
            appDelegate.longitudeOfSendLocation = strLongitude
            
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is ChannelSendBroadcastMsgVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }else if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
            
            appDelegate.locationTitleAddress = lblTitleAddress.text!
            appDelegate.locationDetailddress = lblDetailAddress.text!
            appDelegate.latitudeOfSendLocation = strLatitude
            appDelegate.longitudeOfSendLocation = strLongitude
            
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is OneToOneFromAdminVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
