//
//  MessageInfoVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MessageInfoVC: UIViewController {
    var lblOfTitle = UILabel()
    @IBOutlet weak var viewOfBackChatLabel: UIView!
    @IBOutlet weak var bubbleImgeView: UIImageView!
    @IBOutlet weak var imageViewOfStastus: UIImageView!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblChatMsg: UILabel!
    @IBOutlet weak var tblOfShowMessageInfo: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
    }
    func initialDesign(){
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        tblOfShowMessageInfo.tableFooterView = UIView()
        
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = StringConstants.NavigationTitle.messageInformation
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
        bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
        
        tblOfShowMessageInfo.separatorColor = UIColor.clear
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension MessageInfoVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageInfoVCCell", for: indexPath) as! MessageInfoVCCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.imageViewOfProfile.layer.cornerRadius = cell.imageViewOfProfile.frame.size.width / 2
        cell.lblNameOfMessageSender.text = "Kunal Patil"
        cell.lblOfDateTime.text = "12 February 3:21 PM"
        cell.lblMessageSendStatus.text = "sent"
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 115
    }
    
}
