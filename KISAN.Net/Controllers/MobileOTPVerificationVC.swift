//
//  MobileOTPVerificationVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 25/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import AccountKit
import XMPPFramework
import  Firebase
import FirebaseMessaging

class MobileOTPVerificationVC: UIViewController {

    @IBOutlet weak var lblPlaceOrderMsgForSentNumber: UILabel!
    
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var txtOTPOne: UITextField!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var txtOTPTwo: UITextField!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var txtOTPThree: UITextField!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var txtOTPFour: UITextField!
    @IBOutlet weak var viewFive: UIView!
    @IBOutlet weak var txtOTPFive: UITextField!
    @IBOutlet weak var viewSix: UIView!
    @IBOutlet weak var txtOTPSix: UITextField!
    @IBOutlet weak var lblResendOTP: UILabel!
    @IBOutlet weak var lblChangeMobileNum: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgViewOfCircularGreenCheckMark: UIImageView!
    @IBOutlet weak var loaderImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    public var interests = [String]()
    var state = String()
    var imageUrlValue = String()
    public var token = String()
    public var userMobileNumber = String()
    @IBOutlet weak var txtEnterOtp: UITextField!
    @IBOutlet weak var lblError: UILabel!
    var AuthToken = String()
    var appDelegate = AppDelegate()

    
    
    @IBOutlet weak var bottomOfKisanLogo: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.initialDesign()
        setupLblPlaceOrderMsgForSentNumber()
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title:(NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)) , style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtEnterOtp.inputAccessoryView = toolbar

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    func initialDesign(){
        
        txtEnterOtp.becomeFirstResponder()
        viewOne.layer.cornerRadius = 5.0
        viewTwo.layer.cornerRadius = 5.0
        viewThree.layer.cornerRadius = 5.0
        viewFour.layer.cornerRadius = 5.0
        viewFive.layer.cornerRadius = 5.0
        viewSix.layer.cornerRadius = 5.0
        btnNext.layer.cornerRadius = 5
        
        self.btnNext.backgroundColor = UIColor(red: 0/255, green: 156/255, blue: 16/255, alpha: 1.0)
        
        self.btnNext.setTitleColor(UIColor(red: 89/255, green: 192/255, blue: 109/255, alpha: 1.0), for: .normal)
        
        self.btnNext.isEnabled = false
        
        txtEnterOtp.attributedPlaceholder = NSAttributedString(string: "Enter OTP",
                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

        
        let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
        let change_mobile_numberUnderlineAttributedString = NSAttributedString(string: StringConstants.MobileOTPVerificationVC.change_mobile_number, attributes: underlineAttribute)
        lblChangeMobileNum.attributedText = change_mobile_numberUnderlineAttributedString

        let resend_OTPUnderlineAttributedString = NSAttributedString(string: StringConstants.MobileOTPVerificationVC.resend_OTP, attributes: underlineAttribute)
        lblResendOTP.attributedText = resend_OTPUnderlineAttributedString
        
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeMobileClicked(_:)))
        tap1.cancelsTouchesInView = false
        lblChangeMobileNum.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.resendOTPClicked(_:)))
        tap2.cancelsTouchesInView = false
        lblResendOTP.addGestureRecognizer(tap2)
        
        if UIScreen.main.bounds.size.height <= 568{
            bottomOfKisanLogo.constant = 151
            scrollView.isScrollEnabled = false
        }else{
            bottomOfKisanLogo.constant = 43
            scrollView.isScrollEnabled = true
        }
    }
    
    func setupLblPlaceOrderMsgForSentNumber() {
        let mobileNumber = String(userMobileNumber.dropFirst(3))
        let partOneAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0, weight: .regular)]
        
        let partTwoAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.0, weight: .bold)]
        
        
        let partOne = NSMutableAttributedString(string: "Please enter the OTP sent to ", attributes: partOneAttributes)
        let partTwo = NSMutableAttributedString(string: "\(mobileNumber)", attributes: partTwoAttributes)
        
        let combination = NSMutableAttributedString()
        combination.append(partOne)
        combination.append(partTwo)
        lblPlaceOrderMsgForSentNumber.attributedText = combination
    }
    
    func callLoginAPI() {
        
        let fcmToken = Messaging.messaging().fcmToken
        let resource = RequestName.loginSourceName
        let params = LoginDetailsRequest.convertToDictionary(loginDetails: LoginDetailsRequest.init(facebookToken: token, resource: resource, fcmDeviceId: fcmToken!,app:StringConstants.appLoginParameter,source:RequestName.loginSourceName,verificationCode:txtEnterOtp.text!))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.Login
        _ = LoginServices().loginUser(apiURL,
                                      postData: params as [String : AnyObject],
                                      withSuccessHandler: { (userModel) in
                                        let model = userModel as! LoginResponse
                                        if model.responseCode == ResponseCode.loginResponseCode{
                                            let model = userModel as! LoginResponse
                                            let userData = model.user_details[0]
                                            let userProfile = userData.userProfile[0]
                                            self.interests = userProfile.interests!
                                            let middlewareToken = model.middlewareToken
                                            print("middlewareToken",middlewareToken ?? String())
                                            let oAuthToken = model.oAuthToken
                                            let token = model.token
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:middlewareToken! , keyString: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: oAuthToken!, keyString: StringConstants.NSUserDefauluterKeys.oAuthToken)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.username! , keyString: StringConstants.NSUserDefauluterKeys.username)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: userData.userId!, keyString: StringConstants.NSUserDefauluterKeys.userId)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                            
                                            if let pin = userProfile.pin {
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }
                                            
                                            
                                            let accountType = String(userProfile.accountType!)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:accountType , keyString: StringConstants.NSUserDefauluterKeys.accountType)
                                            //***
                                            if let country = userProfile.country{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:country , keyString: StringConstants.NSUserDefauluterKeys.country)
                                            }
                                            //Show overlay on discover screen
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.TRUE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
                                            
                                            
                                            let strInterests = self.interests.joined(separator: ",")
                                            if self.interests.count > 0 {
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }
                                            
                                            if let imageUrl = userProfile.imageUrl{
                                                self.imageUrlValue = imageUrl
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }
                                            
                                            var mobileNum = String()
                                            if let state = userProfile.state{
                                                self.state = state  //state city
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: state, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }
                                            
                                            if let city = userProfile.city{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: city, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }
                                            
                                            if let mobile1 = userProfile.mobile1{
                                                mobileNum = mobile1
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                            }else{
                                                if let mobile2 = userProfile.mobile2{
                                                    mobileNum = mobile2
                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                                }
                                            }
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.loginResponseCode)
                                            
                                            
                                            /*var ejabberdUserId = userData.userId
                                             ejabberdUserId = ejabberdUserId! + "@" + URLConstant.hostName + "/" + resource
                                             SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId! , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                             self.didTouchLogIn(userJID: ejabberdUserId!, userPassword: middlewareToken!, server: URLConstant.hostName)
                                             
                                             print("ejabberdUserId",ejabberdUserId ?? String())
                                             print("Password as middlewareToken",middlewareToken ?? String())
                                             print("hostName",URLConstant.hostName)*/
                                        }else if model.responseCode == ResponseCode.signUpResponseCode{
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.signUpResponseCode)
                                            
                                            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)

                                            let userProfileVC = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                                            print("registrationToken",model.registrationToken!)
                                            userProfileVC.registration_token = model.registrationToken!
                                            self.navigationController?.pushViewController(userProfileVC, animated: true)
                                        }else{
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            self.view.isUserInteractionEnabled = true
                                            self.imgViewOfCircularGreenCheckMark.image = UIImage(named: "error_image")
                                            self.imgViewOfCircularGreenCheckMark.isHidden = false
                                            self.loaderImageView.isHidden = true
                                            self.lblError.isHidden = false
                                            self.lblError.text = "Invalid OTP. Please try again."
//                                            self.popupAlert(title: "", message: StringConstants.AlertMessage.errorOccured, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
//                                                },{action2 in
//                                                }, nil])
                                        }
                                        
                                        if model.responseCode == ResponseCode.loginResponseCode{
                                            self.sighIntoGreenCloud()
                                        }
                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            self.view.isUserInteractionEnabled = true
            self.imgViewOfCircularGreenCheckMark.image = UIImage(named: "error_image")
            self.imgViewOfCircularGreenCheckMark.isHidden = false
            self.loaderImageView.isHidden = true
            self.lblError.isHidden = false
            self.lblError.text = "Invalid OTP. Please try again."
        })
    }
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            if model.inviteSummary.count > 0{
                                                              
                                                                let inviteSummary = model.inviteSummary[0]
                                                                let standard =  inviteSummary.unlimited_standard
                                                                let extra = inviteSummary.unlimited_extra
                                                                let sent = inviteSummary.unlimited_sent
                                                                let medialink =  inviteSummary.media_link
                                                                let mediatype = inviteSummary.media_type
                                                                let skip_media = inviteSummary.skip_media
                                                                let channelName = inviteSummary.channel_name
                                                                let channelColour = inviteSummary.channel_max_color
                                                                let mediathumb = inviteSummary.media_thumbnail
                                                                let imagechannelProfile = inviteSummary.channel_big_thumb
                                                                let eventStartDate = model.eventstartdatetime

                                                                UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                                UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                                
                                                                UserDefaults.standard.set(extra, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.extra)
                                                                
                                                                UserDefaults.standard.set(sent, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.sent)
                                                                
                                                                
                                                                UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                                
                                                                
                                                                UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                                
                                                                UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                                
                                                                
                                                                UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                                
                                                                UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                                
                                                                UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                                
                                                                UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                                

                                                            }
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)

                                                            let greenPassArray = model.greenpasses

                                                            let myGreenPassCount = greenPassArray.count
                                                           
                                                            UserDefaults.standard.set(myGreenPassCount, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                            

                                                            
                                                            
                                                            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                                                            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)

                                                            //signInTo Ejjaberd
                                                            var ejabberdUserId = userId
                                                            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
                                                            
                                                            print("ejabberdUserId",ejabberdUserId ?? String())
                                                            print("Password as middlewareToken",middlewareToken ?? String())
                                                            print("hostName",URLConstant.hostName)
                                                            self.getInviteSummaryAPI()
                                                            
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
           /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])*/
            //signInTo Ejjaberd
            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)

            var ejabberdUserId = userId
            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
            
            print("ejabberdUserId",ejabberdUserId )
            print("Password as middlewareToken",middlewareToken )
            print("hostName",URLConstant.hostName)
        })
    }
    
    //API call to get Invite Summary
       func getInviteSummaryAPI(){
           
           AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
           let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
           let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
           
           //set Source and filter for both exhibitor and Kisan Mitra
           let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
           var resource = String()
           var  source = String()
           if accountType == StringConstants.ONE {
               resource = RequestName.kisanMita
               source = RequestName.loginSourceName
           }else{
               resource = RequestName.ExhibitorInvite
               source = RequestName.loginSourceExhibitorName
           }
           
           let filter = [URLConstant.filter1: resource, URLConstant.filter2: userName]
           let params = InviteSummuryRequest.convertToDictionary(inviteSummuryRequest:InviteSummuryRequest.init(eventCode:URLConstant.eventCode , source: source, pagesize: 3, currentpage: 0, sort_col: "", app_key: URLConstant.app_key, sessionId: sessionId, filter: filter as [String : AnyObject]))
           print("params",params)
           let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.inviteSummary
           _ = InviteSummaryServices().getInviteSummary(apiURL,postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                           let model = userModel as! InviteSummaryResponse
                                                           print("model",model)
                                                           
                                                           let records = model.records
                                                           let inviteSummary = records[0]
                                                           
                                                           let settings = model.settings
                                                           let inviteSetting = settings[0]
                                                           
                                                           let medialink =  inviteSummary.media_link
                                                           let mediatype = inviteSummary.media_type
                                                           let skip_media = inviteSummary.skip_media
                                                           let channelName = inviteSummary.channel_name
                                                           let channelColour = inviteSummary.channel_max_color
                                                           let mediathumb = inviteSummary.media_thumbnail
                                                           let imagechannelProfile = inviteSummary.channel_big_thumb
                                                           let issueGreenpasslastDate = inviteSetting.issue_greenpass_last_date
                                                           let greenpassAcceptLast_date = inviteSetting.greenpass_accept_last_date
                                                           
                                                           var standard =  Int()
                                                           var extra = Int()
                                                           var sent = Int()

                                                           let show_issue_greenpass = inviteSetting.show_issue_greenpass
                                                           
                                                           UserDefaults.standard.set(show_issue_greenpass, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                                                           
                                                           let show_issue_unlimited_greenpass = inviteSetting.show_issue_unlimited_greenpass
                                                           print("show_issue_unlimited_greenpass",show_issue_unlimited_greenpass)
                                                           
                                                           if(accountType == StringConstants.TWO){
                                                               if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 1) {
                                                                   standard =  inviteSummary.unlimited_standard!
                                                                   extra = inviteSummary.unlimited_extra!
                                                                   sent = inviteSummary.unlimited_sent!
                                                               }else if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 0){
                                                                   standard =  inviteSummary.quota_standard!
                                                                   extra = inviteSummary.quota_extra!
                                                                   sent = inviteSummary.quota_sent!
                                                               }
                                                           }else{
                                                               standard =  inviteSummary.unlimited_standard!
                                                               extra = inviteSummary.unlimited_extra!
                                                               sent = inviteSummary.unlimited_sent!
                                                           }
                                                           
                                                           UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                           
                                                           UserDefaults.standard.set(extra, forKey:
                                                               StringConstants.NSUserDefauluterKeys.extra)
                                                           
                                                           UserDefaults.standard.set(sent, forKey:
                                                               StringConstants.NSUserDefauluterKeys.sent)
                                                           
                                                           
                                                           UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                           
                                                           UserDefaults.standard.set(issueGreenpasslastDate, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                           UserDefaults.standard.set(greenpassAcceptLast_date, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
                                                           
                                                           UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                           
                                                           UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                           
                                                           
                                                           UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                           
                                                           UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                           
                                                           UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                           
                                                           UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                           
                                                           
                                                           
                                                           
           }, withFailureHandlere: { (error: String) in
               print("error",error)
               MBProgressHUD.hide(for: self.view, animated: true);
               //            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
               //                },{action2 in
               //                }, nil])
           })
       }
    
    
       
//    func callLoginAPI() {
//
//            let vCode = txtEnterOtp.text!
//            let params = MobileOtpVerificationRequest.convertToDictionary(
//                MobileOtpVerificationRequest:MobileOtpVerificationRequest.init(
//                    client_id : URLConstant.clientId,
//                    client_secret :URLConstant.client_secret,
//                    verification_token :verificationCode,
//                    verification_code: vCode,
//                    account_type:1,
//                    source:"app"))
//            print("Params", params)
//            let apiURL = URLConstant.authBaseUrl + URLConstant.api_token_custom_otp_auth
//            print("RushikantB:-\(apiURL)")
//            _ = LoginServices().loginUser(apiURL,
//                                          postData: params as [String : AnyObject],
//                                          withSuccessHandler: { (userModel) in
//                                            let model = userModel as! LoginResponse
//                                            self.imgViewOfCircularGreenCheckMark.isHidden = false
//                                            if model.responseCode == ResponseCode.loginResponseCode{
//                                                let model = userModel as! LoginResponse
//                                                let userData = model.user_details[0]
//                                                let userProfile = userData.userProfile[0]
//                                                self.interests = userProfile.interests!
//                                                let token = model.token
//
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.username! , keyString: StringConstants.NSUserDefauluterKeys.username)
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: userProfile.id!, keyString: StringConstants.NSUserDefauluterKeys.userId)
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
//
//                                                if let pin = userProfile.pin {
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
//                                                }else{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
//                                                }
//
//
//                                                let accountType = String(userProfile.accountType!)
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:accountType , keyString: StringConstants.NSUserDefauluterKeys.accountType)
//                                                //***
//                                                if let country = userProfile.country{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:country , keyString: StringConstants.NSUserDefauluterKeys.country)
//                                                }
//                                                //Show overlay on discover screen
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.TRUE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
//
//
//                                                let strInterests = self.interests.joined(separator: ",")
//                                                if self.interests.count > 0 {
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
//                                                }else{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
//                                                }
//
//                                                if let imageUrl = userProfile.imageUrl{
//                                                    self.imageUrlValue = imageUrl
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
//                                                }else{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
//                                                }
//
//                                                var mobileNum = String()
//                                                if let state = userProfile.state{
//                                                    self.state = state  //state city
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: state, keyString: StringConstants.NSUserDefauluterKeys.state)
//                                                }else{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
//                                                }
//
//                                                if let city = userProfile.city{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: city, keyString: StringConstants.NSUserDefauluterKeys.city)
//                                                }else{
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
//                                                }
//
//                                                if let mobile1 = userProfile.mobile1{
//                                                    mobileNum = mobile1
//                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
//                                                }else{
//                                                    if let mobile2 = userProfile.mobile2{
//                                                        mobileNum = mobile2
//                                                        SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
//                                                    }
//                                                }
//
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.loginResponseCode)
//
//
//                                                /*var ejabberdUserId = userData.userId
//                                                 ejabberdUserId = ejabberdUserId! + "@" + URLConstant.hostName + "/" + resource
//                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId! , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
//                                                 self.didTouchLogIn(userJID: ejabberdUserId!, userPassword: middlewareToken!, server: URLConstant.hostName)
//
//                                                 print("ejabberdUserId",ejabberdUserId ?? String())
//                                                 print("Password as middlewareToken",middlewareToken ?? String())
//                                                 print("hostName",URLConstant.hostName)*/
//                                            }else if model.responseCode == ResponseCode.customOtpSignUpResponseCode{
//                                                MBProgressHUD.hide(for: self.view, animated: true);
//
//                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:String(model.responseCode!) , keyString: StringConstants.NSUserDefauluterKeys.signUpResponseCode)
//
//
//                                                let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//                                                print("registrationToken",model.registrationToken!)
//                                                userProfileVC.registration_token = model.registrationToken!
//                                                self.navigationController?.pushViewController(userProfileVC, animated: true)
//                                            }else{
//                                                MBProgressHUD.hide(for: self.view, animated: true);
//                                                self.popupAlert(title: "", message: StringConstants.AlertMessage.errorOccured, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
//                                                    },{action2 in
//                                                    }, nil])
//                                            }
//
////                                            if model.responseCode == ResponseCode.loginResponseCode{
////                                                HomeViewController().sighIntoGreenCloud()
////                                            }
//
//            }, withFailureHandlere: { (error: String) in
//                print("error",error)
//                self.imgViewOfCircularGreenCheckMark.image = UIImage(named: "error_image")
//                self.imgViewOfCircularGreenCheckMark.isHidden = false
//                self.lblError.isHidden = false
//                self.lblError.text = error
//            })
//        }
//

    @objc func changeMobileClicked(_ sender: UITapGestureRecognizer) {
        print("changeMobileClicked")
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func resendOTPClicked(_ sender: UITapGestureRecognizer) {
        print("resendOTPClicked")
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            
            self.btnNext.isHidden = true
            loaderImageView.isHidden = false
            imgViewOfCircularGreenCheckMark.isHidden = true
            loaderImageView.transform = CGAffineTransform.identity
            txtEnterOtp.text = StringConstants.EMPTY
            
            UIView.animate(withDuration: 2.0,
                           delay: 0,
                           options: .curveLinear,
                           animations: { [weak self] in
                            self?.loaderImageView.transform = CGAffineTransform(rotationAngle: CGFloat(360.0))
                },
                           completion: { (completed: Bool) -> Void in
                            
                            let params = SendCustomMobileOtpRequest.convertToDictionary(
                                SendCustomMobileOtpRequest:SendCustomMobileOtpRequest.init(
                                    client_id : URLConstant.clientId,
                                    client_secret :URLConstant.client_secret,
                                    mobile :self.userMobileNumber,
                                    account_type:1,
                                    source:"app"))
                            let apiURL = URLConstant.authBaseUrl + URLConstant.sendCustomOtp
                            
                            _ = SendCustomMobileOtpServices().sendOtp(
                                apiURL,
                                postData: params as [String : AnyObject],
                                withSuccessHandler: { (userModel) in
                                    let model = userModel as! SendCustomMobileOtpResponse
                                    let data = model.data
                                    let token = data["token"]! as! String
                                    print("Resend RuhsiToken-:\(token)")
                                    self.token = token
                                    print("Otp Sent")
                                    self.loaderImageView.isHidden = true
                                    self.imgViewOfCircularGreenCheckMark.isHidden = false
                                    self.imgViewOfCircularGreenCheckMark.image = UIImage(named: "circularGreenCheckMark")
                                    self.lblError.text = "OTP Sent"
                                    self.lblError.isHidden = false
                                    
                            }, withFailureHandlere: { (error: String) in
                                
                                self.loaderImageView.isHidden = true
                                self.imgViewOfCircularGreenCheckMark.image = UIImage(named: "error_image")
                                self.imgViewOfCircularGreenCheckMark.isHidden = false
                                self.lblError.isHidden = false
                                self.lblError.text = error
                                
                            })
            })
        }else{
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)

        }
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
        print("doneButtonAction")
        //callLoginAPI()
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        print("btnNextClick")
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            btnNext.isHidden = true
            loaderImageView.isHidden = false
            imgViewOfCircularGreenCheckMark.isHidden = true
            loaderImageView.transform = CGAffineTransform.identity
            txtEnterOtp.resignFirstResponder()
            self.view.isUserInteractionEnabled = false
            UIView.animate(withDuration: 1,
                           delay: 0,
                           options: [.repeat,.curveLinear],
                           animations: { [weak self] in
                            self?.loaderImageView.transform = (self?.loaderImageView.transform.rotated(by: CGFloat(M_PI)))!;
                },
                           completion: { (completed: Bool) -> Void in
            })
            self.callLoginAPI()
        }else{
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }
    }
    
    
}

extension MobileOTPVerificationVC: UITextFieldDelegate{
    
   /* func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtOTPOne {
            
            txtOTPTwo .becomeFirstResponder()
            
        }else if textField == txtOTPTwo{
            
            txtOTPThree .becomeFirstResponder()
            
        }else if textField == txtOTPThree{
            
            txtOTPFour.resignFirstResponder()
            
        }else if textField == txtOTPFive{
            
            txtOTPSix.resignFirstResponder()
            
        }else if textField == txtOTPSix{
            
            txtOTPSix.resignFirstResponder()
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        print("textFieldShouldBeginEditing isBackSpace")
        if UIScreen.main.bounds.size.height <= 568{
            scrollView.isScrollEnabled = true
        }
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        print("textFieldDidEndEditing isBackSpace")
        if UIScreen.main.bounds.size.height <= 568{
            scrollView.isScrollEnabled = false
        }
    }

    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       
        /*if (string.length==0) { //Delete any cases
            if(range.length > 1){
                //Delete whole word
                print("Delete whole word")
            }
            else if(range.length == 1){
                //Delete single letter
                print("Delete single letter")
            }
            else if(range.length == 0){
                //Tap delete key when textField empty
                print("Tap delete key when textField empty")
            }
        }
        
        
        
        if textField == txtOTPOne  {
            let currentText = txtOTPOne.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPOne.text = string
            if !(txtOTPOne.text!.isEmpty){
                txtOTPTwo.becomeFirstResponder()
            }
            //txtOTPOne.resignFirstResponder()
            return updatedText.count <= 1
        }else if textField == txtOTPTwo  {
            let currentText = txtOTPTwo.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPTwo.text = string
            if !(txtOTPTwo.text!.isEmpty){
                txtOTPThree.becomeFirstResponder()
            }
            //txtOTPTwo.resignFirstResponder()
            return updatedText.count <= 1
        }else if textField == txtOTPThree  {
            let currentText = txtOTPThree.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPThree.text = string
            if !(txtOTPThree.text!.isEmpty){
                txtOTPFour.becomeFirstResponder()
            }
           // txtOTPThree.resignFirstResponder()
            return updatedText.count <= 1
        }else if textField == txtOTPFour  {
            let currentText = txtOTPFour.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPFour.text = string
            
            if !(txtOTPFour.text!.isEmpty){
                txtOTPFive.becomeFirstResponder()
            }
            
            
            //txtOTPFour.resignFirstResponder()
            return updatedText.count <= 1
        }else if textField == txtOTPFive  {
            let currentText = txtOTPFive.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPFive.text = string
            
            if !(txtOTPFive.text!.isEmpty){
                txtOTPSix.becomeFirstResponder()
            }
            
            //txtOTPFive.resignFirstResponder()
            return updatedText.count <= 1
        }else if textField == txtOTPSix  {
            let currentText = txtOTPSix.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            txtOTPSix.text = string
            
            if !(txtOTPFive.text!.isEmpty){
                txtOTPSix.resignFirstResponder()
            }
            
            
            //txtOTPSix.resignFirstResponder()
            return updatedText.count <= 1
        }
        return true*/
        
        // Range.length == 1 means,clicking backspace
        
        
       
        
        if (range.length == 0){
            if textField == txtOTPOne {
                txtOTPTwo?.becomeFirstResponder()
            }
            if textField == txtOTPTwo {
                txtOTPThree?.becomeFirstResponder()
            }
            if textField == txtOTPThree {
                txtOTPFour?.becomeFirstResponder()
            }
            if textField == txtOTPFour {
                txtOTPFive?.becomeFirstResponder()
            }
            if textField == txtOTPFive {
                txtOTPSix?.becomeFirstResponder()
            }
            if textField == txtOTPSix {
                txtOTPSix?.resignFirstResponder()
            }
            textField.text? = string
            return false
        }else if (range.length == 1) {
            if textField == txtOTPSix {
                txtOTPFive?.becomeFirstResponder()
            }
            if textField == txtOTPFive {
                txtOTPFour?.becomeFirstResponder()
            }
            if textField == txtOTPFour {
                txtOTPThree?.becomeFirstResponder()
            }
            if textField == txtOTPThree {
                txtOTPTwo?.becomeFirstResponder()
            }
            if textField == txtOTPTwo {
                txtOTPOne?.becomeFirstResponder()
            }
            if textField == txtOTPOne {
                txtOTPOne?.resignFirstResponder()
            }
            textField.text? = ""
            return false
        }
        return true
    }*/
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = txtEnterOtp.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText.count < 6 {
            btnNext.backgroundColor = UIColor(red: 0/255, green: 156/255, blue: 16/255, alpha: 1.0)
            self.btnNext.setTitleColor(UIColor(red: 89/255, green: 192/255, blue: 109/255, alpha: 1.0), for: .normal)
            self.btnNext.isEnabled = false
        }else{
            
            self.btnNext.backgroundColor = UIColor(red: 79/255, green: 199/255, blue: 0/255, alpha: 1.0)
            self.btnNext.setTitleColor(.white, for: .normal)
            self.btnNext.isEnabled = true
        }
        
        return updatedText.count <= 6
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        imgViewOfCircularGreenCheckMark.isHidden = true
        lblError.isHidden = true
        btnNext.isHidden = false
        
        if UIScreen.main.bounds.size.height <= 568{
            scrollView.isScrollEnabled = true
        }
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if UIScreen.main.bounds.size.height <= 568{
            scrollView.isScrollEnabled = false
        }
    }
}



extension MobileOTPVerificationVC: XMPPStreamDelegate {
    
    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
        
        print("userJIDValue",userJID)
        print("userPasswordValue",userPassword)
        print("serverValue",userPassword)

        
          do {
              try appDelegate.xmppController = XMPPController(hostName: server,
                                                              userJIDString: userJID,
                                                              password: userPassword)
              appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
              appDelegate.xmppController.connect()
              
          } catch {
              print("Something went wrong")
          }
      }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        // Move to chat controller
        print("Aunthenticated at viewcontroller")
        
        if (imageUrlValue.isEmpty){
            MBProgressHUD.hide(for: self.view, animated: true);
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let userProfileVC = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            self.navigationController?.pushViewController(userProfileVC, animated: true)
            
        }else if (self.interests.count) > 0 {
            MBProgressHUD.hide(for: self.view, animated: true);
            if (state.isEmpty){
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectLocationVC = storyboard.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                self.navigationController?.pushViewController(selectLocationVC, animated: true)
                
            }else{
                
                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
                    print("retriveState",retriveState)
                }
                //                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                //                let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as UIViewController
                //                self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
               /* let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                self.navigationController?.pushViewController(dashboardViewController, animated: true)*/
                self.openPopUpConditionally()
                self.loaderImageView.isHidden = true
                loaderImageView.layer.removeAllAnimations()
                //self.getAppStarterDataAPI()
            }
            
        }else{
            MBProgressHUD.hide(for: self.view, animated: true);
            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.UserAreaInterestViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
            let userAreaVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
            self.navigationController?.pushViewController(userAreaVC, animated: true)
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Wrong password or username")
    }
    
    
    func openPopUpConditionally(){
        
        appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)

        /*self.view.isUserInteractionEnabled = true
        let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
        
        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
            var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
            if(isMitra && show_issue_pass == 0){
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }else{

            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    appDelegate.isLaunchFirstTime = false
                    let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }else if(remaining>0){
                appDelegate.isLaunchFirstTime = false
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
            }else{
                if(myGreenPassCount>0){
                    appDelegate.isLaunchFirstTime = false
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                    self.navigationController?.pushViewController(dashboardViewController, animated: true)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }
            }
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
            self.navigationController?.pushViewController(dashboardViewController, animated: true)
        }*/
    }

    
    
    //API call to get Invite Summary
    func getAppStarterDataAPI(){
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginSourceName
        let params = AppstarterRequest.convertToDictionary(appstarterRequest:AppstarterRequest.init(app_key: URLConstant.app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.appStarterdata
        _ = AppStarterServices().getAppStarterData(apiURL,postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                    let model = userModel as! AppStarterResponse
                                                    print("model",model)
                                                    
                                                    let data = model.data[0]
                                                    let pendingInvite = data.pending_invitation_count
                                                    let myGreenPassCount = data.greenpass_count
                                                    
                                                    UserDefaults.standard.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
                                                    
                                                    UserDefaults.standard.set(myGreenPassCount, forKey:
                                                        StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                    
                                                    self.openPopUpConditionally()
                                                    
                                                    //let isMitra = data.isMitra
                                                    let defaults=UserDefaults.standard
                                                    defaults.set(pendingInvite, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                    defaults.set(myGreenPassCount, forKey: StringConstants.NSUserDefauluterKeys.MyGreenpassCount)
                                                    defaults.synchronize()
                                                    /*if pendingInvite! > 0{
                                                     if ((UserDefaults.standard.value(forKey: "isNotFirstTime")) != nil) && UserDefaults.standard.value(forKey: "isNotFirstTime") as! Bool == true{
                                                     print("not First time show congratulation")
                                                     UserDefaults.standard.set(false, forKey: "isNotFirstTime")
                                                     }else{
                                                     print("First time show congratulation")
                                                     UserDefaults.standard.set(true, forKey: "isNotFirstTime")
                                                     let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                                                     self.navigationController?.pushViewController(vc, animated: false)
                                                     }
                                                     }*/
                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }
    
    
}
