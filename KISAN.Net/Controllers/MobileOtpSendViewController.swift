//
//  MobileOtpSendViewController.swift
//  KISAN.Net
//
//  Created by Rushikant on 25/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import MBProgressHUD
import UIKit

class MobileOtpSendViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var kisanIdImageView: UIImageView!
    @IBOutlet weak var otpSendView: UIView!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var circularGreenCheckMark: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var privacyAndTermsLbl: UILabel!
    @IBOutlet weak var lblUnableSendOtpText: UILabel!
    @IBOutlet weak var viewOfBackNumberTextfield: UIView!
    @IBOutlet weak var loaderImageView: UIImageView!
    @IBOutlet weak var lblSentOTP: UILabel!
    
    static var otpReceived = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        setupPrivacyAndTermsLabel()
    }
    
    override func viewWillAppear(_ animated: Bool){
        circularGreenCheckMark.isHidden = true
        lblSentOTP.isHidden = true
        nextButton.isHidden = false
        privacyAndTermsLbl.isHidden = false
    }
    
    func initialDesign(){
        viewOfBackNumberTextfield.layer.cornerRadius = 5.0
        nextButton.layer.cornerRadius = 5
        
        self.nextButton.backgroundColor = UIColor(red: 0/255, green: 156/255, blue: 16/255, alpha: 1.0)
        
        self.nextButton.setTitleColor(UIColor(red: 89/255, green: 192/255, blue: 109/255, alpha: 1.0), for: .normal)
        
        self.nextButton.isEnabled = false
        
        if UIScreen.main.bounds.size.height <= 568{
            self.lblUnableSendOtpText.font = UIFont.systemFont(ofSize: 9)
        }else{
            self.lblUnableSendOtpText.font = UIFont.systemFont(ofSize: 11)
        }
    }
    
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0,1,2,3,4,5,6,7,8,9]{1}[0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("RushiNumber is ")
        let output = result ? "valid" : "Invalid"
        print("\(output)")
        return result
    }
    
    func setup(hidden: Bool) {
        nextButton.isHidden = hidden
        privacyAndTermsLbl.isHidden = hidden
        lblUnableSendOtpText.isHidden = !hidden
        loaderImageView.isHidden = !hidden
        circularGreenCheckMark.isHidden = !hidden
    }
    
    func setupPrivacyAndTermsLabel() {
        
        let partOneAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0, weight: .regular)]
        
        let partOne = NSMutableAttributedString(string: StringConstants.MobileOtpSendViewController.lbl_privacy_and_termsValue1, attributes: partOneAttributes)
        // let partOneSubPart = NSMutableAttributedString(string: "7th Dec. \n", attributes: partThreeAttributes)
        let partTwo = NSMutableAttributedString(string: StringConstants.MobileOtpSendViewController.lbl_privacy_and_termsValue2, attributes: partOneAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        // combination.append(partOneSubPart)
        combination.append(partTwo)

        self.privacyAndTermsLbl.attributedText = combination
        let strPrivacyPolicy = "Privacy policy"
        let strTerms = "Terms"
        
        let range1 = (privacyAndTermsLbl!.text! as NSString).range(of: strPrivacyPolicy)
        let range2 = (privacyAndTermsLbl!.text! as NSString).range(of: strTerms)
         combination.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
         combination.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.0
        combination.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, combination.length))
        self.privacyAndTermsLbl.attributedText = combination

        
        self.privacyAndTermsLbl.textAlignment = .center
        self.privacyAndTermsLbl.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.privacyAndTermsLbl.addGestureRecognizer(tapgesture)

    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.privacyAndTermsLbl.text else { return }
        let privacyPolicyRange = (text as NSString).range(of: "Privacy policy")
        let termsRange = (text as NSString).range(of: "Terms.")
        if gesture.didTapAttributedTextInLabel(label: self.privacyAndTermsLbl, inRange: privacyPolicyRange) {
            print("user tapped on Privacy policy")
            if let url = URL(string: URLConstant.privacyPolicyURL), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        } else if gesture.didTapAttributedTextInLabel(label: self.privacyAndTermsLbl, inRange: termsRange) {
            print("user tapped on Terms text")
            if let url = URL(string: URLConstant.termAndConditionURL), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
    }

    
    @IBAction func nxtButtonClick(_ sender: Any) {
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            self.setup(hidden: true)
            loaderImageView.transform = CGAffineTransform.identity
            loaderImageView.image = UIImage(named: "loader_image")
            circularGreenCheckMark.isHidden = true
            lblUnableSendOtpText.isHidden = true
            mobileNumberTextField.resignFirstResponder()
            
            if (mobileNumberTextField.text != "") {
                if (validate(value: mobileNumberTextField.text!)) {
                    
                    UIView.animate(withDuration: 2.0,
                                   delay: 0,
                                   options: [.repeat, .beginFromCurrentState],
                                   animations: { [weak self] in
                                    self?.loaderImageView.transform = CGAffineTransform(rotationAngle: CGFloat(360.0))
                    })
                    
                    circularGreenCheckMark.image = UIImage(named: "circularGreenCheckMark")
                    sendOtp(number: mobileNumberTextField.text!)
                } else {
                    
                    UIView.animate(withDuration: 2.0,
                                   delay: 0,
                                   options: [.beginFromCurrentState],
                                   animations: { [weak self] in
                                    self?.loaderImageView.transform = CGAffineTransform(rotationAngle: CGFloat(360.0))
                        },
                                   completion: { (completed: Bool) -> Void in
                                    self.loaderImageView.isHidden = true
                                    self.circularGreenCheckMark.isHidden = false
                                    self.circularGreenCheckMark.image = UIImage(named: "error_image")
                                    self.lblUnableSendOtpText.isHidden = false
                    }
                    )
                }
            } else {
                
                UIView.animate(withDuration: 2.0,
                               delay: 0,
                               options: [.beginFromCurrentState],
                               animations: { [weak self] in
                                self?.loaderImageView.transform = CGAffineTransform(rotationAngle: CGFloat(360.0))
                    },
                               completion: { (completed: Bool) -> Void in
                                self.loaderImageView.isHidden = true
                                self.circularGreenCheckMark.isHidden = false
                                self.circularGreenCheckMark.image = UIImage(named: "error_image")
                                self.lblUnableSendOtpText.isHidden = false
                }
                )
            }
        }else{
            self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }
    }
    
        func sendOtp(number: String?) {
    //        var  source = String()
    //        source = RequestName.loginExhibitorSourceName
            let mobileNumber = "+91\(number!)"
            let params = SendCustomMobileOtpRequest.convertToDictionary(
                SendCustomMobileOtpRequest:SendCustomMobileOtpRequest.init(
                    client_id : URLConstant.clientId,
                    client_secret :URLConstant.client_secret,
                    mobile :mobileNumber,
                    account_type:1,
                    source:"app"))
            print("Params", params)
            
            let apiURL = URLConstant.authBaseUrl + URLConstant.sendCustomOtp
            _ = SendCustomMobileOtpServices().sendOtp(
                apiURL,
                postData: params as [String : AnyObject],
                withSuccessHandler: { (userModel) in
                    let model = userModel as! SendCustomMobileOtpResponse
                    let data = model.data
                    let token = data["token"]! as! String
                    print("RuhsiToken-:\(token)")
                    self.setup(hidden: true)
                    self.lblSentOTP.isHidden = false
                    self.loaderImageView.isHidden = true
                    self.lblUnableSendOtpText.isHidden = true
                    self.circularGreenCheckMark.isHidden = false
                    MBProgressHUD.hide(for: self.view, animated: true);
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "MobileOTPVerificationVC") as! MobileOTPVerificationVC
                    dashboardViewController.token = token
                    dashboardViewController.userMobileNumber = mobileNumber
                    self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    
            }, withFailureHandlere: { (error: String) in
                self.loaderImageView.isHidden = true
                self.circularGreenCheckMark.isHidden = false
                self.circularGreenCheckMark.image = UIImage(named: "error_image")
                self.lblUnableSendOtpText.isHidden = false
                MBProgressHUD.hide(for: self.view, animated: true);
//                self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
//                    },{action2 in
//                    }, nil])
                print("error",error)
            })
        }
    
    //MARK: TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        self.setup(hidden: false)
        let currentText = mobileNumberTextField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText.count < 10 {
            self.nextButton.backgroundColor = UIColor(red: 0/255, green: 156/255, blue: 16/255, alpha: 1.0)
            self.nextButton.setTitleColor(UIColor(red: 89/255, green: 192/255, blue: 109/255, alpha: 1.0), for: .normal)
            self.nextButton.isEnabled = false
        }else{
            
            self.nextButton.backgroundColor = UIColor(red: 79/255, green: 199/255, blue: 0/255, alpha: 1.0)
            self.nextButton.setTitleColor(.white, for: .normal)
            self.nextButton.isEnabled = true
        }
        return updatedText.count <= 10
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setup(hidden: false)
        return true
    }
}
