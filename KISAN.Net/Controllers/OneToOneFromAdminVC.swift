//
//  OneToOneFromAdminVC.swift
//  KisanChat
//
//  Created by KISAN TEAM on 05/09/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices
import XMPPFramework
import ReverseExtension
import MBProgressHUD
import UserNotifications


class OneToOneFromAdminVC: UIViewController,UIGestureRecognizerDelegate,PassMultiMediaResponse,UIDocumentPickerDelegate{
    
    @IBOutlet weak var tblOneToOneChat: UITableView!
    @IBOutlet weak var txtViewOfChatMessage: UITextView!
    var navTitleName = String()
    @IBOutlet weak var btnSendMessage: UIButton!
    var lastChatBubbleY: CGFloat = 10.0
    var internalPadding: CGFloat = 8.0
    @IBOutlet weak var bottomOfmessageComposingView: NSLayoutConstraint!
    var ownerId = String()
    @IBOutlet weak var heightConstantOftxtViewOfChatMessage: NSLayoutConstraint!
    @IBOutlet weak var heightConstantOfViewBackChatTextview: NSLayoutConstraint!
    @IBOutlet weak var viewOfBackChatTextView: UIView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var bottomConstantOfViewbackOfChatTextView: NSLayoutConstraint!
    var userId = String()
    var ComingFrom = String()
    var communityJabberIDFromlistOfFollower = String()
    var FollowerUserId = String()
    var memberId = String()
    var userIdOfFollower = String()
    var oneToOneKeyFlag = String()
    var dominantColour = String()
    var arrOfSendMsgData = NSMutableArray()
    @IBOutlet weak var viewOfBackPopUp: UIView!
    @IBOutlet weak var tbtOfPopUp: UITableView!
    var arrOfMenuImages = [StringConstants.ImageNames.camera,StringConstants.ImageNames.image,StringConstants.ImageNames.video,StringConstants.ImageNames.audio,StringConstants.ImageNames.location,StringConstants.ImageNames.doc]
   // var arrOfAttachmentList = [StringConstants.popUpMenuName.camera,StringConstants.popUpMenuName.image,StringConstants.popUpMenuName.video,StringConstants.popUpMenuName.audio,StringConstants.popUpMenuName.location]
     var arrOfAttachmentList = [(NSLocalizedString(StringConstants.popUpMenuName.camera, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.image, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.video, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.audio, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.location, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.doc, comment: StringConstants.EMPTY))]
    
    var arrOfMenuList = [StringConstants.popUpMenuName.viewProfile,StringConstants.popUpMenuName.search,StringConstants.popUpMenuName.mute]
    var flagOfCheckPopup = String()
    var flagOfCheckLongPressRegonizer = Bool()
    var navView = UIView()
    var longPressIndexPath:NSIndexPath?
    var previouslongPressIndexPath:NSIndexPath?
    var flagOfClickOnMoreOption = Bool()
    var flagOfClickOnAttachmentOption = Bool()
    var imagePicker = UIImagePickerController()
    var imagePickerForOnlyImages = UIImagePickerController()
    var capturImage = UIImage()
    var videoURL: URL?
    var appDelegate = AppDelegate()
    lazy   var searchBar:UISearchBar = UISearchBar()
    var arrOfOriginalCopy = NSMutableArray()
    var btnCloseSearch = UIButton()
    var flagForCheckOpenSerachBar = Bool()
    var communityName = String()
    var communityKey = String()
    var communityImageSmallThumbUrl = String()
    var oneToOneCommunityKey = String()
    //var myChatsData = [MyChatsTable]()
    var myChatsData = MyChatsTable()
    var messageList = [Messages]()
    var messageListTemp = [Messages]()
    var filteredObjects: [Messages]?
    var imageData = Data()
    var messageKeyForSendMultiMedia = String()
    var messageTimeForSendMultiMedia = String()
    var originalMediaBucketName = String()
    var thumbMediaBucketName = String()
    let documentDirectoryHelper = DocumentDirectoryHelper()
    var mediaSize = Double()
    var thumbVideoData = Data()
     var createSendMessageRequestInDB = Dictionary<String, Any>()
    var S3UploadKeyNameWithImagePrefix = String()
    var S3UploadKeyNameWithThumbPrefix = String()
    var forwardedmessageDetails: Messages?

    @IBOutlet weak var btnLoadMore: UIButton!
    @IBOutlet weak var heightOfBtnMore: NSLayoutConstraint! //30
    @IBOutlet var lblOfLeaveMegToastView: UILabel!
    //For Pagination
    var isDataLoading:Bool = false
    var page_number : Int = 1
    var page_size : Int = 10
    var fetchOffSet :Int = 0
    @IBOutlet weak var btnNewMessage: UIButton!
    var isFirstRowIsVisible:Bool = false
    var pdfDocData = Data()
    var pdfThumbData = Data()
    var documentName = String()
    
    //PIP Mode
    var videoPlayer = PIPVideoPlayer()
    var strToggle = String()
    //  ------------------------------//
    private var playerView: PlayerView!
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    override var shouldAutorotate: Bool{
        return false
    }
    // -----------------------------//

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        
        //register a PIPViewCell
        self.tblOneToOneChat.register(UINib(nibName: "PIPViewCell", bundle:nil), forCellReuseIdentifier: "PIPViewCell")

        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        setReverseExtension()
        self.addKeyboardNotifications()
        flagOfCheckLongPressRegonizer = false
        flagForCheckOpenSerachBar = false
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        
        
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OneToOneFromAdminVC.dismissPopUp))
        tap.cancelsTouchesInView = false
        self.viewOfBackPopUp.addGestureRecognizer(tap)
     
        // set notification center for update the message list when message deleted from admin
        if ownerId != userId{
            NotificationCenter.default.addObserver(self, selector: #selector(self.refreshMessageFromNotification(_:)), name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.messageDeleted), object: nil)
        }
        print(myChatsData.isMember as Any)
        
        //Get UserId
        memberId = ownerId
        // Get Data from local db
        var messages = [Messages]()
        //self.oneToOneCommunityKey = StringConstants.EMPTY
        if ownerId == userId{
          
            if ComingFrom == StringConstants.flags.ChannelFollowerList{
                messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityJabberIDFromlistOfFollower)
                let myChatsList = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityJabberIDFromlistOfFollower)
                if myChatsList.count > 0 {
                    self.oneToOneCommunityKey = communityJabberIDFromlistOfFollower
                }else{
                    self.oneToOneCommunityKey = StringConstants.EMPTY
                }
                memberId = FollowerUserId
            }else{
                messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
                //   messages.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                if let  communityJabberId = myChatsData.communityJabberId {
                    self.oneToOneCommunityKey = communityJabberId
                }
                memberId = ownerId
            }
            messageList.removeAll()
            messageListTemp.removeAll()
            messageList = messages
            messageListTemp = messages
            
            if messages.count > 0 {
                // check local database
                self.isDataLoading = messages.count > 0 ? true : false
                self.page_number =   self.page_number + 1
                self.fetchOffSet =   self.fetchOffSet + 10
            }
            
            if messages.count < 10 {
                isDataLoading = false
            }
            
            //When Admin is blocked to follower, Then admin not allow to send message to this follower
            if myChatsData.isMember == StringConstants.ZERO {
                txtViewOfChatMessage.isHidden = true
                lblOfLeaveMegToastView.isHidden = false
                btnSendMessage.isHidden = true
            }else{
                txtViewOfChatMessage.isHidden = false
                lblOfLeaveMegToastView.isHidden = true
                btnSendMessage.isHidden = false
            }
        }else{
            // userCommunityJabberId  for oneToOne chat
            if let  userCommunityJabberId = myChatsData.userCommunityJabberId {
                self.oneToOneCommunityKey = userCommunityJabberId
                communityKey = self.oneToOneCommunityKey
            }/*else{
                let communityJabberId = myChatsData.communityKey! + "_" + userId
                self.oneToOneCommunityKey = communityJabberId
            }*/
            
            messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: self.oneToOneCommunityKey)
            //let myChatsList = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: self.oneToOneCommunityKey)
           /* if messages.count > 0 {
                communityKey = self.oneToOneCommunityKey
            }else{
                self.oneToOneCommunityKey = StringConstants.EMPTY
            }*/
            
            if myChatsData.isMember == StringConstants.ZERO {
                txtViewOfChatMessage.isHidden = true
                lblOfLeaveMegToastView.isHidden = false
                btnSendMessage.isHidden = true
            }else{
                txtViewOfChatMessage.isHidden = false
                lblOfLeaveMegToastView.isHidden = true
                btnSendMessage.isHidden = false
            }
            messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: oneToOneCommunityKey)
         //   messages.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
            
            if messages.count == 0 || messages.count == 1 {
                if !self.oneToOneCommunityKey.isEmpty {
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.getOldMessage(isCallFirstTime:true)
                }
            }else{
                messageList.removeAll()
                messageListTemp.removeAll()
                messageList = messages
                messageListTemp = messages
            }
            
            if messages.count > 1  { //messages.count > 0
                // check local database
                self.isDataLoading = messages.count > 0 ? true : false
                self.page_number =   self.page_number + 1
                self.fetchOffSet =   self.fetchOffSet + 10
            }
            
            if messages.count < 10 {
                isDataLoading = false
            }
        }
        
        
        //self.tblOneToOneChat.reloadData()
        self.setTblScrollAtBottom()
        intialiseBtnMore()
        hideBtnMore()
        
        // Receive messages from notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveMessages(_:)), name: NSNotification.Name(rawValue: "receivedMessagesForChannelOneToOne"), object: nil)
        
        //clear the all remote notification from notificatio tray
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        UIApplication.shared.cancelAllLocalNotifications()
        center.removeAllPendingNotificationRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if strToggle == StringConstants.flags.toggle{
            videoPlayer.addPlayerView(in:videoPlayer.videoView)
        }
    }
    
    @objc func receiveMessages(_ notification: NSNotification) {
        
        let messages = notification.userInfo?[StringConstants.NSUserDefauluterKeys.receivedMessages] as? Messages
        if messageList.contains(where: { $0.id == messages!.id }){
            print("Dont append")
        }else{
            // messageList.append(messages)
            if communityKey != messages?.communityId {
                print("Dont append")
            }else{
                messageList.insert(messages!, at: 0)
                messageListTemp = messageList
                if messageList.count < 10 {
                    isDataLoading = false
                }
            }
        }
        //For show new message button
        if userId == myChatsData.ownerId{
            btnNewMessage.isHidden = true
            setTblScrollAtBottom()
            self.tblOneToOneChat.reloadData()
        }else{
            if isFirstRowIsVisible == true{
                setTblScrollAtBottom()
                btnNewMessage.isHidden = true
                self.tblOneToOneChat.reloadData()
            }else{
                if communityKey != messages?.communityId {
                    btnNewMessage.isHidden = true
                }else{
                    btnNewMessage.isHidden = false
                }
            }
        }
    }
    
    func setReverseExtension()  {
        tblOneToOneChat.re.delegate = self
        tblOneToOneChat.re.scrollViewDidReachTop = { scrollView in
            print("scrollViewDidReachTop")
        }
        tblOneToOneChat.re.scrollViewDidReachBottom = { scrollView in
            print("scrollViewDidReachBottom")
        }
        tblOneToOneChat.estimatedRowHeight = 44
        tblOneToOneChat.rowHeight = UITableViewAutomaticDimension
    }
    func intialiseBtnMore()  {
        self.btnLoadMore.layer.cornerRadius = 6.0
        self.btnLoadMore.layer.masksToBounds = true
    }
    
    // ** Pavan
    func setBtnMore ()  {
        self.btnLoadMore.isHidden = false
        self.heightOfBtnMore.constant = 30.0
        self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
    }
    // ** Pavan
    func hideBtnMore() {
        self.heightOfBtnMore.constant = 0.0
        self.btnLoadMore.isHidden = true
    }
    // ** Pavan
    @IBAction func btnLoadMore(_ sender: Any) {
        //Local Pagination
        self.hideBtnMore()
      
       var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey:  self.oneToOneCommunityKey)
        if messages.count > 0 {
            
            let preveousCount = self.messageList.count
            self.isDataLoading = messages.count > 0 ? true : false
            self.page_number =   self.page_number + 1
            self.fetchOffSet =   self.fetchOffSet + 10
            
            for i in 0 ..< messages.count {
                let list = messages[i]
                self.isDataLoading = true
                if !messageList.contains(list){
                    self.messageList.append(list)
                    self.messageListTemp.append(list)
                }
            }
          //  self.messageList.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
          //  self.messageListTemp.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
            self.tblOneToOneChat.reloadData()
           // self.scrollToLoadMorePossition(messagesCount: preveousCount)
        }else{
            // check internet and fire api
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.getOldMessage(isCallFirstTime: false)
        }
    }
    
    @objc func refreshMessageFromNotification(_ notification: NSNotification) {
        
        // Logic for only deleted message delete from list
        var index = -1
        if let obj = notification.object as? [String:Any]{
            let noti_msg_id =  obj["id"] as! String
            let noti_community_id =  obj["communityId"] as! String
            for i in 0..<self.messageList.count {
                let model = self.messageList[i]
                let msgId = model.id ?? ""
                _ = model.communityId
                if msgId == noti_msg_id{ //&& noti_community_id == communityId{
                    index = i
                    break
                }
            }
            if index >= 0{
                if self.messageList.count > index{
                    self.messageList.remove(at: index)
                    if self.messageListTemp.count > index{
                        self.messageListTemp.remove(at: index) }
                    self.tblOneToOneChat.reloadData()
                    // delete from local database
                    let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: Messages(), messageId:noti_msg_id , communityKey: noti_community_id)
                    print(status)
//                    MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey: noti_community_id, messageId: noti_msg_id)
                }
            }else{
                let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: Messages(), messageId:noti_msg_id , communityKey: noti_community_id)
                print(status)
//                MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey: noti_community_id, messageId: noti_msg_id)
            }
        }
 
      /*  // Logic for whole data refresh
        isDataLoading = false
        page_number  = 1
        page_size  = 10
        fetchOffSet  =  0
        
        let messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
        // messages = messages.sorted(by: {$0.createdDatetime! < $1.createdDatetime!} )
        messageList.removeAll()
        messageListTemp.removeAll()
        messageList = messages
        messageListTemp = messages
        
        if messages.count > 0 {
            // check local database
            self.isDataLoading = messages.count > 0 ? true : false
            self.page_number =   self.page_number + 1
            self.fetchOffSet =   self.fetchOffSet + 10
        }
        
        if messages.count < 10 {
            isDataLoading = false
        }
        self.tblOneToOneChat.reloadData()
        self.setTblScrollAtBottom() */
    }
    
    func getOldMessage(isCallFirstTime:Bool){
        let params = ["communityKey":self.oneToOneCommunityKey]
        let currentTime = DateTimeHelper.getCurrentMillis()
        var simpleComunityKey = self.oneToOneCommunityKey
        if let dotRange = simpleComunityKey.range(of: "_") {
            simpleComunityKey.removeSubrange(dotRange.lowerBound..<simpleComunityKey.endIndex)
        }
        var apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + simpleComunityKey + URLConstant.getOldMessages2 + String(self.page_size)
        apiURL = apiURL + URLConstant.getOldMessages3 + String(self.page_number) + URLConstant.getOldMessages4 + self.oneToOneCommunityKey
        apiURL = apiURL + URLConstant.getOldMegOneToOneTimpStmp + String(currentTime)
        print("apiURL",apiURL)
        
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            
                                                            var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(self.fetchOffSet, communityKey: self.oneToOneCommunityKey)
                                                             let preveousCount = self.messageList.count
                                                            self.isDataLoading = messages.count > 0 ? true : false
                                                            self.page_number =  messages.count > 0 ? self.page_number + 1 : self.page_number
                                                            self.fetchOffSet =  messages.count > 0 ? self.fetchOffSet + 10 : self.fetchOffSet
                                                            for i in 0 ..< messages.count {
                                                                let list = messages[i]
                                                                if !self.messageList.contains(list){
                                                                    self.messageList.append(list)
                                                                    self.messageListTemp.append(list)
                                                                }
                                                            }
                                                           // self.messageList.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                                                           // self.messageListTemp.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            self.tblOneToOneChat.reloadData()
//                                                            if isCallFirstTime == true{
//                                                                self.setTblScrollAtBottom()
//                                                            }else{
//                                                                self.scrollToLoadMorePossition(messagesCount: preveousCount)
//                                                            }
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.isDataLoading = true
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
//    delete messages API (Brodcast messages) - https://middleware.kisanlab.com/webapi/communities/195be5cc-f62a-42c7-9285-2ba02dfe54da/messages/c466d3fe-59e1-400b-8f18-24fc4da6a93a
    
    func deleteMessage(messageDetails:Messages,indexValue:NSIndexPath){
        let params = ["communityKey":self.oneToOneCommunityKey]
        var simpleComunityKey = self.oneToOneCommunityKey
        if let dotRange = simpleComunityKey.range(of: "_") {
            simpleComunityKey.removeSubrange(dotRange.lowerBound..<simpleComunityKey.endIndex)
        }
        let apiURL = URLConstant.BASE_URL + URLConstant.deleteMessage1 + simpleComunityKey + URLConstant.deleteMessage2 + messageDetails.id!
        print("apiURL",apiURL)
        _ = DeleteMessageServices().deleteMessage(apiURL, postData: params as [String : AnyObject], withSuccessHandler: { (userModel) in
            let model = userModel as! DeleteMessageResponse
            print("model",model)
            self.deleteMessageLocally(indexValue:indexValue)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.view.makeToast(model.message)
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.isDataLoading = true
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    @objc func dismissPopUp() {
        self.view.endEditing(true)
        txtViewOfChatMessage.resignFirstResponder()
        viewOfBackPopUp.isHidden = true
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Save community key in session for mute sound of comming message
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey)
        //Show notification if user leave this screen
        UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Not allowed show message notification for on same screen
         if !self.oneToOneCommunityKey.isEmpty{
            UserDefaults.standard.set(self.oneToOneCommunityKey, forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne)
        }
        // Save community key in session for mute sound of comming message
        SessionManagerForSaveData.saveDataInSessionManager(valueString:communityKey , keyString: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey)
         UserDefaults.standard.set(true, forKey: "MessageDeleteStatus")
        // Set Navigation Bar
        self.setNavigationBarAtScreenLoadTime()
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        if userId == ownerId /*userIdOfFollower*/ {
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            //self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else {
          //  if dominantColour.isEmpty {
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                //self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
          /*}else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: dominantColour)
            }*/
        }
     
        //For banner message of channel leave
        if myChatsData.isMember == StringConstants.ONE{
        lblOfLeaveMegToastView.isHidden = true
        }else {
            lblOfLeaveMegToastView.isHidden = false
            txtViewOfChatMessage.isHidden = true
            btnSendMessage.setImage(UIImage(named: StringConstants.EMPTY), for: .normal)
            let image = UIImage(named:  StringConstants.ImageNames.baseline_delete_black_18dp)
            btnSendMessage.setBackgroundImage(image, for: .normal)
            btnSendMessage.isHidden = true
            lblOfLeaveMegToastView.text = (NSLocalizedString(StringConstants.Validation.noLongerFollower, comment: StringConstants.EMPTY))
        }
        
        if myChatsData.blockedTime == StringConstants.EMPTY || myChatsData.blockedTime == nil {
            lblOfLeaveMegToastView.isHidden = true
        }else{
            lblOfLeaveMegToastView.isHidden = false
            txtViewOfChatMessage.isHidden = true
            btnSendMessage.setImage(UIImage(named: StringConstants.EMPTY), for: .normal)
            let image = UIImage(named:  StringConstants.ImageNames.baseline_delete_black_18dp)
            btnSendMessage.setBackgroundImage(image, for: .normal)
            btnSendMessage.isHidden = true
            lblOfLeaveMegToastView.text = (NSLocalizedString(StringConstants.Validation.noLongerFollwerOfoneToOne, comment: StringConstants.EMPTY))
            navigationItem.rightBarButtonItems = []
        }
        //Set Image selected from camera
        setMultiMediaPart()

    }
    
    func setMultiMediaPart(){
       
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let size = CGSize(width: 0, height: 0)
        if appDelegate.selctedImage.size.width != size.width{
            
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            // Compress image
            let imageHelper = ImageHelper()
            imageData = imageHelper.compressImage(image: appDelegate.selctedImage, compressQuality: 0.4)!
            messageKeyForSendMultiMedia = strUUID
            messageTimeForSendMultiMedia = String(currentTime)
            //Send original image to Amzon S3
            originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia
            let uploadMediaPartToS3  = UploadMediaPartToS3()
            uploadMediaPartToS3.delegate = self
            //Save Image at docoument directory
            S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + imagePrefix + String(messageTimeForSendMultiMedia)
            let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
            documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imageData as NSData)
            uploadMediaPartToS3.uploadImage(with: imageData, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:true,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithImagePrefix)
        
        }else if appDelegate.selectedVideoURL != nil{
            
            let MyUrl = appDelegate.selectedVideoURL      
            let fileAttributes = try! FileManager.default.attributesOfItem(atPath: MyUrl!.path)
            let fileSizeNumber = fileAttributes[FileAttributeKey.size] as! NSNumber
            let fileSize = fileSizeNumber.int64Value
            var sizeMB = Double(fileSize / 1024)
            sizeMB = Double(sizeMB / 1024)
            print(String(format: "%.2f", sizeMB) + " MB")
            if sizeMB > 5{
                let imageHelper = ImageHelper()
                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4")
                imageHelper.compressVideo(inputURL: (appDelegate.selectedVideoURL)!, outputURL: compressedURL) { (exportSession) in
                    guard let session = exportSession else {
                        return
                    }
                    
                    switch session.status {
                    case .unknown:
                        break
                    case .waiting:
                        break
                    case .exporting:
                        break
                    case .completed:
                        guard let compressedData = NSData(contentsOf: compressedURL) else {
                            return
                        }
                        print("File size after compression: \(Double(compressedData.length / 1024)) kb")
                        
                        DispatchQueue.main.async {
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                            //let video = try NSData(contentsOf: self.appDelegate.selectedVideoURL!, options: .mappedIfSafe)
                            let video = compressedData
                            // Total Size in KB
                            self.mediaSize = Double(video.length) / 1024.0
                            print("mediaSize",self.mediaSize)
                            let imageThumb = imageHelper.getThumbnailImage(forUrl: self.appDelegate.selectedVideoURL!)
                            self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                            self.messageKeyForSendMultiMedia = strUUID
                            self.messageTimeForSendMultiMedia = String(currentTime)
                            //Send original image to Amzon S3
                            self.originalMediaBucketName = S3BucketName + channelMedia + self.communityKey + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                            let uploadMediaPartToS3  = UploadMediaPartToS3()
                            uploadMediaPartToS3.delegate = self
                            //Save Image at docoument directory
                            self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                            let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                            self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                            uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                        }
                        
                    case .failed:
                        break
                    case .cancelled:
                        break
                    }
                }
            }else{
                guard let compressedData = NSData(contentsOf: appDelegate.selectedVideoURL!) else {
                    return
                }
                let imageHelper = ImageHelper()
                DispatchQueue.main.async {
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    //let video = try NSData(contentsOf: self.appDelegate.selectedVideoURL!, options: .mappedIfSafe)
                    let video = compressedData

                    // Total Size in KB
                    print("File size after compression: \(Double(compressedData.length / 1024)) kb")
                    self.mediaSize = Double(video.length) / 1024.0
                    print("mediaSize",self.mediaSize)
                    let imageThumb = imageHelper.getThumbnailImage(forUrl: self.appDelegate.selectedVideoURL!)
                    self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                    self.messageKeyForSendMultiMedia = strUUID
                    self.messageTimeForSendMultiMedia = String(currentTime)
                    //Send original image to Amzon S3
                    self.originalMediaBucketName = S3BucketName + channelMedia + self.communityKey + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                    let uploadMediaPartToS3  = UploadMediaPartToS3()
                    uploadMediaPartToS3.delegate = self
                    //Save Image at docoument directory
                    self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                    let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                    self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                    uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                }
            }
            
        }else if appDelegate.captureAudioUrl != nil {
            
            do {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                let audio = try NSData(contentsOf: appDelegate.captureAudioUrl!, options: .mappedIfSafe)
                // Total Size in KB
                mediaSize = Double(audio.length) / 1024.0
                mediaSize = Double(audio.length) / 1024.0
                let formatter = NumberFormatter()
                formatter.numberStyle = .none
                formatter.maximumFractionDigits = 0
                let formattedAmount = formatter.string(from: mediaSize as NSNumber)!
                print(mediaSize)
                mediaSize = Double(formattedAmount)!
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original audio file to Amzon S3
                originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + audios + "/" + messageKeyForSendMultiMedia
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save audio file at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + audioPrefix + String(self.messageTimeForSendMultiMedia)
                let audioFileName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioFileName , sourceType: StringConstants.MediaType.audio,imageData:audio as NSData)
                uploadMediaPartToS3.uploadImage(with: audio as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.audio,callAgain:false,contentType:audioContentType, uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            } catch {
                print("Error")
            }
            
        }else if !(appDelegate.locationTitleAddress.isEmpty) && !appDelegate.locationDetailddress.isEmpty{
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            messageTimeForSendMultiMedia = String(currentTime)
            let strUUID = UUIDHelper.getUUID()
            let messageKey = strUUID
            var communityNameValue = String()
            var userName = String()
            if let firstName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName) {
                userName = firstName
            }
            if ownerId == userId{
                communityNameValue = communityName
            }else{
                communityNameValue = userName
            }
            
            
            var createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            print("createSendMessageRequest",createSendMessageRequest)
            
            createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            
            // Show message on same screen
            var  messages = Messages()
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
                if !oneToOneCommunityKey.isEmpty{
                    self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: strUUID, Communitykey: oneToOneCommunityKey)
                }
            }else{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            }
  
            //messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            setTblScrollAtBottom()
            tblOneToOneChat.reloadData()

            if !oneToOneCommunityKey.isEmpty{
                // Show message on same screen
                let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
                MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
                
                self.oneToOneChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityKeyVal: oneToOneCommunityKey)
            }else{
                createSendMessageRequest.updateValue(communityKey, forKey: "communityKey")
                print("createSendMessageRequest",createSendMessageRequest)
                self.sendMessage(createSendMessageRequestData: createSendMessageRequest, createSendMessageRequestInDB: createSendMessageRequestInDB, currentTime: Int64(messageTimeForSendMultiMedia)!,messageKey:messageKey)
            }
            
            // Make nill object
            appDelegate.strCaptionText = ""
            appDelegate.selctedImage = UIImage()
            appDelegate.selectedVideoURL = nil
            appDelegate.captureAudioUrl = nil
            appDelegate.locationTitleAddress = ""
            appDelegate.locationDetailddress = ""
            appDelegate.longitudeOfSendLocation = ""
            appDelegate.latitudeOfSendLocation = ""
        }
    }
    
    
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
        if callAgain == true{
            //Send Thumb image to Amzon S3
            let uploadMediaPartToS3  = UploadMediaPartToS3()
            uploadMediaPartToS3.delegate = self
            if mediaSource == StringConstants.MediaType.image{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: imageData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.video{
                
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + videos + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                uploadMediaPartToS3.uploadImage(with: thumbVideoData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:false,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithThumbPrefix)
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + ".pdf"
                uploadMediaPartToS3.uploadImage(with: pdfThumbData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:false,contentType:pdfContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.doc{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName = S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.doc,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }else if mediaSource == StringConstants.MediaType.docx{
                thumbMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + documents + "/" + messageKeyForSendMultiMedia + "/" + thumbnail
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                let uploadKeyName =  S3UploadKeyNameWithThumbPrefix + "." + mediaSource
                uploadMediaPartToS3.uploadImage(with: pdfDocData, bucketName:thumbMediaBucketName,mediaSource:StringConstants.MediaType.docx,callAgain:false,contentType:docContentType,uploadKeyName:uploadKeyName)
            }
            
        }else{
            
            let strUUID = UUIDHelper.getUUID()
            var strChatMessage = String()
            var mediaName = String()
            var mediaType = String()
            var contentField9 = String()
            if mediaSource == StringConstants.MediaType.image{
                let imageHelper = ImageHelper()
                // Total Size in KB
                mediaSize = imageHelper.getImageSize(imgData: imageData as NSData)
                print("mediaSize",mediaSize)
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                // Bucket name after adding Base URL & Image name
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + imagethumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.image
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.video{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp4"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + videoThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName = s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                mediaType = StringConstants.MediaType.video
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.audio{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.audio
                contentField9 = StringConstants.EMPTY
                strChatMessage = appDelegate.strCaptionText
            }else if mediaSource == StringConstants.MediaType.documentspdf {
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".pdf"
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + ".pdf"
                S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + pdfThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix + ".pdf"
                mediaType = StringConstants.MediaType.document
                contentField9 = pdf
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + ".pdf"
            }else if mediaSource == StringConstants.MediaType.doc{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }else if mediaSource == StringConstants.MediaType.docx{
                mediaName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + "." + mediaSource
                originalMediaBucketName = s3BaseURL + originalMediaBucketName + "/" + S3UploadKeyNameWithImagePrefix + "." + mediaSource
                //thumbMediaBucketName =  s3BaseURL + thumbMediaBucketName + "/" + S3UploadKeyNameWithThumbPrefix
                //S3UploadKeyNameWithThumbPrefix = S3UploadKeyName + docThumbPrefix + String(messageTimeForSendMultiMedia)
                thumbMediaBucketName =  StringConstants.EMPTY
                S3UploadKeyNameWithThumbPrefix = StringConstants.EMPTY
                mediaType = StringConstants.MediaType.document
                contentField9 = mediaSource
                strChatMessage = documentName
                self.S3UploadKeyNameWithImagePrefix = self.S3UploadKeyNameWithImagePrefix + "." + mediaSource
            }
            
            // Make nill object
            appDelegate.strCaptionText = ""
            appDelegate.selctedImage = UIImage()
            appDelegate.selectedVideoURL = nil
            appDelegate.captureAudioUrl = nil
            appDelegate.locationTitleAddress = ""
            appDelegate.locationDetailddress = ""
            appDelegate.longitudeOfSendLocation = ""
            appDelegate.latitudeOfSendLocation = ""
            
            var communityNameValue = String()
            var userName = String()
            if let firstName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName) {
                userName = firstName
            }
            if ownerId == userId{
                communityNameValue = communityName
            }else{
                communityNameValue = userName
            }
            
            MBProgressHUD.hide(for: self.view, animated: true);
            var createSendMessageRequest  = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: StringConstants.EMPTY, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: String(mediaSize), contentField9: contentField9, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            print("createSendMessageRequest",createSendMessageRequest)
            
            createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKeyForSendMultiMedia, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: originalMediaBucketName, contentField3: thumbMediaBucketName, contentField4: mediaName, contentField5: StringConstants.EMPTY, contentField6: S3UploadKeyNameWithImagePrefix, contentField7: S3UploadKeyNameWithThumbPrefix, contentField8: String(mediaSize), contentField9: contentField9, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: messageTimeForSendMultiMedia, hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            //send message at server DB
            if !oneToOneCommunityKey.isEmpty{
                self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: strUUID, Communitykey: oneToOneCommunityKey)
            }
            // Show message on same screen
            let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
          //  messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            setTblScrollAtBottom()
            tblOneToOneChat.reloadData()

            if !oneToOneCommunityKey.isEmpty{
                // Show message on same screen
                let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
                MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
                self.oneToOneChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityKeyVal: oneToOneCommunityKey)
            }else{
                createSendMessageRequest.updateValue(communityKey, forKey: "communityId")
                createSendMessageRequestInDB.updateValue(communityKey, forKey: "communityId")
                print("createSendMessageRequest",createSendMessageRequest)
                self.sendMessage(createSendMessageRequestData: createSendMessageRequest, createSendMessageRequestInDB: createSendMessageRequestInDB, currentTime: Int64(messageTimeForSendMultiMedia)!,messageKey:strUUID)
            }
        }
    }
    
    
    func PassMultiMediaFail(_ errorMsg: String) {
        MBProgressHUD.hide(for: self.view, animated: true);
        self.popupAlert(title: "", message: errorMsg, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
            }, nil])
    }

    func setTblScrollAtBottom()  {
        
     /*   let numSections = tblOneToOneChat.numberOfSections
        var contentInsetTop = tblOneToOneChat.bounds.size.height
        for section in 0..<numSections {
            let numRows = tblOneToOneChat.numberOfRows(inSection: section)
            let sectionHeaderHeight = tblOneToOneChat.rectForHeader(inSection: section).size.height
            let sectionFooterHeight = tblOneToOneChat.rectForFooter(inSection: section).size.height
            contentInsetTop -= sectionHeaderHeight + sectionFooterHeight
            for i in 0..<numRows {
                let rowHeight = tblOneToOneChat.rectForRow(at: IndexPath(item: i, section: section)).size.height
                contentInsetTop -= rowHeight
                if contentInsetTop <= 0 {
                    contentInsetTop = 0
                    break
                }
            }
            // Break outer loop as well if contentInsetTop == 0
            if contentInsetTop == 0 {
                break
            }
        }
        tblOneToOneChat.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)*/
        
        let when = DispatchTime.now() + 0.000001 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.messageList.count > 0 {
                self.scrollToBottom()
            }
        }
        
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        txtViewOfChatMessage.returnKeyType = UIReturnKeyType.next
        
        tblOneToOneChat.rowHeight = UITableViewAutomaticDimension
        tblOneToOneChat.estimatedRowHeight = 85.0
        tblOneToOneChat.tableFooterView = UIView()
        tblOneToOneChat.separatorColor = UIColor.clear
        
        txtViewOfChatMessage.text = (NSLocalizedString(StringConstants.CommunityType.txtPlaceHolderForTypeMessage, comment: StringConstants.EMPTY))
        txtViewOfChatMessage.textColor = UIColor.lightGray
        txtViewOfChatMessage.selectedTextRange = txtViewOfChatMessage.textRange(from: txtViewOfChatMessage.beginningOfDocument, to: txtViewOfChatMessage.beginningOfDocument)
        btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGrayBtn), for: .normal)
        btnSendMessage.isEnabled = false
        self.tblOneToOneChat.scrollsToTop = false
        setTblScrollAtBottom()
        tbtOfPopUp.separatorColor = UIColor.clear
        
        // long press recognizer
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.tblOneToOneChat.addGestureRecognizer(longPressGesture)
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)), style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtViewOfChatMessage.inputAccessoryView = toolbar
        self.searchBar.inputAccessoryView = toolbar
        
        if  UIScreen.main.bounds.size.height <= 568{
            lblOfLeaveMegToastView.font = UIFont.systemFont(ofSize: 12)
        }else{
            lblOfLeaveMegToastView.font = UIFont.systemFont(ofSize: 15)
        }
    }
    
    func setNavigationBarAtScreenLoadTime()  {
        // Create a navView to add to the navigation bar
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        navView.isHidden = false
        if ownerId == userId {
            if UIScreen.main.bounds.size.height <= 568{
                navView = UIView(frame: CGRect(x: 0, y: 0, width: 205, height: 50))
            }else{
                navView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
            }
        }else{
            navView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
        }
        // Create the image view
        var imageOfNavTitle = UIImageView()
        imageOfNavTitle = UIImageView(frame: CGRect(x:-10, y:5, width: 35, height: 35))
        
        if let channelProfilePic = myChatsData.communityImageSmallThumbUrl {
            if channelProfilePic == StringConstants.EMPTY{
                if myChatsData.feed == StringConstants.ONE{
                    imageOfNavTitle.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderRSSfeedImg)
                }else{
                    imageOfNavTitle.image = UIImage(named: StringConstants.ImageNames.communityPlaceholderImg)
                }
            }else{
                let imgurl = channelProfilePic
                let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                imageOfNavTitle.sd_setImage(with: URL(string: rep2), placeholderImage:UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
            }
        }
        
        
        imageOfNavTitle.layer.cornerRadius = imageOfNavTitle.frame.height / 2
        imageOfNavTitle.clipsToBounds = true
        // Create the label
        var lblOfNavTitle = UILabel()
        if ownerId == userId {
            if UIScreen.main.bounds.size.height <= 568{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 102 , height: 21)) //w = 102
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
            }else{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 150, height: 21))
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(16)
            }
            let name = myChatsData.communityName
            lblOfNavTitle.text =  StringUTFEncoding.UTFEncong(string:name!)
        }else{
            lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 150, height: 21))
            lblOfNavTitle.font = lblOfNavTitle.font.withSize(16)
            lblOfNavTitle.text = StringUTFEncoding.UTFEncong(string:communityName)
        }
        lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
        lblOfNavTitle.textColor = UIColor.white
        lblOfNavTitle.textAlignment = NSTextAlignment.left
        navView.addSubview(lblOfNavTitle)
        navView.addSubview(imageOfNavTitle)
        // Set the navigation bar's navigation item's titleView to the navView
        self.navigationItem.titleView = navView
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OneToOneFromAdminVC.navTitleClick))
        navView.addGestureRecognizer(tap)
      
        //  if ownerId == userId {
        let moreOptionButton   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.moreOptions), style: .plain, target: self, action: #selector(btnMoreOptionClick))
        let attachMentButton = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.attachFile), style: .plain, target: self, action: #selector(btnAttachmentClick))
        moreOptionButton.tintColor = UIColor.white
        attachMentButton.tintColor = UIColor.white
        //removed more option for phase 1
        navigationItem.rightBarButtonItems = [/*moreOptionButton,*/attachMentButton]
        
        
        
        if myChatsData.feed == StringConstants.ONE{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            if let communityDominantColour = myChatsData.communityDominantColour {
                print("communityDominantColour",communityDominantColour)
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
            
            }else if  !(self.dominantColour == ""){
                print("communityDominantColour",dominantColour)
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: dominantColour)
                
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
             
            }
        }
        
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    @objc func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.tblOneToOneChat)
        let indexPath = self.tblOneToOneChat.indexPathForRow(at: p)
        viewOfBackPopUp.isHidden = true
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            print("Long press on row, at \(indexPath!.row)")
            flagOfCheckLongPressRegonizer = true
            longPressIndexPath = indexPath! as NSIndexPath
            forwardedmessageDetails = messageList[(longPressIndexPath?.row)!]
            let strMessageType =  forwardedmessageDetails?.messageType
            var strMediaType = forwardedmessageDetails?.mediaType
            let cell = tblOneToOneChat.cellForRow(at:indexPath!)
            cell!.backgroundColor = UIColor.gray
            if previouslongPressIndexPath == nil{
                
            }else{
                let cellRect = tblOneToOneChat.rectForRow(at: previouslongPressIndexPath! as IndexPath)
                let completelyVisible = tblOneToOneChat.bounds.contains(cellRect)
                if completelyVisible || (self.isRowZeroVisible(indexPath: previouslongPressIndexPath! as IndexPath)){
                    let cell1 = tblOneToOneChat.cellForRow(at:previouslongPressIndexPath! as IndexPath)
                    cell1!.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
            }
            previouslongPressIndexPath = longPressIndexPath
            //tblOneToOneChat.reloadData()
            hideSearchBar()
            setNavigationBarAtScreenLoadTime()
            navigationItem.rightBarButtonItems = []
            //self.addFuncOnNavForSelectedText(strMessageType: strMessageType!)
            if strMediaType == nil{
                strMediaType = "Location"
            }
            self.addFuncOnNavForSelectedText(strMessageType: strMessageType!,strMediaType:strMediaType!)
        }
    }
    
    
    
    //for checking the previously selected row is visible or not
    func isRowZeroVisible(indexPath:IndexPath) -> Bool {
        let indexes = tblOneToOneChat.indexPathsForVisibleRows
        for index in indexes ?? [] {
            if index == indexPath {
                return true
            }
        }
        return false
    }
    
    func addFuncOnNavForSelectedText(strMessageType:String,strMediaType:String) {
        navView.isHidden = true
        let messageInfo = messageList[(longPressIndexPath?.row)!]
        let btnHelp   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.helpNav), style: .plain, target: self, action: #selector(btnHelpClick))
        let btnCopy = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.copy), style: .plain, target: self, action: #selector(btnCopyClick))
        let btnForword   = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.forword), style: .plain, target: self, action: #selector(btnForwordClick))
        let btnDelete = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.delete), style: .plain, target: self, action: #selector(btnDeleteClick))
        btnHelp.tintColor = UIColor.white
        btnCopy.tintColor = UIColor.white
        btnForword.tintColor = UIColor.white
        btnDelete.tintColor = UIColor.white
        
      /*  if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.text{
            navigationItem.rightBarButtonItems = [/*btnHelp,*/btnCopy,btnForword,btnDelete]
        }
        else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.image{
            navigationItem.rightBarButtonItems = [/*btnHelp*/btnForword,btnDelete]
        }
        else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == ""{
            navigationItem.rightBarButtonItems = [/*btnHelp,*/btnCopy,btnForword,btnDelete]
        }
        else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.video{
            navigationItem.rightBarButtonItems = [/*btnHelp*/btnForword,btnDelete]
        }
        else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.audio{
            navigationItem.rightBarButtonItems = [/*btnHelp*/btnForword,btnDelete]
        }
        else{
            navigationItem.rightBarButtonItems = [/*btnHelp*/btnForword,btnDelete]
        }*/
        
        if strMessageType == StringConstants.MessageType.chat && strMediaType == StringConstants.EMPTY{
            navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,*/btnForword,btnDelete]
        }else{
            if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.image{
                
                let messageId = messageInfo.id
                let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                print(isExist)
                if isExist{
                    print("yes exist")
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                }else{
                    if messageInfo.senderId == userId {
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                        print("yes exist same user")
                    }else{
                        print("not downloaded")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                    }
                }
            }
                
            else if strMessageType == StringConstants.MessageType.chatMessage && (strMediaType  == StringConstants.EMPTY || strMediaType == StringConstants.MediaType.text){
                navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,*/btnCopy,btnForword,btnDelete]
            }
            else if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.audio{
                //new code for undownloaded media
                let messageId = messageInfo.id
                let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
                print(isExist)
                if isExist{
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    print("yes exist")
                }else{
                    if messageInfo.senderId == userId {
                        print("yes exist same user")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    }else{
                        print("not downloaded")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                    }
                }
            }
            else  if strMessageType == StringConstants.MessageType.chatMessage && strMediaType == StringConstants.MediaType.video{
                
                let messageId = messageInfo.id
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                print("cellpath:,\(videoNameFrmDB)")
                
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
                print(isExist)
                if isExist{
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                }else{
                    if messageInfo.senderId == userId {
                        print("yes exist same user")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    }else{
                        print("not downloaded")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                    }
                }
            }else if messageInfo.mediaType == StringConstants.MediaType.document{
                
                let messageId = messageInfo.id
                let contentField9 =  messageInfo.contentField9!
                var imageName1 = String()
                if  contentField9 == doc {
                    imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                }else{
                    imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
                }
                print("imageName1",imageName1)
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                print("isExist",isExist)
                if isExist{
                    navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    print("yes exist")
                }else{
                    if messageInfo.senderId == userId {
                        print("yes exist same user")
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
                    }else{
                        navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,btnForword,*//*btnDelete*/]
                        print("not downloaded")
                    }
                }
                
            }else{
                navigationItem.rightBarButtonItems = [/*btnHideUnHide,btnHelp,btnCopy,*/btnForword,btnDelete]
            }
        }
        
        
    }
    
    @objc func btnHelpClick(sender: UIBarButtonItem) {
        print("btnHelpClick")
        let forwordMessageVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageInfoVC") as! MessageInfoVC
        self.navigationController?.pushViewController(forwordMessageVC, animated: true)
    }
    
    @objc func btnCopyClick(sender: UIBarButtonItem) {
        print("btnCopyClick")
        let board = UIPasteboard.general
        let messageData = messageList[(longPressIndexPath?.row)!]
        board.string = messageData.contentField1
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil
        self.tblOneToOneChat.reloadData()
        self.setNavigationBarAtScreenLoadTime()
    }
   
    @objc func btnForwordClick(sender: UIBarButtonItem) {
        print("btnForwordClick")
        let indxValue = self.longPressIndexPath
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil
        flagOfCheckLongPressRegonizer = false
        let indexPath = IndexPath(item: (indxValue?.row)!, section: 0)
        tblOneToOneChat.reloadRows(at: [indexPath], with: .none)
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        dashboardViewController.commingFrom = StringConstants.flags.oneToOneFromAdminVC
        dashboardViewController.forwardedmessageData = forwardedmessageDetails
        self.navigationController?.pushViewController(dashboardViewController, animated: false)
    }
    
    @objc func btnDeleteClick(sender: UIBarButtonItem) {
        print("btnDeleteClick")
        popupAlert(title: "", message:(NSLocalizedString(StringConstants.AlertMessage.deleteMsgConfirmation, comment: StringConstants.EMPTY)) , actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
                if self.ownerId == self.userId {  // Admin
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.deleteMessage(messageDetails: self.messageList[(self.longPressIndexPath?.row)!], indexValue: self.longPressIndexPath!)
                }else{
                    self.deleteMessageLocally(indexValue: self.longPressIndexPath!)
                }
            }, nil])
    }
    
    func deleteMessageLocally(indexValue:NSIndexPath)  {
        // *Pavan  Deleted message locally
        self.longPressIndexPath = nil
        self.previouslongPressIndexPath = nil
        let messageInfo = self.messageList[indexValue.row]
//        MyChatServices.sharedInstance.isMessageExistInMyChatByCommunityKey(communityKey: messageInfo.communityId!, messageId: messageInfo.id!)
        let status = MessagesServices.sharedInstance.deleteMessageFromMessageTable(messageDetails: self.messageList[indexValue.row], messageId: messageInfo.id!, communityKey: messageInfo.communityId!)
        print(status)

        if self.messageList.count > indexValue.row  {
        self.messageList.remove(at: indexValue.row)
        self.messageListTemp.remove(at: indexValue.row)
        }
        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.deleteConfirmation, comment: StringConstants.EMPTY)))
       // self.setTblScrollAtBottom()
        self.setNavigationBarAtScreenLoadTime()
        self.tblOneToOneChat.reloadData()
//        _ = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
//            self.tblOneToOneChat.reloadData()
//        }
    }
    
    
    @objc func navTitleClick() {
        print("navTitleClick")
        viewOfBackPopUp.isHidden = true
        if ownerId == userId{
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let showUserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "ShowUserProfileVC") as! ShowUserProfileVC
            showUserProfileVC.myChatsData = myChatsData
            showUserProfileVC.commingFrom = StringConstants.flags.oneToOneFromAdminVC
            self.navigationController?.pushViewController(showUserProfileVC, animated: true)
        }else{
            let channelProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
            channelProfileVC.myChatsData = myChatsData
            self.navigationController?.pushViewController(channelProfileVC, animated: true)
        }
    }

    @objc func btnMoreOptionClick(sender: UIBarButtonItem) {
        print("More")

        flagOfClickOnAttachmentOption = false
        if flagOfClickOnMoreOption == false{
            flagOfClickOnMoreOption = true
            viewOfBackPopUp.isHidden = false
            flagOfCheckPopup = StringConstants.flags.moreoptions
            txtViewOfChatMessage.resignFirstResponder()
            tbtOfPopUp.reloadData()
        }else{
            flagOfClickOnMoreOption = false
            viewOfBackPopUp.isHidden = true
        }
    }
    
    
    @objc func btnAttachmentClick(sender: UIBarButtonItem) {
        print("Attach")
        //Hide pip mode video
        if strToggle == StringConstants.flags.toggle{
            strToggle = StringConstants.EMPTY
            videoPlayer.handleCloseTapped((Any).self)
        }
        
        if flagOfClickOnAttachmentOption == false{
            viewOfBackPopUp.isHidden = false
            flagOfClickOnAttachmentOption = true
            flagOfCheckPopup = StringConstants.flags.attachment
            txtViewOfChatMessage.resignFirstResponder()
            tbtOfPopUp.reloadData()
        }else{
            viewOfBackPopUp.isHidden = true
            flagOfClickOnAttachmentOption = false
        }
        flagOfClickOnMoreOption = false
    }
    
    func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(OneToOneFromAdminVC.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OneToOneFromAdminVC.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK:- Notification
    @objc func keyboardWillShow(_ notification: Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //self.buttomLayoutConstraint = keyboardFrame.size.height
            self.bottomOfmessageComposingView.constant = keyboardFrame.size.height
            let when = DispatchTime.now() + 0.001 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.messageList.count > 0 {
                    self.setTblScrollAtBottom()
                }
            }
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
        })
    }
    
    func scrollToBottom(){
        
        let indexPath = IndexPath(row: self.messageList.count - 1, section: 0)
        if self.messageList.count > indexPath.row {
           // self.tblOneToOneChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
            self.tblOneToOneChat.re.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    func scrollToLoadMorePossition(messagesCount:Int){
        let count  = self.messageList.count - messagesCount
        if count > 0 {
            if self.messageList.count > count - 1  {
                let indexPath = IndexPath(row: count - 1, section: 0)
               // self.tblOneToOneChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
                self.tblOneToOneChat.re.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.bottomOfmessageComposingView.constant = 0.0
        }, completion: { (completed: Bool) -> Void in
            // self.moveToLastMessage()
           // self.setTblScrollAtBottom()

        })
    }
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
         UserDefaults.standard.set(false, forKey: "MessageDeleteStatus")
        self.navigationController?.popViewController(animated: true)
        
        //Hide pip mode video
        if strToggle == StringConstants.flags.toggle{
            strToggle = StringConstants.EMPTY
            videoPlayer.handleCloseTapped((Any).self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnSendMessageClick(_ sender: Any) {
        let messagetext = txtViewOfChatMessage.text
        let messagetextfinal = messagetext!.trimmingCharacters(in: .whitespaces)
        txtViewOfChatMessage.text = txtViewOfChatMessage.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let strChatMessage = txtViewOfChatMessage.text
        
        if (txtViewOfChatMessage.text != (NSLocalizedString(StringConstants.CommunityType.txtPlaceHolderForTypeMessage, comment: StringConstants.EMPTY)))  && !txtViewOfChatMessage.text.isEmpty && !(messagetextfinal == StringConstants.EMPTY){
            let text = txtViewOfChatMessage.text
            let types: NSTextCheckingResult.CheckingType = .link
            let detector = try? NSDataDetector(types: types.rawValue)
            guard let detect = detector else {
                return
            }
            
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                let matches = detect.matches(in: text!, options: .reportCompletion, range: NSMakeRange(0, text!.characters.count))
                if matches.count > 0{
                    var urlString = String()
                    for match in matches {
                        print(match.url!)
                        urlString = match.url!.absoluteString
                    }
                    if let youTubeId = getYoutubeId(youtubeUrl: urlString) {
                        if youTubeId == StringConstants.EMPTY{
                            //If link is not a youtube link
                            //send Normal Message
                            self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                        }else{
                            //call API
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                            self.getDetailsFromYouTubeAPI(youTubeId:youTubeId,strChatMessage:strChatMessage!)
                        }
                    }
                }else{
                    //send Normal Message
                     //if the message send by user does not contains a link
                    self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                }
            }else{
                //offline mode send link message as a text message
                //send Normal Message
                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage!,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
            }
        }
    }
    
    
    func sendTextMessageWithUrlMessage(strChatMessage:String,mediaType:String,contentField2:String,contentField3:String,contentField4:String){
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        // let messageKey = String(currentTime) + "-" + strUUID
        let messageKey = strUUID
        
        var communityNameValue = String()
        var userName = String()
        if let firstName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName) {
            userName = firstName
        }
        if ownerId == userId{
            communityNameValue = communityName
        }else{
            communityNameValue = userName
        }
        
        if !self.oneToOneCommunityKey.isEmpty{
            let createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            print("createSendMessageRequest",createSendMessageRequest)
            
            createSendMessageRequestInDB  = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4:contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: oneToOneCommunityKey, jabberId: oneToOneCommunityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            print("createSendMessageRequestInDB",createSendMessageRequestInDB)
            // Show message on same screen
            //let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            var  messages = Messages()
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
                self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: messageKey, Communitykey: oneToOneCommunityKey)
            }else{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            }
            
            //  messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            self.tblOneToOneChat.reloadData()
            setTblScrollAtBottom()
            MBProgressHUD.hide(for: self.view, animated: true);
            //Save message in core data as local DB with pending status
            let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
            print("messageStatusTime",messagesDB.messageStatusTime!)
            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
            self.oneToOneChat(createSendMessageRequest: createSendMessageRequest, messageGuid: messageKey, communityKeyVal:oneToOneCommunityKey)
            
            txtViewOfChatMessage.text = StringConstants.EMPTY
            heightConstantOftxtViewOfChatMessage.constant = 33
            heightConstantOfViewBackChatTextview.constant = 53
            self.view.layoutIfNeeded()
            
        }else{
            // let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            //spinnerActivity.isUserInteractionEnabled = false
            let createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: communityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            createSendMessageRequestInDB  = buildRequestForSendMessage(id: messageKey, mediaType: mediaType, messageType: StringConstants.MessageType.chatMessage, contentField1: strChatMessage, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: StringConstants.EMPTY, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: communityKey, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending,communityName:communityNameValue,communityProfileUrl:myChatsData.communityImageBigThumbUrl!)
            
            // Show message on same screen
            //let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            var  messages = Messages()
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
            }else{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            }
            MBProgressHUD.hide(for: self.view, animated: true);

            // messageList.append(messages)
            messageList.insert(messages, at: 0)
            print("messageList count",messageList.count)
            messageListTemp = messageList
            self.tblOneToOneChat.reloadData()
            setTblScrollAtBottom()
            self.sendMessage(createSendMessageRequestData: createSendMessageRequest, createSendMessageRequestInDB: createSendMessageRequestInDB,currentTime:currentTime,messageKey:messageKey)
            
            txtViewOfChatMessage.text = StringConstants.EMPTY
            heightConstantOftxtViewOfChatMessage.constant = 33
            heightConstantOfViewBackChatTextview.constant = 53
            self.view.layoutIfNeeded()
        }
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        let url = youtubeUrl
        var youtubrID = String()
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            if ((match) != nil){
                let range = match?.range(at: 0)
                youtubrID = (url as NSString).substring(with: range!)
                print(youtubrID)
            }else{
                youtubrID = StringConstants.EMPTY
            }
        } catch {
            youtubrID = StringConstants.EMPTY
        }
        return youtubrID
    }
    
    func getDetailsFromYouTubeAPI(youTubeId:String,strChatMessage:String) {
        let params = ["":""]
        let baseUrl = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + youTubeId + "&key=AIzaSyARI7c1_rmH9yN6N8_dqgWtcOAcg6AGvcs"
        let apiURL = String(describing: baseUrl)
        _ = GetYoutubeurlDataService().getyoutubeurldata(apiURL,
                                                         postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetYoutubeurlDataResponse
                                                            let youTubeData = model.Youtubedata as [YoutubeGetData]
                                                            
                                                            if youTubeData.count > 0 {
                                                                let datadict = youTubeData[0]
                                                                print(datadict.title as Any)
                                                                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage,mediaType:StringConstants.MediaType.link,contentField2:datadict.thumnailImgUrl1!,contentField3:datadict.title!,contentField4:datadict.descreption!)
                                                            }else{
                                                                self.sendTextMessageWithUrlMessage(strChatMessage:strChatMessage,mediaType:StringConstants.EMPTY,contentField2:StringConstants.EMPTY,contentField3:StringConstants.EMPTY,contentField4:StringConstants.EMPTY)
                                                                
                                                            }
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func sendMessage(createSendMessageRequestData:Dictionary<String, Any>,createSendMessageRequestInDB:Dictionary<String, Any>,currentTime:Int64,messageKey:String){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        var membersList = [String]()
        membersList.append(ownerId)
        let firstName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        let lastName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        let username = firstName + StringConstants.singleSpace + lastName
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
        let country = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.country)
        let imageBigthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
        let imageSmallthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
        let imageUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageUrl)
        let userDetails = CreateOneToOneChatRequest.convertToDictionary(createOneToOneChatRequest: CreateOneToOneChatRequest.init(username: username, accountType: Int(accountType)!, city: city, country: country, imageBigthumbUrl: imageBigthumbUrl, imageSmallthumbUrl: imageSmallthumbUrl, imageUrl: imageUrl))
        let currentTime = DateTimeHelper.getCurrentMillis()
        var oneToOneKey = String()
        if ComingFrom == StringConstants.flags.ChannelFollowerList{
            oneToOneKey = StringConstants.flags.FromAdmin
        }else{
            oneToOneKey = RequestName.loginSourceName + String(currentTime)
        }
        let params = CreateOneToOneChatDetailRequest.convertToDictionary(CreateOneToOneChatDetailRequest: CreateOneToOneChatDetailRequest.init(userDetails: userDetails, communityKey: communityKey, oneToOneKey: oneToOneKey, memberId: self.memberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.usersCurrentOnetoOne
        _ = CreateOneToOneChatServices().createOneToOneChat(apiURL,
                                                            postData: params as [String : AnyObject],
                                                            withSuccessHandler: { (userModel) in
                                                                let model = userModel as! MyChatListResponse
                                                                let message = model.message
                                                                self.oneToOneCommunityKey = model.communityKey!
                                                                print("message",message ?? (Any).self)
                                                                //Update value of userCommunityJabberId after oneToone chat init
                                                                self.communityKey = self.oneToOneCommunityKey
                                                                MBProgressHUD.hide(for: self.view, animated: true);

                                                             SessionDetailsForCommunityType.shared.globalMyChatsData.setValue(self.oneToOneCommunityKey, forKey: "userCommunityJabberId")
                                                                
                                                                //Save message in Message Tbl of local device
                                                                var createSendMesgRequestInDB = createSendMessageRequestInDB
                                                                createSendMesgRequestInDB.updateValue(self.oneToOneCommunityKey, forKey: "communityId")
                                                                createSendMesgRequestInDB.updateValue(self.oneToOneCommunityKey, forKey: "jabberId")
                                                                createSendMesgRequestInDB.updateValue(self.oneToOneCommunityKey, forKey: "userCommunityJabberId")
                                                                self.createSendMessageRequestInDB.removeAll()
                                                                self.createSendMessageRequestInDB = createSendMesgRequestInDB
                                                                print("self.createSendMessageRequestInDB",self.createSendMessageRequestInDB)
                                                                let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMesgRequestInDB, saveDataAtDB: true)
                                                                self.txtViewOfChatMessage.text = StringConstants.EMPTY
                                                                self.tblOneToOneChat.reloadData()
                                                                print("messagesDB.communityId",messagesDB.communityId!)
                                                                //Save message in core data as local DB with pending status
                                                                MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
                                                                
                                                                //Change communityId & communityJabberId for send message to ejjaberd
                                                                var createOneToOneSendMessageRequest = createSendMessageRequestData
                                                                createOneToOneSendMessageRequest.updateValue(self.oneToOneCommunityKey, forKey: "communityId")
                                                                createOneToOneSendMessageRequest.updateValue(self.oneToOneCommunityKey, forKey: "jabberId")
                                                                
                                                                // Save message in server DB
                                                                self.SendMessageInDB(createSendMessageRequest: createOneToOneSendMessageRequest, messageuid: messageKey, Communitykey: self.oneToOneCommunityKey)
                                                                //------------------------------
                                                                
                                                                //Not allowed show message notification for on same screen
                                                                UserDefaults.standard.set(self.oneToOneCommunityKey, forKey: StringConstants.NSUserDefauluterKeys.setCommunityKeyForCheckUserAvailableOnSameScreenForPushNotiFromOneToOne)
                                                                print("createOneToOneSendMessageRequest",createOneToOneSendMessageRequest)
                                                                self.oneToOneChat(createSendMessageRequest: createOneToOneSendMessageRequest, messageGuid: messageKey, communityKeyVal: self.oneToOneCommunityKey)
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    func oneToOneChat(createSendMessageRequest:Dictionary<String, Any>,messageGuid:String,communityKeyVal:String) {
       // self.SendMessageInDB(createSendMessageRequest: createSendMessageRequest, messageuid: messageGuid, Communitykey: communityKeyVal)
        let host = URLConstant.hostName
        let serviceJID = XMPPJID(string: "pubsub.\(host)")
        print("serviceJID",serviceJID ?? String())
        let xmppPubSub = XMPPPubSub(serviceJID: serviceJID, dispatchQueue: DispatchQueue.main)
        xmppPubSub.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppPubSub.activate(appDelegate.xmppController.xmppStream)
        let body = XMLElement.element(withName: "title") as? XMLElement // New
        
        var userString = String()
        do{
            if let userOneData = try JSONSerialization.data(withJSONObject: createSendMessageRequest, options: JSONSerialization.WritingOptions.prettyPrinted) as Optional {
                let  jsonString = NSString(data: userOneData , encoding: String.Encoding.utf8.rawValue)! as String
                userString = jsonString.replacingOccurrences(of: "\\/", with: "/"
                    , options:  NSString.CompareOptions.caseInsensitive, range: nil)
                print("userString",userString)
            } }catch {
                print(error.localizedDescription)
        }
        
        body?.stringValue = userString
        txtViewOfChatMessage.text = ""
        let messageBody = XMLElement.element(withName: "book") as? XMLElement
        messageBody?.setXmlns("pubsub:test:book") //New
        messageBody?.addChild(body ?? XMLNode())
        xmppPubSub.publish(toNode: communityKeyVal, entry: messageBody!, withItemID: messageGuid, options: ["pubsub#access_model": "open"])

    }
    
    func SendMessageInDB(createSendMessageRequest:Dictionary<String, Any>,messageuid:String,Communitykey:String) {
        _ = RequestName.SendMessageInDB
        let params = SendMessageInDbRequest.convertToDictionary(sendInvitationRequest: SendMessageInDbRequest.init(message: createSendMessageRequest))
        print("Param:",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.sendMessageInDB
        print("Api",apiURL)
        _ = SendMessageInDBService().SendMessageInDB(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! SendMessageInDbResponse
                                                        print(model)
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
        
    }
    
    

    func buildRequestForSendMessage(id: String, mediaType: String, messageType: String, contentField1: String, contentField2: String, contentField3: String, contentField4: String, contentField5: String, contentField6: String, contentField7: String, contentField8: String, contentField9: String, contentField10: String, senderId: String, communityId: String, jabberId: String, createdDatetime: String, hiddenDatetime: String, deletedDatetime: String, hiddenbyUserId: String, deletedbyUserId: String,messageStatus:String,communityName:String,communityProfileUrl:String)-> Dictionary<String, Any>{
        
        let  createSendMessageRequest = MessageRequest.convertToDictionary(sendMessage: MessageRequest.init(id: id, mediaType: mediaType, messageType: messageType, contentField1: contentField1, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: contentField5, contentField6: contentField6, contentField7: contentField7, contentField8: contentField8, contentField9: contentField9, contentField10: contentField10, senderId: senderId, communityId: communityId, jabberId: jabberId, createdDatetime: createdDatetime, hiddenDatetime: hiddenDatetime, deletedDatetime: deletedDatetime, hiddenbyUserId: hiddenbyUserId, deletedbyUserId: deletedbyUserId,messageStatus:messageStatus, communityName: communityName, communityProfileUrl: communityProfileUrl))
        print("createSendMessageRequest",createSendMessageRequest)
        return createSendMessageRequest
    }
    
    func setTextview(){
        
        txtViewOfChatMessage.isScrollEnabled = false
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 999)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 999)
        heightConstantOftxtViewOfChatMessage.constant = 33
        heightConstantOfViewBackChatTextview.constant = 53
        self.view.layoutIfNeeded()
    }
    

    @IBAction func btnCaptureImageClick(_ sender: Any) {
    }
    
    
    @IBAction func btnNewMessageClick(_ sender: Any) {
        isFirstRowIsVisible = true
        btnNewMessage.isHidden = true
        setTblScrollAtBottom()
        tblOneToOneChat.reloadData()
    }
    
    //pick doc and pdf file from documents
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        let strUUID = UUIDHelper.getUUID()
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        let currentTime = DateTimeHelper.getCurrentMillis()
        var strconfirmation = StringConstants.EMPTY
        if preferredLanguage == "en" {
            strconfirmation = (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY)) /*+ myChatsData.communityName!*/
        } else if preferredLanguage == "mr" {
            strconfirmation = /*myChatsData.communityName! +*/ (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY))
        }else{
            strconfirmation = /*myChatsData.communityName! +*/ (NSLocalizedString(StringConstants.AlertMessage.pdfsendConfirmation, comment: StringConstants.EMPTY))
        }
        
        popupAlert(title: "", message:strconfirmation , actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnCancle, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnConfirm, comment: StringConstants.EMPTY))], actions:[{action1 in
            },{action2 in
                do{
                    self.pdfDocData = try NSData(contentsOf: myURL, options: .mappedIfSafe) as Data
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    
                    self.messageKeyForSendMultiMedia = strUUID
                    self.messageTimeForSendMultiMedia = String(currentTime)
                    //Send original image to Amzon S3
                    self.originalMediaBucketName = S3BucketName + channelMedia + self.communityKey + "/" + documents + "/" + self.messageKeyForSendMultiMedia
                    let uploadMediaPartToS3  = UploadMediaPartToS3()
                    uploadMediaPartToS3.delegate = self
                    //Selected document name
                    print("document name",myURL.lastPathComponent)
                    self.documentName = myURL.lastPathComponent
                    //Save Image at docoument directory  pdfPrfiex
                    var docName = String()
                    if myURL.pathExtension == pdf {
                        //Generate PDF Thumb data (Used third party for generate thubnail of pdf)
                        let thumbnail = ROThumbnail.sharedInstance
                        let documentURL = myURL
                        let pdfThumbImage = thumbnail.getThumbnail(documentURL)
                        self.pdfThumbData = UIImagePNGRepresentation(pdfThumbImage) ?? Data()
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + pdfPrfiex  + String(self.messageTimeForSendMultiMedia)
                        docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(self.messageKeyForSendMultiMedia) + ".pdf"
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: StringConstants.MediaType.documentspdf,imageData:self.pdfThumbData as NSData)
                        let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + ".pdf"
                        uploadMediaPartToS3.uploadImage(with: self.pdfDocData as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.documentspdf,callAgain:true,contentType:pdfContentType,uploadKeyName:uploadKeyName)
                    }else{
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + documentPrefix + String(self.messageTimeForSendMultiMedia)
                        let documentType = myURL.pathExtension
                        docName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(self.messageKeyForSendMultiMedia) + "." + documentType
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: docName , sourceType: documentType,imageData:self.pdfDocData as NSData)
                        let uploadKeyName = self.S3UploadKeyNameWithImagePrefix + "." + documentType
                        uploadMediaPartToS3.uploadImage(with: self.pdfDocData as Data, bucketName:self.originalMediaBucketName,mediaSource:documentType,callAgain:true,contentType:docContentType,uploadKeyName:uploadKeyName)
                    }
                    
                }catch{
                }
                
            }, nil])
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}


extension OneToOneFromAdminVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOneToOneChat{
            return messageList.count
        }else{
            if flagOfCheckPopup == StringConstants.flags.moreoptions{
                return arrOfMenuList.count
            }else if  flagOfCheckPopup == StringConstants.flags.attachment{
                return arrOfAttachmentList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOneToOneChat {
            let messageInfo = messageList[indexPath.row]

            if messageInfo.messageType == StringConstants.MessageType.chatMessage && (messageInfo.mediaType  == StringConstants.EMPTY || messageInfo.mediaType  == StringConstants.MessageType.text) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OneToOneFromAdminTblCell", for: indexPath) as! OneToOneFromAdminTblCell
                //cell.lblOfChatMessage.text = StringUTFEncoding.UTFEncong(string: messageInfo.contentField1!)
                cell.txtViewOfChatMessage.text = StringConstants.EMPTY
                cell.txtViewOfChatMessage.text = messageInfo.contentField1!
                var msgtext = messageInfo.contentField1!
                msgtext = msgtext.lowercased()
                if msgtext.contains(URLConstant.http) ||  msgtext.contains(URLConstant.https){
                    //cell.txtViewOfChatMessage.isSelectable = true
                    cell.txtViewOfChatMessage.isUserInteractionEnabled = true
                }else{
                    //cell.txtViewOfChatMessage.isSelectable = false
                    cell.txtViewOfChatMessage.isUserInteractionEnabled = false
                }
                cell.txtViewOfChatMessage.dataDetectorTypes = UIDataDetectorTypes.link
                cell.txtViewOfChatMessage.linkTextAttributes = [ NSAttributedStringKey.foregroundColor.rawValue: UIColor.init(hexString: ColorConstants.colorPrimaryString) ]
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = time

                if messageInfo.senderId == userId {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfViewOfChatTxtMsg.constant = 100 //65
                    cell.rightConstantOfViewOfChatTxtMsg.constant = 15
                    //cell.rightConstantOfViewOfChatTxtMsg.constant = 7
                    if messageInfo.messageStatus == StringConstants.Status.sent || messageInfo.messageStatus == StringConstants.EMPTY || messageInfo.messageStatus == nil{
                        cell.imageviewOfStatus.isHidden = true
                        cell.widthOfStatusImageView.constant = 0
                    }else{
                        cell.imageviewOfStatus.isHidden = false
                        cell.widthOfStatusImageView.constant = 15
                        cell.imageviewOfStatus.image = UIImage(named: StringConstants.ImageNames.clock)
                        cell.lblOfDateTime?.text = StringConstants.EMPTY
                    }
                } else {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfViewOfChatTxtMsg.constant = 15
                    cell.rightConstantOfViewOfChatTxtMsg.constant = 100 //65
                    cell.imageviewOfStatus.isHidden = true
                    //cell.rightOcnstantOflblDateTime.constant = -17
                    //cell.rightConstantOfViewOfChatTxtMsg.constant = -19
                }
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
                
                return cell
            }else if  messageInfo.messageType == StringConstants.MessageType.chatMessage && messageInfo.mediaType  == StringConstants.MediaType.audio {
               
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetAudioMediaPartCell", for: indexPath) as! SetAudioMediaPartCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                //if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
//                }else{
//                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
//                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
//                }
                cell.btnAudioPlay.tag = indexPath.row + 10
                cell.btnAudioPlay.addTarget(self,action:#selector(btnPlayAudio(sender:)), for: .touchUpInside)
                
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = time
                
                
                //let messageTime = messageInfo.createdDatetime
                let messageId = messageInfo.id

                 let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
                print(isExist)
                if isExist
                {
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)

                }else{
                    if messageInfo.senderId == userId {
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)
                    }else{
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download)
                    }
                }
              /*
                let imageName = messageInfo.contentField4
                if !(imageName?.isEmpty)!{
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.play_audio)
                }else{
                    cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download)
                }
               */
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
                
                if messageInfo.senderId == userId {
                    
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    //cell.widthConstantOfStatusImg.constant  = 19
                    cell.leftConstantOfBackViewOfBubleImage.constant = 100
                    cell.rightConstantOfBackViewOfBubleImage.constant = 15
                    cell.imageViewOfStatus.isHidden = false
                  
                } else {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    //cell.widthConstantOfStatusImg.constant  = 0
                    cell.leftConstantOfBackViewOfBubleImage.constant = 15
                    cell.rightConstantOfBackViewOfBubleImage.constant = 100
                    cell.imageViewOfStatus.isHidden = true
                }
                return cell
            }else if messageInfo.messageType == StringConstants.MessageType.mapLocation {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetLocationPartCell", for: indexPath) as! SetLocationPartCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
//                if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
//                }else{
//                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
//                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
//                }
                cell.lblTitleAddress.text = messageInfo.contentField4
                cell.lblDetailAddress.text = messageInfo.contentField5
                
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = time
                
                cell.btnOpenGoogleMap.tag = indexPath.row + 10
                cell.btnOpenGoogleMap.addTarget(self,action:#selector(btnOpenGooleMap(sender:)), for: .touchUpInside)
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell) 
                }
                if messageInfo.senderId == userId {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    //cell.widthOfStatusImage.constant  = 19
                    cell.leftConstantViewOfBackBubbleImg.constant = 100
                    cell.rightConstantViewOfBackBubbleImg.constant = 15
                    cell.imageViewOfStastus.isHidden = false
                } else {
                    cell.bubbleImage.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    //cell.widthOfStatusImage.constant  = 0
                    cell.leftConstantViewOfBackBubbleImg.constant = 15
                    cell.rightConstantViewOfBackBubbleImg.constant = 100
                    cell.imageViewOfStastus.isHidden = true
                }
                
                return cell
            }else if messageInfo.mediaType == StringConstants.MediaType.image || messageInfo.mediaType == StringConstants.MediaType.video{
               
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetCameraMediaPartCell", for: indexPath) as! SetCameraMediaPartCell
                cell.selectionStyle = .none

        // New Code -----------------
                if messageInfo.mediaType == StringConstants.MediaType.image{
                   // let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id

                    let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                     print(isExist)
                    if isExist
                    {
                        let image = documentDirectoryHelper.getImage(imageName: imageName1)
                        cell.imgViewOfSelectedImage.image = nil
                        cell.imgViewOfSelectedImage.image = image
                        cell.imageViewOfShowMediaStatus.isHidden = true
                        // New code ------------
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.EMPTY)
                        //----------------
                        cell.btnPlayVideo.isHidden = false
                      //  cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.image_icon)
                         cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                    }else{
                        
                        let imgurl = messageInfo.contentField3
                        let rep2 = imgurl!.replacingOccurrences(of:" ", with: "%20")
                        let url = NSURL(string: rep2)
                        cell.imgViewOfSelectedImage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        cell.imageViewOfShowMediaStatus.isHidden = false
                        //New code ------------
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download_white_36dp)
                        //----------------
                        
                        cell.btnPlayVideo.isHidden = false
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.image_icon)
                        if messageInfo.senderId == userId {
                            cell.imageViewOfShowMediaStatus.isHidden = true
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)

                        }else{
                            cell.imageViewOfShowMediaStatus.isHidden = false
                        }
                    }
                }else{

                   // let imageName = messageInfo.contentField4
                    cell.imgViewOfSelectedImage.image = UIImage(named:StringConstants.EMPTY)
                    cell.imgViewOfSelectedImage.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage: UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                    
                    
                    // new code
                    //let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id

                    self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                    let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                    let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
                    print(isExist)
                    if isExist
                    {
                        cell.imageViewOfShowMediaStatus.isHidden = false
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_play_circle_outline_white)
                        cell.btnPlayVideo.isHidden = false
                       // cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                    }else{
                        cell.btnPlayVideo.isHidden = false
                        cell.imageViewOfShowMediaStatus.isHidden = false
                        //New code ------------
                        cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download_white_36dp)
                        //----------------
                        cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                        
                        if messageInfo.senderId == userId {
                            cell.imageViewOfShowMediaStatus.isHidden = false
                            cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_play_circle_outline_white)
                            cell.btnPlayVideo.isHidden = false
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.EMPTY)
                        }else{
                            cell.btnPlayVideo.isHidden = false
                            cell.imageViewOfShowMediaStatus.isHidden = false
                            //New code ------------
                            cell.imageViewOfShowMediaStatus.image = UIImage(named:StringConstants.ImageNames.ic_file_download_white_36dp)
                            //----------------
                            cell.imageViewOfShowMediaType.image = UIImage(named:StringConstants.ImageNames.ic_videocam_white)
                        }
                    }
                }
                // ------------------------- Download & Open image and Download video & Play video --------------- //
                cell.btnPlayVideo.tag = indexPath.row + 10
                cell.btnPlayVideo.addTarget(self,action:#selector(btnPlayVideo(sender:)), for: .touchUpInside)
                // -----------------------------------------//----------------------------------------------- //
                
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                if let captiontxt = messageInfo.contentField1{
                    if captiontxt == StringConstants.null{
                        cell.lblCaptionText.text = StringConstants.EMPTY
                    }else{
                        cell.lblCaptionText.text = StringUTFEncoding.UTFEncong(string:messageInfo.contentField1! )
                    }
                }else{
                    cell.lblCaptionText.text = StringConstants.EMPTY
                }
               //cell.lblCaptionText.text = StringUTFEncoding.UTFEncong(string:messageInfo.contentField1! )
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.isHidden = false
                cell.lblOfDateTime?.text = time
                
//                if dominantColour.isEmpty {
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
//                }else{
//                    cell.backgroundColor = UIColor.init(hexString: dominantColour)
//                    self.view.backgroundColor = UIColor.init(hexString: dominantColour)
//                }
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                    cell.ViewOnCell.backgroundColor =  UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                      cell.ViewOnCell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
                
                if messageInfo.senderId == userId {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfBubbleImgView.constant = 97
                    cell.rightConstantOfBubbleImgView.constant = 12
                    //cell.widthOfStatusImg.constant = 19
                    cell.imageViewOfSatus.isHidden = false
                    cell.imageViewOfShowMediaType.isHidden = true
                    //Set selected image in bubble chat box
                    cell.rightConstantOfSelectedImage.constant = 0 //8
                    cell.leftConstantOfSelectedImage.constant = 0 //2
                    cell.leadingOfBackViewOfSelectedImage.constant = 2
                    
                } else {
                    cell.bubbleImgeView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leftConstantOfBubbleImgView.constant = 12
                    cell.rightConstantOfBubbleImgView.constant = 97
                    cell.imageViewOfSatus.isHidden = true
                    cell.widthOfStatusImg.constant = 0
                    //cell.imageViewOfShowMediaType.isHidden = false
                    //Set selected image in bubble chat box
                    cell.rightConstantOfSelectedImage.constant = -5
                    cell.leftConstantOfSelectedImage.constant = 0 //8
                    cell.leadingOfBackViewOfSelectedImage.constant = 8
                }
                return cell
                
            }else if messageInfo.mediaType == StringConstants.MediaType.link  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PIPViewCell", for: indexPath) as! PIPViewCell
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime.text = time
                cell.txtViewOfLink.text = messageInfo.contentField1
                cell.lblOfTitle.text = messageInfo.contentField3
                cell.lblOfDescription.text = messageInfo.contentField4
                
                //channel profile image
                let imgurl = messageInfo.contentField2
                let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                let url = NSURL(string: rep2)
                cell.imageViewOfThumb.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                    cell.viewOnCell.backgroundColor =  UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    cell.viewOnCell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                 if messageInfo.senderId == userId {
                    cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leadingOfViewOnCell.constant  = 100
                    cell.trailingOfViewOnCell.constant = 17
                    cell.leadingOfThumbImg.constant = 5
                    cell.trailingOfthumbImage.constant = 10
                    cell.leadingOfDescriptionView.constant = 0
                    
                } else {
                    cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                    cell.leadingOfViewOnCell.constant  = 17
                    cell.trailingOfViewOnCell.constant = 100
                    cell.leadingOfThumbImg.constant = 10
                    cell.trailingOfthumbImage.constant = 5
                    cell.leadingOfDescriptionView.constant = 10
                    
                }

                //Open video in PIP mode
                cell.btnOpenVideoInPipModeClick.tag = indexPath.row + 10
                cell.btnOpenVideoInPipModeClick.addTarget(self,action:#selector(btnOpenVideoInPIPMode(sender:)), for: .touchUpInside)
                //--------------------------------------------------------------------//
                return cell
                
            }else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SetPDFDOCCell", for: indexPath) as! SetPDFDOCCell
                cell.selectionStyle = .none
                let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                let time = DateTimeHelper.getTimeAgo(time: String(describing: messageInfo.createdDatetime!))
                cell.lblOfDateTime?.text = time
                let contentField9 =  messageInfo.contentField9!
                
                if let contentField1 = messageInfo.contentField1 {
                    cell.lblOfMediaName.text =  contentField1
                }else{
                    cell.lblOfMediaName.text = StringConstants.EMPTY
                }
                
                if  contentField9 == doc || contentField9 == docx {
                    if messageInfo.senderId == userId {
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfMediaTypeImage.constant = 8
                        cell.leftConstantOfMediaTypeImage.constant = 3
                        cell.imgeViewOfDownloadIcon.isHidden = true
                        cell.btnofDownloadMedia.isHidden = false
                        //Thumbnail
                        cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                        cell.documentTypeImageOnBanerImg.isHidden = false
                        cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.docwhite70)
                    }else{
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfMediaTypeImage.constant = 3
                        cell.leftConstantOfMediaTypeImage.constant = 8
                        cell.btnofDownloadMedia.isHidden = false
                        let messageId = messageInfo.id
                        var imageName1 = String()
                        if  contentField9 == doc{
                            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                        }else{
                            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
                        }
                        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                        print(isExist)
                        if isExist{
                            cell.imgeViewOfDownloadIcon.isHidden = true
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = false
                            cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.docwhite70)
                        }else{
                            cell.imgeViewOfDownloadIcon.isHidden = false
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = true
                        }
                    }
                    cell.lblOfMediaName.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                    cell.documentTypeImageView.image = nil
                    cell.documentTypeImageView.image = UIImage(named: StringConstants.ImageNames.docRedIcon)
                }else{
                    
                    if messageInfo.senderId == userId {
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.senderBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfMediaTypeImage.constant = 8
                        cell.leftConstantOfMediaTypeImage.constant = 3
                        cell.imgeViewOfDownloadIcon.isHidden = true
                        cell.btnofDownloadMedia.isHidden = false
                        cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.pdfDefaultImage)
                        // cell.imageViewOfThumbOfMedia.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        
                        //thumbnail
                        cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                        cell.documentTypeImageOnBanerImg.isHidden = false
                        cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.pdfWhite70)
                        
                    }else{
                        cell.bubbleImageView.image = UIImage(named: StringConstants.ImageNames.receiverBubble)?.resizableImage(withCapInsets: edgeInsets)
                        cell.leftConstatntOfbackViewOfBubbleImg.constant = 15
                        cell.rightConstantOfBackViewOfBubbleImg.constant = 100
                        cell.rightConstantOfMediaTypeImage.constant = 3
                        cell.leftConstantOfMediaTypeImage.constant = 8
                        // cell.imageViewOfThumbOfMedia.sd_setImage(with: URL(string: messageInfo.contentField3!), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                        cell.btnofDownloadMedia.isHidden = false
                        let messageId = messageInfo.id
                        let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
                        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
                        print(isExist)
                        if isExist{
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.imgeViewOfDownloadIcon.isHidden = true
                            cell.documentTypeImageOnBanerImg.isHidden = false
                            cell.documentTypeImageOnBanerImg.image = UIImage(named: StringConstants.ImageNames.pdfWhite70)
                        }else{
                            cell.imgeViewOfDownloadIcon.isHidden = false
                            cell.imageViewOfThumbOfMedia.image = UIImage(named: StringConstants.ImageNames.documentThumbnail)
                            cell.documentTypeImageOnBanerImg.isHidden = true
                        }
                    }
                    cell.lblOfMediaName.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                    cell.documentTypeImageView.image = nil
                    cell.documentTypeImageView.image = UIImage(named: StringConstants.ImageNames.pdfRedIcon)
                }
                cell.btnofDownloadMedia.tag = indexPath.row + 10
                cell.btnofDownloadMedia.addTarget(self,action:#selector(btnOpenDocument(sender:)), for: .touchUpInside)
                
                if longPressIndexPath?.row == indexPath.row{
                    cell.backgroundColor = UIColor.gray
                    cell.viewOnCell.backgroundColor =  UIColor.gray
                }else{
                    cell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                    cell.viewOnCell.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.backroundcolorOfOneToOneChatCell)
                }
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
            cell.selectionStyle = .none
            if flagOfCheckPopup == StringConstants.flags.moreoptions{
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImages[indexPath.row])
                cell.lblMenuTitle.text = StringUTFEncoding.UTFEncong(string: arrOfMenuList[indexPath.row])
                cell.imageViewOfOption.isHidden = true
                cell.widthOfMenuImageView.constant = 0
            }else if  flagOfCheckPopup == StringConstants.flags.attachment{
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImages[indexPath.row])
                cell.lblMenuTitle.text = StringUTFEncoding.UTFEncong(string: arrOfAttachmentList[indexPath.row])
                cell.imageViewOfOption.isHidden = false
                if arrOfAttachmentList[indexPath.row] == (NSLocalizedString(StringConstants.popUpMenuName.doc, comment: StringConstants.EMPTY)){
                    cell.widthOfMenuImageView.constant = 25
                    cell.heightOfOptionMenuImg.constant = 25
                    cell.leadingOfOptinMenuImgView.constant = 15
                    cell.topOfOptionImageView.constant = 11
                    
                }else{
                    cell.widthOfMenuImageView.constant = 30
                    cell.heightOfOptionMenuImg.constant = 30
                    cell.leadingOfOptinMenuImgView.constant = 10
                    cell.topOfOptionImageView.constant = 6
                }
            }
            return cell
        }
    }
    
    @objc func btnOpenVideoInPIPMode(sender:UIButton){
        
        let point = tblOneToOneChat.convert(sender.center, from: sender.superview)
        let indexPath = tblOneToOneChat.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        
        let text = messageInfo.contentField1
        let types: NSTextCheckingResult.CheckingType = .link
        let detector = try? NSDataDetector(types: types.rawValue)
        guard let detect = detector else {
            return
        }
        let matches = detect.matches(in: text!, options: .reportCompletion, range: NSMakeRange(0, text!.characters.count))
        if matches.count > 0{
            var urlString = String()
            for match in matches {
                print(match.url!)
                urlString = match.url!.absoluteString
            }
            if let youTubeId = getYoutubeId(youtubeUrl: urlString) {
                //call API
                videoPlayer.showVideoUrl(in: self.view,youTubeId:youTubeId)
                strToggle = StringConstants.flags.toggle
            }
        }
    }
    
    
    @objc func btnOpenDocument(sender:UIButton){
        let point = tblOneToOneChat.convert(sender.center, from: sender.superview)
        let indexPath = tblOneToOneChat.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        
        if messageInfo.mediaType == StringConstants.MediaType.document{
            let messageId = messageInfo.id
            let contentField9 =  messageInfo.contentField9!
            var imageName1 = String()
            if  contentField9 == doc || contentField9 == docx {
                if  contentField9 == doc{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
                }else{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
                }
            }else{
                imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
            }
            print("imageName1",imageName1)
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            print("isExist",isExist)
            if isExist{
                if flagOfCheckLongPressRegonizer == true{
                    flagOfCheckLongPressRegonizer = false
                    longPressIndexPath = nil
                    tblOneToOneChat.reloadData()
                    // Set Navigation Bar
                    if flagForCheckOpenSerachBar == false{
                        self.setNavigationBarAtScreenLoadTime()
                    }else{
                        addSearchBar()
                    }
                }else{
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.openPDFInFullScreen(messageInfo:messageInfo)
                }
            }else{
                if messageInfo.senderId == userId {
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.getOpenDocument(messageInfo: messageInfo, indexPath: indexPath ?? IndexPath())
                }else{
                    DispatchQueue.main.async {
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                    }
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.getOpenDocument(messageInfo: messageInfo, indexPath: indexPath ?? IndexPath())
                }
                
            }
        }
    }
    
    func openPDFInFullScreen(messageInfo:Messages){
        
        let messageId = messageInfo.id
        let contentField9 =  messageInfo.contentField9!
        var imageName1 = String()
        if  contentField9 == doc {
            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
        }else if contentField9 == docx{
            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
        }
        else{
            imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
        }
        
        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
        print(isExist)
        let fullViewDocumentFileVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewDocumentFileVC") as! FullViewDocumentFileVC
        
        fullViewDocumentFileVC.documentDataFromLocal = imageName1
        fullViewDocumentFileVC.documentDataFromServer = StringConstants.EMPTY
        
        if  contentField9 == doc  {
            fullViewDocumentFileVC.mediaType = ".doc"
        }else if contentField9 == docx {
            fullViewDocumentFileVC.mediaType = ".docx"
        }
        else{
            fullViewDocumentFileVC.mediaType = ".pdf"
        }
        
        self.navigationController?.pushViewController(fullViewDocumentFileVC, animated: false)
    }
    
    
    func getOpenDocument(messageInfo:Messages,indexPath:IndexPath){
        let keys = Array(messageInfo.entity.attributesByName.keys)
        var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
        DispatchQueue.global(qos: .background).async {
            let messageId = messageInfo.id
            var imageName = String()
            let contentField9 =  messageInfo.contentField9!
            if  contentField9 == doc {
                imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".doc"
            }else if contentField9 == docx{
                imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".docx"
            }
            else{
                imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".pdf"
            }
            print("Directoryname:,\(imageName)")
            let messageUrl = messageInfo.contentField2
            //let url = URL(string:messageUrl!)
            //let imgurl = messageUrl
            let imgurl = messageUrl!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)

            let finalurl = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            //let url = URL(string:messageUrl!)
            if let url = URL(string:finalurl){
                if let imgData = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imgData as NSData)
                        messagedict.updateValue(imageName, forKey: "contentField4")
                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                        // Update local mediaLocalPath value
                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                        self.messageList.remove(at: (indexPath.row))
                        self.messageList.insert(messages, at: (indexPath.row))
                        self.messageListTemp.remove(at: (indexPath.row))
                        self.messageListTemp.insert(messages, at: (indexPath.row))
                        let indexPathValue = IndexPath(item: (indexPath.row), section: 0)
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                            if messageInfo.senderId == self.userId {
                                let messageInfo = self.messageList[(indexPath.row)]
                                self.openPDFInFullScreen(messageInfo:messageInfo)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func btnOpenGooleMap(sender:UIButton){
        let point = tblOneToOneChat.convert(sender.center, from: sender.superview)
        let indexPath = tblOneToOneChat.indexPathForRow(at: point)
        let messageInfo = messageList[(indexPath?.row)!]
        let lat = messageInfo.contentField2
        let lon = messageInfo.contentField3
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            if let latvalue = lat, let latitude = Double(latvalue),let longvalue = lon, let longitude = Double(longvalue) {
                UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&zoom=14&views=traffic&q=loc:\(latitude),\(longitude)&directionsmode=driving")!, options: [:], completionHandler: nil)
            }
        } else {
            print("Can't use comgooglemaps://")
            if let latvalue = lat, let latitude = Double(latvalue),let longvalue = lon, let longitude = Double(longvalue) {
                UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(latitude),\(longitude)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func btnPlayAudio(sender:UIButton){
        let point = tblOneToOneChat.convert(sender.center, from: sender.superview)
        let indexPath = tblOneToOneChat.indexPathForRow(at: point)
        let messageInfo = messageList[(indexPath?.row)!]
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
        var audioURL = NSURL()
        
        //let messageTime = messageInfo.createdDatetime
        let messageId = messageInfo.id
        
        let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
        let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: audioNameForDB)
        print(isExist)
        if isExist
        {
            audioURL = documentDirectoryHelper.getVideoPath(videoName:audioNameForDB) as NSURL
            let player = AVPlayer(url: audioURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            if messageInfo.senderId == userId {
                //let messageTime = messageInfo.createdDatetime
                let messageId = messageInfo.id
                
                let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                let messageUrl = messageInfo.contentField2
                let messageInfo = messageList[(indexPath?.row)!]
                let audiourlstr =  messageInfo.contentField2
                audioURL = NSURL(string:audiourlstr!)!
                let keys = Array(messageInfo.entity.attributesByName.keys)
                var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
                let url = URL(string:messageUrl!)
                if let videoData = try? Data(contentsOf: url!){
                    documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioNameForDB, sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                    messagedict.updateValue(audioNameForDB, forKey: "contentField4")
                    MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                    // Update local mediaLocalPath value
                    let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                    messageList.remove(at: (indexPath?.row)!)
                    messageList.insert(messages, at: (indexPath?.row)!)
                    messageListTemp.remove(at: (indexPath?.row)!)
                    messageListTemp.insert(messages, at: (indexPath?.row)!)
                    let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                    tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                }
                let player = AVPlayer(url: audioURL as URL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()}
                
            }else{
                
                let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                if checkInternetConnection == true{
                    let messageInfo = messageList[(indexPath?.row)!]
                    let audiourlstr =  messageInfo.contentField2
                    audioURL = NSURL(string:audiourlstr!)!
                    let keys = Array(messageInfo.entity.attributesByName.keys)
                    var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
                    //Save audio at docoument directory
                    let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                    spinnerActivity.isUserInteractionEnabled = false
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                    DispatchQueue.global(qos: .background).async {
                        let messageId = messageInfo.id
                        let audioNameForDB =  "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId) + ".mp3"
                        let messageUrl = messageInfo.contentField2
                        let url = URL(string:messageUrl!)
                        if let videoData = try? Data(contentsOf: url!){
                            DispatchQueue.main.async {
                                
                                self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioNameForDB, sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                                messagedict.updateValue(audioNameForDB, forKey: "contentField4")
                                MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                // Update local mediaLocalPath value
                                let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                self.messageList.remove(at: (indexPath?.row)!)
                                self.messageList.insert(messages, at: (indexPath?.row)!)
                                self.messageListTemp.remove(at: (indexPath?.row)!)
                                self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                MBProgressHUD.hide(for: self.view, animated: true);
                                self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                            }
                        }
                    }
                }else{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                }
            }
        }
    }
    
    @objc func btnPlayVideo(sender:UIButton){
        let point = tblOneToOneChat.convert(sender.center, from: sender.superview)
        let indexPath = tblOneToOneChat.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = messageList[(indexPath?.row)!]
        let keys = Array(messageInfo.entity.attributesByName.keys)
        var messagedict = messageInfo.dictionaryWithValues(forKeys: keys)
       
       //new Code
        
        if messageInfo.mediaType == StringConstants.MediaType.image{
            //let messageTime = messageInfo.createdDatetime
            let messageId = messageInfo.id
            let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            if isExist
            {     if flagOfCheckLongPressRegonizer == true{
                flagOfCheckLongPressRegonizer = false
                longPressIndexPath = nil
                tblOneToOneChat.reloadData()
                // Set Navigation Bar
                if flagForCheckOpenSerachBar == false{
                    self.setNavigationBarAtScreenLoadTime()
                }else{
                    addSearchBar()
                }
            }else{
                let messageInfo = messageList[(indexPath?.row)!]
                self.openImageInFullScreen(messageInfo:messageInfo)
                }
            }else{
                
                if messageInfo.senderId == userId {
                    let messageInfo = messageList[(indexPath?.row)!]
                    self.openImageInFullScreen(messageInfo:messageInfo)
                }else{
                    
                    let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                    if checkInternetConnection == true{
                        DispatchQueue.main.async {
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                        }
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                        // let messageTime = messageInfo.createdDatetime
                        DispatchQueue.global(qos: .background).async {
                            let messageId = messageInfo.id
                            let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
                            let messageUrl = messageInfo.contentField2
                            //let url = URL(string:messageUrl!)
                            if let url = URL(string:messageUrl!){
                                if let imgData = try? Data(contentsOf: url){
                                    DispatchQueue.main.async {
                                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imgData as NSData)
                                        messagedict.updateValue(imageName, forKey: "contentField4")
                                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                        // Update local mediaLocalPath value
                                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                        self.messageList.remove(at: (indexPath?.row)!)
                                        self.messageList.insert(messages, at: (indexPath?.row)!)
                                        self.messageListTemp.remove(at: (indexPath?.row)!)
                                        self.self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                        let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                        DispatchQueue.main.async {
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }
            }
            
        }else{
            
            //New Code------
            
            //let messageTime = messageInfo.createdDatetime
            let messageId = messageInfo.id
            self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
            let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: videoNameFrmDB)
            print(isExist)
            if isExist
            {    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
                var videoURL = NSURL()
                if isExist{
                    videoURL = documentDirectoryHelper.getVideoPath(videoName:videoNameFrmDB) as NSURL
                }else{
                    videoURL = NSURL(string: messageInfo.contentField2!)!
                }
                let player = AVPlayer(url: videoURL as URL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else{
                if messageInfo.senderId == userId {
                    
                    try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
                    var videoURL = NSURL()
                    // let messageTime = messageInfo.createdDatetime
                    let messageId = messageInfo.id
                    self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                    let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                    let messageUrl = messageInfo.contentField2
                    
                    if let url = URL(string:messageUrl!){
                        if let videoData = try? Data(contentsOf: url){
                            documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                            messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                            MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                            // Update local mediaLocalPath value
                            let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                            messageList.remove(at: (indexPath?.row)!)
                            messageList.insert(messages, at: (indexPath?.row)!)
                            messageListTemp.remove(at: (indexPath?.row)!)
                            messageListTemp.insert(messages, at: (indexPath?.row)!)
                            let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                            DispatchQueue.main.async {
                                self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                            }
                        }
                    }
                    if isExist{
                        videoURL = documentDirectoryHelper.getVideoPath(videoName:videoNameFrmDB) as NSURL
                    }else{
                        videoURL = NSURL(string: messageInfo.contentField2!)!
                    }
                    let player = AVPlayer(url: videoURL as URL)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    
                }else{
                    
                    let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                    if checkInternetConnection == true{
                        DispatchQueue.main.async {
                            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                            spinnerActivity.isUserInteractionEnabled = false
                        }
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.PleaseWait, comment: StringConstants.EMPTY)))
                        DispatchQueue.global(qos: .background).async {
                            let messageId = messageInfo.id
                            self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                            let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                            let messageUrl = messageInfo.contentField2
                            if let url = URL(string:messageUrl!){
                                if let videoData = try? Data(contentsOf: url) {
                                    DispatchQueue.main.async {
                                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                                        messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                                        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                                        // Update local mediaLocalPath value
                                        let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                                        self.messageList.remove(at: (indexPath?.row)!)
                                        self.messageList.insert(messages, at: (indexPath?.row)!)
                                        self.messageListTemp.remove(at: (indexPath?.row)!)
                                        self.messageListTemp.insert(messages, at: (indexPath?.row)!)
                                        let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                                    }
                                }
                            }
                        }
                        
                        
                        //let messageTime = messageInfo.createdDatetime
                        /*  let messageId = messageInfo.id
                         self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                         let videoNameFrmDB = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(describing: messageId!) + ".mp4"
                         let messageUrl = messageInfo.contentField2
                         if let url = URL(string:messageUrl!){
                         if let videoData = try? Data(contentsOf: url){
                         documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameFrmDB , sourceType: StringConstants.MediaType.video,imageData:videoData as NSData)
                         messagedict.updateValue(videoNameFrmDB, forKey: "contentField4") // mediaLocalPath
                         MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagedict)
                         // Update local mediaLocalPath value
                         let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: messagedict)
                         messageList.remove(at: (indexPath?.row)!)
                         messageList.insert(messages, at: (indexPath?.row)!)
                         messageListTemp.remove(at: (indexPath?.row)!)
                         messageListTemp.insert(messages, at: (indexPath?.row)!)
                         let indexPathValue = IndexPath(item: (indexPath?.row)!, section: 0)
                         DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                         MBProgressHUD.hide(for: self.view, animated: true);
                         
                         self.tblOneToOneChat.reloadRows(at: [indexPathValue], with: .none)
                         }
                         }
                         }else{
                         MBProgressHUD.hide(for: self.view, animated: true);
                         
                         self.navigationController?.view.makeToast(StringConstants.ToastViewComments.videoNotFormat)
                         }*/
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                        
                    }
                }
            }
            //new code ----------------------
            
        }
    }
    
    func openImageInFullScreen(messageInfo:Messages){
        //Open image in full screen
        if messageInfo.mediaType == StringConstants.MediaType.image{
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            fullViewOfImageFromMessagesVC.channelName =  myChatsData.communityName!
            /*   if let contentField4 = messageInfo.contentField4{
             fullViewOfImageFromMessagesVC.imageDataFromLocalDB = contentField4
             }*/
            let messageId = messageInfo.id
            let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            print(isExist)
            
            if let contentField3 = messageInfo.contentField3{
                fullViewOfImageFromMessagesVC.imageDataFromServer = contentField3
            }
            if let messageText = messageInfo.contentField1{
                if messageText == StringConstants.null{
                    fullViewOfImageFromMessagesVC.captionText  = StringConstants.EMPTY
                }else{
                    fullViewOfImageFromMessagesVC.captionText  = messageText
                }
            }else{
                fullViewOfImageFromMessagesVC.captionText  = StringConstants.EMPTY
            }
            if isExist{
                fullViewOfImageFromMessagesVC.imageDataFromLocalDB = imageName1
                self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                
            }else{
                
                if userId == messageInfo.senderId{
                    fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.contentField3!
                    fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                }else{
                    fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbtOfPopUp{
            viewOfBackPopUp.isHidden = true
            flagOfClickOnMoreOption = false
            flagOfClickOnAttachmentOption = false
            
            if  flagOfCheckPopup == StringConstants.flags.attachment{
                let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
                if indexPath.row == 0{
                    if checkInternetConnection == true {
                        let selectMultiMediaPopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectMultiMediaPopUpVC") as! SelectMultiMediaPopUpVC
                        selectMultiMediaPopUpVC.modalPresentationStyle = .overCurrentContext
                        let backgroundScreenShot = captureScreen()
                        selectMultiMediaPopUpVC.checkMediaType = "Camera"
                        selectMultiMediaPopUpVC.commingFrom = StringConstants.flags.oneToOneFromAdminVC
                        selectMultiMediaPopUpVC.backroundImage = backgroundScreenShot!
                        self.navigationController?.pushViewController(selectMultiMediaPopUpVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 1{
                    if checkInternetConnection == true {
                        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            imagePickerForOnlyImages.delegate = self
                            imagePickerForOnlyImages.sourceType = .savedPhotosAlbum
                            imagePickerForOnlyImages.allowsEditing = false
                            self.present(imagePickerForOnlyImages, animated: true, completion: nil)
                        }
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 2{
                    if checkInternetConnection == true {
                        imagePicker.sourceType = .savedPhotosAlbum
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = [kUTTypeMovie as String]
                        present(imagePicker, animated: true, completion: nil)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 3{
                    if checkInternetConnection == true {
                        let selectMultiMediaPopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectMultiMediaPopUpVC") as! SelectMultiMediaPopUpVC
                        selectMultiMediaPopUpVC.modalPresentationStyle = .overCurrentContext
                        let backgroundScreenShot = captureScreen()
                        selectMultiMediaPopUpVC.commingFrom = StringConstants.flags.oneToOneFromAdminVC
                        selectMultiMediaPopUpVC.checkMediaType = "Audio"
                        selectMultiMediaPopUpVC.backroundImage = backgroundScreenShot!
                        self.navigationController?.pushViewController(selectMultiMediaPopUpVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                    
                }else if indexPath.row == 4 {
                    if checkInternetConnection == true {
                        let selectLocForSendLocVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocForSendLocVC") as! SelectLocForSendLocVC
                        selectLocForSendLocVC.commingFrom = StringConstants.flags.oneToOneFromAdminVC
                        selectLocForSendLocVC.strchannelname = communityName
                        selectLocForSendLocVC.myChatsData = self.myChatsData
                        self.navigationController?.pushViewController(selectLocForSendLocVC, animated: false)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }else if indexPath.row == 5 {
                    if checkInternetConnection == true {
                        let pdf                                 = String(kUTTypePDF)
                        let docs                                = String(kUTTypeCompositeContent)
                        let types                               = [pdf, docs]
                        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
                        documentPicker.delegate = self
                        documentPicker.modalPresentationStyle = .formSheet
                        self.present(documentPicker, animated: true, completion: nil)
                    }else{
                        self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
                    }
                }
            }else if flagOfCheckPopup == StringConstants.flags.moreoptions{
                
                if indexPath.row == 0{
                    viewOfBackPopUp.isHidden = true
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let showUserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "ShowUserProfileVC") as! ShowUserProfileVC //ChannelProfileVC
                    //channelProfileVC.ownerId = ownerId
                    self.navigationController?.pushViewController(showUserProfileVC, animated: true)
                    
                }else if indexPath.row == 1{
                    addSearchBar()
                }else if indexPath.row == 2{
                    
                }
            }
            
        }else{
            if flagOfCheckLongPressRegonizer == true{
                flagOfCheckLongPressRegonizer = false
                longPressIndexPath = nil
                tblOneToOneChat.reloadData()
                // Set Navigation Bar
                if flagForCheckOpenSerachBar == false{
                    self.setNavigationBarAtScreenLoadTime()
                }else{
                    addSearchBar()
                }
            }
        }
    }
    
    func addSearchBar()  {
        navView.isHidden = true
        flagForCheckOpenSerachBar = true
        searchBar.placeholder = (NSLocalizedString(StringConstants.placeHolder.search, comment: StringConstants.EMPTY))
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.becomeFirstResponder()
        searchBar.sizeToFit()
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
        
        
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        searchBar.delegate = self
        btnCloseSearch = UIButton(frame: CGRect(x: self.searchBar.frame.origin.x  + self.searchBar.frame.size.width, y: 5, width: 40, height: 25))
        btnCloseSearch  = UIButton(type: .custom)
        btnCloseSearch.setImage(UIImage(named: StringConstants.ImageNames.closeSearchIcon), for: .normal)
        btnCloseSearch.addTarget(self, action: #selector(btnCloseSearchClick), for: .touchUpInside)
         navigationItem.rightBarButtonItems = []
        let rightNavBarButton = UIBarButtonItem(customView:btnCloseSearch)
        self.navigationItem.rightBarButtonItem = rightNavBarButton
    }
    
    @objc func btnCloseSearchClick(sender: UIButton!) {
        hideSearchBar()
        flagForCheckOpenSerachBar = false
        sender.isHidden = true
        setNavigationBarAtScreenLoadTime()
        self.messageList = self.messageListTemp
        tblOneToOneChat.reloadData()
        self.setTblScrollAtBottom()
    }
    
    func hideSearchBar(){
        btnCloseSearch.isHidden = true
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.removeFromSuperview()
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] as? UIWindow
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    
}

extension OneToOneFromAdminVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true
        viewOfBackPopUp.isHidden = true
        
        let currentText = textView.text
        let updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        let trimmedString = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)

        if updatedText.isEmpty {
            textView.text = ""
            txtViewOfChatMessage.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGrayBtn), for: .normal)
            btnSendMessage.isEnabled = false
            return false
        }else if txtViewOfChatMessage.textColor == UIColor.lightGray && !text.isEmpty && trimmedString != "" && !trimmedString.isEmpty && trimmedString != StringConstants.CommunityType.txtPlaceHolderForTypeMessage {
                 textView.text = nil
                txtViewOfChatMessage.textColor = UIColor.black
                btnSendMessage.isEnabled = true
                btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGreenBtn), for: .normal)
        }
        if !trimmedString.isEmpty {
            btnSendMessage.isEnabled = true
            btnSendMessage.setImage(UIImage(named: StringConstants.ImageNames.sendChatMesgGreenBtn), for: .normal)
            
        }
        
        //Line number limitation for message textview
        var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        let boundingRect = sizeOfString(string: updatedText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        
        if numberOfLines >= 5  {
            txtViewOfChatMessage.isScrollEnabled = true
        } else {
            txtViewOfChatMessage.isScrollEnabled = false
            
        }
        
        return true
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedStringKey.font: font],
                                                 context: nil).size
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true

        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        viewOfBackPopUp.isHidden = true
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if textView.text.isEmpty {
            textView.text = (NSLocalizedString(StringConstants.CommunityType.txtPlaceHolderForTypeMessage, comment: StringConstants.EMPTY))
            textView.textColor = UIColor.lightGray
            txtViewOfChatMessage.resignFirstResponder()
            txtViewOfChatMessage.isScrollEnabled = false
            heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.constant = 33
            heightConstantOfViewBackChatTextview.constant = 53
            self.view.layoutIfNeeded()
        }
    }
    
    /* func textViewDidChangeSelection(_ textView: UITextView) {
     if self.view.window != nil {
     if textView.textColor == UIColor.lightGray {
     textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
     }
     }
     }*/
}

extension OneToOneFromAdminVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        flagOfClickOnMoreOption = false
        flagOfClickOnAttachmentOption = false
        capturImage = UIImage()
        videoURL = nil
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                dismiss(animated: true, completion: nil)
                let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                capturImage = image
                setCaptionViewController.commingFrom = StringConstants.flags.oneToOneFromAdminVC
                setCaptionViewController.selectedImage = capturImage
                var name = myChatsData.communityName
                if let lastName = myChatsData.lastName {
                    name = name! + StringConstants.singleSpace + lastName
                }
                setCaptionViewController.navigationTitle = StringUTFEncoding.UTFEncong(string:name!)
                self.navigationController?.pushViewController(setCaptionViewController, animated: false)
            }else{
                videoURL = info[UIImagePickerControllerMediaURL] as? URL
                self.dismiss(animated: true, completion: nil)
                let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                setCaptionViewController.commingFrom = StringConstants.flags.oneToOneFromAdminVC
                setCaptionViewController.selectedVideoURL = videoURL
                var name = myChatsData.communityName
                if let lastName = myChatsData.lastName {
                    name = name! + StringConstants.singleSpace + lastName
                }
                setCaptionViewController.navigationTitle = StringUTFEncoding.UTFEncong(string:name!)
                self.navigationController?.pushViewController(setCaptionViewController, animated: false)
            }
        }
    }
}

extension  OneToOneFromAdminVC :UISearchBarDelegate {
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
     /*   let predicate = NSPredicate(format: "message CONTAINS[cd] %@", searchText)
        let arrOfFilteredData = arrOfSendMsgData.filter { predicate.evaluate(with: $0) }  as NSArray
        arrOfSendMsgData = arrOfFilteredData.mutableCopy() as! NSMutableArray
        if arrOfSendMsgData.count == 0{
            for i in (0..<arrOfOriginalCopy.count){
                arrOfSendMsgData.add(arrOfOriginalCopy[i])
            }
        }
        self.tblOneToOneChat.reloadData()
        self.setTblScrollAtBottom()*/
        
        let searchText = String(searchBar.text!)
        self.filteredObjects = messageListTemp.filter { ($0.contentField1)?.range(of: searchText, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        if self.filteredObjects?.count == 0{
            messageList.removeAll()
            messageList = messageListTemp
        }else{
            messageList.removeAll()
            messageList = filteredObjects!
        }
        
        self.tblOneToOneChat.reloadData()
        self.setTblScrollAtBottom()
    }
}

extension OneToOneFromAdminVC: XMPPStreamDelegate {
    
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage){
        print("group chat message",message)
        if message.childCount > 1{
            let createSendMessageRequest = XmppBroadCastMessageReceiveResponse.adminBroadCastMsgReceiveResponse(message: message)
            let messageData = createSendMessageRequest.0 as NSDictionary
            if let receivedMsgCommunityKey = messageData.value(forKey: "communityId") as? String {
                
            //for admin one to one initiate
            if ownerId != userId{

              /*  if let  userCommunityJabberId = myChatsData.userCommunityJabberId {
                    self.oneToOneCommunityKey = userCommunityJabberId
                    communityKey = self.oneToOneCommunityKey
                }else{
                    let communityJabberId = myChatsData.communityKey! + "_" + userId
                    self.oneToOneCommunityKey = communityJabberId
                }
                let messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: self.oneToOneCommunityKey)
                if messages  .count > 0 {
                    //self.oneToOneCommunityKey = communityJabberIDFromlistOfFollower
                    communityKey = self.oneToOneCommunityKey
                }else{
                    self.oneToOneCommunityKey = StringConstants.EMPTY
                }*/
            }
 
                // let  from = createSendMessageRequest.1
                let  senderId = messageData.value(forKey: "senderId") as? String
                if receivedMsgCommunityKey == oneToOneCommunityKey && userId !=  senderId /*from != RequestName.resourceName*/ {
                    let messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest.0)
                    if messageList.contains(where: { $0.id == messages.id }){
                        print("Dont append")
                    }else{
                       // messageList.append(messages)
                        messageList.insert(messages, at: 0)
                        messageListTemp = messageList
                        if messageList.count < 10 {
                            isDataLoading = false
                        }
                    }
                    
                    //For show new message button
                    if userId == myChatsData.ownerId{
                        btnNewMessage.isHidden = true
                        setTblScrollAtBottom()
                        self.tblOneToOneChat.reloadData()
                    }else{
                        if isFirstRowIsVisible == true{
                            setTblScrollAtBottom()
                            btnNewMessage.isHidden = true
                            self.tblOneToOneChat.reloadData()
                        }else{
                            btnNewMessage.isHidden = false
                        }
                    }
                    
                    
                }else if SessionDetailsForCommunityType.shared.changePendingMessageStatus == true {
                    SessionDetailsForCommunityType.shared.changePendingMessageStatus = false
                    self.fetchOffSet = 0
                    let messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey: communityKey)
                    // messages = messages.sorted(by: {$0.createdDatetime! < $1.createdDatetime!} )
                    messageList.removeAll()
                    messageListTemp.removeAll()
                    messageList = messages
                    messageListTemp = messages
                    if messageList.count < 10 {
                        isDataLoading = false
                    }
                    tblOneToOneChat.reloadData()
                }
            }
        }
    }

}

extension OneToOneFromAdminVC : XMPPPubSubDelegate{
    func xmppPubSub(_ sender: XMPPPubSub, didPublishToNode node: String, withResult iq: XMPPIQ) {
        print("node",node)
        createSendMessageRequestInDB.updateValue(StringConstants.Status.sent, forKey: "messageStatus")
        //Update message in core data as local DB with sent status
        MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: createSendMessageRequestInDB)
    }
    
    func xmppPubSub(_ sender: XMPPPubSub, didNotPublishToNode node: String, withError iq: XMPPIQ) {
        print("node",node)
    }
    
}

// ** Pavan
extension  OneToOneFromAdminVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tblOneToOneChat{
//            let top: CGFloat = 0
//            let cellBuffer: CGFloat = 2
//            let bottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
//            let buffer: CGFloat = cellBuffer //* 44.0
//            let scrollPosition = scrollView.contentOffset.y
//
//            // Reached the bottom of the list
//            if scrollPosition < bottom - buffer {
//                self.hideBtnMore()
//            }
//                // Reach the top of the list
//            else if scrollPosition > top {
//                if self.isDataLoading{
//                    self.setBtnMore()
//                }else{
//                    self.hideBtnMore()
//                }
//            }else{
//                //  self.hideBtnMore() // do comment this code
//            }
            
            if  messageList.count > 0 {
                let firstVisibleIndexPath: IndexPath? = tblOneToOneChat.indexPathsForVisibleRows?[0]
                if firstVisibleIndexPath?.row == 0 {
                    isFirstRowIsVisible = true
                }else{
                    isFirstRowIsVisible = false
                }
            }
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //  isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>){
        
        //Local Pagination
        self.hideBtnMore()
        let endScrolling: Float = Float(scrollView.contentOffset.y + scrollView.frame.size.height)
        var scrollContentSize = Float(scrollView.contentSize.height)
        scrollContentSize = (scrollContentSize / 100) * 80
        
        if (scrollView is UITableView) {
            if velocity.y > 0 {
                if endScrolling >= scrollContentSize {
                   
                    var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKeyPagination(fetchOffSet, communityKey:  self.oneToOneCommunityKey)
                   
                    if messages.count > 0 {
                        let preveousCount = self.messageList.count
                        self.isDataLoading = messages.count > 0 ? true : false
                        self.page_number =   self.page_number + 1
                        self.fetchOffSet =   self.fetchOffSet + 10
                        
                        for i in 0 ..< messages.count {
                            let list = messages[i]
                            self.isDataLoading = true
                            if !messageList.contains(list){
                                self.messageList.append(list)
                                self.messageListTemp.append(list)
                            }
                        }
                        //  self.messageList.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                        //  self.messageListTemp.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                        self.tblOneToOneChat.reloadData()
                        // self.scrollToLoadMorePossition(messagesCount: preveousCount)
                    }else{
                        // check internet and fire api
//                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
//                        spinnerActivity.isUserInteractionEnabled = false
                        self.getOldMessage(isCallFirstTime: false)
                    }
                }
            }
        }
    }
}
