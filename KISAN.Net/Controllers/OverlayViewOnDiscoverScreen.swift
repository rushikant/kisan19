//
//  OverlayViewOnDiscoverScreen.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 02/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class OverlayViewOnDiscoverScreen: UIViewController {

    @IBOutlet weak var btnGOTIT: UIButton!
    @IBOutlet weak var viewOfBackPopUpView: UIView!
    @IBOutlet weak var lblFilterInfo: UILabel!
    @IBOutlet weak var imgOfFilter: UIImageView!
    @IBOutlet weak var viewOfPopUp: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.FALSE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
        
        // Do any additional setup after loading the view.
        imgOfFilter.layer.cornerRadius = imgOfFilter.frame.size.width / 2
        imgOfFilter.clipsToBounds = true
        
        viewOfPopUp.layer.cornerRadius = 10
        viewOfPopUp.clipsToBounds = true
        
        btnGOTIT.layer.cornerRadius = 5.0
        btnGOTIT.clipsToBounds = true
        
        if UIScreen.main.bounds.size.height <= 568{
            lblFilterInfo.font = lblFilterInfo.font.withSize(18)
        }else{
            lblFilterInfo.font = lblFilterInfo.font.withSize(20)
        }
        
        lblFilterInfo.text = (NSLocalizedString(StringConstants.DiscoverViewController.tap, comment: StringConstants.EMPTY))
        btnGOTIT.setTitle((NSLocalizedString(StringConstants.DiscoverViewController.gotIt, comment: StringConstants.EMPTY)), for: .normal)
    }

    @IBAction func btnGOTITClick(_ sender: Any) {
         navigationController?.popViewController(animated:false)
         navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
