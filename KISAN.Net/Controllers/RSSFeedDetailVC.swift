//
//  RSSFeedDetailVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 25/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class RSSFeedDetailVC: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    var rssFeedDetails = Messages()
    var navView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setNavigationBarAtScreenLoadTime()
        
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false

        let url =  rssFeedDetails.contentField4!
        if let  encodedURL = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),let url = URL(string: encodedURL) {
            print("valid url")
            webView.load(URLRequest(url: url))
        } else {
            print("invalid url ")
        }
    }

    func setNavigationBarAtScreenLoadTime(){
        // Create a navView to add to the navigation bar
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navView.isHidden = false
        
        if UIScreen.main.bounds.size.height <= 568{
            navView = UIView(frame: CGRect(x: 0, y: 0, width: 205, height: 50))
        }else{
            navView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
        }
        
        // Create the image view
        var imageOfNavTitle = UIImageView()
        imageOfNavTitle = UIImageView(frame: CGRect(x: -10, y:5, width: 35, height: 35))
        imageOfNavTitle.sd_setImage(with: URL(string: StringConstants.ImageNames.kisan_smoll_logo), placeholderImage:UIImage(named: StringConstants.ImageNames.kisan_smoll_logo), options: .refreshCached)
        imageOfNavTitle.layer.cornerRadius = imageOfNavTitle.frame.height / 2
        imageOfNavTitle.clipsToBounds = true
        
        // Create the label
        var lblOfNavTitle = UILabel()
        
            if UIScreen.main.bounds.size.height <= 568{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 102, height: 21))
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
            }else{
                lblOfNavTitle = UILabel(frame: CGRect(x: imageOfNavTitle.frame.size.width + 5, y:10, width: 150, height: 21))
                lblOfNavTitle.font = lblOfNavTitle.font.withSize(16)
            }
        
        lblOfNavTitle.font = lblOfNavTitle.font.withSize(15)
        lblOfNavTitle.text = StringUTFEncoding.UTFEncong(string:"KISAN.Net")
        lblOfNavTitle.textColor = UIColor.white
        lblOfNavTitle.textAlignment = NSTextAlignment.left
        navView.addSubview(lblOfNavTitle)
        navView.addSubview(imageOfNavTitle)
        // Set the navigation bar's navigation item's titleView to the navView
        self.navigationItem.titleView = navView
        self.title = StringConstants.NavigationTitle.discover
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
    }
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView,didStart navigation: WKNavigation!) {
        print("Start Page Loading")
    }
    
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
        print("Page loaded")
        MBProgressHUD.hide(for: self.view, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
