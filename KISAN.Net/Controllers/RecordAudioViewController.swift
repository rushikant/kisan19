//
//  RecordAudioViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 19/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class RecordAudioViewController: UIViewController {
    var backroundImage = UIImage()
    @IBOutlet weak var imageViewOfBCKImg: UIImageView!
    @IBOutlet weak var viewOfBackPopup: UIView!
    @IBOutlet weak var btnRecord: UIButton!
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings         = [String : Int]()
    var audioPlayer : AVAudioPlayer!
    @IBOutlet weak var lblStopRecordingTitle: UILabel!
    @IBOutlet weak var lblOfStartRecordingTitle: UILabel!
    @IBOutlet weak var viewOfBackPopUp: UIView!
    @IBOutlet weak var imgForSetImgToRecordBtn: UIImageView!
    var checkStopRecording = Bool()
    @IBOutlet weak var heightOfStartRecordingTitle: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    var appDelegate = AppDelegate()
    var commingFrom = String()
    @IBOutlet weak var lblStopRecordingTitlePartTwo: UILabel!
    weak var timer: Timer?
    var startTime: Double = 0
    var time: Double = 0
    var elapsed: Double = 0
    var status: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewOfBCKImg.image = backroundImage
        imgForSetImgToRecordBtn.layer.cornerRadius = imgForSetImgToRecordBtn.frame.size.width / 2
        imgForSetImgToRecordBtn.clipsToBounds = true
        checkStopRecording = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RecordAudioViewController.dismissPopUp))
        viewOfBackPopup.addGestureRecognizer(tap)
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        lblOfStartRecordingTitle.text =  (NSLocalizedString(StringConstants.RecordAudioViewController.TapToRecord, comment: StringConstants.EMPTY))
        lblStopRecordingTitle.text = (NSLocalizedString(StringConstants.RecordAudioViewController.talk, comment: StringConstants.EMPTY))
        lblStopRecordingTitlePartTwo.text = (NSLocalizedString(StringConstants.RecordAudioViewController.Stoprecording, comment: StringConstants.EMPTY))
        
        lblStopRecordingTitle.isHidden = true
        lblStopRecordingTitle.textColor = UIColor.white
        lblStopRecordingTitlePartTwo.isHidden = true
        lblStopRecordingTitlePartTwo.textColor = UIColor.white
       self.setRecordingSession()
        
    }
    

    func setRecordingSession() {
        
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    
    
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let filename = NSUUID().uuidString + ".m4a"
        let soundURL = documentDirectory.appendingPathComponent(filename)
        print(soundURL)
        return soundURL as NSURL?
        
        
    }
    
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    
    @IBAction func btnSendClick(_ sender: Any) {
        
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC {
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            // Pass image & caption text to ChannelSendBroadcastMsgVC
            if  audioRecorder.url != nil {
                appDelegate.captureAudioUrl = audioRecorder.url
            }
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is ChannelSendBroadcastMsgVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }else{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
            // Pass image & caption text to ChannelSendBroadcastMsgVC
            if  audioRecorder.url != nil {
                appDelegate.captureAudioUrl = audioRecorder.url
            }
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is OneToOneFromAdminVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }
        
    }
    
    @IBAction func btnRecordClick(_ sender: Any) {
        
        if checkStopRecording == false {
            if audioRecorder == nil{
                checkStopRecording = false
                self.startRecording()
                self.imgForSetImgToRecordBtn.image = UIImage(named:StringConstants.ImageNames.stopRecording)
                self.viewOfBackPopUp.backgroundColor = UIColor(red: 239.0/255.0, green: 76.0/255.0, blue: 80.0/255.0, alpha: 1.0)
                lblStopRecordingTitle.isHidden = false
                lblStopRecordingTitlePartTwo.isHidden = false
                heightOfStartRecordingTitle.constant = 21
                lblOfStartRecordingTitle.textColor = UIColor.white
                //Start audio recording timer
                self.start()
            }else{
                checkStopRecording = true
                self.imgForSetImgToRecordBtn.image = UIImage(named:StringConstants.ImageNames.playRecording)
                lblStopRecordingTitle.isHidden = true
                lblOfStartRecordingTitle.isHidden = false
                lblOfStartRecordingTitle.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
               // lblOfStartRecordingTitle.text = String(time)
                lblStopRecordingTitlePartTwo.isHidden = true
                heightOfStartRecordingTitle.constant = 21
                btnSend.isHidden = false
                self.viewOfBackPopUp.backgroundColor = UIColor.white
                self.finishRecording(success: true)
                //Stop audio recording timer
                 self.stop()
            }
        }else{
            let player = AVPlayer(url: audioRecorder.url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func start() {
        startTime = Date().timeIntervalSinceReferenceDate - elapsed
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        // Set Start/Stop button to true
        status = true
    }
    
    func stop() {
        elapsed = Date().timeIntervalSinceReferenceDate - startTime
        timer?.invalidate()
        //lblOfStartRecordingTitle.text = "00.00"
        // Set Start/Stop button to false
        status = false
    }
    
    @objc func updateCounter() {
        
        // Calculate total time since timer started in seconds
        time = Date().timeIntervalSinceReferenceDate - startTime
        
        // Calculate minutes
        let minutes = UInt8(time / 60.0)
        time -= (TimeInterval(minutes) * 60)
        
        // Calculate seconds
        let seconds = UInt8(time)
        time -= TimeInterval(seconds)
        
        // Calculate milliseconds
        //let milliseconds = UInt8(time * 100)
        
        // Format time vars with leading zero
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        //let strMilliseconds = String(format: "%02d", milliseconds)
        lblOfStartRecordingTitle.text = strMinutes + ":" + strSeconds
        
    }
    
    @objc func dismissPopUp() {
        if status == false {
            if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC {
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                if let nav = self.navigationController {
                    for controller in nav.viewControllers as Array {
                        if controller is ChannelSendBroadcastMsgVC {
                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                            return
                        }
                    }
                    nav.pushViewController(vc, animated: false)
                    return
                }
            }else{
                
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
                if let nav = self.navigationController {
                    for controller in nav.viewControllers as Array {
                        if controller is OneToOneFromAdminVC {
                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                            return
                        }
                    }
                    nav.pushViewController(vc, animated: false)
                    return
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RecordAudioViewController :
AVAudioRecorderDelegate,AVAudioPlayerDelegate,MPMediaPickerControllerDelegate {
    // For Recording
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    // For recording
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
    }
    
    // For media picker
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        let item = mediaItemCollection.items.first
        do {
            if let url = item?.assetURL {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            }
        } catch {
            print(error)
        }
    }
}
