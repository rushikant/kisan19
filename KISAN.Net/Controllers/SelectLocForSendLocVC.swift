//
//  SelectLocForSendLocVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import CoreLocation

class SelectLocForSendLocVC: UIViewController {
    @IBOutlet weak var lblTitleAddress: UILabel!
    @IBOutlet weak var lblOfDetialAddress: UILabel!
    @IBOutlet weak var imageViewOfAddressIcon: UIImageView!
    @IBOutlet weak var tblAdrressList: UITableView!
    @IBOutlet var lblcurrentliocation: UILabel!
    @IBOutlet var lblAroudYouloc: UILabel!
    var arrOfLocation = NSMutableArray()
    var appDelegate = AppDelegate()
    var arrOfDetialAddress = NSMutableArray()
    var arrOfTitleAddress = NSMutableArray()
    var arrOfLocationLatLong = NSMutableArray()
    var strTitleAddress = String()
    var strDetailAddress = String()
    var strLatitude = String()
    var strLongitude = String()
    var strchannelname =  String()
    var myChatsData = MyChatsTable()
    
    

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var commingFrom = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        
        print(strchannelname)

        lblcurrentliocation.text =  (NSLocalizedString(StringConstants.SelectLocation.currentlocation, comment: StringConstants.EMPTY))
        lblAroudYouloc.text =  (NSLocalizedString(StringConstants.SelectLocation.AroundYouLocation, comment: StringConstants.EMPTY))
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        lblTitleAddress.text = ""
        lblOfDetialAddress.text = "Location not found"
        activityIndicator.startAnimating()
        DispatchQueue.global(qos: .background).async {
            self.getNearLocAddressFromLatLon(pdblLatitude:self.appDelegate.latitude , withLongitude: self.appDelegate.longitude)
        }
    }
    
    
    func getNearLocAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        //http://maps.googleapis.com/maps/api/geocode/json?latlng=18.579979591304,73.7359025968859&sensor=false Near location address
        //let urlString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=18.579979591304,73.7359025968859&sensor=false"
        
        // let urlString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + pdblLatitude + "," + pdblLongitude + "&sensor=false"
        //let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=AIzaSyDX6FLbeI7r5eTY4LHNTz_IWowhvcWgNd0"
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pdblLatitude + "," + pdblLongitude + "&key=AIzaSyDX6FLbeI7r5eTY4LHNTz_IWowhvcWgNd0"
    
        print("urlString",urlString)
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    self.popupAlert(title: "", message: error?.localizedDescription, actionTitles: ["OK"], actions:[{action1 in
                        },{action2 in
                        }, nil])
                }
                
            } else {
                do {
                    
                    let arrOfData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    let arrOfResult = arrOfData["results"] as! NSArray
                    print("arrOfResult",arrOfResult)
                    
                    if arrOfResult.count == 0{
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                            self.popupAlert(title: "", message:(NSLocalizedString(StringConstants.AlertMessage.locationNotFound, comment: StringConstants.EMPTY)) , actionTitles: ["OK"], actions:[{action1 in
                                },{action2 in
                                }, nil])
                        }
                    }else{
                        let arrOfAddressComponents = arrOfResult.value(forKey: "address_components") as! NSArray
                        let formattedAddress  = arrOfResult.value(forKey: "formatted_address") as! NSArray
                        self.arrOfDetialAddress = NSMutableArray(array: formattedAddress)
                        
                        for i in 0 ..< arrOfAddressComponents.count {
                            let data = arrOfAddressComponents[i] as! NSArray
                            let longName = data[0] as! NSDictionary
                            let name = longName.value(forKey: "long_name")
                            self.arrOfTitleAddress.add(name ?? String())
                        }
                        
                        let arrOfgeometry = arrOfResult.value(forKey: "geometry") as! NSArray
                        let location = arrOfgeometry.value(forKey: "location") as! NSArray
                        self.arrOfLocationLatLong = NSMutableArray(array: location)
                        
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                            
                            self.lblTitleAddress.text = self.arrOfTitleAddress[0] as? String
                            self.lblOfDetialAddress.text = self.arrOfDetialAddress[0] as? String
                            
                            self.strTitleAddress = self.arrOfTitleAddress[0] as! String
                            self.strDetailAddress = self.arrOfDetialAddress[0] as! String
                            self.strLatitude = String(format: "%@", (self.arrOfLocationLatLong[0] as AnyObject).value(forKey: "lat") as! CVarArg)
                            self.strLongitude = String(format: "%@", (self.arrOfLocationLatLong[0] as AnyObject).value(forKey: "lng") as! CVarArg)
                            
                            self.arrOfTitleAddress.removeObject(at: 0)
                            self.arrOfDetialAddress.removeObject(at: 0)
                            self.arrOfLocationLatLong.removeObject(at: 0)
                            self.tblAdrressList.reloadData()
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
    }
    
    func initialDesign()  {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.selectLocation, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        
        tblAdrressList.rowHeight = UITableViewAutomaticDimension
        tblAdrressList.estimatedRowHeight = 90.0
        tblAdrressList.tableFooterView = UIView()
        tblAdrressList.separatorColor = UIColor.clear
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCurrentLocClick(_ sender: Any) {
        if !strTitleAddress.isEmpty && !strDetailAddress.isEmpty{
            let  mapViewForSendLocVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewForSendLocVC") as! MapViewForSendLocVC
            mapViewForSendLocVC.strTitleAddress = strTitleAddress
            mapViewForSendLocVC.strDetailAddress = strDetailAddress
            mapViewForSendLocVC.strLatitude = strLatitude
            mapViewForSendLocVC.strLongitude = strLongitude
            mapViewForSendLocVC.commingFrom = commingFrom
            mapViewForSendLocVC.strchannelname = self.strchannelname
            mapViewForSendLocVC.channelname = self.strchannelname
            self.navigationController?.pushViewController(mapViewForSendLocVC, animated: true)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SelectLocForSendLocVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfDetialAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectLocForSendLocCell", for: indexPath) as! SelectLocForSendLocCell
        cell.selectionStyle = .none
        cell.lblOfTitleAddress.text = (arrOfTitleAddress[indexPath.row] as? String)
        cell.lblOfDetailAddress.text = arrOfDetialAddress[indexPath.row] as? String
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  mapViewForSendLocVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewForSendLocVC") as! MapViewForSendLocVC
        
        mapViewForSendLocVC.strTitleAddress = (arrOfTitleAddress[indexPath.row] as? String)!
        mapViewForSendLocVC.strDetailAddress = (arrOfDetialAddress[indexPath.row] as? String)!
        mapViewForSendLocVC.strLatitude = String(format: "%@", (self.arrOfLocationLatLong[indexPath.row] as AnyObject).value(forKey: "lat") as! CVarArg)
        mapViewForSendLocVC.strLongitude = String(format: "%@", (self.arrOfLocationLatLong[indexPath.row] as AnyObject).value(forKey: "lng") as! CVarArg)
        mapViewForSendLocVC.commingFrom = commingFrom
        print(strchannelname)
          mapViewForSendLocVC.strchannelname = self.strchannelname
        mapViewForSendLocVC.channelname = self.strchannelname

        self.navigationController?.pushViewController(mapViewForSendLocVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
}
