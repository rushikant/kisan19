//
//  SelectLocationPopUpView.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 17/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

//MARK: step 1 Add Protocol here
protocol PassSelectedLocation: class {
    func passSelectedLocation(_ location: String)
}

class SelectLocationPopUpView: UIViewController {
    var arrOfState :[Any]!
    var arrOfDistrict :[String: AnyObject]!
    //var arrOfLocation = NSMutableArray()
    @IBOutlet weak var tblSelectLoc: UITableView!
    var location = String()
    //MARK: step 2 Create a delegate property here, don't forget to make it weak!
    weak var delegate: PassSelectedLocation?
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewOfBackTblView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       initialDesign()
    }
    
    func initialDesign()  {
        tblSelectLoc.tableFooterView = UIView()
        self.tblSelectLoc.separatorColor = UIColor.clear
        self.callGetStateList()
    }
    
    func callGetStateList()  {
        let params = ["":""]
        let path = Bundle.main.path(forResource: SelectCityURLConstant.stateList, ofType: SelectCityURLConstant.jsonType)
        let fileUrl = NSURL(fileURLWithPath: path!)
        let apiURL = String(describing: fileUrl)
        _ = SelectStateServices().getStateList(apiURL,
                                               postData: params as [String : AnyObject],
                                               withSuccessHandler: { (userModel) in
                                                let model = userModel as! StateListResponse
                                                self.arrOfState = model.item!
                                                self.tblSelectLoc.reloadData()
                                               

        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
     override func viewWillAppear(_ animated: Bool) {
        if self.location != "state"{
            if arrOfState != nil{
                self.arrOfState.removeAll()
            }
            self.callGetDistrictList()
        }
    }
    
  
    
    func callGetDistrictList(){
        
        let params = ["":""]
        let path = Bundle.main.path(forResource: SelectCityURLConstant.stateDistrictListEng, ofType: SelectCityURLConstant.jsonType)
        let fileUrl = NSURL(fileURLWithPath: path!)
        let apiURL = String(describing: fileUrl)
        _ = SelectDistrictServices().getDistrictList(apiURL,
                                               postData: params as [String : AnyObject],
                                               withSuccessHandler: { (userModel) in
                                                let model = userModel as! SelectStateDistrictResponse
                                                self.arrOfDistrict = model.item
                                                let district = self.arrOfDistrict as NSDictionary
                                                self.arrOfState = district.value(forKey: self.location) as! [Any]
                                                self.tblSelectLoc.reloadData()
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    @IBAction func btnCloseClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension SelectLocationPopUpView :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrOfState != nil{
            return self.arrOfState.count
        }
        return 0 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectLocationTblCell", for: indexPath) as! SelectLocationTblCell
        cell.selectionStyle = .none
        cell.lblOfLocationName.text = arrOfState[indexPath.row] as? String
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
        }else{
            delegate?.passSelectedLocation(arrOfState[indexPath.row] as! String)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
}
