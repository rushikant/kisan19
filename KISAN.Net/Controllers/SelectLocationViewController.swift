//
//  SelectLocationViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 17/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class SelectLocationViewController: UIViewController, PassSelectedLocation {
    var lblOfTitle = UILabel()
    @IBOutlet weak var viewOfBackSelectState: UIView!
    @IBOutlet weak var viewOfBackSelectDistrict: UIView!
    @IBOutlet weak var lblSelectState: UILabel!
    @IBOutlet weak var lblSelectDistrict: UILabel!
    var appDelegate = AppDelegate()

    
    var checkTapEvnt = String()
    var userId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblSelectDistrict.text = (NSLocalizedString(StringConstants.Validation.SelectDistricttitl, comment: StringConstants.EMPTY))
        lblSelectState.text = (NSLocalizedString(StringConstants.Validation.selectStateTitl, comment: StringConstants.EMPTY))

        // Do any additional setup after loading the view.
        self.initialDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        appDelegate = UIApplication.shared.delegate as! AppDelegate

    }

    func initialDesign() {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.next, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(nextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.selectLocation, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        
        viewOfBackSelectState.layer.cornerRadius = 5.0
        viewOfBackSelectState.clipsToBounds = true
        viewOfBackSelectDistrict.layer.cornerRadius = 5.0
        viewOfBackSelectDistrict.clipsToBounds = true
        self.dropShadow(view:viewOfBackSelectState)
        self.dropShadow(view:viewOfBackSelectDistrict)
        
        viewOfBackSelectDistrict.isHidden = true
    }
    
    func dropShadow(scale: Bool = true, view:UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: -1, height: 1)
        view.layer.shadowRadius = 1
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextTapped(sender: UIBarButtonItem) {
        
        if lblSelectState.text == (NSLocalizedString(StringConstants.Validation.selectStateTitl, comment: StringConstants.EMPTY)){
            
            popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectState, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                
                },{action2 in
                    
                }, nil])
            
        }else if lblSelectDistrict.text == (NSLocalizedString(StringConstants.Validation.SelectDistricttitl, comment: StringConstants.EMPTY)){
            
            popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectCity, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                
                },{action2 in
                    
                }, nil])
            
        }else{
            self.editUserProfileForCategories()

        }
    }
    
    
    func editUserProfileForCategories(){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let first_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        let last_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        let oAuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let mobileNum = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
        let imageBigthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
        let imageSmallthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
        let imageUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageUrl)
        let strCategories = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.categories)
        
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)
        
        let params = EditUserDetailsRequest.convertToDictionary(userDetails: EditUserDetailsRequest.init(firstName: first_name, lastName: last_name, mobile: mobileNum, accountType: account_Type ?? Int(), password: userId, country: StringConstants.country, state: lblSelectState.text!, city: lblSelectDistrict.text!, profile_status: StringConstants.EMPTY, image_url: imageUrl, big_thumb_url: imageBigthumbUrl, small_thumb_url: imageSmallthumbUrl, categories: strCategories, email: StringConstants.EMPTY, about: StringConstants.EMPTY, imageBlob: StringConstants.EMPTY, oAuthToken: oAuthToken, source: RequestName.loginSourceName))
        print("params",params)
        
        let apiURL = URLConstant.BASE_URL + URLConstant.editUserProfile + userId
        _ = EditUserProfileServices().editUserProfileServices(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! EditUserProfileResponse
                                                                print("model",model)
                                                                MBProgressHUD.hide(for: self.view, animated: true);
                                                           SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                                                                
                                                                let userData = model.user_details[0]
                                                                let userProfile = userData.userProfile[0]
                                                                var interests = [String]()
                                                                interests = userProfile.interests!
                                                                
                                                                let strInterests = interests.joined(separator: ",")
                                                                if interests.count > 0 {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }
                                                                
                                                                if !(userProfile.state?.isEmpty)!{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: userProfile.state!, keyString: StringConstants.NSUserDefauluterKeys.state)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
                                                                }
                                                                if !(userProfile.city?.isEmpty)!{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: userProfile.city!, keyString: StringConstants.NSUserDefauluterKeys.city)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
                                                                }
                                                                
                                                                if account_Type == 2 {
                                                                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                                    let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                                                                    self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
                                                                }else{
                                                                    //                                                                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                                                                    //                                                                    let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as UIViewController
                                                                    //                                                                    self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
                                                                    
                                                                   /* let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                                                                    self.navigationController?.pushViewController(dashboardViewController, animated: true)*/
                                                                    self.sighIntoGreenCloud()
                                                                    
                                                                }
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
 
    }
    
    func openPopUpConditionally(){
        
        appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)

        
       /* let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
        
        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
                var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                if(isMitra && show_issue_pass == 0){
                    if(pendingInviteCount>0){
                        if(!isInviteViewed){
                            appDelegate.isLaunchFirstTime = false
                            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                            self.navigationController?.pushViewController(vc, animated: false)
                        }else{
                            appDelegate.isLaunchFirstTime = false
                        }
                    }else{
                        if(myGreenPassCount>0){
                            appDelegate.isLaunchFirstTime = false
                            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                            self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        }else{
                            appDelegate.isLaunchFirstTime = false
                            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                            let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                            self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                        }
                    }
                }else{
                    if(pendingInviteCount>0){
                        if(!isInviteViewed){
                            appDelegate.isLaunchFirstTime = false
                            let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                            self.navigationController?.pushViewController(vc, animated: false)
                        }else{
                            appDelegate.isLaunchFirstTime = false
                            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                            let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                            self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                        }
                    }else if(remaining>0){
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }else{
                        if(myGreenPassCount>0){
                            appDelegate.isLaunchFirstTime = false
                            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                            self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        }else{
                            appDelegate.isLaunchFirstTime = false
                            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                            let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                            self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                        }
                    }
                }
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
            self.navigationController?.pushViewController(dashboardViewController, animated: true)
        }
    */

    }
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        var AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            if model.inviteSummary.count > 0{
                                                                
                                                                let inviteSummary = model.inviteSummary[0]
                                                                let standard =  inviteSummary.unlimited_standard
                                                                let extra = inviteSummary.unlimited_extra
                                                                let sent = inviteSummary.unlimited_sent
                                                                let medialink =  inviteSummary.media_link
                                                                let mediatype = inviteSummary.media_type
                                                                let skip_media = inviteSummary.skip_media
                                                                let channelName = inviteSummary.channel_name
                                                                let channelColour = inviteSummary.channel_max_color
                                                                let mediathumb = inviteSummary.media_thumbnail
                                                                let imagechannelProfile = inviteSummary.channel_big_thumb
                                                                
                                                                UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                                
                                                                UserDefaults.standard.set(extra, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.extra)
                                                                
                                                                UserDefaults.standard.set(sent, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.sent)
                                                                
                                                                
                                                                UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                                
                                                                
                                                                UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                                
                                                                UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                                
                                                                
                                                                UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                                
                                                                UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                                
                                                                UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                                
                                                                UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                                
                                                            }
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                            
                                                            let greenPassArray = model.greenpasses
                                                            
                                                            let myGreenPassCount = greenPassArray.count
                                                            
                                                            UserDefaults.standard.set(myGreenPassCount, forKey:
                                                                StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                            
                                                            
                                                           /* //signInTo Ejjaberd
                                                            var ejabberdUserId = userId
                                                            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
                                                            
                                                            print("ejabberdUserId",ejabberdUserId ?? String())
                                                            print("Password as middlewareToken",middlewareToken ?? String())
                                                            print("hostName",URLConstant.hostName)*/
                                                            
                                                            self.openPopUpConditionally()
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
             },{action2 in
             }, nil])
            //signInTo Ejjaberd
            
            print("ejabberdUserId",ejabberdUserId ?? String())
            print("Password as middlewareToken",middlewareToken ?? String())
            print("hostName",URLConstant.hostName)*/
        })
    }
    

    @IBAction func btnSelectStateClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationPopUpView") as! SelectLocationPopUpView
        vc.location = "state"
        checkTapEvnt = "state"
        lblSelectDistrict.text = (NSLocalizedString(StringConstants.Validation.SelectDistricttitl, comment: StringConstants.EMPTY))
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnSelectDistrictClick(_ sender: Any) {
        if (lblSelectState.text != (NSLocalizedString(StringConstants.Validation.selectStateTitl, comment: StringConstants.EMPTY))){
            checkTapEvnt = "District"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationPopUpView") as! SelectLocationPopUpView
            vc.location = lblSelectState.text!
            vc.delegate = self
            self.present(vc, animated: false, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: step 6 finally use the method of the contract
    func passSelectedLocation(_ location:String) {
        if checkTapEvnt == "state"{
            lblSelectState.text = location
            
            if lblSelectState.text == (NSLocalizedString(StringConstants.Validation.selectStateTitl, comment: StringConstants.EMPTY)){
                viewOfBackSelectDistrict.isHidden = true
            }else{
                viewOfBackSelectDistrict.isHidden = false
            }
            
        }else if checkTapEvnt == "District"{
            lblSelectDistrict.text = location
        }
    }
}


