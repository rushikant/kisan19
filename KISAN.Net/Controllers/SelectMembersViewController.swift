//
//  SelectMembersViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 24/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import ContactsUI
import MBProgressHUD

class SelectMembersViewController: UIViewController,PassSelectedContactDetails,EPPickerDelegate {

    var flagOfCheckSelectContact = Bool()
    var strComingFrom = String()
    var communityKey = String()
    var arrOfReceiverNum = [String]()
    var manualEnteredNum = String()
    var dominantColour = String()
    
    @IBOutlet var lblselectcontactfromContactList: UILabel!
    @IBOutlet var txtEnterNumManually: UITextField!
    @IBOutlet var txtcontactFromContactList: UITextField!
    var strInviteMode = String()


    @IBOutlet weak var txtEnterNum: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        flagOfCheckSelectContact = true
        txtEnterNumManually.text = (NSLocalizedString(StringConstants.placeHolder.NoEnterManually, comment: StringConstants.EMPTY))
        lblselectcontactfromContactList.text = (NSLocalizedString(StringConstants.placeHolder.SelectContact, comment: StringConstants.EMPTY))

        initialDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("call this method")
        if flagOfCheckSelectContact == false{
            flagOfCheckSelectContact = true
            popupAlert(title: "", message:  (NSLocalizedString(StringConstants.Validation.selectOneContact, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        }
        CheckSendinvite()
    }
    
    func CheckSendinvite(){
        
        if strComingFrom == "ChannelProfileVC" {
            if arrOfReceiverNum.count == 0 {
               // self.navigationController?.view.makeToast(StringConstants.Validation.selectOneContact)
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
            }
        }else if strComingFrom == StringConstants.flags.dashboardViewController{
            if arrOfReceiverNum.count == 0 {
                //self.navigationController?.view.makeToast(StringConstants.Validation.selectOneContact)
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
            }
        }else{
            if arrOfReceiverNum.count == 0 {
                var myChatsList = [MyChatsTable]()
                myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                vc.myChatsData = myChatsList.last!
                vc.isFromcreateChannel = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
            }
        }
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.doneBtn, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(DoneTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        self.title = (NSLocalizedString(StringConstants.NavigationTitle.selectMembers, comment: StringConstants.EMPTY))
        
        if dominantColour.isEmpty {
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: dominantColour)
        }
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        if strComingFrom == StringConstants.flags.userAreaInterestViewController{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is DashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func DoneTapped(sender: UIBarButtonItem) {
        
         self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.selectOneContact, comment: StringConstants.EMPTY)))
        
//        if strComingFrom == "ChannelProfileVC" {
//            if arrOfReceiverNum.count == 0 {
//                self.navigationController?.view.makeToast(StringConstants.Validation.selectOneContact)
//            }else{
//                SVProgressHUD.show()
//                self.sendInvitation()
//            }
//        }else if strComingFrom == StringConstants.flags.dashboardViewController{
//            if arrOfReceiverNum.count == 0 {
//                self.navigationController?.view.makeToast(StringConstants.Validation.selectOneContact)
//            }else{
//                SVProgressHUD.show()
//                self.sendInvitation()
//            }
//        }else{
//            if arrOfReceiverNum.count == 0 {
//                var myChatsList = [MyChatsTable]()
//                myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
//                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
//                let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
//                vc.myChatsData = myChatsList.last!
//                vc.isFromcreateChannel = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                SVProgressHUD.show()
//                self.sendInvitation()
//            }
//        }
    }
    
    func sendInvitation(){
        let currentTime = DateTimeHelper.getCurrentMillis()
        let sendBy = String(SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)) + " " + String(SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName))
        let sender = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let sendInvitationDetails = SendInvitationDetailsRequest.convertToDictionary(sendInvitationDetails: SendInvitationDetailsRequest.init(appLanguage: StringConstants.pushNotificationConstants.notificationLanguageType, communityKey: communityKey, initiated: String(currentTime), invitationType: StringConstants.pushNotificationConstants.appInvitation, purposeCode: StringConstants.pushNotificationConstants.invite, receiver: arrOfReceiverNum, sendBy: sendBy, sender: sender, sentOn: StringConstants.EMPTY, status: StringConstants.pushNotificationConstants.created))
        let params = SendInvitationRequest.convertToDictionary(sendInvitationRequest: SendInvitationRequest.init(invitation: sendInvitationDetails))
        print("params",params)
        
        let apiURL = URLConstant.BASE_URL + URLConstant.invitation
        _ = SendInvitationServices().sendInvitation(apiURL,
                                      postData: params as [String : AnyObject],
                                      withSuccessHandler: { (userModel) in
                                        let model = userModel as! SendInvitationResponse
                                        print("model",model)
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.InvitationSent, comment: StringConstants.EMPTY)))
                                        if self.strComingFrom == "ChannelProfileVC"{
                                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.InvitationSent, comment: StringConstants.EMPTY)))
                                            //self.strComingFrom = StringConstants.EMPTY
                                            if self.strInviteMode == StringConstants.flags.Invitemanually
                                            {
                                                self.strInviteMode = StringConstants.EMPTY
                                            }
                                            else{
                                            if let nav = self.navigationController {
                                                
                                                for controller in nav.viewControllers as Array {
                                                    if controller is ChannelProfileVC {
                                                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                        break
                                                    }
                                                }
                                                return
                                            }
                                        }
                                        }else if self.strComingFrom == StringConstants.flags.dashboardViewController{
                                         self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.InvitationSent, comment: StringConstants.EMPTY)))
                                            // self.strComingFrom = StringConstants.EMPTY
                                            
                                            if self.strInviteMode == StringConstants.flags.Invitemanually
                                            {
                                                self.strInviteMode = StringConstants.EMPTY
                                            }
                                            else{
                                            if let nav = self.navigationController {
                                                for controller in nav.viewControllers as Array {
                                                    if controller is DashboardViewController {
                                                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                        break
                                                    }
                                                }
                                                return
                                            }
                                        }
                                        }else{
                                            var myChatsList = [MyChatsTable]()
                                            myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
                                            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                                            vc.myChatsData = myChatsList.last!
                                            vc.isFromcreateChannel = true
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSelectNumFromPhoneBookClick(_ sender: Any) {
        /*strInviteMode =  StringConstants.flags.InviteFromContact
        let cnPicker = CNContactPickerViewController()
        cnPicker.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        cnPicker.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        cnPicker.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        cnPicker.delegate = self*/
        
        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:true, subtitleCellType: SubtitleCellValue.email)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.present(navigationController, animated: true, completion: nil)
        
        UISearchBar.appearance().backgroundColor = UIColor.white
        let cancelButtonAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        UIBarButtonItem.appearance(whenContainedInInstancesOf:
            [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)
       // self.present(cnPicker, animated: true, completion: nil)
    }
    
    @IBAction func btnEnterManullyClick(_ sender: Any) {
        strInviteMode =  StringConstants.flags.Invitemanually
        let invitemanuallyViewController = self.storyboard?.instantiateViewController(withIdentifier: "InvitemanuallyViewController") as! InvitemanuallyViewController
        invitemanuallyViewController.delegate = self
        self.navigationController?.pushViewController(invitemanuallyViewController, animated: false)
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedContactDetails(_ contact:String) {
        print("contact",contact)

       // txtEnterNum.text = contact
        manualEnteredNum = "+91" + contact
        arrOfReceiverNum.append(manualEnteredNum)
    }
   
    
    //MARK:- EPContactPickerDelegate Method
    
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError)
    {
        print("Failed with error \(error.description)")
    }
    
    internal func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact)
    {
        print("Contact \(contact) has been selected")
    }
    
    func epContactPicker(_: EPContactsPicker, didCancel error : NSError)
    {
        print("Cancel Contact Picker")
        strInviteMode =  StringConstants.flags.InviteFromContact
    }
    
    
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) {
        print("The following contacts are selected")
        for contact in contacts {
            let dict = (contact.phoneNumbers[0])
            print("\(dict.phoneNumber)")
            let mobilenumber = dict.phoneNumber
            var mobileNum = mobilenumber.replacingOccurrences( of:"-", with: "", options: .regularExpression)
            let nonWhiteCharacters = mobileNum.unicodeScalars.filter {
                false == NSCharacterSet.whitespacesAndNewlines.contains($0)
                }.map(Character.init)
            var whitespacelessMobileNumber = String(nonWhiteCharacters)
            whitespacelessMobileNumber = "+91" + whitespacelessMobileNumber
            arrOfReceiverNum.append(whitespacelessMobileNumber)
            print("arrOfReceiverNum is = \(arrOfReceiverNum)")
        }
        
        if contacts.count == 0 {
            flagOfCheckSelectContact = false
        }else{
            if strComingFrom == "ChannelProfileVC"{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
            }else if strComingFrom == StringConstants.flags.dashboardViewController{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
                
            }else if strComingFrom == StringConstants.flags.channelSendBroadcastMsgVC{
                strComingFrom = ""
                if let nav = self.navigationController {
                    for controller in nav.viewControllers as Array {
                        if controller is ChannelSendBroadcastMsgVC {
                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                            break
                        }
                    }
                    return
                }
                flagOfCheckSelectContact = true
                
            }
            else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
            }
        }
        strInviteMode =  StringConstants.flags.InviteFromContact
        
    }
    
}

extension SelectMembersViewController : CNContactPickerDelegate{
    //MARK:- CNContactPickerDelegate Method
   /* func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            for _ in contact.phoneNumbers {
                let phoneNumber  : [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
                let firstPhoneNumber:CNPhoneNumber = phoneNumber[0].value
                let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
                
                var mobileNum = primaryPhoneNumberStr.replacingOccurrences( of:"-", with: "", options: .regularExpression)
                let nonWhiteCharacters = mobileNum.unicodeScalars.filter {
                    false == NSCharacterSet.whitespacesAndNewlines.contains($0)
                    }.map(Character.init)
                var whitespacelessMobileNumber = String(nonWhiteCharacters)
                whitespacelessMobileNumber = "+91" + whitespacelessMobileNumber
                arrOfReceiverNum.append(whitespacelessMobileNumber)
                print("arrOfReceiverNum is = \(arrOfReceiverNum)")
            }
        }
        
        if contacts.count == 0 {
            flagOfCheckSelectContact = false
        }else{
            if strComingFrom == "ChannelProfileVC"{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
            }else if strComingFrom == StringConstants.flags.dashboardViewController{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
                
            }else if strComingFrom == StringConstants.flags.channelSendBroadcastMsgVC{
                strComingFrom = ""
                if let nav = self.navigationController {
                    for controller in nav.viewControllers as Array {
                        if controller is ChannelSendBroadcastMsgVC {
                            let _ = nav.popToViewController(controller as UIViewController, animated: false)
                            break
                        }
                    }
                    return
                }
                flagOfCheckSelectContact = true
                
            }
            else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                self.sendInvitation()
                flagOfCheckSelectContact = true
            }
        }
        strInviteMode =  StringConstants.flags.InviteFromContact

    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
        strInviteMode =  StringConstants.flags.InviteFromContact

    }
   */
    
    
    
}
