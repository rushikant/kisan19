//
//  SelectMultiMediaPopUpVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 15/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices

class SelectMultiMediaPopUpVC: UIViewController{
    let picker = UIImagePickerController()
    @IBOutlet weak var topOfbackViewOfPopup: NSLayoutConstraint!
    @IBOutlet weak var tblPopUp: UITableView!
    var arrOfCameraMutiMediaNm = [String]()
    var arrOfCameraMultiMediaImgName = [String]()
    // Capture video from camera
    var videoURL: URL?
    var capturImage = UIImage()
    var selectedIndex = Int()
    var backroundImage = UIImage()
    var checkMediaType = String()
    @IBOutlet weak var imageViewOfBCKImg: UIImageView!
    @IBOutlet weak var viewOfBackPopup: UIView!
    var commingFrom = String()
    @IBOutlet weak var heightOfPopUpView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        if checkMediaType == "Camera" {
            arrOfCameraMutiMediaNm.append((NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)))
            arrOfCameraMutiMediaNm.append((NSLocalizedString(StringConstants.popUpMenuName.recordavideo, comment: StringConstants.EMPTY)))
            arrOfCameraMultiMediaImgName.append(StringConstants.ImageNames.cameraBlack)
            arrOfCameraMultiMediaImgName.append(StringConstants.ImageNames.videoBlack)
            heightOfPopUpView.constant =  100
        }else{
            arrOfCameraMutiMediaNm.append((NSLocalizedString(StringConstants.popUpMenuName.recordAudio, comment: StringConstants.EMPTY)))
            //arrOfCameraMutiMediaNm.append(StringConstants.popUpMenuName.audioFrmGallary)
            arrOfCameraMultiMediaImgName.append(StringConstants.ImageNames.audioRecord)
            //arrOfCameraMultiMediaImgName.append(StringConstants.ImageNames.audioRecordGallery)
            heightOfPopUpView.constant =  50
        }
        
        tblPopUp.tableFooterView = UIView()
        tblPopUp.separatorColor = UIColor.clear
        picker.delegate = self
        imageViewOfBCKImg.image = backroundImage
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SelectMultiMediaPopUpVC.dismissPopUp))
        viewOfBackPopup.addGestureRecognizer(tap)
    }
    
        @objc func dismissPopUp() {
            self.navigationController?.popViewController(animated: false)
        }
    
    override func viewWillAppear(_ animated: Bool){
        navigationController?.setNavigationBarHidden(true, animated: false)
        if UIScreen.main.bounds.size.height <= 568 {
            topOfbackViewOfPopup.constant = 200
        }else{
            topOfbackViewOfPopup.constant = 263
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension SelectMultiMediaPopUpVC :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOfCameraMutiMediaNm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMultiMediaPopUpCell", for: indexPath) as! SelectMultiMediaPopUpCell
        cell.selectionStyle = .none
        cell.lblOfMediaName.text = arrOfCameraMutiMediaNm[indexPath.row]
        cell.imgWidthconstant.constant = 19;
        cell.imgHeightConstant.constant = 18;
        cell.imageViewOfMedia.image = UIImage(named:arrOfCameraMultiMediaImgName[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if checkMediaType == "Camera" {
            if indexPath.row == 0 {
                selectedIndex = indexPath.row
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerControllerSourceType.camera
                picker.cameraCaptureMode = .photo
                picker.modalPresentationStyle = .fullScreen
                present(picker,animated: true,completion: nil)
                
            }else{
                selectedIndex = indexPath.row
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let picker = UIImagePickerController()
                    picker.delegate = self
                    picker.allowsEditing = true
                    picker.sourceType = .camera
                    picker.mediaTypes = [(kUTTypeMovie as? String)] as? [String] ?? [String]()
                    self.present(picker, animated: true, completion: nil)
                }
            }
        }else{
            if indexPath.row == 0 {
                selectedIndex = indexPath.row
                let recordAudioViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecordAudioViewController") as! RecordAudioViewController
                recordAudioViewController.commingFrom = commingFrom
                self.navigationController?.pushViewController(recordAudioViewController, animated: false)
            }else{
                selectedIndex = indexPath.row
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 50
    }
    
}

extension SelectMultiMediaPopUpVC :
    UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        videoURL = nil
        capturImage = UIImage()
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            if selectedIndex == 0 {
                if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    dismiss(animated: true, completion: nil)
                    let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                    let imageHelper = ImageHelper()
                    let upOrientationImage  = imageHelper.fixOrientationOfImage(image: image)
                    capturImage = upOrientationImage!
                    setCaptionViewController.commingFrom = commingFrom
                    setCaptionViewController.selectedImage = capturImage
                    self.navigationController?.pushViewController(setCaptionViewController, animated: false)
                }
            }else{
                videoURL = info[UIImagePickerControllerMediaURL] as? URL
                self.dismiss(animated: true, completion: nil)
                let setCaptionViewController = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "SetCaptionViewController") as! SetCaptionViewController
                 setCaptionViewController.commingFrom = commingFrom
                setCaptionViewController.selectedVideoURL = videoURL
                self.navigationController?.pushViewController(setCaptionViewController, animated: false)
            }
        }else{
            print("Gallary")
        }
    }
}
