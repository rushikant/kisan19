//
//  SelectStallListPopUpVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 19/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here
protocol PassSelectedStallDetails: class {
    func passSelectedStallDetails(_ stallDetails: [BookedStallInfoDetails]?)
}

class SelectStallListPopUpVC: UIViewController {
    
    @IBOutlet weak var heightOfViewOnPopUp: NSLayoutConstraint!
    @IBOutlet weak var tblSelectStallList: UITableView!
    @IBOutlet weak var imageViewOfBackgroundImgcapture: UIImageView!
    var backImageCapturedValue = UIImage()
    @IBOutlet weak var heightOfTblSelectStallList: NSLayoutConstraint!
    weak var delegate: PassSelectedStallDetails?
    var  booked_stall_info: [BookedStallInfoDetails]?

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewOfBackgroundImgcapture.image = backImageCapturedValue
        // Do any additional setup after loading the view.
        tblSelectStallList.estimatedRowHeight = 44
        tblSelectStallList.rowHeight = UITableViewAutomaticDimension
        tblSelectStallList.tableFooterView = UIView()
    }
    
//    override func viewDidLayoutSubviews() {
//        super.updateViewConstraints()
//        heightOfTblSelectStallList.constant = CGFloat(self.categories_list.count * 75)
//        self.updateViewConstraints()
//    }
}

extension SelectStallListPopUpVC : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.booked_stall_info!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StallListCell", for: indexPath) as! StallListCell
        let stallInfo = booked_stall_info![indexPath.row]
        
        var stallName = stallInfo.stand_type_name!  + " - " +  stallInfo.stand_location_name! + " - "
        stallName = stallName + stallInfo.pavillion_name! + " - " + String(stallInfo.area!)
        stallName = stallName  + " sq.m." + " - " + "(" + String(stallInfo.id!) + ")"
        
        if stallInfo.isSelectedStall == StringConstants.ONE {
            cell.imageViewIfRedioBtn.image = UIImage(named: "radioBrown")
        }else{
            cell.imageViewIfRedioBtn.image = UIImage(named: "radioWhite")
        }
       
        cell.lblStallName.text = stallName
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stallInfo = booked_stall_info![indexPath.row]
        self.booked_stall_info?.filter({$0.isSelectedStall == StringConstants.ONE}).first?.isSelectedStall = StringConstants.ZERO
        self.booked_stall_info?.filter({$0.id == stallInfo.id!}).first?.isSelectedStall = StringConstants.ONE
        delegate?.passSelectedStallDetails(booked_stall_info)
        self.navigationController?.popViewController(animated: false)
    }

}
