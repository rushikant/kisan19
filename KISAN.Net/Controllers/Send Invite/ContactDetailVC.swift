//
//  ContactDetailVC.swift
//  KISAN.Net
//
//  Created by MacMini104 on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD


protocol ContactDetailDelegate : class {
    func updateContactDetail(contactDetail:PhoneContact)
}


class ContactDetailVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    weak var delegateObj: ContactDetailDelegate?
    @IBOutlet weak var line3: UILabel!
    @IBOutlet weak var line2: UILabel!
    @IBOutlet weak var line1: UILabel!
    @IBOutlet weak var numberTxt: UITextField!
    @IBOutlet weak var lNameTxt: UITextField!
    @IBOutlet weak var FnameTxt: UITextField!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var lNameLbl: UILabel!
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet weak var scrollViewEdit: UIScrollView!
   
    @IBOutlet weak var viewBackPopUp: UIView!
    var profileImage: UIImage!
    var data:PhoneContact?
    var fName=""
    var lName=""
    var number=""
    var imagedata: Data?
    var picker:UIImagePickerController? = UIImagePickerController()
    var choseimage=UIImage()
    var isTouch =  false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker?.delegate = self
        
        
        validateData()
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissPopUp(_:)))
        tap1.cancelsTouchesInView = false
        tap1.delegate = self
        self.scrollViewEdit.addGestureRecognizer(tap1)
    }
    
    
    
    func validateData(){
        FnameTxt.text=data?.fName
        lNameTxt.text=data?.LName

        if data!.phoneNumber.count > 0 {
            var phoneNumber = data?.phoneNumber[0]
            phoneNumber = phoneNumber?.replacingOccurrences(of: "-", with: StringConstants.EMPTY)
            phoneNumber = phoneNumber?.replacingOccurrences(of: " ", with: StringConstants.EMPTY)
            phoneNumber = phoneNumber?.replacingOccurrences(of: "(", with: StringConstants.EMPTY)
            phoneNumber = phoneNumber?.replacingOccurrences(of: ")", with: StringConstants.EMPTY)
            numberTxt.text=phoneNumber
        }

        let strFname = data?.fName
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRangeFname = strFname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeFname != nil {
            print("Numbers found")
        }
        
        let strLname = data?.LName
        let decimalRangeLname = strLname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeLname != nil {
            print("Numbers found")
        }
        
        if data?.fName == StringConstants.EMPTY || (data?.fName?.length)!<2 || decimalRangeFname != nil {
            fnameLbl.textColor=UIColor.red
            line1.backgroundColor=UIColor.red
            self.view.makeToast((NSLocalizedString(StringConstants.Validation.nameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
            
        }
        if data?.LName == StringConstants.EMPTY || (data?.LName?.length)!<2 || decimalRangeLname != nil{
            lNameLbl.textColor=UIColor.red
            line2.backgroundColor=UIColor.red
            self.view.makeToast((NSLocalizedString(StringConstants.Validation.lastNameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }
        
        let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
        
        if let fName = data?.fName{
            if let range = fName.rangeOfCharacter(from: validString as CharacterSet){
                print(range)
                fnameLbl.textColor=UIColor.red
                line1.backgroundColor=UIColor.red
                self.view.makeToast((NSLocalizedString(StringConstants.Validation.nameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
            }
        }
        if let LName = data?.LName {
            if let range = LName.rangeOfCharacter(from: validString as CharacterSet){
                print(range)
                lNameLbl.textColor=UIColor.red
                line2.backgroundColor=UIColor.red
                self.view.makeToast((NSLocalizedString(StringConstants.Validation.lastNameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
            }
        }


        if data!.phoneNumber.count > 0 {
            if data?.phoneNumber[0] == StringConstants.EMPTY && (data?.phoneNumber[0].length)! < 10{
                numberLbl.textColor=UIColor.red
                line3.backgroundColor=UIColor.red
                self.view.makeToast((NSLocalizedString(StringConstants.Validation.phoneNumValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
            }else if (!isvalidContact(selectedContact: (data?.phoneNumber[0])!)){
                
                numberLbl.textColor=UIColor.red
                line3.backgroundColor=UIColor.red
                self.view.makeToast((NSLocalizedString(StringConstants.Validation.phoneNumValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
            }
        }else{
            
            numberLbl.textColor=UIColor.red
            line3.backgroundColor=UIColor.red
            self.view.makeToast((NSLocalizedString(StringConstants.Validation.phoneNumValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
        }

        if((data?.imgIsAvailable)!){
            contactImg.image = UIImage.init(data: data!.avatarData!)
        }else{
            contactImg.image = UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg)
        }
        
    }
    
    
    @objc func dismissPopUp(_ sender: UITapGestureRecognizer) {
        if isTouch{
          // self.dismiss(animated: true, completion: nil)
        }
    }

    //Delegate Methods
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if(touch.view == self.viewBackPopUp){
            isTouch = false
            return false
        }else{
            isTouch = true
            return true
        }
    }
    
    @IBAction func picPhotoAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.picker!.allowsEditing = false
            self.picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.picker!, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.picker!.allowsEditing = false
                self.picker!.sourceType = UIImagePickerController.SourceType.camera
                self.picker!.cameraCaptureMode = .photo
                self.present(self.picker!, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
        })
    }
    
    @IBAction func submitAciton(_ sender: UIButton) {
        
        
        if isfNameEmpty == false && islNameEmpty==false && isNumberEmpty==false{
            //submitBtn.isHidden=false
        }else{
            //submitBtn.isHidden=true
        }
        data?.fName = FnameTxt.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        data?.LName = lNameTxt.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)  
        data?.phoneNumber = [numberTxt.text!]
        
        validateData()
        if(isValidUser(phoneContact: data!)){
            delegateObj?.updateContactDetail(contactDetail: data!)
            self.dismiss(animated: true, completion: nil)
        }else{
            //self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.incomplete_information, comment: StringConstants.EMPTY)))
        }
    }
    
    
    
    
    // MARK: - UIImagePicker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        choseimage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
       
        
        dismiss(animated: true) { [unowned self] in
            self.profileImage = self.choseimage.fixOrientation()
            
            self.imagedata = UIImageJPEGRepresentation(self.choseimage, 0.3)
            self.contactImg.image=self.choseimage
            self.openEditor(nil)
            self.data?.avatarData = self.imagedata
            }
    }
    
   
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
    }



    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func uploadContactImgtoGreenCloud(){
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
            let appKey =  URLConstant.app_key
        _ = StringConstants.MediaType.image
            let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadVisitorProfileToImage //URLConstant.uploadFileVisitor
            print("ApiUrl",apiURL)
            let params = ["app_key":appKey,
                          "sessionId":sessionId]
            let fileName = StringConstants.file
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key,value) in params {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
                if let data = self.data!.avatarData{
                    let bytes = data.count
                    /// Description
                    _ = Double(bytes) / 1000.0 // Note the difference
                    let KB = Double(bytes) / 1024.0 // Note the difference
                    print(KB / 1024)
                    multipartFormData.append(data, withName: fileName, fileName: "invite_contact_img.png", mimeType: "image/jpeg")
                }
            },
                             to: apiURL,
                             method: .post,
                             encodingCompletion: { encodingResult in
                                switch encodingResult
                                {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        print(response)
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        guard let responseJSON = response.result.value as? [String:Any],
                                            let _ = responseJSON["message"] as? String,
                                            let status = responseJSON["success"] as? Bool else {
                                                return
                                        }
                                        if(status == false){
                                        }else{
                                            let data=responseJSON["imageDetails"]as!NSDictionary
                                            let imgFullUrl = String(describing: data["imgFullUrl"]!)
                                            self.data!.imgUrl = imgFullUrl
                                            self.data?.imgIsAvailable = true
                                        }
                                    }
                                    
                                case .failure( _ ):
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    self.popupAlert(title: "", message: "Something went wrong", actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                                        },{action2 in
                                        }, nil])
                                }
            })
        }


    //MARK: TextField Delegate
    var isfNameEmpty=true
    var islNameEmpty=true
    var isNumberEmpty=true
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = textField.text
        var search = ""
        if string.count > 0 { // if it was not delete character
            search = string2! + string1
        }
        else if (string2?.count)! > 0{ // if it was a delete character
            
            search = String(string2!.dropLast())
        }
        print("search",search)
        
    
        if !(search.trimmingCharacters(in: .whitespaces).count < 2){
            if textField == FnameTxt{
                isfNameEmpty=false
                 fnameLbl.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                 line1.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
            }else if textField == lNameTxt{
                islNameEmpty=false
                 lNameLbl.textColor=UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                 line2.backgroundColor=UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)

            }else if textField == numberTxt{
                isNumberEmpty=false
                if (!isvalidContact(selectedContact: search)){
                    numberLbl.textColor=UIColor.red
                    line3.backgroundColor=UIColor.red
                    isNumberEmpty=true
                }else{
                    numberLbl.textColor=UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                    line3.backgroundColor=UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
                }
            }
        }else{
            if textField == FnameTxt{
                fnameLbl.textColor=UIColor.red
                line1.backgroundColor=UIColor.red
                isfNameEmpty=true
            }else if textField == lNameTxt{
                lNameLbl.textColor=UIColor.red
                line2.backgroundColor=UIColor.red
                islNameEmpty=true
            }else if textField == numberTxt{
                numberLbl.textColor=UIColor.red
                line3.backgroundColor=UIColor.red
                isNumberEmpty=true
            }
        }
        
        let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")

        if textField == FnameTxt{
            if FnameTxt.text != nil{
                //Not allowed type emoji
                if #available(iOS 7, *){
                    if textField.isFirstResponder {
                        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                            // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                            return false
                        }
                    }
                }
                
                if let range = search.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
//                    fnameLbl.textColor=UIColor.red
//                    line1.backgroundColor=UIColor.red
//                    self.view.makeToast((NSLocalizedString(StringConstants.Validation.nameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
                    return false
                }
            }
        }
        
        if textField == lNameTxt{
            
            //Not allowed type emoji
            if #available(iOS 7, *){
                if textField.isFirstResponder {
                    if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                        // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                        return false
                    }
                }
            }
            
            if lNameTxt.text != nil{
                if let range = search.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
//                    lNameLbl.textColor=UIColor.red
//                    line2.backgroundColor=UIColor.red
//                    self.view.makeToast((NSLocalizedString(StringConstants.Validation.lastNameValidation, comment: StringConstants.EMPTY)), duration: 2.0, position: .center)
                    return false
                }
            }
        }
        
        if isfNameEmpty == false && islNameEmpty==false && isNumberEmpty==false{
            //submitBtn.isHidden=false
        }else{
            //submitBtn.isHidden=true
        }
        if(textField == numberTxt){
            let currentText = textField.text ?? StringConstants.EMPTY
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 10
        }
        
        if(textField == FnameTxt || textField == lNameTxt){
            
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
                return false
            } else {
                guard range.location == 0 else {
                    return true
                }
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                //return true
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
  
    
    
    func isValidUser(phoneContact:PhoneContact)->Bool{
      
        let strFname = data?.fName
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRangeFname = strFname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeFname != nil {
            print("Numbers found")
        }
        
        let strLname = data?.LName
        let decimalRangeLname = strLname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeLname != nil {
            print("Numbers found")
        }
        
        
        let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
        
        if let fName = data?.fName{
            if let range = fName.rangeOfCharacter(from: validString as CharacterSet){
                print(range)
                return false
            }
        }
        if let LName = data?.LName {
            if let range = LName.rangeOfCharacter(from: validString as CharacterSet){
                print(range)
                return false
            }
        }
        
        if((data?.phoneNumber.count)! > 0){
            if data?.fName == StringConstants.EMPTY ||  data?.LName == StringConstants.EMPTY || (data?.fName?.length)! < 2 || (data?.LName?.length)! < 2 || !isvalidContact(selectedContact: phoneContact.phoneNumber[0]) || decimalRangeLname != nil || decimalRangeFname != nil {
                return false
            }else{
                return true
            }
        }else{
            return false
        }
    }
    
    func isvalidContact(selectedContact: String)-> Bool {
        var phoneNumber = selectedContact//Replace it with the Phone number you want to validate
        phoneNumber = phoneNumber.replacingOccurrences(of: "-", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: " ", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: StringConstants.EMPTY)
        
       
        
        if(phoneNumber.contains(" ") ||  phoneNumber.contains("-") ||  phoneNumber.contains("(")  || phoneNumber.contains(")") || phoneNumber.length < 10 ||  phoneNumber.first == "0"  ||  phoneNumber.first == "2" ||  phoneNumber.first == "3" ||  phoneNumber.first == "4" || phoneNumber.first == "5" || phoneNumber.first == "6" || phoneNumber.first == "1"){
            return false
        }else{
            return true
        }
    }
    
    
}
extension ContactDetailVC: UIImageCropperProtocol {
    
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        contactImg.image = croppedImage
        let imgdata:NSData = UIImageJPEGRepresentation(contactImg.image!, 0.7)! as NSData
        data!.avatarData = imgdata as Data
        self.uploadContactImgtoGreenCloud()
        picker!.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker!.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
