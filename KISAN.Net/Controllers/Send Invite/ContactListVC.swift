//
//  ContactListVC.swift
//  KISAN.Net
//
//  Created by MacMini on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import ContactsUI
import MBProgressHUD

class ContactListVC: AppViewController, UITextFieldDelegate,PassSelectedLanguage,PassSelectedStallDetails {
    var phoneContacts = [PhoneContact]() // array of PhoneContact(It is model find it below)
    var filter: ContactsFilter = .none
    @IBOutlet weak var contactTblView: UITableView!
    @IBOutlet weak var contactCountLbl: UILabel!
    @IBOutlet weak var quotaCountLbl: UILabel!
    @IBOutlet var serachTxt: UITextField!
    var checkedArray=NSMutableArray()
    var filterArray=NSMutableArray()
    @IBOutlet weak var nextView: UIView!
    var isFirst=true
    var guestPhonenumbersArray = NSMutableArray()
    var guestListArray = [GuestListDetails]()
    var exhibitorBadgesListArray = [ExhibitorBadges]()

    var comingFrom = String()
    var isFirstTime = Bool()
    var appDelegate = AppDelegate()
    var shareContactListArray = [String]()

    @IBOutlet weak var lblIssueBadgeLastdate: UILabel!
    @IBOutlet weak var viewOfBadgesIssue: UIView!
    @IBOutlet weak var topOfSearchbarViewConstant: NSLayoutConstraint!
    @IBOutlet weak var imgAlredySent: UIImageView!
    @IBOutlet weak var heightOfNavViewConstant: NSLayoutConstraint!
    @IBOutlet weak var heightOfBadgeIssueLastDateConstant: NSLayoutConstraint!
    @IBOutlet weak var lblNext: UILabel!
    var  booked_stall_info: [BookedStallInfoDetails]?
    var stallId = String()
    var flagOfLanuchTime = Bool()
    var issueBadgeQuotaBalance = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        // self.loadContacts(filter: filter) // Calling loadContacts methods
        // Do any additional setup after loading the view.
        contactTblView.tableFooterView = UIView()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        isFirstTime = true
        initialDesign()
    }
    
    func initialDesign(){
        
        if(comingFrom ==  StringConstants.flags.issueBadges){
            self.setInfoLabelDataForIssueBadges(commingFrom: "atlaunchTime")
            let nc = NotificationCenter.default
            nc.post(name: NSNotification.Name(rawValue: "getBalanceQuota"), object: nil, userInfo: ["value" : "noValue"])
            flagOfLanuchTime = true
        }else{
            lblIssueBadgeLastdate.text = StringConstants.EMPTY
        }
    }
    
    func setInfoLabelDataForIssueBadges(commingFrom:String){
        
        var stallName = String()
        if booked_stall_info!.count > 0 {
            if commingFrom == "atlaunchTime" {
                let stallInfo = booked_stall_info![0]
                stallName = stallInfo.stand_type_name!  + " - " +  stallInfo.stand_location_name! + " - "
                stallName = stallName + stallInfo.pavillion_name! + " - " + String(stallInfo.area!)
                stallName = stallName  + " sq.m." + " - " + "(" + String(stallInfo.id!) + ")"
                stallId = String(stallInfo.id!)
                self.booked_stall_info?.filter({$0.id == stallInfo.id!}).first?.isSelectedStall = StringConstants.ONE
            }else{
                let indexValue =  booked_stall_info?.index(where: { $0.isSelectedStall == StringConstants.ONE })
                let stallInfo = booked_stall_info![indexValue!]
                stallName = stallInfo.stand_type_name!  + " - " +  stallInfo.stand_location_name! + " - "
                stallName = stallName + stallInfo.pavillion_name! + " - " + String(stallInfo.area!)
                stallName = stallName  + " sq.m." + " - " + "(" + String(stallInfo.id!) + ")"
                stallId = String(stallInfo.id!)
            }
        }else{
            stallName = StringConstants.EMPTY
            stallId = StringConstants.EMPTY
        }
        let strChangeStall = " Change stall"
        let partOneAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)]
        let partTwoAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold)] as [NSAttributedStringKey : Any]
        let partThreeAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)] as [NSAttributedStringKey : Any]
        
        let partOne = NSMutableAttributedString(string: "The facility to issue badges will be available till ", attributes: partOneAttributes)
        
       var  exh_badge_enddatetimeValue = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime)
        exh_badge_enddatetimeValue =  exh_badge_enddatetimeValue + ". \n"
        
        let partOneSubPart = NSMutableAttributedString(string: exh_badge_enddatetimeValue, attributes: partThreeAttributes)
        let partTwo = NSMutableAttributedString(string: "You are currently issuing the badges for \n", attributes: partOneAttributes)
        
        
        let partThree = NSMutableAttributedString(string: stallName, attributes: partTwoAttributes)
        let partFour = NSMutableAttributedString(string: strChangeStall, attributes: partThreeAttributes)
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partOneSubPart)
        combination.append(partTwo)
        
        combination.append(partThree)
        
        if booked_stall_info!.count > 1 {
            combination.append(partFour)
        }
        
        self.lblIssueBadgeLastdate.attributedText = combination
        
        let range2 = (lblIssueBadgeLastdate!.text! as NSString).range(of: strChangeStall)
        combination.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.0
        combination.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, combination.length))
        self.lblIssueBadgeLastdate.attributedText = combination
        let tap = UITapGestureRecognizer(target: self, action: #selector(ContactListVC.tapFunctionOnInfoLabel))
        lblIssueBadgeLastdate.isUserInteractionEnabled = true
        if booked_stall_info!.count > 1 {
            lblIssueBadgeLastdate.addGestureRecognizer(tap)
        }
        
        // Get BalanceQuota API call
        self.getBalanceQuota()
    }
    
    func getBalanceQuota(){
        
        let stallid = stallId
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginExhibitorSourceName
        let app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let params = GetStallDetailsRequest.convertToDictionary(getStallDetailsRequest:GetStallDetailsRequest.init(app_key: app_key,
                                                                                                                   eventCode: URLConstant.eventCode,
                                                                                                                   source: source,
                                                                                                                   stallid: stallid,
                                                                                                                   sessionId: sessionId))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getstalldetails
        _ = GetStallDetailsServices().getStallDetails(apiURL,postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let model = userModel as! GetStallDetailsResponse
                                                        print("model",model)
                                                        let stallInfo = model.stallInfo
                                                        print("RushiexhstallInfo",stallInfo)
                                                        let quota_details = stallInfo.quota_details
                                                        let quota_balance = quota_details?[0].balance
                                                        if quota_balance != nil {
                                                            
                                                            let quotaBal = String(quota_balance!)
                                                            self.issueBadgeQuotaBalance = quota_balance!
                                                            let nc = NotificationCenter.default
                                                            nc.post(name: NSNotification.Name(rawValue: "getBalanceQuota"), object: nil, userInfo: ["value" : quotaBal])
                                                        }
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    
    @IBAction func tapFunctionOnInfoLabel(gesture: UITapGestureRecognizer) {
        self.openPopUpForStallList()
    }
    
    func openPopUpForStallList(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "SelectStallListPopUpVC") as! SelectStallListPopUpVC
            vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        vc.booked_stall_info = booked_stall_info
        vc.delegate = self
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedStallDetails(_ stallDetails:[BookedStallInfoDetails]?) {
        print("pass Details")
        booked_stall_info?.removeAll()
        booked_stall_info = stallDetails
        flagOfLanuchTime = false
       // setInfoLabelDataForIssueBadges(commingFrom: "")
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(comingFrom ==  StringConstants.flags.shareYourFriendsVc){
            lblNext.text = StringConstants.kisanMitra.shareWithFriends
            viewOfBadgesIssue.isHidden = true
            heightOfBadgeIssueLastDateConstant.constant = 0
            
        }else{
            if(comingFrom ==  StringConstants.flags.issueBadges){
                if flagOfLanuchTime == false{
                    setInfoLabelDataForIssueBadges(commingFrom: "")
                }
                nextView.backgroundColor =  UIColor.init(hexString: ColorConstants.marunColor)
                viewOfBadgesIssue.isHidden = false
                heightOfBadgeIssueLastDateConstant.constant = 50
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = true
                showBadgesListAPi()
            }else{
             lblNext.text = StringConstants.kisanMitra.next
             nextView.backgroundColor =  UIColor.init(hexString: ColorConstants.colorPrimaryString)
             let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
             spinnerActivity.isUserInteractionEnabled = true
             showInviteGuestListAPi()
                viewOfBadgesIssue.isHidden = true
                heightOfBadgeIssueLastDateConstant.constant = 0

            }
        }
        isFirst = true
        if checkedArray.count == 0{
            self.loadContacts(filter: filter)
            nextView.isHidden=true
           // contactTblView.frame.size.height=contactTblView.frame.height+nextView.frame.height
            heightOfNavViewConstant.constant =  0
        }else{
            nextView.isHidden=false
            //nextView.backgroundColor =  UIColor.init(hexString: ColorConstants.marunColor)
            //contactTblView.frame.size.height=contactTblView.frame.height-nextView.frame.height
            let valueOfSelectedContact = checkedArray.count
            let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
             if checkedArray.count > 1 {
                contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
             }else{
                contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
            }
            heightOfNavViewConstant.constant = 63
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        flagOfLanuchTime = false
    }
    
    func showBadgesListAPi(){
        var  source = String()
        var passtype : String
        var app_key :String
        passtype = RequestName.onlineexhibitorbadge
        source = RequestName.loginExhibitorSourceName
        app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let filter = ["stall_id": stallId,"sendpass": "1","csv_id":""]
        let params = GetBadgesListRequest.convertToDictionary(getBadgesListRequest:GetBadgesListRequest.init(app_key: app_key,  eventCode: URLConstant.eventCode, source: source, pagesize: 1000, currentpage: 0, passtype: passtype,filter:filter))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getbadgeslist
        _ = GetBadgesListServices().getBadgesList(apiURL,postData: params as [String : AnyObject],
                                                  withSuccessHandler: { (userModel) in
                                                    let model = userModel as! GetBadgesListResponse
                                                    print("model",model)
                                                    self.filterArray.removeAllObjects()
                                                    let exhibitorBadges = model.exhibitorBadges
                                                    print("exhibitorBadges",exhibitorBadges)
                                                    self.exhibitorBadgesListArray = exhibitorBadges
                                                    self.guestPhonenumbersArray.removeAllObjects()
                                                    for temp in self.exhibitorBadgesListArray{
                                                        let gusetNum = self.removeSpaces(number: temp.mobile!)
                                                        self.guestPhonenumbersArray.add(gusetNum)
                                                    }
                                                    
                                                    self.filterArray.addObjects(from: self.phoneContacts)
                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                    self.contactTblView.reloadData()
                                                    
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            self.contactTblView.reloadData()
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

    func openPopUpForLanguageSelection(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        vc.delegate = self
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0]
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedLanguage(_ language:String) {
        appDelegate.strSelectedLanguage = language
        isFirstTime = false
        print("appDelegate.strSelectedLanguage",appDelegate.strSelectedLanguage)
        self.navigationController?.popViewController(animated: false)
    }

    
    fileprivate func loadContacts(filter: ContactsFilter){
        phoneContacts.removeAll()
        var allContacts = [PhoneContact]()
        for contact in PhoneContacts.getContacts(filter: filter) {
            allContacts.append(PhoneContact(contact: contact))
        }
        
        if(allContacts.isEmpty){
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!

            let messageStr = (productName as! String) + StringConstants.singleSpace + StringConstants.AlertMessage.enableContactPremissionText

            self.popupAlert(title: StringConstants.AlertMessage.contactsPemission, message: messageStr, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        }
        
        phoneContacts.append(contentsOf: allContacts)
        //phoneContacts.filter({$0.LName == StringConstants.EMPTY}).first?.LName = StringConstants.kisanMitra.Ji
        filterArray.removeAllObjects()
        filterArray.addObjects(from: phoneContacts)

        //$0.LName
        DispatchQueue.main.async {
            self.contactTblView.reloadData() // update your tableView having phoneContacts array
        }
    }
    
    func unwrapValue(lName:String?) -> String{
        guard let lNameValue = lName  else {
            return lName!
        }
        return lNameValue
    }
    
    func phoneNumberWithContryCode() -> [String] {
        
        let contacts = PhoneContacts.getContacts() // here calling the getContacts methods
        //        print(contact)
        var arrPhoneNumbers = [String]()
        for contact in contacts {
            for ContctNumVar: CNLabeledValue in contact.phoneNumbers {
                if let fulMobNumVar  = ContctNumVar.value as? CNPhoneNumber {
                    //let countryCode = fulMobNumVar.value(forKey: "countryCode") get country code
                    if let MccNamVar = fulMobNumVar.value(forKey: "digits") as? String {
                        arrPhoneNumbers.append(MccNamVar)
                    }
                }
            }
        }
        return arrPhoneNumbers // here array has all contact numbers.
    }
    
    
    // MARK: - UITextField Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let string1 = string
        let string2 = serachTxt.text
        var search = ""
        if string.count > 0 { // if it was not delete character
            search = string2! + string1
        }
        else if (string2?.count)! > 0{ // if it was a delete character
            
            search = String(string2!.dropLast())
        }
        print(search)
        
        if search.count != 0 {
            
            filterArray.removeAllObjects()
            let i=phoneContacts.count
            for item in 0..<i {
                let str=(phoneContacts[item]).name
                let stringRange:NSRange = (str as AnyObject).range(of: search, options: .caseInsensitive)
                if stringRange.length != 0 {
                    filterArray.add(phoneContacts[item])
                }
            }
        }else {
            filterArray.removeAllObjects()
            filterArray.addObjects(from: phoneContacts)
        }
        contactTblView.reloadData()
        return true;
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK:IBAction Methods
    
    @IBAction func nextAction(_ sender: UIButton) {
        if(comingFrom ==  StringConstants.flags.shareYourFriendsVc){
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = true
            for temp in checkedArray{
                let phoneconatcat = temp as! PhoneContact
                shareContactListArray.append(removeSpaces(number:phoneconatcat.phoneNumber[0]))
            }
            self.shareInviteWithFriendAPI()
        }else{
            let vc = App.dashStoryBoard.instantiateViewController(withIdentifier: "SelectedContactVC") as! SelectedContactVC
            vc.contactsArray=checkedArray
            if(comingFrom ==  StringConstants.flags.issueBadges){
                vc.comingFrom = StringConstants.flags.issueBadges
                vc.booked_stall_info = booked_stall_info
            }else if (comingFrom ==  StringConstants.flags.exhIssuePass){
                vc.comingFrom = StringConstants.flags.exhIssuePass
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    // API FOR GET INVITED GUEST LIST
    
    func showInviteGuestListAPi(){
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var resource = String()
        var  source = String()
        var type : String
        var pagesize : Int
        var passtype : String
        var app_key :String
        if accountType == StringConstants.ONE {
            resource = RequestName.kisanMita
            type = RequestName.kisanMita
            passtype = RequestName.kisanMita
            source = RequestName.loginSourceName
            app_key = URLConstant.app_key
        }else{
            resource = RequestName.ExhibitorInvite
            type = RequestName.ExhibitorInvite
            passtype = "unlimitedexhibitorinvite"
            source = RequestName.loginExhibitorSourceName
            app_key = URLConstant.GREENPASS_EXHIAPPKEY

        }
       let filter = [URLConstant.filter4: resource, URLConstant.filter5: userName]
       pagesize = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)

        let params = getGuestListRequest.convertToDictionary(getGuestListRequest:getGuestListRequest.init(app_key: app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source, type: type, pagesize: pagesize, currentpage: 0, search: StringConstants.EMPTY, passtype: passtype, status: StringConstants.EMPTY, filter: filter as [String : AnyObject]))

        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.guestList
        _ = ShowGuestListServices().getGuestList(apiURL,postData: params as [String : AnyObject],
                                                 withSuccessHandler: { (userModel) in
                                                    let model = userModel as! ShowGuestListResponse
                                                    print("model",model)
                                                    self.filterArray.removeAllObjects()
                                                    let exhInfo = model.exhinfo[0]
                                                    let invitedGuestList = exhInfo.invitation_list
                                                    print("invitedGuestList",invitedGuestList)
                                                    self.guestListArray = invitedGuestList
                                                    
                                                    
                                                    for temp in self.guestListArray{
                                                        let gusetNum = self.removeSpaces(number: temp.mobile!)
                                                        self.guestPhonenumbersArray.add(gusetNum)
                                                        
                                                    }
                                                    
                                                    //self.phoneContacts.filter({$0.LName == StringConstants.EMPTY}).first?.LName = StringConstants.kisanMitra.Ji
                                                    self.filterArray.addObjects(from: self.phoneContacts)
                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                    self.contactTblView.reloadData()
                                                    
                                                    
        }, withFailureHandlere: { (error: String) in
           // MBProgressHUD.hide(for: self.view, animated: true);
            if self.phoneContacts.count > 0{
            if self.phoneContacts[0].phoneNumber.count > 0 {
                // self.phoneContacts.filter({removeSpaces(number: $0.phoneNumber[0]) == gusetNum}).first?.isInvited = true
                for i in 0 ..< self.phoneContacts.count {
                    let data=self.phoneContacts [i]
                    if data.phoneNumber.count > 0 {
                        if let LName = data.LName {
                            if LName == StringConstants.EMPTY {
                                self.phoneContacts[i].LName = StringConstants.kisanMitra.Ji
                            }else{
                                let phone = LName.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                                if phone != StringConstants.EMPTY {
                                    self.phoneContacts[i].LName = StringConstants.kisanMitra.Ji
                                }
                                
                            }
                        }
                    }
                }
                self.filterArray.removeAllObjects()
                self.filterArray.addObjects(from: self.phoneContacts)
                MBProgressHUD.hide(for: self.view, animated: true);
                self.contactTblView.reloadData()
                }
                
            }
                
            print("error",error)
        })
    }






// API to share Invite with friends

func shareInviteWithFriendAPI(){
    
    let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
    var type : String
    var app_key :String
    var  source = String()

    app_key = URLConstant.app_key
    type = RequestName.shareInviteToFrnds
    source = RequestName.loginSourceName
    
    let params = ShareInvitewithFriendsrequest.convertToDictionary(shareInvitewithFriendsrequest:ShareInvitewithFriendsrequest.init(app_key: app_key, sessionId: sessionId, language: appDelegate.strSelectedLanguage, type: type, eventCode: URLConstant.eventCode, source: source, mobiles: shareContactListArray))
    
    let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.sharewithYourFriend
    _ = ShareInvitewithFriendsServices().shareInviteToFriends(apiURL,postData: params as [String : AnyObject],
                                             withSuccessHandler: { (userModel) in
                                                _ = userModel as! ShareInvitewithFriendsResponse
                                             MBProgressHUD.hide(for: self.view, animated: true);
                                            let vc = App.dashStoryBoard.instantiateViewController(withIdentifier: "CongViewForShareYourFriendController") as! CongViewForShareYourFriendController
                                            vc.count = self.shareContactListArray.count
                                            self.navigationController?.pushViewController(vc, animated: true)

                                                
                                                
    }, withFailureHandlere: { (error: String) in
         MBProgressHUD.hide(for: self.view, animated: true);
        let vc = App.dashStoryBoard.instantiateViewController(withIdentifier: "CongViewForShareYourFriendController") as! CongViewForShareYourFriendController
        vc.count = self.shareContactListArray.count
        self.navigationController?.pushViewController(vc, animated: true)
        print("error",error)
    })
}

    
    func removeSpaces(number:String) -> String{
        var phoneNumber = number
        phoneNumber = (phoneNumber.replacingOccurrences(of: "-", with: StringConstants.EMPTY))
        phoneNumber = (phoneNumber.replacingOccurrences(of: " ", with: StringConstants.EMPTY))
        phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: StringConstants.EMPTY)
        phoneNumber = (phoneNumber.replacingOccurrences(of: ")", with: StringConstants.EMPTY))
        phoneNumber = (phoneNumber.replacingOccurrences(of: "+91", with: StringConstants.EMPTY))
        return phoneNumber
    }

    
}

extension ContactListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCell=tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
        let data=filterArray[indexPath.row]as!PhoneContact
        
        if let LName = data.LName {
            if LName.isEmpty {
                cell.nameLbl.text = data.fName! + StringConstants.singleSpace + StringConstants.kisanMitra.Ji
            }else{
                cell.nameLbl.text = data.fName! + StringConstants.singleSpace + LName
            }
        }else{
            cell.nameLbl.text = data.fName! + StringConstants.singleSpace + StringConstants.kisanMitra.Ji
        }
        
        
        if data.phoneNumber.count > 0 {
            let phoneNumberValue = removeSpaces(number:data.phoneNumber[0])
            cell.numberLbl.text = phoneNumberValue
            if(comingFrom ==  StringConstants.flags.issueBadges){
                if(guestPhonenumbersArray.contains(phoneNumberValue)){
                    cell.imgAlreadySent.isHidden = false
                    cell.imgAlreadySent.image = UIImage(named: StringConstants.ImageNames.exhBadge)
                    cell.topOfImgImgAlreadySentInvitation.constant = 20
                    cell.trailingOfImgAlreadySentInvitaion.constant = 15
                    cell.widthOfImgAlreadySentInvitation.constant = 25
                    cell.heightOfImgAlreadySentInvitation.constant = 20
                    cell.checkBoxBtn.isHidden = true
                }else{
                    cell.imgAlreadySent.isHidden = true
                    cell.checkBoxBtn.isHidden = false
                }
            }else{
                if(guestPhonenumbersArray.contains(phoneNumberValue)){
                    cell.imgAlreadySent.isHidden = false
                    cell.checkBoxBtn.isHidden = true
                }else{
                    cell.imgAlreadySent.isHidden = true
                    cell.checkBoxBtn.isHidden = false
                }
            }
        }
        
        if data.imgIsAvailable ?? false{
            cell.contactImg.image=UIImage.init(data: data.avatarData!)
        }else{
            cell.contactImg.image = UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg)
        }
        
        cell.checkBoxBtn.tag=indexPath.row
        cell.checkBoxBtn.addTarget(self, action: #selector(ContactListVC.checkBtnAction), for: .touchUpInside)
        cell.selectionStyle = .none
       
        
        if(comingFrom == StringConstants.flags.issueBadges){
            if checkedArray.contains((filterArray.object(at: indexPath.row))) {
                cell.checkBoxBtn.isSelected = true
                cell.checkBoxBtn.setImage(UIImage(named: StringConstants.ImageNames.checkboxBrown), for: .selected)
            }else{
                cell.checkBoxBtn.isSelected = false
                cell.checkBoxBtn.setImage(UIImage(named: StringConstants.ImageNames.uncheck_gray), for: .normal)
            }
        }else{
            if checkedArray.contains((filterArray.object(at: indexPath.row))) {
                cell.checkBoxBtn.isSelected = true
            }else{
                cell.checkBoxBtn.isSelected = false
            }
        }
        
//        if data.isInvited{
//            cell.imgAlreadySent.isHidden = false
//            cell.checkBoxBtn.isHidden = true
//        }else{
//            cell.imgAlreadySent.isHidden = true
//            cell.checkBoxBtn.isHidden = false
//        }

        return cell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
    
    
    @objc func checkBtnAction(sender: UIButton!) {
       
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent

        if(comingFrom ==  StringConstants.flags.shareYourFriendsVc){
            
            sender.isEnabled = true
            
            let value = sender.tag;
            
            if checkedArray.contains(filterArray.object(at: value)){
                checkedArray.remove(filterArray.object(at: value))
            }else{
                checkedArray.add(filterArray.object(at: value))
            }
            
            if checkedArray.count == 0{
                nextView.isHidden=true
                isFirst=true
                //contactTblView.frame.size.height = contactTblView.frame.height+nextView.frame.height
                heightOfNavViewConstant.constant = 0
            }else{
                nextView.isHidden=false
                if isFirst{
                    isFirst=false
                    // contactTblView.frame.size.height = contactTblView.frame.height-nextView.frame.height
                    heightOfNavViewConstant.constant = 63
                }
                
                let valueOfSelectedContact = checkedArray.count
                let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
                if checkedArray.count > 1 {
                    contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
                }else{
                    contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
                }
            }
            print(checkedArray.count)
            print("Selecetd Array \(checkedArray)")
            self.contactTblView.reloadData()
        }else{
            
            
            if(comingFrom == StringConstants.flags.issueBadges){
                
                if(checkedArray.count >= self.issueBadgeQuotaBalance){
                    var str =  String()
                    if self.issueBadgeQuotaBalance == 0 {
                        str = StringConstants.ToastViewComments.Insufficientquota_for_stall
                    }else{
                        str = StringConstants.ToastViewComments.select_only + StringConstants.singleSpace + String(self.issueBadgeQuotaBalance) + StringConstants.singleSpace + StringConstants.ToastViewComments.remaining_count
                    }
                    self.navigationController?.view.makeToast(str)

//                    sender.isEnabled = false

                }else{
                    
                    sender.isEnabled = true
                    
                    let value = sender.tag;
                    if(comingFrom == StringConstants.flags.issueBadges){
                        if checkedArray.contains(filterArray.object(at: value)){
                            checkedArray.remove(filterArray.object(at: value))
                            sender.setImage(UIImage(named: StringConstants.ImageNames.uncheck_gray), for: .normal)
                        }else{
                            checkedArray.add(filterArray.object(at: value))
                            sender.setImage(UIImage(named: StringConstants.ImageNames.checkboxBrown), for: .selected)
                        }
                    }else{
                        if checkedArray.contains(filterArray.object(at: value)){
                            checkedArray.remove(filterArray.object(at: value))
                        }else{
                            checkedArray.add(filterArray.object(at: value))
                        }
                    }
                    
                    if checkedArray.count == 0{
                        nextView.isHidden=true
                        isFirst=true
                        //contactTblView.frame.size.height = contactTblView.frame.height+nextView.frame.height
                        heightOfNavViewConstant.constant = 0
                    }else{
                        nextView.isHidden=false
                        if isFirst{
                            isFirst=false
                            // contactTblView.frame.size.height = contactTblView.frame.height-nextView.frame.height
                            heightOfNavViewConstant.constant = 63
                        }
                        
                        let valueOfSelectedContact = checkedArray.count
                        let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
                        if checkedArray.count > 1 {
                            contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
                        }else{
                            contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
                        }
                    }
                    print(checkedArray.count)
                    print("Selecetd Array \(checkedArray)")
                    self.contactTblView.reloadData()
                }
                
                
            }else{
                if(checkedArray.count >= remaining){
                    let str = StringConstants.ToastViewComments.select_only + StringConstants.singleSpace + String(remaining) + StringConstants.singleSpace + StringConstants.ToastViewComments.remaining_count
                    self.navigationController?.view.makeToast(str)
                    sender.isEnabled = false
                }else{
                    
                    sender.isEnabled = true
                    
                    let value = sender.tag;
                    if(comingFrom == StringConstants.flags.issueBadges){
                        if checkedArray.contains(filterArray.object(at: value)){
                            checkedArray.remove(filterArray.object(at: value))
                            sender.setImage(UIImage(named: StringConstants.ImageNames.uncheck_gray), for: .normal)
                        }else{
                            checkedArray.add(filterArray.object(at: value))
                            sender.setImage(UIImage(named: StringConstants.ImageNames.checkboxBrown), for: .selected)
                        }
                    }else{
                        if checkedArray.contains(filterArray.object(at: value)){
                            checkedArray.remove(filterArray.object(at: value))
                        }else{
                            checkedArray.add(filterArray.object(at: value))
                        }
                    }
                    
                    if checkedArray.count == 0{
                        nextView.isHidden=true
                        isFirst=true
                        //contactTblView.frame.size.height = contactTblView.frame.height+nextView.frame.height
                        heightOfNavViewConstant.constant = 0
                    }else{
                        nextView.isHidden=false
                        if isFirst{
                            isFirst=false
                            // contactTblView.frame.size.height = contactTblView.frame.height-nextView.frame.height
                            heightOfNavViewConstant.constant = 63
                        }
                        
                        let valueOfSelectedContact = checkedArray.count
                        let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
                        if checkedArray.count > 1 {
                            contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
                        }else{
                            contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
                        }
                    }
                    print(checkedArray.count)
                    print("Selecetd Array \(checkedArray)")
                    self.contactTblView.reloadData()
                }
            }
        }
    }
}


public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        //win.windowLevel = UIWindow.Level.alert + 1  // Swift 3-4: UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
