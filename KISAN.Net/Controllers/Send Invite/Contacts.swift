//
//  Contacts.swift
//  KISAN.Net
//
//  Created by MacMini on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import ContactsUI

enum ContactsFilter {
    case none
    case mail
    case message
}

class PhoneContacts {
    
    class func getContacts(filter: ContactsFilter = .none) -> [CNContact] { //  ContactsFilter is Enum find it below
        
        let contacts: [CNContact] = {
            let contactStore = CNContactStore()
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                               CNContactEmailAddressesKey,
                               CNContactPhoneNumbersKey,
                               CNContactImageDataAvailableKey,
                               CNContactThumbnailImageDataKey] as [Any]
            
            // Get all the containers
            var allContainers: [CNContainer] = []
            do {
                allContainers = try contactStore.containers(matching: nil)
            } catch {
                print("Error fetching containers")
            }
            
            var results: [CNContact] = []
            
            // Iterate all containers and append their contacts to our results array
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                
                do {
                    let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                    //results.appendContentsOf(containerResults)
                    results.append(contentsOf: containerResults)
                } catch {
                    print("Error fetching results for container")
                }
            }
            return results
        }()
        return contacts
    }
    
}
class PhoneContact: NSObject {
    var name: String?
    var fName: String?
    var LName: String?
    var avatarData: Data?
    var imgIsAvailable: Bool?
    var phoneNumber: [String] = [String]()
    var email: [String] = [String]()
    var isSelected: Bool = false
    var isInvited = false
    var imgUrl: String?

    init(contact: CNContact) {
        fName=contact.givenName
        LName=contact.familyName
        imgUrl = ""
        name        = contact.givenName + " " + contact.familyName
        avatarData  = contact.thumbnailImageData
        imgIsAvailable = contact.imageDataAvailable
        for phone in contact.phoneNumbers {
            phoneNumber.append(phone.value.stringValue)
        }
        for mail in contact.emailAddresses {
            email.append(mail.value as String)
        }
    }
    
    override init() {
        super.init()
    }
}
