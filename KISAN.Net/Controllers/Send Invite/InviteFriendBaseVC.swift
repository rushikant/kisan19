//
//  InviteFriendBaseVC.swift
//  KISAN.Net
//
//  Created by MacMini on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import ContactsUI

class InviteFriendBaseVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate,PassSelectedLanguage{
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet private weak var btnTab1: UIButton!
    @IBOutlet private weak var btnTab2: UIButton!
    var filter: ContactsFilter = .none
    //    @IBOutlet private weak var viewLine: UIView!
    @IBOutlet private weak var constantViewLeft: NSLayoutConstraint!
    @IBOutlet weak var lblCountOfGreenPassQuota: UILabel!
    var tab1VC:ContactListVC! = nil
    var tab2VC:InvitedGuestVC! = nil
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    var isComingFromGreenpass=false
    var appDelegate = AppDelegate()
    
    @IBOutlet weak var imgEditIcon: UIImageView!
    @IBOutlet weak var lblEditIcon: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblQuotaName: UILabel!
    var  booked_stall_info: [BookedStallInfoDetails]?
    var isFirstTime = Bool()
    var comingFrom = String()
    @IBOutlet weak var navViewForContactScreen: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPage = 0
        handleContactAuthorization()
        if UIScreen.main.bounds.size.height <= 568{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        }else{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 19.0)
        }
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        isFirstTime = true
        
        if (comingFrom == StringConstants.flags.issueBadges){
            imgEditIcon.isHidden = true
            btnEdit.isHidden = true
            lblEditIcon.isHidden = true
            lblNavTitle.text = "Issue Badges"
            self.view.backgroundColor =  UIColor.init(hexString: ColorConstants.marunColor)
            lblQuotaName.text = StringConstants.NavigationTitle.balance
            getListOfStallList()
           // getBalanceQuota()
            let nc = NotificationCenter.default
            nc.addObserver(self, selector: #selector(getBalanceValue), name: NSNotification.Name(rawValue: "getBalanceQuota"), object: nil)
        }else{
            lblNavTitle.text = "Invite Friends"
            imgEditIcon.isHidden = false
            btnEdit.isHidden = false
            lblEditIcon.isHidden = false
            lblQuotaName.text = StringConstants.NavigationTitle.greenpassQuota
            createPageViewController()
        }
    }
    
    @objc func getBalanceValue(notification:NSNotification) {
    
        let userInfo:Dictionary<String,String> = notification.userInfo as! Dictionary<String,String>
        let balanveValue = userInfo["value"]! as String
        if balanveValue != "noValue" {
            self.lblCountOfGreenPassQuota.text = balanveValue
        }
    }
    
    func getListOfStallList(){
        
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginExhibitorSourceName
        let app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let params = GetExhibitorDetailRequest.convertToDictionary(getExhibitorDetailRequest:GetExhibitorDetailRequest.init(app_key: app_key,  eventCode: URLConstant.eventCode, source: source, username: userName, sessionId: sessionId))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getexhibitordetails
        _ = GetExhibitorsDetailsServices().getExhbitorDetailsServices(apiURL,postData: params as [String : AnyObject],
                                                                      withSuccessHandler: { (userModel) in
                                                                        let model = userModel as! GetExhibitorsDetails
                                                                        print("model",model)
                                                                        let exhInfoDetails = model.exhInfoDetails
                                                                        print("RushiexhInfoDetails",exhInfoDetails)
                                                                         let bookedStallInfoData = exhInfoDetails.booked_stall_info
                                                                        var  booked_stall_Value: [BookedStallInfoDetails] = []

                                                                        for  j in (0..<bookedStallInfoData!.count){
                                                                            let stallData = bookedStallInfoData![j]
                                                                            let status = stallData.status!
                                                                            if status == StringConstants.Approved{
                                                                                self.booked_stall_info?.append(stallData)
                                                                                booked_stall_Value.append(stallData)
                                                                            }
                                                                        }
                                                                        self.booked_stall_info = booked_stall_Value
                                                                        self.createPageViewController()
                                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    

    func getBalanceQuota(){
        
        let stallid = "1022"
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginExhibitorSourceName
        let app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let params = GetStallDetailsRequest.convertToDictionary(getStallDetailsRequest:GetStallDetailsRequest.init(app_key: app_key,
                                                                                                                            eventCode: URLConstant.eventCode,
                                                                                                                            source: source,
                                                                                                                            stallid: stallid,
                                                                                                                            sessionId: sessionId))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getstalldetails
        _ = GetStallDetailsServices().getStallDetails(apiURL,postData: params as [String : AnyObject],
                                                                      withSuccessHandler: { (userModel) in
                                                                        let model = userModel as! GetStallDetailsResponse
                                                                        print("model",model)
                                                                        let stallInfo = model.stallInfo
                                                                        print("RushiexhstallInfo",stallInfo)
                                                                        let quota_details = stallInfo.quota_details
                                                                        let quota_balance = quota_details?[0].balance
                                                                        if quota_balance != nil {
                                                                            print("RushikantB-\(quota_details?[0].balance)")
                                                                            self.lblCountOfGreenPassQuota.text = "\(quota_balance!)"
                                                                        }

                                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    

    //MARK: step 6 finally use the method of the contract
    func passSelectedLanguage(_ language:String) {
        appDelegate.strSelectedLanguage = language
        isFirstTime = false
        print("appDelegate.strSelectedLanguage",appDelegate.strSelectedLanguage)
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (comingFrom == StringConstants.flags.issueBadges){
            
        }else{
        if isFirstTime == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.openPopUpForLanguageSelection()
            })
        }
        }
    }
    
    func openPopUpForLanguageSelection(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        vc.delegate = self
        if(comingFrom == StringConstants.flags.shareYourFriendsVc){
        vc.comingFrom = StringConstants.flags.shareYourFriendsVc
        }
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] as? UIWindow
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func handleContactAuthorization(){
    
        //handle contact Permissions
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
            
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            let messageStr = (productName as! String) + StringConstants.singleSpace + StringConstants.AlertMessage.enableContactPremissionText
            
            self.popupAlert(title: StringConstants.AlertMessage.contactsPemission, message: messageStr, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            
            
        case CNAuthorizationStatus.notDetermined:
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            self.popupAlert(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        case .authorized:break
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
        if(comingFrom != StringConstants.flags.issueBadges){
            let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
            let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
            let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
            let remaining = (standard + extra) - sent
            let intRemainingString = remaining.formattedWithSeparator
            lblCountOfGreenPassQuota.text = intRemainingString
        }else{
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.white, for: .normal)
        constantViewLeft.constant = btn.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
    }
    
    @IBAction private func btnOptionClicked(btn: UIButton) {
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    @IBAction func setInvitationClicked(_ sender: Any) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let  SetInviteSummaryVC = dashboardStoryboard.instantiateViewController(withIdentifier: "SetInviteSummaryViewController") as! SetInviteSummaryViewController
        self.navigationController?.pushViewController(SetInviteSummaryVC, animated: true)
    }
    
    //MARK: - CreatePagination
    private func createPageViewController() {

        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        if UIDevice.current.hasNotch {
            //... consider notch
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.pageController.view.frame = CGRect(x: 0, y:125, width: self.view.frame.size.width, height: self.view.frame.size.height-125)
            }
        } else {
            //... don't have to consider notch
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.pageController.view.frame = CGRect(x: 0, y:100, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
            }
        }
        
        //        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        tab1VC = (App.dashStoryBoard.instantiateViewController(withIdentifier: "ContactListVC") as! ContactListVC)
        
        tab2VC = (App.dashStoryBoard.instantiateViewController(withIdentifier: "InvitedGuestVC") as! InvitedGuestVC)
        if(comingFrom ==  StringConstants.flags.shareYourFriendsVc){
            arrVC = [tab1VC/*, tab2VC*/]
            btnTab2.isHidden = true
            tab1VC.comingFrom = StringConstants.flags.shareYourFriendsVc
            navViewForContactScreen.isHidden = false
        }else{
            if(comingFrom == StringConstants.flags.issueBadges){
                arrVC = [tab1VC, tab2VC]
                btnTab2.isHidden = false
                btnTab2.setTitle("Badge List", for: .normal)
                tab1VC.comingFrom = StringConstants.flags.issueBadges
                tab1VC.booked_stall_info = booked_stall_info
                tab2VC.comingFrom = StringConstants.flags.issueBadges
                tab2VC.booked_stall_info = booked_stall_info
                navViewForContactScreen.isHidden = true
            }else if(comingFrom == StringConstants.flags.exhIssuePass){
                arrVC = [tab1VC/*, tab2VC*/]
                btnTab2.isHidden = true
                tab1VC.comingFrom = StringConstants.flags.exhIssuePass
                navViewForContactScreen.isHidden = false
            }else{
                arrVC = [tab1VC, tab2VC]
                btnTab2.isHidden = false
                navViewForContactScreen.isHidden = true
            }
        }
        pageController.setViewControllers([tab1VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.firstIndex(of: viewCOntroller)!
        }
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index - 1
        }
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index + 1
        }
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.firstIndex(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    private func resetTabBarForTag(tag: Int) {
        var sender: UIButton!
        
        if(tag == 0) {
            sender = btnTab1
        }
        else if(tag == 1) {
            sender = btnTab2
        }
        currentPage = tag
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        selectedButton(btn: sender)
    }
    
    
    //MARK: - UIScrollView Delegate Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        //        let xCoor: CGFloat = CGFloat(viewLine.frame.size.width) * CGFloat(currentPage)
        //        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        //        constantViewLeft.constant = xPosition
    }
    
    
    //MARK:IBAction Methods
    @IBAction func backAction(_ sender: UIButton) {
        if isComingFromGreenpass{
            self.navigationController?.popViewController(animated: true)
        }else{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is DashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                nav.pushViewController(vc, animated: false)
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
