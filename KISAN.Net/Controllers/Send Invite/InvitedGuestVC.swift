//
//  InvitedGuestVC.swift
//  KISAN.Net
//
//  Created by MacMini on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class InvitedGuestVC: UIViewController, UITextFieldDelegate,PassSelectedStallDetails {
    @IBOutlet weak var invitedGuestTblView: UITableView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var acceptCountLbl: UILabel!
    @IBOutlet weak var attendedCountLbl: UILabel!
    @IBOutlet var serachTxt: UITextField!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var infoIcon: UIImageView!
    var  booked_stall_info: [BookedStallInfoDetails]?
    var invitedGuestListArray=NSMutableArray()
    var filterArray=NSMutableArray()
    var comingFrom = String()
    var stallId = String()
    
    
    @IBOutlet weak var topOfSearchBarConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(comingFrom ==  StringConstants.flags.guestList){
            initialDesign()
        }
       let  greenpassAcceptLast_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
        var titleStr=String()
        if !(greenpassAcceptLast_date.isEmpty){
            titleStr = "Before " + setAcceptGreenPassDate(strDate: greenpassAcceptLast_date)
        }else{
            titleStr = "Before 30th Nov"
        }
        
        
        var attributedString = NSMutableAttributedString()
        if UIScreen.main.bounds.size.height <= 568{
            attributedString = NSMutableAttributedString(string: "Invited Guest has to accept the Greenpass Before \(titleStr). ", attributes: [
                .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                .foregroundColor: UIColor.black,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize:13.0, weight: .semibold), range: NSRange(location: 49, length: titleStr.count))
        }else{
            attributedString = NSMutableAttributedString(string: "Invited Guest has to accept the Greenpass Before \(titleStr). ", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .regular),
                .foregroundColor: UIColor.black,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 17.0, weight: .semibold), range: NSRange(location: 49, length: titleStr.count))
        }
        infoLbl.attributedText=attributedString
        
        if comingFrom == StringConstants.flags.issueBadges  {
            
            self.setInfoLabelDataForIssueBadges()
            let nc = NotificationCenter.default
            nc.post(name: NSNotification.Name(rawValue: "getBalanceQuota"), object: nil, userInfo: ["value" : "noValue"])
        }
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = true
        if comingFrom == StringConstants.flags.issueBadges {
            self.showBadgesListAPi()
        } else {
            self.showInviteGuestListAPi()
        }
        
    }
    
    func initialDesign() {
    
        topOfSearchBarConstraint.constant = 70
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.guestList, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        helpButton.isHidden = true
        infoIcon.isHidden = true
        
    }
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    func setInfoLabelDataForIssueBadges(){
        
        var stallName = String()
        if booked_stall_info!.count > 0 {
            let indexValue =  booked_stall_info?.index(where: { $0.isSelectedStall == StringConstants.ONE })
            let stallInfo = booked_stall_info![indexValue!]
            stallName = stallInfo.stand_type_name!  + " - " +  stallInfo.stand_location_name! + " - "
            stallName = stallName + stallInfo.pavillion_name! + " - " + String(stallInfo.area!)
            stallName = stallName  + " sq.m." + " - " + "(" + String(stallInfo.id!) + ")"
            stallId = String(stallInfo.id!)
        }else{
            stallName = StringConstants.EMPTY
            stallId = StringConstants.EMPTY
        }
        
        let strChangeStall = " Change stall"
        
        let partOneAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)]
        let partTwoAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold)] as [NSAttributedStringKey : Any]
        let partThreeAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)] as [NSAttributedStringKey : Any]
        
        let partOne = NSMutableAttributedString(string: "You will get printed badges in your possession kit. \n", attributes: partOneAttributes)
        let partTwo = NSMutableAttributedString(string: "You are currently viewing the badges for \n", attributes: partOneAttributes)
        
        
        let partThree = NSMutableAttributedString(string: stallName, attributes: partTwoAttributes)
        let partFour = NSMutableAttributedString(string: strChangeStall, attributes: partThreeAttributes)
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        
        combination.append(partThree)
        if booked_stall_info!.count > 1 {
            combination.append(partFour)
        }
        
        self.infoLbl.attributedText = combination
        
        let range2 = (infoLbl!.text! as NSString).range(of: strChangeStall)
        combination.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.0
        combination.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, combination.length))
        self.infoLbl.attributedText = combination
        let tap = UITapGestureRecognizer(target: self, action: #selector(InvitedGuestVC.tapFunctionOnInfoLabel))
        infoLbl.isUserInteractionEnabled = true
        if booked_stall_info!.count > 1 {
            infoLbl.addGestureRecognizer(tap)
        }
        
        // Get BalanceQuota API call
        self.getBalanceQuota()

    }
    
    func getBalanceQuota(){
        
        let stallid = stallId
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginExhibitorSourceName
        let app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let params = GetStallDetailsRequest.convertToDictionary(getStallDetailsRequest:GetStallDetailsRequest.init(app_key: app_key,
                                                                                                                   eventCode: URLConstant.eventCode,
                                                                                                                   source: source,
                                                                                                                   stallid: stallid,
                                                                                                                   sessionId: sessionId))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getstalldetails
        _ = GetStallDetailsServices().getStallDetails(apiURL,postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let model = userModel as! GetStallDetailsResponse
                                                        print("model",model)
                                                        let stallInfo = model.stallInfo
                                                        print("RushiexhstallInfo",stallInfo)
                                                        let quota_details = stallInfo.quota_details
                                                        let quota_balance = quota_details?[0].balance
                                                        if quota_balance != nil {
                                                           
                                                            let quotaBal = String(quota_balance!)
                                                            let nc = NotificationCenter.default
                                                            nc.post(name: NSNotification.Name(rawValue: "getBalanceQuota"), object: nil, userInfo: ["value" : quotaBal])
                                                            
                                                        }
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    
    @IBAction func tapFunctionOnInfoLabel(gesture: UITapGestureRecognizer) {
        self.openPopUpForStallList()
    }
    
    func openPopUpForStallList(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "SelectStallListPopUpVC") as! SelectStallListPopUpVC
            vc.modalPresentationStyle = .overCurrentContext
        vc.booked_stall_info = booked_stall_info
        let backgroundScreenShot = captureScreen()
        vc.delegate = self
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
    
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] as? UIWindow
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedStallDetails(_ stallDetails:[BookedStallInfoDetails]?) {
        print("pass Details")
        booked_stall_info?.removeAll()
        booked_stall_info = stallDetails
        self.setInfoLabelDataForIssueBadges()
    }
    
    func setup() {
        if comingFrom == StringConstants.flags.issueBadges {
            self.statusView.isHidden = true
            self.alertView.topAnchor.constraint(equalTo: self.statusView.topAnchor, constant: 0).isActive = true
            self.infoIcon.isHidden = true
            self.helpButton.isHidden = true
//            self.alertView.widthAnchor.constraint(equalTo: self.statusView.widthAnchor, constant: 0).isActive = true
//            self.infoLbl.numberOfLines = 1
        }
    }
    
    func setAcceptGreenPassDate(strDate:String) -> String {
        var formattedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: strDate)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        formattedDate =  dateFormatterPrint.string(from: date! as Date)
        
        return formattedDate
    }
    
    
    // API FOR GET INVITED GUEST LIST
    
    func showInviteGuestListAPi(){
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var resource = String()
        var  source = String()
        var type : String
        var pagesize : Int
        var passtype : String
        var app_key :String
        if accountType == StringConstants.ONE {
            resource = RequestName.kisanMita
            type = RequestName.kisanMita
            passtype = RequestName.kisanMita
            source = RequestName.loginSourceName
            app_key = URLConstant.app_key
        }else{
            resource = RequestName.ExhibitorInvite
            type = RequestName.ExhibitorInvite
            passtype = "unlimitedexhibitorinvite"
            source = RequestName.loginExhibitorSourceName
            app_key = URLConstant.GREENPASS_EXHIAPPKEY
            
        }
        let filter = [URLConstant.filter4: resource, URLConstant.filter5: userName]
        pagesize = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        
        let params = getGuestListRequest.convertToDictionary(getGuestListRequest:getGuestListRequest.init(app_key: app_key, sessionId: sessionId, eventCode: URLConstant.eventCode, source: source, type: type, pagesize: pagesize, currentpage: 0, search: StringConstants.EMPTY, passtype: passtype, status: StringConstants.EMPTY, filter: filter as [String : AnyObject]))
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.guestList
        _ = ShowGuestListServices().getGuestList(apiURL,postData: params as [String : AnyObject],
                                                 withSuccessHandler: { (userModel) in
                                                    let model = userModel as! ShowGuestListResponse
                                                    print("model",model)
                                                    self.filterArray.removeAllObjects()
                                                    let exhInfo = model.exhinfo[0]
                                                    let invitedGuestList = exhInfo.invitation_list
                                                    print("invitedGuestList",invitedGuestList)
                                                    self.invitedGuestListArray.addObjects(from: invitedGuestList)
                                                    var acceptedCount=0
                                                    var attendedCount=0
                                                    for temp in invitedGuestList{
                                                        if temp.collected_datetime != "" && temp.collected_datetime != nil{
                                                            acceptedCount += 1
                                                        }
                                                        if temp.visited_datetime != "" && temp.visited_datetime != nil{
                                                            attendedCount += 1
                                                        }
                                                    }
                                                    self.acceptCountLbl.text="Accepted (\(acceptedCount))"
                                                    self.attendedCountLbl.text="Attended (\(attendedCount))"
                                                    self.filterArray.addObjects(from: invitedGuestList)
                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                    self.invitedGuestTblView.reloadData()
                                                    
                                                    
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            self.invitedGuestTblView.reloadData()
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    func showBadgesListAPi(){
        var  source = String()
        var passtype : String
        var app_key :String
        passtype = RequestName.onlineexhibitorbadge
        source = RequestName.loginExhibitorSourceName
        app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let filter = ["stall_id": stallId,"sendpass": "1","csv_id":""] //stall_id = 1022
        let params = GetBadgesListRequest.convertToDictionary(getBadgesListRequest:GetBadgesListRequest.init(app_key: app_key,  eventCode: URLConstant.eventCode, source: source, pagesize: 1000, currentpage: 0, passtype: passtype,filter:filter))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getbadgeslist
        _ = GetBadgesListServices().getBadgesList(apiURL,postData: params as [String : AnyObject],
                                                 withSuccessHandler: { (userModel) in
                                                    let model = userModel as! GetBadgesListResponse
                                                    print("model",model)
                                                    self.filterArray.removeAllObjects()
                                                    let exhibitorBadges = model.exhibitorBadges
                                                    print("exhibitorBadges",exhibitorBadges)
                                                    self.invitedGuestListArray.addObjects(from: exhibitorBadges)
                                                    self.filterArray.addObjects(from: exhibitorBadges)
                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                    self.invitedGuestTblView.reloadData()
                                                
                                                    
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            self.invitedGuestTblView.reloadData()
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    
    // MARK: - UITextField Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let string1 = string
        let string2 = serachTxt.text
        var search = ""
        if string.count > 0 { // if it was not delete character
            search = string2! + string1
        }
        else if (string2?.count)! > 0{ // if it was a delete character
            
            search = String(string2!.dropLast())
        }
        print(search)
        
        if search.count != 0 {
            
            filterArray.removeAllObjects()
            let i=invitedGuestListArray.count
            
            if comingFrom == StringConstants.flags.issueBadges {
                //let data=invitedGuestListArray[item]as!ExhibitorBadges
                for item in 0..<i {
                    let data = invitedGuestListArray[item] as! ExhibitorBadges
                    let str = data.first_name! + data.last_name!
                    let stringRange:NSRange = (str as AnyObject).range(of: search, options: .caseInsensitive)
                    if stringRange.length != 0 {
                        filterArray.add(invitedGuestListArray[item])
                    }
                }
            }else{
                for item in 0..<i {
                    let data = invitedGuestListArray[item] as! GuestListDetails
                    let str = data.first_name! + data.last_name!
                    let stringRange:NSRange = (str as AnyObject).range(of: search, options: .caseInsensitive)
                    if stringRange.length != 0 {
                        filterArray.add(invitedGuestListArray[item])
                    }
                }
            }
        }else {
            filterArray.removeAllObjects()
            filterArray.addObjects(from: invitedGuestListArray as! [Any])
        }
        invitedGuestTblView.reloadData()
        return true;
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:IBAction Methods
    @IBAction func btnHelpClick(_ sender: Any) {
        let knowMoreVc = self.storyboard?.instantiateViewController(withIdentifier: "KnowMoreViewController") as! KnowMoreViewController
        self.navigationController?.pushViewController(knowMoreVc, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension InvitedGuestVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "InvitedGuestTVC") as! InvitedGuestTVC
        
        if comingFrom == StringConstants.flags.issueBadges {
            let data=filterArray[indexPath.row]as!ExhibitorBadges
            cell.statusImg.isHidden = true
            cell.nameLbl.text = data.first_name! + " " + data.last_name!
            let myString = data.mobile
            let replaced = myString?.replacingOccurrences(of: "+91", with: "") // "aaaaaaaa
            cell.numberLbl.text = replaced
            if data.fullimageurl != "" && data.fullimageurl != nil{
                cell.contactImg.sd_setImage(with: URL.init(string: data.fullimageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
            }else{
                cell.contactImg.image = UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg)
            }
        } else {
            let data=filterArray[indexPath.row]as!GuestListDetails
            cell.nameLbl.text = data.first_name! + " " + data.last_name!
            let myString = data.mobile
            let replaced = myString?.replacingOccurrences(of: "+91", with: "") // "aaaaaaaa
            cell.numberLbl.text = replaced
            cell.statusImg.isHidden = false
            
            if (data.collected_datetime == "" || data.collected_datetime == nil) && (data.collected_datetime == "" || data.collected_datetime == nil){
                cell.statusImg.image =  UIImage(named: "")
            }
            
            if data.collected_datetime != "" && data.collected_datetime != nil{
                cell.statusImg.image =  UIImage(named: "AcceptStatusicon")
            }
            
            if data.visited_datetime != "" && data.visited_datetime != nil{
                cell.statusImg.image =  UIImage(named: "attendedicon")
            }
            
            if data.imageurl != "" && data.imageurl != nil{
                cell.contactImg.sd_setImage(with: URL.init(string: data.imageurl!), placeholderImage: UIImage(named:StringConstants.ImageNames.kisan_web_logo_white_x50))
            }else{
                cell.contactImg.image = UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg)
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
    
    
    
    
}
