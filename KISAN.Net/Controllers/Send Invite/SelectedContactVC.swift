//
//  SelectedContactVC.swift
//  KISAN.Net
//
//  Created by MacMini104 on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class SelectedContactVC: AppViewController, ContactDetailDelegate,PassSelectedLanguage,PassSelectedStallDetails {

    @IBOutlet weak var lblCountGreenPassQuota: UILabel!
    @IBOutlet weak var contactTblView: UITableView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var contactCountLbl: UILabel!
    var contactsArray=NSMutableArray()
    var updatedContactsArray=NSMutableArray()
    var selectedIndex=0
    var index = 0
    var uploadContactsArray=NSMutableArray()
    var issueInvitiesPeopleDetailsRequest = IssueInvitiesPeopleDetailsRequest.self
    var appDelegate = AppDelegate()
    @IBOutlet weak var lblNavTitle: UILabel!
    var comingFrom  = String()
    @IBOutlet weak var sendPassesView: UIView!
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var lblQuotaName: UILabel!
    @IBOutlet weak var lblSendFreePass: UILabel!
    var  booked_stall_info: [BookedStallInfoDetails]?
    var stallId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
     
        setValueOfTitleLabel()
        let valueOfSelectedContact = contactsArray.count
        let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
        
        if contactsArray.count > 1{
            contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
        }else{
            contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
        }
        
        contactTblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        contactTblView.rowHeight = UITableViewAutomaticDimension
        contactTblView.estimatedRowHeight = 85.0
        contactTblView.separatorColor = UIColor.clear
        if UIScreen.main.bounds.size.height <= 568{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        }else{
            lblNavTitle.font = UIFont.boldSystemFont(ofSize: 19.0)
        }
        
        for i in 0 ..< self.contactsArray.count {
            let data=self.contactsArray [i] as!PhoneContact
            if data.phoneNumber.count > 0 {
                if let LName = data.LName {
                    let newLastNameValue = removeSpecialCharFromContactList(lName: LName)
                    data.LName = newLastNameValue
                    if data.LName == StringConstants.EMPTY {
                        data.LName = StringConstants.kisanMitra.Ji
                    }else{
                        let phone = LName.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                        if phone != StringConstants.EMPTY {
                            data.LName = StringConstants.kisanMitra.Ji
                        }
                    }
                    self.contactsArray.removeObject(at: i)
                    self.contactsArray.insert(data, at: i)   
                }
            }
        }
        
        if(comingFrom ==  StringConstants.flags.issueBadges){
            lblQuotaName.text = "Balance"
            self.setInfoLabelDataForIssueBadges()
        }else{
            infoImage.isHidden = false
            infoButton.isHidden = false
            lblQuotaName.text = "Greenpass Quota"
            lblCountGreenPassQuota.text = StringConstants.NavigationTitle.greenpassQuota
        }
    }
    
    func setInfoLabelDataForIssueBadges(){
        
        self.view.backgroundColor = UIColor.init(hexString: ColorConstants.marunColor)
        sendPassesView.backgroundColor = UIColor.init(hexString: ColorConstants.marunColor)
        infoLbl.font = UIFont.systemFont(ofSize: 13)
        infoLbl.text = StringConstants.issueExhBadgeViewController.editContactsNote1
        
        var stallName = String()
        if booked_stall_info!.count > 0 {
            let indexValue =  booked_stall_info?.index(where: { $0.isSelectedStall == StringConstants.ONE })
            let stallInfo = booked_stall_info![indexValue!]
            stallName = stallInfo.stand_type_name!  + " - " +  stallInfo.stand_location_name! + " - "
            stallName = stallName + stallInfo.pavillion_name! + " - " + String(stallInfo.area!)
            stallName = stallName  + " sq.m." + " - " + "(" + String(stallInfo.id!) + ")"
            stallId = String(stallInfo.id!)
        }else{
            stallName = StringConstants.EMPTY
            stallId = StringConstants.EMPTY
        }
        
        // let stallName = "Prefab- Row-Water-9 sq.m"
        let strChangeStall = " Change stall"
        
        let partOneAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)]
        let partTwoAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold)] as [NSAttributedStringKey : Any]
        let partThreeAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: (152/255), green: (78/255), blue: (0), alpha: 1.0), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0, weight: .regular)] as [NSAttributedStringKey : Any]
        
        let partOne = NSMutableAttributedString(string: StringConstants.issueExhBadgeViewController.editContactsNote1, attributes: partOneAttributes)
        // let partOneSubPart = NSMutableAttributedString(string: "7th Dec. \n", attributes: partThreeAttributes)
        let partTwo = NSMutableAttributedString(string: StringConstants.issueExhBadgeViewController.editContactsNote2, attributes: partOneAttributes)
        
        
        let partThree = NSMutableAttributedString(string: stallName, attributes: partTwoAttributes)
        let partFour = NSMutableAttributedString(string: strChangeStall, attributes: partThreeAttributes)
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        // combination.append(partOneSubPart)
        combination.append(partTwo)
        
        combination.append(partThree)
        if booked_stall_info!.count > 1 {
            combination.append(partFour)
        }
        self.infoLbl.attributedText = combination
        
        let range2 = (infoLbl!.text! as NSString).range(of: strChangeStall)
        combination.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.0
        combination.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, combination.length))
        self.infoLbl.attributedText = combination
        let tap = UITapGestureRecognizer(target: self, action: #selector(ContactListVC.tapFunctionOnInfoLabel))
        infoLbl.isUserInteractionEnabled = true
        if booked_stall_info!.count > 1 {
            infoLbl.addGestureRecognizer(tap)
        }
        
        infoImage.isHidden = true
        infoButton.isHidden = true
        lblCountGreenPassQuota.text = StringConstants.NavigationTitle.balance
        lblSendFreePass.text = StringConstants.issueExhBadgeViewController.issueBadges
        
        // Get BalanceQuota API call
        self.getBalanceQuota()
    }
    
    func getBalanceQuota(){
        
        let stallid = stallId
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let source = RequestName.loginExhibitorSourceName
        let app_key = URLConstant.GREENPASS_EXHIAPPKEY
        let params = GetStallDetailsRequest.convertToDictionary(getStallDetailsRequest:GetStallDetailsRequest.init(app_key: app_key,
                                                                                                                   eventCode: URLConstant.eventCode,
                                                                                                                   source: source,
                                                                                                                   stallid: stallid,
                                                                                                                   sessionId: sessionId))
        print("Params", params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getstalldetails
        _ = GetStallDetailsServices().getStallDetails(apiURL,postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let model = userModel as! GetStallDetailsResponse
                                                        print("model",model)
                                                        let stallInfo = model.stallInfo
                                                        print("RushiexhstallInfo",stallInfo)
                                                        let quota_details = stallInfo.quota_details
                                                        let quota_balance = quota_details?[0].balance
                                                        if quota_balance != nil {
                                                            
                                                            let quotaBal = String(quota_balance!)
                                                           self.lblCountGreenPassQuota.text = quotaBal
                                                        }
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            print("error",error)
        })
    }
    
    @IBAction func tapFunctionOnInfoLabel(gesture: UITapGestureRecognizer) {
        self.openPopUpForStallList()
    }
    
    func openPopUpForStallList(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "SelectStallListPopUpVC") as! SelectStallListPopUpVC
            vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        vc.delegate = self
        vc.booked_stall_info = booked_stall_info
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedStallDetails(_ stallDetails:[BookedStallInfoDetails]?) {
        print("pass Details")
        booked_stall_info?.removeAll()
        booked_stall_info = stallDetails
        self.setInfoLabelDataForIssueBadges()
    }
    
    func setValueOfTitleLabel(){
       
       let greenpassAcceptLast_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
        
        var titleStr=String()
        if !(greenpassAcceptLast_date.isEmpty){
            titleStr  = setAcceptGreenPassDate(strDate: greenpassAcceptLast_date)
        }else{
            titleStr="15th November"
        }

        var attributedString = NSMutableAttributedString()
        
         let selectedLanguageValue = appDelegate.strSelectedLanguage.capitalizingFirstLetter()
        
        if UIScreen.main.bounds.size.height <= 568{
            attributedString = NSMutableAttributedString(string: "Invited Guest has to accept the Greenpass Before \(titleStr).Sending SMS in \(selectedLanguageValue)", attributes: [
                .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                .foregroundColor: UIColor.black,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize:13.0, weight: .semibold), range: NSRange(location: 49, length: titleStr.count))
            infoLbl.attributedText=attributedString
            let range2 = (infoLbl!.text! as NSString).range(of: selectedLanguageValue)
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(SelectedContactVC.tapFunction))
            infoLbl.isUserInteractionEnabled = true
            infoLbl.addGestureRecognizer(tap)
            
        }else{
            attributedString = NSMutableAttributedString(string: "Invited Guest has to accept the Greenpass Before \(titleStr).Sending SMS in \(selectedLanguageValue)", attributes: [
                .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
                .foregroundColor: UIColor.black,
                .kern: 0.0
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: .semibold), range: NSRange(location: 49, length: titleStr.count))
            
            infoLbl.attributedText=attributedString
            let range2 = (infoLbl!.text! as NSString).range(of: selectedLanguageValue)
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(SelectedContactVC.tapFunction))
            infoLbl.isUserInteractionEnabled = true
            infoLbl.addGestureRecognizer(tap)
            
        }
        infoLbl.attributedText=attributedString
    }
    
    func setAcceptGreenPassDate(strDate:String) -> String {
        var formattedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: strDate)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        formattedDate =  dateFormatterPrint.string(from: date! as Date)
        
        return formattedDate
    }
    
    @IBAction func tapFunction(gesture: UITapGestureRecognizer) {
        let selectedLanguageValue = appDelegate.strSelectedLanguage.capitalizingFirstLetter()
        let text = (infoLbl.text)!
        let termsRange = (text as NSString).range(of: selectedLanguageValue)
        if gesture.didTapAttributedTextInLabel(label: infoLbl, inRange: termsRange) {
           openPopUpForLanguageSelection()
        }
    }
    
    func openPopUpForLanguageSelection(){
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
            vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        vc.delegate = self
        vc.backImageCapturedValue = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] as? UIWindow
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: step 6 finally use the method of the contract
    func passSelectedLanguage(_ language:String) {
        appDelegate.strSelectedLanguage = language
        setValueOfTitleLabel()
        self.navigationController?.popViewController(animated: false)
    }
    
    func removeSpecialCharFromContactList(lName:String)-> String{
        var lNameValue = String()
        let charsToRemove: Set<Character> = Set("!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢₹".characters)
        lNameValue = String(lName.characters.filter { !charsToRemove.contains($0) });
        return lNameValue
    }
    
    
    @IBAction func btnHelpClick(_ sender: Any) {
        let knowMoreVc = self.storyboard?.instantiateViewController(withIdentifier: "KnowMoreViewController") as! KnowMoreViewController
        self.navigationController?.pushViewController(knowMoreVc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent
        let intRemainingString = remaining.formattedWithSeparator
        lblCountGreenPassQuota.text = String(intRemainingString)
    }
    
    func updateContactDetail(contactDetail: PhoneContact) {
        contactsArray.replaceObject(at: selectedIndex, with: contactDetail)
        contactTblView.reloadData()
    }
    //MARK:IBAction Methods
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {

        if(comingFrom ==  StringConstants.flags.issueBadges){
            if(!isAllConatctasValid(phoneContactArray: contactsArray as! [PhoneContact])){
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.incomplete_information, comment: StringConstants.EMPTY)))
            }else{
                print("Rushi-ContactArray")
                uploadContactsArray=[]
                let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
                let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
                for item in contactsArray{
                    let data=item as!PhoneContact
                    let dataDict=NSMutableDictionary()
                    dataDict.setValue(data.fName, forKey: "first_name")
                    dataDict.setValue(data.LName, forKey: "last_name")
                    dataDict.setValue( removeSpaces(number:data.phoneNumber[0]), forKey: "mobile")
                    //dataDict.setValue("9730674658", forKey: "mobile")
                    dataDict.setValue(data.imgUrl, forKey: "imageUrl")
                    dataDict.setValue(state, forKey: "state")
                    dataDict.setValue(city, forKey: "district")
                    dataDict.setValue("+91", forKey: "countrycode")
                    uploadContactsArray.add(dataDict)
                    print(dataDict)
                }
                uploadContactImg(i: index)
                
            }
            
        }else{
            if(!isAllConatctasValid(phoneContactArray: contactsArray as! [PhoneContact])){
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.incomplete_information, comment: StringConstants.EMPTY)))
            }else{
                uploadContactsArray=[]
                let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
                let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
                for item in contactsArray{
                    let data=item as!PhoneContact
                    let dataDict=NSMutableDictionary()
                    dataDict.setValue(data.fName, forKey: "first_name")
                    dataDict.setValue(data.LName, forKey: "last_name")
                    dataDict.setValue( removeSpaces(number:data.phoneNumber[0]), forKey: "mobile")
                    //dataDict.setValue("9730674658", forKey: "mobile")
                    dataDict.setValue(data.imgUrl, forKey: "imageUrl")
                    dataDict.setValue(state, forKey: "state")
                    dataDict.setValue(city, forKey: "district")
                    dataDict.setValue("+91", forKey: "countrycode")
                    uploadContactsArray.add(dataDict)
                }
                uploadContactImg(i: index)
                
            }
        }
    }
    
    func removeSpaces(number:String) -> String{
        var phoneNumber = number
        phoneNumber = (phoneNumber.replacingOccurrences(of: "-", with: StringConstants.EMPTY))
        phoneNumber = (phoneNumber.replacingOccurrences(of: " ", with: StringConstants.EMPTY))
        phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: StringConstants.EMPTY)
        phoneNumber = (phoneNumber.replacingOccurrences(of: ")", with: StringConstants.EMPTY))
        phoneNumber = (phoneNumber.replacingOccurrences(of: "+91", with: StringConstants.EMPTY))
        return phoneNumber
    }
    
    //Convert Array data into Json String.
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    
    func uploadContactImg(i:Int){
        let data=contactsArray[i]as!PhoneContact
        if data.imgIsAvailable ?? false{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
            let appKey =  URLConstant.app_key
            _ = StringConstants.MediaType.image
            let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadVisitorProfileToImage //URLConstant.uploadFileVisitor
            print("ApiUrl",apiURL)
            let params = ["app_key":appKey,
                          "sessionId":sessionId]
            let fileName = StringConstants.file
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key,value) in params {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
                if let data = data.avatarData{
                    let bytes = data.count
                    /// Description
                    _ = Double(bytes) / 1000.0 // Note the difference
                    let KB = Double(bytes) / 1024.0 // Note the difference
                    print(KB / 1024)
                    multipartFormData.append(data, withName: fileName, fileName: "invite_contact_img.png", mimeType: "image/jpeg")
                }
            },
                             to: apiURL,
                             method: .post,
                             encodingCompletion: { encodingResult in
                                switch encodingResult
                                {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        print(response)
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        guard let responseJSON = response.result.value as? [String:Any],
                                            let _ = responseJSON["message"] as? String,
                                            let status = responseJSON["success"] as? Bool else {
                                                return
                                        }
                                        if(status == false)
                                        {
                                        }
                                        else{
                                            let data=responseJSON["imageDetails"]as!NSDictionary
                                            var dictDataValue = self.uploadContactsArray[i] as! Dictionary<String,String>
                                            let imgFullUrl = String(describing: data["imgFullUrl"]!)
                                            dictDataValue.updateValue(imgFullUrl, forKey: "imageUrl")
                                            self.uploadContactsArray.removeObject(at: i)
                                            self.uploadContactsArray.insert(dictDataValue, at: i)
                                            if self.index==self.contactsArray.count-1{
                                                if (self.comingFrom ==  StringConstants.flags.issueBadges) {
                                                    self.issueBadgesApi()
                                                } else {
                                                    self.SentInviteContactsApiCall()
                                                }
                                            }else{
                                                self.index += 1
                                                print(self.index)
                                                self.uploadContactImg(i: self.index)
                                            }
                                            
                                        }
                                    }
                                    
                                case .failure( _ ):
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    self.popupAlert(title: "", message: "Something went wrong", actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                                        },{action2 in
                                        }, nil])
                                }
            })
        }else{
            if index==contactsArray.count-1{
                if (comingFrom ==  StringConstants.flags.issueBadges) {
                    issueBadgesApi()
                } else {
                    SentInviteContactsApiCall()
                }
            }else{
                index += 1
                print(index)
                uploadContactImg(i: index)
            }
            
        }
    }
    
    
    //setInvite API CAll
    func SentInviteContactsApiCall(){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        var appKey = String()
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.issueInvites
        //set Source and filter for both exhibitor and Kisan Mitra
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var resource = String()
        var  source = String()
        var  passtype = String()
        
        if accountType == StringConstants.ONE {
            passtype = StringConstants.PassType.MitraIvite
            resource = RequestName.kisanMita
            source = RequestName.loginSourceName
            appKey = URLConstant.app_key
        }else{
            resource = RequestName.ExhibitorInvite
            source = RequestName.loginExhibitorSourceName
            appKey = URLConstant.GREENPASS_EXHIAPPKEY
           let show_issue_greenpass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
           let show_issue_unlimited_greenpass  = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
            
            if (show_issue_greenpass == 1 && show_issue_unlimited_greenpass == 1) {
                passtype = StringConstants.PassType.UnlimitedExhibitorInvite
            }else if (show_issue_greenpass == 1 && show_issue_unlimited_greenpass == 0){
                passtype = StringConstants.PassType.exhibitorinvite
            }
        }
        
        print("self.uploadContactsArray",self.uploadContactsArray)
        let param = ["app_key":String(appKey),
                     "sessionId":String(sessionId),
                     "eventCode":String(URLConstant.eventCode),
                     "source":String(source),
                     "type":String(resource),
                     "passType":String(passtype),
                     "entryType":String(StringConstants.EventType) ,
                     "lang" : appDelegate.strSelectedLanguage,
                     "people":self.uploadContactsArray] as [String : Any]
        print("param",param)
        
        _ = IssueInviteDeatilsServices().issueInvites(apiURL,
                                                      postData: param as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        let model = userModel as! IssueInviteDeatilsResponse
                                                        print("model",model)
                                                        let message = model.message
                                                        print("message",message as Any)
                                                        let summaryDetails = model.summary[0]
                                                        let duplicate_visitors = summaryDetails.duplicate_visitors
                                                        print("duplicate_visitors",duplicate_visitors as Any)
                                                        let valid_visitors = summaryDetails.valid_visitors
                                                        print("valid_visitors",valid_visitors as Any)
                                                        let invalid_visitors = summaryDetails.invalid_visitors
                                                        print("invalid_visitors",invalid_visitors as Any)
                                                       
                                                        var sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
                                                        sent = sent + valid_visitors!
                                                        UserDefaults.standard.set(sent, forKey:
                                                            StringConstants.NSUserDefauluterKeys.sent)

                                                        let congratulationsVC = self.storyboard?.instantiateViewController(withIdentifier: "CongratulationsViewController") as! CongratulationsViewController
                                                        congratulationsVC.strSenntInviteCout = valid_visitors!
                                                        congratulationsVC.comingFrom = self.comingFrom
                                                        self.navigationController?.pushViewController(congratulationsVC, animated: true)

                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    //setInvite API CAll
    func issueBadgesApi(){
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        var appKey = String()
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.issuebadges
        var resource = String()
        var  source = String()
        var  passtype = String()
        
            passtype = StringConstants.PassType.onlinebadge
            resource = RequestName.ExhibitorInvite
            source = RequestName.loginExhibitorSourceName
            appKey = URLConstant.GREENPASS_EXHIAPPKEY

        print("self.uploadContactsArray",self.uploadContactsArray)
        let param = ["app_key":String(appKey),
                     "sessionId":String(sessionId),
                     "eventCode":String(URLConstant.eventCode),
                     "source":String(source),
                     "type":String(resource),
                     "passType":String(passtype),
                     "entryType":String(StringConstants.MultiEntry) ,
                     "sendpass":String(StringConstants.ONE) ,
                     "stallId": stallId, //"1022",
                     "people":self.uploadContactsArray] as [String : Any]
        print("param",param)
        
        _ = IssueInviteDeatilsServices().issueInvites(apiURL,
                                                      postData: param as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        let model = userModel as! IssueInviteDeatilsResponse
                                                        print("model",model)
                                                        let message = model.message
                                                        print("message",message as Any)
                                                        let summaryDetails = model.summary[0]
                                                        let duplicate_visitors = summaryDetails.duplicate_visitors
                                                        print("duplicate_visitors",duplicate_visitors as Any)
                                                        let valid_visitors = summaryDetails.valid_visitors
                                                        print("valid_visitors",valid_visitors as Any)
                                                        let invalid_visitors = summaryDetails.invalid_visitors
                                                        print("invalid_visitors",invalid_visitors as Any)
                                                        
                                                        var sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
                                                        sent = sent + valid_visitors!
                                                        UserDefaults.standard.set(sent, forKey:
                                                            StringConstants.NSUserDefauluterKeys.sent)
                                                        
                                                        let congratulationsVC = self.storyboard?.instantiateViewController(withIdentifier: "IssueBadgesCongratulationsViewController") as! IssueBadgesCongratulationsViewController
                                                        congratulationsVC.strSenntInviteCout = valid_visitors!
                                                        self.navigationController?.pushViewController(congratulationsVC, animated: true)
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    // MARK: - Local Service Responder
    override func didFinishLoading(result: NSDictionary, methodName: String)
    {
        DispatchQueue.main.async {
            self.popupAlert(title: "", message: result.value(forKey: "message")as?String, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: SetInviteSummaryViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
            self.DismissHud()
            
        }
        
    }
    
    
    override func didFailed(error: String,result: NSDictionary, methodName: String)
    {
        DispatchQueue.main.async {
            self.popupAlert(title: "", message: result.value(forKey: "message")as?String, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
            self.DismissHud()
            
        }
    }
    
    
    func isValidUser(phoneContact:PhoneContact)->Bool{
        
        let strFname = phoneContact.fName
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRangeFname = strFname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeFname != nil {
            print("Numbers found")
        }
        
        let strLname = phoneContact.LName
        let decimalRangeLname = strLname!.rangeOfCharacter(from: decimalCharacters)
        if decimalRangeLname != nil {
            print("Numbers found")
        }
        
        let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢₹")
        if(phoneContact.phoneNumber.count>0){
            if phoneContact.fName == StringConstants.EMPTY ||  phoneContact.LName == StringConstants.EMPTY || (phoneContact.fName?.length)! < 2 || (phoneContact.LName?.length)! < 2 || !isvalidContact(selectedContact: phoneContact.phoneNumber[0]) || decimalRangeFname != nil || decimalRangeLname != nil{
                return false
            }else if phoneContact.fName != nil {
                if let range = phoneContact.fName!.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
            }else if phoneContact.LName != nil {
                if let range = phoneContact.LName!.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
            }else{
                return true
            }
        }else{
            return false
        }
        return true
    }
    
    
    func isvalidContact(selectedContact: String)-> Bool {
        var phoneNumber = selectedContact //Replace it with the Phone number you want to validate
        phoneNumber = phoneNumber.replacingOccurrences(of: "-", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: " ", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: StringConstants.EMPTY)
        phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: StringConstants.EMPTY)

        if(phoneNumber.contains(" ") ||  phoneNumber.contains("-") ||  phoneNumber.contains("(")  || phoneNumber.contains(")") || phoneNumber.length < 10 ||  phoneNumber.first == "0"  ||  phoneNumber.first == "2" ||  phoneNumber.first == "3" ||  phoneNumber.first == "4" || phoneNumber.first == "5" || phoneNumber.first == "6" || phoneNumber.first == "1"){
                return false
        }else{
                return true
        }
    }
    
    
    func isAllConatctasValid(phoneContactArray:[PhoneContact]) ->Bool{
        for contact in phoneContactArray{
            if(isValidUser(phoneContact: contact)){
            }else{
                return false
            }
        }
        return true
    }
}


extension SelectedContactVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCell=tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
        let data=contactsArray[indexPath.row]as!PhoneContact
        cell.nameLbl.text=data.fName! + " " + data.LName!
        if data.imgIsAvailable ?? false{
            cell.contactImg.image=UIImage.init(data: data.avatarData!)
        }else{
            cell.contactImg.image = UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg)
        }
        
        cell.crossBtn.tag=indexPath.row
        cell.crossBtn.addTarget(self, action: #selector(SelectedContactVC.removeBtnAction), for: .touchUpInside)
        cell.selectionStyle = .none
        
        if(comingFrom ==  StringConstants.flags.issueBadges){
            cell.imgSingleEntry.image = UIImage(named: StringConstants.ImageNames.exhBadge)
            cell.numberLbl.textColor =  UIColor.init(hexString: ColorConstants.marunColor)
            cell.numberLbl.text = StringConstants.kisanMitra.exhBadge
            cell.widthOfImageOfPassType.constant = 18
        }

        if(!isValidUser(phoneContact: data)){
         cell.viewBackgroudBoreder.layer.cornerRadius = 8
         cell.viewBackgroudBoreder.clipsToBounds = true
         cell.viewBackgroudBoreder.layer.borderWidth = 1
         cell.viewBackgroudBoreder.layer.borderColor = UIColor.red.cgColor
         cell.heightConstraintConstantOfErrorLbl.constant = 21
            
        }else{
            cell.viewBackgroudBoreder.layer.cornerRadius = 8
            cell.viewBackgroudBoreder.clipsToBounds = true
            cell.viewBackgroudBoreder.layer.borderWidth = 1
            cell.viewBackgroudBoreder.layer.borderColor = UIColor.lightGray.cgColor
            cell.heightConstraintConstantOfErrorLbl.constant = 0
        }
        
        if UIScreen.main.bounds.size.height <= 568{
            cell.lblErrorMessage.font = UIFont.systemFont(ofSize: 9)
        }else{
            cell.lblErrorMessage.font = UIFont.systemFont(ofSize: 11)
        }
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // if(comingFrom ==  StringConstants.flags.issueBadges){
       //  }else{
        selectedIndex=indexPath.row
        let data=contactsArray[indexPath.row]as!PhoneContact
        print(data.fName!)
        print(data.LName!)
        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "ContactDetailVC")as!ContactDetailVC
        vc.modalPresentationStyle = .overFullScreen
        vc.delegateObj=self
        vc.data=data
        self.present(vc, animated: true, completion: nil)
       // }
    }
    

    @objc func removeBtnAction(sender: UIButton!) {
        
        let value = sender.tag;
        
        contactsArray.removeObject(at: value)
        if contactsArray.count==0{
            self.navigationController?.popViewController(animated: true)
        }else{
            let valueOfSelectedContact = contactsArray.count
            let intSelectedContact = valueOfSelectedContact.formattedWithSeparator
        
            if contactsArray.count > 1 {
                contactCountLbl.text="(\(intSelectedContact) Contacts Selected)"
            }else{
                contactCountLbl.text="(\(intSelectedContact) Contact Selected)"
            }
        }
        self.contactTblView.reloadData()
        
    }
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
       
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);

        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
