//
//  SetCaptionViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 16/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class SetCaptionViewController: UIViewController {

    @IBOutlet weak var HeightOfimageViewOfCaptureImg: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfCaptureImg: UIImageView!
    @IBOutlet weak var lblOfNavigationTitle: UILabel!
    @IBOutlet weak var bottomOfmessageComposingView: NSLayoutConstraint!
    @IBOutlet weak var txtViewOfChatMessage: UITextView!
    @IBOutlet weak var heightConstantOftxtViewOfChatMessage: NSLayoutConstraint!
    @IBOutlet weak var heightConstantOfViewBackChatTextview: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    var selectedImage = UIImage()
    var selectedVideoURL: URL?
    var appDelegate = AppDelegate()
    var commingFrom = String()
    var navigationTitle = String()
    @IBOutlet weak var imageViewOfPlayVideoIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardNotifications()
        initialDesign()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let size = CGSize(width: 0, height: 0)
        if selectedImage.size.width != size.width{
            //Set selected image
            imageViewOfCaptureImg.image = selectedImage
            imageViewOfPlayVideoIcon.isHidden = true
        }else if  selectedVideoURL != nil {
            //Set selected video thumbnail Image
            imageViewOfPlayVideoIcon.isHidden = false
            let thumbImage   = self.getThumbnailImage(forUrl:selectedVideoURL! )
            //Set up orientation to image
            let imageHelper = ImageHelper()
            let upOrientationImage  = imageHelper.fixOrientationOfImage(image: thumbImage!)
            imageViewOfCaptureImg.image = upOrientationImage
            // Play video click on thumbnail image
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SetCaptionViewController.playVideo))
            imageViewOfCaptureImg.isUserInteractionEnabled = true
            self.imageViewOfCaptureImg.addGestureRecognizer(tap)
        }
        
    }
    
        @objc func playVideo() {
            let player = AVPlayer(url: selectedVideoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }

    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }

    
    func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(SetCaptionViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SetCaptionViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK:- Notification
    @objc func keyboardWillShow(_ notification: Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.bottomOfmessageComposingView.constant = -(keyboardFrame.size.height)
            let when = DispatchTime.now() + 0.001 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {

            }
        }, completion: { (completed: Bool) -> Void in
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.bottomOfmessageComposingView.constant = 0.0
            
        }, completion: { (completed: Bool) -> Void in
    
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        if UIScreen.main.bounds.size.height <= 568{
            HeightOfimageViewOfCaptureImg.constant = 402
        }else if UIScreen.main.bounds.size.height == 812{
            HeightOfimageViewOfCaptureImg.constant = 612
        }else{
            HeightOfimageViewOfCaptureImg.constant = 501
        }
       
        
        let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
        if preferredLanguage == "en" {
            print("this is English")
            let strnavtitle = (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendtoLowercase, comment: StringConstants.EMPTY)) + navigationTitle
            lblOfNavigationTitle.text = strnavtitle
        } else if preferredLanguage == "mr" {
            print("this is Ukrainian")
            let strnavtitle = navigationTitle  + (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendtoLowercase, comment: StringConstants.EMPTY))
            lblOfNavigationTitle.text = strnavtitle
        }
        else{
            let strnavtitle = navigationTitle  + (NSLocalizedString(StringConstants.NSUserDefauluterValues.sendtoLowercase, comment: StringConstants.EMPTY))
            lblOfNavigationTitle.text = strnavtitle
        }
    }
    
    func initialDesign() {
        self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.setCaptionBackViewColor)
        txtViewOfChatMessage.text = (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY))
        txtViewOfChatMessage.textColor = UIColor.lightGray
        txtViewOfChatMessage.selectedTextRange = txtViewOfChatMessage.textRange(from: txtViewOfChatMessage.beginningOfDocument, to: txtViewOfChatMessage.beginningOfDocument)
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title:(NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)) , style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtViewOfChatMessage.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            appDelegate.selctedImage = UIImage()
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is ChannelSendBroadcastMsgVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }else if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is OneToOneFromAdminVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }
    }
    
    
    @IBAction func btnSendClick(_ sender: Any) {
      
        if commingFrom == StringConstants.flags.channelSendBroadcastMsgVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
            // Pass image & caption text to ChannelSendBroadcastMsgVC
            
            let size = CGSize(width: 0, height: 0)
            if selectedImage.size.width != size.width{
                appDelegate.selctedImage = selectedImage
                if txtViewOfChatMessage.text == (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY)){
                    appDelegate.strCaptionText = ""
                }else{
                    appDelegate.strCaptionText = txtViewOfChatMessage.text
                }
            }else if  selectedVideoURL != nil {
                appDelegate.selectedVideoURL = selectedVideoURL
                appDelegate.videoThumbNailImage = imageViewOfCaptureImg.image!
                if txtViewOfChatMessage.text == (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY)){
                    appDelegate.strCaptionText = ""
                }else{
                    appDelegate.strCaptionText = txtViewOfChatMessage.text
                }
            }
            
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is ChannelSendBroadcastMsgVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }else if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "OneToOneFromAdminVC") as! OneToOneFromAdminVC
            // Pass image & caption text to OneToOneFromAdminVC
            
            let size = CGSize(width: 0, height: 0)
            if selectedImage.size.width != size.width{
                appDelegate.selctedImage = selectedImage
                if txtViewOfChatMessage.text == (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY)){
                    appDelegate.strCaptionText = ""
                }else{
                    appDelegate.strCaptionText = txtViewOfChatMessage.text
                }
            }else if  selectedVideoURL != nil {
                appDelegate.selectedVideoURL = selectedVideoURL
                appDelegate.videoThumbNailImage = imageViewOfCaptureImg.image!
                if txtViewOfChatMessage.text == (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY)){
                    appDelegate.strCaptionText = ""
                }else{
                    appDelegate.strCaptionText = txtViewOfChatMessage.text
                }
            }
            
            if let nav = self.navigationController {
                //self.navigationController?.popViewController(animated: true)
                for controller in nav.viewControllers as Array {
                    if controller is OneToOneFromAdminVC {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        return
                    }
                }
                nav.pushViewController(vc, animated: false)
                return
            }
        }
        
    }
    
}


extension SetCaptionViewController: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true
        
        let currentText = textView.text
        let updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        let trimmedString = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if txtViewOfChatMessage.textColor == UIColor.lightGray && !text.isEmpty && trimmedString != "" && !trimmedString.isEmpty && trimmedString != StringConstants.placeHolder.writeCaption  {
            textView.text = nil
            txtViewOfChatMessage.textColor = UIColor.white
        }
        
        //Line number limitation for message textview
        var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        let boundingRect = sizeOfString(string: updatedText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        
        if numberOfLines >= 3  {
            txtViewOfChatMessage.isScrollEnabled = true
        } else {
            txtViewOfChatMessage.isScrollEnabled = false
            
        }
         return true
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedStringKey.font: font],
                                                 context: nil).size
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 250)
        heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 250)
        txtViewOfChatMessage.isScrollEnabled = true
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if textView.text.isEmpty {
            textView.text = (NSLocalizedString(StringConstants.placeHolder.writeCaption, comment: StringConstants.EMPTY))
            textView.textColor = UIColor.lightGray
            txtViewOfChatMessage.resignFirstResponder()
            txtViewOfChatMessage.isScrollEnabled = false
            heightConstantOfViewBackChatTextview.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.priority = UILayoutPriority(rawValue: 999)
            heightConstantOftxtViewOfChatMessage.constant = 33
            heightConstantOfViewBackChatTextview.constant = 53
            self.view.layoutIfNeeded()
        }
    }
}
