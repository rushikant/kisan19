//
//  ShowUserProfileVC.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 13/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class ShowUserProfileVC: UIViewController {

    @IBOutlet weak var viewOfBackUserProfile: UIView!
    @IBOutlet weak var imageViewOfUserProfile: UIImageView!
    @IBOutlet weak var viewOfBackbtnEditProfileClick: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tblOfFollowerList: UITableView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var viewOfBackInfoTab: UIView!
    @IBOutlet weak var btnEditUserProfile: UIButton!
    @IBOutlet weak var imageViewOfEditProfile: UIImageView!
    var myChatsList = [MyChatsTable]()
    var myChatsListTemp = [MyChatsTable]()
    var myChatsForPassNext = MyChatsTable()
    var userId = String()
    @IBOutlet weak var heightOfTblFollowerList: NSLayoutConstraint!
    var arrOfMenusAdmin = [(NSLocalizedString(StringConstants.popUpMenuName.channelprofile, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.sendaMessage, comment: StringConstants.EMPTY))]
    var arrOfMenuImagesAdmin  = [StringConstants.ImageNames.channelblack,StringConstants.ImageNames.sms]
    var arrOfMenusFollower = [(NSLocalizedString(StringConstants.popUpMenuName.channelprofile, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.sendaMessage, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.popUpMenuName.leaveChannel, comment: StringConstants.EMPTY))]
    var arrOfMenuImagesFollower  = [StringConstants.ImageNames.channelblack,StringConstants.ImageNames.sms,StringConstants.ImageNames.clear]
    @IBOutlet weak var viewOfPopUp: UIView!
    @IBOutlet weak var viewOfBackPopUp: UIView!
    @IBOutlet weak var tblOfMenuOption: UITableView!
    @IBOutlet weak var followChannelCount: UILabel!
    var categories_list: [CategoriesList] = []
    var communityMembersData: [CommunityMembersList] = []
    var commingFrom = String()
    @IBOutlet weak var heightOfMobileIcon: NSLayoutConstraint!
    @IBOutlet weak var heightOfMobileNumLable: NSLayoutConstraint!
    @IBOutlet weak var btnViewAll: UIButton!
     var commonCommunityList: [CommonChannelList] = []
    var commonChannelListForPassNext = [CommonChannelList]()
    var checkAdmin : Bool = false
    @IBOutlet weak var heightOfViewOnPopUpView: NSLayoutConstraint!
    @IBOutlet weak var lblThatsYou: UILabel!
    @IBOutlet weak var constantOfHeightOflblDescri: NSLayoutConstraint!
    @IBOutlet weak var lblOfDescription: UILabel!
    var membersListData = MembersTable()
    var membersData = [MembersTable]()
    var myChatsData = MyChatsTable()
    @IBOutlet var lblInfoTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnViewAll.setTitle((NSLocalizedString(StringConstants.showUserProfile.viewAll, comment: StringConstants.EMPTY)), for: .normal)
        lblInfoTitle.text = (NSLocalizedString(StringConstants.showUserProfile.info, comment: StringConstants.EMPTY))
        lblThatsYou.text = (NSLocalizedString(StringConstants.showUserProfile.thatsYou, comment: StringConstants.EMPTY))
        // Do any additional setup after loading the view.
        initialDesign()
    }
    
    func setData()  {
        if commingFrom == StringConstants.flags.ChannelFollowerList{
            //Show categories Name
            callGetUserAreaInterestAPI()
            //Set UserProfile data
           // self.getUserDetails(userId:self.membersListData.userId!)
            if let userId = self.membersListData.userId {
                self.getUserDetails(userId:userId)
            }else{
                self.getUserDetails(userId:self.membersListData.id!)
            }
            setProfileData()
        }else if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            
            //check internet connection
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                btnViewAll.isHidden = false
                self.getUserDetails(userId:myChatsData.userId!)
            } else{
                btnViewAll.isHidden = true
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
            }
            
        }else{
            //Show categories Name
            callGetUserAreaInterestAPI()

            heightOfMobileIcon.constant = 25
            heightOfMobileNumLable.constant = 21
            if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
                if accountType == StringConstants.ONE {
                    viewOfBackbtnEditProfileClick.isHidden = false
                }else{
                    viewOfBackbtnEditProfileClick.isHidden = true
                    
                }
            }
            lblThatsYou.isHidden = false
            let firstName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
            let lastName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
            
            self.lblUserName.text = firstName + StringConstants.singleSpace + lastName
            if let userProfilePic = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl) {
                self.imageViewOfUserProfile.sd_setImage(with: URL(string: userProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg), options: .refreshCached)
            }
            
            
            self.lblMobileNumber.text = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
            
            let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
            let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
            let country = StringConstants.country
            self.lblAddress.text = city + StringConstants.comma + state +  StringConstants.comma + country
            if let about = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.about) {
                self.lblOfDescription.text = about
                self.lblOfDescription.isHidden = false
                constantOfHeightOflblDescri.priority = UILayoutPriority(rawValue: 250)
            }else{
                self.lblOfDescription.isHidden = true
                self.constantOfHeightOflblDescri.priority = UILayoutPriority(rawValue: 999)
                self.constantOfHeightOflblDescri.constant = 0
            }
            self.myChatsList.removeAll()
            self.myChatsListTemp.removeAll()
            self.myChatsList = MyChatServices.sharedInstance.getIsMemberTypeTrueMyChatDetails()
            self.myChatsListTemp = self.myChatsList
            if myChatsList.count == 0 || myChatsList.count <= 5 {
                btnViewAll.isHidden = true
            }else{
                btnViewAll.isHidden = false
            }
          
            self.followChannelCount.text = (NSLocalizedString(StringConstants.showUserProfile.channelsYouFollows, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + String(self.myChatsList.count)
//            if userId == memberData.userId{
//                lblThatsYou.isHidden = false
//            }else{
//                lblThatsYou.isHidden = true
//            }
            
            var myChatCount = Int()
            if self.myChatsList.count  > 5 {
                myChatCount = self.myChatsList.count - 5
            }
            
            self.myChatsList.removeLast(myChatCount)
            self.tblOfFollowerList.reloadData()
        }

    }
    
    
    func setProfileData(){
       
        heightOfMobileIcon.constant = 0
        heightOfMobileNumLable.constant = 0
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
            if accountType == StringConstants.ONE {
                viewOfBackbtnEditProfileClick.isHidden = true
            }else{
                viewOfBackbtnEditProfileClick.isHidden = true
                
            }
        }
        let memberData = self.membersListData
        let firstName = memberData.firstName
        let lastName = memberData.lastName
        lblThatsYou.isHidden = true
        self.lblUserName.text = firstName! + StringConstants.singleSpace + lastName!
        if let userProfilePic = memberData.imageUrl {
            self.imageViewOfUserProfile.sd_setImage(with: URL(string: userProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg), options: .refreshCached)
        }
        let city = memberData.city
        let state = memberData.state
        self.lblAddress.text = city! + StringConstants.comma + state! +  StringConstants.comma + StringConstants.country
        
        if let about = memberData.profileStatus {
            self.lblOfDescription.text = about
            self.lblOfDescription.isHidden = false
            constantOfHeightOflblDescri.priority = UILayoutPriority(rawValue: 250)
        }else{
            self.lblOfDescription.isHidden = true
            self.constantOfHeightOflblDescri.priority = UILayoutPriority(rawValue: 999)
            self.constantOfHeightOflblDescri.constant = 0
        }
        
        if userId == memberData.userId{
            lblThatsYou.isHidden = false
        }else{
            lblThatsYou.isHidden = true
        }
        
        //check internet connection
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            //Get Common Channel
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.getCommonFollowChannelNames()
        }else{
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
        }
        
    }
    
    func getUserDetails(userId:String){
        let params = ["":""]
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.getUserDetails1 + userId + URLConstant.getUserDetails2
        _ = GetUserDetailsServices().getUserDetailsServices(apiURL,
                                                                      postData: params as [String : AnyObject],
                                                                      withSuccessHandler: { (userModel) in
                                                                        let model = userModel as! GetUserDetailsResponse
                                                                        print("model",model)
                                                                        
                                                                        //Show categories Name
                                                                        self.callGetUserAreaInterestAPI()
                                                                        self.membersData = MembersServices.sharedInstance.getMembersDetailsOnId(id:userId/*self.myChatsData.userId!*/)
                                                                         self.membersListData = self.membersData[0]
                                                                        //Set UserProfile data
                                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                                        self.setProfileData()
                                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        // Set Data
        self.setData()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
    }
    
    func getCommonFollowChannelNames() {
        var userId  = String()
        if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            userId = self.membersListData.id!
        }else{
            //userId = self.membersListData.userId!
            if let userIdValue = self.membersListData.userId{
                userId = userIdValue
            }else{
                userId = self.membersListData.id!
            }
        }
        
        let params = ["":""]
        let apiURL = URLConstant.BASE_URL + URLConstant.getCommonChannelList1 + userId + URLConstant.getCommonChannelList2
        print("apiURL",apiURL)
        _ = GetCommonChannelListServices().getCommonChannelList(apiURL,
                                                                postData: params as [String : AnyObject],
                                                                withSuccessHandler: { (userModel) in
                                                                    
                                                                    let model = userModel as! CommonChannelListResponse
                                                                    self.commonCommunityList = model.commonCommunityList
                                                                    
                                                                    self.myChatsList.removeAll()
                                                                    self.myChatsListTemp.removeAll()
                                                                    for  i in (0..<self.commonCommunityList.count){
                                                                        let commonCommunityData = self.commonCommunityList[i]
                                                                        let  dict  = ConvertToDicCommunityCommunityObject.ConvertToDicCommunityCommunityObject(communityDetail: [commonCommunityData]) 
                                                                        let myChatData = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict)
                                                                        self.myChatsList.append(myChatData)
                                                                    }
                                                                    self.myChatsListTemp = self.myChatsList
                                                                    if self.myChatsList.count == 0 || self.myChatsList.count <= 5 {
                                                                        self.btnViewAll.isHidden = true
                                                                    }else{
                                                                        self.btnViewAll.isHidden = false
                                                                    }
                                                                    self.followChannelCount.text = (NSLocalizedString(StringConstants.showUserProfile.channelsYouFollows, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + String(self.myChatsList.count)
                                                                    
                                                                    
                                                                    if self.commingFrom == StringConstants.flags.ChannelFollowerList  {
                                                                        let  userId1 = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                                                                        if userId == userId1
                                                                        {
                                                                            self.followChannelCount.text = (NSLocalizedString(StringConstants.showUserProfile.channelsYouFollows, comment: StringConstants.EMPTY)) + StringConstants.singleSpace + String(self.myChatsList.count)
                                                                            
                                                                        }else{
                                                                            self.followChannelCount.text = (NSLocalizedString(StringConstants.showUserProfile.commonChannels, comment: StringConstants.EMPTY))  + StringConstants.singleSpace + String(self.myChatsList.count) }//New
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    var myChatCount = Int()
                                                                    if self.myChatsList.count  > 5 {
                                                                        myChatCount = self.myChatsList.count - 5
                                                                    }
                                                                    self.myChatsList.removeLast(myChatCount)
                                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                                    self.tblOfFollowerList.reloadData()
                                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    func callGetUserAreaInterestAPI() {
        let params = ["":""]
        print("params",params)
        let path = Bundle.main.path(forResource: "categoriesList", ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        
        let apiURL = String(describing: fileUrl)
        _ = UserAreaOfInterestServices().getInterest(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! UserAreaOfInterestResponse
                                                        let categoriesList = model.categories_list
                                                        let categoryData = categoriesList.filter( { (list: CategoriesList) -> Bool in
                                                            return list.is_occupation == false
                                                        })
                                                        
                                                        var categoryId = String()
                                                        if self.commingFrom == StringConstants.flags.ChannelFollowerList  {
                                                            let memberData = self.membersListData //self.communityMembersData[0]
                                                            categoryId = memberData.categoryIds!
                                                            
                                                            
                                                          //  self.followChannelCount.text = StringConstants.showUserProfile.commonChannels + StringConstants.singleSpace + String(self.myChatsList.count) //New
                                                            
                                                            
                                                            
                                                            
                                                        }else if self.commingFrom == StringConstants.flags.oneToOneFromAdminVC{
                                                            let memberData = self.membersListData
                                                            categoryId = memberData.categoryIds!
                                                        }else{
                                                            categoryId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.interests)
                                                        }
                                                        
                                                        if !categoryId.isEmpty {
                                                            var arrOfId = categoryId.components(separatedBy: ",")
                                                            arrOfId = arrOfId.filter { $0 != "" }
                                                            var strCategoryName = String()
                                                            self.categories_list.removeAll()
                                                            for  i in (0..<arrOfId.count){
                                                                let strId = arrOfId[i]
                                                                let categories_list = categoryData.filter { ($0.id)?.range(of: strId, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                                                                
                                                                if categories_list.count != 0 {
                                                                    let category = categories_list[0]
                                                                    self.categories_list.append(category)
                                                                    if strCategoryName.isEmpty{
                                                                        strCategoryName = category.name!
                                                                    }else{
                                                                        strCategoryName = strCategoryName + "," + category.name!
                                                                    }
                                                                    self.lblCategories.text = strCategoryName
                                                                }
                                                            }
                                                        }
                                                        
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }

    override func viewDidLayoutSubviews() {
        super.updateViewConstraints()
            heightOfTblFollowerList.constant = CGFloat(self.myChatsList.count * 70)
        self.updateViewConstraints()
    }
    
    func initialDesign(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        viewOfBackInfoTab.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewOfBackInfoTab.layer.shadowOpacity = 0.3
        viewOfBackInfoTab.layer.shadowRadius = 1
        viewOfBackInfoTab.layer.shadowColor = UIColor.black.cgColor
        
        viewOfBackbtnEditProfileClick.layer.cornerRadius = viewOfBackbtnEditProfileClick.frame.size.width/2
        viewOfBackbtnEditProfileClick.clipsToBounds = true
        btnEditUserProfile.layer.cornerRadius = btnEditUserProfile.frame.size.width/2
        btnEditUserProfile.clipsToBounds = true
        tblOfFollowerList.separatorColor = UIColor.clear
        tblOfFollowerList.tableFooterView = UIView()
        
        imageViewOfUserProfile.layer.cornerRadius = imageViewOfUserProfile.frame.size.width / 2
        imageViewOfUserProfile.clipsToBounds = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AllFollowChannelList.hideMenuOptionPopUp))
        tap.cancelsTouchesInView = false
        self.viewOfPopUp.addGestureRecognizer(tap)
    }
    
    @objc func hideMenuOptionPopUp() {
        self.viewOfPopUp.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        
        if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            self.navigationController?.popViewController(animated: true)
        }else if commingFrom == StringConstants.flags.ChannelFollowerList{
            self.navigationController?.popViewController(animated: true)
        }else{
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            if let nav = self.navigationController {
                for controller in nav.viewControllers as Array {
                    if controller is DashboardViewController {
                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                        break
                    }
                }
                nav.pushViewController(vc, animated: false)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnOfUserProfileClick(_ sender: Any) {
        if commingFrom == StringConstants.flags.ChannelFollowerList{
            let memberData = self.membersListData //self.communityMembersData[0]
            if !(memberData.imageUrl?.isEmpty)! {
                let fullViewProfilePicVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewProfilePicVC") as! FullViewProfilePicVC
                fullViewProfilePicVC.commingFrom = StringConstants.flags.showUserProfileVC
                fullViewProfilePicVC.imageData = memberData.imageUrl!
                self.navigationController?.pushViewController(fullViewProfilePicVC, animated: false)
            }
        }else if commingFrom == StringConstants.flags.oneToOneFromAdminVC{
            let memberData = self.membersListData
            if let userProfilePic = memberData.imageUrl {
                let fullViewProfilePicVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewProfilePicVC") as! FullViewProfilePicVC
                fullViewProfilePicVC.commingFrom = StringConstants.flags.showUserProfileVC
                fullViewProfilePicVC.imageData = userProfilePic
                self.navigationController?.pushViewController(fullViewProfilePicVC, animated: false)
            }
        }
        else{
            if let userProfilePic = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl), !userProfilePic.isEmpty{
                let fullViewProfilePicVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewProfilePicVC") as! FullViewProfilePicVC
                fullViewProfilePicVC.commingFrom = StringConstants.flags.showUserProfileVC
                fullViewProfilePicVC.imageData = userProfilePic
                self.navigationController?.pushViewController(fullViewProfilePicVC, animated: false)
            }
        }
    }
    
    @IBAction func btnEditUserProfileClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let editUserProfileVC = storyboard.instantiateViewController(withIdentifier: "EditUserProfileVC") as! EditUserProfileVC
        editUserProfileVC.categories_list =  self.categories_list
        self.navigationController?.pushViewController(editUserProfileVC, animated: true)
    }
    
    @IBAction func btnViewAllClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let allFollowChannelList = storyboard.instantiateViewController(withIdentifier: "AllFollowChannelList") as! AllFollowChannelList
        allFollowChannelList.commingFrom = commingFrom
        allFollowChannelList.myChatsList = self.myChatsListTemp
        self.navigationController?.pushViewController(allFollowChannelList, animated: true)
    }

}


extension ShowUserProfileVC : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOfMenuOption{
            if checkAdmin == true{
                return arrOfMenusAdmin.count
            }else{
                return arrOfMenusFollower.count
            }
        }else{
            return myChatsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if tableView == tblOfMenuOption{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionMenuCell", for: indexPath) as! OptionMenuCell
            cell.selectionStyle = .none
            if checkAdmin == true{
                if indexPath.row == 2 {
                    cell.lblMenuTitle.textColor = UIColorFromRGB.init(rgb: ColorConstants.textColorOnAllFollowerChannelListPopUp)
                }else{
                    cell.lblMenuTitle.textColor = UIColor.darkGray
                }
                cell.lblMenuTitle.text = arrOfMenusAdmin[indexPath.row]
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImagesAdmin[indexPath.row])
            }else{
                cell.lblMenuTitle.textColor = UIColor.darkGray
                cell.lblMenuTitle.text = arrOfMenusFollower[indexPath.row]
                cell.imageViewOfOption.image = UIImage(named:arrOfMenuImagesFollower[indexPath.row])
            }
            return cell
         }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelListOfYouFollowCell", for: indexPath) as! ChannelListOfYouFollowCell
            cell.selectionStyle = .none
            cell.imageviewOfUserProfile.layer.cornerRadius = cell.imageviewOfUserProfile.frame.size.width / 2
            cell.imageviewOfUserProfile.clipsToBounds = true
            cell.lblUserRole.isHidden = true
            let myChats = myChatsList[indexPath.row]
            if let communityName = myChats.communityName, !communityName.isEmpty {
                cell.lblChannelName.text = communityName //StringUTFEncoding.UTFEncong(string: communityName)
            }
            if let channelProfilePic = myChats.communityImageBigThumbUrl {
                cell.imageviewOfUserProfile.sd_setImage(with: URL(string: channelProfilePic), placeholderImage: UIImage(named: StringConstants.ImageNames.communityPlaceholderImg), options: .refreshCached)
            }
            
            if myChats.ownerId == userId{
                cell.lblUserRole.isHidden = false
            }else{
                cell.lblUserRole.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      /*  if tableView == tblOfMenuOption{
            navigationController?.setNavigationBarHidden(false, animated: false)
            if indexPath.row == 0 {
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelProfileVC = storyboard.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
                channelProfileVC.myChatsData = myChatsForPassNext
                channelProfileVC.commingFrom = StringConstants.flags.FromUserProfile
                viewOfPopUp.isHidden = true
                self.navigationController?.pushViewController(channelProfileVC, animated: true)
            }else if indexPath.row == 1 {
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelSendBroadcastMsgVC = storyboard.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                channelSendBroadcastMsgVC.communityName = myChatsForPassNext.communityName!
                channelSendBroadcastMsgVC.communityKey = myChatsForPassNext.communityKey!
                channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsForPassNext.communityImageSmallThumbUrl!
                channelSendBroadcastMsgVC.myChatsData = myChatsForPassNext
                viewOfPopUp.isHidden = true
                self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
            }else{
                viewOfPopUp.isHidden = true
                popupAlert(title: StringConstants.EMPTY, message: (NSLocalizedString(StringConstants.AlertMessage.leaveChannelConfirmation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                        self.leaveChannel(myChatData:self.myChatsForPassNext)
                    }, nil])
            }
        }else{
            navigationController?.setNavigationBarHidden(true, animated: false)
            myChatsForPassNext = myChatsList[indexPath.row]
            if myChatsForPassNext.ownerId == userId{
                checkAdmin = true
                heightOfViewOnPopUpView.constant = CGFloat(44 * arrOfMenusAdmin.count)
            }else{
                checkAdmin = false
                heightOfViewOnPopUpView.constant = CGFloat(44 * arrOfMenusFollower.count)
            }
            tblOfMenuOption.reloadData()
            viewOfPopUp.isHidden = false
        }*/
        
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let channelProfileVC = storyboard.instantiateViewController(withIdentifier: "ChannelProfileVC") as! ChannelProfileVC
        channelProfileVC.myChatsData = myChatsList[indexPath.row]
        channelProfileVC.commingFrom = StringConstants.flags.FromUserProfile
        viewOfPopUp.isHidden = true
        self.navigationController?.pushViewController(channelProfileVC, animated: true)
    }
    
    func leaveChannel(myChatData:MyChatsTable){
        
        let params = LeaveChannelRequest.convertToDictionary(leaveChannelDetails: LeaveChannelRequest.init(operation:StringConstants.channelProfile.leaveChannelOperation , communityJabberId: myChatData.communityJabberId!, new_admin_user_id: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.leaveCommunity1 + myChatData.communityKey! + URLConstant.leaveCommunity2 + userId
        print("apiURL",apiURL)
        _ = LeaveChannelServices().leaveChannelServices(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! LeaveChannelResponse
                                                            print("model",model)
                                                            CommunityDetailService.sharedInstance.updateisMemberFromComminityDetailsTbl(isMemberValue: StringConstants.ZERO, communityKey: myChatData.communityKey!)
                                                            let currentTime = DateTimeHelper.getCurrentMillis()
                                                            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: StringConstants.DashboardViewController.unfollowStatus, latestMessageTime:String(currentTime), communityKey:myChatData.communityKey!,mediaType:StringConstants.EMPTY,messageType:StringConstants.MessageType.banner, lastMessageId: "")
                                                            MyChatServices.sharedInstance.updateisMemberFromMyChatTbl(isMemberValue: StringConstants.ZERO, communityKey: myChatData.communityKey!)
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            
                                                            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                            let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                                            if let nav = self.navigationController {
                                                                for controller in nav.viewControllers as Array {
                                                                    if controller is DashboardViewController {
                                                                        let _ = nav.popToViewController(controller as UIViewController, animated: false)
                                                                        break
                                                                    }
                                                                }
                                                                nav.pushViewController(vc, animated: false)
                                                                return
                                                            }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOfMenuOption{
             return 44
        }
         return 70
    }
}
