//
//  SupportViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 14/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var lblOfInfo: UILabel!
    @IBOutlet var lblhelp: UILabel!
    @IBOutlet var lblregards: UILabel!
    @IBOutlet var lblteamKisan: UILabel!
    @IBOutlet var lblnamaskar: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        lblhelp.text    = (NSLocalizedString(StringConstants.supportViewController.helptext, comment: StringConstants.EMPTY))
        lblregards.text = (NSLocalizedString(StringConstants.supportViewController.Regards, comment: StringConstants.EMPTY))
        lblnamaskar.text = (NSLocalizedString(StringConstants.supportViewController.Namaskar, comment: StringConstants.EMPTY))
        lblteamKisan.text = (NSLocalizedString(StringConstants.supportViewController.TeamKisan, comment: StringConstants.EMPTY))
        
        let attrs = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 15)]
        let attributedString2 = NSMutableAttributedString(string:(NSLocalizedString(StringConstants.supportViewController.Email, comment: StringConstants.EMPTY)), attributes:attrs)
         let attributedString3 = NSMutableAttributedString(string: (NSLocalizedString(StringConstants.supportViewController.Supporttextpart2, comment: StringConstants.EMPTY)), attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
        let attributedString4 = NSMutableAttributedString(string:(NSLocalizedString(StringConstants.supportViewController.contactNo, comment: StringConstants.EMPTY)), attributes:attrs)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 20
        
        let attrString = NSMutableAttributedString(string: (NSLocalizedString(StringConstants.supportViewController.SupportInfo, comment: StringConstants.EMPTY)))
        attrString.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        
        
        attrString.append(attributedString2)
        attrString.append(attributedString3)
        attrString.append(attributedString4)
        lblOfInfo.attributedText = attrString
        lblOfInfo.textAlignment = .center
        initialDesign()
    }
    
    @IBAction func btnComposeMailClick(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
   
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["app@kisan.com"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
      popupAlert(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", actionTitles: ["OK"], actions:[{action1 in
            },{action2 in
            }, nil])
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func initialDesign() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.support, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
