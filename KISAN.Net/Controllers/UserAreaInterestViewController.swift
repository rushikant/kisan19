//
//  UserAreaInterestViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 25/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import Contacts
import SDWebImage
import MBProgressHUD

//MARK: step 1 Add Protocol here
protocol PassSelectedCategory: class {
    func PassSelectedCategory(_ category: [CategoriesList])
}

class UserAreaInterestViewController: UIViewController {

    @IBOutlet weak var tblOfUserInterest: UITableView!
    var arrOfCheckBoxTemp = [String]()
    var arrOfCategoriesId = [String]()
    var commingFromScreen = String()
    var dominantColour = String()
    var lblOfTitle = UILabel()
    var lblOfseprator = UILabel()
    var base64String: String = ""
    var retriveDataOfCommunityTypeChannel = String()
    var mobiles = [CNPhoneNumber]()
    var  contact = CNMutableContact() //CNContact()
    var categories_list: [CategoriesList] = []
    var groupName = String()
    var communityPrivacy = String()
    var communityDescprionData = String()
    var communityDetailsList: [CommunityDetailsList] = []
    var communityDetilas = Dictionary<String, Any>()
    var createCommunityRequest = Dictionary<String, Any>()
    var communityJabberId = String()
    var communityImageUrl = String()
    var userId = String()
    var selectedCategories_list: [CategoriesList] = []
    //MARK: step 2 Create a delegate property here, don't forget to make it weak!
     weak var delegate: PassSelectedCategory?
    var appDelegate = AppDelegate()
    public var interests = [String]()
    var imageBlob = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        // Do any additional setup after loading the view.
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        callGetUserAreaInterestAPI()
    }
    
    
    func callGetUserAreaInterestAPI() {
        let params = ["":""]
        print("params",params)
        let path = Bundle.main.path(forResource: "categoriesList", ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        
        let apiURL = String(describing: fileUrl)
        _ = UserAreaOfInterestServices().getInterest(apiURL,
                                      postData: params as [String : AnyObject],
                                      withSuccessHandler: { (userModel) in
                                        let model = userModel as! UserAreaOfInterestResponse
                                        let categoriesList = model.categories_list
                                        self.categories_list = categoriesList.filter( { (list: CategoriesList) -> Bool in
                                            return list.is_occupation == false
                                        })
                                        
                                        for  _ in (0..<self.categories_list.count){
                                            self.arrOfCheckBoxTemp .append("0")
                                            self.arrOfCategoriesId.append("")
                                        }
                                        
                                        if self.selectedCategories_list.count > 0 {
                                            if self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditUserProfileVC || self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC{
                                                for  i in (0..<self.selectedCategories_list.count){
                                                    let selectedCategory = self.selectedCategories_list[i]
                                                    let selectedId = selectedCategory.id
                                                    //Get IndexValue
                                                    let index =   self.categories_list.index { (selectedCategory) -> Bool in
                                                        selectedCategory.id == selectedId
                                                    }
                                                    self.arrOfCheckBoxTemp.remove(at: index!)
                                                    self.arrOfCheckBoxTemp.insert("1", at: index!)
                                                    self.arrOfCategoriesId.remove(at: index!)
                                                    self.arrOfCategoriesId.insert((selectedId)!, at: index!)
                                                }
                                            }
                                        }
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        self.tblOfUserInterest.reloadData()
                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    func getPersonIndex(from: [CategoriesList], id: CategoriesList) -> Int? {
        return from.index(where: { $0 === id })
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        if #available(iOS 11.2, *) {
            navigationController?.navigationBar.tintAdjustmentMode = .normal
            navigationController?.navigationBar.tintAdjustmentMode = .automatic
        }
        
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        checkTypeOfCommunity()
        if commingFromScreen == StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController || commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC {
            if dominantColour.isEmpty {
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
                
            }else{
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: dominantColour)
            }
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }
    }
    
    func checkTypeOfCommunity(){
        
        guard let retriveDataOfCommunityType =  SessionDetailsForCommunityType.shared.communityTypeChannel else {
            return
        }
        retriveDataOfCommunityTypeChannel = retriveDataOfCommunityType
    }
    
    func initialDesign() {
    
       // self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.next, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(nextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.tblOfUserInterest.separatorStyle = UITableViewCellSeparatorStyle.none
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        if let commingFrom = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.commingFromScreen) {
            commingFromScreen = commingFrom
        }
        
        if commingFromScreen == StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController || commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC{
            
            lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 100 /*view.frame.width - 90*/, height: 70))
            lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.selectCategory, comment: StringConstants.EMPTY))
            lblOfTitle.numberOfLines = 3
            lblOfTitle.textColor = UIColor.white
            lblOfTitle.font = UIFont.boldSystemFont(ofSize: 14.0)
            navigationItem.titleView = lblOfTitle
            
        }else{
            lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
            lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.myInterests, comment: StringConstants.EMPTY))
            lblOfTitle.textColor = UIColor.white
            lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
            navigationItem.titleView = lblOfTitle
        }
    
       
        
    }

    @objc func backTapped(sender: UIBarButtonItem) {
        
        if commingFromScreen == StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController || commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC ||  commingFromScreen == StringConstants.NSUserDefauluterValues.EditUserProfileVC{
            self.navigationController?.popViewController(animated: true)
        }else{
            exit(0)
        }
    }
    
    @objc func nextTapped(sender: UIBarButtonItem) {
        var categoriesId = arrOfCategoriesId
        categoriesId = categoriesId.filter { $0 != "" }
     
        if commingFromScreen == StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController  {
            if categoriesId.count == 0 {
                popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectsAtLeastCategory, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else if categoriesId.count > 3 {
                popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectMaxCategory, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString((NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY)), comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else{
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                let joindCategoriesId = categoriesId.joined(separator: ",")
                self.callCreateCommunityService(categoryIdValues:joindCategoriesId)
            }
        }else if commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC {
            if categoriesId.count == 0 {
                popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectsAtLeastCategory, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else if categoriesId.count > 3 {
                popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectMaxCategory, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else{
                delegate?.PassSelectedCategory( self.selectedCategories_list)
                self.navigationController?.popViewController(animated: false)
            }
        }else if commingFromScreen == StringConstants.NSUserDefauluterValues.EditUserProfileVC {
            if categoriesId.count == 0 {
                popupAlert(title: "", message:(NSLocalizedString(StringConstants.Validation.selectsAtLeastCategory, comment: StringConstants.EMPTY)) , actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else{
                delegate?.PassSelectedCategory( self.selectedCategories_list)
                self.navigationController?.popViewController(animated: false)
            }
        }else{
            if categoriesId.count == 0 {
                popupAlert(title: "", message: (NSLocalizedString(StringConstants.Validation.selectsAtLeastCategory, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])
            }else {
                
                if !(imageBlob.isEmpty){
                    self.callEditUserProfile()
                }else{
                    self.editUserProfileForCategories()
                }
            }
        }
    }
    
    
    
    func callCreateCommunityService(categoryIdValues:String) {
        
        let password = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
        let currentTime = DateTimeHelper.getCurrentMillis()
        let ownerName = String(SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)) + " " + String(SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName))
        let ownerId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let ownerImageUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageUrl)
        
        communityDetilas = CreateCommunityDetailsRequest.convertToDictionary(createCommunityDetails: CreateCommunityDetailsRequest.init(communityCategories: categoryIdValues, communityDesc: communityDescprionData, communityDominantColour: dominantColour, communityImageBigThumbUrl: communityImageUrl, communityImageSmallThumbUrl: communityImageUrl, communityImageUrl: communityImageUrl, communityName: groupName, communityScore: 0, ownerName: ownerName, privacy: communityPrivacy, totalMembers: 0, type: StringConstants.CommunityType.C, communityJabberId: communityJabberId, communityJoinedTimestamp: String(currentTime), isCommunityWithRssFeed: false , isDefault: false,ownerId:ownerId,ownerImageUrl:ownerImageUrl) )
       
        let params = CreateCommunityRequest.convertToDictionary(createCommunityRequest: CreateCommunityRequest.init(communityDetails: communityDetilas, password: password, resource: RequestName.resourceName + String(currentTime))) as Dictionary<String, Any>
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.createCommunity
        _ = CreateCommunityServices().createCommunity(apiURL,
                                                      postData: params as [String : AnyObject],
                                                   withSuccessHandler: { (userModel) in
                                                    let model = userModel as! CreateChannelResponse
                                                    self.communityDetailsList = model.communityDetailsList
                                                    MBProgressHUD.hide(for: self.view, animated: true);
                                                    let selectMembersViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                                                    selectMembersViewController.dominantColour = self.dominantColour
                                                    selectMembersViewController.communityKey = model.communityKey!
                                                    selectMembersViewController.strComingFrom = StringConstants.flags.userAreaInterestViewController
                                                    self.navigationController?.pushViewController(selectMembersViewController, animated: true)
                                                    
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    func editUserProfileForCategories(){
        var categoriesId = arrOfCategoriesId
        categoriesId = categoriesId.filter { $0 != "" }
        let strCategories = categoriesId.joined(separator: ",")
     
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let first_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        let last_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        let oAuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let mobileNum = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
        let imageBigthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
        let imageSmallthumbUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
        let imageUrl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.imageUrl)
        let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
        
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)

        let params = EditUserDetailsRequest.convertToDictionary(userDetails: EditUserDetailsRequest.init(firstName: first_name, lastName: last_name, mobile: mobileNum, accountType: account_Type ?? Int(), password: userId, country: StringConstants.country, state: state, city: city, profile_status: StringConstants.EMPTY, image_url: imageUrl, big_thumb_url: imageBigthumbUrl, small_thumb_url: imageSmallthumbUrl, categories: strCategories, email: StringConstants.EMPTY, about: StringConstants.EMPTY, imageBlob: StringConstants.EMPTY, oAuthToken: oAuthToken,source: RequestName.loginSourceName))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.editUserProfile + userId
        _ = EditUserProfileServices().editUserProfileServices(apiURL,
                                        postData: params as [String : AnyObject],
                                        withSuccessHandler: { (userModel) in
                                            let model = userModel as! EditUserProfileResponse
                                            let userData = model.user_details[0]
                                            let userProfile = userData.userProfile[0]
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:strCategories, keyString: StringConstants.NSUserDefauluterKeys.categories)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: strCategories, keyString: StringConstants.NSUserDefauluterKeys.interests)

                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            if (userProfile.state?.isEmpty)!{
                                                let selectLocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                                                self.navigationController?.pushViewController(selectLocationViewController, animated: true)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                                                
                                                if account_Type == 2 {
                                                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                    let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                                                    self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
                                                }else{
                                                    //                                                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                                                    //                                                    let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as UIViewController
                                                    //                                                    self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
                                                   /* let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                                                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                                                    self.navigationController?.pushViewController(dashboardViewController, animated: true)*/
                                                    
                                                    self.sighIntoGreenCloud()
                                                }
                                            }
                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    
    
    func callEditUserProfile(){
        
        //let strCategories = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.interests)
        
        var categoriesId = arrOfCategoriesId
        categoriesId = categoriesId.filter { $0 != "" }
        let strCategories = categoriesId.joined(separator: ",")
        
        
        let about =  UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.about)
        
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        
        let first_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userFirstName)
        let last_name = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userLastName)
        let oAuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let mobileNum = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
       let state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
        
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)
        
        let userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let middlewareToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)
        
        let params = EditUserDetailsRequest.convertToDictionary(userDetails: EditUserDetailsRequest.init(firstName: first_name, lastName: last_name, mobile: mobileNum, accountType: account_Type ?? Int(), password: userId, country: StringConstants.country, state: state, city: city, profile_status: about!, image_url: StringConstants.EMPTY, big_thumb_url: StringConstants.EMPTY, small_thumb_url: StringConstants.EMPTY, categories: strCategories, email: StringConstants.EMPTY, about: about!, imageBlob: imageBlob, oAuthToken: oAuthToken,source:RequestName.loginSourceName))//base64String
        
        let apiURL = URLConstant.BASE_URL + URLConstant.editUserProfile + userId
        print("apiURL",apiURL)
        _ = EditUserProfileServices().editUserProfileServices(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! EditUserProfileResponse
                                                                let userData = model.user_details[0]
                                                                let userProfile = userData.userProfile[0]
                                                                self.interests = userProfile.interests!
                                                                
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                                                
                                                                if let pin = userProfile.pin {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }
                                                                
                                                                
                                                                let strInterests =  self.interests.joined(separator: ",")
                                                                if  self.interests.count > 0 {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }
                                                                
                                                                if let imageUrl = userProfile.imageUrl{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                    
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                }
                                                                
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                                                                
                                                                MBProgressHUD.hide(for: self.view, animated: true);

                                                               self.openPopUpConditionally()
                                                                
                                                                /*var ejabberdUserId = userId
                                                                 ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + RequestName.resourceName
                                                                 SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                                 self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)*/
                                                                // self.sighIntoGreenCloud()
                                                                
                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        var AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            if model.inviteSummary.count > 0{
                                                                
                                                                let inviteSummary = model.inviteSummary[0]
                                                                let standard =  inviteSummary.unlimited_standard
                                                                let extra = inviteSummary.unlimited_extra
                                                                let sent = inviteSummary.unlimited_sent
                                                                let medialink =  inviteSummary.media_link
                                                                let mediatype = inviteSummary.media_type
                                                                let skip_media = inviteSummary.skip_media
                                                                let channelName = inviteSummary.channel_name
                                                                let channelColour = inviteSummary.channel_max_color
                                                                let mediathumb = inviteSummary.media_thumbnail
                                                                let imagechannelProfile = inviteSummary.channel_big_thumb
                                                                
                                                                UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                                
                                                                UserDefaults.standard.set(extra, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.extra)
                                                                
                                                                UserDefaults.standard.set(sent, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.sent)
                                                                
                                                                
                                                                UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                                
                                                                
                                                                UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                                
                                                                UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                                
                                                                
                                                                UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                                
                                                                UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                                
                                                                UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                                
                                                                UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                                
                                                            }
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                            
                                                            let greenPassArray = model.greenpasses
                                                            
                                                            let myGreenPassCount = greenPassArray.count
                                                            
                                                            UserDefaults.standard.set(myGreenPassCount, forKey:
                                                                StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                                                            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                                            
                                                            self.openPopUpConditionally()
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
             },{action2 in
             }, nil])*/
            //signInTo Ejjaberd
        })
    }
    
    
    
    
    //Open Popup Conditionally
    func openPopUpConditionally(){
        
        appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)

        
       /* let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
        
        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
            var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
            if(isMitra && show_issue_pass == 0){
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }else{

            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    appDelegate.isLaunchFirstTime = false
                    let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }else if(remaining>0){
                appDelegate.isLaunchFirstTime = false
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
            }else{
                if(myGreenPassCount>0){
                    appDelegate.isLaunchFirstTime = false
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                    self.navigationController?.pushViewController(dashboardViewController, animated: true)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }
            }
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
            self.navigationController?.pushViewController(dashboardViewController, animated: true)
        }*/
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension UserAreaInterestViewController : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return   self.categories_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserAreaInterestTblCell", for: indexPath) as! UserAreaInterestTblCell
        cell.selectionStyle = .none
        let category = self.categories_list[(indexPath as NSIndexPath).row]
        cell.imageViewOfInterest.sd_setImage(with: URL(string: category.image_url!), placeholderImage: UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
        cell.lblofDetailsTxt.text = category.name
        cell.viewOnContentView.layer.cornerRadius =  5.0
        cell.viewOnContentView.layer.masksToBounds = true
        cell.btnCheckBox.isHidden = true
        let checkBoxStatus = arrOfCheckBoxTemp[indexPath.row] as String
        if checkBoxStatus == "0"  {
            cell.imageViewOfCheckBox.image = UIImage(named:StringConstants.ImageNames.unchecked)
        } else {
            cell.imageViewOfCheckBox.image = UIImage(named:StringConstants.ImageNames.checked)
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let buttonRow = indexPath.row
        let checkBoxStatus = arrOfCheckBoxTemp[buttonRow] as String
        let category = self.categories_list[(indexPath as NSIndexPath).row]
        
        if checkBoxStatus == "0" {
            arrOfCheckBoxTemp.remove(at: buttonRow)
            arrOfCheckBoxTemp.insert("1", at: buttonRow)
            arrOfCategoriesId.remove(at: buttonRow)
            arrOfCategoriesId.insert((category.id)!, at: buttonRow)
            
            // Added this category for show updated category on edit user profile screen
            if self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditUserProfileVC || self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC{
                self.selectedCategories_list.append(category)
            }
        }else if checkBoxStatus == "1"{
            arrOfCheckBoxTemp.remove(at: buttonRow)
            arrOfCheckBoxTemp.insert("0", at: buttonRow)
            arrOfCategoriesId.remove(at: buttonRow)
            arrOfCategoriesId.insert("" as String, at: buttonRow)
            
            // Added this category for show updated category on edit user profile screen
            if self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditUserProfileVC || self.commingFromScreen == StringConstants.NSUserDefauluterValues.EditChannelProfileVC{
                let id = category.id
                let index =   self.selectedCategories_list.index { (category) -> Bool in
                    category.id == id
                }
                self.selectedCategories_list.remove(at: index!)
            }
        }
        
        print("commingFromScreen",commingFromScreen)
        if commingFromScreen != StringConstants.NSUserDefauluterValues.CreateCommunityStep2ViewController && commingFromScreen != StringConstants.NSUserDefauluterValues.EditChannelProfileVC{
            
let arrOfTempCount = arrOfCategoriesId.filter { $0 != "" }
            if arrOfTempCount.count != 0 {
                lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.myInterests, comment: StringConstants.EMPTY)) + " " + String(arrOfTempCount.count)
            }else{
                lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.myInterests, comment: StringConstants.EMPTY))
            }
        }
        let indexPath = IndexPath(item: buttonRow, section: 0)
        tblOfUserInterest.reloadRows(at: [indexPath], with: .none)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 85
    }
}




