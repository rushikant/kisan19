//
//  UserProfileViewController.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 20/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import SVProgressHUD
import XMPPFramework
import AccountKit
import  Firebase
import FirebaseMessaging
import MBProgressHUD

class UserProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    @IBOutlet weak var viewOfBackNameText: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewOfBackLastName: UIView!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var viewOfBackPinCode: UIView!
    @IBOutlet weak var txtPinCode: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblOfNameCantBlank: UILabel!
    @IBOutlet weak var lblOfSurNameCantBlank: UILabel!
    @IBOutlet weak var lblOfPinCodeCantBlank: UILabel!
    @IBOutlet var lblPhoto: UILabel!
    var basicBool : Bool = false
    @IBOutlet weak var imageViewOfUserProfile: UIImageView!
    @IBOutlet weak var viewOfBackProfilePic: UIView!
    var profileImage: UIImage!
    var nameTxtLength: Int! = 0
    var lastNameTxtLength: Int! = 0
    var pinCodeTxtLength: Int! = 0
    var base64String: String = ""
    //var registrationToken = String()
    var lblOfseprator = UILabel()
    var lblOfTitle = UILabel()
    var registration_token = String()
    var accountKit: AKFAccountKit!
    var appDelegate = AppDelegate()
    let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.'"
    var firstLoad = true
    private let picker = UIImagePickerController()
    var userFirstName = ""
    var userLastName = ""
    var pin = ""
    public var interests = [String]()
    var state = String()
    var AuthToken = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        // initialize Account Kit
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        if accountKit == nil {
            // may also specify AKFResponseTypeAccessToken
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
        }
        
        // Do any additional setup after loading the view.
        
        initialDesign()
    }
    
    func initialDesign() {
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: (NSLocalizedString(StringConstants.NavigationTitle.next, comment: StringConstants.EMPTY)), style: .plain, target: self, action: #selector(nextTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        viewOfBackNameText.layer.cornerRadius = 5.0
        viewOfBackLastName.layer.cornerRadius = 5.0
        viewOfBackPinCode.layer.cornerRadius = 5.0
        
        scrollView.isScrollEnabled = false
        
        imageViewOfUserProfile.layer.cornerRadius = imageViewOfUserProfile.frame.size.width/2
        imageViewOfUserProfile.clipsToBounds = true
        
        viewOfBackProfilePic.layer.cornerRadius = viewOfBackProfilePic.frame.size.width/2
        viewOfBackProfilePic.clipsToBounds = true
        
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.text = (NSLocalizedString(StringConstants.NavigationTitle.yourProfile, comment: StringConstants.EMPTY))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        
        txtName.placeholder = (NSLocalizedString(StringConstants.showUserProfile.name, comment: StringConstants.EMPTY))
        txtLastName.placeholder = (NSLocalizedString(StringConstants.showUserProfile.surname, comment: StringConstants.EMPTY))
        txtPinCode.placeholder = (NSLocalizedString(StringConstants.showUserProfile.pincode, comment: StringConstants.EMPTY))
        lblPhoto.text = (NSLocalizedString(StringConstants.showUserProfile.Photo, comment: StringConstants.EMPTY))

        
        
        //Done button for hide numeric key pad of pin code
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title:(NSLocalizedString(StringConstants.NavigationTitle.done, comment: StringConstants.EMPTY)) , style: .done, target: self, action: #selector(doneButtonAction))
        doneBtn.tintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.txtPinCode.inputAccessoryView = toolbar
        
       // Not allow emoji
//        txtName.keyboardType = UIKeyboardTypeASCIICapable
//        txtLastName.keyboardType = UIKeyboardTypeASCIICapable
        
        if let loginResponseCode = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.loginResponseCode){
            if loginResponseCode == String(ResponseCode.loginResponseCode) {
               
                if let userFirstNameValue = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName){
                    userFirstName = userFirstNameValue
                }
                if let userLastNameValue = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userLastName){
                    userLastName = userLastNameValue
                }
                if let pinValue = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.pin){
                    pin = pinValue
                }
                
                if !(userFirstName.isEmpty) && !(userLastName.isEmpty){
                    txtName.text = userFirstName
                    txtLastName.text = userLastName
                    txtPinCode.text = pin
                }
            }
        }
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        //self.navigationController?.popViewController(animated: true)
        exit(0)
    }
    
    @objc func nextTapped(sender: UIBarButtonItem) {
        
        self.txtName.resignFirstResponder()
        self.txtLastName.resignFirstResponder()
        self.txtPinCode.resignFirstResponder()
        
        
        if imageViewOfUserProfile.image == UIImage(named: "photoPlaceHolder"){
            self.view.makeToast((NSLocalizedString(StringConstants.Validation.profile_Image_mandatory, comment: StringConstants.EMPTY)))
        }else if (txtName.text?.isEmpty)! {
            
            lblOfNameCantBlank.isHidden = false
            lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))
            txtName.becomeFirstResponder()
            
        }else if (!(txtName.text?.isEmpty)!) {
            
            let str = txtName.text
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = str!.rangeOfCharacter(from: decimalCharacters)
            
            if nameTxtLength == 1 {
                lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                lblOfNameCantBlank.isHidden = false
                txtName.becomeFirstResponder()
            }else if decimalRange != nil  {
                lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.notnumber, comment: StringConstants.EMPTY))
                self.lblOfNameCantBlank.isHidden = false
                txtName.becomeFirstResponder()
            }else if (txtLastName.text?.isEmpty)! {
                lblOfSurNameCantBlank.isHidden = false
                lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                txtLastName.becomeFirstResponder()
            } else if (!(txtLastName.text?.isEmpty)!) {
                let str = txtLastName.text
                let decimalCharacters2 = CharacterSet.decimalDigits
                let decimalRange2 = str!.rangeOfCharacter(from: decimalCharacters2)
                if lastNameTxtLength == 1 {
                    lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    lblOfSurNameCantBlank.isHidden = false
                    txtLastName.becomeFirstResponder()
                }else if decimalRange2 != nil  {
                    lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.notnumber, comment: StringConstants.EMPTY))
                    self.lblOfSurNameCantBlank.isHidden = false
                    txtLastName.becomeFirstResponder()
                }
                else if (txtPinCode.text?.isEmpty)! {
                    lblOfPinCodeCantBlank.isHidden = false
                    lblOfPinCodeCantBlank.text = (NSLocalizedString(StringConstants.Validation.pinCode_cannotBlank, comment: StringConstants.EMPTY))
                    txtPinCode.becomeFirstResponder()
                }else if (txtPinCode.text?.characters.count)! < 6 { //txtPinCode.characterRange.count < 6 {
                    lblOfPinCodeCantBlank.isHidden = false
                    lblOfPinCodeCantBlank.text = (NSLocalizedString(StringConstants.Validation.sixDigit_PinCode, comment: StringConstants.EMPTY))
                    txtPinCode.becomeFirstResponder()
                }else{
                    lblOfNameCantBlank.isHidden = true
                    lblOfSurNameCantBlank.isHidden = true
                    lblOfPinCodeCantBlank.isHidden = true

                    if let loginResponseCodeValue = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.loginResponseCode){
                        if loginResponseCodeValue == String(ResponseCode.loginResponseCode) {
                            if !(userFirstName.isEmpty) && !(userLastName.isEmpty){
                                self.callEditUserProfile()
                            }else{
                                self.callSignUpUser()
                            }                            
                        }else{
                            self.callSignUpUser()
                        }
                    }else{
                        self.callSignUpUser()
                    }
                }
            }
            
        }
        
    }
    
    
    func callSignUpUser(){
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let fcmToken = Messaging.messaging().fcmToken //UserDefaults.standard.value(forKey: StringConstants.NSUserDefauluterKeys.FCMToken) as! String //Messaging.messaging().fcmToken
        //let strUUID = UUIDHelper.getUUID()
        
        txtName.text =  txtName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtLastName.text =  txtLastName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        let resource = RequestName.resourceName //+ strUUID
        let params = SignUpRequest.convertToDictionary(signUpDetails: SignUpRequest.init(firstName: txtName.text!, lastName: txtLastName.text!, pinCode: txtPinCode.text!, loginType: URLConstant.LoginType, registrationId: registration_token, email: "", resource: resource, fcmDeviceId: fcmToken!, imageBlob: base64String,app:StringConstants.appLoginParameter,source:RequestName.loginSourceName)) 

//        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.SignUp
        print("apiURL",apiURL)

        _ = SignUpServices().signUpUser(apiURL,
                                        postData: params as [String : AnyObject],
                                        withSuccessHandler: { (signUpRespo) in
                                            let model = signUpRespo as! SignUpResponse
                                            print("model",model)
                                            
                                            let userData = model.user_details[0]
                                            print("user_details",userData.firstName ?? String())
                                            let userProfile = userData.userProfile[0]
                                            print("user_profile",userProfile.country ?? String())
                                            var interests = [String]()
                                            interests = userProfile.interests!
                                            self.interests = interests
                                            print("interests",interests )
                                            let middlewareToken = model.middlewareToken
                                            print("middlewareToken",middlewareToken ?? String())
                                            let oAuthToken = model.oAuthToken
                                            
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:middlewareToken! , keyString: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: oAuthToken!, keyString: StringConstants.NSUserDefauluterKeys.oAuthToken)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.username! , keyString: StringConstants.NSUserDefauluterKeys.username)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString: userData.userId!, keyString: StringConstants.NSUserDefauluterKeys.userId)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                            if let pin = userProfile.pin {
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                            }
                                            
                                            let accountType = String(userProfile.accountType!)
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:accountType , keyString: StringConstants.NSUserDefauluterKeys.accountType)
                                            if let country = userProfile.country{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:country , keyString: StringConstants.NSUserDefauluterKeys.country)
                                            }
                                            //Show overlay on discover screen
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.TRUE , keyString: StringConstants.NSUserDefauluterKeys.showOverlayOnDiscover)
                                            
                                            let strInterests = interests.joined(separator: ",")
                                            if interests.count > 0 {
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                            }
                                            
                                            if let imageUrl = userProfile.imageUrl{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                            }
                                            
                                            var mobileNum = String()
                                            if let state = userProfile.state{
                                                self.state = state
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: state, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.state)
                                            }
                                            
                                            if let city = userProfile.city{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: city, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }else{
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.city)
                                            }
                                            
                                            if let mobile1 = userProfile.mobile1{
                                                mobileNum = mobile1
                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                            }else{
                                                if let mobile2 = userProfile.mobile2{
                                                    mobileNum = mobile2
                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:mobileNum , keyString: StringConstants.NSUserDefauluterKeys.mobileNum)
                                                }
                                            }
                                            
                                            /*var ejabberdUserId = userData.userId
                                            ejabberdUserId = ejabberdUserId! + "@" + URLConstant.hostName + "/" + resource
                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId! , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                            self.didTouchLogIn(userJID: ejabberdUserId!, userPassword: middlewareToken!, server: URLConstant.hostName)*/
                                            
                                            self.sighIntoGreenCloud()

                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    

    func callEditUserProfile(){
        
        //SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.UserAreaInterestViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
        
        
        SessionManagerForSaveData.saveDataInSessionManager(valueString:txtName.text! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
        SessionManagerForSaveData.saveDataInSessionManager(valueString:txtLastName.text! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
        SessionManagerForSaveData.saveDataInSessionManager(valueString:txtPinCode.text! , keyString: StringConstants.NSUserDefauluterKeys.pin)
        let userAreaInterestViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
        userAreaInterestViewController.imageBlob = base64String
        self.navigationController?.pushViewController(userAreaInterestViewController, animated: true)
        
    
       /* let strCategories = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.interests)
        let about =  UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.about)
        
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let oAuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        let mobileNum = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mobileNum)
        state = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.state)
        let city = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.city)
        
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)
        
        let userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        let middlewareToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)
        
        let params = EditUserDetailsRequest.convertToDictionary(userDetails: EditUserDetailsRequest.init(firstName: userFirstName, lastName: userLastName, mobile: mobileNum, accountType: account_Type ?? Int(), password: userId, country: StringConstants.country, state: state, city: city, profile_status: about!, image_url: StringConstants.EMPTY, big_thumb_url: StringConstants.EMPTY, small_thumb_url: StringConstants.EMPTY, categories: strCategories!, email: StringConstants.EMPTY, about: about!, imageBlob: base64String, oAuthToken: oAuthToken,source:RequestName.loginSourceName))//base64String
        
        let apiURL = URLConstant.BASE_URL + URLConstant.editUserProfile + userId
        print("apiURL",apiURL)
        _ = EditUserProfileServices().editUserProfileServices(apiURL,
                                                              postData: params as [String : AnyObject],
                                                              withSuccessHandler: { (userModel) in
                                                                let model = userModel as! EditUserProfileResponse
                                                                let userData = model.user_details[0]
                                                                let userProfile = userData.userProfile[0]
                                                                self.interests = userProfile.interests!
                                                                
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.firstName! , keyString: StringConstants.NSUserDefauluterKeys.userFirstName)
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userData.lastName! , keyString: StringConstants.NSUserDefauluterKeys.userLastName)
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.about! , keyString: StringConstants.NSUserDefauluterKeys.about)
                                                                
                                                                if let pin = userProfile.pin {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:pin , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY , keyString: StringConstants.NSUserDefauluterKeys.pin)
                                                                }
                                                                
                                                                
                                                                let strInterests =  self.interests.joined(separator: ",")
                                                                if  self.interests.count > 0 {
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: strInterests, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString: StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.interests)
                                                                }
                                                                
                                                                if let imageUrl = userProfile.imageUrl{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageBigthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:userProfile.imageSmallthumbUrl!, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:imageUrl, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                    
                                                                }else{
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageSmallthumbUrl)
                                                                    SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.EMPTY, keyString: StringConstants.NSUserDefauluterKeys.imageUrl)
                                                                }
                                                                
                                                                self.txtName.resignFirstResponder()
                                                                self.txtPinCode.resignFirstResponder()
                                                                self.txtLastName.resignFirstResponder()

                                                                /*var ejabberdUserId = userId
                                                                ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + RequestName.resourceName
                                                                SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                                self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)*/
                                                               // self.sighIntoGreenCloud()

                                                                
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })*/
        
        
        
    }
    
    
    func isValidInput(Input:String) -> Bool {
        let RegEx = "\\w{7,18}"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }

    func didTouchLogIn( userJID: String, userPassword: String, server: String) {
        do {
            try appDelegate.xmppController = XMPPController(hostName: server,
                                                            userJIDString: userJID,
                                                            password: userPassword)
            appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
            appDelegate.xmppController.connect()
            
        } catch {
            print("Something went wrong")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSelectPicClick(_ sender: Any) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.photoLibrary, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewOfUserProfile.contentMode = .scaleAspectFit
            imageViewOfUserProfile.image = pickedImage
            viewOfBackProfilePic.isHidden = true
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                self.openEditor(nil)
            }
            
        }
    }
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
        // Use view controller
        
      /*  let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)*/
        
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)

  
    }
    
    // MARK: - CropView new
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        // imageViewOfProfilePic.image = image
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        imageViewOfUserProfile.image = image
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfUserProfile.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
   /* func circleCropDidCancel() {
        dismiss(animated: false, completion: nil)
    }
    
    func circleCropDidCropImage(_ image: UIImage) {
        imageViewOfUserProfile.image = image
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfUserProfile.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        dismiss(animated: false, completion: nil)
    }*/
    
}

extension UserProfileViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            
            txtLastName .becomeFirstResponder()
            
        }else if textField == txtLastName{
            
            txtPinCode .becomeFirstResponder()
            
        }else if textField == txtPinCode{
            
            txtPinCode .resignFirstResponder()
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            
            txtName.returnKeyType = UIReturnKeyType.next
            scrollView.isScrollEnabled = true
            
        }else if textField == txtLastName{
            
            txtLastName.returnKeyType = UIReturnKeyType.next
            scrollView.isScrollEnabled = true
            
        }else if textField == txtPinCode{
            
            txtPinCode.returnKeyType = UIReturnKeyType.done
            scrollView.isScrollEnabled = true
        }
        return true;
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == txtName {
            var basicBool : Bool = false
            nameTxtLength = (txtName.text! as NSString).length + (string as NSString).length - range.length as Int
            if  (!(txtName.text?.isEmpty)! || !(string.isEmpty))  {
                    if nameTxtLength == 1 {
                        lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                        lblOfNameCantBlank.isHidden = false
                        
                    } else if nameTxtLength == 0 {
                        lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))
                        lblOfNameCantBlank.isHidden = false
                        
                    }else{
                        lblOfNameCantBlank.isHidden = true
                    }
                //Not allowed type emoji
                if #available(iOS 7, *){
                    if textField.isFirstResponder {
                        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                            // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                            return false
                        }
                    }
                }
                //Not all special character
//                let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                return (string == filtered)
               
                let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
                //Not allowed digits
                if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                    return false
                } else {
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                   // return true
                }
                
            }else{
                
                if basicBool == false {
                    basicBool = true
                    lblOfNameCantBlank.isHidden = false
                    // Not allow space in beginning of textfield
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (txtName.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    let trimmedString = string.trimmingCharacters(in: .whitespaces)
                    if (trimmedString.isEmpty || trimmedString == "" ) {
                        if nameTxtLength == 1{
                            nameTxtLength = nameTxtLength - 1
                        }
                    }else if ACCEPTABLE_CHARACTERS.range(of: newString as String) != nil{
                        print("Got the string")
                    }else{
                        if nameTxtLength == 1{
                            nameTxtLength = nameTxtLength - 1
                        }
                    }
                    
                    if nameTxtLength == 1 {
                        lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    } else if nameTxtLength == 0 {
                        lblOfNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.nameText_cannotBlank, comment: StringConstants.EMPTY))
                    }
                    
                    //Not allowed type emoji
                    if #available(iOS 7, *){
                        if textField.isFirstResponder {
                            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                                // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                                return false
                            }
                        }
                    }
                    
                    
                    //Not all special character & white space
//                    let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                    let filtered: String = (newString.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                    let whiteSpaceValue = newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
//                    if (string == filtered && whiteSpaceValue == true){
//                        return true
//                    }else{
//                        return false
//                    }
                    
                    let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                    if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                        print(range)
                        return false
                    }
                    //Not allowed digits
                    if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                        return false
                    } else {
                        guard range.location == 0 else {
                            return true
                        }
                        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                        //return true
                    }
                }
            }
        }else if textField == txtLastName{
            
            var basicBool : Bool = false
            lastNameTxtLength  = (txtLastName.text! as NSString).length + (string as NSString).length - range.length as Int
            if  (!(txtLastName.text?.isEmpty)! || !(string.isEmpty)) {
                if lastNameTxtLength == 1 {
                    lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    lblOfSurNameCantBlank.isHidden = false
                    
                } else if lastNameTxtLength == 0 {
                    lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                    lblOfSurNameCantBlank.isHidden = false
                    
                }else{
                    lblOfSurNameCantBlank.isHidden = true
                }
                
                //Not allowed type emoji
                if #available(iOS 7, *){
                    if textField.isFirstResponder {
                        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                            // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                            return false
                        }
                    }
                }
                
                //Not all special character
//                let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                return (string == filtered)
                
                let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                    print(range)
                    return false
                }
                //Not allowed digits
                if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                    return false
                } else {
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                    //return true
                }
                
            }else{
                
                if basicBool == false {
                    basicBool = true
                    lblOfSurNameCantBlank.isHidden = false
                    
                    // Not allow space in beginning of textfield
                    guard range.location == 0 else {
                        return true
                    }
                    let newString = (txtLastName.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                    let trimmedString = string.trimmingCharacters(in: .whitespaces)
                    if (trimmedString.isEmpty || trimmedString == "" ) {
                        if lastNameTxtLength == 1{
                            lastNameTxtLength = lastNameTxtLength - 1
                        }
                    }else if ACCEPTABLE_CHARACTERS.range(of: newString as String) != nil{
                        print("Got the string")
                    }else{
                        if lastNameTxtLength == 1{
                            lastNameTxtLength = lastNameTxtLength - 1
                        }
                    }
                    
                    if lastNameTxtLength == 1 {
                        lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameRequired_minTwoChar, comment: StringConstants.EMPTY))
                    } else if nameTxtLength == 0 {
                        lblOfSurNameCantBlank.text = (NSLocalizedString(StringConstants.Validation.surNameText_cannotBlank, comment: StringConstants.EMPTY))
                    }
                    
                    //Not allowed type emoji
                    if #available(iOS 7, *){
                        if textField.isFirstResponder {
                            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                                // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                                return false
                            }
                        }
                    }
                    
                    //Not all special character & white space
//                    let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//                    let filtered: String = (newString.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                    let whiteSpaceValue = newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
//                    if (string == filtered && whiteSpaceValue == true){
//                        return true
//                    }else{
//                        return false
//                    }
                    
                    
                    let validString = NSCharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
                    if let range = string.rangeOfCharacter(from: validString as CharacterSet){
                        print(range)
                        return false
                    }
                    //Not allowed digits
                    if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                        return false
                    } else {
                        guard range.location == 0 else {
                            return true
                        }
                        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
                        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
                        //return true
                    }
                }
            }
            
            
        }else if textField == txtPinCode{
            
            var basicBool : Bool = false
            pinCodeTxtLength = (txtPinCode.text! as NSString).length + (string as NSString).length - range.length as Int
            if  (!(txtPinCode.text?.isEmpty)! || !(string.isEmpty)) {
                
                if pinCodeTxtLength == 1 {
                    
                } else if pinCodeTxtLength == 0 {
                    lblOfPinCodeCantBlank.text = (NSLocalizedString(StringConstants.Validation.pinCode_cannotBlank, comment: StringConstants.EMPTY))
                    lblOfPinCodeCantBlank.isHidden = false
                    
                }else{
                    lblOfPinCodeCantBlank.isHidden = true
                }
                
                
            }else{
                
                if basicBool == false {
                    basicBool = true
                    if pinCodeTxtLength == 1 {
                        lblOfPinCodeCantBlank.isHidden = true
                        
                    } else if pinCodeTxtLength == 0 {
                        lblOfPinCodeCantBlank.text = (NSLocalizedString(StringConstants.Validation.pinCode_cannotBlank, comment: StringConstants.EMPTY))
                        lblOfPinCodeCantBlank.isHidden = false
                    }else{
                        lblOfPinCodeCantBlank.isHidden = true
                    }
                }
            }
            
            let currentText = txtPinCode.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 6
        }
        return true
    }
   
}



extension UserProfileViewController: XMPPStreamDelegate {
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        // Move to chat controller
        print("Aunthenticated at viewcontroller")

        if (self.interests.count) > 0 {
            MBProgressHUD.hide(for: self.view, animated: true);
            if (state.isEmpty){
                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let selectLocationVC = storyboard.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                self.navigationController?.pushViewController(selectLocationVC, animated: true)
                
            }else{
                
                SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.loginSuccessfully , keyString: StringConstants.NSUserDefauluterKeys.checkedLogin)
                if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
                    print("retriveState",retriveState)
                }
                
                //                let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                //                let exhibitionAdvertiseVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as UIViewController
                //                self.navigationController?.pushViewController(exhibitionAdvertiseVC, animated: true)
               /* let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                self.navigationController?.pushViewController(dashboardViewController, animated: true)*/
                openPopUpConditionally()
            }
            
        }else{
            MBProgressHUD.hide(for: self.view, animated: true);
            SessionManagerForSaveData.saveDataInSessionManager(valueString:StringConstants.NSUserDefauluterValues.UserAreaInterestViewController, keyString: StringConstants.NSUserDefauluterKeys.commingFromScreen)
            let userAreaInterestViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserAreaInterestViewController") as! UserAreaInterestViewController
            self.navigationController?.pushViewController(userAreaInterestViewController, animated: true)
        }
        
        
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Wrong password or username")
    }
    
    
    //Open Popup Conditionally
    func openPopUpConditionally(){
        
        appDelegate.isLaunchFirstTime = false
        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)

        
       /* let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
        let  myGreenPassCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.myGreenPassCount)
        let isMitra = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isMitra)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
        
        let remaining = (standard + extra) - sent
        
        //PopUpRedirection Logic
        
        if(isMitra && appDelegate.isLaunchFirstTime){
            var show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
            if(isMitra && show_issue_pass == 0){
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }else{

            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    appDelegate.isLaunchFirstTime = false
                    let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }else if(remaining>0){
                appDelegate.isLaunchFirstTime = false
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "InvitePopUpViewController") as! InvitePopUpViewController
                self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
            }else{
                if(myGreenPassCount>0){
                    appDelegate.isLaunchFirstTime = false
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                    self.navigationController?.pushViewController(dashboardViewController, animated: true)
                }else{
                    appDelegate.isLaunchFirstTime = false
                    let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                    let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                    self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                }
            }
          }
        }else if(appDelegate.isLaunchFirstTime){
            
            let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
            // if self.myChatsList.count != 0 {
            if accountType == StringConstants.ONE {
                
                if(pendingInviteCount>0){
                    if(!isInviteViewed){
                        appDelegate.isLaunchFirstTime = false
                        let vc=App.dashStoryBoard.instantiateViewController(withIdentifier: "CongRecievedPassVC")as!CongRecievedPassVC
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else{
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                        appDelegate.isLaunchFirstTime = false
                    }
                }else{
                    if(myGreenPassCount>0){
                        appDelegate.isLaunchFirstTime = false
                        let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                        self.navigationController?.pushViewController(dashboardViewController, animated: true)
                    }else{
                        appDelegate.isLaunchFirstTime = false
                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
                        let invitationPreviewPopupVc = mainStoryboard.instantiateViewController(withIdentifier: "ExhibitionAdvertiseVC") as! ExhibitionAdvertiseVC
                        self.navigationController?.pushViewController(invitationPreviewPopupVc, animated: false)
                    }
                }
            }
        }else{
            appDelegate.isLaunchFirstTime = false
            let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
            self.navigationController?.pushViewController(dashboardViewController, animated: true)
        }*/
    }
    
    
    //API call to Green cloud Sign In
    func sighIntoGreenCloud(){
        
        AuthToken = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.oAuthToken)
        var resource = String()
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType){
            if accountType == StringConstants.ONE {
                resource = RequestName.loginSourceName //+ strUUID
            }else{
                resource = RequestName.loginSourceExhibitorName //+ strUUID
            }
        }else{
            resource = RequestName.loginSourceName //+ strUUID
        }
        let params = GreenCloudLoginRequest.convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest.init(eventCode: URLConstant.eventCode, source: resource, app_key: URLConstant.app_key, token: AuthToken))
        print("params",params)
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.greenCloudLogin
        _ = GreenCloudLoginService().greenCLoudloginUser(apiURL,postData: params as [String : AnyObject],
                                                         withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GreenCloudLoginResponse
                                                            print("model",model)
                                                            
                                                            let pendingInviattionCount = model.pending_invitation_count
                                                            let isMitra = model.isMitra
                                                            
                                                            let ProfileDetails = model.profileDetails[0]
                                                            let sessionId = ProfileDetails.sessionId
                                                            
                                                        
                                                            let greenPassArray = model.greenpasses
                                                            
                                                            let myGreenPassCount = greenPassArray.count
                                                            
                                                            let eventStartDate = model.eventstartdatetime
                                                            
                                                            UserDefaults.standard.set(eventStartDate, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

                                                            UserDefaults.standard.set(myGreenPassCount, forKey:
                                                                StringConstants.NSUserDefauluterKeys.myGreenPassCount)
                                                            
                                                            if model.inviteSummary.count > 0{
                                                                
                                                                let inviteSummary = model.inviteSummary[0]
                                                                
                                                                let standard =  inviteSummary.unlimited_standard
                                                                let extra = inviteSummary.unlimited_extra
                                                                let sent = inviteSummary.unlimited_sent
                                                                let medialink =  inviteSummary.media_link
                                                                let mediatype = inviteSummary.media_type
                                                                let skip_media = inviteSummary.skip_media
                                                                let channelName = inviteSummary.channel_name
                                                                let channelColour = inviteSummary.channel_max_color
                                                                let mediathumb = inviteSummary.media_thumbnail
                                                                let imagechannelProfile = inviteSummary.channel_big_thumb
                                                                
                                                                UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                                
                                                                UserDefaults.standard.set(extra, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.extra)
                                                                
                                                                UserDefaults.standard.set(sent, forKey:
                                                                    StringConstants.NSUserDefauluterKeys.sent)
                                                                
                                                                
                                                                UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                                
                                                                
                                                                UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                                
                                                                UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                                
                                                                
                                                                UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                                
                                                                UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                                
                                                                UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                                
                                                                UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                                
                                                            }
                                                            
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:sessionId!, keyString: StringConstants.NSUserDefauluterKeys.sessionId)
                                                            
                                                            UserDefaults.standard.set(pendingInviattionCount, forKey: StringConstants.NSUserDefauluterKeys.PendingInviteCount)
                                                            
                                                            UserDefaults.standard.set(isMitra, forKey: StringConstants.NSUserDefauluterKeys.isMitra)
                                                            
                                                            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
                                                            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.middlewareToken)
                                                            
                                                            //signInTo Ejjaberd
                                                            var ejabberdUserId = userId
                                                            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
                                                            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
                                                            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
                                                            
                                                            print("ejabberdUserId",ejabberdUserId ?? String())
                                                            print("Password as middlewareToken",middlewareToken ?? String())
                                                            print("hostName",URLConstant.hostName)
                                                            
                                                            
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
             },{action2 in
             }, nil])*/
            //signInTo Ejjaberd
            let userId =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
            let middlewareToken =                                         SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
            
            var ejabberdUserId = userId
            ejabberdUserId = ejabberdUserId + "@" + URLConstant.hostName + "/" + resource
            SessionManagerForSaveData.saveDataInSessionManager(valueString:ejabberdUserId , keyString: StringConstants.NSUserDefauluterKeys.ejabberdServerUsreName)
            self.didTouchLogIn(userJID: ejabberdUserId, userPassword: middlewareToken, server: URLConstant.hostName)
            
            print("ejabberdUserId",ejabberdUserId ?? String())
            print("Password as middlewareToken",middlewareToken ?? String())
            print("hostName",URLConstant.hostName)
        })
    }
    
}

extension UserProfileViewController: UIImageCropperProtocol {
    
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imageViewOfUserProfile.image = croppedImage
        let imageData:NSData = UIImageJPEGRepresentation(imageViewOfUserProfile.image!, 0.7)! as NSData
        base64String = imageData.base64EncodedString()
        viewOfBackProfilePic.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
