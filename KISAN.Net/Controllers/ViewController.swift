//
//  ViewController.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 10/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import Crashlytics
class ViewController: UIViewController {
    @IBOutlet weak var tblLanguage: UITableView!
    var arrOfLanguages = [String]()
    @IBOutlet weak var imageViewOfBaner: UIImageView!
    @IBOutlet weak var leadingConstOfBanerImg: NSLayoutConstraint!
    @IBOutlet weak var trailingConstantOfBanerImg: NSLayoutConstraint!
    @IBOutlet weak var topConstantOfBanerImg: NSLayoutConstraint!
    @IBOutlet weak var lblText: UILabel!
    var strCommingFrom = String()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        
        lblText.text = (NSLocalizedString(StringConstants.EnglishConstant.Chooselanguage, comment: StringConstants.EMPTY))
        if let retriveState = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.checkedLogin) {
            if strCommingFrom == StringConstants.flags.leftDrawerLanguage{
                
            }else if  strCommingFrom == StringConstants.flags.dashboardViewController{
                
            }else if retriveState == StringConstants.NSUserDefauluterValues.loginSuccessfully{
               /* let comingFrom = UserDefaults.standard.string(forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                if comingFrom == StringConstants.pushNotificationConstants.appUpdate{
                    let urlstring = UserDefaults.standard.string(forKey: StringConstants.pushNotificationConstants.appUpdateLink)
                    UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.pushNotificationConstants.customNotificationForAllUser)
                    if let url = URL(string: urlstring!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }else{*/
                    let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let DashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
                    self.navigationController?.pushViewController(DashboardVC, animated: false)
                //}
            }
        }
        
    }
    
    func initialDesign() {
        self.view.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        tblLanguage.tableFooterView = UIView()
        tblLanguage.cellLayoutMarginsFollowReadableWidth = false
        tblLanguage.isScrollEnabled = false
        tblLanguage.layer.cornerRadius = 4
        tblLanguage.layer.masksToBounds = true
        arrOfLanguages.append(StringConstants.EnglishConstant.languageName)
        arrOfLanguages.append(StringConstants.MarathiConstant.languageName)
        arrOfLanguages.append(StringConstants.HindiConstant.languageName)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if UIScreen.main.bounds.size.width <= 320{
            leadingConstOfBanerImg.constant = 35
            trailingConstantOfBanerImg.constant = 35
            topConstantOfBanerImg.constant = 40
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController : UITableViewDataSource, UITableViewDelegate{
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfLanguages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectLanguageTblCell", for: indexPath) as! SelectLanguageTblCell
        cell.selectionStyle = .none
        cell.lblOfLanguages.text = arrOfLanguages[indexPath.row]
        cell.lblOfLanguages.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        cell.lblOfLanguages.textAlignment = .center
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if strCommingFrom == StringConstants.flags.leftDrawerLanguage{
            strCommingFrom = ""
            if indexPath.row == 0 {
                L102Language.setAppleLAnguageTo(lang:"en")
            }else if indexPath.row == 1 {
                L102Language.setAppleLAnguageTo(lang:"mr") //mr
            }else{
                L102Language.setAppleLAnguageTo(lang:"hi") //hi
            }
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(dashboardVC, animated: true)
           
        }else if strCommingFrom == StringConstants.flags.dashboardViewController{
            strCommingFrom = ""
            if indexPath.row == 0 {
                L102Language.setAppleLAnguageTo(lang:"en")
            }else if indexPath.row == 1 {
                L102Language.setAppleLAnguageTo(lang:"mr") //mr
            }else{
                L102Language.setAppleLAnguageTo(lang:"hi") //hi
            }
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(dashboardVC, animated: true)
            
        }else{
            if indexPath.row == 0 {
                L102Language.setAppleLAnguageTo(lang:"en")
            }else if indexPath.row == 1 {
                L102Language.setAppleLAnguageTo(lang:"mr") //mr
            }else{
                L102Language.setAppleLAnguageTo(lang:"hi") //hi
            }
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
}


extension ViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}




