//
//  CommunityDetailService.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 07/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CommunityDetailService{
    static let sharedInstance = CommunityDetailService()
    var tasks = [CommunityDetailTable]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let contextTemp = (UIApplication.shared.delegate as! AppDelegate).persistentContainerTemp.viewContext

    
    func sendMessageAtCommunityDetailTable(communityDetail:Dictionary<String, Any>){
        
        let communityDetailCommunity = communityDetail
        let communityDetailTable = CommunityDetailTable(context:context)
        communityDetailTable.communityCategories = communityDetailCommunity["communityCategories"] as? String
        communityDetailTable.communityCreatedTimestamp = String(describing: communityDetailCommunity["communityCreatedTimestamp"])
        communityDetailTable.communityDesc = communityDetailCommunity["communityDesc"] as? String
        communityDetailTable.communityDominantColour = communityDetailCommunity["communityDominantColour"] as? String
        communityDetailTable.communityImageBigThumbUrl = communityDetailCommunity["communityImageBigThumbUrl"] as? String
        communityDetailTable.communityImageSmallThumbUrl = communityDetailCommunity["communityImageSmallThumbUrl"] as? String
        communityDetailTable.communityImageUrl = communityDetailCommunity["communityImageUrl"] as? String
        communityDetailTable.communityJoinedTimestamp = String(describing:communityDetailCommunity["communityJoinedTimestamp"])
        communityDetailTable.communityKey = communityDetailCommunity["communityKey"] as? String
        communityDetailTable.communityName = communityDetailCommunity["communityName"] as? String
       communityDetailTable.communityScore = String(describing:communityDetailCommunity["communityScore"])
        //communityDetailTable.communityScore = (communityDetailCommunity["communityScore"] as! NSNumber)
        communityDetailTable.communityStatus = communityDetailCommunity["communityStatus"] as? String
        communityDetailTable.communitySubType = communityDetailCommunity["communitySubType"] as? String
        communityDetailTable.communityUpdatedTimestamp = String(describing:communityDetailCommunity["communityUpdatedTimestamp"])
        communityDetailTable.finalScore = communityDetailCommunity["finalScore"] as? String
        communityDetailTable.isdeleted = communityDetailCommunity["isDeleted"] as? String
        communityDetailTable.isMember = String(describing: communityDetailCommunity["isMember"]!)
        communityDetailTable.ownerId = communityDetailCommunity["ownerId"] as? String
        communityDetailTable.ownerImageUrl = communityDetailCommunity["ownerImageUrl"] as? String
        communityDetailTable.ownerName = communityDetailCommunity["ownerName"] as? String
        communityDetailTable.privacy = communityDetailCommunity["privacy"] as? String
        if let pavilion_name = communityDetailCommunity["pavilion_name"]{
            communityDetailTable.pavilion_name = String(describing: pavilion_name)
        }else{
            communityDetailTable.pavilion_name = String(describing: "")
        }
        if let totalMembers = communityDetailCommunity["totalMembers"]{
            communityDetailTable.totalMembers = String(describing: totalMembers)
        }else{
            communityDetailTable.totalMembers = String(describing: "0")
        }
        if let communityWithRssFeed = communityDetailCommunity["communityWithRssFeed"] {
            communityDetailTable.communityWithRssFeed  = String(describing:communityWithRssFeed)
        }
        communityDetailTable.type = communityDetailCommunity["type"] as? String
        communityDetailTable.messageText = communityDetailCommunity["messageText"] as? String
        communityDetailTable.communityJabberId = communityDetailCommunity["communityJabberId"] as? String
        communityDetailTable.userCommunityJabberId = communityDetailCommunity["userCommunityJabberId"] as? String
        
        if let event_code = communityDetailCommunity["event_code"]{
            communityDetailTable.event_code = String(describing: event_code)
        }else{
            communityDetailTable.event_code = StringConstants.EMPTY
        }
        
        if let address = communityDetailCommunity["address"]{
            communityDetailTable.address = String(describing: address)
        }else{
            communityDetailTable.address = StringConstants.EMPTY
        }
        
        if let contact_person_first_name = communityDetailCommunity["contact_person_first_name"]{
            communityDetailTable.contact_person_first_name = String(describing: contact_person_first_name)
        }else{
            communityDetailTable.contact_person_first_name = StringConstants.EMPTY
        }
        
        if let contact_person_last_name = communityDetailCommunity["contact_person_last_name"]{
            communityDetailTable.contact_person_last_name = String(describing: contact_person_last_name)
        }else{
            communityDetailTable.contact_person_last_name = StringConstants.EMPTY
        }
        
        if let contact_person_designation = communityDetailCommunity["contact_person_designation"]{
            communityDetailTable.contact_person_designation = String(describing: contact_person_designation)
        }else{
            communityDetailTable.contact_person_designation = StringConstants.EMPTY
        }
        
        if let company_website = communityDetailCommunity["company_website"]{
            communityDetailTable.company_website = String(describing: company_website)
        }else{
            communityDetailTable.company_website = StringConstants.EMPTY
        }
        
        if let alloted_stall_number = communityDetailCommunity["alloted_stall_number"]{
            communityDetailTable.alloted_stall_number = String(describing: alloted_stall_number)
        }else{
            communityDetailTable.alloted_stall_number = StringConstants.EMPTY
        }
        /*if let community_suggestion_score_for_user = communityDetailCommunity["community_suggestion_score_for_user"]{
            communityDetailTable.community_suggestion_score_for_user = String(describing: community_suggestion_score_for_user)
        }else{
            communityDetailTable.community_suggestion_score_for_user = StringConstants.EMPTY
        }*/
        if let community_suggestion_score_for_user = communityDetailCommunity["community_suggestion_score_for_user"]{
            communityDetailTable.community_suggestion_score_for_user =  community_suggestion_score_for_user as! Int32 as NSNumber
        }else{
            communityDetailTable.community_suggestion_score_for_user = 0
        }
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus in core data",saveStatus)
    }
    
    func convertTemCommunityDeatilsObj(communityDetail:Dictionary<String, Any>) -> CommunityDetailTable{
       
        let communityDetailCommunity = communityDetail
        print("communityDetailCommunity",communityDetailCommunity)
        let communityDetailTable = CommunityDetailTable(context:contextTemp)
        
        communityDetailTable.communityCategories = communityDetailCommunity["communityCategories"] as? String
        communityDetailTable.communityCreatedTimestamp = String(describing: communityDetailCommunity["communityCreatedTimestamp"])
        communityDetailTable.communityDesc = communityDetailCommunity["communityDesc"] as? String
        communityDetailTable.communityDominantColour = communityDetailCommunity["communityDominantColour"] as? String
        communityDetailTable.communityImageBigThumbUrl = communityDetailCommunity["communityImageBigThumbUrl"] as? String
        communityDetailTable.communityImageSmallThumbUrl = communityDetailCommunity["communityImageSmallThumbUrl"] as? String
        communityDetailTable.communityImageUrl = communityDetailCommunity["communityImageUrl"] as? String
        communityDetailTable.communityJoinedTimestamp = String(describing:communityDetailCommunity["communityJoinedTimestamp"])
        communityDetailTable.communityKey = communityDetailCommunity["communityKey"] as? String
        communityDetailTable.communityName = communityDetailCommunity["communityName"] as? String
        communityDetailTable.communityScore = String(describing:communityDetailCommunity["communityScore"])
        //communityDetailTable.communityScore = (communityDetailCommunity["communityScore"] as! NSNumber)
        communityDetailTable.communityStatus = communityDetailCommunity["communityStatus"] as? String
        communityDetailTable.communitySubType = communityDetailCommunity["communitySubType"] as? String
        communityDetailTable.communityUpdatedTimestamp = String(describing:communityDetailCommunity["communityUpdatedTimestamp"])
        communityDetailTable.finalScore = communityDetailCommunity["finalScore"] as? String
        communityDetailTable.isdeleted = communityDetailCommunity["isDeleted"] as? String
        communityDetailTable.isMember = String(describing: communityDetailCommunity["isMember"]!)
        communityDetailTable.ownerId = communityDetailCommunity["ownerId"] as? String
        communityDetailTable.ownerImageUrl = communityDetailCommunity["ownerImageUrl"] as? String
        communityDetailTable.ownerName = communityDetailCommunity["ownerName"] as? String
        communityDetailTable.privacy = communityDetailCommunity["privacy"] as? String
        
        if let totalMembers = communityDetailCommunity["totalMembers"]{
            communityDetailTable.totalMembers = String(describing: totalMembers)
        }else{
            communityDetailTable.totalMembers = String(describing: "0")
        }
        if let communityWithRssFeed = communityDetailCommunity["communityWithRssFeed"] {
            communityDetailTable.communityWithRssFeed  = String(describing:communityWithRssFeed)
        }
        communityDetailTable.type = communityDetailCommunity["type"] as? String
        communityDetailTable.messageText = communityDetailCommunity["messageText"] as? String
        communityDetailTable.communityJabberId = communityDetailCommunity["communityJabberId"] as? String
        communityDetailTable.userCommunityJabberId = communityDetailCommunity["userCommunityJabberId"] as? String
        
        if let pavilion_name = communityDetailCommunity["pavilion_name"]{
            communityDetailTable.pavilion_name = String(describing: pavilion_name)
        }else{
            communityDetailTable.pavilion_name = String(describing: "")
        }
        
        if let event_code = communityDetailCommunity["event_code"]{
            communityDetailTable.event_code = String(describing: event_code)
        }else{
            communityDetailTable.event_code = StringConstants.EMPTY
        }
        
        if let address = communityDetailCommunity["address"]{
            communityDetailTable.address = String(describing: address)
        }else{
            communityDetailTable.address = StringConstants.EMPTY
        }
        
        if let contact_person_first_name = communityDetailCommunity["contact_person_first_name"]{
            communityDetailTable.contact_person_first_name = String(describing: contact_person_first_name)
        }else{
            communityDetailTable.contact_person_first_name = StringConstants.EMPTY
        }
        
        if let contact_person_last_name = communityDetailCommunity["contact_person_last_name"]{
            communityDetailTable.contact_person_last_name = String(describing: contact_person_last_name)
        }else{
            communityDetailTable.contact_person_last_name = StringConstants.EMPTY
        }
        
        if let contact_person_designation = communityDetailCommunity["contact_person_designation"]{
            communityDetailTable.contact_person_designation = String(describing: contact_person_designation)
        }else{
            communityDetailTable.contact_person_designation = StringConstants.EMPTY
        }
        
        if let company_website = communityDetailCommunity["company_website"]{
            communityDetailTable.company_website = String(describing: company_website)
        }else{
            communityDetailTable.company_website = StringConstants.EMPTY
        }
        
        if let alloted_stall_number = communityDetailCommunity["alloted_stall_number"]{
            communityDetailTable.alloted_stall_number = String(describing: alloted_stall_number)
        }else{
            communityDetailTable.alloted_stall_number = StringConstants.EMPTY
        }
       /* if let community_suggestion_score_for_user = communityDetailCommunity["community_suggestion_score_for_user"]{
            communityDetailTable.community_suggestion_score_for_user = String(describing: community_suggestion_score_for_user)
        }else{
            communityDetailTable.community_suggestion_score_for_user = StringConstants.EMPTY
        }*/
        
        if let community_suggestion_score_for_user = communityDetailCommunity["community_suggestion_score_for_user"]{
            communityDetailTable.community_suggestion_score_for_user =  community_suggestion_score_for_user as! Int32 as NSNumber
        }else{
            communityDetailTable.community_suggestion_score_for_user = 0
        }
        return communityDetailTable
    }
    
    func updateAtCommunityDetailTable(communityDetail:Dictionary<String, Any>){
        
        do {
            tasks = try context.fetch(CommunityDetailTable.fetchRequest()) as! [CommunityDetailTable]
            for task in tasks {
                
                let communityDetilsData = communityDetail
                let communityKey = communityDetilsData["communityKey"] as? String
                
                if task.communityKey == communityKey {
                    task.communityCategories = communityDetilsData["communityCategories"] as? String
                    task.communityCreatedTimestamp = String(describing: communityDetilsData["communityCreatedTimestamp"])
                    task.communityDesc = communityDetilsData["communityDesc"] as? String
                    task.communityDominantColour = communityDetilsData["communityDominantColour"] as? String
                    task.communityImageBigThumbUrl = communityDetilsData["communityImageBigThumbUrl"] as? String
                    task.communityImageSmallThumbUrl = communityDetilsData["communityImageSmallThumbUrl"] as? String
                    task.communityImageUrl = communityDetilsData["communityImageUrl"] as? String
                    task.communityJoinedTimestamp = String(describing: communityDetilsData["communityJoinedTimestamp"])
                    task.communityName = communityDetilsData["communityName"] as? String
                    task.communityScore = String(describing: communityDetilsData["communityScore"])
                    task.communityStatus = communityDetilsData["communityStatus"] as? String
                    task.communitySubType = communityDetilsData["communitySubType"] as? String
                    task.communityUpdatedTimestamp = String(describing: communityDetilsData["communityUpdatedTimestamp"])
                    task.finalScore = communityDetilsData["finalScore"] as? String
                    task.isdeleted = communityDetilsData["isDeleted"] as? String
                    if communityDetilsData["isMember"] != nil{
                        task.isMember = String(describing: communityDetilsData["isMember"]!)
                    }
                    task.ownerId = communityDetilsData["ownerId"] as? String
                    task.ownerImageUrl = communityDetilsData["ownerImageUrl"] as? String
                    task.ownerName = communityDetilsData["ownerName"] as? String
                    task.privacy = communityDetilsData["privacy"] as? String
                    if communityDetilsData["totalMembers"] == nil || communityDetilsData["totalMembers"] is NSNull {
                        print("No update totalMembers value")
                        if let totalMembers = communityDetilsData["memberCount"] as? Int{
                            task.totalMembers = String(totalMembers)
                        }
                    }else{
                        if let totalMembers = communityDetilsData["totalMembers"] as? Int {
                            task.totalMembers = String(describing: totalMembers )
                        }else if let totalMembers = communityDetilsData["totalMembers"] as? String {
                             task.totalMembers = String(describing: totalMembers )
                        }
                    }
                    task.type = communityDetilsData["type"] as? String
                    task.messageText = communityDetilsData["messageText"] as? String
                    task.communityJabberId = communityDetilsData["communityJabberId"] as? String
                    task.userCommunityJabberId = communityDetilsData["userCommunityJabberId"] as? String
                    if let communityWithRssFeed = communityDetilsData["communityWithRssFeed"] {
                        task.communityWithRssFeed  = String(describing:communityWithRssFeed)
                    }
                    
                    if let pavilion_name = communityDetilsData["pavilion_name"] {
                        task.pavilion_name  = String(describing:pavilion_name)
                    }
                    
                    if let event_code = communityDetilsData["event_code"] {
                        task.event_code  = String(describing:event_code)
                    }
                    
                    if let address = communityDetilsData["address"] {
                        task.address  = String(describing:address)
                    }
                                        
                    if let contact_person_first_name = communityDetilsData["contact_person_first_name"] {
                        task.contact_person_first_name  = String(describing:contact_person_first_name)
                    }
                    
                    if let contact_person_last_name = communityDetilsData["contact_person_last_name"] {
                        task.contact_person_last_name  = String(describing:contact_person_last_name)
                    }
                    
                    if let contact_person_designation = communityDetilsData["contact_person_designation"] {
                        task.contact_person_designation  = String(describing:contact_person_designation)
                    }
                    
                    if let company_website = communityDetilsData["company_website"] {
                        task.company_website  = String(describing:company_website)
                    }
                    
                    if let alloted_stall_number = communityDetilsData["alloted_stall_number"] {
                        task.alloted_stall_number  = String(describing:alloted_stall_number)
                    }
                   /* if let community_suggestion_score_for_user = communityDetilsData["community_suggestion_score_for_user"]{
                        task.community_suggestion_score_for_user = String(describing: community_suggestion_score_for_user)
                    }*/
                     if let community_suggestion_score_for_user = communityDetilsData["community_suggestion_score_for_user"]{
                        task.community_suggestion_score_for_user = community_suggestion_score_for_user as! Int32 as NSNumber
                     }
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
        
    }
    
    // Get getCommunityDetails
    func getCommunityDetails() -> [CommunityDetailTable] {
        do {
            tasks = try context.fetch(CommunityDetailTable.fetchRequest())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    func getCommunityDetailsForRightVC() -> [CommunityDetailTable] {
        do {
            tasks = try context.fetch(CommunityDetailTable.getAllCommunitiesDetilsOnRightVC())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    
    // Make status as isMember true for  follow channel
    func updateisMemberFromComminityDetailsTbl(isMemberValue:String,communityKey:String){
        
        do {
            tasks = try context.fetch(CommunityDetailTable.fetchRequest()) as! [CommunityDetailTable]
            for task in tasks {
                if task.communityKey == communityKey {
                    task.isMember = isMemberValue
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus",saveStatus)
    }
    
    //fetchChannelAfterParticularTimeStamp
    func fetchChannelAfterSentTimeStamp(communityCreatedTimestamp:String)-> [CommunityDetailTable]{
        
        do {
            tasks = try context.fetch(CommunityDetailTable.fetchChannelAfterParticularTimeStamp(communityCreatedTimestamp: communityCreatedTimestamp))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    
    // Check communityKey presentLocalD from local db
    func checkMyChatCommunityKey(communityKey:String) -> [CommunityDetailTable] {
        do {
            tasks = try context.fetch(CommunityDetailTable.fetchMessageCommunityKey(communityKey: communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    
    //Fetch 10 records from communityDetail Tbl on scroll
    func getCommunityDetailsRecord(_ fetchOffSet: Int) -> [CommunityDetailTable] {
        var record = [CommunityDetailTable]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDesc = NSEntityDescription.entity(forEntityName: "CommunityDetailTable", in: self.context)
        fetchRequest.entity = entityDesc
        let sectionSortDescriptor = NSSortDescriptor(key: "community_suggestion_score_for_user", ascending: false)
        //let sectionSortDescriptor2 = NSSortDescriptor(key: "communityScore", ascending: false)
        //let sectionSortDescriptor3 = NSSortDescriptor(key: "communityJoinedTimestamp", ascending:false)
       // let sectionSortDescriptor2 = NSSortDescriptor(key: "communityScore", ascending: false)
        let sortDescriptors = [sectionSortDescriptor/*,sectionSortDescriptor2,sectionSortDescriptor3*/]
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.fetchLimit = 10
        fetchRequest.fetchOffset = fetchOffSet

        do {
            //tasks = try context.fetch(CommunityDetailTable.fetchRequest())

            var fetchedOjects: [CommunityDetailTable] = try self.context.fetch(fetchRequest) as! [CommunityDetailTable]
            for  i in (0..<fetchedOjects.count){
                let communityDetail  = fetchedOjects[i]
                record.append(communityDetail)
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        print(record)
        return record
    }
    
}
