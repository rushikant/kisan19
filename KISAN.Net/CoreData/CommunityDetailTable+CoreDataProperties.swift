//
//  CommunityDetailTable+CoreDataProperties.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 07/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//
//

import Foundation
import CoreData

extension CommunityDetailTable {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CommunityDetailTable> {
        //return NSFetchRequest<CommunityDetailTable>(entityName: "CommunityDetailTable")
        let fetchRequest = NSFetchRequest<CommunityDetailTable>(entityName: "CommunityDetailTable")
        let sectionSortDescriptor = NSSortDescriptor(key: "community_suggestion_score_for_user", ascending: false)
        //let sectionSortDescriptor2 = NSSortDescriptor(key: "communityScore", ascending: false)
        //let sectionSortDescriptor3 = NSSortDescriptor(key: "communityJoinedTimestamp", ascending: false)

       // let sectionSortDescriptor2 = NSSortDescriptor(key: "communityScore", ascending: false)
        fetchRequest.sortDescriptors = [sectionSortDescriptor/*,sectionSortDescriptor2,sectionSortDescriptor3*/]
        fetchRequest.fetchLimit = 10
        return fetchRequest
    }
    
    @nonobjc public class func getAllCommunitiesDetilsOnRightVC() -> NSFetchRequest<CommunityDetailTable> {
        return NSFetchRequest<CommunityDetailTable>(entityName: "CommunityDetailTable")
    }
    
    @nonobjc public class func fetchMessageCommunityKey(communityKey:String) -> NSFetchRequest<CommunityDetailTable> {
        let fetchRequest = NSFetchRequest<CommunityDetailTable>(entityName: "CommunityDetailTable")
        let predicate = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    @nonobjc public class func fetchChannelAfterParticularTimeStamp(communityCreatedTimestamp:String) -> NSFetchRequest<CommunityDetailTable> {
        let fetchRequest = NSFetchRequest<CommunityDetailTable>(entityName: "CommunityDetailTable")
        let sectionSortDescriptor = NSSortDescriptor(key: "communityCreatedTimestamp", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        let predicate = NSPredicate(format: "communityCreatedTimestamp <=  %@", communityCreatedTimestamp)
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 10
        return fetchRequest
    }
    
    @NSManaged public var communityCategories: String?
    @NSManaged public var communityCreatedTimestamp: String?
    @NSManaged public var communityDeletedTimestamp: String?
    @NSManaged public var communityDesc: String?
    @NSManaged public var communityDominantColour: String?
    @NSManaged public var communityImageBigThumbUrl: String?
    @NSManaged public var communityImageSmallThumbUrl: String?
    @NSManaged public var communityImageUrl: String?
    @NSManaged public var communityJoinedTimestamp: String?
    @NSManaged public var communityKey: String?
    @NSManaged public var communityLeftTimestamp: String?
    @NSManaged public var communityName: String?
    @NSManaged public var communityScore: String?
    @NSManaged public var communityStatus: String?
    @NSManaged public var communitySubType: String?
    @NSManaged public var communityUpdatedTimestamp: String?
    @NSManaged public var featured: String?
    @NSManaged public var finalScore: String?
    @NSManaged public var isCommunityWithRssFeed: String?
    @NSManaged public var isdeleted: String?
    @NSManaged public var isMember: String?
    @NSManaged public var messageCount: String?
    @NSManaged public var ownerId: String?
    @NSManaged public var ownerImageUrl: String?
    @NSManaged public var ownerName: String?
    @NSManaged public var privacy: String?
    @NSManaged public var totalMembers: String?
    @NSManaged public var type: String?
    @NSManaged public var messageText: String?
    @NSManaged public var communityJabberId: String?
    @NSManaged public var communityWithRssFeed: String?
    @NSManaged public var userCommunityJabberId: String?
    @NSManaged public var pavilion_name: String?
   // New
    @NSManaged public var alloted_stall_number: String?
    @NSManaged public var event_code: String?
    @NSManaged public var address: String?
    @NSManaged public var contact_person_first_name: String?
    @NSManaged public var contact_person_last_name: String?
    @NSManaged public var contact_person_designation: String?
    @NSManaged public var company_website: String?
    @NSManaged public var community_suggestion_score_for_user: NSNumber?
}
