//
//  MembersServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 24/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MembersServices{
    static let sharedInstance = MembersServices()
    var tasks = [MembersTable]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    func sendMembersAtMembersTable(membersDetails:Dictionary<String, Any>){
        let membersDetailsValue = membersDetails
        let membersDetailsTable = MembersTable(context:context)
        membersDetailsTable.accountType = membersDetailsValue["accountType"] as? String
        membersDetailsTable.bigThumbUrl = membersDetailsValue["bigThumbUrl"] as? String
        membersDetailsTable.categoryIds = membersDetailsValue["categoryIds"] as? String
        membersDetailsTable.city = membersDetailsValue["city"] as? String
        membersDetailsTable.country = membersDetailsValue["country"] as? String
        membersDetailsTable.createdDateTime = membersDetailsValue["createdDateTime"] as? String
        membersDetailsTable.fcmIds = membersDetailsValue["fcmIds"] as? String
        membersDetailsTable.firstName = membersDetailsValue["firstName"] as? String
        membersDetailsTable.userId = membersDetailsValue["userId"] as? String
        membersDetailsTable.imageUrl = membersDetailsValue["imageUrl"] as? String
        membersDetailsTable.isBlocked = membersDetailsValue["isBlocked"] as? String
        membersDetailsTable.isdeleted = membersDetailsValue["isDeleted"] as? String
        membersDetailsTable.isSpam = membersDetailsValue["isSpam"] as? String
        membersDetailsTable.joinedDateTime = membersDetailsValue["joinedDateTime"] as? String
        membersDetailsTable.lastActiveDateTime = membersDetailsValue["lastActiveDateTime"] as? String
        membersDetailsTable.lastLoginDateTime = membersDetailsValue["lastLoginDateTime"] as? String
        membersDetailsTable.lastLogoutDateTime = membersDetailsValue["lastLogoutDateTime"] as? String
        membersDetailsTable.lastName = membersDetailsValue["lastName"] as? String
        membersDetailsTable.profileStatus = membersDetailsValue["profileStatus"] as? String
        membersDetailsTable.role = membersDetailsValue["role"] as? String
        membersDetailsTable.smallThumbUrl = membersDetailsValue["smallThumbUrl"] as? String
        membersDetailsTable.state = membersDetailsValue["state"] as? String
        membersDetailsTable.updatedDateTime = membersDetailsValue["updatedDateTime"] as? String
        membersDetailsTable.communityKey = membersDetailsValue["communityKey"] as? String
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus in core data",saveStatus)
    }
    
    
    
    func updateAtMembersTable(membersDetails:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(MembersTable.fetchRequest()) as! [MembersTable]
            for task in tasks {
                let membersDetailsData = membersDetails
                let userId = membersDetailsData["userId"] as? String
                if task.userId == userId {
                    task.accountType = membersDetailsData["accountType"] as? String
                    task.bigThumbUrl = membersDetailsData["bigThumbUrl"] as? String
                    task.categoryIds = membersDetailsData["categoryIds"] as? String
                    task.city = membersDetailsData["city"] as? String
                    task.country = membersDetailsData["country"] as? String
                    task.createdDateTime = membersDetailsData["createdDateTime"] as? String
                    task.fcmIds = membersDetailsData["fcmIds"] as? String
                    task.firstName = membersDetailsData["firstName"] as? String
                    task.userId = membersDetailsData["userId"] as? String
                    task.imageUrl = membersDetailsData["imageUrl"] as? String
                    task.isBlocked = membersDetailsData["isBlocked"] as? String
                    task.isdeleted = membersDetailsData["isDeleted"] as? String
                    task.isSpam = membersDetailsData["isSpam"] as? String
                    task.joinedDateTime = membersDetailsData["joinedDateTime"] as? String
                    task.lastActiveDateTime = membersDetailsData["lastActiveDateTime"] as? String
                    task.lastLoginDateTime = membersDetailsData["lastLoginDateTime"] as? String
                    task.lastLogoutDateTime = membersDetailsData["lastLogoutDateTime"] as? String
                    task.lastName = membersDetailsData["lastName"] as? String
                    task.profileStatus = membersDetailsData["profileStatus"] as? String
                    task.role = membersDetailsData["role"] as? String
                    task.smallThumbUrl = membersDetailsData["smallThumbUrl"] as? String
                    task.state = membersDetailsData["state"] as? String
                    task.updatedDateTime = membersDetailsData["updatedDateTime"] as? String
                    task.communityKey = membersDetailsData["communityKey"] as? String
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    
    //----------------- Particular user details ------------------ //
    
    func sendParticularMembersAtMembersTable(membersDetails:Dictionary<String, Any>){
        //email id mobile password username

        let membersDetailsValue = membersDetails
        let membersDetailsTable = MembersTable(context:context)
        membersDetailsTable.accountType = membersDetailsValue["account_type"] as? String
        membersDetailsTable.bigThumbUrl = membersDetailsValue["big_thumb_url"] as? String
        membersDetailsTable.categoryIds = membersDetailsValue["categories"] as? String
        membersDetailsTable.city = membersDetailsValue["city"] as? String
        membersDetailsTable.country = membersDetailsValue["country"] as? String
        membersDetailsTable.createdDateTime = membersDetailsValue["created_datetime"] as? String
        membersDetailsTable.firstName = membersDetailsValue["first_name"] as? String
        membersDetailsTable.imageUrl = membersDetailsValue["image_url"] as? String
        membersDetailsTable.isBlocked = membersDetailsValue["is_blocked"] as? String
        membersDetailsTable.isdeleted = membersDetailsValue["is_deleted"] as? String
        membersDetailsTable.isSpam = membersDetailsValue["is_spam"] as? String
        membersDetailsTable.lastActiveDateTime = membersDetailsValue["last_active_datetime"] as? String
        membersDetailsTable.lastLoginDateTime = membersDetailsValue["last_login_datetime"] as? String
        membersDetailsTable.lastLogoutDateTime = membersDetailsValue["last_logout_datetime"] as? String
        membersDetailsTable.lastName = membersDetailsValue["last_name"] as? String
        membersDetailsTable.profileStatus = membersDetailsValue["profile_status"] as? String
        membersDetailsTable.smallThumbUrl = membersDetailsValue["small_thumb_url"] as? String
        membersDetailsTable.state = membersDetailsValue["state"] as? String
        membersDetailsTable.updatedDateTime = membersDetailsValue["updated_datetime"] as? String
        membersDetailsTable.email = membersDetailsValue["email"] as? String
        membersDetailsTable.id = membersDetailsValue["id"] as? String
        membersDetailsTable.mobile = membersDetailsValue["mobile"] as? String
        membersDetailsTable.password = membersDetailsValue["password"] as? String
        membersDetailsTable.username = membersDetailsValue["username"] as? String
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus in core data",saveStatus)
    }
    
    
    
    func updateParticularMembersAtMembersTable(membersDetails:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(MembersTable.fetchRequest()) as! [MembersTable]
            for task in tasks {
                let membersDetailsData = membersDetails
                let id = membersDetailsData["id"] as? String
                if task.id == id {
                    task.accountType = membersDetailsData["account_type"] as? String
                    task.bigThumbUrl = membersDetailsData["big_thumb_url"] as? String
                    task.categoryIds = membersDetailsData["categories"] as? String
                    task.city = membersDetailsData["city"] as? String
                    task.country = membersDetailsData["country"] as? String
                    task.createdDateTime = membersDetailsData["created_datetime"] as? String
                    task.firstName = membersDetailsData["first_name"] as? String
                    task.imageUrl = membersDetailsData["image_url"] as? String
                    task.isBlocked = membersDetailsData["is_blocked"] as? String
                    task.isdeleted = membersDetailsData["is_deleted"] as? String
                    task.isSpam = membersDetailsData["is_spam"] as? String
                    task.lastActiveDateTime = membersDetailsData["last_active_datetime"] as? String
                    task.lastLoginDateTime = membersDetailsData["last_login_datetime"] as? String
                    task.lastLogoutDateTime = membersDetailsData["last_logout_datetime"] as? String
                    task.lastName = membersDetailsData["last_name"] as? String
                    task.profileStatus = membersDetailsData["profile_status"] as? String
                    task.smallThumbUrl = membersDetailsData["small_thumb_url"] as? String
                    task.state = membersDetailsData["state"] as? String
                    task.updatedDateTime = membersDetailsData["updated_datetime"] as? String
                    task.email = membersDetailsData["email"] as? String
                    task.id = membersDetailsData["id"] as? String
                    task.mobile = membersDetailsData["mobile"] as? String
                    task.password = membersDetailsData["password"] as? String
                    task.username = membersDetailsData["username"] as? String
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    //--------------------------------------------------------------------------------------------//
    
    // Get getMembersDetails on communityKey
    func getMembersDetails(communityKey:String) -> [MembersTable] {
        do {
            tasks = try context.fetch(MembersTable.fetchRequest(communityKey:communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // Get getMembersDetails on id
    func getMembersDetailsOnId(id:String) -> [MembersTable] {
        do {
            tasks = try context.fetch(MembersTable.fetchRecordOnId(id:id))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    func getBlockedMembersList(communityKey:String) -> [MembersTable] {
        do {
            tasks = try context.fetch(MembersTable.fetchRequestBlockedMembersList(communityKey:communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // block members from local
    func changeStatusOfBlockAndUnBlock(userId:String,isBlocked:String){
        do {
            tasks = try context.fetch(MembersTable.fetchRequest()) as! [MembersTable]
            for task in tasks {
                if task.userId == userId {
                    task.isBlocked = isBlocked
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
    }
    
    // Check userId presentLocalD from local db
    func checkMembersuserId(userId:String) -> [MembersTable] {
        do {
            tasks = try context.fetch(MembersTable.fetchRecordOnUserId(userId: userId))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    
    //Delete blocked members from local
    func deleteRemoveMemberFromChannel(userId:String){
        let fetchRequest = NSFetchRequest<MembersTable>(entityName: "MembersTable")
        let predicate = NSPredicate(format: "userId LIKE[c] %@",userId )
        fetchRequest.predicate = predicate
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save()
        } catch {
            print("Error fetching data from CoreData")
        }
    }
    
    //Fetch 10 records from MembersTable on scroll
    func getMembersDetailsRecord(_ fetchOffSet: Int,communityKey:String,isBlocked:String) -> [MembersTable] {
        
        var record = [MembersTable]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.fetchLimit = 10
        fetchRequest.fetchOffset = fetchOffSet
        let entityDesc = NSEntityDescription.entity(forEntityName: "MembersTable", in: self.context)
        fetchRequest.entity = entityDesc
        
        let predicate1 = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        let predicate2 = NSPredicate(format: "isBlocked LIKE[c] %@",isBlocked)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound

        do {
            var fetchedOjects: [MembersTable] = try self.context.fetch(fetchRequest) as! [MembersTable]
            for  i in (0..<fetchedOjects.count){
                let communityDetail  = fetchedOjects[i]
                record.append(communityDetail)
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        return record
    }
    
}
