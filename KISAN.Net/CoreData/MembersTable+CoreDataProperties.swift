//
//  MembersTable+CoreDataProperties.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 24/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//
//

import Foundation
import CoreData


extension MembersTable {

    @nonobjc public class func fetchRequest(communityKey:String) -> NSFetchRequest<MembersTable> {
        let fetchRequest = NSFetchRequest<MembersTable>(entityName: "MembersTable")
        fetchRequest.fetchLimit = 10
        let predicate1 = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        let predicate2 = NSPredicate(format: "isBlocked LIKE[c] %@","f")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    @nonobjc public class func fetchRequestBlockedMembersList(communityKey:String) -> NSFetchRequest<MembersTable> {
        let fetchRequest = NSFetchRequest<MembersTable>(entityName: "MembersTable")
        fetchRequest.fetchLimit = 10
        let predicate1 = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        let predicate2 = NSPredicate(format: "isBlocked LIKE[c] %@","t")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    @nonobjc public class func fetchRecordOnUserId(userId:String) -> NSFetchRequest<MembersTable> {
        let fetchRequest = NSFetchRequest<MembersTable>(entityName: "MembersTable")
        let predicate = NSPredicate(format: "userId LIKE[c] %@", userId)
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    @nonobjc public class func fetchRecordOnId(id:String) -> NSFetchRequest<MembersTable> {
        let fetchRequest = NSFetchRequest<MembersTable>(entityName: "MembersTable")
        let predicate = NSPredicate(format: "id LIKE[c] %@", id)
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var accountType: String?
    @NSManaged public var bigThumbUrl: String?
    @NSManaged public var categoryIds: String?
    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var createdDateTime: String?
    @NSManaged public var fcmIds: String?
    @NSManaged public var firstName: String?
    @NSManaged public var userId: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var isBlocked: String?
    @NSManaged public var isdeleted: String?
    @NSManaged public var isSpam: String?
    @NSManaged public var joinedDateTime: String?
    @NSManaged public var lastActiveDateTime: String?
    @NSManaged public var lastLoginDateTime: String?
    @NSManaged public var lastLogoutDateTime: String?
    @NSManaged public var lastName: String?
    @NSManaged public var profileStatus: String?
    @NSManaged public var role: String?
    @NSManaged public var smallThumbUrl: String?
    @NSManaged public var state: String?
    @NSManaged public var updatedDateTime: String?
    @NSManaged public var communityKey: String?
    @NSManaged public var email: String?
    @NSManaged public var id: String?
    @NSManaged public var mobile: String?
    @NSManaged public var password: String?
    @NSManaged public var username: String?
}
