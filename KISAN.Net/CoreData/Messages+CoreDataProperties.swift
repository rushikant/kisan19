//
//  Messages+CoreDataProperties.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//
//

import Foundation
import CoreData


extension Messages {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Messages> {
        return NSFetchRequest<Messages>(entityName: "Messages")
    }
    
    @nonobjc public class func fetchMessageOnCommunityKey(communityKey:String) -> NSFetchRequest<Messages> {
        let fetchRequest = NSFetchRequest<Messages>(entityName: "Messages")
        let predicate = NSPredicate(format: "communityId LIKE[c] %@", communityKey)
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    @nonobjc public class func checkMessageKeyPresentInDB(messageKey:String) -> NSFetchRequest<Messages> {
        let fetchRequest = NSFetchRequest<Messages>(entityName: "Messages")
        let predicate = NSPredicate(format: "id LIKE[c] %@", messageKey)
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    @nonobjc public class func fetchMessageInOfflineMode() -> NSFetchRequest<Messages> {
        let fetchRequest = NSFetchRequest<Messages>(entityName: "Messages")
        let predicate = NSPredicate(format: "messageStatus LIKE[c] %@", StringConstants.Status.pending) 
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
 
    @NSManaged public var contentField2: String?
    @NSManaged public var communityId: String?
    @NSManaged public var contentField5: String?
    @NSManaged public var messageType: String?
    @NSManaged public var contentField1: String?
    @NSManaged public var contentField3: String?
    @NSManaged public var contentField7: String?
    @NSManaged public var deletedDatetime: String?
    @NSManaged public var mediaType: String?
    @NSManaged public var contentField6: String?
    @NSManaged public var contentField8: String?
    @NSManaged public var jabberId: String?
    @NSManaged public var hiddenDatetime: String?
    @NSManaged public var deletedbyUserId: String?
    @NSManaged public var contentField9: String?
    @NSManaged public var id: String?
    @NSManaged public var contentField4: String?
    @NSManaged public var createdDatetime: String?
    @NSManaged public var hiddenbyUserId: String?
    @NSManaged public var contentField10: String?
    @NSManaged public var senderId: String?
    
    @NSManaged public var messageStatus: String?
    @NSManaged public var messageStatusTime: String?
    @NSManaged public var hiddenStatus: String?
    @NSManaged public var messageRecipientId: String?
    @NSManaged public var savedCurrentTimeStatus: String?
    
    @NSManaged public var senderBigThumbnailUrl: String?
    @NSManaged public var senderFirstName: String?
    @NSManaged public var senderImageUrl: String?
    @NSManaged public var senderLastActiveDatetime: String?
    @NSManaged public var senderLastName: String?
    @NSManaged public var senderProfilestatus: String?
    @NSManaged public var senderRole: String?
    @NSManaged public var senderSmallThumbnailUrl: String?
    @NSManaged public var senderUserName: String?
    @NSManaged public var userCommunityJabberID: String?
    @NSManaged public var userRole: String?
    @NSManaged public var communityName: String?
    @NSManaged public var communityProfileUrl: String?

}
