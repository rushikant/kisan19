//
//  MessagesServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MessagesServices{
    static let sharedInstance = MessagesServices()
    var tasks = [Messages]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let contextTemp = (UIApplication.shared.delegate as! AppDelegate).persistentContainerTemp.viewContext
    
    func sendMessageAtMessageTable(messageDetail:Dictionary<String, Any>,saveDataAtDB:Bool) -> Messages {
        let messageDetails = messageDetail
        let messages = Messages(context:context)
        
        messages.contentField2 =  messageDetails["contentField2"] as? String ?? ""
        messages.communityId  =   messageDetails["communityId"] as? String ?? ""
        messages.contentField5 = messageDetails["contentField5"] as? String ?? ""
        messages.messageType = messageDetails["messageType"] as? String ?? ""
        messages.contentField1 = messageDetails["contentField1"] as? String ?? ""
        
        messages.contentField3 = messageDetails["contentField3"] as? String ?? ""
        messages.contentField7 = messageDetails["contentField7"] as? String ?? ""
        messages.deletedDatetime = messageDetails["deletedDatetime"] as? String ?? ""
        messages.mediaType = messageDetails["mediaType"] as? String ?? ""
        messages.contentField6 = messageDetails["contentField6"] as? String ?? ""
        messages.contentField8 = messageDetails["contentField8"] as? String ?? ""
        messages.jabberId = messageDetails["jabberId"] as? String ?? ""
        messages.hiddenDatetime = messageDetails["hiddenDatetime"] as? String ?? ""
        messages.deletedbyUserId = messageDetails["deletedbyUserId"] as? String ?? ""
        messages.contentField9 = messageDetails["contentField9"] as? String ?? ""
        messages.id = messageDetails["id"] as? String ?? ""
        messages.contentField4 = messageDetails["contentField4"] as? String ?? ""
        if let createdDatetime = messageDetails["createdDatetime"]{
            messages.createdDatetime = String(describing: createdDatetime)
            messages.messageStatusTime = String(describing: createdDatetime)
        }
        messages.hiddenbyUserId = messageDetails["hiddenbyUserId"] as? String ?? ""
        messages.contentField10 = messageDetails["contentField10"] as? String ?? ""
        messages.senderId = messageDetails["senderId"] as? String ?? ""
        
        messages.messageStatus = messageDetails["messageStatus"] as? String ?? ""
        messages.hiddenStatus = messageDetails["hiddenStatus"] as? String ?? ""
        messages.messageRecipientId = messageDetails["messageRecipientId"] as? String ?? ""
        messages.savedCurrentTimeStatus = messageDetails["savedCurrentTimeStatus"] as? String ?? ""
        
        messages.senderBigThumbnailUrl = messageDetails["senderBigThumbnailUrl"] as? String ?? ""
        messages.senderFirstName = messageDetails["senderFirstName"] as? String ?? ""
        messages.senderImageUrl = messageDetails["senderImageUrl"] as? String ?? ""
        messages.senderLastActiveDatetime = messageDetails["senderLastActiveDatetime"] as? String ?? ""
        messages.senderLastName = messageDetails["senderLastName"] as? String ?? ""
        messages.senderProfilestatus = messageDetails["senderProfilestatus"] as? String ?? ""
        messages.senderRole = messageDetails["senderRole"] as? String ?? ""
        messages.senderSmallThumbnailUrl = messageDetails["senderSmallThumbnailUrl"] as? String ?? ""
        messages.senderUserName = messageDetails["senderUserName"] as? String ?? ""
        messages.userCommunityJabberID = messageDetails["userCommunityJabberID"] as? String ?? ""
        messages.userRole = messageDetails["userRole"] as? String ?? ""
        messages.communityName = messageDetails["communityName"] as? String ?? ""
        messages.communityProfileUrl = messageDetails["communityProfileUrl"] as? String ?? ""

        
        if saveDataAtDB == true{
            let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
            print("saveStatus in core data",saveStatus)
        }
        return messages
    }
    
    
    func checkIsNullValue(messageValue:Any?) -> String {
        var messageData = ""
        if let message = messageValue{
            messageData = String(describing: message)
            return messageData
        }else{
            return messageData
        }
    }
    
    func updateAtMessageTable(messageDetail:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(Messages.fetchRequest()) as! [Messages]
            for task in tasks {
                let messageDetailsData = messageDetail
                let id = messageDetailsData["id"] as? String
                if task.id == id {
                    task.contentField2 = messageDetailsData["contentField2"] as? String
                    task.communityId = messageDetailsData["communityId"] as? String
                    task.contentField5 = messageDetailsData["contentField5"] as? String
                    task.messageType = messageDetailsData["messageType"] as? String
                    task.contentField1 = messageDetailsData["contentField1"] as? String
                    task.contentField3 = messageDetailsData["contentField3"] as? String
                    task.contentField7 = messageDetailsData["contentField7"] as? String
                    task.deletedDatetime = messageDetailsData["deletedDatetime"] as? String
                    task.mediaType = messageDetailsData["mediaType"] as? String
                    task.contentField6 = messageDetailsData["contentField6"] as? String
                    task.contentField8 = messageDetailsData["contentField8"] as? String
                    task.jabberId = messageDetailsData["jabberId"] as? String
                    task.hiddenDatetime = messageDetailsData["hiddenDatetime"] as? String
                    task.deletedbyUserId = messageDetailsData["deletedbyUserId"] as? String
                    task.contentField9 = messageDetailsData["contentField9"] as? String
                    task.id = messageDetailsData["id"] as? String
                    let contentField4 = messageDetailsData["contentField4"] as? String
                    if contentField4 == nil{
                        print("contentField4",contentField4 ?? String())
                    }else if !(contentField4?.isEmpty)!{
                        task.contentField4 = contentField4
                    }
                    if let createdDatetime = messageDetailsData["createdDatetime"]{
                        task.createdDatetime = String(describing: createdDatetime)
                        task.messageStatusTime = String(describing: createdDatetime)
                    }
                    
                    task.hiddenbyUserId = messageDetailsData["hiddenbyUserId"] as? String
                    task.contentField10 = messageDetailsData["contentField10"] as? String
                    task.senderId = messageDetailsData["senderId"] as? String
                    
                    task.messageStatus = messageDetailsData["messageStatus"] as? String
                    task.hiddenStatus = messageDetailsData["hiddenStatus"] as? String
                    task.messageRecipientId = messageDetailsData["messageRecipientId"] as? String
                    task.savedCurrentTimeStatus = messageDetailsData["savedCurrentTimeStatus"] as? String
                    
                    task.senderBigThumbnailUrl = messageDetailsData["senderBigThumbnailUrl"] as? String
                    task.senderFirstName = messageDetailsData["senderFirstName"] as? String
                    task.senderImageUrl = messageDetailsData["senderImageUrl"] as? String
                    task.senderLastActiveDatetime = messageDetailsData["senderLastActiveDatetime"] as? String
                    task.senderLastName = messageDetailsData["senderLastName"] as? String
                    task.senderProfilestatus = messageDetailsData["senderProfilestatus"] as? String
                    task.senderRole = messageDetailsData["senderRole"] as? String
                    task.senderSmallThumbnailUrl = messageDetailsData["senderSmallThumbnailUrl"] as? String
                    task.senderUserName = messageDetailsData["senderUserName"] as? String
                    task.userCommunityJabberID = messageDetailsData["userCommunityJabberID"] as? String
                    task.userRole = messageDetailsData["userRole"] as? String
                    task.communityName = messageDetailsData["communityName"] as? String ?? ""
                    task.communityProfileUrl = messageDetailsData["communityProfileUrl"] as? String ?? ""
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
        
    }
    
    // Create Temp message object model
    func crateMessageInBaseType(messageDetail:Dictionary<String, Any>) -> Messages {
        let messageDetails = messageDetail
        let messages = Messages(context:contextTemp)
        messages.contentField2 = messageDetails["contentField2"] as? String
        messages.communityId = messageDetails["communityId"] as? String
        messages.contentField5 = messageDetails["contentField5"] as? String
        messages.messageType = messageDetails["messageType"] as? String
        messages.contentField1 = messageDetails["contentField1"] as? String
        messages.contentField3 = messageDetails["contentField3"] as? String
        messages.contentField7 = messageDetails["contentField7"] as? String
        messages.deletedDatetime = messageDetails["deletedDatetime"] as? String
        messages.mediaType = messageDetails["mediaType"] as? String
        messages.contentField6 = messageDetails["contentField6"] as? String
        messages.contentField8 = messageDetails["contentField8"] as? String
        messages.jabberId = messageDetails["jabberId"] as? String
        messages.hiddenDatetime = messageDetails["hiddenDatetime"] as? String
        messages.deletedbyUserId = messageDetails["deletedbyUserId"] as? String
        messages.contentField9 = messageDetails["contentField9"] as? String
        messages.id = messageDetails["id"] as? String
        messages.contentField4 = messageDetails["contentField4"] as? String
        if let createdDatetime = messageDetails["createdDatetime"]{
            messages.createdDatetime = String(describing: createdDatetime)
            messages.messageStatusTime = String(describing: createdDatetime)
        }
        messages.hiddenbyUserId = messageDetails["hiddenbyUserId"] as? String
        messages.contentField10 = messageDetails["contentField10"] as? String
        messages.senderId = messageDetails["senderId"] as? String
        
        messages.messageStatus = messageDetails["messageStatus"] as? String
        messages.hiddenStatus = messageDetails["hiddenStatus"] as? String
        messages.messageRecipientId = messageDetails["messageRecipientId"] as? String
        messages.savedCurrentTimeStatus = messageDetails["savedCurrentTimeStatus"] as? String
        
        messages.senderBigThumbnailUrl = messageDetails["senderBigThumbnailUrl"] as? String
        messages.senderFirstName = messageDetails["senderFirstName"] as? String
        messages.senderImageUrl = messageDetails["senderImageUrl"] as? String
        messages.senderLastActiveDatetime = messageDetails["senderLastActiveDatetime"] as? String
        messages.senderLastName = messageDetails["senderLastName"] as? String
        messages.senderProfilestatus = messageDetails["senderProfilestatus"] as? String
        messages.senderRole = messageDetails["senderRole"] as? String
        messages.senderSmallThumbnailUrl = messageDetails["senderSmallThumbnailUrl"] as? String
        messages.senderUserName = messageDetails["senderUserName"] as? String
        messages.userCommunityJabberID = messageDetails["userCommunityJabberID"] as? String
        messages.userRole = messageDetails["userRole"] as? String
        messages.communityName = messageDetails["communityName"] as? String ?? ""
        messages.communityProfileUrl = messageDetails["communityProfileUrl"] as? String ?? ""
        
        return messages
    }
    
    // Get getMyChatDetails
    func getMyChatDetails(communityKey:String) -> [Messages] {
        do {
            tasks = try context.fetch(Messages.fetchRequest())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    
    // get message on particular  communityKey from local db
    func getMyChatDetailsOnCommunityKey(communityKey:String) -> [Messages] {
        do {
            tasks = try context.fetch(Messages.fetchMessageOnCommunityKey(communityKey: communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // Pagination for fetching mychat on communityKey
    func getMyChatDetailsOnCommunityKeyPagination(_ fetchOffSet: Int,communityKey:String) -> [Messages] {
        var message = [Messages]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        let sectionSortDescriptor = NSSortDescriptor(key: #keyPath(Messages.createdDatetime), ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        fetchRequest.fetchLimit = 10
        fetchRequest.fetchOffset = fetchOffSet
        
        let entityDesc = NSEntityDescription.entity(forEntityName: "Messages", in: self.context)
        fetchRequest.predicate = NSPredicate(format: "communityId = %@", communityKey)
        fetchRequest.entity = entityDesc
        do {
            var fetchedOjects: [Messages] = try self.context.fetch(fetchRequest) as! [Messages]
            for  i in (0..<fetchedOjects.count){
                let messageDetail  = fetchedOjects[i]
                message.append(messageDetail)
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        return message
    }
    
    func getPendingMessagesInOffline() -> [Messages] {
        do {
            tasks = try context.fetch(Messages.fetchMessageInOfflineMode())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    
    func checkMessagePresentInDB(messageKey:String) -> [Messages]{
        do {
            tasks = try context.fetch(Messages.checkMessageKeyPresentInDB(messageKey: messageKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // delete mesages from local database
    //Delete notification
    func deleteMessageFromMessageTable(messageDetails:Messages,messageId:String,communityKey:String) -> Bool{
        var status = false
        let messageKey = messageId
        let communities = communityKey
        let fetchRequest = NSFetchRequest<Messages>(entityName: "Messages")
        let predicate1 = NSPredicate(format: "id LIKE[c] %@",messageKey )
        let predicate2 = NSPredicate(format: "communityId LIKE[c] %@",communities)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save()
            status = true
        } catch {
            status = false
            print("Error fetching data from CoreData")
        }
        return status
    }
}
