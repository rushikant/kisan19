//
//  MyChatServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MyChatServices{
    static let sharedInstance = MyChatServices()
    var tasks = [MyChatsTable]()  
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let contextTemp = (UIApplication.shared.delegate as! AppDelegate).persistentContainerTemp.viewContext

    func sendMessageAtMyChatTable(communityDetail:Dictionary<String, Any>, isFirstTime:Bool,iSCommingFromOneTOneinitiate:Bool){

        let communityDetailCommunity = communityDetail
        print("communityDetailCommunity",communityDetailCommunity)
        let myChatTable = MyChatsTable(context:context)
        myChatTable.communityCategories = communityDetailCommunity["communityCategories"] as? String
        myChatTable.communityCreatedTimestamp = communityDetailCommunity["communityCreatedTimestamp"] as? String
        myChatTable.communityDesc = communityDetailCommunity["communityDesc"] as? String
        myChatTable.communityDominantColour = communityDetailCommunity["communityDominantColour"] as? String
        myChatTable.communityImageBigThumbUrl = communityDetailCommunity["communityImageBigThumbUrl"] as? String
        myChatTable.communityImageSmallThumbUrl = communityDetailCommunity["communityImageSmallThumbUrl"] as? String
        myChatTable.communityImageUrl = communityDetailCommunity["communityImageUrl"] as? String
        myChatTable.communityJoinedTimestamp = communityDetailCommunity["communityJoinedTimestamp"] as? String
        myChatTable.communityKey = communityDetailCommunity["communityKey"] as? String
        myChatTable.communityName = communityDetailCommunity["communityName"] as? String
        myChatTable.communityScore = communityDetailCommunity["communityScore"] as? String
        myChatTable.communityStatus = communityDetailCommunity["communityStatus"] as? String
        myChatTable.communitySubType = communityDetailCommunity["communitySubType"] as? String
        myChatTable.communityUpdatedTimestamp = communityDetailCommunity["communityUpdatedTimestamp"] as? String
        myChatTable.finalScore = communityDetailCommunity["finalScore"] as? String
        myChatTable.isdeleted = communityDetailCommunity["isDeleted"] as? String
        if let isMember = communityDetailCommunity["isMember"]{
            myChatTable.isMember = String(describing: isMember)
        }
        if let blockedTime = communityDetailCommunity["blockedTime"]{
            myChatTable.blockedTime = String(describing: blockedTime)
        }
        if let logged_user_blocked_timestamp = communityDetailCommunity["logged_user_blocked_timestamp"]{
            myChatTable.logged_user_blocked_timestamp = String(describing: logged_user_blocked_timestamp)
        }else{
            myChatTable.logged_user_blocked_timestamp = StringConstants.EMPTY
        }
        if iSCommingFromOneTOneinitiate == false {
            myChatTable.ownerId = communityDetailCommunity["ownerId"] as? String
        }
        myChatTable.ownerImageUrl = communityDetailCommunity["ownerImageUrl"] as? String
        myChatTable.ownerName = communityDetailCommunity["ownerName"] as? String
        myChatTable.privacy = communityDetailCommunity["privacy"] as? String
       // myChatTable.totalMembers = communityDetailCommunity["totalMembers"] as? String
        if let totalMembers = communityDetailCommunity["totalMembers"] as? Int {
            myChatTable.totalMembers = String(describing: totalMembers )
        }else if let totalMembers = communityDetailCommunity["totalMembers"] as? String {
            myChatTable.totalMembers = totalMembers
        }
        myChatTable.type = communityDetailCommunity["type"] as? String
        myChatTable.messageAt = communityDetailCommunity["messageAt"] as? String ?? "0"
        myChatTable.last_activity_datetime = communityDetailCommunity["last_activity_datetime"] as? String ?? "0"
        myChatTable.communityJabberId = communityDetailCommunity ["communityJabberId"] as? String
        myChatTable.bigThumb = communityDetailCommunity["bigThumb"] as? String
        myChatTable.createddatetime = communityDetailCommunity["createddatetime"] as? String
        if let firstname = communityDetailCommunity["firstname"] as? String{
            myChatTable.firstname = firstname
        }else if let firstname = communityDetailCommunity["owner_first_name"] as? String{
            myChatTable.firstname = firstname
        }
        myChatTable.imageUrl = communityDetailCommunity["imageUrl"] as? String
        myChatTable.lastMessageDatetime = communityDetailCommunity["lastMessageDatetime"] as? String
        if let lastName = communityDetailCommunity["lastName"] as? String{
            myChatTable.lastName = lastName
        }else if let lastName = communityDetailCommunity["owner_last_name"] as? String{
            myChatTable.lastName = lastName
        }
        if let memberCount = communityDetailCommunity["memberCount"] as? Int{
            myChatTable.memberCount = String(memberCount)
        }else if let memberCount = communityDetailCommunity["memberCount"] as? String {
            myChatTable.memberCount = String(memberCount)
        }else{
            myChatTable.memberCount = StringConstants.ZERO
        }
        if let profileStatus = communityDetailCommunity["profileStatus"] as? String{
            myChatTable.profileStatus = profileStatus
        }
        myChatTable.smallThumb = communityDetailCommunity["smallThumb"] as? String
        if let unreadMessagesCount = communityDetailCommunity["unreadMessagesCount"] as? Int {
            myChatTable.unreadMessagesCount = String(describing: unreadMessagesCount )
        }else if let unreadMessagesCount = communityDetailCommunity["unreadMessagesCount"] as? String {
            myChatTable.unreadMessagesCount = String(describing: unreadMessagesCount )
        }
        myChatTable.messageBy = communityDetailCommunity["messageBy"] as? String
        myChatTable.featured = communityDetailCommunity["featured"] as? String
        
        if iSCommingFromOneTOneinitiate == true {
            myChatTable.userId = communityDetailCommunity["ownerId"] as? String
        }else{
            myChatTable.userId = communityDetailCommunity["userId"] as? String
        }
       
        myChatTable.isMuted = communityDetailCommunity["isMuted"] as? String ?? ""
        myChatTable.messageText = communityDetailCommunity["messageText"] as? String
        
        if let lastMessageId = communityDetailCommunity["lastMessageId"] as? Int {
            myChatTable.lastMessageId = String(describing: lastMessageId )
        }else if let lastMessageId = communityDetailCommunity["lastMessageId"] as? String {
            myChatTable.lastMessageId = lastMessageId
        }
        
        if let mediaType = communityDetailCommunity["mediaType"]{
            myChatTable.mediaType = mediaType as? String
        }
        
        if let messageType = communityDetailCommunity["messageType"]{
            myChatTable.messageType = messageType as? String
        }
        if let feed = communityDetailCommunity["feed"] {
            myChatTable.feed  = String(describing:feed)
        }else if let feed = communityDetailCommunity["communityWithRssFeed"]{
            myChatTable.feed  = String(describing:feed)
        }
        
        if let isSponsoredMessage = communityDetailCommunity["isSponsoredMessage"]{
            myChatTable.isSponsoredMessage  = String(describing:isSponsoredMessage)
        }
        
        if let id = communityDetailCommunity["id"] as? String{
            myChatTable.id  = id
        }
        if let lastMessageContentField2 = communityDetailCommunity["lastMessageContentField2"] as? String{
            myChatTable.lastMessageContentField2  = lastMessageContentField2
        }
        if let lastMessageContentField3 = communityDetailCommunity["lastMessageContentField3"] as? String{
            myChatTable.lastMessageContentField3  = lastMessageContentField3
        }
        if let lastMessageContentField4 = communityDetailCommunity["lastMessageContentField4"] as? String{
            myChatTable.lastMessageContentField4  = lastMessageContentField4
        }
        if let lastMessageContentField5 = communityDetailCommunity["lastMessageContentField5"] as? String{
            myChatTable.lastMessageContentField5  = lastMessageContentField5
        }
        if let lastMessageContentField6 = communityDetailCommunity["lastMessageContentField6"] as? String{
            myChatTable.lastMessageContentField6  = lastMessageContentField6
        }
        if let lastMessageContentField7 = communityDetailCommunity["lastMessageContentField7"] as? String{
            myChatTable.lastMessageContentField7  = lastMessageContentField7
        }
        if let lastMessageContentField8 = communityDetailCommunity["lastMessageContentField8"] as? String{
            myChatTable.lastMessageContentField8  = lastMessageContentField8
        }
        if let lastMessageContentField9 = communityDetailCommunity["lastMessageContentField9"] as? String{
            myChatTable.lastMessageContentField9  = lastMessageContentField9
        }
        if let lastessageContentField10 = communityDetailCommunity["lastessageContentField10"] as? String{
            myChatTable.lastessageContentField10  = lastessageContentField10
        }
        if let member = communityDetailCommunity["member"] as? String{
            myChatTable.member = String(member)
        }else{
            myChatTable.member = StringConstants.ZERO
        }
        if let request_callback_id = communityDetailCommunity["request_callback_id"] {
            myChatTable.request_callback_id = request_callback_id as? String
        }
        if let request_callback_time = communityDetailCommunity["request_callback_time"]{
            myChatTable.request_callback_time = request_callback_time as? String
        }

        if let userCommunityJabberId = communityDetailCommunity["userCommunityJabberId"]{
            myChatTable.userCommunityJabberId = userCommunityJabberId as? String
        }/*else{
            if let communutyjabberId = communityDetailCommunity ["communityJabberId"] as? String{
                myChatTable.userCommunityJabberId = communutyjabberId
            }
        }*/
        
        if let userOneValue = communityDetailCommunity["userOne"] {
            myChatTable.userOne = userOneValue as? String
        }
        if let userTwoValue = communityDetailCommunity["userTwo"]{
            myChatTable.userTwo = userTwoValue as? String
        }
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus in core data",saveStatus)
        
        if isFirstTime == true{
          let myChat   = self.convertCommunityDetailsToMyChat(communityDetail:communityDetail)
          let myChatsData = ["myChats": myChat]
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.shaoutMyChatData), object: nil, userInfo: myChatsData)
        }
        
    }
    

    func convertCommunityDetailsToMyChat(communityDetail:Dictionary<String, Any>) -> MyChatsTable {
       
        let communityDetailCommunity = communityDetail
        let myChatTable = MyChatsTable(context:contextTemp)
        myChatTable.communityCategories = communityDetailCommunity["communityCategories"] as? String
        myChatTable.communityCreatedTimestamp = communityDetailCommunity["communityCreatedTimestamp"] as? String
        myChatTable.communityDesc = communityDetailCommunity["communityDesc"] as? String
        myChatTable.communityDominantColour = communityDetailCommunity["communityDominantColour"] as? String
        myChatTable.communityImageBigThumbUrl = communityDetailCommunity["communityImageBigThumbUrl"] as? String
        myChatTable.communityImageSmallThumbUrl = communityDetailCommunity["communityImageSmallThumbUrl"] as? String
        myChatTable.communityImageUrl = communityDetailCommunity["communityImageUrl"] as? String
        myChatTable.communityJoinedTimestamp = communityDetailCommunity["communityJoinedTimestamp"] as? String
        myChatTable.communityKey = communityDetailCommunity["communityKey"] as? String
        myChatTable.communityName = communityDetailCommunity["communityName"] as? String
        myChatTable.communityScore = communityDetailCommunity["communityScore"] as? String
        myChatTable.communityStatus = communityDetailCommunity["communityStatus"] as? String
        myChatTable.communitySubType = communityDetailCommunity["communitySubType"] as? String
        myChatTable.communityUpdatedTimestamp = communityDetailCommunity["communityUpdatedTimestamp"] as? String
        myChatTable.finalScore = communityDetailCommunity["finalScore"] as? String
        myChatTable.isdeleted = communityDetailCommunity["isDeleted"] as? String
        if let isMember = communityDetailCommunity["isMember"]{
            myChatTable.isMember = String(describing:isMember)
        }
        if let blockedTime = communityDetailCommunity["blockedTime"]{
            myChatTable.blockedTime = String(describing: blockedTime)
        }
        if let logged_user_blocked_timestamp = communityDetailCommunity["logged_user_blocked_timestamp"]{
            myChatTable.logged_user_blocked_timestamp = String(describing: logged_user_blocked_timestamp)
        }else{
            myChatTable.logged_user_blocked_timestamp = StringConstants.EMPTY
        }
        myChatTable.ownerId = communityDetailCommunity["ownerId"] as? String
        myChatTable.ownerImageUrl = communityDetailCommunity["ownerImageUrl"] as? String
        myChatTable.ownerName = communityDetailCommunity["ownerName"] as? String
        myChatTable.privacy = communityDetailCommunity["privacy"] as? String
        //myChatTable.totalMembers = communityDetailCommunity["totalMembers"] as? String
        if let totalMembers = communityDetailCommunity["totalMembers"] as? Int {
            myChatTable.totalMembers = String(describing: totalMembers )
        }else if let totalMembers = communityDetailCommunity["totalMembers"] as? String {
            myChatTable.totalMembers = totalMembers
        }
        
        myChatTable.type = communityDetailCommunity["type"] as? String
        myChatTable.messageText = communityDetailCommunity["messageText"] as? String
        myChatTable.lastMessageId = communityDetailCommunity["lastMessageId"] as? String
        
        myChatTable.messageAt = communityDetailCommunity["messageAt"] as? String ?? "0"
        myChatTable.last_activity_datetime = communityDetailCommunity["last_activity_datetime"] as? String ?? "0"

        myChatTable.communityJabberId = communityDetailCommunity ["communityJabberId"] as? String
        myChatTable.bigThumb = communityDetailCommunity["bigThumb"] as? String
        myChatTable.createddatetime = communityDetailCommunity["createddatetime"] as? String
        if let firstname = communityDetailCommunity["firstname"] as? String{
            myChatTable.firstname = firstname
        }else if let firstname = communityDetailCommunity["owner_first_name"] as? String{
            myChatTable.firstname = firstname
        }
        myChatTable.imageUrl = communityDetailCommunity["imageUrl"] as? String
        myChatTable.lastMessageDatetime = communityDetailCommunity["lastMessageDatetime"] as? String
        if let lastName = communityDetailCommunity["lastName"] as? String{
            myChatTable.lastName = lastName
        }else if let lastName = communityDetailCommunity["owner_last_name"] as? String{
            myChatTable.lastName = lastName
        }
        if let memberCount = communityDetailCommunity["memberCount"] {
            myChatTable.memberCount =  String(describing:memberCount) //String(memberCount)
        }else if let memberCount = communityDetailCommunity["memberCount"] as? String {
            myChatTable.memberCount = String(memberCount)
        }else if let memberCount = communityDetailCommunity["totalMembers"] as? Int {
            myChatTable.memberCount = String(memberCount)
        }else{
            myChatTable.memberCount = StringConstants.ZERO
        }

        myChatTable.profileStatus = communityDetailCommunity["profileStatus"] as? String
        myChatTable.smallThumb = communityDetailCommunity["smallThumb"] as? String
        if let unreadMessagesCount = communityDetailCommunity["unreadMessagesCount"] as? Int {
            myChatTable.unreadMessagesCount = String(describing: unreadMessagesCount )
        }else if let unreadMessagesCount = communityDetailCommunity["unreadMessagesCount"] as? String {
            myChatTable.unreadMessagesCount = String(describing: unreadMessagesCount )
        }
        myChatTable.messageBy = communityDetailCommunity["messageBy"] as? String
        myChatTable.featured = communityDetailCommunity["featured"] as? String
        myChatTable.userId = communityDetailCommunity["userId"] as? String
        myChatTable.isMuted = communityDetailCommunity["isMuted"] as? String ?? ""

        if let mediaType = communityDetailCommunity["mediaType"]{
            myChatTable.mediaType = mediaType as? String
        }
        if let messageType = communityDetailCommunity["messageType"]{
            myChatTable.messageType = messageType as? String
        }
 
        //Notificaton filed key & value        
        myChatTable.notificationKey = communityDetailCommunity["notificationKey"] as? String
        myChatTable.sendTo = communityDetailCommunity["sendTo"] as? String
        myChatTable.action = communityDetailCommunity["action"] as? String
        myChatTable.sendAt = communityDetailCommunity["sendAt"] as? String
        myChatTable.sso_id = communityDetailCommunity["sso_id"] as? String
        
        if let feed = communityDetailCommunity["feed"] {
            myChatTable.feed  = String(describing:feed)
        }else if let feed = communityDetailCommunity["communityWithRssFeed"]{
            myChatTable.feed  = String(describing:feed)
        }
        
        if let isSponsoredMessage = communityDetailCommunity["isSponsoredMessage"]{
            myChatTable.isSponsoredMessage  = String(describing:isSponsoredMessage)
        }
        
        if let id = communityDetailCommunity["id"] as? String{
            myChatTable.id  = id
        }
        if let lastMessageContentField2 = communityDetailCommunity["lastMessageContentField2"] as? String{
            myChatTable.lastMessageContentField2  = lastMessageContentField2
        }
        if let lastMessageContentField3 = communityDetailCommunity["lastMessageContentField3"] as? String{
            myChatTable.lastMessageContentField3  = lastMessageContentField3
        }
        if let lastMessageContentField4 = communityDetailCommunity["lastMessageContentField4"] as? String{
            myChatTable.lastMessageContentField4  = lastMessageContentField4
        }
        if let lastMessageContentField5 = communityDetailCommunity["lastMessageContentField5"] as? String{
            myChatTable.lastMessageContentField5  = lastMessageContentField5
        }
        if let lastMessageContentField6 = communityDetailCommunity["lastMessageContentField6"] as? String{
            myChatTable.lastMessageContentField6  = lastMessageContentField6
        }
        if let lastMessageContentField7 = communityDetailCommunity["lastMessageContentField7"] as? String{
            myChatTable.lastMessageContentField7  = lastMessageContentField7
        }
        if let lastMessageContentField8 = communityDetailCommunity["lastMessageContentField8"] as? String{
            myChatTable.lastMessageContentField8  = lastMessageContentField8
        }
        if let lastMessageContentField9 = communityDetailCommunity["lastMessageContentField9"] as? String{
            myChatTable.lastMessageContentField9  = lastMessageContentField9
        }
        if let lastessageContentField10 = communityDetailCommunity["lastessageContentField10"] as? String{
            myChatTable.lastessageContentField10  = lastessageContentField10
        }
        if let userCommunityJabberId = communityDetailCommunity["userCommunityJabberId"]{
            myChatTable.userCommunityJabberId = userCommunityJabberId as? String
        }/*else{
            if let communutyjabberId = communityDetailCommunity ["communityJabberId"] as? String{
              myChatTable.userCommunityJabberId = communutyjabberId
            }
        }*/
        if let member = communityDetailCommunity["member"] as? String{
            myChatTable.member = String(member)
        }else{
            myChatTable.member = StringConstants.ZERO
        }

        if let userOneValue = communityDetailCommunity["userOne"] {
            myChatTable.userOne = userOneValue as? String
        }
        if let userTwoValue = communityDetailCommunity["userTwo"]{
            myChatTable.userTwo = userTwoValue as? String
        }
        
        //Added New
        if let defaultValue = communityDetailCommunity["defaultValue"] {
            myChatTable.defaultValue = defaultValue as? String
        }else{
            myChatTable.defaultValue = StringConstants.EMPTY
        }
        
        if let descriptionValue = communityDetailCommunity["descriptionValue"] {
            myChatTable.descriptionValue = descriptionValue as? String
        }else{
            myChatTable.descriptionValue = StringConstants.EMPTY
        }
        
        if let loggedUserJoinedDateTime = communityDetailCommunity["loggedUserJoinedDateTime"] {
            myChatTable.loggedUserJoinedDateTime = loggedUserJoinedDateTime as? String
        }else{
            myChatTable.loggedUserJoinedDateTime = StringConstants.EMPTY
        }
        
        if let address = communityDetailCommunity["address"] {
            myChatTable.address = address as? String
        }else{
            myChatTable.address = StringConstants.EMPTY
        }
        
        if let alloted_stall_number = communityDetailCommunity["alloted_stall_number"] {
            myChatTable.alloted_stall_number = alloted_stall_number as? String
        }else{
            myChatTable.alloted_stall_number = StringConstants.EMPTY
        }
        
        if let company_website = communityDetailCommunity["company_website"] {
            myChatTable.company_website = company_website as? String
        }else{
            myChatTable.company_website = StringConstants.EMPTY
        }
        
        if let contact_person_designation = communityDetailCommunity["contact_person_designation"] {
            myChatTable.contact_person_designation = contact_person_designation as? String
        }else{
            myChatTable.contact_person_designation = StringConstants.EMPTY
        }
        
        if let contact_person_first_name = communityDetailCommunity["contact_person_first_name"] {
            myChatTable.contact_person_first_name = contact_person_first_name as? String
        }else{
            myChatTable.contact_person_first_name = StringConstants.EMPTY
        }
        
        if let contact_person_last_name = communityDetailCommunity["contact_person_last_name"] {
            myChatTable.contact_person_last_name = contact_person_last_name as? String
        }else{
            myChatTable.contact_person_last_name = StringConstants.EMPTY
        }
        
        if let pavilion_name = communityDetailCommunity["pavilion_name"] {
            myChatTable.pavilion_name = pavilion_name as? String
        }else{
            myChatTable.pavilion_name = StringConstants.EMPTY
        }
        
        if let stall_id = communityDetailCommunity["stall_id"] {
            myChatTable.stall_id = stall_id as? String
        }else{
            myChatTable.stall_id = StringConstants.EMPTY
        }
      
        if let event_code = communityDetailCommunity["event_code"] {
            myChatTable.event_code = event_code as? String
        }else{
            myChatTable.event_code = StringConstants.EMPTY
        }
    
        if let request_callback_id = communityDetailCommunity["request_callback_id"] {
            myChatTable.request_callback_id = request_callback_id as? String
        }
        if let request_callback_time = communityDetailCommunity["request_callback_time"]{
            myChatTable.request_callback_time = request_callback_time as? String
        }
        

        return myChatTable
    }
    
    //Other Than Get Mychat Api
    func updateAtMyChatTable(communityDetail:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                let communityDetilsData = communityDetail
                print("communityDetilsData",communityDetilsData)
                let communityKey = communityDetilsData["communityKey"] as? String
                if task.communityKey == communityKey {
                    task.communityCategories = communityDetilsData["communityCategories"] as? String
                    task.communityCreatedTimestamp = communityDetilsData["communityCreatedTimestamp"] as? String
                    task.communityDesc = communityDetilsData["communityDesc"] as? String
                    task.communityDominantColour = communityDetilsData["communityDominantColour"] as? String
                    task.communityImageBigThumbUrl = communityDetilsData["communityImageBigThumbUrl"] as? String
                    task.communityImageSmallThumbUrl = communityDetilsData["communityImageSmallThumbUrl"] as? String
                    task.communityImageUrl = communityDetilsData["communityImageUrl"] as? String
                    task.communityJoinedTimestamp = communityDetilsData["communityJoinedTimestamp"] as? String
                    task.communityName = communityDetilsData["communityName"] as? String
                    task.communityScore = communityDetilsData["communityScore"] as? String
                    task.communityStatus = communityDetilsData["communityStatus"] as? String
                    task.communitySubType = communityDetilsData["communitySubType"] as? String
                    task.communityUpdatedTimestamp = communityDetilsData["communityUpdatedTimestamp"] as? String
                    task.finalScore = communityDetilsData["finalScore"] as? String
                    task.isdeleted = communityDetilsData["isDeleted"] as? String
                    //task.isMember = String(describing: communityDetilsData["isMember"]!) //communityDetilsData["isMember"] as? String
                    if communityDetilsData["isMember"] != nil{
                        task.isMember = String(describing: communityDetilsData["isMember"]!)
                    }
                    if let blockedTime = communityDetilsData["blockedTime"]{
                        task.blockedTime = String(describing: blockedTime)
                    }
                    if let logged_user_blocked_timestamp = communityDetilsData["logged_user_blocked_timestamp"]{
                        task.logged_user_blocked_timestamp = String(describing: logged_user_blocked_timestamp)
                    }else{
                        task.logged_user_blocked_timestamp = StringConstants.EMPTY
                    }
                    task.ownerId = communityDetilsData["ownerId"] as? String
                    task.ownerImageUrl = communityDetilsData["ownerImageUrl"] as? String
                    task.ownerName = communityDetilsData["ownerName"] as? String
                    task.privacy = communityDetilsData["privacy"] as? String
                    
                    if communityDetilsData["totalMembers"] == nil || communityDetilsData["totalMembers"] is NSNull {
                        print("No update totalMembers value")
                    }else{
                        task.totalMembers = communityDetilsData["totalMembers"] as? String
                    }
                    task.type = communityDetilsData["type"] as? String
                    task.messageText = communityDetilsData["messageText"] as? String
                    task.lastMessageId = communityDetilsData["lastMessageId"] as? String
                    task.messageAt = communityDetilsData["messageAt"] as? String ?? "0"
                    task.last_activity_datetime = communityDetilsData["last_activity_datetime"] as? String ?? "0"
                    task.communityJabberId = communityDetilsData ["communityJabberId"] as? String
                    task.bigThumb = communityDetilsData["bigThumb"] as? String
                    task.createddatetime = communityDetilsData["createddatetime"] as? String
                    if let firstname = communityDetilsData["firstname"] as? String{
                        task.firstname = firstname
                    }else if let firstname = communityDetilsData["owner_first_name"] as? String{
                        task.firstname = firstname
                    }
                    task.imageUrl = communityDetilsData["imageUrl"] as? String
                    task.lastMessageDatetime = communityDetilsData["lastMessageDatetime"] as? String
                    if let lastName = communityDetilsData["lastName"] as? String{
                        task.lastName = lastName
                    }else if let lastName = communityDetilsData["owner_last_name"] as? String{
                        task.lastName = lastName
                    }
                    if let memberCount = communityDetilsData["memberCount"] as? Int{
                        task.memberCount = String(memberCount)
                    }else if let memberCount = communityDetilsData["memberCount"] as? String {
                        task.memberCount = String(memberCount)
                    }else{
                        task.memberCount = StringConstants.ZERO
                    }
                    task.profileStatus = communityDetilsData["profileStatus"] as? String
                    task.smallThumb = communityDetilsData["smallThumb"] as? String
                    if let unreadMessagesCount = communityDetilsData["unreadMessagesCount"] as? Int {
                        task.unreadMessagesCount = String(describing: unreadMessagesCount )
                    }else if let unreadMessagesCount = communityDetilsData["unreadMessagesCount"] as? String {
                        task.unreadMessagesCount = String(describing: unreadMessagesCount )
                    }
                    task.messageBy = communityDetilsData["messageBy"] as? String
                    task.featured = communityDetilsData["featured"] as? String
                    task.userId = communityDetilsData["userId"] as? String
                    task.isMuted = communityDetilsData["isMuted"] as? String ?? ""
                    
                    if let mediaType = communityDetilsData["mediaType"]{
                        task.mediaType = mediaType as? String
                    }
                    
                    if let messageType = communityDetilsData["messageType"]{
                        task.messageType = messageType as? String
                    }
                    
                    if let feed = communityDetilsData["feed"] {
                       task.feed  = String(describing:feed)
                    }else if let feed = communityDetilsData["communityWithRssFeed"]{
                        task.feed  = String(describing:feed)
                    }
                    
                    if let isSponsoredMessage = communityDetilsData["isSponsoredMessage"]{
                        task.isSponsoredMessage  = String(describing:isSponsoredMessage)
                    }
                    
                    if let id = communityDetilsData["id"] as? String{
                        task.id  = id
                    }
                    if let lastMessageContentField2 = communityDetilsData["lastMessageContentField2"] as? String{
                        task.lastMessageContentField2  = lastMessageContentField2
                    }
                    if let lastMessageContentField3 = communityDetilsData["lastMessageContentField3"] as? String{
                        task.lastMessageContentField3  = lastMessageContentField3
                    }
                    if let lastMessageContentField4 = communityDetilsData["lastMessageContentField4"] as? String{
                        task.lastMessageContentField4  = lastMessageContentField4
                    }
                    if let lastMessageContentField5 = communityDetilsData["lastMessageContentField5"] as? String{
                        task.lastMessageContentField5  = lastMessageContentField5
                    }
                    if let lastMessageContentField6 = communityDetilsData["lastMessageContentField6"] as? String{
                        task.lastMessageContentField6  = lastMessageContentField6
                    }
                    if let lastMessageContentField7 = communityDetilsData["lastMessageContentField7"] as? String{
                        task.lastMessageContentField7  = lastMessageContentField7
                    }
                    if let lastMessageContentField8 = communityDetilsData["lastMessageContentField8"] as? String{
                        task.lastMessageContentField8  = lastMessageContentField8
                    }
                    if let lastMessageContentField9 = communityDetilsData["lastMessageContentField9"] as? String{
                        task.lastMessageContentField9  = lastMessageContentField9
                    }
                    if let lastessageContentField10 = communityDetilsData["lastessageContentField10"] as? String{
                        task.lastessageContentField10  = lastessageContentField10
                    }
                    if let member = communityDetilsData["member"] as? String {
                        task.member =  String(member)
                    }else{
                        task.member = StringConstants.ZERO
                    }

                    if let userCommunityJabberId = communityDetilsData["userCommunityJabberId"]{
                        task.userCommunityJabberId = userCommunityJabberId as? String
                    }/*else{
                        if let communutyjabberId = communityDetilsData ["communityJabberId"] as? String{
                            task.userCommunityJabberId = communutyjabberId
                        }
                    }*/
                    if let userOneValue = communityDetilsData["userOne"] {
                        task.userOne = userOneValue as? String
                    }
                    if let userTwoValue = communityDetilsData["userTwo"]{
                        task.userTwo = userTwoValue as? String
                    }
                    if let request_callback_id = communityDetilsData["request_callback_id"] {
                        task.request_callback_id = request_callback_id as? String
                    }
                    if let request_callback_time = communityDetilsData["request_callback_time"]{
                        task.request_callback_time = request_callback_time as? String
                    }

                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)

    }
    
    //THIS METHOD FOR GetMyChat API TO UPDATE MYCAHT FOR ISSOPNSERD VALUE 0

    func updateAtMyChatTableForiSSponsoredValueZero(communityDetail:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                let communityDetilsData = communityDetail
                print("communityDetilsData",communityDetilsData)
                let communityKey = communityDetilsData["communityKey"] as? String
                if task.communityKey == communityKey && task.isSponsoredMessage == StringConstants.ZERO {
                    task.communityCategories = communityDetilsData["communityCategories"] as? String
                    task.communityCreatedTimestamp = communityDetilsData["communityCreatedTimestamp"] as? String
                    task.communityDesc = communityDetilsData["communityDesc"] as? String
                    task.communityDominantColour = communityDetilsData["communityDominantColour"] as? String
                    task.communityImageBigThumbUrl = communityDetilsData["communityImageBigThumbUrl"] as? String
                    task.communityImageSmallThumbUrl = communityDetilsData["communityImageSmallThumbUrl"] as? String
                    task.communityImageUrl = communityDetilsData["communityImageUrl"] as? String
                    task.communityJoinedTimestamp = communityDetilsData["communityJoinedTimestamp"] as? String
                    task.communityName = communityDetilsData["communityName"] as? String
                    task.communityScore = communityDetilsData["communityScore"] as? String
                    task.communityStatus = communityDetilsData["communityStatus"] as? String
                    task.communitySubType = communityDetilsData["communitySubType"] as? String
                    task.communityUpdatedTimestamp = communityDetilsData["communityUpdatedTimestamp"] as? String
                    task.finalScore = communityDetilsData["finalScore"] as? String
                    task.isdeleted = communityDetilsData["isDeleted"] as? String
                    //task.isMember = String(describing: communityDetilsData["isMember"]!) //communityDetilsData["isMember"] as? String
                    if communityDetilsData["isMember"] != nil{
                        task.isMember = String(describing: communityDetilsData["isMember"]!)
                    }
                    task.ownerId = communityDetilsData["ownerId"] as? String
                    task.ownerImageUrl = communityDetilsData["ownerImageUrl"] as? String
                    task.ownerName = communityDetilsData["ownerName"] as? String
                    task.privacy = communityDetilsData["privacy"] as? String
                    
                    if communityDetilsData["totalMembers"] == nil || communityDetilsData["totalMembers"] is NSNull {
                        print("No update totalMembers value")
                    }else{
                        task.totalMembers = communityDetilsData["totalMembers"] as? String
                    }
                    task.type = communityDetilsData["type"] as? String
                    task.messageText = communityDetilsData["messageText"] as? String
                    task.lastMessageId = communityDetilsData["lastMessageId"] as? String
                    task.messageAt = communityDetilsData["messageAt"] as? String ?? "0"
                    task.last_activity_datetime = communityDetilsData["last_activity_datetime"] as? String ?? "0"
                    task.communityJabberId = communityDetilsData ["communityJabberId"] as? String
                    task.bigThumb = communityDetilsData["bigThumb"] as? String
                    task.createddatetime = communityDetilsData["createddatetime"] as? String
                    if let firstname = communityDetilsData["firstname"] as? String{
                        task.firstname = firstname
                    }else if let firstname = communityDetilsData["owner_first_name"] as? String{
                        task.firstname = firstname
                    }
                    task.imageUrl = communityDetilsData["imageUrl"] as? String
                    task.lastMessageDatetime = communityDetilsData["lastMessageDatetime"] as? String
                    if let lastName = communityDetilsData["lastName"] as? String{
                        task.lastName = lastName
                    }else if let lastName = communityDetilsData["owner_last_name"] as? String{
                        task.lastName = lastName
                    }
                    if let memberCount = communityDetilsData["memberCount"] as? Int{
                        task.memberCount = String(memberCount)
                    }else if let memberCount = communityDetilsData["memberCount"] as? String {
                        task.memberCount = String(memberCount)
                    }else{
                        task.memberCount = StringConstants.ZERO
                    }
                    task.profileStatus = communityDetilsData["profileStatus"] as? String
                    task.smallThumb = communityDetilsData["smallThumb"] as? String
                    if let unreadMessagesCount = communityDetilsData["unreadMessagesCount"] as? Int {
                        task.unreadMessagesCount = String(describing: unreadMessagesCount )
                    }else if let unreadMessagesCount = communityDetilsData["unreadMessagesCount"] as? String {
                        task.unreadMessagesCount = String(describing: unreadMessagesCount )
                    }
                    task.messageBy = communityDetilsData["messageBy"] as? String
                    task.featured = communityDetilsData["featured"] as? String
                    task.userId = communityDetilsData["userId"] as? String
                    task.isMuted = communityDetilsData["isMuted"] as? String ?? ""
                    
                    if let mediaType = communityDetilsData["mediaType"]{
                        task.mediaType = mediaType as? String
                    }
                    
                    if let messageType = communityDetilsData["messageType"]{
                        task.messageType = messageType as? String
                    }
                    
                    if let feed = communityDetilsData["feed"] {
                        task.feed  = String(describing:feed)
                    }else if let feed = communityDetilsData["communityWithRssFeed"]{
                        task.feed  = String(describing:feed)
                    }
                    
                    if let isSponsoredMessage = communityDetilsData["isSponsoredMessage"]{
                        task.isSponsoredMessage  = String(describing:isSponsoredMessage)
                    }
                    
                    if let id = communityDetilsData["id"] as? String{
                        task.id  = id
                    }
                    if let lastMessageContentField2 = communityDetilsData["lastMessageContentField2"] as? String{
                        task.lastMessageContentField2  = lastMessageContentField2
                    }
                    if let lastMessageContentField3 = communityDetilsData["lastMessageContentField3"] as? String{
                        task.lastMessageContentField3  = lastMessageContentField3
                    }
                    if let lastMessageContentField4 = communityDetilsData["lastMessageContentField4"] as? String{
                        task.lastMessageContentField4  = lastMessageContentField4
                    }
                    if let lastMessageContentField5 = communityDetilsData["lastMessageContentField5"] as? String{
                        task.lastMessageContentField5  = lastMessageContentField5
                    }
                    if let lastMessageContentField6 = communityDetilsData["lastMessageContentField6"] as? String{
                        task.lastMessageContentField6  = lastMessageContentField6
                    }
                    if let lastMessageContentField7 = communityDetilsData["lastMessageContentField7"] as? String{
                        task.lastMessageContentField7  = lastMessageContentField7
                    }
                    if let lastMessageContentField8 = communityDetilsData["lastMessageContentField8"] as? String{
                        task.lastMessageContentField8  = lastMessageContentField8
                    }
                    if let lastMessageContentField9 = communityDetilsData["lastMessageContentField9"] as? String{
                        task.lastMessageContentField9  = lastMessageContentField9
                    }
                    if let lastessageContentField10 = communityDetilsData["lastessageContentField10"] as? String{
                        task.lastessageContentField10  = lastessageContentField10
                    }
                    if let member = communityDetilsData["member"] as? String {
                        task.member =  String(member)
                    }else{
                        task.member = StringConstants.ZERO
                    }
                    
                    if let userCommunityJabberId = communityDetilsData["userCommunityJabberId"]{
                        task.userCommunityJabberId = userCommunityJabberId as? String
                    }/*else{
                     if let communutyjabberId = communityDetilsData ["communityJabberId"] as? String{
                     task.userCommunityJabberId = communutyjabberId
                     }
                     }*/
                    if let userOneValue = communityDetilsData["userOne"] {
                        task.userOne = userOneValue as? String
                    }
                    if let userTwoValue = communityDetilsData["userTwo"]{
                        task.userTwo = userTwoValue as? String
                    }
                    
                    if let request_callback_id = communityDetilsData["request_callback_id"] {
                        task.request_callback_id = request_callback_id as? String
                    }
                    if let request_callback_time = communityDetilsData["request_callback_time"]{
                        task.request_callback_time = request_callback_time as? String
                    }

                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
        if saveStatus == true {
            let myChat   = self.convertCommunityDetailsToMyChat(communityDetail:communityDetail)
            let myChatsData = ["myChats": myChat]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.shaoutMyChatData), object: nil, userInfo: myChatsData)
        }
    }
    
    //THIS METHOD FOR GetMyChat API TO UPDATE MYCAHT FOR ISSOPNSERD VALUE 1
    func updateAtOnlyUIONMyChatTableForiSSponsoredValueOne(communityDetail:Dictionary<String, Any>){
        let myChat   = self.convertCommunityDetailsToMyChat(communityDetail:communityDetail)
        let myChatsData = ["myChats": myChat]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.shaoutMyChatData), object: nil, userInfo: myChatsData)
    }

    //Update latest mychat object with condition for sponsored and normal message
   
    func updateMyChatTableForSponsoredMessages(latestMsg:String,latestMessageTime:String,communityKey:String,isMember:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey  && task.isSponsoredMessage == StringConstants.ZERO{
                    task.messageText = latestMsg
                    task.messageAt = latestMessageTime
                    task.last_activity_datetime = latestMessageTime
                    task.isMember = isMember
                }else if task.communityKey == communityKey  && task.isSponsoredMessage == StringConstants.ONE{
                    task.isMember = isMember
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    //Update Latest Message
    
    func updateLatestMsgAtMyChatTable(latestMsg:String,latestMessageTime:String,communityKey:String,mediaType:String,messageType:String,lastMessageId:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey  && task.isSponsoredMessage != StringConstants.ONE{
                    task.messageText = latestMsg
                    task.messageAt = latestMessageTime
                    task.mediaType = mediaType
                    task.messageType = messageType
                    task.lastMessageId = lastMessageId
                    task.last_activity_datetime = latestMessageTime
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    // Pavan* - CheckMessageExistInMyChatTable
    
    func isMessageExistInMyChatByCommunityKey(communityKey:String,messageId:String) {
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey && task.lastMessageId == messageId {
                    var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKey(communityKey: communityKey)
                    messages.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                    if messages.count > 0{
                        let message = messages.last
                        task.messageText = message?.contentField1
                        task.messageAt = message?.messageStatusTime
                        task.mediaType = message?.mediaType
                        task.messageType = message?.messageType
                        task.lastMessageId = message?.id
                    }
                    break
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
        
        //Used this notification center on Dashboard Screen
        let notificationName = Notification.Name(StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnDashboard)
        // Post notification
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    func fetchFirstMessageOnCommunityKey(communityKey:String) {
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey  {
                    var messages = MessagesServices.sharedInstance.getMyChatDetailsOnCommunityKey(communityKey: communityKey)
                    messages.sort(by:{$0.createdDatetime! < $1.createdDatetime!} )
                    if messages.count > 0{
                        let message = messages.last
                        task.messageText = message?.contentField1
                        task.messageAt = message?.messageStatusTime
                        task.mediaType = message?.mediaType
                        task.messageType = message?.messageType
                        task.lastMessageId = message?.id
                    }
                    break
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
        
        //Used this notification center on Dashboard Screen
        let notificationName = Notification.Name(StringConstants.pushNotificationConstants.blockUnBlockRemoveMemberPushNotificationCenterOnDashboard)
        // Post notification
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    //Update unreadMessagesCount
    func unreadMessagesCountAtMyChatTable(unreadMessagesCount:String,communityKey:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey {
                    task.unreadMessagesCount = unreadMessagesCount
                    task.communityKey = communityKey
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    
    // Get getMyChatDetails
    func getMyChatDetails() -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    
    //Fetch login user admin channels
    func fetchLoginUserAdminChannelList(ownerId:String) -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchLogedInUserAdminChannel(ownerId:ownerId))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // Get getMyChatDetailsOn ChannelDashboard for OneToOneChat
    func getOneToOneMyChatDetails(communityKey:String) -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequestForOneToOneMyChatOnChannelDashboard(communityKey:communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    

    // Get My chat data show on "ShowUserProfile Screen"
    
    func getIsMemberTypeTrueMyChatDetails() -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchisMemberTypeTrueMyChatData())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    // Check communityKey presentLocalD from local db
    func checkMyChatCommunityKey(communityKey:String) -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchMessageCommunityKey(communityKey: communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    //Check communityKey and isSponserd false
    func checkMychatCommunityKeyAndiSSponserdFalse(communityKey:String,isSponsoredMessage:String) -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchMychatCommunityKeyAndiSSponserdFalse(communityKey:communityKey,isSponsoredMessage:isSponsoredMessage))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    //*****
    //Check communityKey and isSponserd true  fetchMychatCommunityKeyAndiSSponserdTrue
    func checkMychatCommunityKeyAndiSSponserdTrue(communityKey:String,isSponsoredMessage:String,id:String) -> [MyChatsTable] {
        do {
            tasks = try context.fetch(MyChatsTable.fetchMychatCommunityKeyAndiSSponserdTrue(communityKey:communityKey,isSponsoredMessage:isSponsoredMessage, id: id))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }

    
    // Make status as isMember true or false for  follow channel and leave channel
    func updateisMemberFromMyChatTbl(isMemberValue:String,communityKey:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey {
                    task.isMember = isMemberValue
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus",saveStatus)
    }
    
    
    
    func updatemessageIdForSponsoredMessageCallmefunctionality(id:String,communityKey:String,messageId:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey && task.isSponsoredMessage == StringConstants.ONE && task.id == messageId{
                    task.request_callback_id =  id
                    task.request_callback_time = String(DateTimeHelper.getCurrentMillis())
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus",saveStatus)
    }
    
    
    // Make status as isMember true or false for  follow channel and leave channel
    func updatemessageIdForSponsoredMessageCallmefunctionality(id:String,communityKey:String){
        do {
            tasks = try context.fetch(MyChatsTable.fetchRequest()) as! [MyChatsTable]
            for task in tasks {
                if task.communityKey == communityKey && task.isSponsoredMessage == StringConstants.ONE{
                    task.id =  id
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus",saveStatus)
    }

    
    //  Get MyChat Data on isMember field is true
    
    func getCommunityKeyOfisMemberTypeTrue(communityKey:String) -> [MyChatsTable] {
        
        do {
            tasks = try context.fetch(MyChatsTable.fetchCommunityKeyOfisMemberTypeTrue(communityKey: communityKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        
        return tasks
    }
    
}
