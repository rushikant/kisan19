//
//  MyChatsTable+CoreDataProperties.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//
//

import Foundation
import CoreData

extension MyChatsTable {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyChatsTable> {
//         return NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
       // let predicate1 = NSPredicate(format: "communitySubType == nil")
        let predicate1 = NSPredicate(format: "featured == nil")
        fetchRequest.predicate = predicate1
        return fetchRequest
    }
    
    //fetch mychat on channeldashboard for OneToOne
     @nonobjc public class func fetchRequestForOneToOneMyChatOnChannelDashboard(communityKey:String) -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let communityKeyNew = communityKey + "_"
        let predicate = NSPredicate(format: "communityJabberId contains[c] %@", communityKeyNew)
       // let sectionSortDescriptor = NSSortDescriptor(key: "last_activity_datetime", ascending: false)
       // let sortDescriptors = [sectionSortDescriptor]
       // fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    @nonobjc public class func fetchMessageCommunityKey(communityKey:String) -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let predicate = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        fetchRequest.predicate = predicate
        return fetchRequest
    }
    
    //Check communityKey and isSponserd false
    @nonobjc public class func fetchMychatCommunityKeyAndiSSponserdFalse(communityKey:String,isSponsoredMessage:String) -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let predicate1 = NSPredicate(format: "communityKey LIKE[c] %@", communityKey)
        let predicate2 = NSPredicate(format: "isSponsoredMessage LIKE[c] %@", isSponsoredMessage)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    //Check communityKey and isSponserd true
    @nonobjc public class func fetchMychatCommunityKeyAndiSSponserdTrue(communityKey:String,isSponsoredMessage:String,id:String) -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let predicate1 = NSPredicate(format: "communityKey = %@", communityKey)
        let predicate2 = NSPredicate(format: "isSponsoredMessage = %@", isSponsoredMessage)
        let predicate3 = NSPredicate(format: "id = %@", id)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2,predicate3])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    
    @nonobjc public class func fetchCommunityKeyOfisMemberTypeTrue(communityKey:String) -> NSFetchRequest<MyChatsTable> {
        //return NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        //let predicate = NSPredicate(format: "communitySubType LIKE[c] %@","c_OneToOne")
        //let predicate = NSPredicate(format: "communitySubType.length > 0")
        let predicate1 = NSPredicate(format: "isMember LIKE[c] %@","true")
        let predicate2 = NSPredicate(format: "communityKey contains[c] %@", communityKey)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    @nonobjc public class func fetchisMemberTypeTrueMyChatData() -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let predicate = NSPredicate(format: "isMember LIKE[c] %@",StringConstants.ONE)
        let predicate2 = NSPredicate(format: "isSponsoredMessage LIKE[c] %@",StringConstants.ZERO)
        let predicate1 = NSPredicate(format: "featured == nil")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicate,predicate1,predicate2])
        fetchRequest.predicate = predicateCompound
        return fetchRequest
    }
    
    //Fetch login user admin channels
    @nonobjc public class func fetchLogedInUserAdminChannel(ownerId:String) -> NSFetchRequest<MyChatsTable> {
        let fetchRequest = NSFetchRequest<MyChatsTable>(entityName: "MyChatsTable")
        let predicate = NSPredicate(format: "ownerId LIKE[c] %@", ownerId)
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var communityKey: String?
    @NSManaged public var communityCreatedTimestamp: String?
    @NSManaged public var communityJoinedTimestamp: String?
    @NSManaged public var communityLeftTimestamp: String?
    @NSManaged public var communityDeletedTimestamp: String?
    @NSManaged public var communityStatus: String?
    @NSManaged public var featured: String?
    @NSManaged public var communityCategories: String?
    @NSManaged public var communityName: String?
    @NSManaged public var communityDesc: String?
    @NSManaged public var ownerId: String?
    @NSManaged public var ownerImageUrl: String?
    @NSManaged public var ownerName: String?
    @NSManaged public var communityImageUrl: String?
    @NSManaged public var communityImageSmallThumbUrl: String?
    @NSManaged public var communityImageBigThumbUrl: String?
    @NSManaged public var isdeleted: String?
    @NSManaged public var type: String?
    @NSManaged public var privacy: String?
    @NSManaged public var totalMembers: String?
    @NSManaged public var isMember: String?
    @NSManaged public var messageText: String?
    @NSManaged public var messageAt: String?
    @NSManaged public var messageBy: String?
    @NSManaged public var messageType: String?
    @NSManaged public var mediaType: String?
    @NSManaged public var userId: String?
    @NSManaged public var userOne: String?
    @NSManaged public var userTwo: String?
    @NSManaged public var communityDominantColour: String?
    @NSManaged public var communitySubType: String?
    @NSManaged public var messageCount: String?
    @NSManaged public var communityScore: String?
    @NSManaged public var communityUpdatedTimestamp: String?
    @NSManaged public var finalScore: String?
    @NSManaged public var communityJabberId: String?
    @NSManaged public var bigThumb: String?
    @NSManaged public var createddatetime: String?
    @NSManaged public var firstname: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var lastMessageDatetime: String?
    @NSManaged public var lastName: String?
    @NSManaged public var memberCount: String?
    @NSManaged public var profileStatus: String?
    @NSManaged public var smallThumb: String?
    @NSManaged public var unreadMessagesCount: String?
    @NSManaged public var userCommunityJabberId: String?
    @NSManaged public var feed: String?
    @NSManaged public var notificationKey: String?
    @NSManaged public var sendTo: String?
    @NSManaged public var action: String?
    @NSManaged public var sendAt: String?
    @NSManaged public var sso_id: String?
    @NSManaged public var isMuted: String?
    @NSManaged public var lastMessageId: String?
    
    @NSManaged public var defaultValue: String?
    @NSManaged public var descriptionValue: String?
    @NSManaged public var loggedUserJoinedDateTime: String?
    @NSManaged public var address: String?
    @NSManaged public var alloted_stall_number: String?
    @NSManaged public var company_website: String?
    @NSManaged public var contact_person_designation: String?
    @NSManaged public var contact_person_first_name: String?
    @NSManaged public var contact_person_last_name: String?
    @NSManaged public var pavilion_name: String?
    @NSManaged public var stall_id: String?
    @NSManaged public var event_code: String?
    @NSManaged public var last_activity_datetime: String?
    @NSManaged public var isSponsoredMessage: String?
    @NSManaged public var id: String?
    @NSManaged public var lastMessageContentField2: String?
    @NSManaged public var lastMessageContentField3: String?
    @NSManaged public var lastMessageContentField4: String?
    @NSManaged public var lastMessageContentField5: String?
    @NSManaged public var lastMessageContentField6: String?
    @NSManaged public var lastMessageContentField7: String?
    @NSManaged public var lastMessageContentField8: String?
    @NSManaged public var lastMessageContentField9: String?
    @NSManaged public var lastessageContentField10: String?
    @NSManaged public var member: String?
    @NSManaged public var blockedTime: String?
    @NSManaged public var logged_user_blocked_timestamp: String?
    @NSManaged public var request_callback_id: String?
    @NSManaged public var request_callback_time: String?

}
