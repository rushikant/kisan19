//
//  NotificationServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 02/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class NotificationServices{
    static let sharedInstance = NotificationServices()
    var tasks = [NotificationTable]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func sendNotiAtNotificationTable(notficationDetails:Dictionary<String, Any>){
        let notficationDetailsValue = notficationDetails
        let notficationDetailsTable = NotificationTable(context:context)
        notficationDetailsTable.notificationKey = notficationDetailsValue["notificationKey"] as? String
        notficationDetailsTable.communityImageUrl = notficationDetailsValue["communityImageUrl"] as? String
        notficationDetailsTable.communityJabberId = notficationDetailsValue["communityJabberId"] as? String
        notficationDetailsTable.communityKey = notficationDetailsValue["communityKey"] as? String
        notficationDetailsTable.action = notficationDetailsValue["action"] as? String
        notficationDetailsTable.communityName = notficationDetailsValue["communityName"] as? String
        notficationDetailsTable.sendAt = notficationDetailsValue["sendAt"] as? String
        notficationDetailsTable.sso_id = notficationDetailsValue["sso_id"] as? String
        notficationDetailsTable.communityCategories = notficationDetailsValue["communityCategories"] as? String
        notficationDetailsTable.communityDesc = notficationDetailsValue["communityDesc"] as? String
        notficationDetailsTable.totalMembers = notficationDetailsValue["totalMembers"] as? String
        notficationDetailsTable.communityWithRssFeed = notficationDetailsValue["communityWithRssFeed"] as? String
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("saveStatus in core data",saveStatus)
    }
    
    
    
    func updateNotiAtNotificationTable(notficationDetails:Dictionary<String, Any>){
        do {
            tasks = try context.fetch(NotificationTable.fetchRequest()) as! [NotificationTable]
            for task in tasks {
                let notficationDetailsData = notficationDetails
                let notificationKey = notficationDetailsData["notificationKey"] as? String
                if task.notificationKey == notificationKey {
                    task.notificationKey = notficationDetailsData["notificationKey"] as? String
                    task.communityImageUrl = notficationDetailsData["communityImageUrl"] as? String
                    task.communityJabberId = notficationDetailsData["communityJabberId"] as? String
                    task.communityKey = notficationDetailsData["communityKey"] as? String
                    task.action = notficationDetailsData["action"] as? String
                    task.communityName = notficationDetailsData["communityName"] as? String
                    task.sendAt = notficationDetailsData["sendAt"] as? String
                    task.sso_id = notficationDetailsData["sso_id"] as? String
                    task.communityCategories = notficationDetailsData["communityCategories"] as? String
                    task.communityDesc = notficationDetailsData["communityDesc"] as? String
                    task.totalMembers = notficationDetailsData["totalMembers"] as? String
                    task.communityWithRssFeed = notficationDetailsData["communityWithRssFeed"] as? String
                }
            }
        }catch {
            print("Error fetching data from CoreData")
        }
        
        let saveStatus = (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("updated in core data",saveStatus)
    }
    
    //Delete notification
    func deleteNotiAtNotificationTable(notficationDetails:NotificationTable){
        let notificationKey = notficationDetails.notificationKey
        let fetchRequest = NSFetchRequest<NotificationTable>(entityName: "NotificationTable")
        let predicate = NSPredicate(format: "notificationKey LIKE[c] %@",notificationKey! )
        fetchRequest.predicate = predicate
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save()
        } catch {
            print("Error fetching data from CoreData")
        }
    }
    
    // Get getMembersDetails
    func getNotificationDetails() -> [NotificationTable] {
        do {
            tasks = try context.fetch(NotificationTable.fetchRequest())
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
    
    // Check notificationKey presentLocalD from local db
    func checkNotificationKey(notificationKey:String) -> [NotificationTable] {
        do {
            tasks = try context.fetch(NotificationTable.fetchNotificationKey(notificationKey: notificationKey))
        }catch {
            print("Error fetching data from CoreData")
        }
        return tasks
    }
    
}
