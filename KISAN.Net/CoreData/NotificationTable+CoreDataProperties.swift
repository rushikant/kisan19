//
//  NotificationTable+CoreDataProperties.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 02/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//
//

import Foundation
import CoreData


extension NotificationTable { 

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NotificationTable> {
        return NSFetchRequest<NotificationTable>(entityName: "NotificationTable")
    }
    
    @nonobjc public class func fetchNotificationKey(notificationKey:String) -> NSFetchRequest<NotificationTable> {
        let fetchRequest = NSFetchRequest<NotificationTable>(entityName: "NotificationTable")
        let predicate = NSPredicate(format: "notificationKey LIKE[c] %@", notificationKey)
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var notificationKey: String?
    @NSManaged public var communityImageUrl: String?
    @NSManaged public var communityJabberId: String?
    @NSManaged public var communityKey: String?
    @NSManaged public var action: String?
    @NSManaged public var communityName: String?
    @NSManaged public var sendAt: String?
    @NSManaged public var sso_id: String?
    @NSManaged public var communityCategories: String?
    @NSManaged public var communityDesc: String?
    @NSManaged public var totalMembers: String?
    @NSManaged public var communityWithRssFeed: String?
}
