//
//  CreatePostViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 14/01/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MobileCoreServices

class CreatePostViewController: UIViewController{
    @IBOutlet var TopofPOstBUttonConstarint: NSLayoutConstraint!
    @IBOutlet var txtpostTitl: UITextView!
    @IBOutlet var txtPOstMEssage: UITextView!
    @IBOutlet var viewOFAttachment: UIView!
    @IBOutlet var viewOfAttchButton: UIView!
    @IBOutlet var viewOfSelectedAttachment: UIView!
    @IBOutlet var imgViewOfselectedAttachment: UIImageView!
    @IBOutlet var topContarintconstantOfPostTitleView: NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var lblOfPostTitleCanNotBlank: UILabel!
    @IBOutlet weak var lblOfMesgCanNotBlank: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var imageViewOfAttachmentIcon: UIImageView!
    
    @IBOutlet var lblTitlOfthePost: UILabel!
    @IBOutlet var lblPostMessage: UILabel!
    @IBOutlet var lblPhotoOrVideo: UILabel!
    @IBOutlet var lblPhotoOrVideoAttchmentview: UILabel!
    @IBOutlet var lblattach: UILabel!
    
    
    private let picker = UIImagePickerController()

    var postImage: UIImage!
    var afterCroppedImg: UIImage!
    var strPostTitl = String()
    var strPostMsg = String()
    var myChatsData = MyChatsTable()
    var appDelegate = AppDelegate()
    var selectedVideoURL: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        // Do any additional setup after loading the view.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        imgViewOfselectedAttachment.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imgViewOfselectedAttachment.addGestureRecognizer(tapRecognizer)
        btnPost.backgroundColor = UIColor.lightGray
        imgViewOfselectedAttachment.image = nil
        btnPost.isEnabled = false
    }
    
    @objc func imageTapped(sender: UIImageView) {
        self.openAttachment()
    }
    
    
    @IBAction func btnEditImageClick(_ sender: Any) {
        self.openAttachment()
    }
    
    func initialDesign(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.text =  (NSLocalizedString(StringConstants.NavigationTitle.CreatePost, comment: StringConstants.EMPTY))
        navigationItem.titleView = lblOfTitle
        //Post button
        self.viewOfSelectedAttachment.isHidden = true
        if  UIScreen.main.bounds.size.height <= 568{
            self.TopofPOstBUttonConstarint.constant = -240
        }else{
            self.TopofPOstBUttonConstarint.constant = -210
        }
        topContarintconstantOfPostTitleView.constant = 10
        self.viewOfAttchButton.layer.cornerRadius = 2
        self.viewOfAttchButton.clipsToBounds = true
        self.viewOfAttchButton.layer.borderWidth = 1
        self.viewOfAttchButton.layer.borderColor = UIColor(red:241/255, green:241/255, blue:241/255, alpha: 1).cgColor
        self.viewOfAttchButton.backgroundColor = UIColor.clear
        txtpostTitl.text = (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY))
        txtPOstMEssage.text = (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY))
        txtpostTitl.textColor = UIColor.lightGray
        txtPOstMEssage.textColor = UIColor.lightGray

        lblTitlOfthePost.text = (NSLocalizedString(StringConstants.CreatePostViewController.titleofpost, comment: StringConstants.EMPTY))
        lblPostMessage.text = (NSLocalizedString(StringConstants.CreatePostViewController.messageOfPost, comment: StringConstants.EMPTY))
        lblPhotoOrVideo.text = (NSLocalizedString(StringConstants.CreatePostViewController.PhotoOrVideo, comment: StringConstants.EMPTY))
        lblPhotoOrVideoAttchmentview.text = (NSLocalizedString(StringConstants.CreatePostViewController.PhotoOrVideo, comment: StringConstants.EMPTY))
        lblattach.text = (NSLocalizedString(StringConstants.CreatePostViewController.Attach, comment: StringConstants.EMPTY))
        btnPost.setTitle((NSLocalizedString(StringConstants.CreatePostViewController.post, comment: StringConstants.EMPTY)), for: .normal)

    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        txtpostTitl.resignFirstResponder()
        txtPOstMEssage.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAttachmentClicked(_ sender: Any) {
        self.openAttachment()
    }
    
    func openAttachment(){
        txtpostTitl.resignFirstResponder()
        txtPOstMEssage.resignFirstResponder()
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.image, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.delegate = self
            picker.sourceType = .savedPhotosAlbum
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.video, comment: StringConstants.EMPTY)), style: .default, handler: {
            action in
            picker.sourceType = .savedPhotosAlbum
            picker.delegate = self
            picker.mediaTypes = [kUTTypeMovie as String]
            self.present(picker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func BTncloseclicked(_ sender: Any) {
        txtpostTitl.resignFirstResponder()
        txtPOstMEssage.resignFirstResponder()
        self.TopofPOstBUttonConstarint.constant = -210 //-90
        self.viewOfSelectedAttachment.isHidden = true
        self.viewOFAttachment.isHidden = false
    }
    
    @IBAction func BtnPostClicked(_ sender: Any) {
        txtpostTitl.resignFirstResponder()
        txtPOstMEssage.resignFirstResponder()
        
        let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
        //channelSendBroadcastMsgVC.dominantColour = self.myChatsData.communityDominantColour!
        //channelSendBroadcastMsgVC.ownerId = ownerId
        channelSendBroadcastMsgVC.ownerId = myChatsData.ownerId!
        channelSendBroadcastMsgVC.communityName = myChatsData.communityName!
        channelSendBroadcastMsgVC.communityKey = myChatsData.communityKey!
        channelSendBroadcastMsgVC.communityImageSmallThumbUrl = myChatsData.communityImageSmallThumbUrl!
        channelSendBroadcastMsgVC.myChatsData = myChatsData
        channelSendBroadcastMsgVC.titleOfCreatePost = txtpostTitl.text
        channelSendBroadcastMsgVC.mesaageOfCreatePost = txtPOstMEssage.text
        channelSendBroadcastMsgVC.commingFrom = StringConstants.flags.channelSendBroadcastMsgVC
        appDelegate.strCreatePost = StringConstants.flags.CreatePostViewController
        if (afterCroppedImg != nil) && selectedVideoURL == nil{
            appDelegate.selectedImageOfPostCreation = afterCroppedImg
            afterCroppedImg = UIImage()
        }else{
            appDelegate.selectedVideoOfPostCreation = selectedVideoURL
            selectedVideoURL = nil
        }

        txtpostTitl.text = (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY))
        txtPOstMEssage.text = (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY))
        imgViewOfselectedAttachment.image = nil
        self.TopofPOstBUttonConstarint.constant = -210 //-90
        self.viewOfSelectedAttachment.isHidden = true
        self.viewOFAttachment.isHidden = false
        enablePostButton()
        self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
    }
    
    func enablePostButton(){
        strPostTitl = strPostTitl.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        strPostMsg = strPostMsg.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)

        if !strPostTitl.isEmpty &&  txtpostTitl.text != (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY)) && !strPostMsg.isEmpty && txtPOstMEssage.text != (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY)) && imgViewOfselectedAttachment.image != nil {
            btnPost.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
            btnPost.isEnabled = true
        }else{
            btnPost.backgroundColor = UIColor.lightGray
            btnPost.isEnabled = false
        }
    }
}

extension CreatePostViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate,CropViewControllerDelegate{

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                postImage = image.fixOrientation()
                // postImage = image
                dismiss(animated: true) { [unowned self] in
                    self.openEditor(nil)
                }
            }else{
                selectedVideoURL = nil
                afterCroppedImg = UIImage()
                selectedVideoURL = info[UIImagePickerControllerMediaURL] as? URL
                self.dismiss(animated: true, completion: nil)
                //Set selected video thumbnail Image
                let imageHelper = ImageHelper()
                let thumbImage   =  imageHelper.getThumbnailImage(forUrl: selectedVideoURL!) //self.getThumbnailImage(forUrl:selectedVideoURL! )
                //Set up orientation to image
                let upOrientationImage  = imageHelper.fixOrientationOfImage(image: thumbImage!)
                //Crop the video thumb in 120 * 120
                let croppedImage = self.cropToBounds(image: upOrientationImage!, width: 120, height: 120)
                imgViewOfselectedAttachment.image = croppedImage
                self.viewOfSelectedAttachment.isHidden = false
                self.TopofPOstBUttonConstarint.constant = 29
                self.viewOFAttachment.isHidden = true
                enablePostButton()
            }
        }
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
       guard let image = postImage else {
            return
        }
      /*  let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)*/
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
    }
    
    // MARK: - CropView New Library delegate
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        // imageViewOfProfilePic.image = image
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        imgViewOfselectedAttachment.image = image
        afterCroppedImg = UIImage()
        selectedVideoURL = nil
        afterCroppedImg = image
        self.viewOfSelectedAttachment.isHidden = false
        self.TopofPOstBUttonConstarint.constant = 29
        self.viewOFAttachment.isHidden = true
        controller.dismiss(animated: true, completion: nil)
        enablePostButton()
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
extension CreatePostViewController: UITextViewDelegate{

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == txtpostTitl{
            let currentText = txtpostTitl.text ?? StringConstants.EMPTY
            guard let stringRange = Range(range, in: currentText) else { return false }
            let changedText = currentText.replacingCharacters(in: stringRange, with: text)
            strPostTitl = changedText
            if text.isEmpty {
                if changedText.count < 1 {
                    lblOfPostTitleCanNotBlank.isHidden = false
                    lblOfPostTitleCanNotBlank.text = (NSLocalizedString(StringConstants.CreatePostViewController.validationTitl, comment: StringConstants.EMPTY))
                    txtpostTitl.text = StringConstants.EMPTY
                    enablePostButton()
                }
            }else{
                lblOfPostTitleCanNotBlank.text =  StringConstants.EMPTY
                lblOfPostTitleCanNotBlank.isHidden = true
                txtpostTitl.text = textView.text
                enablePostButton()
                return changedText.count <= 25
            }
        }else{
            let currentText = txtPOstMEssage.text ?? StringConstants.EMPTY
            guard let stringRange = Range(range, in: currentText) else { return false }
            let changedText = currentText.replacingCharacters(in: stringRange, with: text)
            strPostMsg = changedText
            if text.isEmpty {
                if changedText.count < 1 {
                    lblOfMesgCanNotBlank.isHidden = false
                    lblOfMesgCanNotBlank.text = (NSLocalizedString(StringConstants.CreatePostViewController.validationMessage, comment: StringConstants.EMPTY))
                    txtPOstMEssage.text = StringConstants.EMPTY
                    enablePostButton()
                }
            }else{
                lblOfMesgCanNotBlank.isHidden = true
                lblOfMesgCanNotBlank.text = StringConstants.EMPTY
                txtPOstMEssage.text = textView.text
                enablePostButton()
                return changedText.count <= 250
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtpostTitl{
            if txtpostTitl.text == (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY)){
                txtpostTitl.text = StringConstants.EMPTY
                txtpostTitl.textColor = UIColor.black
            }
        }else{
            if txtPOstMEssage.text == (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY)){
                txtPOstMEssage.text = StringConstants.EMPTY
                txtPOstMEssage.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //textView.text = textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if textView == txtpostTitl{
                if txtpostTitl.text.isEmpty {
                    txtpostTitl.text = (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY))
                    lblOfPostTitleCanNotBlank.isHidden = false
                    lblOfPostTitleCanNotBlank.text = (NSLocalizedString(StringConstants.CreatePostViewController.validationTitl, comment: StringConstants.EMPTY))
                    enablePostButton()
                }else{
                    lblOfPostTitleCanNotBlank.isHidden = true
                    enablePostButton()
                }
            }else{
                if txtPOstMEssage.text.isEmpty {
                    txtPOstMEssage.text = (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY))
                    lblOfMesgCanNotBlank.isHidden = false
                    lblOfMesgCanNotBlank.text = (NSLocalizedString(StringConstants.CreatePostViewController.validationMessage, comment: StringConstants.EMPTY))
                    enablePostButton()
                }else{
                    lblOfMesgCanNotBlank.isHidden = true
                    enablePostButton()
                }
            }
            textView.textColor = UIColor.lightGray
        
        
        if textView == txtpostTitl{
            if txtpostTitl.text != (NSLocalizedString(StringConstants.placeHolder.EnterpostTitl, comment: StringConstants.EMPTY)){
                txtpostTitl.textColor = UIColor.black
            }else{
                txtpostTitl.textColor = UIColor.lightGray
            }
        }else{
            if txtPOstMEssage.text != (NSLocalizedString(StringConstants.placeHolder.EntertPostMessage, comment: StringConstants.EMPTY)){
                txtPOstMEssage.textColor = UIColor.black
            }else{
                txtPOstMEssage.textColor = UIColor.lightGray
            }
        }
            textView.resignFirstResponder()
            self.view.layoutIfNeeded()
    }
   
}

extension CreatePostViewController: UIImageCropperProtocol {
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imgViewOfselectedAttachment.image = croppedImage?.fixOrientation()
        afterCroppedImg = UIImage()
        selectedVideoURL = nil
        afterCroppedImg = croppedImage
        self.viewOfSelectedAttachment.isHidden = false
        self.TopofPOstBUttonConstarint.constant = 29
        self.viewOFAttachment.isHidden = true
        picker.dismiss(animated: true, completion: nil)
        enablePostButton()
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
