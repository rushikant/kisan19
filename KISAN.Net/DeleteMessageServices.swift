//
//  DeleteMessageServices.swift
//  KISAN.Net
//
//  Created by Easebuzz Pvt Ltd on 14/09/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import Alamofire

class DeleteMessageServices: Repository {
    
    func deleteMessage(_ baseUrl: String,
                            postData: [String: AnyObject],
                            withSuccessHandler success: CompleteionHandler?,
                            withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.DELETE(baseUrl, deleteData: postData as NSDictionary, requestName: RequestName.deleteMessage)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.deleteMessage {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var deleteMessageResponse: DeleteMessageResponse = DeleteMessageResponse.initializeDeleteMessageResponse()
                    deleteMessageResponse = DeleteMessageResponse.getDeleteMessageResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(deleteMessageResponse)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
    

}
