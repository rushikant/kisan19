//
//  LeftViewController.swift
//  KisanChat
//
//  Created by KISAN TEAM on 28/11/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit
import SDWebImage

// Normal
enum LeftMenu: Int {
    case mychats = 0
    //case startyourchannel
    case discover
    //case Kisan2018
    //case myGreenpass
   // case FairLayout
   // case Language
    case share
    case support
    case aboutApp
    case logout
}

//Exhibitor
enum LeftMenuWithMyChannel: Int {
    case mychats = 0
    //case startyourchannel
    case discover
    case myChannels
    // case issueGreenPass
   // case issueBadges
    //case FairLayout
    //case Kisan2018
   // case Language
    case share
    case support
    case aboutApp
    case logout
}

//Login as Exhibitor but not booked stall
enum LeftMenuForExhiButWithoutBookedStall: Int {
    case mychats = 0
    //case startyourchannel
    case discover
    case myChannels
//    case issueGreenPass
//    case issueBadges
    //case FairLayout
    //case Kisan2018
    // case Language
    case share
    case support
    case aboutApp
    case logout
}

//Exhibitor : remove issueGreenPass if show_issue_pass == 0 && show_issue_unlimited_greenpass == 0
enum LeftMenuWithRemoveIssueGreenPass: Int {
    case mychats = 0
    //case startyourchannel
    case discover
    case myChannels
    //case issueGreenPass
    case issueBadges
    //case FairLayout
    //case Kisan2018
    // case Language
    case share
    case support
    case aboutApp
    case logout
}

//Exhibitor : remove issueGreenPass if show_issue_pass == 0 && show_issue_unlimited_greenpass == 0
enum LeftMenuWithRemoveIssueBadges: Int {
    case mychats = 0
    //case startyourchannel
    case discover
    case myChannels
    case issueGreenPass
    //case issueBadges
    //case FairLayout
    //case Kisan2018
    // case Language
    case share
    case support
    case aboutApp
    case logout
}


//Kisan Mitra
enum LeftMenuWithKisanMitra: Int {
    case mychats = 0
    //case startyourchannel
    case discover
   // case Kisan2018
    //case myGreenpass
    case InviteFriends
    
    // case FairLayout
   // case Language
    case share
    case support
    case aboutApp
    case logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}
class LeftViewController: UIViewController, LeftMenuProtocol {
  
   // var arrOfmenus = [(NSLocalizedString(StringConstants.DrawerMenuNames.Home, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Discover, comment: StringConstants.EMPTY)) ,/*StringConstants.DrawerMenuNames.Start_your_channel,*/(NSLocalizedString(StringConstants.DrawerMenuNames.Kisan_2018, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Language, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Share, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Support, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Logout, comment: StringConstants.EMPTY))]
    var arrOfmenus = [String]()
    var arrOfmenuImages = [String]()
    var dashboardViewController: UIViewController!
    var introducingChannelViewController: UIViewController!
    var supportViewController: UIViewController!
    var showUserProfileVC: UIViewController!
    var viewContro: UIViewController!
    var greenpassViewController: UIViewController!
    var discoverChannelViewController: UIViewController!
    var channelDashboardViewController: UIViewController!
    var LanguageviewController: UIViewController!
    var SetInviteSummaryViewController: UIViewController!
    var PhoneContactViewController: UIViewController!
    var CongratulationsScreenViewController: UIViewController!
    var AboutAppViewController: UIViewController!
    var scanBarcodeViewController: UIViewController!
    var brochurelist: UIViewController!
    
    @IBOutlet weak var imgViewOFProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tblMenuList: UITableView!
    
    //FooterView Outlets
    @IBOutlet weak var qrScanFooterView: UIView!
    @IBOutlet weak var brochureLbl: UILabel!
    @IBOutlet weak var scanInfoLbl: UILabel!
    @IBOutlet weak var scanImage: UIImageView!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var heightOfQrScannerFooterView: NSLayoutConstraint!
    
    var fullName = String()
    var myChatsList = [MyChatsTable]()
    var userId = String()
    var accountTypeValue = String()
    var sessionID = String()
    var isKisanMitra  = Bool()
    var isSkip  = Bool()
    var medialink = StringConstants.EMPTY
    var show_issue_pass  = Int()
    var show_issue_unlimited_greenpass = Int()
    var isEventOngoing = true

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgViewOFProfile.layer.cornerRadius = imgViewOFProfile.frame.size.width / 2
        imgViewOFProfile.clipsToBounds = true
        tblMenuList.separatorColor = UIColor.clear
        
         sessionID = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
         isKisanMitra = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.isMitra)
        isSkip = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.skip_media)
        medialink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)
        // setup footer View
       // self.setupScanbarcodeFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sessionID = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        isKisanMitra = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.isMitra)
        show_issue_pass = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
        show_issue_unlimited_greenpass  = UserDefaults.standard.integer(forKey:  StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)

        exhibitionAds()

        isSkip = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.skip_media)
        medialink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)

        arrOfmenus = [(NSLocalizedString(StringConstants.DrawerMenuNames.Home, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Discover, comment: StringConstants.EMPTY)) ,(NSLocalizedString(StringConstants.DrawerMenuNames.Kisan_2018, comment: StringConstants.EMPTY)),/*(NSLocalizedString(StringConstants.DrawerMenuNames.My_Greenpass, comment: StringConstants.EMPTY)),
             (NSLocalizedString(StringConstants.DrawerMenuNames.Invite, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Language, comment: StringConstants.EMPTY)),*/(NSLocalizedString(StringConstants.DrawerMenuNames.Share, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Support, comment: StringConstants.EMPTY)),  (NSLocalizedString(StringConstants.DrawerMenuNames.AboutApp, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.DrawerMenuNames.Logout, comment: StringConstants.EMPTY))]
        
        arrOfmenuImages = [StringConstants.ImageNames.chat,StringConstants.ImageNames.discover ,/*StringConstants.ImageNames.channel,*/StringConstants.ImageNames.kisanlogo,/*StringConstants.ImageNames.IssueGreenPass,StringConstants.ImageNames.InviteFriends,*StringConstants.ImageNames.Layout,*//*StringConstants.ImageNames.language,*/StringConstants.ImageNames.share,StringConstants.ImageNames.help,StringConstants.ImageNames.aboutApp,StringConstants.ImageNames.logout]

        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        if let userProfilePic = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.imageBigthumbUrl) {
            imgViewOFProfile.sd_setImage(with: URL(string: userProfilePic), placeholderImage:UIImage(named: StringConstants.ImageNames.LeftDrawerProfilePlaceholderImg))
        }
        
        if let firstName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userFirstName) {
            fullName = firstName
        }
        if let lastName = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userLastName) {
            fullName =  fullName + " " + lastName
        }
        lblUserName.text = fullName

        // Get data from local MyChat DB
        if let userId = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.userId) {
            self.myChatsList = MyChatServices.sharedInstance.fetchLoginUserAdminChannelList(ownerId:userId)
            
            if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
                accountTypeValue = accountType
               // if self.myChatsList.count != 0 {
                if accountTypeValue == StringConstants.TWO {
                    //my greenpass option remove
                    let fourthIndexValue = arrOfmenus[3]
                    if fourthIndexValue == (NSLocalizedString(StringConstants.DrawerMenuNames.My_Greenpass, comment: StringConstants.EMPTY)) {
                        arrOfmenus.remove(at: 3)
                        arrOfmenuImages.remove(at: 3)
                        
                    }
                    let thirdIndexValue = arrOfmenus[2]
                    if thirdIndexValue == (NSLocalizedString(StringConstants.DrawerMenuNames.Kisan_2018, comment: StringConstants.EMPTY)) {
                        arrOfmenus.remove(at: 2)
                        arrOfmenuImages.remove(at: 2)
                        arrOfmenus.insert((NSLocalizedString(StringConstants.DrawerMenuNames.MyChannel, comment: StringConstants.EMPTY)), at: 2)
                        arrOfmenuImages.insert(StringConstants.ImageNames.channel, at: 2)
                    }
                   
                    //issue Green pass option for exhibitor
                    let inviteMenuIndex = arrOfmenus[3]
                    if inviteMenuIndex == (NSLocalizedString(StringConstants.DrawerMenuNames.Invite, comment: StringConstants.EMPTY)) {
                        arrOfmenus.remove(at: 3)
                        arrOfmenuImages.remove(at: 3)
                        arrOfmenus.insert((NSLocalizedString(StringConstants.DrawerMenuNames.IssueGreenPass, comment: StringConstants.EMPTY)), at: 3)
                        arrOfmenuImages.insert(StringConstants.ImageNames.IssueGreenPass, at: 3)
                    }
                    
                    // add this for issue badges from exhibitors
                   /* if (!isHideBadgeList()){
                      arrOfmenus.insert(StringConstants.DrawerMenuNames.Issue_badges, at: 4)
                      arrOfmenuImages.insert(StringConstants.ImageNames.badgeIssue, at: 4)
                    }*/
                    
                    let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)

                   /* if(sessionId == StringConstants.EMPTY){
                        let indexOfIssueGreenPass = arrOfmenus.firstIndex(where: {$0 == StringConstants.DrawerMenuNames.IssueGreenPass})
                        arrOfmenus.remove(at: indexOfIssueGreenPass!)
                        arrOfmenuImages.remove(at: indexOfIssueGreenPass!)
                        let indexOfIssue_badges = arrOfmenus.firstIndex(where: {$0 == StringConstants.DrawerMenuNames.Issue_badges})
                        arrOfmenus.remove(at: indexOfIssue_badges!)
                        arrOfmenuImages.remove(at: indexOfIssue_badges!)
                    }else if show_issue_unlimited_greenpass  == 0 && show_issue_pass == 0 {
                        let indexOfIssueGreenPass = arrOfmenus.firstIndex(where: {$0 == StringConstants.DrawerMenuNames.IssueGreenPass})
                        arrOfmenus.remove(at: indexOfIssueGreenPass!)
                        arrOfmenuImages.remove(at: indexOfIssueGreenPass!)
                    }*/
                    
                    tblMenuList.reloadData()
                }else{
                    let thirdIndexValue = arrOfmenus[2]
                    if thirdIndexValue == (NSLocalizedString(StringConstants.DrawerMenuNames.Kisan_2018, comment: StringConstants.EMPTY)) {
                        arrOfmenus.remove(at: 2)
                        arrOfmenuImages.remove(at: 2)
                        /*arrOfmenus.insert((NSLocalizedString(StringConstants.DrawerMenuNames.Kisan_2018, comment: StringConstants.EMPTY)), at: 2)
                        arrOfmenuImages.insert(StringConstants.ImageNames.kisanlogo, at: 2)*/
                    }
                    
                    //issue Green pass option for exhibitor
                    let inviteMenuIndex = arrOfmenus[4]
                    
                    if(!isKisanMitra){
                    if inviteMenuIndex == (NSLocalizedString(StringConstants.DrawerMenuNames.Invite, comment: StringConstants.EMPTY)) {
                        arrOfmenus.remove(at: 4)
                        arrOfmenuImages.remove(at: 4)
                    }
                    }else if(isKisanMitra && show_issue_pass == 0){
                        if inviteMenuIndex == (NSLocalizedString(StringConstants.DrawerMenuNames.Invite, comment: StringConstants.EMPTY)) {
                            arrOfmenus.remove(at: 4)
                            arrOfmenuImages.remove(at: 4)
                            arrOfmenus.insert((NSLocalizedString(StringConstants.DrawerMenuNames.guest_list, comment: StringConstants.EMPTY)), at: 4)
                            arrOfmenuImages.insert(StringConstants.ImageNames.guestlist_icon, at: 4)
                    }
                }
                    tblMenuList.reloadData()
            }
        }

    }
        
        
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
            accountTypeValue = accountType
            // if self.myChatsList.count != 0 {
            if accountTypeValue == StringConstants.TWO {
                qrScanFooterView.isHidden = true
                heightOfQrScannerFooterView.constant = 0
            }else{
                qrScanFooterView.isHidden = false
                heightOfQrScannerFooterView.constant = 91
                /*if isShowBroucherReaderOption(){
                    qrScanFooterView.isHidden = false
                    heightOfQrScannerFooterView.constant = 91
                }else{
                    qrScanFooterView.isHidden = true
                    heightOfQrScannerFooterView.constant = 0
                }*/
            }
        }
}
            
    @IBAction func btnProfileEditClick(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let showUserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "ShowUserProfileVC") as! ShowUserProfileVC
        self.showUserProfileVC = UINavigationController(rootViewController: showUserProfileVC)
        self.slideMenuController()?.changeMainViewController(self.showUserProfileVC, close: true)
    }
    
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .mychats:
            
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
            // case .startyourchannel:
            //
            //            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            //            let introducingChannelViewController = mainStoryboard.instantiateViewController(withIdentifier: "IntroducingChannelViewController") as! IntroducingChannelViewController
            //            self.introducingChannelViewController = UINavigationController(rootViewController: introducingChannelViewController)
            //            self.slideMenuController()?.changeMainViewController(self.introducingChannelViewController, close: true)
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
       /* case .Kisan2018:
            
            let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
            let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
            
            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let CongRecievedPassVC = dashboardStoryboard.instantiateViewController(withIdentifier: "CongRecievedPassVC") as! CongRecievedPassVC
                    self.CongratulationsScreenViewController = UINavigationController(rootViewController: CongRecievedPassVC)
                    self.slideMenuController()?.changeMainViewController(self.CongratulationsScreenViewController, close: true)
                }else{
                    let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                    dashboardViewController.commingFrom = StringConstants.flags.leftDrawerKisan2018
                    self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                    self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                }
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                dashboardViewController.commingFrom = StringConstants.flags.leftDrawerKisan2018
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                
            }*/
//           let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
//            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerKisan2018
//            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
//            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
//
            
        /*case .FairLayout:
            //For Fair Layout
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let greenPassViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "GreenPassViewController") as! GreenPassViewController
            self.greenpassViewController = UINavigationController(rootViewController: greenPassViewController)
            self.slideMenuController()?.changeMainViewController(self.greenpassViewController, close: true)*/
            
            
//        case .Language:
//            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
//            let ViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            ViewController.strCommingFrom = StringConstants.flags.leftDrawerLanguage
//            self.LanguageviewController = UINavigationController(rootViewController: ViewController)
//            self.slideMenuController()?.changeMainViewController(self.LanguageviewController, close: true)
            
         /*case .myGreenpass:

            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let PassBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "PassBaseVC") as! PassBaseVC
            self.dashboardViewController = UINavigationController(rootViewController: PassBaseVC)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)*/

        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
      
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
        }
    }

    
    
    func changeViewControllerForKisanMitra(_ menu: LeftMenuWithKisanMitra) {
        switch menu {
        case .mychats:
            
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
       /* case .Kisan2018:
            let  pendingInviteCount = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.pendingInviteCount)
            let isInviteViewed = UserDefaults.standard.bool(forKey: StringConstants.NSUserDefauluterKeys.isViewdInvite)
            
            if(pendingInviteCount>0){
                if(!isInviteViewed){
                    let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let CongRecievedPassVC = dashboardStoryboard.instantiateViewController(withIdentifier: "CongRecievedPassVC") as! CongRecievedPassVC
                    self.CongratulationsScreenViewController = UINavigationController(rootViewController: CongRecievedPassVC)
                    self.slideMenuController()?.changeMainViewController(self.CongratulationsScreenViewController, close: true)
                }else{
                    let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                    let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                    dashboardViewController.commingFrom = StringConstants.flags.leftDrawerKisan2018
                    self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                    self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                }
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                dashboardViewController.commingFrom = StringConstants.flags.leftDrawerKisan2018
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)

            }
        case .myGreenpass:

            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let PassBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "PassBaseVC") as! PassBaseVC
            self.dashboardViewController = UINavigationController(rootViewController: PassBaseVC)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)*/

        case .InviteFriends:
            medialink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)
            //show_issue_pass = 0

            if(isKisanMitra && show_issue_pass == 0){
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let invitedGuestVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InvitedGuestVC") as! InvitedGuestVC
                invitedGuestVC.comingFrom = StringConstants.flags.guestList
                self.dashboardViewController = UINavigationController(rootViewController: invitedGuestVC)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            }else{
            //if media link is empty and not empty
            if(!medialink.isEmpty && medialink != nil){
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
                self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
                self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)
            }else{
            if(isSkip){
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
                self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
                self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let SetInviteSummaryViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "SetInviteSummaryViewController") as! SetInviteSummaryViewController
                self.SetInviteSummaryViewController = UINavigationController(rootViewController: SetInviteSummaryViewController)
                self.slideMenuController()?.changeMainViewController(self.SetInviteSummaryViewController, close: true)
            }
            }
            }
          //  case .Language:
//            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
//            let ViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            ViewController.strCommingFrom = StringConstants.flags.leftDrawerLanguage
//            self.LanguageviewController = UINavigationController(rootViewController: ViewController)
//            self.slideMenuController()?.changeMainViewController(self.LanguageviewController, close: true)
            
        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
            
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
        }
    }
    
    
    func changeViewControllerWithMyCahnnel(_ menu: LeftMenuWithMyChannel) {
        switch menu {
            
        case .mychats:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
            //                    case .startyourchannel:
            //
            //                        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            //                        let introducingChannelViewController = mainStoryboard.instantiateViewController(withIdentifier: "IntroducingChannelViewController") as! IntroducingChannelViewController
            //                        self.introducingChannelViewController = UINavigationController(rootViewController: introducingChannelViewController)
            //                        self.slideMenuController()?.changeMainViewController(self.introducingChannelViewController, close: true)
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
        case .myChannels:
            if self.myChatsList.count > 0 {
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelDashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                let myChats = self.myChatsList[0]
                channelDashboardViewController.myChatsData = myChats
                self.greenpassViewController = UINavigationController(rootViewController: channelDashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.greenpassViewController, close: true)
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                self.navigationController?.view.makeToast((NSLocalizedString("No any Mychannel available", comment: StringConstants.EMPTY)))
            }
            
        /*case .issueGreenPass:
            
            if(!medialink.isEmpty){
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                inviteFriendBaseVC.comingFrom = StringConstants.flags.exhIssuePass
                self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
                self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
                self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)
            }else{
                
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let SetInviteSummaryViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "SetInviteSummaryViewController") as! SetInviteSummaryViewController
                self.SetInviteSummaryViewController = UINavigationController(rootViewController: SetInviteSummaryViewController)
                self.slideMenuController()?.changeMainViewController(self.SetInviteSummaryViewController, close: true)
            }
            
        case .issueBadges:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
            inviteFriendBaseVC.comingFrom = StringConstants.flags.issueBadges
            self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
            self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
            self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)*/
            
            
        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
            
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        }
    }
    
    func changeViewControllerForExhiButWithoutBookedStall(_ menu: LeftMenuForExhiButWithoutBookedStall) {
       
        switch menu {
            
        case .mychats:
            
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
        case .myChannels:
            if self.myChatsList.count > 0 {
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelDashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                let myChats = self.myChatsList[0]
                channelDashboardViewController.myChatsData = myChats
                self.greenpassViewController = UINavigationController(rootViewController: channelDashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.greenpassViewController, close: true)
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                self.navigationController?.view.makeToast((NSLocalizedString("No any Mychannel available", comment: StringConstants.EMPTY)))
            }
            
        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
            
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        }
    }
    
    
    func changeViewControllerForExhiButWithoutIssueGreenPass(_ menu: LeftMenuWithRemoveIssueGreenPass) {
        switch menu {
            
        case .mychats:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
        case .myChannels:
            if self.myChatsList.count > 0 {
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelDashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                let myChats = self.myChatsList[0]
                channelDashboardViewController.myChatsData = myChats
                self.greenpassViewController = UINavigationController(rootViewController: channelDashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.greenpassViewController, close: true)
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                self.navigationController?.view.makeToast((NSLocalizedString("No any Mychannel available", comment: StringConstants.EMPTY)))
            }
            
        case .issueBadges:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
            inviteFriendBaseVC.comingFrom = StringConstants.flags.issueBadges
            self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
            self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
            self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)
            
            
        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
            
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        }
    }
    
    
    
    
    func changeViewControllerForExhiButWithoutIssueBadges(_ menu: LeftMenuWithRemoveIssueBadges) {
        switch menu {
            
        case .mychats:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
            
        case .discover:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let discoverChannelViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DiscoverChannelViewController") as! DiscoverChannelViewController
            self.discoverChannelViewController = UINavigationController(rootViewController: discoverChannelViewController)
            self.slideMenuController()?.changeMainViewController(self.discoverChannelViewController, close: true)
            
        case .myChannels:
            if self.myChatsList.count > 0 {
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let channelDashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "ChannelDashboardViewController") as! ChannelDashboardViewController
                let myChats = self.myChatsList[0]
                channelDashboardViewController.myChatsData = myChats
                self.greenpassViewController = UINavigationController(rootViewController: channelDashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.greenpassViewController, close: true)
            }else{
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
                self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
                self.navigationController?.view.makeToast((NSLocalizedString("No any Mychannel available", comment: StringConstants.EMPTY)))
            }
            
            
        case .issueGreenPass:
            
            if(!medialink.isEmpty){
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                inviteFriendBaseVC.comingFrom = StringConstants.flags.exhIssuePass
                self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
                self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
                self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)
            }else{
                
                let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let SetInviteSummaryViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "SetInviteSummaryViewController") as! SetInviteSummaryViewController
                self.SetInviteSummaryViewController = UINavigationController(rootViewController: SetInviteSummaryViewController)
                self.slideMenuController()?.changeMainViewController(self.SetInviteSummaryViewController, close: true)
            }

            
        /*case .issueBadges:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let  inviteFriendBaseVC = dashboardStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
            inviteFriendBaseVC.comingFrom = StringConstants.flags.issueBadges
            self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
            self.PhoneContactViewController = UINavigationController(rootViewController: inviteFriendBaseVC)
            self.slideMenuController()?.changeMainViewController(self.PhoneContactViewController, close: true)*/
            
            
        case .share:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerShare
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        case .support:
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
            let supportViewController = mainStoryboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.supportViewController = UINavigationController(rootViewController: supportViewController)
            self.slideMenuController()?.changeMainViewController(self.supportViewController, close: true)
            
        case .aboutApp:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let AboutAppViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "AboutAppViewController") as! AboutAppViewController
            self.AboutAppViewController = UINavigationController(rootViewController: AboutAppViewController)
            self.slideMenuController()?.changeMainViewController(self.AboutAppViewController, close: true)
            
        case .logout:
            let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            dashboardViewController.commingFrom = StringConstants.flags.leftDrawerLogout
            self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
            self.slideMenuController()?.changeMainViewController(self.dashboardViewController, close: true)
            
        }
    }

    
    func isHideBadgeList()->Bool{
        var currentdate = Date()
        var exh_badge_issue_Last_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.exh_badge_enddatetime_original)
        
       if (exh_badge_issue_Last_date.isEmpty){
            exh_badge_issue_Last_date = "2019-12-11 00:00:00"
        }
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateEvent: Date? = dateFormatterGet.date(from: exh_badge_issue_Last_date)! as Date
        
        let endDatetimeForissueBadges = dateFormatterGet.string(from: Date())
        currentdate = dateFormatterGet.date(from: endDatetimeForissueBadges)! as Date
        
        // Compare them
        switch currentdate.compare(dateEvent!) {
        case .orderedAscending     :   print("currentdate is earlier than date eventStart Date")
        return false
            
        case .orderedDescending    :   print("currentdate is later than eventStart Date")
        return true
            
        case .orderedSame          :   print("currentdate dates are eventStart Date")
        return false
        }
    }

    
    func isShowBroucherReaderOption()->Bool{
        var currentdate = Date()
        var eventStartDateTime = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.eventStartDateTime)
        
        if (eventStartDateTime.isEmpty){
            eventStartDateTime = "2019-12-11 00:00:00"
        }
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateEvent: Date? = dateFormatterGet.date(from: eventStartDateTime)! as Date
        
        let eventStartDateTimetoshow = dateFormatterGet.string(from: Date())
        currentdate = dateFormatterGet.date(from: eventStartDateTimetoshow)! as Date
        
        // Compare them
        switch currentdate.compare(dateEvent!) {
        case .orderedAscending     :   print("currentdate is earlier than date eventStart Date")
        return false
            
        case .orderedDescending    :   print("currentdate is later than eventStart Date")
        return true
            
        case .orderedSame          :   print("currentdate dates are eventStart Date")
        return true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    

extension LeftViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfmenus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewControllerCell", for: indexPath) as! LeftViewControllerCell
        cell.selectionStyle = .none
        tableView.allowsSelection = true
        cell.lblMenuList.text = arrOfmenus[indexPath.row]
        cell.contentView.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        cell.imageViewOfLeftIconMenu.image = UIImage(named: arrOfmenuImages[indexPath.row])!
        if indexPath.row == 1 || indexPath.row == 3 {
            cell.lblOfSepratorLine.isHidden = false
        } else {
            cell.lblOfSepratorLine.isHidden = true
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print("deselected")
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        if cellToDeSelect.isEqual(LeftViewControllerCell()){
            cellToDeSelect.contentView.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        }
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.selectedCellOfLeftDrawer)
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.selectedCellOfLeftDrawer)
        //if self.myChatsList.count == 0 {

        if accountTypeValue == StringConstants.ONE {
            
            if let menu = LeftMenu(rawValue: indexPath.row) {
                self.changeViewController(menu)
            }

          /*  if(isKisanMitra){
                if let menu = LeftMenuWithKisanMitra(rawValue: indexPath.row) {
                    self.changeViewControllerForKisanMitra(menu)
                }
            }else{
                if let menu = LeftMenu(rawValue: indexPath.row) {
                    self.changeViewController(menu)
                }
            }*/
        }else{
            
            if let menuValue = LeftMenuWithMyChannel(rawValue: indexPath.row) {
                self.changeViewControllerWithMyCahnnel(menuValue)
            }

            //comment
           /* let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
            if sessionId == StringConstants.EMPTY{
                if let menuValue = LeftMenuForExhiButWithoutBookedStall(rawValue: indexPath.row) {
                    self.changeViewControllerForExhiButWithoutBookedStall(menuValue)
                }
            }else if show_issue_pass == 0 && show_issue_unlimited_greenpass == 0  &&  isHideBadgeList(){
                if let menuValue = LeftMenuForExhiButWithoutBookedStall(rawValue: indexPath.row) {
                    self.changeViewControllerForExhiButWithoutBookedStall(menuValue)
                }
            }else if show_issue_pass == 0 && show_issue_unlimited_greenpass == 0  &&  !isHideBadgeList(){
                if let menuValue = LeftMenuWithRemoveIssueGreenPass(rawValue: indexPath.row) {
                    self.changeViewControllerForExhiButWithoutIssueGreenPass(menuValue)
                }
            }else{
                if(isHideBadgeList()){
                    if let menuValue = LeftMenuWithRemoveIssueBadges(rawValue: indexPath.row) {
                        self.changeViewControllerForExhiButWithoutIssueBadges(menuValue)
                    }

                }else{
                    if let menuValue = LeftMenuWithMyChannel(rawValue: indexPath.row) {
                        self.changeViewControllerWithMyCahnnel(menuValue)
                    }
                }
            }*/
        }
    }
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        //if self.myChatsList.count != 0 {
         if accountTypeValue == StringConstants.TWO {
            if indexPath.row == 1 {
                return 44
            }else{
                return 44
            }
        }else{
            return 44

        }
    }
}

//IBActions
extension LeftViewController {
    
    private func setupScanbarcodeFooter() {
        self.scanImage.image = #imageLiteral(resourceName: "barcode")
        self.brochureLbl.font = UIFont.systemFont(ofSize: 16)
        self.brochureLbl.textColor = .white
        self.brochureLbl.text = "brochure_reader".localized
        self.scanInfoLbl.font = UIFont.systemFont(ofSize: 13)
        self.scanInfoLbl.textColor = .white
        self.scanInfoLbl.text = "brochure_reader_hint".localized
        self.scanButton.addTarget(self, action: #selector(scanBarcodeTapped(sender:)), for: .touchUpInside)
    }
    
    private func setupListScanBarcodeFooter() {
        self.scanImage.image = #imageLiteral(resourceName: "list")
        self.brochureLbl.font = UIFont.systemFont(ofSize: 16)
        self.brochureLbl.textColor = .white
        self.brochureLbl.text = "brochure_list".localized
        self.scanInfoLbl.font = UIFont.systemFont(ofSize: 13)
        self.scanInfoLbl.textColor = .white
        self.scanInfoLbl.text = "brochure_reader_list_hint".localized
        self.scanButton.addTarget(self, action: #selector(showBrochureListTapped(sender:)), for: .touchUpInside)
    }
    
    
    // Scan Barcode button tapped -> Scan Brochure flow
    @objc func scanBarcodeTapped(sender: UIButton) {
        let scanBarcode = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "ScanBarcodeViewController") as! ScanBarcodeViewController
        self.scanBarcodeViewController = UINavigationController(rootViewController: scanBarcode)
       self.slideMenuController()?.changeMainViewController(self.scanBarcodeViewController, close: true)
    }
    
    // Scan Barcode button tapped -> Scan Brochure flow
    @objc func showBrochureListTapped(sender: UIButton) {
        let brochureList = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "BrochureListViewController") as! BrochureListViewController
        brochureList.isEventOngoing = self.isEventOngoing
       self.brochurelist = UINavigationController(rootViewController: brochureList)
       self.slideMenuController()?.changeMainViewController(self.brochurelist, close: true)
    }
    
    private func exhibitionAds(){
        let app_key = URLConstant.app_key
        let eventCode = URLConstant.eventCode
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
            source = RequestName.loginExhibitorSourceName
        }
        
        let params = ExhibitionAdvertiseDetailsRequest.convertToDictionary(exhibitorAdvertiseDetails: ExhibitionAdvertiseDetailsRequest.init(app_key: app_key, eventCode: eventCode, source: source, discount_name: "online", referrer_code: StringConstants.EMPTY))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.exhibitionDetails
        _ = ExhibitionAdvertiseServices().exhibitionAdvertise(apiURL, postData: params as [String : AnyObject], withSuccessHandler: { (userModel) in
            let model = userModel as! ExhibitionAdvertiseResponse
            let userData = model.exhibitiondetails
            
            let eventStartDateTime = userData["eventstartdatetime"]
            let eventEndDateTime = userData["eventenddatetime"]
            

            UserDefaults.standard.set(eventStartDateTime, forKey: StringConstants.NSUserDefauluterKeys.eventStartDateTime)

            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let eventStartDate = formatter.date(from: eventStartDateTime as! String)
            let eventEndDate = formatter.date(from: eventEndDateTime as! String)
            
            //            if self.isBetween(eventStartDate!, and: eventEndDate!) {
            //                self.setupScanbarcodeFooter()
            //            } else {
            //              self.setupListScanBarcodeFooter()
            //            }
            if self.isLessThan(Date(),and: eventEndDate!) {
                self.setupScanbarcodeFooter()
                self.isEventOngoing = true
            } else {
                self.setupListScanBarcodeFooter()
                self.isEventOngoing = false
            }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            
        })
    }
    
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(Date())
    }
    
    func isLessThan(_ date1: Date, and date2: Date) -> Bool {
        if date1 < date2  {
            return true
        }
        return false
    }
    
}
