//
//  LeftViewControllerCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 28/11/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class LeftViewControllerCell: UITableViewCell {

@IBOutlet weak var lblMenuList: UILabel!
@IBOutlet weak var imageViewOfLeftIconMenu: UIImageView!
    @IBOutlet weak var lblOfSepratorLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

