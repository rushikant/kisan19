//
//  RightViewController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 22/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class RightViewController: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet var lblOfNoAnyNotificationText: UILabel!
    @IBOutlet weak var tblRightDrawer: UITableView!
    @IBOutlet weak var lblnumberOfApplyFilter: UILabel!
   // var arrOfCheckSelectedCategory = NSMutableArray()
    var arrOfCheckSelectedCategory = [String]()
    var arrOfCheckPavilionCategory = [String]()

    var arrOfCatId = [String]()
    var arrOfCatName = [String]()
    var arrOfpavilions = [String]()

    var discoverChannelViewController: UIViewController!
    //  Notification Drawer
    var categories_list: [CategoriesList] = []
    var pavilion_list: [PavilionList] = []

    @IBOutlet weak var viewOfBackTblNotifiation: UIView!
    @IBOutlet weak var tblNotification: UITableView!
    var commingFromValue = String()
    var pavilionCount = 0
    //MARK: step 2 Create a delegate property here, don't forget to make it weak!
    var notificationList = [NotificationTable]()

    @IBOutlet var lblselectcategories: UILabel!
    @IBOutlet var btnclearAll: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDesign()
        
       
    }
    
    func initialDesign() {
        self.tblRightDrawer.separatorColor = UIColor.clear
        self.tblNotification.separatorColor = UIColor.white
        tblNotification.tableFooterView = UIView()
        tblRightDrawer.tableFooterView = UIView()
        
        tblRightDrawer.estimatedRowHeight = 59
        tblRightDrawer.rowHeight = UITableViewAutomaticDimension
        
        // long press recognizer
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.tblNotification.addGestureRecognizer(longPressGesture)
    }
    
    
    @objc func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.tblNotification)
        let indexPath = self.tblNotification.indexPathForRow(at: p)
      
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            print("Long press on row, at \(indexPath!.row)")
            popupAlert(title: "", message: (NSLocalizedString(StringConstants.AlertMessage.deleteNotofocation, comment: StringConstants.EMPTY)), actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnNo, comment: StringConstants.EMPTY)),(NSLocalizedString(StringConstants.AlertMessage.btnYes, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                    self.deleteNotification(indexPath:indexPath! as NSIndexPath)
                }, nil])
            
        }
    }
    
    func deleteNotification(indexPath:NSIndexPath){
        let notficationDetails = self.notificationList[indexPath.row]
        NotificationServices.sharedInstance.deleteNotiAtNotificationTable(notficationDetails: notficationDetails)
        self.notificationList.remove(at: indexPath.row)
        
        if self.notificationList.count == 0{
            lblOfNoAnyNotificationText.isHidden = false
            tblNotification.isHidden = true
        }else{
            lblOfNoAnyNotificationText.isHidden = true
            tblNotification.isHidden = false
            tblNotification.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Register to receive notification in your class
        lblselectcategories.text = (NSLocalizedString(StringConstants.RightViewController.SelectAll, comment: StringConstants.EMPTY))
        btnclearAll.setTitle((NSLocalizedString(StringConstants.RightViewController.ClearAll, comment: StringConstants.EMPTY)), for: .normal)
        lblOfNoAnyNotificationText.text = (NSLocalizedString(StringConstants.RightViewController.NoNoti, comment: StringConstants.EMPTY))
        NotificationCenter.default.addObserver(self, selector: #selector(self.setDrawer(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
    }

    // handle notification
    @objc func setDrawer(_ notification: NSNotification) {
        let commingFrom = notification.userInfo?[StringConstants.NSUserDefauluterKeys.commingFromScreen] as? String
            // do something with your image
            if commingFrom == StringConstants.flags.channelDashboardViewController  || commingFrom == StringConstants.flags.dashboardViewController || commingFrom == StringConstants.flags.fromJoinNotification{
                self.pavilion_list.removeAll()
                self.categories_list.removeAll()
                self.tblRightDrawer.isHidden = true
                viewOfBackTblNotifiation.isHidden = false
                self.tblNotification.isHidden = false
                self.notificationList = NotificationServices.sharedInstance.getNotificationDetails()
                if self.notificationList.count == 0{
                    lblOfNoAnyNotificationText.isHidden = false
                    tblNotification.isHidden = true
                }else{
                    lblOfNoAnyNotificationText.isHidden = true
                    self.notificationList.sort(by:{$0.sendAt! > $1.sendAt!} )
                    tblNotification.isHidden = false
                    tblNotification.reloadData()
                }
            }else{
            //Comming from discover channel
          // if (notification.userInfo?[StringConstants.NSUserDefauluterKeys.communityListData]) != nil {
                viewOfBackTblNotifiation.isHidden = true
                tblNotification.rowHeight = UITableViewAutomaticDimension
                tblNotification.estimatedRowHeight = 60.0
                self.tblNotification.isHidden = true
                self.tblRightDrawer.isHidden = false
//                let discoverData = notification.userInfo?[StringConstants.NSUserDefauluterKeys.communityListData]
//                self.callGetCategotiesAPI(communityDetails:discoverData as! [CommunityDetailTable])
                let discoverData = CommunityDetailService.sharedInstance.getCommunityDetailsForRightVC()
               let commingFrom = notification.userInfo?[StringConstants.NSUserDefauluterKeys.commingFromScreen] as? String
                 if commingFrom == StringConstants.flags.FromDiscoverPavilionFilter{
                    if self.pavilion_list.count > 0{
                        commingFromValue = StringConstants.flags.FromDiscoverPavilionFilter
                        self.tblRightDrawer.reloadData()
                    }else{
                    self.callGetPavilionFilterList(communityDetails:discoverData )
                    commingFromValue = StringConstants.flags.FromDiscoverPavilionFilter
                    }

                 }else if commingFrom == StringConstants.flags.FromDiscoverCategoryFilter{
                    if self.categories_list.count > 0{
                        commingFromValue = StringConstants.flags.FromDiscoverCategoryFilter
                        self.tblRightDrawer.reloadData()
                    }else{
                    self.callGetCategotiesAPI(communityDetails:discoverData )
                    commingFromValue = StringConstants.flags.FromDiscoverCategoryFilter
                    }
                }
           // }
        }
    }
    
    
    func callGetCategotiesAPI(communityDetails:[CommunityDetailTable]) {
        let params = ["":""]
        print("params",params)
        let path = Bundle.main.path(forResource: "categoriesList", ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        let apiURL = String(describing: fileUrl)
        _ = UserAreaOfInterestServices().getInterest(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! UserAreaOfInterestResponse
                                                        let categoriesList = model.categories_list
                                                        self.categories_list = categoriesList.filter( { (list: CategoriesList) -> Bool in
                                                            return list.is_occupation == false
                                                        })
                                                        self.arrOfCheckSelectedCategory.removeAll()
                                                        self.arrOfCatId.removeAll()
                                                        self.arrOfCatName.removeAll()
                                                        let categoryId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.interests)
                                                        for i in 0 ..< self.categories_list.count {
                                                            self.arrOfCheckSelectedCategory.append("0")
                                                            self.arrOfCatId.append("")
                                                            self.arrOfCatName.append("")
                                                            //Find category count from discover channel list
                                                            let category = self.categories_list[i]
                                                            
                                                            //filter data for show category follow by channel
                                                            let filterData   = communityDetails.filter { ($0.communityCategories)?.range(of: category.id!, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                                                            
                                                            let newCategory = CategoriesList.init(id: category.id, name: category.name, image_url: category.image_url!, is_occupation: category.is_occupation!, categoryCountOnDiscoverList: String(filterData.count))
                                                            self.categories_list.remove(at: i)
                                                            self.categories_list.insert(newCategory, at: i)
                                                        }
                                                        
                                                        var arrOfId = categoryId.components(separatedBy: ",")
                                                        arrOfId = arrOfId.filter { $0 != "" }
                                                        
                                                        for  i in (0..<arrOfId.count){
                                                            let selectedId = arrOfId[i]
                                                            //Get IndexValue
                                                            let index =    self.categories_list.index { (selectedCategory) -> Bool in
                                                                selectedCategory.id == selectedId
                                                            }
                                                            let nameOfCatgory = self.categories_list[index!]
                                                            let selectedCatName = nameOfCatgory.name
                                                            print("selectedCatName",selectedCatName)
                                                            self.arrOfCheckSelectedCategory.remove(at: index!)
                                                            self.arrOfCheckSelectedCategory.insert("1", at: index!)
                                                            self.arrOfCatId.remove(at: index!)
                                                            self.arrOfCatId.insert((selectedId), at: index!)
                                                            self.arrOfCatName.remove(at: index!)
                                                            self.arrOfCatName.insert((selectedCatName!), at: index!)
                                                        }
                                                        let arrOfTempCatId = self.arrOfCatId.filter { $0 != "" }
                                                        if arrOfTempCatId.count != 0 {
                                                            
                                                            
                                                            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                                                            if preferredLanguage == "en" {
                                                               self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                                                            } else if preferredLanguage == "mr" {
                                                                self.lblnumberOfApplyFilter.text =  String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            } else{
                                                                self.lblnumberOfApplyFilter.text =  String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            }
                                                            
                                                        }else if arrOfTempCatId.count == 0{
                                                            
                                                            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                                                            if preferredLanguage == "en" {
                                                                  self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                                                            } else if preferredLanguage == "mr" {
                                                                  self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            } else{
                                                                 self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            }
                                                        }
                                                        self.tblRightDrawer.reloadData()
                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    

    func callGetPavilionFilterList(communityDetails:[CommunityDetailTable]) {
        let params = ["":""]
        print("params",params)
        let path = Bundle.main.path(forResource: "pavilionCategories", ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        let apiURL = String(describing: fileUrl)
        _ = PavilionFilterService().getPavilionFilterlist(apiURL,
                                                     postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! PavilionFilterResponse
                                                        let pavilionList = model.pavilion_list
                                                        self.pavilion_list = pavilionList
                                                        self.arrOfCheckPavilionCategory.removeAll()
                                                        self.arrOfpavilions.removeAll()
                                                        for i in 0 ..< self.pavilion_list.count {
                                                            
                                                            //Find category count from discover channel list
                                                            let category = self.pavilion_list[i]
                                                           
                                                            self.arrOfCheckPavilionCategory.append("0")
                                                            self.arrOfpavilions.append(StringConstants.no)
                                                            //filter data for show category follow by channel
                                                            let filterData   = communityDetails.filter { ($0.pavilion_name)?.range(of: category.name!, options: [.diacriticInsensitive, .caseInsensitive]) != nil }

                                                            let newCategory = PavilionList.init(name: category.name, image_url: category.image_url!, categoryCountOnDiscoverList: String(filterData.count))
                                                            self.pavilion_list.remove(at: i)
                                                            self.pavilion_list.insert(newCategory, at: i)
                                                        }
                                                            
                                                            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                                                            if preferredLanguage == "en" {
                                                                self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                                                            } else if preferredLanguage == "mr" {
                                                                self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            } else{
                                                                self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                                                            }
                                                        self.tblRightDrawer.reloadData()
                                                        
        }, withFailureHandlere: { (error: String) in
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnClearAllClick(_ sender: Any) {
        
        if commingFromValue == StringConstants.flags.FromDiscoverCategoryFilter{
           for i in 0 ..< arrOfCheckSelectedCategory.count {
              arrOfCheckSelectedCategory.remove(at: i)
              arrOfCheckSelectedCategory.insert("0", at: i)
              arrOfCatId.remove(at: i)
              arrOfCatId.insert("", at: i)
              self.arrOfCatName.remove(at: i)
              self.arrOfCatName.insert("", at: i)
            
            let arrOfTempCatId = arrOfCatId.filter { $0 != "" }
            if arrOfTempCatId.count == 0{
                // lblnumberOfApplyFilter.text = StringConstants.RightViewController.apply + "0" +  StringConstants.RightViewController.filters
                let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text = "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
             }
          }
        }else{
            //new code
            
            for i in 0 ..< arrOfCheckPavilionCategory.count {
                arrOfCheckPavilionCategory.remove(at: i)
                arrOfCheckPavilionCategory.insert("0", at: i)
                arrOfpavilions.remove(at: i)
                arrOfpavilions.insert(StringConstants.no, at: i)
                
                let arrOfTempaPavilion = arrOfpavilions.filter { $0 != StringConstants.no }
                if arrOfTempaPavilion.count == 0{
                    // lblnumberOfApplyFilter.text = StringConstants.RightViewController.apply + "0" +  StringConstants.RightViewController.filters
                    let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                    if preferredLanguage == "en" {
                        self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                    } else if preferredLanguage == "mr" {
                        self.lblnumberOfApplyFilter.text =  "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                    } else{
                        self.lblnumberOfApplyFilter.text = "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                    }
                }
            }
            
            /*print(arrOfCheckPavilionCategory)
            arrOfCheckPavilionCategory.removeAll()
            pavilionCount = 0
            if arrOfCheckPavilionCategory.count == 0{
                // lblnumberOfApplyFilter.text = StringConstants.RightViewController.apply + "0" +  StringConstants.RightViewController.filters
                let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text = "0" + (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
            }*/
        }
          tblRightDrawer.reloadData()
    }
    
    
    @IBAction func btnApplyFilterClick(_ sender: Any) {
        
        if commingFromValue == StringConstants.flags.FromDiscoverCategoryFilter{

        if arrOfCatId.count > 0 {
            var arrCatId = arrOfCatId
            arrCatId = arrOfCatId.filter { $0 != "" }

            var arrCatName = arrOfCatName
            arrCatName = arrOfCatName.filter { $0 != "" }
            print("arrCatName",arrCatName)
            
            var arrOfCategoryData : Dictionary = [String : [String]]()
            arrOfCategoryData["categoriesId"] = arrCatId
            arrOfCategoryData["categoriesName"] = arrCatName
            
            if arrCatId.count>0{
            // Set flag for open notifiation drawer from right drawer
            let nitificationDataDict:[String: [String : [String]]] = ["categories": arrOfCategoryData]//arrCatId
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.flags.ApplyFilter), object: nil, userInfo: nitificationDataDict)
                removeRightDrawerOnApplyFiterButton()
            }else{

           self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterValidation, comment: StringConstants.EMPTY)))
            }
          }else{
             self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterValidation, comment: StringConstants.EMPTY)))
          }
        }else{
            
            // new code
            if arrOfpavilions.count > 0 {
                var arrPavilion = arrOfpavilions
                arrPavilion = arrOfpavilions.filter { $0 != StringConstants.no }
                
                if arrPavilion.count>0{
                    // Set flag for open notifiation drawer from right drawer
                    let nitificationDataDict:[String: [String]] = ["Pavilions": arrPavilion]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.flags.ApplyPavilionFilter), object: nil, userInfo: nitificationDataDict)
                    removeRightDrawerOnApplyFiterButton()
                }else{
                    
                    self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterValidation, comment: StringConstants.EMPTY)))
                }
            }else{
                self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterValidation, comment: StringConstants.EMPTY)))
            }
            
            
            
           /* if arrOfCheckPavilionCategory.count > 0 {
                    //  let strCatId = arrCatId.joined(separator: ",")
                    // Set flag for open notifiation drawer from right drawer
                    let nitificationDataDict:[String: [String]] = ["Pavilions": arrOfCheckPavilionCategory]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Apply Pavilion Filter"), object: nil, userInfo: nitificationDataDict)
                   arrOfCheckPavilionCategory.removeAll()
                   self.pavilionCount = 0
                    removeRightDrawerOnApplyFiterButton()
            }else{
                self.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.applyeFilterValidation, comment: StringConstants.EMPTY)))
            }*/
        }
    }
}


extension RightViewController :
UITableViewDataSource,UITableViewDelegate {
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblRightDrawer{
            if commingFromValue == StringConstants.flags.FromDiscoverCategoryFilter{
            return  self.categories_list.count
            }else{
                return  self.pavilion_list.count
            }
        }else{
            return self.notificationList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         tableView.allowsSelection = true

        if tableView == tblRightDrawer{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewControllerCell", for: indexPath) as! RightViewControllerCell
            cell.selectionStyle = .none
            if commingFromValue == StringConstants.flags.FromDiscoverCategoryFilter{
                lblselectcategories.text = (NSLocalizedString(StringConstants.RightViewController.SelectAll, comment: StringConstants.EMPTY))
                let category = self.categories_list[(indexPath as NSIndexPath).row]
                let strCheckSelectCat = arrOfCheckSelectedCategory[indexPath.row]
                if strCheckSelectCat == "1"  {
                    cell.viewOfBackCategory.backgroundColor = UIColor.white
                    cell.lblOfCategoryName.textColor = UIColor.black
                    cell.lblOfcategoryCountOnDiscList.textColor = UIColor.black
                }else{
                    cell.lblOfCategoryName.textColor = UIColor.white
                    cell.lblOfcategoryCountOnDiscList.textColor = UIColor.white
                    cell.viewOfBackCategory.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.selectedCellOfRightDrawer)
                }
                cell.imageViewOfCategory.sd_setImage(with: URL(string: category.image_url!), placeholderImage:UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
                
                cell.lblOfCategoryName.text = category.name
                cell.lblOfcategoryCountOnDiscList.text = String(category.categoryCountOnDiscoverList!)
            }else{
                //new code
                lblselectcategories.text = (NSLocalizedString(StringConstants.RightViewController.SelectPavilion, comment: StringConstants.EMPTY))
                let strCheckSelectCat = arrOfCheckPavilionCategory[indexPath.row]
                if strCheckSelectCat == "1"  {
                    cell.viewOfBackCategory.backgroundColor = UIColor.white
                    cell.lblOfCategoryName.textColor = UIColor.black
                    cell.lblOfcategoryCountOnDiscList.textColor = UIColor.black
                }else{
                    cell.lblOfCategoryName.textColor = UIColor.white
                    cell.lblOfcategoryCountOnDiscList.textColor = UIColor.white
                    cell.viewOfBackCategory.backgroundColor = UIColorFromRGB.init(rgb: ColorConstants.selectedCellOfRightDrawer)
                }
                let pavilion = self.pavilion_list[(indexPath as NSIndexPath).row]
                cell.imageViewOfCategory.image = UIImage(named: pavilion.image_url!)
                if pavilion.name! == "Open"{
                    cell.lblOfCategoryName.text = "Open Arena"
                }else if pavilion.name! == "Enterprise" {
                    cell.lblOfCategoryName.text = "Enterprise Pavilion"
                }else if pavilion.name! == "Plasticulture & Protective cultivation" {
                    cell.lblOfCategoryName.text = "Protective Cultivation"
                }else if pavilion.name! == "Gramvikas"{
                    cell.lblOfCategoryName.text = "GramVikas"
                }else{
                    cell.lblOfCategoryName.text = pavilion.name!
                }
                
                cell.lblOfcategoryCountOnDiscList.text = String(pavilion.categoryCountOnDiscoverList!)
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewControllerCell", for: indexPath) as! RightViewControllerCell
            cell.selectionStyle = .none
            let notificationData = self.notificationList[indexPath.row]
            let commnunityNameStyle = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
            let otherTextStyle = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)]
            let communityName = NSMutableAttributedString(string: notificationData.communityName!, attributes: commnunityNameStyle)
            let singleSpace = NSMutableAttributedString(string: StringConstants.singleSpace)
            let otherText = NSMutableAttributedString(string:(NSLocalizedString(StringConstants.RightViewController.Requested, comment: StringConstants.EMPTY)) , attributes: otherTextStyle)
            let combinationText = NSMutableAttributedString()
            combinationText.append(communityName)
            combinationText.append(singleSpace)
            combinationText.append(otherText)
            cell.lblOfCategoryName.attributedText = combinationText
            cell.imageViewOfCategory.layer.cornerRadius = cell.imageViewOfCategory.frame.size.width / 2
            cell.imageViewOfCategory.clipsToBounds = true

            if let communityImageUrl = notificationData.communityImageUrl{
                cell.imageViewOfCategory.sd_setImage(with: URL(string: communityImageUrl), placeholderImage:UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg), options: .refreshCached)
            }else{
                cell.imageViewOfCategory.image = UIImage(named: StringConstants.ImageNames.categotyPlaceholderImg)
            }
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == tblRightDrawer{
        if commingFromValue == StringConstants.flags.FromDiscoverCategoryFilter{
            let checkFollower = arrOfCheckSelectedCategory[indexPath.row]
            let category = self.categories_list[(indexPath as NSIndexPath).row]
            if checkFollower == "0"{
                arrOfCheckSelectedCategory.remove(at: indexPath.row)
                arrOfCheckSelectedCategory.insert("1", at: indexPath.row)
                arrOfCatId.remove(at: indexPath.row)
                arrOfCatId.insert(category.id!, at: indexPath.row)
                arrOfCatName.remove(at: indexPath.row)
                arrOfCatName.insert(category.name!, at: indexPath.row)
                
            }else{
                arrOfCheckSelectedCategory.remove(at: indexPath.row)
                arrOfCheckSelectedCategory.insert("0", at: indexPath.row)
                arrOfCatId.remove(at: indexPath.row)
                arrOfCatId.insert("", at: indexPath.row)
                arrOfCatName.remove(at: indexPath.row)
                arrOfCatName.insert("", at: indexPath.row)
            }
            
            let arrOfTempCatId = arrOfCatId.filter { $0 != "" }
            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
            if arrOfTempCatId.count != 0 {
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text =  String(arrOfTempCatId.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
            }else if arrOfTempCatId.count == 0{
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
            }
            
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            tblRightDrawer.reloadRows(at: [indexPath], with: .none)
            
            }else{
            
            //new code
            let checkFollower = arrOfCheckPavilionCategory[indexPath.row]
            let pavilion = self.pavilion_list[(indexPath as NSIndexPath).row]
            if checkFollower == "0"{
                arrOfCheckPavilionCategory.remove(at: indexPath.row)
                arrOfCheckPavilionCategory.insert("1", at: indexPath.row)
                
                arrOfpavilions.remove(at: indexPath.row)
                arrOfpavilions.insert(pavilion.name!, at: indexPath.row)
            }else{
                arrOfCheckPavilionCategory.remove(at: indexPath.row)
                arrOfCheckPavilionCategory.insert("0", at: indexPath.row)
                
                arrOfpavilions.remove(at: indexPath.row)
                arrOfpavilions.insert(StringConstants.no, at: indexPath.row)
            }
            
            let arrOfTempPavilion = arrOfpavilions.filter { $0 != StringConstants.no }
            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
            if arrOfTempPavilion.count != 0 {
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + String(arrOfTempPavilion.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  String(arrOfTempPavilion.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) + (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text =  String(arrOfTempPavilion.count) +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
            }else if arrOfTempPavilion.count == 0{
                if preferredLanguage == "en" {
                    self.lblnumberOfApplyFilter.text = (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY)) + "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY))
                } else if preferredLanguage == "mr" {
                    self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                } else{
                    self.lblnumberOfApplyFilter.text =  "0" +  (NSLocalizedString(StringConstants.RightViewController.filters, comment: StringConstants.EMPTY)) +        (NSLocalizedString(StringConstants.RightViewController.apply, comment: StringConstants.EMPTY))
                }
            }
            
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            tblRightDrawer.reloadRows(at: [indexPath], with: .none)
            }
        }else{
            let notificationData = self.notificationList[indexPath.row]
            var  dict  = ConvertToDictNotificationObject.ConvertToDictNotificationObject(notificationDetails: [notificationData])
            print("dict",dict)
            dict.updateValue(StringConstants.ZERO, forKey: "isMember")
            let communityWithRssFeed = notificationData.communityWithRssFeed
            dict.updateValue(communityWithRssFeed ?? String(), forKey: "feed")
            let myChatData = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict)
            
            // Set flag for open notifiation drawer from right drawer (move to channel profile)
            let nitificationDataDict:[String: MyChatsTable] = [StringConstants.pushNotificationConstants.myChatData: myChatData]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstants.pushNotificationConstants.channelProfileFrmNotification), object: nil, userInfo: nitificationDataDict)
            self.removeRightDrawerOnApplyFiterButton()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if tableView == tblRightDrawer{
            return 60
        }else{
            return UITableViewAutomaticDimension
        }
    }
}
