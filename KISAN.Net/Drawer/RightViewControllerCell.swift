//
//  RightViewControllerCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 22/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class RightViewControllerCell: UITableViewCell {
    @IBOutlet weak var lblOfcategoryCountOnDiscList: UILabel!
    @IBOutlet weak var lblOfCategoryName: UILabel!
    @IBOutlet weak var imageViewOfCategory: UIImageView!
    @IBOutlet weak var viewOfBackCategory: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
