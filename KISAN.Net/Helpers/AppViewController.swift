//
//  AppViewController.swift
//  LBA
//
//  Created by MacMini on 13/07/19.
//  Copyright © 2019 LBA. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD
import AVFoundation

class AppViewController: UIViewController{
    //MARK: - Check Network
    func reachability() -> Bool {
        let reachability = Reachability()
        switch reachability?.connection {
        case .wifi?:
            print("Reachable via WiFi")
            return true
        case .cellular?:
            print("Reachable via Cellular")
            return true
        case .none:
            print("Network not reachable")
            return false
        case .some(.none):
            print("Network not reachable")
            return false
        }
    }
    
    func ShowHud(){
        self.view.isUserInteractionEnabled=false
        SVProgressHUD.show()
    }
    func DismissHud(){
        self.view.isUserInteractionEnabled=true
        SVProgressHUD.dismiss()
    }
    
    //MARK: WebService call methods
    
    func performRequest(_ method: HTTPMethod, requestURL: String, params: [String:Any] ,delegate:AnyObject) {
        let requesturl = requestURL
        
        if self.reachability() {
            
            Alamofire.request(requesturl, method: method, parameters: params).responseJSON { response in
                print(response)
                switch(response.result) {
                case .success(_):
                    let responseDict = response.result.value! as? [String: Any]
                    print(responseDict!)
                    
                    
                    if (responseDict!["success"] != nil)
                    {
                        if (responseDict!["success"] as! Bool) == true
                        {
                           
                                self.didFinishLoading(result: responseDict! as NSDictionary, methodName: requestURL)
                            
                        }
                        else{
                            
                            self.didFailed(error: responseDict!["message"] as! String, result: responseDict! as NSDictionary, methodName: requestURL)
                            
                        }
                    }
                    else{
                        
                        self.didFailed(error: responseDict!["message"] as! String, result: responseDict! as NSDictionary, methodName: requestURL)
                        
                    }
                    break
                case .failure(let error):
                    print(error)
                    if error._code == -1005 {
                        
                    }
                    if error._code ==  -1001 {
                    }
                    self.DismissHud()
                    break
                }
            }
        }else{
            self.DismissHud()
()
            let alert = UIAlertController(title: "Alert", message: "No internet connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler:{(UIAlertAction)in
                self.ShowHud()
                self.performRequest(.post, requestURL: requestURL, params: params ,delegate:delegate)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Local Service Responder
    func didFinishLoading(result: NSDictionary, methodName: String)
    {
        
    }
    func didFinishLoading(result: Data, methodName: String)
    {
        
    }
    func commentSent(result: NSDictionary, methodName: String)
    {
        
    }
    
    func didFailed(error: String,result: NSDictionary, methodName: String)
    {
    }
    
}
