//
//  ButtonAnimationHelper.swift
//  KISAN.Net
//
//  Created by Rushikant on 01/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

class ButtonAnimationHelper: UIViewController {
    public func animateButton (button: UIButton!) {
        button.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        
        UIView.animate(withDuration: 3.0,
        delay: 0,
        usingSpringWithDamping: 0.2,
        initialSpringVelocity: 3.0,
        options: [.allowUserInteraction, .repeat],
        animations: { [weak self] in
            button.transform = .identity
        },
        completion: nil)
    }
    
    public func blinkView(view: UIView) {
        UIView.animate(withDuration: 0.8,
        delay:0.0,
        options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
        animations: { view.alpha = 0 },
        completion: nil)
    }
    
    public func animateView(view: UIView) {
        view.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        UIView.animate(withDuration: 3.0,
        delay: 0,
        usingSpringWithDamping: 0.2,
        initialSpringVelocity: 3.0,
        options: [.allowUserInteraction, .repeat],
        animations: { [weak self] in
            view.transform = .identity
        },
        completion: nil)
    }
}
