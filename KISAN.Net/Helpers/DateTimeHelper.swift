//
//  DateTimeHelper.swift
//  KisanChat
//
//  Created by Kisan-MAC on 11/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

class DateTimeHelper {
    
    public static func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    public static func getCurrentDateTime()->String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let timeInHrs = formatter.string(from: date as Date)
        return timeInHrs
    }
    
    public static func convertMillisToHoursMinute(time:String)->String {
        let date = NSDate(timeIntervalSince1970: (Double(time))! / 1000)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let timeInHrs = formatter.string(from: date as Date)
        return timeInHrs
    }
    
    public static func convertMillisToDateHoursMinute(time:String)->String {
        let date = NSDate(timeIntervalSince1970: (Double(time))! / 1000)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM hh:mm a"
        let timeInHrs = formatter.string(from: date as Date)
        return timeInHrs
    }
    
    static let SECOND_MILLIS = 1000
    static let MINUTE_MILLIS = 60 * SECOND_MILLIS
    static let HOUR_MILLIS =  60 * MINUTE_MILLIS
    static let DAY_MILLIS = 24 * HOUR_MILLIS
    
    public static func getTimeAgo(time:String)->String {
        var timeStamp = Int(time)
        if timeStamp! < 1000000000000 {
            timeStamp = timeStamp! * 1000
        }
        let now = self.getCurrentMillis()
        if (timeStamp! > now || timeStamp! <= 0) {
            return "0"
        }
        // TODO: localize
        let diff = Int(now) - timeStamp!;
        if (diff < MINUTE_MILLIS) {
            return (NSLocalizedString(StringConstants.DashboardViewController.now, comment: StringConstants.EMPTY))
        } else if (diff < 2 * MINUTE_MILLIS) {
            return (NSLocalizedString(StringConstants.DashboardViewController.aMinuteAgo, comment: StringConstants.EMPTY))
        } else if (diff < 50 * MINUTE_MILLIS) {
            let minuteValue = diff / MINUTE_MILLIS
            return String(minuteValue) + (NSLocalizedString(StringConstants.DashboardViewController.minutesAgo, comment: StringConstants.EMPTY))
        } else if (diff < 90 * MINUTE_MILLIS) {
            return (NSLocalizedString(StringConstants.DashboardViewController.anHourAgo, comment: StringConstants.EMPTY));
        } else if (diff < 12 * HOUR_MILLIS) {
            //let hoursValue = diff / HOUR_MILLIS
            let date = NSDate(timeIntervalSince1970: (Double(time))! / 1000)
            let formatter = DateFormatter()
            //let localeFormatString = DateFormatter.dateFormat(fromTemplate: "hh:mm a", options: 0, locale: formatter.locale)
            
            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
            if preferredLanguage == "en" {
                formatter.dateFormat = "hh:mm a"
                formatter.amSymbol = "am"
                formatter.pmSymbol = "pm"
                formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
            } else if preferredLanguage == "mr" {
                formatter.dateFormat = "hh:mm a"
                formatter.amSymbol = "am"
                formatter.pmSymbol = "pm"
                formatter.locale = NSLocale(localeIdentifier: "mr") as Locale
            }else{
                formatter.dateFormat = "hh:mm a"
                formatter.amSymbol = "am"
                formatter.pmSymbol = "pm"
                formatter.locale = NSLocale(localeIdentifier: "hi") as Locale
            }
            let timeInHrs = formatter.string(from: date as Date)
            return timeInHrs
            
        } else if (diff < 24 * HOUR_MILLIS) {
            return (NSLocalizedString(StringConstants.DashboardViewController.yesterday, comment: StringConstants.EMPTY))
        } else {
            let dayMillisValue = diff / DAY_MILLIS
            let date = Date()
            let formatter = DateFormatter()
            let preferredLanguage = Locale.components(fromIdentifier: Locale.preferredLanguages[0])["kCFLocaleLanguageCodeKey"]
            if preferredLanguage == "en" {
                formatter.dateFormat = "dd MMM"
                formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
            } else if preferredLanguage == "mr" {
                formatter.dateFormat = "dd MMM"
                formatter.locale = NSLocale(localeIdentifier: "mr") as Locale
            }else{
                formatter.dateFormat = "dd MMM"
                formatter.locale = NSLocale(localeIdentifier: "hi") as Locale
            }
            let lastDate = date.addingTimeInterval(TimeInterval(-(dayMillisValue) * 24 * 60 * 60))
            print("7 days ago: \(formatter.string(from: lastDate))")
            return formatter.string(from: lastDate)
        }
    }

}

extension Date {
    
    func dateFormatWithSuffix() -> String {
        return "dd'\(self.daySuffix())' MMMM"
    }
    
    func daySuffix() -> String {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components(.day, from: self)
        let dayOfMonth = components.day
        switch dayOfMonth {
        case 1, 21, 31:
            return "st"
        case 2, 22:
            return "nd"
        case 3, 23:
            return "rd"
        default:
            return "th"
        }
    }
}
