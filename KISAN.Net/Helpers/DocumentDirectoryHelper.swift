//
//  DocumentDirectoryHelper.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class DocumentDirectoryHelper: NSObject {
    
    func createDirectory(){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(StringConstants.documentDirectory.documentDirectoryName)
        print("paths",paths)
        if !fileManager.fileExists(atPath: paths){
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            print("Already dictionary created.")
        }
    }
    
    func saveImageDocumentDirectory(imageName:String,sourceType:String,imageData:NSData){
        if sourceType == StringConstants.MediaType.video{
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: AnyObject = paths[0] as AnyObject
            let dataPath = documentsDirectory.appending(imageName)
            //let dataPath = documentsDirectory.appending("/vid1.mp4") 
            print("dataPath",dataPath)
            imageData.write(toFile: dataPath, atomically: false)
        }else{
            let fileManager = FileManager.default
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
            print(paths)
            fileManager.createFile(atPath: paths as String, contents: imageData as Data, attributes: nil)
        }
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(imageName:String)-> UIImage{
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePAth){
            if let image = UIImage(contentsOfFile: imagePAth){
              return image//Return this value
            }
        }else{
            print("No Image")
        }
        return UIImage()
    }
    
    func getVideoPath(videoName:String) -> URL{
        let fm = FileManager.default
        let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let path = docsurl.appendingPathComponent(videoName)
        return path
    }
    
    func deleteDirectory(){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(StringConstants.documentDirectory.documentDirectoryName)
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        }else{
            print("Something wronge.")
        }
    }
    
    func checkImageORVideoIsPresent(imageName:String)->Bool{
        var isPresent = Bool()
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(imageName).path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            print("FILE AVAILABLE")
            isPresent = true
        } else {
            print("FILE NOT AVAILABLE")
            isPresent = false
        }
        return isPresent
    }
}
