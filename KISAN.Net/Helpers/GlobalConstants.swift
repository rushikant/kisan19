//
//  GlobalConstants.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 18/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class GlobalConstants: NSObject {
    // FIXME: - properties
    var dictOfCameraMultiMediaPart: NSMutableDictionary?
    
    // FIXME: - initializer
    private override init() {
    }
    
    // FIXME: - shared singleton instance
    static let shared: GlobalConstants = {
        let instance = GlobalConstants()
        return instance
    }()
    
}
