//
//  SessionManager.swift
//  KisanChat
//
//  Created by KISAN TEAM on 09/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

class SessionManagerForSaveData{
    
    class func saveDataInSessionManager(valueString: String, keyString: String) {
        UserDefaults.standard.set(valueString, forKey: keyString)
    }
    
    
    class func retriveDataFromSessionManager(keyOfRetriveData:String) -> String {
        let  retriveString = UserDefaults.standard.string(forKey: keyOfRetriveData)
        if(retriveString == nil){
           return ""
        }else{
            return retriveString!
        }
     }
}
final class SessionDetailsForCommunityType {
    
    // FIXME: - properties
    
    var communityTypeChannel: String?
    var dictOfCameraMultiMediaPart: NSDictionary?
    //Use this global variable for one to one chat (for get userCommunityJabberId save in mychat)
    var globalMyChatsData = MyChatsTable()
    var changePendingMessageStatus : Bool = false
    // FIXME: - initializer
    private init() {
        
    }
    
    // FIXME: - shared singleton instance
    static let shared: SessionDetailsForCommunityType = {
        let instance = SessionDetailsForCommunityType()
        return instance
    }()
}


/*final class SendChatData {
    
    // FIXME: - properties
    
    var sendChatdataFlag = false
    
    // FIXME: - initializer
    private init?() {
        return nil
    }
    
    // FIXME: - shared singleton instance
    static let shared: SendChatData = {
        let instance = SendChatData()
        return instance!
    }()
}*/

private var _SingletonSharedInstance:SendChatData! = SendChatData()

class SendChatData {
    var sendChatdataFlag = false
    
    class var sharedInstance : SendChatData {
        return _SingletonSharedInstance
    }
    
    init () {}
    
    func destroy() {
        _SingletonSharedInstance = nil
    }
}


