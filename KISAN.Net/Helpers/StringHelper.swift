//
//  StringHelper.swift
//  KisanChat
//
//  Created by Kisan-MAC on 03/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

class StringHelper{
    static func isNilOrEmpty(string: String!) -> Bool!{
        return string?.isEmpty ?? true
    }
}

class StringUTFEncoding{
    static func UTFEncong(string: String) -> String!{
        
            var communityName = string.removingPercentEncoding!
            communityName = communityName.replacingOccurrences(of: "+", with: " ")
            return communityName
    }
}


class StringUTFDecoding{
    static func UTFDecoding(string: String) -> String!{
        //let str = String(UTF8String: strToDecode.cStringUsingEncoding(NSUTF8StringEncoding))
        let str = string.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        return str
    }
}
