//
//  UUIDHelper.swift
//  KisanChat
//
//  Created by KISAN TEAM on 23/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import Foundation

class UUIDHelper {
    public static func getUUID()->String {
        var uuid = UUID().uuidString
        uuid = uuid.lowercased()
        return uuid
    }
}
