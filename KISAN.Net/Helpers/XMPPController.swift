
//
//  XMPPController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
import XMPPFramework
import AVFoundation
import UIKit

enum XMPPControllerError: Error {
    case wrongUserJID
}
class XMPPController: NSObject,UIAlertViewDelegate {
    var xmppStream: XMPPStream
    let hostName: String
    let userJID: XMPPJID
    let hostPort: UInt16
    let password: String
    var xmppReconnect: XMPPReconnect
    var xmppAutoPing: XMPPAutoPing
    var player : AVAudioPlayer?
    let fetOldMessageGroup = DispatchGroup()

    init(hostName: String, userJIDString: String, hostPort: UInt16 = 5222, password: String) throws {
        guard let userJID = XMPPJID(string: userJIDString) else {
            throw XMPPControllerError.wrongUserJID
        }
        self.hostName = hostName
        self.userJID = userJID
        self.hostPort = hostPort
        self.password = password
        // Stream Configuration
        self.xmppStream = XMPPStream()
        self.xmppReconnect = XMPPReconnect()
        self.xmppAutoPing = XMPPAutoPing()
        self.xmppStream.hostName = hostName
        self.xmppStream.hostPort = hostPort
        self.xmppStream.startTLSPolicy = XMPPStreamStartTLSPolicy.allowed
        self.xmppStream.myJID = userJID
        super.init()
        self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
       
        self.xmppReconnect = XMPPReconnect(dispatchQueue: DispatchQueue.main)
        self.xmppReconnect.activate(self.xmppStream)
        self.xmppReconnect.addDelegate(self, delegateQueue: DispatchQueue.main)
        self.xmppReconnect.autoReconnect = true
        
        self.xmppAutoPing =  XMPPAutoPing(dispatchQueue: DispatchQueue.main)
        self.xmppAutoPing.pingInterval = 20
        self.xmppAutoPing.pingTimeout = 20
        self.xmppAutoPing.activate(self.xmppStream)
        self.xmppAutoPing.addDelegate(self, delegateQueue: DispatchQueue.main)

    }
    
    func connect() {
        if !self.xmppStream.isDisconnected {
            return
        }
        
        try! self.xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
    }
}


extension XMPPController: XMPPStreamDelegate {
    func xmppStreamDidConnect(_ stream: XMPPStream) {
        print("Stream: Connected")
        try! stream.authenticate(withPassword: self.password)
        
        // For fetching messages, when receiver goes offline mode
       let myChatsList = MyChatServices.sharedInstance.getMyChatDetails()
        for i in 0 ..< myChatsList.count {
            let mychatData = myChatsList[i]
            let  userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
            if mychatData.ownerId == userId {
                let oneToOneMyChatsList = MyChatServices.sharedInstance.getOneToOneMyChatDetails(communityKey: mychatData.communityKey!)
                for j in 0 ..< oneToOneMyChatsList.count {
                    self.fetOldMessageGroup.enter()
                    let oneToOnemychatData = oneToOneMyChatsList[j]
                    self.getOldMessage(communityKey:oneToOnemychatData.communityKey!,isOneToOne:true)
                }

            }else{
                self.fetOldMessageGroup.enter()
                self.getOldMessage(communityKey:mychatData.communityKey!,isOneToOne:false)
            }
        }
    }
    
    //Call get old messages API
    func getOldMessage (communityKey:String, isOneToOne:Bool){
        let params = ["communityKey":communityKey]
        var apiURL = String()
        let currentTime = DateTimeHelper.getCurrentMillis()
        if isOneToOne == true {
            var simpleComunityKey = communityKey
            if let dotRange = simpleComunityKey.range(of: "_") {
                simpleComunityKey.removeSubrange(dotRange.lowerBound..<simpleComunityKey.endIndex)
            }
            apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + simpleComunityKey + URLConstant.getOldMessages2 + "10"
            apiURL = apiURL + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessages4 + communityKey
            apiURL = apiURL + URLConstant.getOldMegOneToOneTimpStmp + String(currentTime)            
        }else{
            apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + "10" + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        }
        print("apiURL",apiURL)
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            self.fetOldMessageGroup.leave()
                                                            
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            /*self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
             },{action2 in
             }, nil])*/
        })
    }
    
    func xmppStreamDidDisconnect(_ sender: XMPPStream, withError error: Error?) {
        print("xmppStreamDidDisconnect")
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        self.xmppStream.send(XMPPPresence())
        print("Stream: Authenticated at xmpp")
        // send pending messages
        self.sendPendingMessages()
    }
    
    func sendPendingMessages(){
        let pendingMessages = MessagesServices.sharedInstance.getPendingMessagesInOffline()
        print("pendingCount",pendingMessages.count)
        for i in 0 ..< pendingMessages.count {
            let pendingMSg = pendingMessages[i]
            let createSendMessageRequestInDB = buildRequestForSendMessage(id: pendingMSg.id!, mediaType: pendingMSg.mediaType!, messageType: pendingMSg.messageType!, contentField1: pendingMSg.contentField1!, contentField2: pendingMSg.contentField2!, contentField3: pendingMSg.contentField3!, contentField4: pendingMSg.contentField4!, contentField5: pendingMSg.contentField5!, contentField6: pendingMSg.contentField6!, contentField7: pendingMSg.contentField7!, contentField8: pendingMSg.contentField8!, contentField9: pendingMSg.contentField9!, contentField10: pendingMSg.contentField10!, senderId: pendingMSg.senderId!, communityId: pendingMSg.communityId!, jabberId: pendingMSg.jabberId!, createdDatetime: pendingMSg.createdDatetime!, hiddenDatetime: pendingMSg.hiddenDatetime!, deletedDatetime: pendingMSg.deletedDatetime!, hiddenbyUserId: pendingMSg.hiddenbyUserId!, deletedbyUserId: pendingMSg.deletedbyUserId!, messageStatus: StringConstants.Status.sent,communityName: pendingMSg.communityName ?? "",communityProfileUrl:pendingMSg.communityProfileUrl ?? "")
            
            let sendMessagesServices  = SendMessagesServices()
            sendMessagesServices.groupChat(createSendMessageRequest: createSendMessageRequestInDB, messageGuid: pendingMSg.id!, communityJabberId:pendingMSg.jabberId! )
            //Update message in core data as local DB with sent status
            MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: createSendMessageRequestInDB)
            SessionDetailsForCommunityType.shared.changePendingMessageStatus = true
        }
    }
    
    
    func buildRequestForSendMessage(id: String, mediaType: String, messageType: String, contentField1: String, contentField2: String, contentField3: String, contentField4: String, contentField5: String, contentField6: String, contentField7: String, contentField8: String, contentField9: String, contentField10: String, senderId: String, communityId: String, jabberId: String, createdDatetime: String, hiddenDatetime: String, deletedDatetime: String, hiddenbyUserId: String, deletedbyUserId: String,messageStatus:String,communityName:String,communityProfileUrl:String)-> Dictionary<String, Any>{
        
        let  createSendMessageRequest = MessageRequest.convertToDictionary(sendMessage: MessageRequest.init(id: id, mediaType: mediaType, messageType: messageType, contentField1: contentField1, contentField2: contentField2, contentField3: contentField3, contentField4: contentField4, contentField5: contentField5, contentField6: contentField6, contentField7: contentField7, contentField8: contentField8, contentField9: contentField9, contentField10: contentField10, senderId: senderId, communityId: communityId, jabberId: jabberId, createdDatetime: createdDatetime, hiddenDatetime: hiddenDatetime, deletedDatetime: deletedDatetime, hiddenbyUserId: hiddenbyUserId, deletedbyUserId: deletedbyUserId,messageStatus:messageStatus, communityName: communityName, communityProfileUrl: communityProfileUrl))
        print("createSendMessageRequest",createSendMessageRequest)
        return createSendMessageRequest
    }
    
    
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Stream: Fail to Authenticate")
    }
    @objc func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        print("message Received at xmppcontroller class",message)
        if message.childCount > 1{
            let createSendMessageRequest = XmppBroadCastMessageReceiveResponse.adminBroadCastMsgReceiveResponse(message: message)
           // let messageKey = createSendMessageRequest["id"] as? String
            var updatedmessageData = createSendMessageRequest.0
            if let  messageKey = updatedmessageData["id"] as? String {
                let messageKeyAvaiblity = MessagesServices.sharedInstance.checkMessagePresentInDB(messageKey: messageKey)
                // Play sound
               // let dontPlaySoundForThisCommunityKey = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey)
                let communityKey = updatedmessageData["communityId"] as! String
                var dontPlaySoundForThisCommunityKey = StringConstants.EMPTY
                if let dontPlaySound = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.DontPlaySoundForThisCommunityKey) {
                    dontPlaySoundForThisCommunityKey = dontPlaySound
                }
                if dontPlaySoundForThisCommunityKey != communityKey {
                    if communityKey.range(of:"_") == nil{
                        print("communityKey123",communityKey)
                        let communityDetailsOnCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey )
                        if communityDetailsOnCommunityKey.count != 0{
                            let myChat = communityDetailsOnCommunityKey[0]
                            if ((myChat.isMuted?.isEmpty)!){
                                //self.playSound()
                            }
                        }
                    }else{
                        //For oneTooneChat
                    }
                }
                
                if(messageKeyAvaiblity.count == 0){
                    //send message at message table
                    let messageType = String(describing: updatedmessageData["messageType"]!)
                    if messageType == StringConstants.MessageType.mapLocation {
                        // Update latest message at mychat table
                        let  messages = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: updatedmessageData, saveDataAtDB: true)
                        print("messages",messages)
                        MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: StringConstants.DashboardViewController.location, latestMessageTime: String(describing: updatedmessageData["createdDatetime"]!), communityKey: String(describing: updatedmessageData["communityId"]!),mediaType:StringConstants.EMPTY,messageType:String(describing: updatedmessageData["messageType"]!), lastMessageId: updatedmessageData["id"] as? String ?? "")
                        //Update unreadMessagesCount at mychat table
                        MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ONE, communityKey: String(describing: updatedmessageData["communityId"]!))
                    }else{
                      let  messages = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: updatedmessageData, saveDataAtDB: true)
                        if let latestMsg = messages.contentField1 , let mediaType =  messages.mediaType {
                            // Update latest message at mychat table
                            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: latestMsg, latestMessageTime: messages.createdDatetime!, communityKey: messages.communityId!,mediaType:mediaType,messageType:messages.messageType!, lastMessageId: messages.id!)
                            //Update unreadMessagesCount at mychat table
                            MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ONE, communityKey: messages.communityId!)
                        }
                    }
                    
                }else if (messageKeyAvaiblity.count == 1){
                    //update message at message table
                    MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: updatedmessageData)
                }else{
                    print("Not allowed")
                }
            }
        }
    }
    
    
    func playSound() {
        let path = Bundle.main.path(forResource: "slow-spring-board-longer-tail", ofType:"mp3")!
        let url = URL(fileURLWithPath: path)
        
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            self.player = sound
            //sound.numberOfLoops = 1
            sound.prepareToPlay()
            sound.play()
        } catch {
            print("error loading file")
            // couldn't load file :(
        }
    }
    
    
    
    @objc(xmppStream:didReceivePresence:) func xmppStream(_ stream:XMPPStream, didReceive presence:XMPPPresence) {
        print("\(presence)")
    }
    
}

extension XMPPController:XMPPReconnectDelegate{
    
    func xmppReconnect(_ sender: XMPPReconnect, didDetectAccidentalDisconnect connectionFlags: SCNetworkConnectionFlags) {
        print("didDetectAccidentalDisconnect disconnected")
    }
    
    func xmppReconnect(_ sender: XMPPReconnect, shouldAttemptAutoReconnect connectionFlags: SCNetworkConnectionFlags) -> Bool {
        print("shouldAttemptAutoReconnect disconnected")
        return true
    }
    
    func xmppStream(_ sender: XMPPStream, socketDidConnect socket: GCDAsyncSocket) {
        socket.enableBackgroundingOnSocket()
    }
}

extension XMPPController:XMPPAutoPingDelegate,XMPPAutoTimeDelegate{
    
    func xmppAutoPingDidSend(_ sender: XMPPAutoPing!) {
        print("xmppAutoPingDidSend")
    }
    
    func xmppAutoPingDidReceivePong(_ sender: XMPPAutoPing!) {
        print("xmppAutoPingDidReceivePong")
    }
    
    func xmppAutoPingDidTimeout(_ sender: XMPPAutoPing!) {
        print("xmppAutoPingDidTimeout")
    }
}

