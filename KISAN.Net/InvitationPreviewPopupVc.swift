//
//  InvitationPreviewPopupVc.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InvitationPreviewPopupVc: UIViewController {

    @IBOutlet weak var imgBackGround: UIImageView!

    @IBOutlet weak var btnAcceptGreenPass: UIButton!
    
    @IBOutlet weak var viewGreenPassPreview: UIView!
    
    @IBOutlet weak var imgChannelProfileSmall: UIImageView!
    
    @IBOutlet weak var imgChhanelProfileBIg: UIImageView!
    
    @IBOutlet weak var imgMedia: UIImageView!
    
    @IBOutlet weak var lblCimpanyNameLarge: UILabel!
    
    @IBOutlet weak var lblchannelNameSmall: UILabel!
    
    @IBOutlet weak var imgVideoNew: UIImageView!
    
    @IBOutlet weak var viewOfMedia: UIView!
    
    @IBOutlet weak var lblInvitationAcceptDate: UILabel!
    
    var backroundImage = UIImage()
    
    var appDelegate = AppDelegate()

    @IBAction func dismissPopUpViewClicked(_ sender: Any) {
        //self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBackGround.image = backroundImage
        btnAcceptGreenPass.layer.cornerRadius = 8
        btnAcceptGreenPass.clipsToBounds = true
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        setdata()
        self.navigationController?.navigationBar.isHidden  = true
        let  greenpassAcceptLast_date = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
        if !(greenpassAcceptLast_date.isEmpty){
            lblInvitationAcceptDate.text  = "Before " + setAcceptGreenPassDate(strDate: greenpassAcceptLast_date)
        }else{
            lblInvitationAcceptDate.text = "Before 30th Nov"
        }
    }
    
    func setAcceptGreenPassDate(strDate:String) -> String {
        var formattedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: strDate)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        formattedDate =  dateFormatterPrint.string(from: date! as Date)
        
        return formattedDate
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden  = true
    }
    
    
    @IBAction func btnOpenMediaClicked(_ sender: Any) {
        
        if(appDelegate.mediaType == StringConstants.MediaType.image){
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            let imgurl = appDelegate.mediaUrl
            let imageUrlByRemovingSpace = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            fullViewOfImageFromMessagesVC.imageDataFromServer = imageUrlByRemovingSpace
            self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
        }else if(appDelegate.mediaType == StringConstants.MediaType.externallink) {
            
            let youtubeUrl = NSURL(string:appDelegate.mediaUrl!)!
            if UIApplication.shared.canOpenURL(youtubeUrl as URL){
                UIApplication.shared.openURL(youtubeUrl as URL)
            } else{
                let youtubeUrl = NSURL(string:appDelegate.mediaUrl!)!
                UIApplication.shared.openURL(youtubeUrl as URL)
            }
        }else{
            let docUrl = appDelegate.mediaUrl
            let finalUrl = docUrl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let url = URL(string: finalUrl)
            let controller = SVWebViewController(url: url)
            controller?.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }

    
    func setdata(){
        self.viewGreenPassPreview.backgroundColor = hexStringToUIColor(hex: (appDelegate.channelMaxColor))
        self.lblchannelNameSmall.text = appDelegate.companyName
        self.lblCimpanyNameLarge.text = appDelegate.companyName
        
        let url = appDelegate.imgprofileLogo
        let rep2 = url.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
        let url2 = NSURL(string: rep2)
        self.imgChhanelProfileBIg.sd_setImage(with: url2! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
        self.imgChannelProfileSmall.sd_setImage(with: url2! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)

        
        if  (appDelegate.mediThumbUrl != StringConstants.EMPTY){
            let mediThumbUrlrep2 = appDelegate.mediThumbUrl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let mediThumbUrlurl2 = NSURL(string: mediThumbUrlrep2)
            self.imgMedia.sd_setImage(with: mediThumbUrlurl2! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
            viewOfMedia.isHidden = false

        }else{
            viewOfMedia.isHidden = true
            viewOfMedia.backgroundColor = UIColor.clear

        }
        
        imgChhanelProfileBIg.layer.cornerRadius = imgChhanelProfileBIg.frame.size.width/2
        imgChhanelProfileBIg.clipsToBounds = true
        
        imgChannelProfileSmall.layer.cornerRadius = imgChannelProfileSmall.frame.size.width/2
        imgChannelProfileSmall.clipsToBounds = true
        
        if(appDelegate.mediaType == StringConstants.MediaType.externallink){
            imgVideoNew.isHidden = false
        }else{
            imgVideoNew.isHidden = true
        }
        

    }
}
