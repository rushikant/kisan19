//
//  InviteFriendsViewCell.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InviteFriendsViewCell: UITableViewCell {

    @IBOutlet weak var lblinfo: UILabel!
    @IBOutlet weak var viewShareWithFriends: UIView!
    @IBOutlet weak var viewOfbackground: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
