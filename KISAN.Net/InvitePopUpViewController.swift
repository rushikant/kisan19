//
//  InvitePopUpViewController.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 22/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InvitePopUpViewController: UIViewController {

    var sessionID = String()
    var isKisanMitra  = Bool()
    var isSkip  = Bool()
    var medialink = StringConstants.EMPTY
    @IBOutlet weak var lblGreenpassIssueLastDate: UILabel!
    @IBOutlet weak var viewSendInvite: UIView!
    
    @IBOutlet weak var lblSendFreeGreenPassesCount: UILabel!
    var issueInviteLastDate : String?

    @IBOutlet weak var infoLabel1: UILabel!
    @IBOutlet weak var infoLabel2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=true
        // Do any additional setup after loading the view.
        sessionID = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        isKisanMitra = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.isMitra)
        isSkip = UserDefaults.standard.bool(forKey:  StringConstants.NSUserDefauluterKeys.skip_media)
        medialink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)
        
        viewSendInvite.layer.cornerRadius = 8
        viewSendInvite.clipsToBounds = true
        viewSendInvite.shadowRadius = 1
        
        viewSendInvite.layer.masksToBounds = false
        viewSendInvite.layer.shadowOpacity = 0.5
        viewSendInvite.layer.shadowOffset = CGSize(width: -1, height: 1)
        viewSendInvite.layer.shadowRadius = 1
        viewSendInvite.layer.shouldRasterize = false
        
        
        if  UIScreen.main.bounds.size.height <= 568{
            lblSendFreeGreenPassesCount.font = UIFont.boldSystemFont(ofSize: 14)
            lblGreenpassIssueLastDate.font = UIFont.boldSystemFont(ofSize: 13)
        }else{
            lblSendFreeGreenPassesCount.font = UIFont.boldSystemFont(ofSize: 16)
            lblGreenpassIssueLastDate.font = UIFont.boldSystemFont(ofSize: 15)
        }

        
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent
        let intRemainingString = remaining.formattedWithSeparator

        self.issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)

        lblSendFreeGreenPassesCount.text = StringConstants.setInviteSummaryViewController.send + StringConstants.singleSpace + intRemainingString + StringConstants.singleSpace + StringConstants.setInviteSummaryViewController.FreeGreenPass
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        
        
        if let issueInviteLastDaTe = self.issueInviteLastDate  {
            if !(issueInviteLastDaTe.isEmpty){
                let date: NSDate? = dateFormatterGet.date(from: issueInviteLastDaTe)! as NSDate
                print(dateFormatterPrint.string(from: date! as Date))
            }
        }
        
        if let issueInviteLastDATE = self.issueInviteLastDate {
            if !(issueInviteLastDATE.isEmpty){
                getDateInFormat(dateStr: issueInviteLastDate!)
            }
        }
        
        if UIScreen.main.bounds.size.height <= 568 {
            infoLabel1.font = UIFont.boldSystemFont(ofSize: 15)
            infoLabel2.font = UIFont.boldSystemFont(ofSize: 15)
            infoLabel2.font = UIFont.systemFont(ofSize: 14)
            lblSendFreeGreenPassesCount.font = UIFont.boldSystemFont(ofSize: 13)
            lblGreenpassIssueLastDate.font = UIFont.systemFont(ofSize: 13)

        }
        ButtonAnimationHelper().animateView(view: viewSendInvite)
    }
    
    func getDateInFormat(dateStr:String){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: dateStr)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        lblGreenpassIssueLastDate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
        
    }

    
    @IBAction func btnSkipClicked(_ sender: Any) {
        
        /*let storyboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as UIViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)*/

//        self.navigationController?.popViewController(animated: false)
        
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }

    }
    
    
    @IBAction func btnSendInvitationClicked(_ sender: Any) {
        let  medialink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)
        //if media link is empty and not empty
        if(!medialink.isEmpty){
            let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
            let inviteFriendBaseVC = mainStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
            self.navigationController?.pushViewController(inviteFriendBaseVC, animated: false)
        }else{
            if(isSkip){
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let inviteFriendBaseVC = mainStoryboard.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                self.navigationController?.pushViewController(inviteFriendBaseVC, animated: false)
                
            }else{
                let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
                let setInviteSummaryViewController = mainStoryboard.instantiateViewController(withIdentifier: "SetInviteSummaryViewController") as! SetInviteSummaryViewController
                self.navigationController?.pushViewController(setInviteSummaryViewController, animated: false)
            }
        }
        
    }

    @IBAction func btnKisanVideoClicked(_ sender: Any) {
        let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
            UIApplication.shared.openURL(youtubeUrl as URL)
        } else{
            let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
            UIApplication.shared.openURL(youtubeUrl as URL)
        }
    }
    
    
    
    
}
