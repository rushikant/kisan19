//
//  InviteSummaryServices.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 25/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class InviteSummaryServices: Repository {
    
    func getInviteSummary(_ baseUrl: String,
                             postData: [String: AnyObject],
                             withSuccessHandler success: CompleteionHandler?,
                             withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.InviteSummary)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.InviteSummary {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    var loginRespo: InviteSummaryResponse = InviteSummaryResponse.initializeInviteSummaryResponse()
                    loginRespo = InviteSummaryResponse.getInviteSummaryResponseFromDictionary(responseDict)
                    print("GrrenCloudloginRespo",loginRespo)
                    if let success = self.completeionBlock {
                        success(loginRespo)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
    

}
