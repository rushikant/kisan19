//
//  KnowMoreViewController.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 01/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class KnowMoreViewController: UIViewController {

    @IBOutlet weak var lblThingFirst: UILabel!
    @IBOutlet weak var lblThingSecond: UILabel!
    
    @IBAction func letsGoClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnPlayVideoClick(_ sender: Any) {
        let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
            UIApplication.shared.openURL(youtubeUrl as URL)
        } else{
            let youtubeUrl = NSURL(string:StringConstants.kisanMitra.kisanYoutubeUrl)!
            UIApplication.shared.openURL(youtubeUrl as URL)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        // Do any additional setup after loading the view.

        lblThingFirst.attributedText = setThingsOfRemberText(reminderText: StringConstants.KnowMoreViewController.issueGreenPassDateText,
                                                             reminderDate: setDate(strDate: SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)),
                                                             location: 43)
        lblThingSecond.attributedText = setThingsOfRemberText(reminderText: StringConstants.KnowMoreViewController.acceptGreenPassDateText,
                                                              reminderDate: setDate(strDate: SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)),
                                                              location: 49)

        
    }
    
    func setThingsOfRemberText(reminderText:String, reminderDate:String, location: Int) -> NSMutableAttributedString {
        var attributedString = NSMutableAttributedString()
        
        attributedString = NSMutableAttributedString(string: "\(reminderText) \(reminderDate).", attributes: [
            .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
            .foregroundColor: UIColor.black,
            .kern: 0.0
        ])
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize:14.0, weight: .bold), range: NSRange(location: location, length: reminderDate.count))
        return attributedString
    }
    
    func setDate(strDate:String) -> String {
        var formattedDate = String()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: strDate)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
       formattedDate =  dateFormatterPrint.string(from: date! as Date)
        
        return formattedDate
    }
    
    func setNavigation(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.text =  (NSLocalizedString(StringConstants.NavigationTitle.knowMoreTitl, comment: StringConstants.EMPTY))
        navigationItem.titleView = lblOfTitle
        
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

}
