//
//  LogoutUserServices.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 22/08/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class LogoutUserServices: Repository {
    
    func logOutUser(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.PUT(baseUrl, postData: postData, requestName:  RequestName.logOutUser)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.logOutUser {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    if let success = self.completeionBlock {
                        success(responseDict)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
    
}
