//
//  AppStarterData.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 12/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class AppStarterData: NSObject {
    
    var pending_invitation_count : Int?
    var greenpass_count : Int?
    var isMitra : Bool?
    var inviteSummary : [InviteSummaryRecords]

    init( pending_invitation_count : Int?,greenpass_count : Int?,isMitra : Bool?,inviteSummary:[InviteSummaryRecords]) {
        
       self.pending_invitation_count  = pending_invitation_count
       self.greenpass_count  = greenpass_count
        self.isMitra  = isMitra
       self.inviteSummary = inviteSummary

    }
    
    class func initaiLiseAppStarterData() -> AppStarterData {
        let response = AppStarterData(pending_invitation_count : 0,
                                    greenpass_count : 0,
                                    isMitra : false,
                                    inviteSummary:[]
                                    )
        return response
    }
    
    class func getAppStarterDataFromDictionary(_ dictionary: [String: AnyObject?]) -> AppStarterData {
        let response = initaiLiseAppStarterData()
        response.pending_invitation_count = dictionary["pending_invitation_count"]  as? Int
        response.greenpass_count = dictionary["greenpass_count"]  as? Int
        response.isMitra = dictionary["isMitra"]  as? Bool
        if let dict = dictionary["inviteSummary"]{
            if dict != nil {
                 response.inviteSummary = [InviteSummaryRecords.genInviteSummeryRecordsFromDictionary(dict as! [String : AnyObject?])]
            }
        }

        return response
    }
}
