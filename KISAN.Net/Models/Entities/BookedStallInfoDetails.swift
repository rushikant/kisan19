//
//  BookedStallInfoDetails.swift
//  KISAN.Net
//
//  Created by Rushikant on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class BookedStallInfoDetails: NSObject {
    
    var  id  : Int?
    var  transaction_id: Int?
    var  company_name: String?
    var  fascia_name: String?
    var  sku: String?
    var  area: Int?
    var  pavillion_type  : String?
    var  stand_type  : String?
    var  stand_location  : String?
    var  pavillion_name  : String?
    var  stand_type_name  : String?
    var  stand_location_name  : String?
    var  created_date  : String?
    var  updated_date  : String?
    var  is_formsubmitted: Int?
    var  is_vendor_coordination_form_editable  : Int?
    var  vendor_coordination_form_submits  : Int?
    var  status: String?
    var  form_submitted_datetime: String?
    var  stall_approved_datetime: String?
    var  is_trusted: Int?
    var  shop_order_id: String?
    var  payment_status  : String?
    var  payment_datetime  : String?
    var  payment_gateway  : String?
    var  logo_url  : String?
    var  logo_big_thumb  : String?
    var  logo_small_thumb  : String?
    var  show_history_prefilled_message  : Int?
    var isSelectedStall : String?
    
    init(id  : Int?,
         transaction_id: Int?,company_name: String?,
         fascia_name: String?,
         sku: String?,
         area: Int?,
         pavillion_type  : String?,
         stand_type  : String?,
         stand_location  : String?,
         pavillion_name  : String?,
         stand_type_name  : String?,
         stand_location_name  : String?,
         created_date  : String?,
         updated_date  : String?,
         is_formsubmitted: Int?,
         is_vendor_coordination_form_editable  : Int?,
         vendor_coordination_form_submits  : Int?,
         status: String?,
         form_submitted_datetime: String?,
         stall_approved_datetime: String?,
         is_trusted: Int?,
         shop_order_id: String?,
         payment_status  : String?,
         payment_datetime  : String?,
         payment_gateway  : String?,
         logo_url  : String?,
         logo_big_thumb  : String?,
         logo_small_thumb  : String?,
         show_history_prefilled_message  : Int?,
         isSelectedStall : String?
        ) {
            self.id = id
            self.transaction_id = transaction_id
            self.company_name = company_name
            self.fascia_name = fascia_name
            self.sku = sku
            self.area = area
            self.pavillion_type = pavillion_type
            self.stand_type = stand_type
            self.stand_location = stand_location
            self.pavillion_name = pavillion_name
            self.stand_type_name = stand_type_name
            self.stand_location_name = stand_location_name
            self.created_date = created_date
            self.updated_date = updated_date
            self.is_formsubmitted = is_formsubmitted
            self.is_vendor_coordination_form_editable = is_vendor_coordination_form_editable
            self.vendor_coordination_form_submits = vendor_coordination_form_submits
            self.status = status
            self.form_submitted_datetime = form_submitted_datetime
            self.stall_approved_datetime = stall_approved_datetime
            self.is_trusted = is_trusted
            self.shop_order_id = shop_order_id
            self.payment_status = payment_status
            self.payment_datetime = payment_datetime
            self.payment_gateway = payment_gateway
            self.logo_url = logo_url
            self.logo_big_thumb = logo_big_thumb
            self.logo_small_thumb = logo_small_thumb
            self.show_history_prefilled_message = show_history_prefilled_message
            self.isSelectedStall = isSelectedStall
    }
    
    class func initaiLiseBookedStallInfoDetails() -> BookedStallInfoDetails {
        let response = BookedStallInfoDetails(
            id : 0,
            transaction_id: 0,
            company_name: "",
            fascia_name: "",
            sku: "",
            area: 0,
            pavillion_type : "",
            stand_type : "",
            stand_location : "",
            pavillion_name : "",
            stand_type_name : "",
            stand_location_name : "",
            created_date : "",
            updated_date : "",
            is_formsubmitted: 0,
            is_vendor_coordination_form_editable : 0,
            vendor_coordination_form_submits : 0,
            status: "",
            form_submitted_datetime: "",
            stall_approved_datetime: "",
            is_trusted: 0,
            shop_order_id: "",
            payment_status : "",
            payment_datetime : "",
            payment_gateway : "",
            logo_url : "",
            logo_big_thumb : "",
            logo_small_thumb : "",
            show_history_prefilled_message : 0,
            isSelectedStall : "0"
        )
        return response
    }
    
    
    class func getBookedStallInfoDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> BookedStallInfoDetails {
        let response = initaiLiseBookedStallInfoDetails()
        
        response.id  =  dictionary["id"]  as? Int
        response.transaction_id  =  dictionary["transaction_id"]  as? Int
        response.company_name  =  dictionary["company_name"]  as? String
        response.fascia_name  =  dictionary["fascia_name"]  as? String
        response.sku  =  dictionary["sku"]  as? String
        response.area  =  dictionary["area"]  as? Int
        response.pavillion_type  =  dictionary["pavillion_type"]  as? String
        response.stand_type  =  dictionary["stand_type"]  as? String
        response.stand_location  =  dictionary["stand_location"]  as? String
        response.pavillion_name  =  dictionary["pavillion_name"]  as? String
        response.stand_type_name  =  dictionary["stand_type_name"]  as? String
        response.stand_location_name  =  dictionary["stand_location_name"]  as? String
        response.created_date  =  dictionary["created_date"]  as? String
        response.updated_date  =  dictionary["updated_date"]  as? String
        response.is_formsubmitted  =  dictionary["is_formsubmitted"]  as? Int
        response.is_vendor_coordination_form_editable  =  dictionary["is_vendor_coordination_form_editable"]  as? Int
        response.vendor_coordination_form_submits  =  dictionary["vendor_coordination_form_submits"]  as? Int
        response.status  =  dictionary["status"]  as? String
        response.form_submitted_datetime  =  dictionary["form_submitted_datetime"]  as? String
        response.stall_approved_datetime  =  dictionary["stall_approved_datetime"]  as? String
        response.is_trusted  =  dictionary["is_trusted"]  as? Int
        response.shop_order_id  =  dictionary["shop_order_id"]  as? String
        response.payment_status  =  dictionary["payment_status"]  as? String
        response.payment_datetime  =  dictionary["payment_datetime"]  as? String
        response.payment_gateway  =  dictionary["payment_gateway"]  as? String
        response.logo_url  =  dictionary["logo_url"]  as? String
        response.logo_big_thumb  =  dictionary["logo_big_thumb"]  as? String
        response.logo_small_thumb  =  dictionary["logo_small_thumb"]  as? String
        response.show_history_prefilled_message  =  dictionary["show_history_prefilled_message"] as? Int
        response.isSelectedStall = "0"
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [BookedStallInfoDetails] {
        var modelArray: [BookedStallInfoDetails] = [BookedStallInfoDetails]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getBookedStallInfoDetailsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}



