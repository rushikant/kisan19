//
//  CategoriesList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 01/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CategoriesList: NSObject {
    
    var id: String?
    var name: String?
    var image_url: String?
    var is_occupation: Bool?
    var categoryCountOnDiscoverList: String?
    
    init(id: String?,
         name: String?,
         image_url: String,
         is_occupation: Bool,categoryCountOnDiscoverList:String){
        self.id = id
        self.name = name
        self.image_url = image_url
        self.is_occupation = is_occupation
        self.categoryCountOnDiscoverList = categoryCountOnDiscoverList
    }
    
    class func initializeCategoriesList() -> CategoriesList {
        let model = CategoriesList(id: "",
                                   name: "",
                                   image_url: "",
                                   is_occupation: false,categoryCountOnDiscoverList:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CategoriesList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCategoriesListFromDictionary(_ dictionary: [String: AnyObject?]) -> CategoriesList {
        let model = initializeCategoriesList()
        model.id = dictionary["id"] as? String
        model.name = dictionary["name"] as? String
        model.image_url = dictionary["image_url"] as? String
        model.is_occupation = dictionary["is_occupation"] as? Bool
        model.categoryCountOnDiscoverList = ""
        return model as CategoriesList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CategoriesList] {
        var modelArray: [CategoriesList] = [CategoriesList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCategoriesListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
