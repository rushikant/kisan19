//
//  CommonChannelList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 08/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommonChannelList: NSObject {

    var communityCategories : String?
    var communityCreatedTimestamp : String?
    var communityDesc : String?
    var communityImageBigThumbUrl : String?
    var communityImageSmallThumbUrl : String?
    var communityImageUrl : String?
    var communityJabberId : String?
    var communityKey : String?
    var communityName : String?
    var communityScore : String?
    var groupRole : String?
    var isMember : String?
    var ownerFirstName : String?
    var ownerId : String?
    var ownerImageUrl : String?
    var ownerLastName : String?
    var ownerName : String?
    var privacy : String?
    var totalMembers : Int?
    var type : String?
    var communityDominantColour : String?

    init(communityCategories: String?,
         communityCreatedTimestamp: String?,
         communityDesc: String?,communityImageBigThumbUrl: String?,communityImageSmallThumbUrl: String?,communityImageUrl: String?,communityJabberId: String?,communityKey: String?,communityName: String?,communityScore: String?,groupRole: String?,isMember: String?,ownerFirstName: String?,ownerId: String?,ownerImageUrl: String?,ownerLastName: String?,ownerName: String?,privacy: String?,totalMembers: Int?,type: String?,communityDominantColour:String?){
        self.communityCategories = communityCategories
        self.communityCreatedTimestamp = communityCreatedTimestamp
        self.communityDesc = communityDesc
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.communityImageUrl = communityImageUrl
        self.communityJabberId = communityJabberId
        self.communityKey = communityKey
        self.communityName = communityName
        self.communityScore = communityScore
        self.groupRole = groupRole
        self.isMember = isMember
        self.ownerFirstName = ownerFirstName
        self.ownerId = ownerId
        self.ownerImageUrl = ownerImageUrl
        self.ownerLastName = ownerLastName
        self.ownerName = ownerName
        self.privacy = privacy
        self.totalMembers = totalMembers
        self.type = type
        self.communityDominantColour = communityDominantColour
    }
    
   
    
    class func initializeCommonChannelList() -> CommonChannelList {
        let model = CommonChannelList(communityCategories: "",
                                      communityCreatedTimestamp: "", communityDesc: "",
                                         communityImageBigThumbUrl: "",
                                         communityImageSmallThumbUrl: "",communityImageUrl: "",communityJabberId:"",communityKey:"",communityName:"",communityScore:"",groupRole:"",isMember:"",ownerFirstName:"",ownerId:"",ownerImageUrl:"",ownerLastName:"",ownerName:"", privacy: "",totalMembers:-1,type:"",communityDominantColour:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommonChannelList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommonChannelListFromDictionary(_ dictionary: [String: AnyObject?]) -> CommonChannelList {
        let model = initializeCommonChannelList()
        model.communityCategories = dictionary["communityCategories"] as? String
        model.communityCreatedTimestamp = dictionary["communityCreatedTimestamp"] as? String
        model.communityDesc = dictionary["communityDesc"] as? String
        model.communityImageBigThumbUrl = dictionary["communityImageBigThumbUrl"] as? String
        model.communityImageSmallThumbUrl = dictionary["communityImageSmallThumbUrl"] as? String
        model.communityImageUrl = dictionary["communityImageUrl"] as? String
        model.communityJabberId = dictionary["communityJabberId"] as? String
        model.communityKey = dictionary["communityKey"] as? String
        model.communityName = dictionary["communityName"] as? String
        model.communityScore = dictionary["communityScore"] as? String
        model.groupRole = dictionary["groupRole"] as? String
        model.isMember = dictionary["isMember"] as? String
        model.ownerFirstName = dictionary["ownerFirstName"] as? String
        model.ownerId = dictionary["ownerId"] as? String
        model.ownerImageUrl = dictionary["ownerImageUrl"] as? String
        model.ownerLastName = dictionary["ownerLastName"] as? String
        model.ownerName = dictionary["ownerName"] as? String
        model.privacy = dictionary["privacy"] as? String
        model.totalMembers = dictionary["totalMembers"] as? Int
        model.type = dictionary["type"] as? String
        model.communityDominantColour = dictionary["communityDominantColour"] as? String
       
        return model as CommonChannelList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CommonChannelList] {
        var modelArray: [CommonChannelList] = [CommonChannelList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCommonChannelListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
