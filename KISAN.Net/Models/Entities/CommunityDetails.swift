//
//  CommunityDetails.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 16/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommunityDetails: NSObject {
    
    public var communityCategories: String?
    public var communityCreatedTimestamp: String?
    public var communityDominantColour: String?
    public var communityImageBigThumbUrl: String?
    public var communityImageSmallThumbUrl: String?
    public var communityImageUrl: String?
    public var communityJabberId: String?
    public var communityKey: String?
    public var communityName: String?
    public var communityScore: Int?
    public var communityWithRssFeed: Bool?
    public var defaultValue: Bool?
    public var descriptionValue : String?
    public var loggedUserJoinedDateTime : String?
    public var ownerId : String?
    public var privacy : String?
    
    public var address : String?
    public var location : [String:String]

    public var alloted_stall_number : String?
    public var communityDesc : String?
    public var company_website :  String?
    public var contact_person_designation : String?
    public var contact_person_first_name : String?
    public var contact_person_last_name : String?
    public var pavilion_name : String?
    public var stall_id : String?
    public var totalMembers : Int?
    public var isMember : Bool?
    public var event_code : String?
    public var productMedias: [ProductMedias] = []
    public var ownerName : String?
    public var type : String?
    public var blockedDatetime : String?
    public var logged_user_blocked_timestamp : String?

    
    init(communityCategories: String?,
         communityCreatedTimestamp: String?,
         communityDominantColour: String?,
         communityImageBigThumbUrl: String?,communityImageUrl:String?,communityJabberId:String?,communityKey:String?,communityName:String?,communityScore:Int?,communityWithRssFeed:Bool?,defaultValue:Bool?,descriptionValue:String?,loggedUserJoinedDateTime:String,ownerId:String?,privacy:String?,address:String?,alloted_stall_number:String?,communityDesc:String?,company_website:String?,contact_person_designation:String?,contact_person_first_name:String?,contact_person_last_name:String?,pavilion_name:String?,stall_id:String?,totalMembers:Int?,isMember:Bool?,communityImageSmallThumbUrl:String?,productMedias: [ProductMedias],location: [String:String],event_code:String?,ownerName:String?,type:String?,blockedDatetime:String?,logged_user_blocked_timestamp:String?){
        self.communityCategories = communityCategories
        self.communityCreatedTimestamp = communityCreatedTimestamp
        self.communityDominantColour = communityDominantColour
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
        self.communityImageUrl = communityImageUrl
        self.communityJabberId = communityJabberId
        self.communityKey = communityKey
        self.communityName = communityName
        self.communityScore = communityScore
        self.communityWithRssFeed = communityWithRssFeed
        self.defaultValue = defaultValue
        self.descriptionValue = descriptionValue
        self.loggedUserJoinedDateTime = loggedUserJoinedDateTime
        self.ownerId = ownerId
        self.privacy = privacy
        
        self.address = address
        self.location = location

        self.alloted_stall_number = alloted_stall_number
        self.communityDesc = communityDesc
        self.company_website = company_website
        self.contact_person_designation = contact_person_designation
        self.contact_person_first_name = contact_person_first_name
        self.contact_person_last_name = contact_person_last_name
        self.pavilion_name = pavilion_name
        self.stall_id = stall_id
        self.totalMembers = totalMembers
        self.isMember = isMember
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.productMedias = productMedias
        self.event_code = event_code
        self.ownerName = ownerName
        self.type = type
        self.blockedDatetime = blockedDatetime
        self.logged_user_blocked_timestamp = logged_user_blocked_timestamp
    }
    
    class func initializeCommunityDetails() -> CommunityDetails {
        let model = CommunityDetails(communityCategories: "",
                                   communityCreatedTimestamp: "",
                                   communityDominantColour: "",
                                   communityImageBigThumbUrl: "",communityImageUrl:"",communityJabberId:"",communityKey:"",communityName:"",communityScore:-1,communityWithRssFeed:false,defaultValue:false,descriptionValue:"",loggedUserJoinedDateTime:"",ownerId:"",privacy:"",address:"",alloted_stall_number:"",communityDesc:"",company_website:"",contact_person_designation:"",contact_person_first_name:"",contact_person_last_name:"",pavilion_name:"",stall_id:"",totalMembers:-1,isMember:false,communityImageSmallThumbUrl:"",productMedias:[],location:["":""],event_code:"",ownerName:"",type:"",blockedDatetime:"", logged_user_blocked_timestamp: "")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommunityDetails) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommunityDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> CommunityDetails {
        let model = initializeCommunityDetails()
        model.communityCategories = dictionary["communityCategories"] as? String
        model.communityCreatedTimestamp = dictionary["communityCreatedTimestamp"] as? String
        model.communityDominantColour = dictionary["communityDominantColour"] as? String
        model.communityImageBigThumbUrl = dictionary["communityImageBigThumbUrl"] as? String
        model.communityImageUrl = dictionary["communityImageUrl"] as? String
        
        model.communityJabberId = dictionary["communityJabberId"] as? String
        model.communityKey = dictionary["communityKey"] as? String
        model.communityName = dictionary["communityName"] as? String
        model.communityScore = dictionary["communityScore"] as? Int
        
        model.communityWithRssFeed = dictionary["communityWithRssFeed"] as? Bool
        model.defaultValue = dictionary["defaultValue"] as? Bool
        model.descriptionValue = dictionary["descriptionValue"] as? String
        model.loggedUserJoinedDateTime = dictionary["loggedUserJoinedDateTime"] as? String

        model.ownerId = dictionary["ownerId"] as? String
        model.privacy = dictionary["privacy"] as? String
        
        model.address = dictionary["address"] as? String
        if let dict = dictionary["location"]{
            model.location = dict as! [String:String]
        }
        model.alloted_stall_number = dictionary["alloted_stall_number"] as? String
        model.communityDesc = dictionary["communityDesc"] as? String
        model.company_website = dictionary["company_website"] as? String
        
        model.contact_person_designation = dictionary["contact_person_designation"] as? String
        model.contact_person_first_name = dictionary["contact_person_first_name"] as? String
        model.contact_person_last_name = dictionary["contact_person_last_name"] as? String
        model.pavilion_name = dictionary["pavilion_name"] as? String
        
        model.stall_id = dictionary["stall_id"] as? String
        model.totalMembers = dictionary["totalMembers"] as? Int
        model.isMember = dictionary["isMember"] as? Bool
        model.communityImageSmallThumbUrl = dictionary["communityImageSmallThumbUrl"] as? String
        model.event_code = dictionary["event_code"] as? String
        
        if let dict = dictionary["productMedias"]{
          model.productMedias = ProductMedias.getModelFromArray(dict as! [AnyObject])
        }
        model.ownerName = dictionary["ownerName"] as? String
        model.type = dictionary["type"] as? String
        model.blockedDatetime = dictionary["blockedDatetime"] as? String
        
        if let logged_user_blocked_timestamp = dictionary["logged_user_blocked_timestamp"] as? String{
            model.logged_user_blocked_timestamp = logged_user_blocked_timestamp
        }else{
            model.logged_user_blocked_timestamp = StringConstants.EMPTY
        }
        return model as CommunityDetails
    }

}
