//
//  CommunityDetailsList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommunityDetailsList: NSObject {
    
    var communityKey: String?
    var communityCreatedTimestamp: String?
    var communityUpdatedTimestamp: Int?
    var communityScore: Int?
    var finalScore: Int?
    var communityJoinedTimestamp: String?
    var communityLeftTimestamp: String?
    var communityDeletedTimestamp : String?
    var communityStatus : String?
    var featured:String?
    var communityName :String?
    var communityDesc :String?
    var ownerId: String?
    var ownerImageUrl: String?
    var ownerName:String?
    var communityImageUrl: String?
    var communityImageSmallThumbUrl:String?
    var communityImageBigThumbUrl:String?
    var isDeleted:String?
    var type:String?
    var isCommunityWithRssFeed:String?
    var privacy:String?
    var totalMembers: Int?
    var isMember: String?
    var messageCount: String?
    var communityCategories:String?
    var communitySubType:String?
    var communityDominantColour:String?
    //var userOne:
    //var userTwo:
    
    init(communityKey: String?,
         communityCreatedTimestamp: String?,
         communityUpdatedTimestamp: Int,
         communityScore: Int?,finalScore:Int?,communityJoinedTimestamp:String?,communityLeftTimestamp:String?,communityDeletedTimestamp:String?,communityStatus:String?,featured:String?,communityName:String?,communityDesc:String?,ownerId:String?,ownerImageUrl:String?,ownerName:String?,communityImageUrl:String?,communityImageSmallThumbUrl:String?,communityImageBigThumbUrl:String,isDeleted:String,type:String,isCommunityWithRssFeed:String,privacy:String,totalMembers:Int,isMember:String,messageCount:String,communityCategories:String,communitySubType:String,communityDominantColour:String){
        self.communityKey = communityKey
        self.communityCreatedTimestamp = communityCreatedTimestamp
        self.communityUpdatedTimestamp = communityUpdatedTimestamp
        self.communityScore = communityScore
        self.finalScore = finalScore
        self.communityJoinedTimestamp = communityJoinedTimestamp
        self.communityLeftTimestamp = communityLeftTimestamp
        self.communityDeletedTimestamp = communityDeletedTimestamp
        self.communityStatus = communityStatus
        self.featured = featured
        self.communityName = communityName
        self.communityDesc = communityDesc
        self.ownerId = ownerId
        self.ownerImageUrl = ownerImageUrl
        self.ownerName = ownerName
        self.communityImageUrl = communityImageUrl
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
        self.isDeleted = isDeleted
        self.type = type
        self.isCommunityWithRssFeed = isCommunityWithRssFeed
        self.privacy = privacy
        self.totalMembers = totalMembers
        self.isMember = isMember
        self.messageCount = messageCount
        self.communityCategories = communityCategories
        self.communitySubType = communitySubType
        self.communityDominantColour = communityDominantColour
    }
    
    class func initializeCommunityDetailsList() -> CommunityDetailsList {
        let model = CommunityDetailsList(communityKey: "",
                                   communityCreatedTimestamp: "",
                                   communityUpdatedTimestamp: -1,
                                   communityScore: -1,finalScore: -1,communityJoinedTimestamp:"",communityLeftTimestamp:"",communityDeletedTimestamp:"",communityStatus:"",featured:"",communityName:"",communityDesc:"",ownerId:"",ownerImageUrl:"",ownerName:"",communityImageUrl:"",communityImageSmallThumbUrl:"",communityImageBigThumbUrl:"",isDeleted:"",type:"",isCommunityWithRssFeed:"",privacy:"",totalMembers:-1,isMember:"",messageCount:"",communityCategories:"",communitySubType:"",communityDominantColour:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommunityDetailsList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommunityDetailsListFromDictionary(_ dictionary: [String: AnyObject?]) -> CommunityDetailsList {
        let model = initializeCommunityDetailsList()
        model.communityKey = dictionary["communityKey"] as? String
        model.communityCreatedTimestamp = dictionary["communityCreatedTimestamp"] as? String
        model.communityUpdatedTimestamp = dictionary["communityUpdatedTimestamp"] as? Int
        model.communityScore = dictionary["communityScore"] as? Int
        model.finalScore = dictionary["finalScore"] as? Int
        model.communityJoinedTimestamp = dictionary["communityJoinedTimestamp"] as? String
        model.communityLeftTimestamp = dictionary["communityLeftTimestamp"] as? String
        model.communityDeletedTimestamp = dictionary["communityDeletedTimestamp"] as? String
        model.communityStatus = dictionary["communityStatus"] as? String
        model.featured = dictionary["featured"] as? String
        model.communityName = dictionary["communityName"] as? String
        model.communityDesc = dictionary["communityDesc"] as? String
        model.ownerId = dictionary["ownerId"] as? String
        model.ownerImageUrl = dictionary["ownerImageUrl"] as? String
        model.ownerName = dictionary["ownerName"] as? String
        model.communityImageUrl = dictionary["communityImageUrl"] as? String
        model.communityImageSmallThumbUrl = dictionary["communityImageSmallThumbUrl"] as? String
        model.communityImageBigThumbUrl = dictionary["communityImageBigThumbUrl"] as? String
        model.isDeleted = dictionary["isDeleted"] as? String
        model.type = dictionary["type"] as? String
        model.isCommunityWithRssFeed = dictionary["isCommunityWithRssFeed"] as? String
        model.privacy = dictionary["privacy"] as? String
        model.totalMembers = dictionary["totalMembers"] as? Int
        model.isMember = dictionary["isMember"] as? String
        model.messageCount = dictionary["messageCount"] as? String
        model.communityCategories = dictionary["communityCategories"] as? String
        model.communitySubType = dictionary["communitySubType"] as? String
        model.communityDominantColour = dictionary["communityDominantColour"] as? String
        return model as CommunityDetailsList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CommunityDetailsList] {
        var modelArray: [CommunityDetailsList] = [CommunityDetailsList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCommunityDetailsListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }

}
