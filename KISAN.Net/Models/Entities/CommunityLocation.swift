//
//  CommunityLocation.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/12/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommunityLocation: NSObject {
    var city: String?
    var state: String?
    
    init(city: String?,
         state: String?){
        self.city = city
        self.state = state
    }
    
    class func initializeLocation() -> CommunityLocation {
        let model = CommunityLocation(city: "",state: "")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommunityLocation) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommunityLocationFromDictionary(_ dictionary: [String: AnyObject?]) -> CommunityLocation {
        let model = initializeLocation()
        model.city = dictionary["city"] as? String
        model.state = dictionary["state"] as? String
        
        return model as CommunityLocation
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CommunityLocation] {
        var modelArray: [CommunityLocation] = [CommunityLocation]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCommunityLocationFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}
