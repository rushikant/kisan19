//
//  CommunityMembersList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 29/05/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommunityMembersList: NSObject {
    
    var accountType : String?
    var bigThumbUrl : String?
    var categoryIds : String?
    var city : String?
    var country : String?
    var createdDateTime : String?
    var firstName : String?
    var id : String?
    var imageUrl : String?
    var isBlocked : String?
    var isDeleted : String?
    var  isSpam : String?
    var joinedDateTime : String?
    var lastActiveDateTime : String?
    var lastLoginDateTime : String?
    var lastLogoutDateTime : String?
    var lastName : String?
    var profileStatus : String?
    var role : String?
    var smallThumbUrl : String?
    var state : String?
    var updatedDateTime : String?
    
    
    init(accountType: String?,
         bigThumbUrl: String?,
         categoryIds: String?,
         city: String?,country:String?,createdDateTime:String?,firstName:String?,id:String?,imageUrl:String?,isBlocked:String?,isDeleted:String?,isSpam:String?,joinedDateTime:String?,lastActiveDateTime:String?,lastLoginDateTime:String?,lastLogoutDateTime:String?,lastName:String?,profileStatus:String,role:String,smallThumbUrl:String,state:String,updatedDateTime:String){
        self.accountType = accountType
        self.bigThumbUrl = bigThumbUrl
        self.categoryIds = categoryIds
        self.city = city
        self.country = country
        self.createdDateTime = createdDateTime
        self.firstName = firstName
        self.id = id
        self.imageUrl = imageUrl
        self.isBlocked = isBlocked
        self.isDeleted = isDeleted
        self.isSpam = isSpam
        self.joinedDateTime = joinedDateTime
        self.lastActiveDateTime = lastActiveDateTime
        self.lastLoginDateTime = lastLoginDateTime
        self.lastLogoutDateTime = lastLogoutDateTime
        self.lastName = lastName
        self.profileStatus = profileStatus
        self.role = role
        self.smallThumbUrl = smallThumbUrl
        self.state = state
        self.updatedDateTime = updatedDateTime
    }
    
    class func initializeCommunityMembersList() -> CommunityMembersList {
        let model = CommunityMembersList(accountType: "",
                                         bigThumbUrl: "",
                                         categoryIds: "",
                                         city: "",country: "",createdDateTime:"",firstName:"",id:"",imageUrl:"",isBlocked:"",isDeleted:"",isSpam:"",joinedDateTime:"",lastActiveDateTime:"",lastLoginDateTime:"",lastLogoutDateTime:"",lastName:"",profileStatus:"",role:"",smallThumbUrl:"", state: "",updatedDateTime:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommunityMembersList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommunityMembersListFromDictionary(_ dictionary: [String: AnyObject?]) -> CommunityMembersList {
        let model = initializeCommunityMembersList()
        model.accountType = dictionary["accountType"] as? String
        model.bigThumbUrl = dictionary["bigThumbUrl"] as? String
        model.categoryIds = dictionary["categoryIds"] as? String
        model.city = dictionary["city"] as? String
        model.country = dictionary["country"] as? String
        model.createdDateTime = dictionary["createdDateTime"] as? String
        model.firstName = dictionary["firstName"] as? String
        model.id = dictionary["id"] as? String
        model.imageUrl = dictionary["imageUrl"] as? String
        model.isBlocked = dictionary["isBlocked"] as? String
        model.isDeleted = dictionary["isDeleted"] as? String
        model.isSpam = dictionary["isSpam"] as? String
        model.joinedDateTime = dictionary["joinedDateTime"] as? String
        model.lastActiveDateTime = dictionary["lastActiveDateTime"] as? String
        model.lastLoginDateTime = dictionary["lastLoginDateTime"] as? String
        model.lastLogoutDateTime = dictionary["lastLogoutDateTime"] as? String
        model.lastName = dictionary["lastName"] as? String
        model.profileStatus = dictionary["profileStatus"] as? String
        model.role = dictionary["role"] as? String
        model.smallThumbUrl = dictionary["smallThumbUrl"] as? String
        model.state = dictionary["state"] as? String
        model.updatedDateTime = dictionary["updatedDateTime"] as? String
        return model as CommunityMembersList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CommunityMembersList] {
        var modelArray: [CommunityMembersList] = [CommunityMembersList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCommunityMembersListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}
