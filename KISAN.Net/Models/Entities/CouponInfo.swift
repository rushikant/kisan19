//
//  CouponInfo.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 14/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

import UIKit

class CouponInfo: NSObject {
    
    var   GreenpassForSelf : String?
    var   cashbackEarned: String?
    var   cashbackPaid: String?
    var   couponCode: String?
    var   inviteLink: String?
    var   referalCount: String?
    var   upiId: String?
    
    
    
    
    init (GreenpassForSelf : String?,cashbackEarned : String?,cashbackPaid: String?,couponCode: String?,inviteLink: String?,referalCount: String?,upiId: String?){
        
        self.GreenpassForSelf = GreenpassForSelf
        self.cashbackEarned = cashbackEarned
        self.cashbackPaid = cashbackPaid
        self.couponCode = couponCode
        self.inviteLink = inviteLink
        self.referalCount = referalCount
        self.upiId = upiId
    }
    
    
    class func initaiLiseCouponInfo() -> CouponInfo {
        let response = CouponInfo(GreenpassForSelf: "", cashbackEarned: "", cashbackPaid: "", couponCode: "", inviteLink: "", referalCount: "", upiId: "")
        return response
    }
    
    
    class func getCouponInfoFromDictionary(_ dictionary: [String: AnyObject?]) -> CouponInfo {
        let response = initaiLiseCouponInfo()
        response.GreenpassForSelf = dictionary["GreenpassForSelf"]  as? String
        
        response.cashbackEarned = dictionary["cashbackEarned"]  as? String
        
        response.cashbackPaid = dictionary["cashbackPaid"]  as? String
        
        response.couponCode = dictionary["couponCode"]  as? String
        
        response.inviteLink = dictionary["inviteLink"]  as? String
        
        response.referalCount = dictionary["referalCount"]  as? String
        
        response.upiId = dictionary["upiId"]  as? String
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CouponInfo] {
        var modelArray: [CouponInfo] = [CouponInfo]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCouponInfoFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
