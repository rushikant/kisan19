//
//  ExhbitorInfo.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhbitorInfo: NSObject {
   
    var  success : Bool?
    var invitation_list : [GuestListDetails] = []
    var total_count : Int?
    
    
    init (success : Bool?,
     invitation_list : [GuestListDetails],
     total_count : Int?){
      self.success = success
      self.invitation_list = invitation_list
      self.total_count = total_count
    }
    
    class func initaiLiseExhbitorInfo() -> ExhbitorInfo {
        let response = ExhbitorInfo(success : false,
                                        invitation_list : [],
                                        total_count : 0)
        return response
    }
    
    class func genInviteGuestListDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhbitorInfo {
        let response = initaiLiseExhbitorInfo()
        response.success = dictionary["success"]  as? Bool
        response.total_count = dictionary["total_count"]  as? Int
        response.invitation_list = GuestListDetails.getModelFromArray(dictionary["invitation_list"] as! [AnyObject])
        
        return response
    }
}
