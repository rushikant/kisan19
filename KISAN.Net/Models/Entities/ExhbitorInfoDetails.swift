//
//  ExhbitorInfoDetails.swift
//  KISAN.Net
//
//  Created by Rushikant on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhbitorInfoDetails: NSObject {
    
    var  id  : Int?
    var  user_id: Int?
    var  username : String?
    var  mobile : String?
    var  first_name : String?
    var  last_name : String?
    var  booked_stall_info: [BookedStallInfoDetails]?
    
    init(id  : Int?,
        user_id: Int?,
        username: String?,
        mobile : String?,
        first_name : String?,
        last_name : String?,
        booked_stall_info: [BookedStallInfoDetails]?
    ){
        self.id = id
        self.user_id = user_id
        self.username = username
        self.mobile = mobile
        self.first_name = first_name
        self.last_name = last_name
        self.booked_stall_info = booked_stall_info
    }
    
    class func initaiLiseExhbitorInfoDetails() -> ExhbitorInfoDetails {
        let response = ExhbitorInfoDetails(
            id  : 0,
            user_id: 0,
            username : "",
            mobile : "",
            first_name : "",
            last_name : "",
            booked_stall_info: []
        )
        return response
    }
    
    
    class func getExhbitorInfoDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhbitorInfoDetails {
        let response = initaiLiseExhbitorInfoDetails()
        
        response.id  =  dictionary["id"]  as? Int
        response.user_id = dictionary["user_id"] as? Int
        response.username = dictionary["username"] as? String
        response.first_name = dictionary["first_name"] as? String
        response.last_name = dictionary["last_name"] as? String
        response.mobile = dictionary["mobile"] as? String
       //response.booked_stall_info = dictionary["booked_stall_info"] as? [BookedStallInfoDetails]
        if dictionary["booked_stall_info"] != nil {
            response.booked_stall_info = BookedStallInfoDetails.getModelFromArray(dictionary["booked_stall_info"] as! [AnyObject])
        }
        return response
    }
}
