//
//  ExhibitorBadges.swift
//  KISAN.Net
//
//  Created by Rushikant on 16/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit

class ExhibitorBadges: NSObject {
        
    var   id : String?
    var   stall_id : String?
    var   csv_id : String?
    var   people_id : String?
    var   ticket_id : String?
    var   transaction_id: String?
    var   user_id: String?
    var   sendpass: String?
    var   created_date: String?
    var   invited_date: String?
    var   reply_date: String?
    var   issued_date: String?
    var   verified_date: String?
    var   barcode: String?
    var   city: String?
    var   company_name: String?
    var   country: String?
    var   district: String?
    var   mobile: String?
    var   first_name: String?
    var   last_name: String?
    var   state: String?
    var   fullimageurl: String?
    var   imageurl :String?
    var   contact_name: String?
    var   pass_link: String?
    

    
    init (id : String?,
          stall_id: String?,
          csv_id: String?,
          people_id: String?,
          ticket_id: String?,
          transaction_id: String?,
          user_id: String?,
          sendpass: String?,
          created_date: String?,
          invited_date: String?,
          reply_date: String?,
          issued_date:String?,
          verified_date: String?,
          barcode: String?,
          city: String?,
          company_name: String?,
          country: String?,
          state: String?,
          district: String?,
          last_name: String?,
          mobile: String?,
          first_name: String?,
          imageurl :String?,
          fullimageurl: String?,
          contact_name: String?,
          pass_link: String?){
        
        self.id = id
        self.stall_id = stall_id
        self.csv_id = csv_id
        self.people_id = people_id
        self.ticket_id = ticket_id
        self.transaction_id = transaction_id
        self.user_id = user_id
        self.sendpass = sendpass
        self.created_date = created_date
        self.invited_date = invited_date
        self.reply_date = reply_date
        self.issued_date = issued_date
        self.verified_date = verified_date
        self.barcode = barcode
        self.city = city
        self.company_name = company_name
        self.country = country
        self.state = state
        self.district = district
        self.last_name = last_name
        self.mobile = mobile
        self.first_name = first_name
        self.imageurl = imageurl
        self.fullimageurl = fullimageurl
        self.contact_name = contact_name
        self.pass_link = pass_link
        
    }
    
    
    class func initaiLiseExhibitorBadges() -> ExhibitorBadges {
        let response = ExhibitorBadges(
            id : "",
            stall_id: "",
            csv_id: "",
            people_id: "",
            ticket_id: "",
            transaction_id: "",
            user_id: "",
            sendpass: "",
            created_date: "",
            invited_date: "",
            reply_date: "",
            issued_date:"",
            verified_date: "",
            barcode: "",
            city: "",
            company_name: "",
            country: "",
            state: "",
            district: "",
            last_name: "",
            mobile: "",
            first_name: "",
            imageurl :"",
            fullimageurl: "",
            contact_name: "",
            pass_link: "")
        return response
    }
    
    
    class func getExhibitorBadgesFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhibitorBadges {
        let response = initaiLiseExhibitorBadges()

        response.id = dictionary["id"]  as? String
        response.stall_id = dictionary["stall_id"]  as? String
        response.csv_id = dictionary["csv_id"]  as? String
        response.people_id = dictionary["people_id"]  as? String
        response.ticket_id = dictionary["ticket_id"]  as? String
        response.transaction_id = dictionary["transaction_id"]  as? String
        response.user_id = dictionary["user_id"]  as? String
        response.sendpass = dictionary["sendpass"]  as? String
        response.created_date = dictionary["created_date"]  as? String
        response.invited_date = dictionary["invited_date"]  as? String
        response.reply_date = dictionary["reply_date"]  as? String
        response.issued_date = dictionary["issued_date"]  as? String
        response.verified_date = dictionary["verified_date"]  as? String
        response.barcode = dictionary["barcode"]  as? String
        response.city = dictionary["city"]  as? String
        response.company_name = dictionary["company_name"]  as? String
        response.country = dictionary["country"]  as? String
        response.state = dictionary["state"]  as? String
        response.district = dictionary["district"]  as? String
        response.last_name = dictionary["last_name"]  as? String
        response.mobile = dictionary["mobile"]  as? String
        response.first_name = dictionary["first_name"]  as? String
        response.imageurl = dictionary["imageurl"]  as? String
        response.fullimageurl = dictionary["fullimageurl"]  as? String
        response.contact_name = dictionary["contact_name"]  as? String
        response.pass_link = dictionary["pass_link"]  as? String
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [ExhibitorBadges] {
        var modelArray: [ExhibitorBadges] = [ExhibitorBadges]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getExhibitorBadgesFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
