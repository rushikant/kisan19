//
//  ExhibitorData.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhibitorData: NSObject {
    
    public var first_name: String?
    public var last_name: String?
    public var username: String?
    public var email: String?
    public var user_profile:[ExhibitorProfile]
    public var token:String?
    public var oAuthToken:String?
    
    init(first_name: String?,last_name: String?,username: String?,email: String?,user_profile:[ExhibitorProfile],token: String?,oAuthToken:String?){
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.email = email
        self.user_profile = user_profile
        self.token = token
        self.oAuthToken = oAuthToken
    }
    
    class func initializeUser() -> ExhibitorData {
        let response = ExhibitorData(first_name: "",last_name:"",username:"",email:"", user_profile:[],token:"",oAuthToken:"")
        return response
    }
    
    class func getDictionaryFromUserDetails(_ response: ExhibitorData) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUserDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhibitorData {
        let response = initializeUser()
        response.first_name = dictionary["first_name"] as? String
        response.last_name = dictionary["last_name"] as? String
        response.username = dictionary["username"] as? String
        response.email = dictionary["email"] as? String
        response.oAuthToken = dictionary["oAuthToken"] as? String
        response.token = dictionary["token"] as? String
        let user_profile = dictionary["user_profile"] as? [String: AnyObject?]
        response.user_profile  = [ExhibitorProfile.getExhibitorProfileFromDictionary(user_profile!)]
        return response as ExhibitorData
    }
    
}
