//
//  ExhibitorProfile.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhibitorProfile: NSObject {
    
    public var account_type : String?
    public var city : String?
    public var country : String?
    public var created_by : String?
    public var created_on : String?
    public var email : String?
    public var has_paid : String?
    public var id : String?
    public var interest_other : String?
    public var interests : [String]?
    public var interests_subcategories : [String]?
    public var is_email_verified : String?
    public var is_mobile_verified : String?
    public var kisan_net_status : String?
    public var last_active_datetime : String?
    public var last_modified_by : String?
    public var last_modified_on : String?
    public var mobile1 : String?
    public var mobile2 : String?
    public var organisation_name : String?
    public var pin : String?
    public var registration_type : String?
    public var sessionId : String?
    public var sso_id : String?
    public var state : String?
    public var user : String?
    public var visited_kisan : String?
    public var address1 : String?
    public var image_bigthumb_url : String?
    public var image_smallthumb_url : String?
    public var image_url : String?
    public var latitude : String?
    public var longitude : String?
    public var website : String?


    init(account_type: String?,city: String?,country : String?,created_by : String?,created_on : String?,email : String?,has_paid : String?,id : String?,interest_other : String?,  interests:[String],interests_subcategories:[String],is_email_verified : String?,is_mobile_verified : String?,kisan_net_status : String?,last_active_datetime : String?,last_modified_by : String?,last_modified_on : String?,mobile1 : String?,mobile2 : String?,organisation_name : String?,pin : String?,registration_type : String?,sessionId : String?,sso_id : String?,state : String?,user : String?,visited_kisan : String?,address1:String?,image_bigthumb_url:String?,image_smallthumb_url:String?,image_url:String?,latitude:String?,longitude:String?,website:String?){
        self.account_type = account_type
        self.city = city
        self.country = country
        self.created_by = created_by
        self.created_on = created_on
        self.email = email
        self.has_paid = has_paid
        self.id = id
        self.interest_other = interest_other
        self.interests = interests
        self.interests_subcategories = interests_subcategories
        self.is_email_verified = is_email_verified
        self.is_mobile_verified = is_mobile_verified
        self.kisan_net_status = kisan_net_status
        self.last_active_datetime = last_active_datetime
        self.last_modified_by = last_modified_by
        self.last_modified_on = last_modified_on
        self.mobile1 = mobile1
        self.mobile2 = mobile2
        self.organisation_name = organisation_name
        self.pin = pin
        self.registration_type = registration_type
        self.sessionId = sessionId
        self.sso_id = sso_id
        self.state = state
        self.user = user
        self.visited_kisan = visited_kisan
        self.address1 = address1
        self.image_bigthumb_url = image_bigthumb_url
        self.image_smallthumb_url = image_smallthumb_url
        self.image_url = image_url
        self.latitude = latitude
        self.longitude = longitude
        self.website = website
    }
    
    class func initializeExhibitor() -> ExhibitorProfile {
        let response = ExhibitorProfile(account_type: "",city: "",country : "",created_by : "",created_on : "",email : "",has_paid : "",id : "",interest_other : "",  interests:[""],interests_subcategories:[""],is_email_verified : "",is_mobile_verified : "",kisan_net_status : "",last_active_datetime : "",last_modified_by : "",last_modified_on : "",mobile1 : "",mobile2 : "",organisation_name : "",pin : "",registration_type : "",sessionId : "",sso_id : "",state : "",user : "",visited_kisan : "",address1 : "",image_bigthumb_url : "",image_smallthumb_url:"",image_url : "",latitude : "",longitude : "",website: "")
        return response
    }
    
    class func getDictionaryFromExhibitorProfile(_ response: ExhibitorProfile) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    
    class func getExhibitorProfileFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhibitorProfile {
        let response = initializeExhibitor()
        response.account_type = dictionary["account_type"] as? String
        response.city = dictionary["city"] as? String
        response.country = dictionary["country"] as? String
        response.created_by = dictionary["created_by"] as? String
        response.created_on = dictionary["created_on"] as? String
        response.email = dictionary["email"] as? String
        response.has_paid = dictionary["has_paid"] as? String
        response.id = dictionary["id"] as? String
        response.interest_other = dictionary["interest_other"] as? String
        response.interests = dictionary["interests"] as? [String]
        response.interests_subcategories = dictionary["interests_subcategories"] as? [String]
        response.is_email_verified = dictionary["is_email_verified"] as? String
        response.is_mobile_verified = dictionary["is_mobile_verified"] as? String
        response.kisan_net_status = dictionary["kisan_net_status"] as? String
        response.last_active_datetime = dictionary["last_active_datetime"] as? String
        response.last_modified_by = dictionary["last_modified_by"] as? String
        response.last_modified_on = dictionary["last_modified_on"] as? String
        response.mobile1 = dictionary["mobile1"] as? String
        response.mobile2 = dictionary["mobile2"] as? String
        response.organisation_name = dictionary["organisation_name"] as? String
        response.pin = dictionary["pin"] as? String
        response.registration_type = dictionary["registration_type"] as? String
        response.sessionId = dictionary["sessionId"] as? String
        response.sso_id = dictionary["sso_id"] as? String
        response.state = dictionary["state"] as? String
        response.user = dictionary["user"] as? String
        response.visited_kisan = dictionary["visited_kisan"] as? String
        response.address1 = dictionary["address1"] as? String
        response.image_bigthumb_url = dictionary["image_bigthumb_url"] as? String
        response.image_smallthumb_url = dictionary["image_smallthumb_url"] as? String
        response.image_url = dictionary["image_url"] as? String
        response.latitude = dictionary["latitude"] as? String
        response.longitude = dictionary["longitude"] as? String
        response.website = dictionary["website"] as? String
        return response as ExhibitorProfile
    }
}
