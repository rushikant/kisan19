//
//  ExihibitorScanBrochure.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

/*
{
   "community_key":"c765bb2a-36f5-40ea-ad62-c293d17f2a40",
   "community_name":"john deer",
   "community_logo_url":"https://s3-ap-southeast-1.amazonaws.com/kisan-oauth-dev/images/users/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54.png",
   "community_logo_big_thumb_url":"https://s3-ap-southeast-1.amazonaws.com/kisan-oauth-dev/images/users/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54.thb.png",
   "community_theme_max_color":"#ED3237",
   "community_logo_small_thumb_url":"https://s3-ap-southeast-1.amazonaws.com/kisan-oauth-dev/images/users/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54/c29d36f9-85ab-4ac0-a2a6-1a7eb0fa5d54.ths.png",
   "community_theme_min_color":"#CDCCCD",
   "media_type":"image",
   "media_link":"http://greencloud.kisanlab.com/images/exhibitors/greenpass_invite_media/57_1565268416176.jpeg",
   "media_thumbnail":"http://greencloud.kisanlab.com/images/exhibitors/greenpass_invite_media/thumb/57_1565268416176.thb.jpeg"
       "exhibitor_user_id":57,
       "shortcode":"545"
}

*/
class ExihibitorScanBrochure: NSObject {
    
    var community_key : String?
    var community_name : String?
    var community_logo_url : String?
    var community_logo_big_thumb_url : String?
    var community_theme_max_color : String?
    var community_logo_small_thumb_url : String?
    var community_theme_min_color : String?
    var media_type : String?
    var media_link : String?
    var media_thumbnail : String?
    var exhibitor_user_id: Int?
    var shortcode: String?

    init(community_key : String?, community_name : String?,community_logo_url : String?,community_logo_big_thumb_url : String?,community_theme_max_color : String?,community_logo_small_thumb_url : String?,community_theme_min_color : String?, media_type : String?,media_link : String?,media_thumbnail : String?, exhibitor_user_id: Int?, shortCode: String?) {
        
        self.community_key = community_key
        self.community_name = community_name
        self.community_logo_url = community_logo_url
        self.community_logo_big_thumb_url = community_logo_big_thumb_url
        self.community_theme_max_color = community_theme_max_color
        self.community_logo_small_thumb_url = community_logo_small_thumb_url
        self.community_theme_min_color = community_theme_min_color
        self.media_type = media_type
        self.media_link = media_link
        self.media_thumbnail = media_thumbnail
        self.exhibitor_user_id = exhibitor_user_id
        self.shortcode = shortCode
    }
    
    class func initialiseExihibitorScanBrochure() -> ExihibitorScanBrochure {
        let response = ExihibitorScanBrochure(community_key: "", community_name: "", community_logo_url: "", community_logo_big_thumb_url: "", community_theme_max_color: "", community_logo_small_thumb_url: "", community_theme_min_color: "", media_type: "", media_link: "", media_thumbnail: "", exhibitor_user_id: 0, shortCode: "")
        return response
    }
    
    class func getExihibitorScanBrochureFromDictionary(_ dictionary: [String: AnyObject?]) -> ExihibitorScanBrochure {
        let response = initialiseExihibitorScanBrochure()
        response.community_key = dictionary["community_key"]  as? String
        response.community_name = dictionary["community_name"]  as? String
        response.community_logo_url = dictionary["community_logo_url"]  as? String
        response.community_logo_big_thumb_url = dictionary["community_logo_big_thumb_url"]  as? String
        response.community_theme_max_color = dictionary["community_theme_max_color"]  as? String
        response.community_logo_small_thumb_url = dictionary["community_logo_small_thumb_url"]  as? String
        response.community_theme_min_color = dictionary["community_theme_min_color"]  as? String
        response.media_type = dictionary["media_type"]  as? String
        response.media_link = dictionary["media_link"]  as? String
        response.media_thumbnail = dictionary["media_thumbnail"]  as? String
        response.exhibitor_user_id = dictionary["exhibitor_user_id"]  as? Int
        response.shortcode = dictionary["shortcode"]  as? String
        return response
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [ExihibitorScanBrochure] {
        var modelArray: [ExihibitorScanBrochure] = [ExihibitorScanBrochure]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getExihibitorScanBrochureFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}

class ExihibitorScanBrochureData: NSObject {
    
    var timeInMillis : Int?
    var success : Bool?
    var message : String?
    var responseCode : Int?
    var data : [ExihibitorScanBrochure]?
    
    init(timeInMillis : Int?, success : Bool?,message : String?,responseCode : Int?,data : [ExihibitorScanBrochure]?) {
        self.timeInMillis = timeInMillis
        self.success = success
        self.responseCode = responseCode
        self.data = data
    }
    
    class func initialiseExihibitorScanBrochureData() -> ExihibitorScanBrochureData {
        let response = ExihibitorScanBrochureData(timeInMillis: 0, success: false, message: "", responseCode: 0, data: [])
        return response
    }
    
    class func getExihibitorScanBrochureDataFromDictionary(_ dictionary: [String: AnyObject?]) -> ExihibitorScanBrochureData {
        let response = initialiseExihibitorScanBrochureData()
        response.timeInMillis = dictionary["timeInMillis"]  as? Int
        response.success = dictionary["success"]  as? Bool
        response.message = dictionary["message"]  as? String
        response.responseCode = dictionary["responseCode"]  as? Int
        if let dict = dictionary["inviteSummary"]{
            if dict != nil {
                 response.data = [ExihibitorScanBrochure.getExihibitorScanBrochureFromDictionary(dict as! [String : AnyObject?])]
            }
        }
        
        return response
    }
}
