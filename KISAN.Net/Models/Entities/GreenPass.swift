//
//  GreenPass.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 23/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
class GreenPass:NSObject{
   
private var   barcode: String?
private var   id: String?
private var   transaction_id: String?
private var   people_id: String?
private var   user_id: String?
private var   mobile: String?
private var   first_name: String?
private var   last_name: String?
private var   amount: String?
private var   country: String?
private var   state: String?
private var   district: String?
private var   city: String?
private var   lat: String?
private var   lang: String?
private var   imageurl: String?
private var   passtype_name: String?
private var   entrytype_name:  String?
private var   buyer_firstname: String?
private var   buyer_lastname: String?
private var   company_name: String?
private var   channel_name: String?
private var   channel_big_thumb: String?
private var   channel_max_color: String?
private var   channel_min_color: String?
private var   fullimageurl:String?
    
    init(barcode: String?,id: String?,transaction_id: String?,people_id: String?,user_id: String?,mobile: String?,first_name: String?,last_name: String?,amount: String?,country: String?,state: String?,district: String?,city: String?,lat: String?,lang: String?,imageurl: String?,passtype_name: String?,entrytype_name:  String?,buyer_firstname: String?,buyer_lastname:String?,company_name: String?,channel_name: String?,channel_big_thumb: String?,channel_max_color: String?,channel_min_color: String?,fullimageurl:String?){
    
       self.barcode = barcode
       self.id = id
       self.transaction_id =  transaction_id
       self.people_id = people_id
       self.user_id = user_id
       self.mobile = mobile
       self.first_name = first_name
       self.last_name = last_name
       self.amount = amount
       self.country = country
       self.state = state
       self.district = district
       self.city = city
       self.lat = lat
       self.lang = lang
       self.imageurl = imageurl
        self.passtype_name = passtype_name
       self.entrytype_name =  entrytype_name
       self.buyer_firstname = buyer_firstname
       self.buyer_lastname = buyer_lastname
       self.company_name = company_name
       self.channel_name = channel_name
       self.channel_big_thumb = channel_big_thumb
       self.channel_max_color =  channel_max_color
       self.channel_min_color = channel_min_color
       self.fullimageurl = fullimageurl
    }
    
    class func initializeGreenPassDetails() -> GreenPass {
        let response = GreenPass(barcode: "",id: "",transaction_id: "",people_id: "",user_id: "",mobile: "",first_name: "",last_name: "",amount: "",country: "",state: "",district: "",city: "",lat: "",lang: "",imageurl: "",passtype_name: "",entrytype_name:  "",buyer_firstname: "",buyer_lastname:"",company_name: "",channel_name: "",channel_big_thumb: "",channel_max_color: "",channel_min_color: "",fullimageurl:"")
        return response
    }
    
    class func getDictionaryFromGreenpassDetails(_ response: GreenPass) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getGreenPassDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> GreenPass {
        let response = initializeGreenPassDetails()
        response.barcode  = dictionary["barcode"]  as? String
        response.id = dictionary["id"]  as? String
        response.transaction_id = dictionary[" transaction_id"]  as? String
        response.people_id = dictionary["people_id"]  as? String
        response.user_id = dictionary["user_id"]  as? String
        response.mobile = dictionary["mobile"]  as? String
        response.first_name = dictionary["first_name"]  as? String
        response.last_name = dictionary["last_name"]  as? String
        response.amount = dictionary["amount"]  as? String
        response.country = dictionary["country"]  as? String
        response.state = dictionary["state"]  as? String
        response.district = dictionary["district"]  as? String
        response.city = dictionary["city"]  as? String
        response.lat = dictionary["lat"]  as? String
        response.lang = dictionary["lang"]  as? String
        response.imageurl = dictionary["imageurl"]  as? String
        response.passtype_name = dictionary["passtype_name"]  as? String
        response.entrytype_name = dictionary[" entrytype_name"]  as? String
        response.buyer_firstname = dictionary["buyer_firstname"]  as? String
        response.buyer_lastname = dictionary["buyer_lastname"]  as? String
        response.company_name = dictionary["company_name"]  as? String
        response.channel_name = dictionary["channel_name"]  as? String
        response.channel_big_thumb = dictionary["channel_big_thumb"]  as? String
        response.channel_max_color = dictionary[" channel_max_color"]  as? String
        response.channel_min_color = dictionary["channel_min_color"]  as? String
        response.fullimageurl = dictionary["fullimageurl"]  as? String
        return response as GreenPass
    }
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [GreenPass] {
        var modelArray: [GreenPass] = [GreenPass]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getGreenPassDetailsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
