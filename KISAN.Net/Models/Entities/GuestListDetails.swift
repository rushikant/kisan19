//
//  GuestListDetails.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GuestListDetails: NSObject {
   
 var   id : String?
 var   invite_by_user_id : String?
 var   invitation_code: String?
 var   event_entry_pass_type: String?
 var   first_name: String?
 var   last_name: String?
 var   mobile: String?
 var   country: String?
 var   state: String?
 var   district: String?
 var   imageurl :String?
 var   sms_short_link: String?
 var   ticket_id: String?
 var   user_id: String?
 var   csv_id: String?
 var   summary_id: String?
 var   created_datetime: String?
 var   invited_datetime: String?
 var   collected_datetime: String?
 var   visited_datetime: String?
 var   first_click_datetime: String?
 var   collected_source: String?
 var   invite_source: String?
 var   invited_by_username: String?
 var   invited_by_firstname: String?
 var   invited_by_lastname: String?
 var   invited_by_mobile: String?
 var   channel_key: String?
 var   channel_name: String?
 var   channel_big_thumb: String?
 var   channel_max_color: String?
 var   channel_min_color: String?
 var   type : String?
    var   passtype_name : String?
    var   last_accept_datetime: String?
    var   entrytype_name: String?
 var   sms_name: String?
 var   media_type: String?
 var   media_link: String?
 var   barcode: String?
 var   badge_barcode: String?
 var   badgecreated_datetime: String?
 var   pass_first_name: String?
 var   pass_last_name: String?
 var   pass_state: String?
 var   pass_district: String?
 var   pass_imageurl: String?
 var   entrytypes_name: String?
 var   passtypes_name: String?
 var   fullimageurl: String?
    
    
    
    
    init (id : String?,invite_by_user_id : String?,invitation_code: String?,event_entry_pass_type: String?,first_name: String?,last_name: String?,mobile: String?,country: String?,state: String?,district: String?,imageurl :String?,sms_short_link: String?,ticket_id: String?,user_id: String?,csv_id: String?,summary_id: String?,created_datetime: String?,invited_datetime: String?,collected_datetime: String?,visited_datetime: String?,first_click_datetime: String?,collected_source: String?,invite_source: String?,invited_by_username: String?,invited_by_firstname: String?,invited_by_lastname: String?,invited_by_mobile: String?,channel_key: String?,channel_name: String?,channel_big_thumb: String?,channel_max_color: String?,channel_min_color: String?,type : String?,passtype_name : String?,last_accept_datetime : String?,entrytype_name : String?,sms_name: String?,media_type: String?,media_link: String?,barcode: String?,badge_barcode: String?,badgecreated_datetime: String?,pass_first_name: String?,pass_last_name: String?,pass_state: String?,pass_district: String?,pass_imageurl: String?,entrytypes_name: String?,passtypes_name: String?,fullimageurl: String?){
        
      self.id = id
      self.invite_by_user_id = invite_by_user_id
      self.invitation_code = invitation_code
      self.event_entry_pass_type = event_entry_pass_type
      self.first_name = first_name
      self.last_name = last_name
      self.mobile = mobile
      self.country  = country
      self.state = state
      self.district = district
      self.imageurl = imageurl
      self.sms_short_link = sms_short_link
      self.ticket_id = ticket_id
      self.user_id = user_id
      self.csv_id = csv_id
      self.summary_id = summary_id
      self.created_datetime = created_datetime
      self.invited_datetime = invited_datetime
      self.collected_datetime = collected_datetime
      self.visited_datetime = visited_datetime
      self.first_click_datetime = first_click_datetime
      self.collected_source = collected_source
      self.invite_source = invite_source
      self.invited_by_username = invited_by_username
      self.invited_by_firstname = invited_by_firstname
      self.invited_by_lastname = invited_by_lastname
      self.invited_by_mobile = invited_by_mobile
      self.channel_key = channel_key
      self.channel_name = channel_name
      self.channel_big_thumb = channel_big_thumb
      self.channel_max_color = channel_max_color
      self.channel_min_color = channel_min_color
      self.type  = type
        self.passtype_name  = passtype_name
        self.last_accept_datetime  = last_accept_datetime
        self.entrytype_name  = entrytype_name
      self.sms_name = sms_name
      self.media_type = media_type
      self.media_link = media_link
      self.barcode = barcode
      self.badge_barcode = badge_barcode
      self.badgecreated_datetime = badgecreated_datetime
      self.pass_first_name = pass_first_name
      self.pass_last_name = pass_last_name
      self.pass_state = pass_state
      self.pass_district = pass_district
      self.pass_imageurl = pass_imageurl
      self.entrytypes_name = entrytypes_name
      self.passtypes_name = passtypes_name
      self.fullimageurl = fullimageurl
    }
    
    
    class func initaiLiseGuestListDetails() -> GuestListDetails {
        let response = GuestListDetails(id : "",invite_by_user_id : "",invitation_code: "",event_entry_pass_type: "",first_name: "",last_name: "",mobile: "",country: "",state: "",district: "",imageurl :"",sms_short_link: "",ticket_id: "",user_id: "",csv_id: "",summary_id: "",created_datetime: "",invited_datetime: "",collected_datetime: "",visited_datetime: "",first_click_datetime: "",collected_source: "",invite_source: "",invited_by_username: "",invited_by_firstname: "",invited_by_lastname: "",invited_by_mobile: "",channel_key: "",channel_name: "",channel_big_thumb: "",channel_max_color: "",channel_min_color: "",type : "", passtype_name: "", last_accept_datetime: "", entrytype_name: "",sms_name: "",media_type: "",media_link: "",barcode: "",badge_barcode: "",badgecreated_datetime: "",pass_first_name: "",pass_last_name: "",pass_state: "",pass_district: "",pass_imageurl: "",entrytypes_name: "",passtypes_name: "",fullimageurl: "")
        return response
    }
    
    
    class func genInviteGuestListDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> GuestListDetails {
        let response = initaiLiseGuestListDetails()
        response.id = dictionary["id"]  as? String

        response.invite_by_user_id = dictionary["invite_by_user_id"]  as? String

        response.invitation_code = dictionary["invitation_code"]  as? String

        response.event_entry_pass_type = dictionary["event_entry_pass_type"]  as? String

        response.first_name = dictionary["first_name"]  as? String

        response.last_name = dictionary["last_name"]  as? String

        response.mobile = dictionary["mobile"]  as? String

        response.country  = dictionary["country"]  as? String

        response.state = dictionary["state"]  as? String

        response.district = dictionary["district"]  as? String

        response.imageurl = dictionary["imageurl"]  as? String

        response.sms_short_link = dictionary["sms_short_link"]  as? String

        response.ticket_id = dictionary["ticket_id"]  as? String

        response.user_id = dictionary["user_id"]  as? String

        response.csv_id = dictionary["csv_id"]  as? String

        response.summary_id = dictionary["summary_id"]  as? String

        response.created_datetime = dictionary["created_datetime"]  as? String

        response.invited_datetime = dictionary["invited_datetime"]  as? String

        response.collected_datetime = dictionary["collected_datetime"]  as? String

        response.visited_datetime = dictionary["visited_datetime"]  as? String

        response.first_click_datetime = dictionary["first_click_datetime"]  as? String

        response.collected_source = dictionary["collected_source"]  as? String

        response.invite_source = dictionary["invite_source"]  as? String

        response.invited_by_username = dictionary["invited_by_username"]  as? String

        response.invited_by_firstname = dictionary["invited_by_firstname"]  as? String

        response.invited_by_lastname = dictionary["invited_by_lastname"]  as? String

        response.invited_by_mobile = dictionary["invited_by_mobile"]  as? String

        response.channel_key = dictionary["channel_key"]  as? String

        response.channel_name = dictionary["channel_name"]  as? String

        response.channel_big_thumb = dictionary["channel_big_thumb"]  as? String

        response.channel_max_color = dictionary["channel_max_color"]  as? String

        response.channel_min_color = dictionary["channel_min_color"]  as? String

        response.type  = dictionary["type"]  as? String
        
        response.passtype_name  = dictionary["passtype_name"]  as? String
        
        response.last_accept_datetime  = dictionary["last_accept_datetime"]  as? String
        
        response.entrytype_name  = dictionary["entrytype_name"]  as? String

        response.sms_name = dictionary["sms_name"]  as? String

        response.media_type = dictionary["media_type"]  as? String

        response.media_link = dictionary["media_link"]  as? String

        response.barcode = dictionary["barcode"]  as? String

        response.badge_barcode = dictionary["badge_barcode"]  as? String

        response.badgecreated_datetime = dictionary["badgecreated_datetime"]  as? String

        response.pass_first_name = dictionary["pass_first_name"]  as? String

        response.pass_last_name = dictionary["pass_last_name"]  as? String

        response.pass_state = dictionary["pass_state"]  as? String

        response.pass_district = dictionary["pass_district"]  as? String

        response.pass_imageurl = dictionary["pass_imageurl"]  as? String

        response.entrytypes_name = dictionary["entrytypes_name"]  as? String

        response.passtypes_name = dictionary["passtypes_name"]  as? String

        response.fullimageurl = dictionary["fullimageurl"]  as? String

        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [GuestListDetails] {
        var modelArray: [GuestListDetails] = [GuestListDetails]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.genInviteGuestListDetailsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
