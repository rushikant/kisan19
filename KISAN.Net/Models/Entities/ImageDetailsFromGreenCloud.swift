//
//  ImageDetailsFromGreenCloud.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 30/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ImageDetailsFromGreenCloud: NSObject {
    
    
    var  imgUrl  : String?
    var  imgBigThumbUrl  : String?
    var  imgSmallThumbUrl  : String?
    var  imgFullUrl  : String?
    var  imgBigThumbFullUrl  : String?
    var  imgSmallThumbFullUrl  : String?
    var  imgMaxColor  : String?
    var  imgMinColor  : String?
    
    init( imgUrl  : String?,
       imgBigThumbUrl  : String?,
      imgSmallThumbUrl  : String?,
      imgFullUrl  : String?,
      imgBigThumbFullUrl  : String?,
      imgSmallThumbFullUrl  : String?,
      imgMaxColor  : String?,
      imgMinColor  : String?) {
        
       self.imgUrl = imgUrl
       self.imgBigThumbUrl = imgBigThumbUrl
        self.imgSmallThumbUrl  = imgSmallThumbUrl
        self.imgFullUrl  = imgFullUrl
        self.imgBigThumbFullUrl = imgBigThumbFullUrl
        self.imgSmallThumbFullUrl  = imgSmallThumbFullUrl
         self.imgMaxColor  = imgMaxColor
         self.imgMinColor  = imgMinColor
    }
    
    class func initialiseImageDetails()->ImageDetailsFromGreenCloud{
        let response = ImageDetailsFromGreenCloud(imgUrl  : "" ,imgBigThumbUrl  : "" ,imgSmallThumbUrl  : "" ,imgFullUrl: "" ,imgBigThumbFullUrl: "", imgSmallThumbFullUrl: "" ,imgMaxColor  : "" ,imgMinColor  : "")
        return response
    }
    
    
    class func getDictionaryFromGreenCloudProfileDetails(_ response: ProfileDetails) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }

    
    class func genImageDetailsFromGreenCloud(_ dictionary: [String: AnyObject?]) -> ImageDetailsFromGreenCloud {
        let response = initialiseImageDetails()
        response.imgUrl  =  dictionary["imgUrl"]  as? String
        response.imgBigThumbUrl  =  dictionary["imgBigThumbUrl"]  as? String
        response.imgSmallThumbUrl  =  dictionary["imgSmallThumbUrl"]  as? String
        response.imgFullUrl  =  dictionary["imgFullUrl"]  as? String
        response.imgSmallThumbFullUrl  =  dictionary["imgSmallThumbFullUrl"]  as? String
        response.imgMaxColor  =  dictionary["imgMaxColor"]  as? String
        response.imgMinColor  =  dictionary["imgMinColor"]  as? String
        
        return response
    }

}
