//
//  InvitationInfo.swift
//  KISAN.Net
//
//  Created by MacMini on 11/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
class InvitationInfo: NSObject {
    
    var  success : Bool?
    var invitation_list : [GuestListDetails] = []
    var total_count : Int?
    
    
    init (success : Bool?,
          invitation_list : [GuestListDetails],
          total_count : Int?){
        self.success = success
        self.invitation_list = invitation_list
        self.total_count = total_count
    }
    
    class func initaiLiseInvitationInfo() -> InvitationInfo {
        let response = InvitationInfo(success : false,
                                    invitation_list : [],
                                    total_count : 0)
        return response
    }
    
    class func getInvitationListDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> InvitationInfo {
        let response = initaiLiseInvitationInfo()
        response.success = dictionary["success"]  as? Bool
        response.total_count = dictionary["total_count"]  as? Int
        if dictionary["invitation_list"] != nil {
            response.invitation_list = GuestListDetails.getModelFromArray(dictionary["invitation_list"] as! [AnyObject])
        }
        if dictionary["invite_info"] != nil {
            response.invitation_list = GuestListDetails.getModelFromArray([dictionary["invite_info"] as! [String:AnyObject]]as! [AnyObject])
        }
        
        
        return response
    }
}
