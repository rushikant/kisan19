
//  InviteSummaryRecords.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 25/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InviteSummaryRecords: NSObject {
    var  firstname  : String?
    var  lastname  : String?
    var  email  : String?
    var  imageurl  : String?
    var  company_name  : String?
    var  id  : String?
    var  user_id  : String?
    var  type  : String?
    var  transaction_id  : String?
    var  interests : String?
    var  channel_key  : String?
    var  channel_name  : String?
    var  channel_big_thumb  : String?
    var  channel_max_color  : String?
    var  channel_min_color  : String?
    var  sms_name  : String?
    var  skip_media : Int = 0
    var  sms_name_updated_datetime  : String?
    var  media_type  : String?
    var  media_link  : String?
    var  media_thumbnail  : String?
    var  media_updated_datetime  : String?
    var  unlimited_standard  : Int?
    var  unlimited_extra  : Int?
    var  unlimited_sent  : Int?
    var  unlimited_collected  : Int?
    var  unlimited_visited  : Int?
    var  quota_standard  : Int?
    var  quota_extra  : Int?
    var  quota_sent  : Int?
    var  quota_collected  : Int?
    var  quota_visited  : Int?
    var  created_datetime : String?
    var  updated_datetime : String?
    var  username  : String?
    var  mobile  : String?
    var  community_key  : String?
    var  community_stall_id  : String?
    
    
    init(firstname  : String? ,lastname  : String? ,email  : String? ,imageurl  : String? ,company_name  : String? ,id  : String? ,
    user_id  : String? ,type  : String? ,transaction_id  : String? ,interests : String? ,channel_key  : String? ,channel_name  : String? ,channel_big_thumb  : String? ,channel_max_color  : String? ,channel_min_color  : String? ,sms_name  : String? ,skip_media :Int = 0,sms_name_updated_datetime  : String? ,media_type  : String? ,media_link  : String? ,media_thumbnail  :String? ,media_updated_datetime  : String? ,unlimited_standard  : Int? ,unlimited_extra  : Int? ,unlimited_sent  : Int? ,unlimited_collected  : Int? ,unlimited_visited  : Int? ,quota_standard  : Int? ,quota_extra  : Int? ,quota_sent  : Int? ,quota_collected  : Int? ,quota_visited  : Int? ,created_datetime : String? ,updated_datetime : String? ,username  : String? ,mobile  : String? ,community_key  : String? ,community_stall_id  : String?){
        
        
        self.firstname  = firstname
        self.lastname  = lastname
        self.email  = email
        self.imageurl  = imageurl
        self.company_name  = company_name
        self.id  = id
        self.user_id  = user_id
        self.type  = type
        self.transaction_id  = transaction_id
        self.interests = interests
        self.channel_key  = channel_key
        self.channel_name  = channel_name
        self.channel_big_thumb  = channel_big_thumb
        self.channel_max_color  = channel_max_color
        self.channel_min_color  = channel_min_color
        self.sms_name  = sms_name
        self.skip_media  = skip_media
        self.sms_name_updated_datetime  = sms_name_updated_datetime
        self.media_type  = media_type
        self.media_link  = media_link
        self.media_thumbnail  = media_thumbnail
        self.media_updated_datetime  = media_updated_datetime
        self.unlimited_standard  =  unlimited_standard
        self.unlimited_extra  = unlimited_extra
        self.unlimited_sent  = unlimited_sent
        self.unlimited_collected  = unlimited_collected
        self.unlimited_visited  = unlimited_visited
        self.quota_standard  = quota_standard
        self.quota_extra  = quota_extra
        self.quota_sent  = quota_sent
        self.quota_collected  = quota_collected
        self.quota_visited  = quota_visited
        self.created_datetime = created_datetime
        self.updated_datetime = updated_datetime
        self.username  = updated_datetime
        self.mobile  = mobile
        self.community_key  = community_key
        self.community_stall_id  = community_stall_id
    
    }
    
    class func initaiLiseInviteSummeryRecords() -> InviteSummaryRecords {
        let response = InviteSummaryRecords(firstname  : "" ,lastname  : "" ,email  : "" ,imageurl  : "" ,company_name  : "" ,id  : "" ,user_id  : "" ,type  : "" ,transaction_id  : "" ,interests : "" ,channel_key  : "" ,channel_name  : "" ,channel_big_thumb  : "" ,channel_max_color  : "" ,channel_min_color  : "" ,sms_name  : "" ,skip_media : 0,sms_name_updated_datetime  : "" ,media_type  : "" ,media_link  : "" ,media_thumbnail  :"" ,media_updated_datetime  : "" ,unlimited_standard  : 0 ,unlimited_extra  : 0 ,unlimited_sent  : 0 ,unlimited_collected  : 0 ,unlimited_visited  : 0 ,quota_standard  : 0 ,quota_extra  : 0 ,quota_sent  : 0 ,quota_collected  : 0 ,quota_visited  : 0 ,created_datetime : "" ,updated_datetime : "" ,username  : "" ,mobile  : "" ,community_key  : "" ,community_stall_id  : "")
        return response
    }
    
    
    class func genInviteSummeryRecordsFromDictionary(_ dictionary: [String: AnyObject?]) -> InviteSummaryRecords {
        let response = initaiLiseInviteSummeryRecords()
        
        response.firstname  =  dictionary["firstname"]  as? String
       response.lastname  =  dictionary["lastname"]  as? String
       response.email  =  dictionary["email"]  as? String
       response.imageurl  =  dictionary["imageurl"]  as? String
       response.company_name  =  dictionary["company_name"]  as? String
       response.id  =  dictionary["id"]  as? String
       response.user_id  =  dictionary["user_id"]  as? String
       response.type  =  dictionary["type"]  as? String
       response.transaction_id  =  dictionary["transaction_id"]  as? String
       response.interests =  dictionary["interests"]  as? String
       response.channel_key  =  dictionary["channel_key"]  as? String
       response.channel_name  =  dictionary["channel_name"]  as? String
       response.channel_big_thumb  =  dictionary["channel_big_thumb"]  as? String
       response.channel_max_color  =  dictionary["channel_max_color"]  as? String
       response.channel_min_color  =  dictionary["channel_min_color"]  as? String
       response.sms_name  =  dictionary["sms_name  "]  as? String
       response.skip_media  =  dictionary["skip_media"]  as! Int
       response.sms_name_updated_datetime  =  dictionary["sms_name_updated_datetime"]  as? String
       response.media_type  =  dictionary["media_type"]  as? String
       response.media_link  =  dictionary["media_link"]  as? String
       response.media_thumbnail  =  dictionary["media_thumbnail"]  as? String
       response.media_updated_datetime  =  dictionary["media_updated_datetime"]  as? String
       response.unlimited_standard  =  dictionary["unlimited_standard"]  as? Int
       response.unlimited_extra  =  dictionary["unlimited_extra"]  as? Int
       response.unlimited_sent  =  dictionary["unlimited_sent"]  as? Int
       response.unlimited_collected  =  dictionary["unlimited_collected"]  as? Int
       response.unlimited_visited  =  dictionary["unlimited_visited"]  as? Int
       response.quota_standard  =  dictionary["quota_standard"]  as? Int
       response.quota_extra  =  dictionary["quota_extra"]  as? Int
       response.quota_sent  =  dictionary["quota_sent"]  as? Int
       response.quota_collected  =  dictionary["quota_collected"]  as? Int
       response.quota_visited  =  dictionary["quota_visited"]  as? Int
       response.created_datetime =  dictionary["created_datetime"]  as? String
       response.updated_datetime =  dictionary["updated_datetime"]  as? String
       response.username  =  dictionary["updated_datetime"]  as? String
       response.mobile  =  dictionary["mobile"]  as? String
       response.community_key  =  dictionary["community_key"]  as? String
       response.community_stall_id  =  dictionary["community_stall_id"]  as? String
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [InviteSummaryRecords] {
        var modelArray: [InviteSummaryRecords] = [InviteSummaryRecords]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.genInviteSummeryRecordsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}
