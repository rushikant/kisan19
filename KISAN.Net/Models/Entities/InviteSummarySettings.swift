//
//  InviteSummarySettings.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 18/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InviteSummarySettings: NSObject {
    
    var greenpass_accept_last_date : String?
    var issue_greenpass_last_date : String?
    var issue_greenpass_start_date : String?
    var show_greenpass_issue_last_date_warning : Int?
    var show_issue_greenpass : Int?
    var show_issue_unlimited_greenpass : Int?
    var show_unlimited_greenpass_accept : Int?
    var show_unlimited_greenpass_issue_last_date_warning : Int?
    var unlimited_greenpass_accepted_lastdate : String?
    var unlimited_greenpass_issued_lastdate : String?
    var issue_greenass_last_date : String?
    

    init(  greenpass_accept_last_date : String?,issue_greenpass_last_date : String?,issue_greenpass_start_date : String?,show_greenpass_issue_last_date_warning : Int,show_issue_greenpass : Int,show_issue_unlimited_greenpass: Int,show_unlimited_greenpass_accept:Int,show_unlimited_greenpass_issue_last_date_warning:Int,unlimited_greenpass_accepted_lastdate:String,unlimited_greenpass_issued_lastdate:String,issue_greenass_last_date:String) {
        self.greenpass_accept_last_date = greenpass_accept_last_date
        self.issue_greenpass_last_date = issue_greenpass_last_date
        self.issue_greenpass_start_date = issue_greenpass_start_date
        self.show_greenpass_issue_last_date_warning = show_greenpass_issue_last_date_warning
        self.show_issue_greenpass = show_issue_greenpass
        self.show_issue_unlimited_greenpass = show_issue_unlimited_greenpass
        self.show_unlimited_greenpass_accept =  show_unlimited_greenpass_accept
        self.show_unlimited_greenpass_issue_last_date_warning = show_unlimited_greenpass_issue_last_date_warning
        self.unlimited_greenpass_accepted_lastdate = unlimited_greenpass_accepted_lastdate
        self.unlimited_greenpass_issued_lastdate = unlimited_greenpass_issued_lastdate
        self.issue_greenass_last_date = issue_greenass_last_date

    }
    
    
    
    class func initaiLiseinviteSummarySettings() -> InviteSummarySettings {
        let response = InviteSummarySettings(greenpass_accept_last_date : "",issue_greenpass_last_date : "",issue_greenpass_start_date : "",show_greenpass_issue_last_date_warning : 0 ,show_issue_greenpass : 0,show_issue_unlimited_greenpass : 0, show_unlimited_greenpass_accept: 0,show_unlimited_greenpass_issue_last_date_warning : 0, unlimited_greenpass_accepted_lastdate : "",unlimited_greenpass_issued_lastdate : "",issue_greenass_last_date:"")
        return response
    }
    
    class func getinviteSummarySettingsFromDictionary(_ dictionary: [String: AnyObject?]) -> InviteSummarySettings {
        let response = initaiLiseinviteSummarySettings()
        
        if let greenpass_accept_last_date = dictionary["greenpass_accept_last_date"]  as? String{
            response.greenpass_accept_last_date = greenpass_accept_last_date
        }
        
        if let issue_greenpass_last_date = dictionary["issue_greenpass_last_date"]  as? String{
         response.issue_greenpass_last_date = issue_greenpass_last_date
        }
       
        if let issue_greenass_last_date = dictionary["issue_greenass_last_date"]  as? String{
         response.issue_greenpass_last_date = issue_greenass_last_date
        }
        
        if let issue_greenpass_start_date = dictionary["issue_greenpass_start_date"]  as? String{
            response.issue_greenpass_start_date = issue_greenpass_start_date
        }
      
        if let show_greenpass_issue_last_date_warning = (dictionary["show_greenpass_issue_last_date_warning"] as? Int){
            response.show_greenpass_issue_last_date_warning = show_greenpass_issue_last_date_warning
        }
       
        if let show_issue_greenpass = (dictionary["show_issue_greenpass"] as? Int){
            response.show_issue_greenpass = show_issue_greenpass
        }
      
        if let show_issue_unlimited_greenpass = (dictionary["show_issue_unlimited_greenpass"] as? Int){
            response.show_issue_unlimited_greenpass = show_issue_unlimited_greenpass
        }
        if let show_unlimited_greenpass_accept = (dictionary["show_unlimited_greenpass_accept"] as? Int){
            response.show_unlimited_greenpass_accept = show_unlimited_greenpass_accept
        }
        
        if let show_unlimited_greenpass_issue_last_date_warning = (dictionary["show_unlimited_greenpass_issue_last_date_warning"] as? Int){
            response.show_unlimited_greenpass_issue_last_date_warning = show_unlimited_greenpass_issue_last_date_warning
        }
        
        
        if let unlimited_greenpass_accepted_lastdate = dictionary["unlimited_greenpass_accepted_lastdate"]  as? String{
            response.unlimited_greenpass_accepted_lastdate = unlimited_greenpass_accepted_lastdate
        }
        
        if let unlimited_greenpass_issued_lastdate = dictionary["unlimited_greenpass_issued_lastdate"]  as? String{
            response.unlimited_greenpass_issued_lastdate = unlimited_greenpass_issued_lastdate
        }
        
        if let issue_greenass_last_date = dictionary["issue_greenass_last_date"]  as? String{
            response.issue_greenass_last_date = issue_greenass_last_date
        }

        return response
    }
}
