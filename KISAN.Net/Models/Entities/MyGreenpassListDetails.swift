//
//  MyGreenpassListDetails.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 14/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit

class MyGreenpassListDetails: NSObject {
    
    var   id : String?
    var   amount : String?
    var   barcode: String?
    var   buyer_firstname: String?
    var   buyer_lastname: String?
    var   channel_big_thumb: String?
    var   channel_max_color: String?
    var   channel_min_color: String?
    var   channel_name: String?
    var   city: String?
    var   company_name: String?
    var   country: String?
    var   district: String?
    var   entrytype_name: String?
    var   first_name: String?
    var   fullimageurl: String?
    var   imageurl :String?
    var   lang :String?
    var   last_name: String?
    var   lat: String?
    var   mobile: String?
    var   passtype_name : String?
    var   people_id : String?
    var   state: String?
    var   transaction_id: String?
    var   user_id: String?
    var   badge_barcode: String?

    
    
    init (id : String?,amount : String?,barcode: String?,buyer_firstname: String?,buyer_lastname: String?,channel_big_thumb: String?,channel_max_color: String?,channel_min_color: String?,channel_name: String?,city: String?,company_name: String?,last_name: String?,mobile: String?,country: String?,state: String?,district: String?,entrytype_name : String?,first_name: String?,imageurl :String?,lang: String?,user_id: String?,lat: String?,people_id: String?,transaction_id: String?,fullimageurl: String?,passtype_name: String?,badge_barcode: String?){
        
       
        self.id = id
        self.amount = amount
        self.barcode = barcode
        self.buyer_firstname  = buyer_firstname
        self.buyer_lastname  = buyer_lastname
        self.channel_name = channel_name
        self.channel_big_thumb = channel_big_thumb
        self.channel_max_color = channel_max_color
        self.channel_min_color = channel_min_color
        self.country  = country
        self.city = city
        self.company_name = company_name
        self.district = district
        self.first_name = first_name
        self.last_name = last_name
        self.mobile = mobile
        self.state = state
        self.imageurl = imageurl
        self.user_id = user_id
        self.people_id = people_id
        self.transaction_id = transaction_id
        self.entrytype_name  = entrytype_name
        self.fullimageurl = fullimageurl
        self.lang = lang
        self.lat = lat
        self.passtype_name = passtype_name
        self.badge_barcode = badge_barcode
        
    }
    
    
    class func initaiLiseMyGreenpassListDetails() -> MyGreenpassListDetails {
        let response = MyGreenpassListDetails(id: "", amount: "", barcode: "", buyer_firstname: "", buyer_lastname: "", channel_big_thumb: "", channel_max_color: "", channel_min_color: "", channel_name: "", city: "", company_name: "", last_name: "", mobile: "", country: "", state: "", district: "", entrytype_name: "", first_name: "", imageurl: "", lang: "", user_id: "", lat: "", people_id: "", transaction_id: "", fullimageurl: "", passtype_name: "",badge_barcode : "")
        return response
    }
    
    
    class func getMyGreenpassListDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> MyGreenpassListDetails {
        let response = initaiLiseMyGreenpassListDetails()
        response.id = dictionary["id"]  as? String
        
        response.barcode = dictionary["barcode"]  as? String
        
        
        if let badgebarcode = dictionary["badge_barcode"]  as? String{
            response.badge_barcode = badgebarcode
        }else{
            response.badge_barcode = ""
        }

    
        response.amount = dictionary["amount"]  as? String
        
        response.buyer_firstname = dictionary["buyer_firstname"]  as? String
        
        response.buyer_lastname = dictionary["buyer_lastname"]  as? String
        
        response.first_name = dictionary["first_name"]  as? String
        
        response.last_name = dictionary["last_name"]  as? String
        
        response.mobile = dictionary["mobile"]  as? String
        
        response.country  = dictionary["country"]  as? String
        
        response.state = dictionary["state"]  as? String
        
        response.district = dictionary["district"]  as? String
        
        response.imageurl = dictionary["imageurl"]  as? String
        
        response.city = dictionary["city"]  as? String
        
        response.company_name = dictionary["company_name"]  as? String
        
        response.user_id = dictionary["user_id"]  as? String
        
        response.lang = dictionary["lang"]  as? String
        
        response.lat = dictionary["lat"]  as? String
        
        response.people_id = dictionary["people_id"]  as? String
        
        response.transaction_id = dictionary["transaction_id"]  as? String
        
        response.channel_name = dictionary["channel_name"]  as? String
        
        response.channel_big_thumb = dictionary["channel_big_thumb"]  as? String
        
        response.channel_max_color = dictionary["channel_max_color"]  as? String
        
        response.channel_min_color = dictionary["channel_min_color"]  as? String
        
        response.passtype_name  = dictionary["passtype_name"]  as? String
        
        response.entrytype_name  = dictionary["entrytype_name"]  as? String
        
        response.fullimageurl = dictionary["fullimageurl"]  as? String
        
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [MyGreenpassListDetails] {
        var modelArray: [MyGreenpassListDetails] = [MyGreenpassListDetails]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getMyGreenpassListDetailsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
