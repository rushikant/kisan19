//
//  OldMessagesList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class OldMessagesList: NSObject {
    
    /*var contentField1 : String?
    var createdDatetime : String?
    var id : String?
    var communityImageBigThumbUrl : String?
    var communityImageSmallThumbUrl : String?
    var communityImageUrl : String?
    var communityJabberId : String?
    var communityKey : String?
    var communityName : String?
    var communityScore : String?
    var groupRole : String?
    var isMember : String?
    var ownerFirstName : String?
    var ownerId : String?
    var ownerImageUrl : String?
    var ownerLastName : String?
    var ownerName : String?
    var privacy : String?
    var totalMembers : Int?
    var type : String?
    
    
//    "contentField1": "hello",
//    "createdDatetime": "1527752839000",
//    "id": "64cb0eee-ef18-4d3c-9bcc-7e0d5cba4e04",
//    "jabberId": "1527069795477_e0c3e336-8f85-4a9b-b4ad-454440c6a85e",
//    "mediaType": "text",
//    "messageType": "chat",
//    "senderBigThumbnailUrl": "",
//    "senderFirstName": "Sk",
//    "senderId": "3623926c-3083-47ac-90fb-a68f34ccd66a",
//    "senderImageUrl": "",
//    "senderLastActiveDatetime": "1528799164000",
//    "senderLastName": "Kisan",
//    "senderProfilestatus": "",
//    "senderRole": "owner",
//    "senderSmallThumbnailUrl": "",
//    "senderUserName": "288629731637687",
//    "userCommunityJabberID": "1527069795477_e0c3e336-8f85-4a9b-b4ad-454440c6a85e",
//    "userRole": "member"
    
    init(communityCategories: String?,
         communityCreatedTimestamp: String?,
         communityDesc: String?,communityImageBigThumbUrl: String?,communityImageSmallThumbUrl: String?,communityImageUrl: String?,communityJabberId: String?,communityKey: String?,communityName: String?,communityScore: String?,groupRole: String?,isMember: String?,ownerFirstName: String?,ownerId: String?,ownerImageUrl: String?,ownerLastName: String?,ownerName: String?,privacy: String?,totalMembers: Int?,type: String?){
        self.communityCategories = communityCategories
        self.communityCreatedTimestamp = communityCreatedTimestamp
        self.communityDesc = communityDesc
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.communityImageUrl = communityImageUrl
        self.communityJabberId = communityJabberId
        self.communityKey = communityKey
        self.communityName = communityName
        self.communityScore = communityScore
        self.groupRole = groupRole
        self.isMember = isMember
        self.ownerFirstName = ownerFirstName
        self.ownerId = ownerId
        self.ownerImageUrl = ownerImageUrl
        self.ownerLastName = ownerLastName
        self.ownerName = ownerName
        self.privacy = privacy
        self.totalMembers = totalMembers
        self.type = type
    }
    
    
    
    class func initializeCommonChannelList() -> CommonChannelList {
        let model = CommonChannelList(communityCategories: "",
                                      communityCreatedTimestamp: "", communityDesc: "",
                                      communityImageBigThumbUrl: "",
                                      communityImageSmallThumbUrl: "",communityImageUrl: "",communityJabberId:"",communityKey:"",communityName:"",communityScore:"",groupRole:"",isMember:"",ownerFirstName:"",ownerId:"",ownerImageUrl:"",ownerLastName:"",ownerName:"", privacy: "",totalMembers:-1,type:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: CommonChannelList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getCommonChannelListFromDictionary(_ dictionary: [String: AnyObject?]) -> CommonChannelList {
        let model = initializeCommonChannelList()
        model.communityCategories = dictionary["communityCategories"] as? String
        model.communityCreatedTimestamp = dictionary["communityCreatedTimestamp"] as? String
        model.communityDesc = dictionary["communityDesc"] as? String
        model.communityImageBigThumbUrl = dictionary["communityImageBigThumbUrl"] as? String
        model.communityImageSmallThumbUrl = dictionary["communityImageSmallThumbUrl"] as? String
        model.communityImageUrl = dictionary["communityImageUrl"] as? String
        model.communityJabberId = dictionary["communityJabberId"] as? String
        model.communityKey = dictionary["communityKey"] as? String
        model.communityName = dictionary["communityName"] as? String
        model.communityScore = dictionary["communityScore"] as? String
        model.groupRole = dictionary["groupRole"] as? String
        model.isMember = dictionary["isMember"] as? String
        model.ownerFirstName = dictionary["ownerFirstName"] as? String
        model.ownerId = dictionary["ownerId"] as? String
        model.ownerImageUrl = dictionary["ownerImageUrl"] as? String
        model.ownerLastName = dictionary["ownerLastName"] as? String
        model.ownerName = dictionary["ownerName"] as? String
        model.privacy = dictionary["privacy"] as? String
        model.totalMembers = dictionary["totalMembers"] as? Int
        model.type = dictionary["type"] as? String
        
        return model as CommonChannelList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [CommonChannelList] {
        var modelArray: [CommonChannelList] = [CommonChannelList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getCommonChannelListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }*/
    
}
