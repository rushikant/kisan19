//
//  PavilionList.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 03/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class PavilionList: NSObject {
   
    var name: String?
    var image_url: String?
    var categoryCountOnDiscoverList: String?
    
    init(name: String?,
         image_url: String,
         categoryCountOnDiscoverList:String){
        self.name = name
        self.image_url = image_url
        self.categoryCountOnDiscoverList = categoryCountOnDiscoverList
    }
    
    class func initializePavilionList() -> PavilionList {
        let model = PavilionList(name: "",
                                   image_url: "",
                                   categoryCountOnDiscoverList:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: PavilionList) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getPavilionListFromDictionary(_ dictionary: [String: AnyObject?]) -> PavilionList {
        let model = initializePavilionList()
       
        model.name = dictionary["name"] as? String
        model.image_url = dictionary["image_url"] as? String
        model.categoryCountOnDiscoverList = ""
        return model as PavilionList
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [PavilionList] {
        var modelArray: [PavilionList] = [PavilionList]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getPavilionListFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
