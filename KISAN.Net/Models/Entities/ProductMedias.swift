//
//  ProductMedias.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 16/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ProductMedias: NSObject {
    
    var type: String?
    var url: String?
   
    init(type: String?,
         url: String?){
        self.type = type
        self.url = url
    }
    
    class func initializeProductMedias() -> ProductMedias {
        let model = ProductMedias(type: "",url: "")
        return model
    }
    
    class func getDictionaryFromModel(_ model: ProductMedias) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getProductMediasFromDictionary(_ dictionary: [String: AnyObject?]) -> ProductMedias {
        let model = initializeProductMedias()
        model.type = dictionary["type"] as? String
        model.url = dictionary["url"] as? String
        
        return model as ProductMedias
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [ProductMedias] {
        var modelArray: [ProductMedias] = [ProductMedias]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getProductMediasFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
