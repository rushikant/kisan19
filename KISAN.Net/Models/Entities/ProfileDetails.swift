//
//  ProfileDetails.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 24/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ProfileDetails: NSObject {

 var id: String?
 var username: String?
 var mobile: String?
 var email: String?
 var country: String?
 var state: String?
 var district: String?
 var city: String?
 var firstname: String?
 var lastname: String?
 var imageUrl: String?
 var imagesmallthumburl: String?
 var imagebigthumburl: String?
 var interests: String?
 var organisation_name: String?
 var sessionId: String?
    
    init( id: String?,username: String?,mobile: String?,email: String?,country: String?,state: String?,district: String?,city:String?,firstname: String?,lastname: String?,imageUrl: String?,imagesmallthumburl: String?,imagebigthumburl: String?,
        interests: String?,organisation_name:String?,sessionId: String?) {

       self.id = id
       self.username = username
       self.mobile = mobile
       self.email = email
       self.country = country
       self.state = state
       self.district = district
       self.city = city
       self.firstname = firstname
       self.lastname = lastname
       self.imageUrl = imageUrl
       self.imagesmallthumburl = imagesmallthumburl
       self.imagebigthumburl = imagebigthumburl
       self.interests = interests
       self.organisation_name = organisation_name
       self.sessionId = sessionId
    
    }
    

    class func initializeProfileDetails() -> ProfileDetails {
        let response = ProfileDetails(id: "",username: "",mobile: "",email: "",country: "",state: "",district: "",city:"",firstname: "",lastname: "",imageUrl: "",imagesmallthumburl: "",imagebigthumburl: "",interests: "",organisation_name:"",sessionId: "")
        return response
    }
    
    class func getDictionaryFromGreenCloudProfileDetails(_ response: ProfileDetails) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }

    class func getGreenCloudUserDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> ProfileDetails {
        let response = initializeProfileDetails()
       
        response.id  = dictionary["id"] as? String
        response.username  = dictionary["username"] as? String
        response.mobile  = dictionary["mobile"] as? String
        response.email  = dictionary["email"] as? String
        response.country  = dictionary["country"] as? String
        response.state  = dictionary["state"] as? String
        response.district  = dictionary["district"] as? String
        response.city  = dictionary["city"] as? String
        response.firstname  = dictionary["firstname"] as? String
        response.lastname  = dictionary["lastname"] as? String
        response.imageUrl  = dictionary["imageUrl"] as? String
        response.imagesmallthumburl  = dictionary["imagesmallthumburl"] as? String
        response.imagebigthumburl  = dictionary["imagebigthumburl"] as? String
        response.interests  = dictionary["interests"] as? String
        response.organisation_name  = dictionary["organisation_name"] as? String
        response.sessionId  = dictionary["sessionId"] as? String
        return response as ProfileDetails
    }


    
    
}
