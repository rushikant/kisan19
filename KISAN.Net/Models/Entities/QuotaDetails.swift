//
//  QuotaDetails.swift
//  KISAN.Net
//
//  Created by Rushikant on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class QuotaDetails: NSObject {
    
    var  id  : Int?
    var  entry_pass_type_id  : Int?
    var  standard  : Int?
    var  extra  : Int?
    var  total  : Int?
    var  issued  : Int?
    var  used  : Int?
    var  attended  : Int?
    var  balance  : Int?
    var  display_name : String?
    var  maxticketspervisitor  : Int?
    var  invite_by : String?
    
    init(id  : Int?,
         entry_pass_type_id  : Int?,
         standard  : Int?,
         extra  : Int?,
         total  : Int?,
         issued  : Int?,
         used  : Int?,
         attended  : Int?,
         balance  : Int?,
         display_name : String?,
         maxticketspervisitor  : Int?,
         invite_by : String?
    ){
        self.id = id
        self.entry_pass_type_id = entry_pass_type_id
        self.standard = standard
        self.extra = extra
        self.total = total
        self.issued = issued
        self.used = used
        self.attended = attended
        self.balance = balance
        self.display_name = display_name
        self.maxticketspervisitor = maxticketspervisitor
        self.invite_by = invite_by
        
    }
    
    class func initaiLiseQuotaDetails() -> QuotaDetails {
        let response = QuotaDetails(
            id  : 0,
            entry_pass_type_id  : 0,
            standard  : 0,
            extra  : 0,
            total  : 0,
            issued  : 0,
            used  : 0,
            attended  : 0,
            balance  : 0,
            display_name : "",
            maxticketspervisitor  : 0,
            invite_by : ""
        )
        return response
    }
    
    
    class func getQuotaDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> QuotaDetails {
        let response = initaiLiseQuotaDetails()
        
        response.id  =  dictionary["id"]  as? Int
        response.entry_pass_type_id  =  dictionary["entry_pass_type_id"]  as? Int
        response.standard  =  dictionary["standard"]  as? Int
        response.extra  =  dictionary["extra"]  as? Int
        response.total  =  dictionary["total"]  as? Int
        response.issued  =  dictionary["issued"]  as? Int
        response.used  =  dictionary["used"]  as? Int
        response.attended  =  dictionary["attended"]  as? Int
        response.balance  =  dictionary["balance"]  as? Int
        response.display_name  =  dictionary["display_name"]  as? String
        response.maxticketspervisitor  =  dictionary["maxticketspervisitor"]  as? Int
        response.invite_by  =  dictionary["invite_by"]  as? String
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [QuotaDetails] {
        var modelArray: [QuotaDetails] = [QuotaDetails]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getQuotaDetailsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}


