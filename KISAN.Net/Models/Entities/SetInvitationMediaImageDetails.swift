//
//  SetInvitationMediaImageDetails.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class SetInvitationMediaImageDetails: NSObject {
    
    var  mediaUrl  : String?
    var  mediaThumbUrl  : String?
    var  mediaType  : String?
    
    
    init( mediaUrl  : String?,
          mediaThumbUrl  : String?,
          mediaType  : String?) {
        
        self.mediaUrl = mediaUrl
        self.mediaThumbUrl = mediaThumbUrl
        self.mediaType  = mediaType
    }
    
    class func initialiseImageDetails()->SetInvitationMediaImageDetails{
        let response = SetInvitationMediaImageDetails(mediaUrl  : "" ,mediaThumbUrl  : "" ,mediaType  : "")
        return response
    }
    
    
    class func getDictionaryFromGreenCloudProfileDetails(_ response: ProfileDetails) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    
    class func genSetInvitationMediaImageDetails(_ dictionary: [String: AnyObject?]) -> SetInvitationMediaImageDetails {
        let response = initialiseImageDetails()
        response.mediaUrl  =  dictionary["mediaUrl"]  as? String
        response.mediaThumbUrl  =  dictionary["mediaThumbUrl"]  as? String
        response.mediaType  =  dictionary["mediaType"]  as? String
        return response
    }


}
