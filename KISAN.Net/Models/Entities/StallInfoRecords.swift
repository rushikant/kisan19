//
//  StallInfoRecords.swift
//  KISAN.Net
//
//  Created by Rushikant on 19/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class StallInfoRecords: NSObject {
    
    var  id  : Int?
    var  exhibitor_id: Int?
    var  transaction_id  : Int?
    var  company_name : String?
    var  company_name_marathi : String?
    var  contact_person_first_name : String?
    var  contact_person_last_name : String?
    var  contact_person_mobile  : String?
    var  contact_person_email  : String?
    var  contact_person_designation  : String?
    var  company_email  : String?
    var  company_phone  : String?
    var  company_website  : String?
    var  fascia_name  : String?
    var  company_sms_name  : String?
    var  product_info  : String?
    var  product_images  : String?
    var  product_range_covers  : String?
    var  product_range_covers_others  : String?
    var  product_category  : String?
    var  product_category_other  : String?
    var  logo_url  : String?
    var  logo_big_thumb  : String?
    var  logo_small_thumb  : String?
    var  country  : String?
    var  state  : String?
    var  city  : String?
    var  addressline1  : String?
    var  addressline2  : String?
    var  address  : String?
    var  zip  : String?
    var  pavillion_type  : String?
    var  stand_type  : String?
    var  stand_location  : String?
    var  area  : Int?
    var  sku  : String?
    var  shop_order_id  : String?
    var  shop_product_id  : String?
    var  shop_variant_id  : String?
    var  base_amount  : Int?
    var  total_discount  : Int?
    var  total_tax  : Int?
    var  tds_amount  : Int?
    var  paid_amount  : Int?
    var  isGSTIN  : Int?
    var  GSTIN  : String?
    var  is_formsubmitted  : Int?
    var  status  : String?
    var  layout_stall_no  : String?
    var  created_date  : String?
    var  updated_date  : String?
    var  faircat_status  : Int?
    var  last_formlink_send_datetime  : String?
    var  stall_reject_reason  : String?
    var  form_submitted_date  : String?
    var  faircat_locked_datetime  : String?
    var  stall_approved_datetime  : String?
    var  stall_rejected_datetime  : String?
    var  isActive  : Int?
    var  priority_number  : String?
    var  updated_datetime  : String?
    var  booked_datetime  : String?
    var  is_trusted  : Int?
    var  payment_status  : String?
    var  payment_datetime  : String?
    var  payment_gateway  : String?
    var  payment_method  : String?
    var  booked_by_fname  : String?
    var  booked_by_lname  : String?
    var  booked_by_mobile  : String?
    var  booked_by_email  : String?
    var  profile_completed_datetime  : String?
    var  first_greenpass_issued_datetime  : String?
    var  first_badge_issued_datetime  : String?
    var  first_extra_item_paid_datetime  : String?
    var  good_to_go_datetime  : String?
    var  alloted_stall_no  : String?
    var  alloted_hall_no  : String?
    var  history_prefilled_stall_info  : Int?
    var  show_history_prefilled_message  : Int?
    var  user_id  : Int?
    var  username  : String?
    var  pavillion_name  : String?
    var  stand_type_name  : String?
    var  stand_location_name  : String?
    var  pavillion_banner_image  : String?
    var  exhibitor_fname  : String?
    var  exhibitor_lname  : String?
    var  stand_image  : String?
    var  exhibitor_mobile  : String?
    var  exhibitor_email  : String?
    var  vendor_coordinator_id  : Int?
    var  stall_design_images  : String?
    var  exhibitor_name  : String?
    var  booked_by_name  : String?
    var  community_key  : String?
    var  community_stall_id  : Int?
    var  community_name  : String?
    var  community_logo_url  : String?
    var  community_logo_big_thumb_url  : String?
    var  community_logo_small_thumb_url  : String?
    var  community_theme_max_color  : String?
    var  community_theme_min_color  : String?
    var  is_vendor_coordination_form_editable  : Int?
    var  vendor_coordination_form_submits  : Int?
    var  profile_completenes_percentages  : Int?
    var  quota_details  : [QuotaDetails]?
//    var  furniture_details  : String?

    
    init(
        id  : Int?,
        exhibitor_id: Int?,
        transaction_id  : Int?,
        company_name : String?,
        company_name_marathi : String?,
        contact_person_first_name : String?,
        contact_person_last_name : String?,
        contact_person_mobile  : String?,
        contact_person_email  : String?,
        contact_person_designation  : String?,
        company_email  : String?,
        company_phone  : String?,
        company_website  : String?,
        fascia_name  : String?,
        company_sms_name  : String?,
        product_info  : String?,
        product_images  : String?,
        product_range_covers  : String?,
        product_range_covers_others  : String?,
        product_category  : String?,
        product_category_other  : String?,
        logo_url  : String?,
        logo_big_thumb  : String?,
        logo_small_thumb  : String?,
        country  : String?,
        state  : String?,
        city  : String?,
        addressline1  : String?,
        addressline2  : String?,
        address  : String?,
        zip  : String?,
        pavillion_type  : String?,
        stand_type  : String?,
        stand_location  : String?,
        area  : Int?,
        sku  : String?,
        shop_order_id  : String?,
        shop_product_id  : String?,
        shop_variant_id  : String?,
        base_amount  : Int?,
        total_discount  : Int?,
        total_tax  : Int?,
        tds_amount  : Int?,
        paid_amount  : Int?,
        isGSTIN  : Int?,
        GSTIN  : String?,
        is_formsubmitted  : Int?,
        status  : String?,
        layout_stall_no  : String?,
        created_date  : String?,
        updated_date  : String?,
        faircat_status  : Int?,
        last_formlink_send_datetime  : String?,
        stall_reject_reason  : String?,
        form_submitted_date  : String?,
        faircat_locked_datetime  : String?,
        stall_approved_datetime  : String?,
        stall_rejected_datetime  : String?,
        isActive  : Int?,
        priority_number  : String?,
        updated_datetime  : String?,
        booked_datetime  : String?,
        is_trusted  : Int?,
        payment_status  : String?,
        payment_datetime  : String?,
        payment_gateway  : String?,
        payment_method  : String?,
        booked_by_fname  : String?,
        booked_by_lname  : String?,
        booked_by_mobile  : String?,
        booked_by_email  : String?,
        profile_completed_datetime  : String?,
        first_greenpass_issued_datetime  : String?,
        first_badge_issued_datetime  : String?,
        first_extra_item_paid_datetime  : String?,
        good_to_go_datetime  : String?,
        alloted_stall_no  : String?,
        alloted_hall_no  : String?,
        history_prefilled_stall_info  : Int?,
        show_history_prefilled_message  : Int?,
        user_id  : Int?,
        username  : String?,
        pavillion_name  : String?,
        stand_type_name  : String?,
        stand_location_name  : String?,
        pavillion_banner_image  : String?,
        exhibitor_fname  : String?,
        exhibitor_lname  : String?,
        stand_image  : String?,
        exhibitor_mobile  : String?,
        exhibitor_email  : String?,
        vendor_coordinator_id  : Int?,
        stall_design_images  : String?,
        exhibitor_name  : String?,
        booked_by_name  : String?,
        community_key  : String?,
        community_stall_id  : Int?,
        community_name  : String?,
        community_logo_url  : String?,
        community_logo_big_thumb_url  : String?,
        community_logo_small_thumb_url  : String?,
        community_theme_max_color  : String?,
        community_theme_min_color  : String?,
        is_vendor_coordination_form_editable  : Int?,
        vendor_coordination_form_submits  : Int?,
        profile_completenes_percentages  : Int?,
        quota_details  : [QuotaDetails]?
//        furniture_details  : String?
    ){
        
        
        self.id = id
        self.exhibitor_id = exhibitor_id
        self.transaction_id = transaction_id
        self.company_name = company_name
        self.company_name_marathi = company_name_marathi
        self.contact_person_first_name = contact_person_first_name
        self.contact_person_last_name = contact_person_last_name
        self.contact_person_mobile = contact_person_mobile
        self.contact_person_email = contact_person_email
        self.contact_person_designation = contact_person_designation
        self.company_email = company_email
        self.company_phone = company_phone
        self.company_website = company_website
        self.fascia_name = fascia_name
        self.company_sms_name = company_sms_name
        self.product_info = product_info
        self.product_images = product_images
        self.product_range_covers = product_range_covers
        self.product_range_covers_others = product_range_covers_others
        self.product_category = product_category
        self.product_category_other = product_category_other
        self.logo_url = logo_url
        self.logo_big_thumb = logo_big_thumb
        self.logo_small_thumb = logo_small_thumb
        self.country = country
        self.state = state
        self.city = city
        self.addressline1 = addressline1
        self.addressline2 = addressline2
        self.address = address
        self.zip = zip
        self.pavillion_type = pavillion_type
        self.stand_type = stand_type
        self.stand_location = stand_location
        self.area = area
        self.sku = sku
        self.shop_order_id = shop_order_id
        self.shop_product_id = shop_product_id
        self.shop_variant_id = shop_variant_id
        self.base_amount = base_amount
        self.total_discount = total_discount
        self.total_tax = total_tax
        self.tds_amount = tds_amount
        self.paid_amount = paid_amount
        self.isGSTIN = isGSTIN
        self.GSTIN = GSTIN
        self.is_formsubmitted = is_formsubmitted
        self.status = status
        self.layout_stall_no = layout_stall_no
        self.created_date = created_date
        self.updated_date = updated_date
        self.faircat_status = faircat_status
        self.last_formlink_send_datetime = last_formlink_send_datetime
        self.stall_reject_reason = stall_reject_reason
        self.form_submitted_date = form_submitted_date
        self.faircat_locked_datetime = faircat_locked_datetime
        self.stall_approved_datetime = stall_approved_datetime
        self.stall_rejected_datetime = stall_rejected_datetime
        self.isActive = isActive
        self.priority_number = priority_number
        self.updated_datetime = updated_datetime
        self.booked_datetime = booked_datetime
        self.is_trusted = is_trusted
        self.payment_status = payment_status
        self.payment_datetime = payment_datetime
        self.payment_gateway = payment_gateway
        self.payment_method = payment_method
        self.booked_by_fname = booked_by_fname
        self.booked_by_lname = booked_by_lname
        self.booked_by_mobile = booked_by_mobile
        self.booked_by_email = booked_by_email
        self.profile_completed_datetime = profile_completed_datetime
        self.first_greenpass_issued_datetime = first_greenpass_issued_datetime
        self.first_badge_issued_datetime = first_badge_issued_datetime
        self.first_extra_item_paid_datetime = first_extra_item_paid_datetime
        self.good_to_go_datetime = good_to_go_datetime
        self.alloted_stall_no = alloted_stall_no
        self.alloted_hall_no = alloted_hall_no
        self.history_prefilled_stall_info = history_prefilled_stall_info
        self.show_history_prefilled_message = show_history_prefilled_message
        self.user_id = user_id
        self.username = username
        self.pavillion_name = pavillion_name
        self.stand_type_name = stand_type_name
        self.stand_location_name = stand_location_name
        self.pavillion_banner_image = pavillion_banner_image
        self.exhibitor_fname = exhibitor_fname
        self.exhibitor_lname = exhibitor_lname
        self.stand_image = stand_image
        self.exhibitor_mobile = exhibitor_mobile
        self.exhibitor_email = exhibitor_email
        self.vendor_coordinator_id = vendor_coordinator_id
        self.stall_design_images = stall_design_images
        self.exhibitor_name = exhibitor_name
        self.booked_by_name = booked_by_name
        self.community_key = community_key
        self.community_stall_id = community_stall_id
        self.community_name = community_name
        self.community_logo_url = community_logo_url
        self.community_logo_big_thumb_url = community_logo_big_thumb_url
        self.community_logo_small_thumb_url = community_logo_small_thumb_url
        self.community_theme_max_color = community_theme_max_color
        self.community_theme_min_color = community_theme_min_color
        self.is_vendor_coordination_form_editable = is_vendor_coordination_form_editable
        self.vendor_coordination_form_submits = vendor_coordination_form_submits
        self.profile_completenes_percentages = profile_completenes_percentages
        self.quota_details = quota_details
//        self.furniture_details = furniture_details
        
    }
    
    class func initaiLiseStallInfoRecords() -> StallInfoRecords {
        let response = StallInfoRecords(
            id  : 0,
            exhibitor_id: 0,
            transaction_id  : 0,
            company_name : "",
            company_name_marathi : "",
            contact_person_first_name : "",
            contact_person_last_name : "",
            contact_person_mobile  : "",
            contact_person_email  : "",
            contact_person_designation  : "",
            company_email  : "",
            company_phone  : "",
            company_website  : "",
            fascia_name  : "",
            company_sms_name  : "",
            product_info  : "",
            product_images  : "",
            product_range_covers  : "",
            product_range_covers_others  : "",
            product_category  : "",
            product_category_other  : "",
            logo_url  : "",
            logo_big_thumb  : "",
            logo_small_thumb  : "",
            country  : "",
            state  : "",
            city  : "",
            addressline1  : "",
            addressline2  : "",
            address  : "",
            zip  : "",
            pavillion_type  : "",
            stand_type  : "",
            stand_location  : "",
            area  : 0,
            sku  : "",
            shop_order_id  : "",
            shop_product_id  : "",
            shop_variant_id  : "",
            base_amount  : 0,
            total_discount  : 0,
            total_tax  : 0,
            tds_amount  : 0,
            paid_amount  : 0,
            isGSTIN  : 0,
            GSTIN  : "",
            is_formsubmitted  : 0,
            status  : "",
            layout_stall_no  : "",
            created_date  : "",
            updated_date  : "",
            faircat_status  : 0,
            last_formlink_send_datetime  : "",
            stall_reject_reason  : "",
            form_submitted_date  : "",
            faircat_locked_datetime  : "",
            stall_approved_datetime  : "",
            stall_rejected_datetime  : "",
            isActive  : 0,
            priority_number  : "",
            updated_datetime  : "",
            booked_datetime  : "",
            is_trusted  : 0,
            payment_status  : "",
            payment_datetime  : "",
            payment_gateway  : "",
            payment_method  : "",
            booked_by_fname  : "",
            booked_by_lname  : "",
            booked_by_mobile  : "",
            booked_by_email  : "",
            profile_completed_datetime  : "",
            first_greenpass_issued_datetime  : "",
            first_badge_issued_datetime  : "",
            first_extra_item_paid_datetime  : "",
            good_to_go_datetime  : "",
            alloted_stall_no  : "",
            alloted_hall_no  : "",
            history_prefilled_stall_info  : 0,
            show_history_prefilled_message  : 0,
            user_id  : 0,
            username  : "",
            pavillion_name  : "",
            stand_type_name  : "",
            stand_location_name  : "",
            pavillion_banner_image  : "",
            exhibitor_fname  : "",
            exhibitor_lname  : "",
            stand_image  : "",
            exhibitor_mobile  : "",
            exhibitor_email  : "",
            vendor_coordinator_id  : 0,
            stall_design_images  : "",
            exhibitor_name  : "",
            booked_by_name  : "",
            community_key  : "",
            community_stall_id  : 0,
            community_name  : "",
            community_logo_url  : "",
            community_logo_big_thumb_url  : "",
            community_logo_small_thumb_url  : "",
            community_theme_max_color  : "",
            community_theme_min_color  : "",
            is_vendor_coordination_form_editable  : 0,
            vendor_coordination_form_submits  : 0,
            profile_completenes_percentages  : 0,
            quota_details  : []
//            furniture_details  : ""
        )
        return response
    }
    
    
    class func getStallInfoRecordsFromDictionary(_ dictionary: [String: AnyObject?]) -> StallInfoRecords {
        let response = initaiLiseStallInfoRecords()
        
        response.id  =  dictionary["id"]  as? Int
        response.exhibitor_id  =  dictionary["exhibitor_id"]  as? Int
        response.transaction_id  =  dictionary["transaction_id"]  as? Int
        response.company_name  =  dictionary["company_name"]  as? String
        response.company_name_marathi  =  dictionary["company_name_marathi"]  as? String
        response.contact_person_first_name  =  dictionary["contact_person_first_name"]  as? String
        response.contact_person_last_name  =  dictionary["contact_person_last_name"]  as? String
        response.contact_person_mobile  =  dictionary["contact_person_mobile"]  as? String
        response.contact_person_email  =  dictionary["contact_person_email"]  as? String
        response.contact_person_designation  =  dictionary["contact_person_designation"]  as? String
        response.company_email  =  dictionary["company_email"]  as? String
        response.company_phone  =  dictionary["company_phone"]  as? String
        response.company_website  =  dictionary["company_website"]  as? String
        response.fascia_name  =  dictionary["fascia_name"]  as? String
        response.company_sms_name  =  dictionary["company_sms_name"]  as? String
        response.product_info  =  dictionary["product_info"]  as? String
        response.product_images  =  dictionary["product_images"]  as? String
        response.product_range_covers  =  dictionary["product_range_covers"]  as? String
        response.product_range_covers_others  =  dictionary["product_range_covers_others"]  as? String
        response.product_category  =  dictionary["product_category"]  as? String
        response.product_category_other  =  dictionary["product_category_other"]  as? String
        response.logo_url  =  dictionary["logo_url"]  as? String
        response.logo_big_thumb  =  dictionary["logo_big_thumb"]  as? String
        response.logo_small_thumb  =  dictionary["logo_small_thumb"]  as? String
        response.country  =  dictionary["country"]  as? String
        response.state  =  dictionary["state"]  as? String
        response.city  =  dictionary["city"]  as? String
        response.addressline1  =  dictionary["addressline1"]  as? String
        response.addressline2  =  dictionary["addressline2"]  as? String
        response.address  =  dictionary["address"]  as? String
        response.zip  =  dictionary["zip"]  as? String
        response.pavillion_type  =  dictionary["pavillion_type"]  as? String
        response.stand_type  =  dictionary["stand_type"]  as? String
        response.stand_location  =  dictionary["stand_location"]  as? String
        response.area  =  dictionary["area"]  as? Int
        response.sku  =  dictionary["sku"]  as? String
        response.shop_order_id  =  dictionary["shop_order_id"]  as? String
        response.shop_product_id  =  dictionary["shop_product_id"]  as? String
        response.shop_variant_id  =  dictionary["shop_variant_id"]  as? String
        response.base_amount  =  dictionary["base_amount"]  as? Int
        response.total_discount  =  dictionary["total_discount"]  as? Int
        response.total_tax  =  dictionary["total_tax"]  as? Int
        response.tds_amount  =  dictionary["tds_amount"]  as? Int
        response.paid_amount  =  dictionary["paid_amount"]  as? Int
        response.isGSTIN  =  dictionary["isGSTIN"]  as? Int
        response.GSTIN  =  dictionary["GSTIN"]  as? String
        response.is_formsubmitted  =  dictionary["is_formsubmitted"]  as? Int
        response.status  =  dictionary["status"]  as? String
        response.layout_stall_no  =  dictionary["layout_stall_no"]  as? String
        response.created_date  =  dictionary["created_date"]  as? String
        response.updated_date  =  dictionary["updated_date"]  as? String
        response.faircat_status  =  dictionary["faircat_status"]  as? Int
        response.last_formlink_send_datetime  =  dictionary["last_formlink_send_datetime"]  as? String
        response.stall_reject_reason  =  dictionary["stall_reject_reason"]  as? String
        response.form_submitted_date  =  dictionary["form_submitted_date"]  as? String
        response.faircat_locked_datetime  =  dictionary["faircat_locked_datetime"]  as? String
        response.stall_approved_datetime  =  dictionary["stall_approved_datetime"]  as? String
        response.stall_rejected_datetime  =  dictionary["stall_rejected_datetime"]  as? String
        response.isActive  =  dictionary["isActive"]  as? Int
        response.priority_number  =  dictionary["priority_number"]  as? String
        response.updated_datetime  =  dictionary["updated_datetime"]  as? String
        response.booked_datetime  =  dictionary["booked_datetime"]  as? String
        response.is_trusted  =  dictionary["is_trusted"]  as? Int
        response.payment_status  =  dictionary["payment_status"]  as? String
        response.payment_datetime  =  dictionary["payment_datetime"]  as? String
        response.payment_gateway  =  dictionary["payment_gateway"]  as? String
        response.payment_method  =  dictionary["payment_method"]  as? String
        response.booked_by_fname  =  dictionary["booked_by_fname"]  as? String
        response.booked_by_lname  =  dictionary["booked_by_lname"]  as? String
        response.booked_by_mobile  =  dictionary["booked_by_mobile"]  as? String
        response.booked_by_email  =  dictionary["booked_by_email"]  as? String
        response.profile_completed_datetime  =  dictionary["profile_completed_datetime"]  as? String
        response.first_greenpass_issued_datetime  =  dictionary["first_greenpass_issued_datetime"]  as? String
        response.first_badge_issued_datetime  =  dictionary["first_badge_issued_datetime"]  as? String
        response.first_extra_item_paid_datetime  =  dictionary["first_extra_item_paid_datetime"]  as? String
        response.good_to_go_datetime  =  dictionary["good_to_go_datetime"]  as? String
        response.alloted_stall_no  =  dictionary["alloted_stall_no"]  as? String
        response.alloted_hall_no  =  dictionary["alloted_hall_no"]  as? String
        response.history_prefilled_stall_info  =  dictionary["history_prefilled_stall_info"]  as? Int
        response.show_history_prefilled_message  =  dictionary["show_history_prefilled_message"]  as? Int
        response.user_id  =  dictionary["user_id"]  as? Int
        response.username  =  dictionary["username"]  as? String
        response.pavillion_name  =  dictionary["pavillion_name"]  as? String
        response.stand_type_name  =  dictionary["stand_type_name"]  as? String
        response.stand_location_name  =  dictionary["stand_location_name"]  as? String
        response.pavillion_banner_image  =  dictionary["pavillion_banner_image"]  as? String
        response.exhibitor_fname  =  dictionary["exhibitor_fname"]  as? String
        response.exhibitor_lname  =  dictionary["exhibitor_lname"]  as? String
        response.stand_image  =  dictionary["stand_image"]  as? String
        response.exhibitor_mobile  =  dictionary["exhibitor_mobile"]  as? String
        response.exhibitor_email  =  dictionary["exhibitor_email"]  as? String
        response.vendor_coordinator_id  =  dictionary["vendor_coordinator_id"]  as? Int
        response.stall_design_images  =  dictionary["stall_design_images"]  as? String
        response.exhibitor_name  =  dictionary["exhibitor_name"]  as? String
        response.booked_by_name  =  dictionary["booked_by_name"]  as? String
        response.community_key  =  dictionary["community_key"]  as? String
        response.community_stall_id  =  dictionary["community_stall_id"]  as? Int
        response.community_name  =  dictionary["community_name"]  as? String
        response.community_logo_url  =  dictionary["community_logo_url"]  as? String
        response.community_logo_big_thumb_url  =  dictionary["community_logo_big_thumb_url"]  as? String
        response.community_logo_small_thumb_url  =  dictionary["community_logo_small_thumb_url"]  as? String
        response.community_theme_max_color  =  dictionary["community_theme_max_color"]  as? String
        response.community_theme_min_color  =  dictionary["community_theme_min_color"]  as? String
        response.is_vendor_coordination_form_editable  =  dictionary["is_vendor_coordination_form_editable"]  as? Int
        response.vendor_coordination_form_submits  =  dictionary["vendor_coordination_form_submits"]  as? Int
        response.profile_completenes_percentages  =  dictionary["profile_completenes_percentages"]  as? Int
        
        if dictionary["quota_details"] != nil {
            response.quota_details = QuotaDetails.getModelFromArray(dictionary["quota_details"] as! [AnyObject])
        }
        
        
//        response.furniture_details  =  dictionary["furniture_details"]  as? String
        
        return response
    }
    
    
    
    class func getModelFromArray(_ array: [AnyObject]) -> [StallInfoRecords] {
        var modelArray: [StallInfoRecords] = [StallInfoRecords]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getStallInfoRecordsFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}

