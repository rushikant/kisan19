//
//  SummaryDetailsOfIssueInvite.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 09/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class SummaryDetailsOfIssueInvite: NSObject {
    
    var  passes  : Int?
    var  pass_error_visitors  : Int?
    var  valid_visitors  : Int?
    var invalid_visitors : Int?
    var duplicate_visitors : Int?
    
    init( passes  : Int?,
          pass_error_visitors  : Int?,
          valid_visitors  : Int?,invalid_visitors  : Int?,duplicate_visitors  : Int?) {
        self.passes = passes
        self.pass_error_visitors = pass_error_visitors
        self.valid_visitors  = valid_visitors
        self.invalid_visitors = invalid_visitors
        self.duplicate_visitors  = duplicate_visitors
    }
    
    class func initialiseSummaryDetailsOfIssueInvite()->SummaryDetailsOfIssueInvite{
        let response = SummaryDetailsOfIssueInvite(passes  : -1 ,pass_error_visitors  : -1 ,valid_visitors  : -1,invalid_visitors: -1, duplicate_visitors: -1)
        return response
    }
    
    
    class func getDictionaryFromGreenCloudProfileDetails(_ response: SummaryDetailsOfIssueInvite) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    
    class func genSummaryDetailsOfIssueInvite(_ dictionary: [String: AnyObject?]) -> SummaryDetailsOfIssueInvite {
        let response = initialiseSummaryDetailsOfIssueInvite()
        response.passes  =  dictionary["passes"]  as? Int
        response.pass_error_visitors  =  dictionary["pass_error_visitors"]  as? Int
        response.valid_visitors  =  dictionary["valid_visitors"]  as? Int
        response.invalid_visitors  =  dictionary["invalid_visitors"]  as? Int
        response.duplicate_visitors  =  dictionary["duplicate_visitors"]  as? Int

        return response
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [SummaryDetailsOfIssueInvite] {
        var modelArray: [SummaryDetailsOfIssueInvite] = [SummaryDetailsOfIssueInvite]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.genSummaryDetailsOfIssueInvite(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
    
}
