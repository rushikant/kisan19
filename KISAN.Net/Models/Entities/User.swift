//
//  User.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class User: NSObject {

    public var firstName: String?
    public var lastName: String?
    public var username: String?
    public var email: String?
    public var userProfile:[UserProfile]
    public var userId:String?
   
    init(firstName: String?,lastName: String?,username: String?,email: String?,userProfile:[UserProfile],userId: String?){
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
        self.email = email
        self.userProfile = userProfile
        self.userId = userId
    }
    
    class func initializeUser() -> User {
        let response = User(firstName: "",lastName:"",username:"",email:"", userProfile:[],userId:"")
        return response
    }
    
    class func getDictionaryFromUserDetails(_ response: User) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUserDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> User {
        let response = initializeUser()
        response.firstName = dictionary["firstName"] as? String
        response.lastName = dictionary["lastName"] as? String
        response.username = dictionary["username"] as? String
        response.userId = dictionary["userId"] as? String
        response.email = dictionary["email"] as? String
        let userProfile = dictionary["userProfile"] as? [String: AnyObject?]
        response.userProfile  = [UserProfile.getUserProfileFromDictionary(userProfile!)]
        return response as User
    }
    
}
