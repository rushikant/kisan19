//
//  UserProfile.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class UserProfile: NSObject {
    
    public var about : String?
    public var address1 : String?
    public var address2 : String?
    public var address3 : String?
    public var avatar : String?
    public var city : String?
    public var contact_first_name : String?
    public var contact_last_name : String?
    public var country : String?
    public var designation : String?
    public var email : String?
    public var email2 : String?
    public var gender : String?
    public var has_paid : String?
    public var imageBigthumbUrl : String?
    public var imageSmallthumbUrl : String?
    public var imageUrl : String?
    public var interestOther : String?
    public var interests : [String]?
    public var isEmailVerified : String?
    public var isMobileVerified : String?
    public var kisan_net_status : String?
    public var land_line : String?
    public var latitude : String?
    public var longitude : String?
    public var location : String?
    public var mobile1 : String?
    public var mobile2 : String?
    public var occupation : String?
    public var occupation_other : String?
    public var organisationName : String?
    public var pin : String?
    public var source : String?
    public var visitedKisan : String?
    public var state : String?
    public var website : String?
    public var accountType: Int?
    
    init(country: String?,about: String?, address1:String?,address2:String,address3: String?, avatar:String?, city:String?,contact_first_name:String,contact_last_name:String,designation:String,email:String,email2:String,gender:String,has_paid:String,imageBigthumbUrl:String,imageSmallthumbUrl:String,imageUrl:String,interestOther:String, interests:[String],isEmailVerified:String,isMobileVerified:String,kisan_net_status:String,land_line:String,latitude:String,longitude:String,location:String,mobile1:String,mobile2:String,occupation:String,occupation_other:String,organisationName:String,pin:String,source:String,visitedKisan:String,state:String,website:String,accountType:Int){
        self.country = country
        self.about = about
        self.address1 = address1
        self.address2 = address2
        self.address3 = address3
        self.avatar = avatar
        self.city = city
        self.contact_first_name = contact_first_name
        self.contact_last_name = contact_last_name
        self.designation = designation
        self.email = email
        self.email2 = email2
        self.gender = gender
        self.has_paid = has_paid
        self.imageBigthumbUrl = imageBigthumbUrl
        self.imageSmallthumbUrl = imageSmallthumbUrl
        self.imageUrl = imageUrl
        self.interestOther = interestOther
        self.interests = interests
        self.isEmailVerified = isEmailVerified
        self.isMobileVerified = isMobileVerified
        self.kisan_net_status = kisan_net_status
        self.land_line = land_line
        self.latitude = latitude
        self.longitude = longitude
        self.location = location
        self.mobile1 = mobile1
        self.mobile2 = mobile2
        self.occupation = occupation
        self.occupation_other = occupation_other
        self.organisationName = organisationName
        self.pin = pin
        self.source = source
        self.visitedKisan = visitedKisan
        self.state = state
        self.website = website
        self.accountType = accountType
    }
    
    class func initializeUser() -> UserProfile {
        let response = UserProfile(country: "",about: "",address1: "", address2:"",address3:"", avatar:"", city:"",contact_first_name:"", contact_last_name:"", designation:"",email:"",email2:"",gender:"",has_paid:"",imageBigthumbUrl:"",imageSmallthumbUrl:"", imageUrl:"", interestOther:"",interests:[""],isEmailVerified:"",isMobileVerified:"",kisan_net_status:"",land_line:"",latitude:"",longitude:"",location:"",mobile1:"",mobile2:"",occupation:"",occupation_other:"",organisationName:"",pin:"",source:"",visitedKisan:"",state:"",website:"",accountType:-1)
        return response
    }
    
    class func getDictionaryFromUserProfile(_ response: UserProfile) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUserProfileFromDictionary(_ dictionary: [String: AnyObject?]) -> UserProfile {
        let response = initializeUser()
        response.country = dictionary["country"] as? String
        response.about = dictionary["about"] as? String
        response.address1 = dictionary["address1"] as? String
        response.address2 = dictionary["address2"] as? String
        response.address3 = dictionary["address3"] as? String
        response.avatar = dictionary["avatar"] as? String
        response.city = dictionary["city"] as? String
        response.contact_first_name = dictionary["contact_first_name"] as? String
        response.contact_last_name = dictionary["contact_last_name"] as? String
        response.designation = dictionary["designation"] as? String
        response.email = dictionary["email"] as? String
        response.email2 = dictionary["email2"] as? String
        response.gender = dictionary["gender"] as? String
        response.has_paid = dictionary["has_paid"] as? String
        response.imageBigthumbUrl = dictionary["imageBigthumbUrl"] as? String
        response.imageSmallthumbUrl = dictionary["imageSmallthumbUrl"] as? String
        response.imageUrl = dictionary["imageUrl"] as? String
        response.interestOther = dictionary["interestOther"] as? String
        response.interests = dictionary["interests"] as? [String]
        response.isEmailVerified = dictionary["isEmailVerified"] as? String
        response.isMobileVerified = dictionary["isMobileVerified"] as? String
        response.kisan_net_status = dictionary["kisan_net_status"] as? String
        response.land_line = dictionary["land_line"] as? String
        response.latitude = dictionary["latitude"] as? String
        response.longitude = dictionary["longitude"] as? String
        response.location = dictionary["location"] as? String
        response.mobile1 = dictionary["mobile1"] as? String
        response.mobile2 = dictionary["mobile2"] as? String
        response.occupation = dictionary["occupation"] as? String
        response.occupation_other = dictionary["occupation_other"] as? String
        response.organisationName = dictionary["organisationName"] as? String
        response.pin = dictionary["pin"] as? String
        response.source = dictionary["source"] as? String
        response.visitedKisan = dictionary["visitedKisan"] as? String
        response.state = dictionary["state"] as? String
        response.website = dictionary["website"] as? String
        response.accountType = dictionary["accountType"] as? Int
        return response as UserProfile
    }
    
}
