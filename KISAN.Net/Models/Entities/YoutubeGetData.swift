//
//  YoutubeGetData.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/02/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class YoutubeGetData: NSObject {
  
    var title: String?
    var channelTitle: String?
    var thumnailImgUrl1: String?
    var thumnailImgUrl2: String?
    var descreption: String?

    init(title: String?,
         descreption: String?,
         channelTitle: String,
         thumnailImgUrl1: String,thumnailImgUrl2:String){
        self.title = title
        self.descreption = descreption
        self.channelTitle = channelTitle
        self.thumnailImgUrl1 = thumnailImgUrl1
        self.thumnailImgUrl2 = thumnailImgUrl2
    }
    
    class func initializeYoutubeUrlData() -> YoutubeGetData {
        let model = YoutubeGetData(title: "",
                                   descreption: "",
                                   channelTitle: "",
                                   thumnailImgUrl1: "",thumnailImgUrl2:"")
        return model
    }
    
    class func getDictionaryFromModel(_ model: YoutubeGetData) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getYoutubeUrlDataFromDictionary(_ dictionary: [String: AnyObject?]) -> YoutubeGetData {
        let model = initializeYoutubeUrlData()
        let dictdata = dictionary["snippet"] as! [String: AnyObject]
        let thumnailData = dictdata["thumbnails"] as! [String: AnyObject]
        let mediumthumnailDict = thumnailData["default"] as! [String: AnyObject]
        let HighthumnailDict = thumnailData["high"] as! [String: AnyObject]
        model.title = dictdata["title"] as? String
        model.descreption = dictdata["description"] as? String
        model.channelTitle = dictdata["channelTitle"] as? String
        model.thumnailImgUrl1 = mediumthumnailDict["url"] as? String
        model.thumnailImgUrl2 = HighthumnailDict["url"] as? String
        return model as YoutubeGetData
    }
    
    class func getModelFromArray(_ array: [AnyObject]) -> [YoutubeGetData] {
        var modelArray: [YoutubeGetData] = [YoutubeGetData]()
        for dict in array {
            if let info =  dict as? [String: AnyObject] {
                let model = self.getYoutubeUrlDataFromDictionary(info)
                modelArray.append(model)
            }
        }
        return modelArray
    }
}
