//
//  MobileOtpVerificationRequest.swift
//  KISAN.Net
//
//  Created by Rushikant on 26/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

class MobileOtpVerificationRequest: NSObject {
            
            var client_id :String?
            var client_secret :String?
            var verification_token :String?
            var verification_code: String?
            var account_type :Int
            var source :String
            
            
            
        init( client_id : String?,
              client_secret :String?,
              verification_token :String?,
              verification_code: String?,
              account_type:Int,
              source:String){
                
                self.client_id = client_id
                self.client_secret = client_secret
                self.verification_token = verification_token
                self.verification_code = verification_code
                self.account_type = account_type
                self.source = source
                
            }
            

            
            class func convertToDictionary(MobileOtpVerificationRequest:MobileOtpVerificationRequest) -> Dictionary<String, Any> {
                return [
                    "client_id": MobileOtpVerificationRequest.client_id!,
                    "client_secret": MobileOtpVerificationRequest.client_secret!,
                    "verification_token" : MobileOtpVerificationRequest.verification_token!,
                    "verification_code": MobileOtpVerificationRequest.verification_code!,
                    "account_type": MobileOtpVerificationRequest.account_type,
                    "source": MobileOtpVerificationRequest.source
                ]
            }
    }

