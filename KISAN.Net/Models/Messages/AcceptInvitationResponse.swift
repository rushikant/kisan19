//
//  AcceptInvitationResponse.swift
//  KISAN.Net
//
//  Created by MacMini on 11/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class AcceptInvitationResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var invitationsInfo: [InvitationInfo]
    
    
    init( success: Bool?,
          message: String?,
          responseCode: Int?,
          invitationsInfo: [InvitationInfo],
          successRecord: Bool?){
        self.success = success
        self.message =  message
        self.responseCode = responseCode
        self.invitationsInfo = invitationsInfo
        
    }
    
    
    
    
    class func initializeAcceptInvitationResponse() -> AcceptInvitationResponse{
        let response = AcceptInvitationResponse(success: false, message: "",responseCode: -1,invitationsInfo:[],successRecord : false)
        return response
    }
    
    
    class func getAcceptInvitationResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> AcceptInvitationResponse {
        let response = initializeAcceptInvitationResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.invitationsInfo = [InvitationInfo.getInvitationListDetailsFromDictionary((dictionary["data"] as? [String:AnyObject])!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as AcceptInvitationResponse
    }
}
