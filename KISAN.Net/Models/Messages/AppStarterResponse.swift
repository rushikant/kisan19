//
//  AppStarterResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 12/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class AppStarterResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var data: [AppStarterData]

    init(success: Bool?,message: String?,responseCode: Int?,timeInMillis: Int?,data: [AppStarterData]) {
       
      self.success = success
      self.message = message
      self.responseCode = responseCode
      self.timeInMillis = timeInMillis
      self.data = data
    }
    
    class func initializeAppStarterResponse() -> AppStarterResponse {
        let response = AppStarterResponse(success: false, message: "",responseCode: -1,timeInMillis:-1,data:[])
        return response
    }
    
    class func getDictionaryFromAppStarterResponse(_ response: AppStarterResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getIssueInviteDeatilsResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> AppStarterResponse {
        let response = initializeAppStarterResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.data  = [AppStarterData.getAppStarterDataFromDictionary(dictionary["data"] as! [String : AnyObject?])]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int

        return response as AppStarterResponse
    }

}
