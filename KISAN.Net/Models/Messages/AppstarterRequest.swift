//
//  AppstarterRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 12/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class AppstarterRequest{
   
    var app_key :String?
    var sessionId :String?
    var eventCode :String?
    var source : String?
    
    init( app_key : String?,sessionId :String?,eventCode :String?,source :String?){
        
        self.app_key = app_key
        self.sessionId = sessionId
        self.eventCode = eventCode
        self.source = source
    }
    
    class func convertToDictionary(appstarterRequest:AppstarterRequest) -> Dictionary<String, Any> {
        return [
            "app_key": appstarterRequest.app_key!,
            "sessionId": appstarterRequest.sessionId!,
            "eventCode" : appstarterRequest.eventCode!,
            "source" : appstarterRequest.source!,
        ]
    }
    
}
