//
//  BaseResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class BaseResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var timeInMillis: String?
    public var responseCode: Int?
    public var communityDominantColour: String?
    
    init(success: Bool?,
         message: String,
         timeInMillis: String,
         responseCode: Int,
         communityDominantColour: String){
        self.success = success
        self.message = message
        self.timeInMillis = timeInMillis
        self.responseCode = responseCode
        self.communityDominantColour = communityDominantColour
    }
    
    class func initializeBaseResponse() -> BaseResponse {
        let response = BaseResponse(success: false,
                              message: "",
                              timeInMillis: "",
                              responseCode: -1,
                              communityDominantColour: "")
        return response
    }
    
    class func getDictionaryFromModel(_ response: BaseResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getModelFromDictionary(_ dictionary: [String: AnyObject?]) -> BaseResponse {
        
        let response = initializeBaseResponse()
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? String
        response.responseCode = dictionary["response_code"] as? Int
        response.communityDominantColour = dictionary["first_name"] as? String
        return response as BaseResponse
    }
    
}
