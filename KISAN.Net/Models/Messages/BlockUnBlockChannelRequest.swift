//
//  BlockUnBlockChannelRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class BlockUnBlockChannelRequest {
    public var operation : String?
    public var communityJabberId : String?
    public var newAdminUserId : String?
    
    init(operation: String, communityJabberId: String, newAdminUserId: String) {
        self.operation = operation
        self.communityJabberId = communityJabberId
        self.newAdminUserId = newAdminUserId
    }
    
    class func convertToDictionary(blockUnBlockChannelRequest:BlockUnBlockChannelRequest) -> Dictionary<String, Any> {
        return [
            "operation": blockUnBlockChannelRequest.operation!,
            "communityJabberId": blockUnBlockChannelRequest.communityJabberId!,
            "newAdminUserId": blockUnBlockChannelRequest.newAdminUserId!
        ]
    }
}
