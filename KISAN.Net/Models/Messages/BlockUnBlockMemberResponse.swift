//
//  BlockUnBlockMemberResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class BlockUnBlockMemberResponse: NSObject {
    
    public var httpStatusCode : Int?
    public var message : String?
    public var requestId : String?
    public var responseCode: Int?
    public var success : Int?
    public var timeInMillis : Int?
    
    
    init(httpStatusCode: Int?,message: String?,
         requestId: String?,
         responseCode: Int?,success:Int?,timeInMillis:Int?){
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    
    class func initializeBlockUnBlockMemberResponse() -> BlockUnBlockMemberResponse {
        let response = BlockUnBlockMemberResponse(httpStatusCode:-1,message:"", requestId: "",responseCode: -1,success: -1,timeInMillis: -1)
        return response
    }
    
    class func getDictionaryFromBlockUnBlockMemberResponse(_ response: BlockUnBlockMemberResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getBlockUnBlockMemberResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> BlockUnBlockMemberResponse {
        let response = initializeBlockUnBlockMemberResponse()
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.message = dictionary["message"] as? String
        response.requestId = dictionary["requestId"] as? String
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Int
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as BlockUnBlockMemberResponse
    }
    
    
}
