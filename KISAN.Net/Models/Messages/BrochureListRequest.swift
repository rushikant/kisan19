//
//  BrochureListRequest.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

/*
{
  *app_key: APP_KEY,
  *eventCode: EVENTCODE,
  *session_id: SESSION_ID,
  *pagesize:PAGESIZE,
  *currentpage : CURRENT_PAGE,
  search: SEARCH
}
*/

class BrochureListRequest: NSObject {
    
    var   app_key : String?
    var   eventCode: String?
    var   sessionId: String?
    var   pagesize: Int?
    var   currentpage: Int?
    var   search: String?
    
    
    init (app_key : String?,eventCode : String?,sessionId: String?,pagesize: Int?,currentpage:Int?, search: String?){
        self.app_key = app_key
        self.eventCode = eventCode
        self.sessionId = sessionId
        self.pagesize = pagesize
        self.currentpage = currentpage
        self.search = search
    }
    
    class func convertToDictionary(request:BrochureListRequest) -> Dictionary<String, Any> {
        return [
            "app_key": request.app_key!,
            "eventCode": request.eventCode!,
            "sessionId": request.sessionId!,
            "pagesize": request.pagesize!,
            "currentpage" :request.currentpage!,
            "search" :request.search!
        ]
    }
}

