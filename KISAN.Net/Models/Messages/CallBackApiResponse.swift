//
//  CallBackApiResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 05/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class CallBackApiResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var callbackRequestId: String?
   
    
    init(success: Bool?,message: String?,responseCode: Int?,timeInMillis: Int?,callbackRequestId: String?) {
        
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
        self.callbackRequestId = callbackRequestId
    }
    
    class func initializeCallBackApiResponse() -> CallBackApiResponse {
        let response = CallBackApiResponse(success: false, message: "",responseCode: -1,timeInMillis:-1,callbackRequestId:"")
        return response
    }
    
    class func getDictionaryFromCallBackApiResponse(_ response: CallBackApiResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getCallBackApiResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> CallBackApiResponse {
        let response = initializeCallBackApiResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.callbackRequestId  = dictionary["callbackRequestId"] as? String
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        
        return response as CallBackApiResponse
    }
}


