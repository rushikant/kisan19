//
//  ChannelProfileDetails.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 16/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ChannelProfileDetailsResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var httpStatusCode: Int?
    public var requestId: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var communityDetails: [CommunityDetails] = []
    
    init(communityDetails: [CommunityDetails],success: Bool?,
         message: String,
         httpStatusCode: Int,requestId:String,responseCode:Int,timeInMillis:Int){
        self.success = success
        self.communityDetails = communityDetails
        self.message = message
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    class func initializeChannelProfileDetailsResponse() -> ChannelProfileDetailsResponse {
        let response = ChannelProfileDetailsResponse(communityDetails: [],success: false, message: "",httpStatusCode: -1,requestId:"",responseCode:-1,timeInMillis:-1)
        return response
    }
    
    class func getDictionaryFromChannelProfileDetailsResponse(_ response: ChannelProfileDetailsResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getChannelProfileDetailsResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ChannelProfileDetailsResponse {
        let response = initializeChannelProfileDetailsResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let communityDetails = dictionary["communityDetails"] as? [String: AnyObject?]
        print("communityDetails",communityDetails)
//        if let paymentsTemp = dictionary["communityDetails"] as? [AnyObject]{
//            print("paymentsTemp",paymentsTemp)
//
//        }
       // print("communityDetails",communityDetails as! [AnyObject])
        //print("communityDetails",communityDetails as [Any])
       // response.communityDetails = CommunityDetails.getModelFromArray(communityDetails as! [AnyObject])
       // response.communityDetails = CommunityDetails.getModelFromArray((dictionary["communityDetails"] as! [AnyObject]))
        
        var channelProfileRespo: CommunityDetails = CommunityDetails.initializeCommunityDetails()
        channelProfileRespo = CommunityDetails.getCommunityDetailsFromDictionary(dictionary["communityDetails"] as! [String : AnyObject?])
        
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as ChannelProfileDetailsResponse
    }

}
