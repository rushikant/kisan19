//
//  CommonChannelListResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 08/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommonChannelListResponse: NSObject {

    public var success: Bool?
    public var message: String?
    public var httpStatusCode: Int?
    public var requestId: Int?
    public var responseCode: Int?
    public var commonCommunityList: [CommonChannelList] = []
    public var timeInMillis: Int?
    
    init(commonCommunityList: [CommonChannelList],success: Bool?,
         message: String,
         responseCode: Int,httpStatusCode:Int,requestId:Int){
        self.success = success
        self.commonCommunityList = commonCommunityList
        self.message = message
        self.responseCode = responseCode
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
    }
    
    
    class func initializeCommonChannelListResponse() -> CommonChannelListResponse {
        let response = CommonChannelListResponse(commonCommunityList: [],success: false, message: "",responseCode: -1,httpStatusCode: -1,requestId: -1)
        return response
    }
    
    class func getDictionaryFromCommonChannelListResponse(_ response: CommonChannelListResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getCommonChannelListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> CommonChannelListResponse {
        let response = initializeCommonChannelListResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.commonCommunityList = CommonChannelList.getModelFromArray(dictionary["communities"] as! [AnyObject])
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? Int
        return response as CommonChannelListResponse
    }
}
