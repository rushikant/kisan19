//
//  CommunityMembersListResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 29/05/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CommunityMembersListResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var httpStatusCode: Int?
    public var requestId: Int?
    public var responseCode: Int?
    public var communityMembersList: [CommunityMembersList] = []
    public var timeInMillis: Int?
    
    init(communityMembersList: [CommunityMembersList],success: Bool?,
         message: String,
         responseCode: Int,httpStatusCode:Int,requestId:Int){
        self.success = success
        self.communityMembersList = communityMembersList
        self.message = message
        self.responseCode = responseCode
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
    }
    
    class func initializeCommunityMembersListResponse() -> CommunityMembersListResponse {
        let response = CommunityMembersListResponse(communityMembersList: [],success: false, message: "",responseCode: -1,httpStatusCode: -1,requestId: -1)
        return response
    }
    
    class func getDictionaryFromCommunityMembersListResponse(_ response: CommunityMembersListResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getCommunityMembersListResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> CommunityMembersListResponse {
        let response = initializeCommunityMembersListResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        //response.communityMembersList = CommunityMembersList.getModelFromArray(dictionary["communityMembersList"] as! [AnyObject])
        let communityMembersList = dictionary["communityMembersList"] as! [AnyObject]
        for  j in (0..<communityMembersList.count){
            var membersList = communityMembersList[j] as! Dictionary<String, Any>
            let userId = membersList["userId"] as? String
            //let communityDetailsCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: userId!)
            let membersDetailsOnUserId = MembersServices.sharedInstance.checkMembersuserId(userId: userId!)
            membersList.updateValue(postData["communityKey"] ?? String(), forKey: "communityKey")
            membersList.updateValue(postData["isBlocked"] ?? String(), forKey: "isBlocked")
            print("membersList",membersList)
            if(membersDetailsOnUserId.count == 0){
                MembersServices.sharedInstance.sendMembersAtMembersTable(membersDetails: membersList)
            }else if (membersDetailsOnUserId.count == 1){
                MembersServices.sharedInstance.updateAtMembersTable(membersDetails: membersList)
            }else{
                print("Not allowed")
            }
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? Int
        return response as CommunityMembersListResponse
    }

}
