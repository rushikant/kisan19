//
//  ConvertToDicCommunityCommunityObject.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 14/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ConvertToDicCommunityCommunityObject{
    
    class func ConvertToDicCommunityCommunityObject(communityDetail:[CommonChannelList]) -> Dictionary<String, Any> {
        var allDictionaries = [[String : AnyObject]]()
        for data in communityDetail {
            let  dictionary = [
                "communityCategories" : data.communityCategories,
                "communityCreatedTimestamp" : data.communityCreatedTimestamp,
                "communityDesc" : data.communityDesc ,
                "communityImageBigThumbUrl" : data.communityImageBigThumbUrl,
                "communityImageSmallThumbUrl" : data.communityImageSmallThumbUrl ,
                "communityImageUrl" : data.communityImageUrl,
                "communityKey" : data.communityKey ,
                "communityName" : data.communityName  ,
                "communityScore" : data.communityScore ,
                "isMember" : data.isMember ,
                "ownerId" : data.ownerId ,
                "ownerImageUrl" : data.ownerImageUrl ,
                "ownerName" : data.ownerName ,
                "privacy" : data.privacy ,
                "totalMembers" : String(describing: data.totalMembers),
                "type" : data.type ,
                "communityJabberId" : data.communityJabberId ,
                "groupRole" : data.groupRole ,
                "ownerFirstName" : data.ownerFirstName ,
                "ownerLastName" : data.ownerLastName ,
                "communityDominantColour" : data.communityDominantColour,
                ] as [String : Any]
            allDictionaries.append(dictionary as [String : AnyObject])
            
        }
        return allDictionaries[0]
    }
    
}
