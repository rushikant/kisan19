//
//  ConvertToDicCommunityDetailsObject.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 10/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ConvertToDicCommunityDetailsObject{
    
   class func convertToDictionary(communityDetail:[CommunityDetailTable]) -> Dictionary<String, Any> {
        var allDictionaries = [[String : AnyObject]]()
        
        for data in communityDetail {
            let  dictionary = [
                "communityCategories" : data.communityCategories ,
                "communityCreatedTimestamp" : data.communityCreatedTimestamp,
                "communityDesc" : data.communityDesc,
                "communityDominantColour" : data.communityDominantColour,
                "communityImageBigThumbUrl" : data.communityImageBigThumbUrl,
                "communityImageSmallThumbUrl" : data.communityImageSmallThumbUrl,
                "communityImageUrl" : data.communityImageUrl,
                "communityJoinedTimestamp" : data.communityJoinedTimestamp,
                "communityKey" : data.communityKey,
                "communityName" : data.communityName ,
                "communityScore" : data.communityScore,
                "communityStatus" : data.communityStatus,
                "communitySubType" : data.communitySubType,
                "communityUpdatedTimestamp" : data.communityUpdatedTimestamp,
                "finalScore" : data.finalScore,
                "isDeleted" : data.isdeleted,
                "isMember" : data.isMember!,
                "ownerId" : data.ownerId,
                "ownerImageUrl" : data.ownerImageUrl,
                "ownerName" : data.ownerName,
                "privacy" : data.privacy,
                "memberCount" : data.totalMembers,
                "type" : data.type, //?? String()
                "messageText" : data.messageText,
                "messageAt" : "",
                "communityJabberId" : data.communityJabberId,
                "communityWithRssFeed" : data.communityWithRssFeed,
                "totalMembers" : data.totalMembers,
                "pavilion_name" : data.pavilion_name,
                "event_code" : data.event_code,
                "address" : data.address,
                "contact_person_first_name" : data.contact_person_first_name,
                "contact_person_last_name" : data.contact_person_last_name,
                "contact_person_designation" : data.contact_person_designation,
                "company_website" : data.company_website,
                "alloted_stall_number" : data.alloted_stall_number
                
                ] as [String : Any]
            allDictionaries.append(dictionary as [String : AnyObject])
            
        }
         return allDictionaries[0]
    }
    
}
