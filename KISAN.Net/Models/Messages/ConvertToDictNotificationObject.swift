//
//  ConvertToDictNotificationObject.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ConvertToDictNotificationObject{
    
    class func ConvertToDictNotificationObject(notificationDetails:[NotificationTable]) -> Dictionary<String, Any> {
        var allDictionaries = [[String : AnyObject]]()
        for data in notificationDetails {
            let  dictionary = [
                "notificationKey" : data.notificationKey,
                "communityImageUrl" : data.communityImageUrl,
                "communityJabberId" : data.communityJabberId,
                "communityKey" : data.communityKey,
                "action" : data.action,
                "communityName" : data.communityName,
                "sendAt" : data.sendAt,
                "sso_id" : data.sso_id,
                "communityImageSmallThumbUrl" : data.communityImageUrl,
                "communityImageBigThumbUrl" : data.communityImageUrl,
                "totalMembers" : data.totalMembers,
                "communityDesc" : data.communityDesc,
                "communityCategories" : data.communityCategories,
                "communityWithRssFeed" : data.communityWithRssFeed,
                ] as [String : Any]
            allDictionaries.append(dictionary as [String : AnyObject])
        }
        return allDictionaries[0]
    }

}
