//
//  ConvertToDictOfChannelProfileObj.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 19/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation


class ConvertToDictOfChannelProfileObj{

    class func convertToDictOfChannelProfileObj(myChatDetails:[CommunityDetails]) -> Dictionary<String, Any> {
        var allDictionaries = [[String : AnyObject]]()
        for data in myChatDetails {
            let  dictionary = [
                "communityCategories" : data.communityCategories,
                "communityCreatedTimestamp" : data.communityCreatedTimestamp,
                "communityDominantColour" : data.communityDominantColour,
                "communityImageBigThumbUrl" : data.communityImageBigThumbUrl,
                "communityImageUrl" : data.communityImageUrl,
                "communityJabberId" : data.communityJabberId,
                "communityKey" : data.communityKey,
                "communityName" : data.communityName,
                "communityScore" : data.communityScore,
                "communityWithRssFeed" : data.communityWithRssFeed,
                "defaultValue" : data.defaultValue,
                "descriptionValue" : data.descriptionValue,
                "loggedUserJoinedDateTime" : data.loggedUserJoinedDateTime,
                "ownerId" : data.ownerId,
                "privacy" : data.privacy,
                "address" : data.address,
                "alloted_stall_number" : data.alloted_stall_number,
                "communityDesc" : data.communityDesc,
                "company_website" : data.company_website,
                "contact_person_designation" : data.contact_person_designation,
                "contact_person_first_name" : data.contact_person_first_name,
                "contact_person_last_name" : data.contact_person_last_name,
                "pavilion_name" : data.pavilion_name,
                "stall_id" : data.stall_id,
                "memberCount" : data.totalMembers,
                "isMember" : data.isMember,
                "communityImageSmallThumbUrl" : data.communityImageSmallThumbUrl,
                "event_code" : data.event_code,
                "ownerName" : data.ownerName,
                "type" : data.type,
                "logged_user_blocked_timestamp" : data.logged_user_blocked_timestamp

                ] as [String : Any]
            allDictionaries.append(dictionary as [String : AnyObject])
            
        }
        return allDictionaries[0]
    }
    
}
