//
//  ConvertToDictOfMyChatObj.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ConvertToDictOfMyChatObj{
    
    class func ConvertToDictOfMyChatObj(myChatDetails:[MyChatsTable]) -> Dictionary<String, Any> {
        var allDictionaries = [[String : AnyObject]]()
        for data in myChatDetails {
            let  dictionary = [
                "communityCategories" : data.communityCategories,
                "communityCreatedTimestamp" : data.communityCreatedTimestamp,
                "communityDesc" : data.communityDesc,
                "communityDominantColour" : data.communityDominantColour,
                "communityImageBigThumbUrl" : data.communityImageBigThumbUrl,
                "communityImageSmallThumbUrl" : data.communityImageSmallThumbUrl,
                "communityImageUrl" : data.communityImageUrl,
                "communityJoinedTimestamp" : data.communityJoinedTimestamp,
                "communityKey" : data.communityKey,
                "communityName" : data.communityName,
                "communityScore" : data.communityScore,
                "communityStatus" : data.communityStatus,
                "communitySubType" : data.communitySubType,
                "communityUpdatedTimestamp" : data.communityUpdatedTimestamp,
                "finalScore" : data.finalScore,
                "isdeleted" : data.isdeleted,
                "isMember" : data.isMember,
                "ownerId" : data.ownerId,
                "ownerImageUrl" : data.ownerImageUrl,
                "ownerName" : data.ownerName,
                "privacy" : data.privacy,
                "totalMembers" : data.totalMembers,
                "type" : data.type,
                "messageText" : data.messageText,
                "messageAt" : data.messageAt,
                "communityJabberId" : data.communityJabberId,
                "bigThumb" : data.bigThumb,
                "createddatetime" : data.createddatetime,
                "firstname" : data.firstname,
                "imageUrl" : data.imageUrl,
                "lastMessageDatetime" : data.lastMessageDatetime,
                "lastName" : data.lastName,
                "memberCount" : data.memberCount,
                "profileStatus" : data.profileStatus,
                "smallThumb" : data.smallThumb,
                "unreadMessagesCount" : data.unreadMessagesCount,
                "messageBy" : data.messageBy,
                "featured" : data.featured,
                "userId" : data.userId,
                "notificationKey" : data.notificationKey,
                "sendTo" : data.sendTo,
                "action" : data.action,
                "sendAt" : data.sendAt,
                "sso_id" : data.sso_id,
                "feed" :  data.feed,
                "userCommunityJabberId" : data.userCommunityJabberId,
                ] as [String : Any]
            allDictionaries.append(dictionary as [String : AnyObject])
            
        }
        return allDictionaries[0]
    }
    
}









