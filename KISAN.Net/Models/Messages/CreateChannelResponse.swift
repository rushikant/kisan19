//
//  CreateChannelResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class CreateChannelResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var communityDetailsList: [CommunityDetailsList] = []
    public var timeInMillis: Int?
    public var httpStatusCode: Int?
    public var communityKey: String?
    
    init(communityDetailsList: [CommunityDetailsList],success: Bool?,
         message: String,
         response_code: Int,timeInMillis:Int,httpStatusCode:Int,communityKey:String){
        self.success = success
        self.communityDetailsList = communityDetailsList
        self.message = message
        self.response_code = response_code
        self.timeInMillis = timeInMillis
        self.httpStatusCode = httpStatusCode
        self.communityKey = communityKey
    }
    
    
    class func initializeCreateChannelResponse() -> CreateChannelResponse {
        let response = CreateChannelResponse(communityDetailsList: [],success: false, message: "",response_code: -1,timeInMillis:-1,httpStatusCode:-1,communityKey: "")
        return response
    }
    
    class func getDictionaryFromCreateChannelResponse(_ response: CreateChannelResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getCreateChannelResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> CreateChannelResponse {
        let response = initializeCreateChannelResponse()
        response.response_code = dictionary["response_code"] as? Int
        // response.communityDetailsList = CommunityDetailsList.getModelFromArray(dictionary["communityDetails"] as! [AnyObject])
        ////CommunityDetailService.sharedInstance.sendMessageAtCommunityDetailTable(dictionary["communityDetails"] as! [AnyObject])
        let communityDetails = postData["communityDetails"]
        var updatedCommunityDetails = communityDetails as! Dictionary<String, Any>
        updatedCommunityDetails.updateValue(StringConstants.DashboardViewController.channelCreation, forKey: "messageText")
        let currentTime = DateTimeHelper.getCurrentMillis()
        updatedCommunityDetails.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
        updatedCommunityDetails.updateValue(dictionary["communityKey"] as! String, forKey: "communityKey")
        let userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        updatedCommunityDetails.updateValue(userId, forKey: "ownerId")
        updatedCommunityDetails.updateValue(StringConstants.ONE, forKey: "isMember")
        updatedCommunityDetails.updateValue(StringConstants.ZERO, forKey: "feed")
        updatedCommunityDetails.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
        updatedCommunityDetails.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
        
        MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: updatedCommunityDetails, isFirstTime: false,iSCommingFromOneTOneinitiate:false )
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.communityKey = dictionary["communityKey"] as? String
        return response as CreateChannelResponse
    }
    
}
