//
//  CreateCommunityRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class CreateCommunityDetailsRequest {
    
    public var communityCategories : String!
    public var communityDesc : String!
    public var communityDominantColour : String!
    public var communityImageBigThumbUrl : String!
    public var communityImageSmallThumbUrl:String!
    public var communityImageUrl:String!
    public var communityName:String!
    public var communityScore:Int!
    public var ownerName:String!
    public var privacy:String!
    public var totalMembers:Int!
    public var type: String!
    public var communityJabberId: String!
    public var communityJoinedTimestamp: String!
    public var isCommunityWithRssFeed: Bool!
    public var isDefault: Bool!
    public var ownerId: String!
    public var ownerImageUrl: String!

    init(communityCategories: String, communityDesc: String, communityDominantColour: String,communityImageBigThumbUrl:String,communityImageSmallThumbUrl:String,communityImageUrl:String,communityName:String,communityScore:Int,ownerName:String,privacy:String,totalMembers:Int,type:String,communityJabberId:String,communityJoinedTimestamp:String,isCommunityWithRssFeed:Bool,isDefault:Bool,ownerId:String,ownerImageUrl:String) {
        self.communityCategories = communityCategories
        self.communityDesc = communityDesc
        self.communityDominantColour = communityDominantColour
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.communityImageUrl = communityImageUrl
        self.communityName = communityName
        self.communityScore = communityScore
        self.ownerName = ownerName
        self.privacy = privacy
        self.totalMembers = totalMembers
        self.type = type
        self.communityJabberId = communityJabberId
        self.communityJoinedTimestamp = communityJoinedTimestamp
        self.isCommunityWithRssFeed = isCommunityWithRssFeed
        self.isDefault = isDefault
        self.ownerId = ownerId
        self.ownerImageUrl = ownerImageUrl
    }
    
    class func convertToDictionary(createCommunityDetails:CreateCommunityDetailsRequest) -> Dictionary<String, Any> {
        return [
            "communityCategories": createCommunityDetails.communityCategories!,
            "communityDesc": createCommunityDetails.communityDesc!,
            "communityDominantColour": createCommunityDetails.communityDominantColour!,
            "communityImageBigThumbUrl": createCommunityDetails.communityImageBigThumbUrl,
            "communityImageSmallThumbUrl": createCommunityDetails.communityImageSmallThumbUrl,
            "communityImageUrl": createCommunityDetails.communityImageUrl,
            "communityName" : createCommunityDetails.communityName,
            "communityScore" : createCommunityDetails.communityScore,
            "ownerName" : createCommunityDetails.ownerName,
            "privacy" : createCommunityDetails.privacy,
            "totalMembers" : createCommunityDetails.totalMembers,
            "type" : createCommunityDetails.type,
            "communityJabberId" : createCommunityDetails.communityJabberId,
            "communityJoinedTimestamp" : createCommunityDetails.communityJoinedTimestamp,
            "isCommunityWithRssFeed" : createCommunityDetails.isCommunityWithRssFeed,
            "isDefault" : createCommunityDetails.isDefault,
            "ownerId" : createCommunityDetails.ownerId,
            "ownerImageUrl" : createCommunityDetails.ownerImageUrl
        ]
    }
}
