//
//  CreateCommunityRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class CreateCommunityRequest {
    
    public var communityDetails: Dictionary<String, Any>!
    public var password:String!
    public var resource:String!
   
    init(communityDetails: Dictionary<String, Any>,password: String,resource:String) {
        self.communityDetails = communityDetails
        self.password = password
        self.resource = resource
    }
    
    class func convertToDictionary(createCommunityRequest:CreateCommunityRequest) -> Dictionary<String, Any>{
        return [
            "communityDetails": createCommunityRequest.communityDetails!,
            "password": createCommunityRequest.password!,
            "resource": createCommunityRequest.resource
        ]
    }
}

