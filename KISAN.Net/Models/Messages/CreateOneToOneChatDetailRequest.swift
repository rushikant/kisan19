//
//  CreateOneToOneChatDetailRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class CreateOneToOneChatDetailRequest {
    
    public var userDetails: Dictionary<String, Any>!
    public var communityKey:String!
    public var oneToOneKey:String!
    public var memberId:String!
    
    init(userDetails: Dictionary<String, Any>,communityKey: String,oneToOneKey:String,memberId:String) {
        self.userDetails = userDetails
        self.communityKey = communityKey
        self.oneToOneKey = oneToOneKey
        self.memberId = memberId
    }
    
    class func convertToDictionary(CreateOneToOneChatDetailRequest:CreateOneToOneChatDetailRequest) -> Dictionary<String, Any>{
        return [
            "userDetails": CreateOneToOneChatDetailRequest.userDetails!,
            "communityKey": CreateOneToOneChatDetailRequest.communityKey!,
            "oneToOneKey": CreateOneToOneChatDetailRequest.oneToOneKey!,
            "memberId": CreateOneToOneChatDetailRequest.memberId!
        ]
    }
}
