//
//  CreateOneToOneChatRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 10/04/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation


class CreateOneToOneChatRequest {
    
   public var username : String!
   public var accountType: Int!
   public var city : String!
   public var country : String!
   public var imageBigthumbUrl : String!
   public var imageSmallthumbUrl : String!
   public var imageUrl : String!
    
    init( username: String, accountType: Int,city:String,country:String,imageBigthumbUrl:String,imageSmallthumbUrl:String,imageUrl:String) {
        self.username = username
        self.accountType = accountType
        self.city = city
        self.country = country
        self.imageBigthumbUrl = imageBigthumbUrl
        self.imageSmallthumbUrl = imageSmallthumbUrl
        self.imageUrl = imageUrl
    }
    
    class func convertToDictionary(createOneToOneChatRequest:CreateOneToOneChatRequest) -> Dictionary<String, Any>{
        return [
            "username": createOneToOneChatRequest.username!,
            "accountType": createOneToOneChatRequest.accountType!,
            "city": createOneToOneChatRequest.city!,
            "country": createOneToOneChatRequest.country,
            "imageBigthumbUrl": createOneToOneChatRequest.imageBigthumbUrl,
            "imageSmallthumbUrl": createOneToOneChatRequest.imageSmallthumbUrl,
            "imageUrl" : createOneToOneChatRequest.imageUrl
        ]
    }
}
