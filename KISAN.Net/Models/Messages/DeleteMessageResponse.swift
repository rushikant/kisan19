//
//  DeleteMessageResponse.swift
//  KISAN.Net
//
//  Created by Easebuzz Pvt Ltd on 15/09/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class DeleteMessageResponse: NSObject {
    
    public var httpStatusCode : Int?
    public var message : String?
    public var requestId : String?
    public var responseCode: Int?
    public var success : Int?
    public var timeInMillis : Int?
    
    
    init(httpStatusCode: Int?,message: String?,
         requestId: String?,
         responseCode: Int?,success:Int?,timeInMillis:Int?){
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    
    class func initializeDeleteMessageResponse() -> DeleteMessageResponse {
        let response = DeleteMessageResponse(httpStatusCode:-1,message:"", requestId: "",responseCode: -1,success: -1,timeInMillis: -1)
        return response
    }
    
    class func getDictionaryFromDeleteMessageResponse(_ response: DeleteMessageResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getDeleteMessageResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> DeleteMessageResponse {
        let response = initializeDeleteMessageResponse()
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.message = dictionary["message"] as? String
        response.requestId = dictionary["requestId"] as? String
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Int
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as DeleteMessageResponse
    }
}
