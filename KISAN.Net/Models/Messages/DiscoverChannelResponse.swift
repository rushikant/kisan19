//
//  DiscoverChannelResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class DiscoverChannelResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var communityDetailsList: [CommunityDetailsList] = []

    init(communityDetailsList: [CommunityDetailsList],success: Bool?,
         message: String,
         response_code: Int){
        self.success = success
        self.communityDetailsList = communityDetailsList
        self.message = message
        self.response_code = response_code
    }
    
    
    class func initializeDiscoverChannelResponse() -> DiscoverChannelResponse {
        let response = DiscoverChannelResponse(communityDetailsList: [],success: false, message: "",response_code: -1)
        return response
    }
    
    class func getDictionaryFromDiscoverChannelResponse(_ response: DiscoverChannelResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    //Save data in local data base with pagination
    class func getDiscoverChannelResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> DiscoverChannelResponse {
        let response = initializeDiscoverChannelResponse()
        response.response_code = dictionary["response_code"] as? Int
        //response.communityDetailsList = CommunityDetailsList.getModelFromArray(dictionary["communityDetailsArrayList"] as! [AnyObject])
        ////CommunityDetailService.sharedInstance.sendMessageAtCommunityDetailTable(dictionary["communityDetailsArrayList"] as! [AnyObject])
        var communityDetails = dictionary["communityDetailsList"] as! [AnyObject]
        //let sortedArray = dicArray.sorted {$0["last"]! < $1["last"]!}
        communityDetails = (communityDetails as NSArray).sortedArray(using: [NSSortDescriptor(key: "community_suggestion_score_for_user", ascending: false)]) as! [[String:AnyObject]] as [AnyObject]
        //print(communityDetails)
        for  j in (0..<communityDetails.count){
            let communityList = communityDetails[j]
            let communityKey = communityList["communityKey"] as? String
            let communityDetailsCommunityKey = CommunityDetailService.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey!)
            if(communityDetailsCommunityKey.count == 0){
                CommunityDetailService.sharedInstance.sendMessageAtCommunityDetailTable(communityDetail: communityList as! Dictionary<String, Any>)
            }else if (communityDetailsCommunityKey.count == 1){
                CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: communityList as! Dictionary<String, Any>)
            }else{
                print("Not allowed")
            }
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as DiscoverChannelResponse
    }
    // Searching discover list from server(Not save data in local database)
    class func getDiscoverChannelResponseFromSearching(_ dictionary: [String: AnyObject?]) -> [CommunityDetailTable] {
        var communityDetailData = [CommunityDetailTable]()
        if dictionary["success"] as? Int == 1 {
            var communityDetails = dictionary["communityDetailsList"] as! [AnyObject]
            for  j in (0..<communityDetails.count){
                let communityList = communityDetails[j]
                let communityData =  CommunityDetailService.sharedInstance.convertTemCommunityDeatilsObj(communityDetail: communityList as! Dictionary<String, Any>)
                communityDetailData.append(communityData)
            }
        }
        return communityDetailData
    }
    
}
