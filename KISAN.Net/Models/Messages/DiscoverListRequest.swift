//
//  DiscoverListRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class DiscoverListRequest {
    public var userName : String!
    public var password : String!
    public var token : String!
    public var resource : String!
    public var appKey:String!
    public var appSecret:String!
    
    init(userName: String, password: String, token: String,resource:String,appKey:String,appSecret:String) {
        self.userName = userName
        self.password = password
        self.token = token
        self.resource = resource
        self.appKey = appKey
        self.appSecret = appSecret
    }
    
    class func convertToDictionary(discoverListDetails:DiscoverListRequest) -> Dictionary<String, Any> {
        return [
            "userName": discoverListDetails.userName!,
            "password": discoverListDetails.password!,
            "token": discoverListDetails.token!,
            "resource": discoverListDetails.resource,
            "appKey": discoverListDetails.appKey,
            "appSecret": discoverListDetails.appSecret
        ]
    }
}
