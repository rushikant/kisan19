//
//  EditChannelProfileDetailRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class EditChannelProfileDetailRequest {
    
    public var communityDetails: Dictionary<String, Any>!
    
    init(communityDetails: Dictionary<String, Any>) {
        self.communityDetails = communityDetails
    }
    
    class func convertToDictionary(editChannelProfileDetailRequest:EditChannelProfileDetailRequest) -> Dictionary<String, Any>{
        return [
            "communityDetails": editChannelProfileDetailRequest.communityDetails!,
        ]
    }
}
