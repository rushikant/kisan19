//
//  EditChannelProfileRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class EditChannelProfileRequest {
    
     public var communityCategories:String!
    // public var communityCreatedTimestamp:String!
     public var communityDesc:String!
     public var communityDominantColour:String!
     public var communityName:String!
     public var communityImageSmallThumbUrl:String!
     public var communityImageUrl:String!
     public var communityJabberId:String!
    // public var communityJoinedTimestamp:String!
     public var communityKey:String!
    // public var communityScore:Int!
     public var isCommunityWithRssFeed:Bool!
     public var isDefault:Bool!
    // public var isMember:String!
     public var ownerId:String!
    // public var ownerImageUrl:String!
     public var ownerName:String!
     public var privacy:String!
    // public var totalMembers:Int!
     public var type:String!
     public var communityImageBigThumbUrl:String!

    init(communityCategories: String,communityDesc:String,communityDominantColour:String,communityName:String,communityImageSmallThumbUrl:String,communityImageUrl:String,communityJabberId:String,communityKey:String,isCommunityWithRssFeed:Bool,isDefault:Bool,ownerId:String,ownerName:String,privacy:String,type:String,communityImageBigThumbUrl:String) {
        self.communityCategories = communityCategories
        self.communityDesc = communityDesc
        self.communityDominantColour = communityDominantColour
        self.communityName = communityName
        self.communityImageSmallThumbUrl = communityImageSmallThumbUrl
        self.communityImageUrl = communityImageUrl
        self.communityJabberId = communityJabberId
        self.communityKey = communityKey
        self.isCommunityWithRssFeed = isCommunityWithRssFeed
        self.isDefault = isDefault
        self.ownerId = ownerId
        self.ownerName = ownerName
        self.privacy = privacy
        self.type = type
        self.communityImageBigThumbUrl = communityImageBigThumbUrl
    }
    
    class func convertToDictionary(editChannelProfileDetails:EditChannelProfileRequest) -> Dictionary<String, Any> {
        return [
            "communityCategories": editChannelProfileDetails.communityCategories!,
            "communityDesc": editChannelProfileDetails.communityDesc,
            "communityDominantColour": editChannelProfileDetails.communityDominantColour,
            "communityName": editChannelProfileDetails.communityName,
            "communityImageSmallThumbUrl": editChannelProfileDetails.communityImageSmallThumbUrl!,
            "communityImageUrl": editChannelProfileDetails.communityImageUrl!,
            "communityJabberId": editChannelProfileDetails.communityJabberId,
            "communityKey": editChannelProfileDetails.communityKey,
            "isCommunityWithRssFeed" : editChannelProfileDetails.isCommunityWithRssFeed,
            "isDefault" : editChannelProfileDetails.isDefault,
            "ownerId" : editChannelProfileDetails.ownerId,
            "ownerName" : editChannelProfileDetails.ownerName,
            "privacy" : editChannelProfileDetails.privacy,
            "type" : editChannelProfileDetails.type,
            "communityImageBigThumbUrl" : editChannelProfileDetails.communityImageBigThumbUrl
        ]
    }
}
