//
//  EditProfileDetailsRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class EditProfileDetailsRequest {
    public var about : String!
    public var interests : NSArray!

    init(about: String, interests: NSArray) {
        self.about = about
        self.interests = interests
    }
    
    class func convertToDictionary(userProfile:EditProfileDetailsRequest) -> Dictionary<String, Any> {
        return [
            "about": userProfile.about!,
            "interests": userProfile.interests!,
        ]
    }
}
