//
//  EditProfileRequestForSetLoc.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class EditProfileRequestForSetLoc {
    public var state : String!
    
    init(state: String) {
        self.state = state
    }
    class func convertToDictionary(userProfile:EditProfileRequestForSetLoc) -> Dictionary<String, Any> {
        return [
            "state": userProfile.state!,
        ]
    }
}
