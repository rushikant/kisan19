//
//  EditUserProfileResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class EditUserProfileResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var user_details: [User]

    init(user_details: [User],success: Bool?,
         message: String,
         response_code: Int){
        self.user_details = user_details
        self.success = success
        self.message = message
        self.response_code = response_code
    }
    
    
    class func initializeEditUserProfileResponse() -> EditUserProfileResponse {
        let response = EditUserProfileResponse(user_details: [],success: false, message: "",response_code: -1)
        return response
    }
    
    class func getDictionaryFromEditUserProfileResponse(_ response: EditUserProfileResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getEditUserProfileResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> EditUserProfileResponse {
        let response = initializeEditUserProfileResponse()
        response.response_code = dictionary["response_code"] as? Int
        let userDetails = dictionary["data"] as? [String: AnyObject?]
        response.user_details  = [User.getUserDetailsFromDictionary(userDetails!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as EditUserProfileResponse
    }


}
