//
//  EditUserProfileRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class EditUserDetailsRequest {
    
    public var firstName : String?
    public var lastName : String?
    public var mobile : String?
    public var accountType : Int!
    public var password : String?
    public var country : String?
    public var state : String?
    public var city : String?
    public var profile_status : String?
    public var image_url : String?
    public var big_thumb_url : String?
    public var small_thumb_url : String?
    public var categories : String?
    public var email : String?
    public var about : String?
    public var imageBlob : String?;
    public var oAuthToken : String?
    public var source : String?

    init(firstName: String, lastName: String,mobile:String,accountType:Int,password:String,country:String,state:String,city:String,profile_status:String,image_url:String,big_thumb_url:String,small_thumb_url:String,categories:String,email:String,about:String,imageBlob:String,oAuthToken:String,source:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.mobile = mobile
        self.accountType = accountType
        self.password = password
        self.country = country
        self.state = state
        self.city = city
        self.profile_status = profile_status
        self.image_url = image_url
        self.big_thumb_url = big_thumb_url
        self.small_thumb_url = small_thumb_url
        self.categories = categories
        self.email = email
        self.about = about
        self.imageBlob = imageBlob
        self.oAuthToken = oAuthToken
        self.source = source
    }
    
    class func convertToDictionary(userDetails:EditUserDetailsRequest) -> Dictionary<String, Any> {
        return [
            "firstName": userDetails.firstName!,
            "lastName": userDetails.lastName!,
            "mobile": userDetails.mobile!,
            "accountType": userDetails.accountType!,
            "password": userDetails.password!,
            "country": userDetails.country!,
            "state": userDetails.state!,
            "city": userDetails.city!,
            "profile_status": userDetails.profile_status!,
            "image_url": userDetails.image_url!,
            "big_thumb_url": userDetails.big_thumb_url!,
            "small_thumb_url": userDetails.small_thumb_url!,
            "categories": userDetails.categories!,
            "email": userDetails.email!,
            "about": userDetails.about!,
            "imageBlob": userDetails.imageBlob!,
            "oAuthToken": userDetails.oAuthToken!,
            "source": userDetails.source!
        ]
    }
}


