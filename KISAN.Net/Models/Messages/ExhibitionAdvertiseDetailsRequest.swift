//
//  ExhibitionAdvertiseDetailsRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 20/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ExhibitionAdvertiseDetailsRequest {
    
    public var app_key : String?
    public var eventCode : String?
    public var source : String?
    public var discount_name : String?
    public var referrer_code : String?
    
    init(app_key: String, eventCode: String, source: String, discount_name:String, referrer_code:String) {
        self.app_key = app_key
        self.eventCode = eventCode
        self.source = source
        self.discount_name = discount_name
        self.referrer_code = referrer_code
    }
    
    class func convertToDictionary(exhibitorAdvertiseDetails:ExhibitionAdvertiseDetailsRequest) -> Dictionary<String, Any> {
        return [
            "app_key": exhibitorAdvertiseDetails.app_key!,
            "eventCode": exhibitorAdvertiseDetails.eventCode!,
            "source": exhibitorAdvertiseDetails.source!,
            "discount_name" : exhibitorAdvertiseDetails.discount_name!,
            "referrer_code" : exhibitorAdvertiseDetails.referrer_code!
        ]
    }
    
  
}
