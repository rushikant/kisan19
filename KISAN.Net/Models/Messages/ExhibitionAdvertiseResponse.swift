//
//  ExhibitionAdvertiseResponse.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 23/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhibitionAdvertiseResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var exhibitiondetails: [String: AnyObject?]
    
    init(exhibitiondetails: [String: AnyObject?],success: Bool?,
         message: String,
         responseCode: Int,timeInMillis:Int){
        self.exhibitiondetails = exhibitiondetails
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    
    class func initializeExhibitionAdvertiseResponse() -> ExhibitionAdvertiseResponse {
        let response = ExhibitionAdvertiseResponse(exhibitiondetails: ["": "" as AnyObject],success: false, message: "",responseCode: -1,timeInMillis:-1)
        return response
    }
    
    class func getDictionaryFromExhibitionAdvertiseResponse(_ response: ExhibitionAdvertiseResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getExhibitionAdvertiseResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhibitionAdvertiseResponse {
        let response = initializeExhibitionAdvertiseResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let eventDetails = dictionary["eventDetails"] as? [String: AnyObject?]
        response.exhibitiondetails = (eventDetails?["details"] as? [String: AnyObject?])!
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as ExhibitionAdvertiseResponse
    }
    
}
