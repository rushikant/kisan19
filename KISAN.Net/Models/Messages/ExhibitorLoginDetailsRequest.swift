//
//  ExhibitorLoginDetailsRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class ExhibitorLoginDetailsRequest {
    public var client_id : String?
    public var client_secret : String?
    public var source : String?
    public var account_type : String?
    public var type : String?
    public var username : String?
    public var password : String?
    public var resource : String?
    public var fcmDeviceId : String?
    public var app : String?
    
    init(client_id: String, client_secret: String, source: String, account_type:String, type:String,username: String, password: String, resource: String, fcmDeviceId:String, app:String) {
        self.client_id = client_id
        self.client_secret = client_secret
        self.source = source
        self.account_type = account_type
        self.type = type
        self.username = username
        self.password = password
        self.resource = resource
        self.fcmDeviceId = fcmDeviceId
        self.app = app
    }
    
    class func convertToDictionary(exhibitorLoginDetails:ExhibitorLoginDetailsRequest) -> Dictionary<String, Any> {
        return [
            "client_id": exhibitorLoginDetails.client_id!,
            "client_secret": exhibitorLoginDetails.client_secret!,
            "source": exhibitorLoginDetails.source!,
            "account_type" : exhibitorLoginDetails.account_type!,
            "type" : exhibitorLoginDetails.type!,
            "username" : exhibitorLoginDetails.username!,
            "password" : exhibitorLoginDetails.password!,
            "resource" : exhibitorLoginDetails.resource!,
            "fcmDeviceId" : exhibitorLoginDetails.fcmDeviceId!,
            "app" : exhibitorLoginDetails.app!
        ]
    }
}

