//
//  ExhibitorLoginResponse.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ExhibitorLoginResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var httpStatusCode: Int?
    public var requestId: String?
    public var timeInMillis: Int?
    public var data: [ExhibitorData]
    public var recordsAffected: Int?
    
    init(data: [ExhibitorData],success: Bool?,
         message: String,
         responseCode: Int,httpStatusCode:Int,requestId:String,timeInMillis:Int,recordsAffected:Int){
        self.data = data
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.timeInMillis = timeInMillis
        self.recordsAffected = recordsAffected
    }
    
    
    class func initializeExhibitorLoginResponse() -> ExhibitorLoginResponse {
        let response = ExhibitorLoginResponse(data: [],success: false, message: "",responseCode: -1,httpStatusCode:-1,requestId:"",timeInMillis:-1,recordsAffected:-1)
        return response
    }
    
    class func getDictionaryFromExhibitorLoginResponse(_ response: ExhibitorLoginResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getExhibitorLoginResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ExhibitorLoginResponse {
        let response = initializeExhibitorLoginResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let exhibitorDetails = dictionary["data"] as? [String: AnyObject?]
        response.data  = [ExhibitorData.getUserDetailsFromDictionary(exhibitorDetails!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.recordsAffected = dictionary["recordsAffected"] as? Int
        return response as ExhibitorLoginResponse
    }
    
}
