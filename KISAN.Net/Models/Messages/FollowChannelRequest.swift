//
//  FollowChannelRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 16/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class FollowChannelRequest {
    public var user_role : String!
    public var i_jabber_id : String!
   
    init(user_role: String, i_jabber_id: String) {
        self.user_role = user_role
        self.i_jabber_id = i_jabber_id
    }

    class func convertToDictionary(followChannelDetails:FollowChannelRequest) -> Dictionary<String, Any> {
        return [
            "user_role": followChannelDetails.user_role!,
            "i_jabber_id": followChannelDetails.i_jabber_id!
        ]
    }
}
