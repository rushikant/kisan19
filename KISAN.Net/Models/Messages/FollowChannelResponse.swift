//
//  FollowChannelResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 16/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class FollowChannelResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var timeInMillis: Int?
    public var httpStatusCode: Int?
    
    
    init(success: Bool?,
         message: String,
         response_code: Int,timeInMillis:Int,httpStatusCode:Int){
        self.success = success
        self.message = message
        self.response_code = response_code
        self.timeInMillis = timeInMillis
        self.httpStatusCode = httpStatusCode
    }
    
    
    class func initializeFollowChannelResponse() -> FollowChannelResponse {
        let response = FollowChannelResponse(success: false, message: "",response_code: -1, timeInMillis: -1,httpStatusCode:-1)
        return response
    }
    
    class func getDictionaryFromFollowChannelResponse(_ response: FollowChannelResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getFollowChannelResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> FollowChannelResponse {
        let response = initializeFollowChannelResponse()
        response.response_code = dictionary["response_code"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        return response as FollowChannelResponse
    }
    
    
}
