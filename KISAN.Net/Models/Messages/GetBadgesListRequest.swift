//
//  GetBadgesListRequest.swift
//  KISAN.Net
//
//  Created by Rushikant on 18/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetBadgesListRequest {
  
    var app_key :String?
    var eventCode :String?
    var source : String?
    var pagesize : Int?
    var currentpage : Int?
    var passtype : String?
    var filter :Dictionary<String, Any>


    init( app_key : String?,
          eventCode :String?,
          source :String?,
          pagesize : Int?,
          currentpage :Int?,
          passtype : String?
          ,filter: Dictionary<String, Any>
    ){

        self.app_key = app_key
        self.eventCode = eventCode
        self.source = source
        self.pagesize = pagesize
        self.currentpage = currentpage
        self.passtype  = passtype
        self.filter = filter
    }

    class func convertToDictionary(getBadgesListRequest:GetBadgesListRequest) -> Dictionary<String, Any> {
        return [
            "app_key": getBadgesListRequest.app_key!,
            "eventCode" : getBadgesListRequest.eventCode!,
            "source" : getBadgesListRequest.source!,
            "pagesize": getBadgesListRequest.pagesize!,
            "currentpage": getBadgesListRequest.currentpage!,
            "passType" : getBadgesListRequest.passtype!,
            "filter" : getBadgesListRequest.filter
        ]
    }
    
}

