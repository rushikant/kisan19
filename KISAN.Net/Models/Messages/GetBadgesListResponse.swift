//
//  GetBadgesListResponse.swift
//  KISAN.Net
//
//  Created by Rushikant on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class GetBadgesListResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var cnt: Int?
    public var exhibitorBadges:[ExhibitorBadges] = []
    
    init(success: Bool?,
         message: String?,
         exhibitorBadges:[ExhibitorBadges],
         responseCode: Int?,
         timeInMillis: Int?,
         cnt: Int?) {
        self.success = success
        self.message = message
        self.exhibitorBadges = exhibitorBadges
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
        self.cnt = cnt
    }
    
    class func initializeGetBadgesListResponse() -> GetBadgesListResponse {
        let response = GetBadgesListResponse(success: false, message: "",exhibitorBadges:[],responseCode: -1,timeInMillis:-1,cnt:-1)
        return response
    }
    
    class func getDictionaryFromGetBadgesListResponse(_ response: GetBadgesListResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getBadgesListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> GetBadgesListResponse {
        let response = initializeGetBadgesListResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        let overview  = dictionary["overview"] as! [String:AnyObject]
        let total  = overview["total"] as! [String:AnyObject]
        response.exhibitorBadges = ExhibitorBadges.getModelFromArray(overview["records"] as! [AnyObject])
        response.cnt = total["cnt"] as? Int
        
        return response as GetBadgesListResponse
    }
}
