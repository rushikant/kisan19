//
//  GetBadgesListRequest.swift
//  KISAN.Net
//
//  Created by Rushikant on 18/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetExhibitorDetailRequest {
  
    var app_key :String?
    var eventCode :String?
    var source : String?
    var username : String?
    var sessionId : String?


    init( app_key : String?,
          eventCode :String?,
          source :String?,
          username: String?,
        sessionId: String? = ""
    ){

        self.app_key = app_key
        self.eventCode = eventCode
        self.source = source
        self.username = username
        self.sessionId = sessionId
    }

    class func convertToDictionary(getExhibitorDetailRequest:GetExhibitorDetailRequest) -> Dictionary<String, Any> {
        return [
            "app_key": getExhibitorDetailRequest.app_key!,
            "eventCode" : getExhibitorDetailRequest.eventCode!,
            "source" : getExhibitorDetailRequest.source!,
            "username": getExhibitorDetailRequest.username!,
            "sessionId": getExhibitorDetailRequest.sessionId!
        ]
    }
    
}

