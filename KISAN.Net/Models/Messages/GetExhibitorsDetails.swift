//
//  GetExhibitorsDetails.swift
//  KISAN.Net
//
//  Created by Rushikant on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetExhibitorsDetails: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var exhInfoDetails: ExhbitorInfoDetails

    
    init( success: Bool?,
   message: String?,
   responseCode: Int?,
   exhInfoDetails: ExhbitorInfoDetails,
   timeInMillis: Int?,
   successRecord: Bool?){
        
       self.success = success
       self.message =  message
       self.responseCode = responseCode
       self.timeInMillis = timeInMillis
       self.exhInfoDetails = exhInfoDetails

    }
    
    
    class func initializeGetExhibitorsDetails() -> GetExhibitorsDetails{
        let response = GetExhibitorsDetails(success: false,
                                            message: "",
                                            responseCode: -1,
                                            exhInfoDetails:ExhbitorInfoDetails.initaiLiseExhbitorInfoDetails(),
                                            timeInMillis:0,
                                            successRecord : false)
        return response
    }
    
    
    
    class func getExhibitorsDetailsFromDictionary(_ dictionary: [String: AnyObject?]) -> GetExhibitorsDetails {
        let response = initializeGetExhibitorsDetails()
        response.responseCode = dictionary["responseCode"] as? Int
        
        if dictionary["exhinfo"] != nil {
            response.exhInfoDetails = ExhbitorInfoDetails.getExhbitorInfoDetailsFromDictionary(dictionary["exhinfo"] as! [String: AnyObject])
        }
        
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as GetExhibitorsDetails
    }

}


