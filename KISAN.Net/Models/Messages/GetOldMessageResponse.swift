//
//  GetOldMessageResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class GetOldMessageResponse: NSObject {

    public var httpStatusCode: Int?
    public var message: String?
    public var requestId: String?
    public var responseCode:Int?
    public var success:Bool?
    public var timeInMillis:Int?
    
    init(httpStatusCode: Int?,
         message: String,
         requestId: String,responseCode:Int,success:Bool,timeInMillis:Int){
        self.httpStatusCode = httpStatusCode
        self.message = message
        self.requestId = requestId
        self.responseCode = responseCode
        self.success = success
        self.timeInMillis = timeInMillis
    }
    
    
    class func initializeGetOldMessageResponse() -> GetOldMessageResponse {
        let response = GetOldMessageResponse(httpStatusCode: -1, message: "",requestId:"",responseCode:-1,success:false,timeInMillis: -1)
        return response
    }
    
    class func getDictionaryFromGetOldMessageResponse(_ response: GetOldMessageResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getGetOldMessageResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> GetOldMessageResponse {
        let response = initializeGetOldMessageResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let oldMessagesList = dictionary["messagesList"] as! [AnyObject]
        for  j in (0..<oldMessagesList.count){
            var messagesList = oldMessagesList[j] as! Dictionary<String, Any>
            messagesList.updateValue(postData["communityKey"] ?? String(), forKey: "communityId")
            print("messagesList",messagesList)
            if let  messageKey = messagesList["id"] as? String {
                let messageKeyAvaiblity = MessagesServices.sharedInstance.checkMessagePresentInDB(messageKey: messageKey)
                if(messageKeyAvaiblity.count == 0){
                    _ = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: messagesList , saveDataAtDB: true)
                }else if (messageKeyAvaiblity.count == 1){
                    MessagesServices.sharedInstance.updateAtMessageTable(messageDetail: messagesList )
                }
            }
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as GetOldMessageResponse
    }
    
}
