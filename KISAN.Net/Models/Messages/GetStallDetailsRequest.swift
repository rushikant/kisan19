//
//  GetStallDetailsRequest.swift
//  KISAN.Net
//
//  Created by Rushikant on 21/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetStallDetailsRequest {
  
    var app_key :String?
    var eventCode :String?
    var source : String?
    var stallid : String?
    var sessionId : String?


    init( app_key : String?,
          eventCode :String?,
          source :String?,
          stallid: String?,
        sessionId: String? = ""
    ){

        self.app_key = app_key
        self.eventCode = eventCode
        self.source = source
        self.stallid = stallid
        self.sessionId = sessionId
    }

    class func convertToDictionary(getStallDetailsRequest:GetStallDetailsRequest) -> Dictionary<String, Any> {
        return [
            "app_key": getStallDetailsRequest.app_key!,
            "eventCode" : getStallDetailsRequest.eventCode!,
            "source" : getStallDetailsRequest.source!,
            "stallid": getStallDetailsRequest.stallid!,
            "sessionId": getStallDetailsRequest.sessionId!
        ]
    }
    
}


