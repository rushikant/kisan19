//
//  GetStallDetailsResponse.swift
//  KISAN.Net
//
//  Created by Rushikant on 19/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetStallDetailsResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var stallInfo: StallInfoRecords

    
    init( success: Bool?,
   message: String?,
   responseCode: Int?,
   stallInfo: StallInfoRecords,
   timeInMillis: Int?,
   successRecord: Bool?){
        
       self.success = success
       self.message =  message
       self.responseCode = responseCode
       self.stallInfo = stallInfo
        self.timeInMillis = timeInMillis

    }
    
    
    class func initializeGetStallDetailsResponse() -> GetStallDetailsResponse{
        let stallInfoRecords = StallInfoRecords(
                    id  : 0,
                    exhibitor_id: 0,
                    transaction_id  : 0,
                    company_name : "",
                    company_name_marathi : "",
                    contact_person_first_name : "",
                    contact_person_last_name : "",
                    contact_person_mobile  : "",
                    contact_person_email  : "",
                    contact_person_designation  : "",
                    company_email  : "",
                    company_phone  : "",
                    company_website  : "",
                    fascia_name  : "",
                    company_sms_name  : "",
                    product_info  : "",
                    product_images  : "",
                    product_range_covers  : "",
                    product_range_covers_others  : "",
                    product_category  : "",
                    product_category_other  : "",
                    logo_url  : "",
                    logo_big_thumb  : "",
                    logo_small_thumb  : "",
                    country  : "",
                    state  : "",
                    city  : "",
                    addressline1  : "",
                    addressline2  : "",
                    address  : "",
                    zip  : "",
                    pavillion_type  : "",
                    stand_type  : "",
                    stand_location  : "",
                    area  : 0,
                    sku  : "",
                    shop_order_id  : "",
                    shop_product_id  : "",
                    shop_variant_id  : "",
                    base_amount  : 0,
                    total_discount  : 0,
                    total_tax  : 0,
                    tds_amount  : 0,
                    paid_amount  : 0,
                    isGSTIN  : 0,
                    GSTIN  : "",
                    is_formsubmitted  : 0,
                    status  : "",
                    layout_stall_no  : "",
                    created_date  : "",
                    updated_date  : "",
                    faircat_status  : 0,
                    last_formlink_send_datetime  : "",
                    stall_reject_reason  : "",
                    form_submitted_date  : "",
                    faircat_locked_datetime  : "",
                    stall_approved_datetime  : "",
                    stall_rejected_datetime  : "",
                    isActive  : 0,
                    priority_number  : "",
                    updated_datetime  : "",
                    booked_datetime  : "",
                    is_trusted  : 0,
                    payment_status  : "",
                    payment_datetime  : "",
                    payment_gateway  : "",
                    payment_method  : "",
                    booked_by_fname  : "",
                    booked_by_lname  : "",
                    booked_by_mobile  : "",
                    booked_by_email  : "",
                    profile_completed_datetime  : "",
                    first_greenpass_issued_datetime  : "",
                    first_badge_issued_datetime  : "",
                    first_extra_item_paid_datetime  : "",
                    good_to_go_datetime  : "",
                    alloted_stall_no  : "",
                    alloted_hall_no  : "",
                    history_prefilled_stall_info  : 0,
                    show_history_prefilled_message  : 0,
                    user_id  : 0,
                    username  : "",
                    pavillion_name  : "",
                    stand_type_name  : "",
                    stand_location_name  : "",
                    pavillion_banner_image  : "",
                    exhibitor_fname  : "",
                    exhibitor_lname  : "",
                    stand_image  : "",
                    exhibitor_mobile  : "",
                    exhibitor_email  : "",
                    vendor_coordinator_id  : 0,
                    stall_design_images  : "",
                    exhibitor_name  : "",
                    booked_by_name  : "",
                    community_key  : "",
                    community_stall_id  : 0,
                    community_name  : "",
                    community_logo_url  : "",
                    community_logo_big_thumb_url  : "",
                    community_logo_small_thumb_url  : "",
                    community_theme_max_color  : "",
                    community_theme_min_color  : "",
                    is_vendor_coordination_form_editable  : 0,
                    vendor_coordination_form_submits  : 0,
                    profile_completenes_percentages  : 0,
                    quota_details  : []
        //            furniture_details  : ""
                )
        let response = GetStallDetailsResponse(success: false, message: "",responseCode: -1,stallInfo: stallInfoRecords,timeInMillis:0,successRecord : false)
        return response
    }
    
    
    
    class func getStallDetailsResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> GetStallDetailsResponse {
        let response = initializeGetStallDetailsResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        
        if dictionary["stallinfo"] != nil {
            response.stallInfo = StallInfoRecords.getStallInfoRecordsFromDictionary(dictionary["stallinfo"] as! [String: AnyObject])
        }
        
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as GetStallDetailsResponse
    }

}

