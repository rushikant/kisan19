//
//  GetUserDetailsResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class GetUserDetailsResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var httpStatusCode: Int?
    public var requestId: String?
    public var responseCode: Int?
    public var usersDB: [CommunityMembersList] = []
    public var timeInMillis: Int?
    
    init(usersDB: [CommunityMembersList],success: Bool?,
         message: String,
         responseCode: Int,httpStatusCode:Int,requestId:String){
        self.success = success
        self.usersDB = usersDB
        self.message = message
        self.responseCode = responseCode
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
    }
    
    class func initializeGetUserDetailsResponse() -> GetUserDetailsResponse {
        let response = GetUserDetailsResponse(usersDB: [],success: false, message: "",responseCode: -1,httpStatusCode: -1,requestId: "")
        return response
    }
    
    class func getDictionaryFromGetUserDetailsResponse(_ response: GetUserDetailsResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getGetUserDetailsResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> GetUserDetailsResponse {
        let response = initializeGetUserDetailsResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        var membersData = dictionary["usersDB"] as! Dictionary<String, Any>
        let id = membersData["id"] as? String
        //let membersDetailsOnUserId = MembersServices.sharedInstance.checkMembersuserId(userId: userId!)
        let membersDetailsOnUserId = MembersServices.sharedInstance.getMembersDetailsOnId(id:id!)
        if(membersDetailsOnUserId.count == 0){
            MembersServices.sharedInstance.sendParticularMembersAtMembersTable(membersDetails: membersData)
        }else if (membersDetailsOnUserId.count == 1){
            MembersServices.sharedInstance.updateParticularMembersAtMembersTable(membersDetails: membersData)
        }else{
            print("Not allowed")
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        return response as GetUserDetailsResponse
    }
    
}
