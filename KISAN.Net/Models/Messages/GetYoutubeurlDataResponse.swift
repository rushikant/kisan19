//
//  GetYoutubeurlDataResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/02/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class GetYoutubeurlDataResponse: NSObject {
    public var Youtubedata: [YoutubeGetData] = []
    
    init(Youtubedata: [YoutubeGetData]){
        self.Youtubedata = Youtubedata
    }
    
    
    class func initializeGetYoutubeurlDataResponse() -> GetYoutubeurlDataResponse {
        let response = GetYoutubeurlDataResponse(Youtubedata: [])
        return response
    }
    
    class func getDictionaryFromUserAreaOfInterestResponse(_ response: GetYoutubeurlDataResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getGetYoutubeurlDataResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> GetYoutubeurlDataResponse {
        let response = initializeGetYoutubeurlDataResponse()
        let data = dictionary["items"]
        response.Youtubedata = YoutubeGetData.getModelFromArray(data as! [AnyObject])
        return response as GetYoutubeurlDataResponse
    }
}
