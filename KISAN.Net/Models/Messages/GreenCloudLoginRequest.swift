//
//  GreenCloudLoginRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 23/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class GreenCloudLoginRequest{
    private var eventCode : String?
    private var source : String?
    private var app_key: String?
    private var token:String?
    
    
    init(eventCode: String, source: String, app_key: String, token:String) {
        self.eventCode = eventCode
        self.source = source
        self.app_key = app_key
        self.token = token
    }
    
    class func convertToDictionary(greenCloudloginDetails:GreenCloudLoginRequest) -> Dictionary<String, Any> {
        return [
            "eventCode": greenCloudloginDetails.eventCode!,
            "source": greenCloudloginDetails.source!,
            "app_key" : greenCloudloginDetails.app_key!,
            "token" : greenCloudloginDetails.token!
        ]
    }
}



