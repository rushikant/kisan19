//
//  GreenCloudLoginResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 23/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class GreenCloudLoginResponse :NSObject{
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var profileDetails: [ProfileDetails]
    public var inviteSummary: [InviteSummaryRecords]
    public var greenpasses: [GreenPass] = []
    public var isMitra: Bool?
    public var pending_invitation_count : Int?
    public var eventstartdatetime : String?


    init(success:Bool?,message:String?,responseCode:Int,profileDetails:[ProfileDetails],greenPass:[GreenPass],isMitra:Bool,pendingInviteCount:Int,inviteSummary: [InviteSummaryRecords],eventstartdatetime:String) {
        
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.profileDetails = profileDetails
        self.greenpasses = greenPass
        self.pending_invitation_count = pendingInviteCount
        self.isMitra = isMitra
        self.inviteSummary = inviteSummary
    
    }
    
    class func initializeGreenCloudResponse() -> GreenCloudLoginResponse{
        let response = GreenCloudLoginResponse(success: false, message: "",responseCode: -1,profileDetails:[],greenPass:[],isMitra:false,pendingInviteCount:0,inviteSummary:[], eventstartdatetime: "" )
         return response
    }
    
    
    
    class func getGreenCloudLoginResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> GreenCloudLoginResponse {
        let response = initializeGreenCloudResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.pending_invitation_count = dictionary["pending_invitation_count"] as? Int
        response.isMitra = dictionary["isMitra"] as? Bool
        let profileDetails = dictionary["profileDetails"] as? [String: AnyObject?]
        response.profileDetails  = [ProfileDetails.getGreenCloudUserDetailsFromDictionary(profileDetails!)]
        if let dict = dictionary["greenpasses"] {
          response.greenpasses = GreenPass.getModelFromArray(dict as! [AnyObject])
        }
        if let dict = dictionary["inviteSummary"] {
            if dict != nil {
                response.inviteSummary = [InviteSummaryRecords.genInviteSummeryRecordsFromDictionary(dict as! [String : AnyObject?])]
            }
        }
        
        if let dictEventDetails =  dictionary["eventDetails"] {
            let details = dictEventDetails!["details"] as? [String: AnyObject?]
            response.eventstartdatetime = (details!["eventstartdatetime"] as! String)
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as GreenCloudLoginResponse
    }

}
