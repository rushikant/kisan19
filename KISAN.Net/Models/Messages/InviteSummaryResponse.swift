//
//  InviteSummaryResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 25/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InviteSummaryResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var records: [InviteSummaryRecords] = []
    public var settings: [InviteSummarySettings] = []

    
    init( success: Bool?,
   message: String?,
   responseCode: Int?,
   records: [InviteSummaryRecords] = [],
   settings: [InviteSummarySettings] = [],

   successRecord: Bool?){
        
       self.success = success
       self.message =  message
       self.responseCode = responseCode
       self.records = records
        self.settings = settings

    }
    
    
    class func initializeInviteSummaryResponse() -> InviteSummaryResponse{
        let response = InviteSummaryResponse(success: false, message: "",responseCode: -1,records:[],settings:[],successRecord : false)
        return response
    }
    
    
    
    class func getInviteSummaryResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> InviteSummaryResponse {
        let response = initializeInviteSummaryResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let exhInfo = dictionary["exhinfo"] as? [String:AnyObject]
        let dictRecords  = exhInfo!["record"]
        let dictSettings = exhInfo!["settings"]
        response.records = InviteSummaryRecords.getModelFromArray(dictRecords as! [AnyObject])
        response.settings = [InviteSummarySettings.getinviteSummarySettingsFromDictionary(dictSettings as! [String : AnyObject?])]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as InviteSummaryResponse
    }

}
