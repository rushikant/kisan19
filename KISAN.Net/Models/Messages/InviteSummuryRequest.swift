//
//  InviteSummuryRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 25/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InviteSummuryRequest {
    var eventCode : String?
    var source :String?
    var pagesize :Int?
    var currentpage :Int?
    var sort_col :String?
    var app_key : String?
    var sessionId :String?
    var filter :[String:AnyObject]
    
    
    init( eventCode : String?,source :String?,pagesize :Int?,currentpage :Int?,sort_col :String?,app_key : String?,sessionId :String?,filter:[String:AnyObject]){
        
        self.eventCode = eventCode
        self.source = source
        self.pagesize = pagesize
        self.currentpage = currentpage
        self.sort_col = sort_col
        self.app_key = app_key
        self.sessionId = sessionId
        self.filter = filter
    }
    
    class func convertToDictionary(inviteSummuryRequest:InviteSummuryRequest) -> Dictionary<String, Any> {
        return [
            "eventCode": inviteSummuryRequest.eventCode!,
            "source": inviteSummuryRequest.source!,
            "pagesize" : inviteSummuryRequest.pagesize!,
            "currentpage" : inviteSummuryRequest.currentpage!,
            "sort_col": inviteSummuryRequest.sort_col!,
            "app_key": inviteSummuryRequest.app_key!,
            "sessionId" : inviteSummuryRequest.sessionId!,
            "filter" : inviteSummuryRequest.filter
        ]
    }


}
