//
//  IssueInviteDeatilsResponse.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 09/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class IssueInviteDeatilsResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var summary: [SummaryDetailsOfIssueInvite]
    
    
 //   {"timeInMillis":930,"success":true,"message":"passes issued successfully","responseCode":0,"summary":{"passes":1,"pass_error_visitors":0,"valid_visitors":1,"invalid_visitors":0,"duplicate_visitors":0}}
    
    
    init(summary: [SummaryDetailsOfIssueInvite],success: Bool?,
         message: String,
         responseCode: Int,httpStatusCode:Int,requestId:String,timeInMillis:Int,recordsAffected:Int){
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
        self.summary = summary
    }
    
    
    class func initializeIssueInviteDeatilsResponse() -> IssueInviteDeatilsResponse {
        let response = IssueInviteDeatilsResponse(summary: [],success: false, message: "",responseCode: -1,httpStatusCode:-1,requestId:"",timeInMillis:-1,recordsAffected:-1)
        return response
    }
    
    class func getDictionaryFromIssueInviteDeatilsResponse(_ response: IssueInviteDeatilsResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getIssueInviteDeatilsResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> IssueInviteDeatilsResponse {
        let response = initializeIssueInviteDeatilsResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        let summaryDetails = dictionary["summary"] as? [String: AnyObject?]
        response.summary  = [SummaryDetailsOfIssueInvite.genSummaryDetailsOfIssueInvite(summaryDetails!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
       // response.httpStatusCode = dictionary["httpStatusCode"] as? Int
       // response.requestId = dictionary["requestId"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
       // response.recordsAffected = dictionary["recordsAffected"] as? Int
        return response as IssueInviteDeatilsResponse
    }
    
}
