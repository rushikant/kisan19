//
//  IssueInvitiesPeopleDetailsRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class IssueInvitiesPeopleDetailsRequest {
    public var mobile : String?
    public var first_name : String?
    public var last_name : String?
    public var state : String?
    public var district : String?
    public var countrycode : String?
    public var imageUrl : String?
    
    init(mobile: String, first_name: String, last_name: String, state:String, district:String,countrycode:String,imageUrl:String) {
        self.mobile = mobile
        self.first_name = first_name
        self.last_name = last_name
        self.state = state
        self.district = district
        self.countrycode = countrycode
        self.imageUrl = imageUrl
    }
    
    class func convertToDictionary(issueInvitiesPeopleDetailsDetails:IssueInvitiesPeopleDetailsRequest) -> Dictionary<String, Any> {
        return [
            "mobile": issueInvitiesPeopleDetailsDetails.mobile!,
            "first_name": issueInvitiesPeopleDetailsDetails.first_name!,
            "last_name": issueInvitiesPeopleDetailsDetails.last_name!,
            "state" : issueInvitiesPeopleDetailsDetails.state!,
            "district" : issueInvitiesPeopleDetailsDetails.district!,
            "countrycode" : issueInvitiesPeopleDetailsDetails.countrycode!,
            "imageUrl" : issueInvitiesPeopleDetailsDetails.imageUrl!
        ]
    }
}
