//
//  IssueInvitiesRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 08/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class IssueInvitiesRequest {
    public var app_key : String?
    public var sessionId : String?
    public var eventCode : String?
    public var source : String?
    public var type : String?
    public var passType : String?
    public var entryType : String?

    init(app_key: String, sessionId: String, eventCode: String, source:String, type:String,passType:String,entryType:String) {
        self.app_key = app_key
        self.sessionId = sessionId
        self.eventCode = eventCode
        self.source = source
        self.type = type
        self.passType = passType
        self.entryType = entryType
    }
    
    class func convertToDictionary(issueInvitiesDetails:IssueInvitiesRequest) -> Dictionary<String, Any> {
        return [
            "app_key": issueInvitiesDetails.app_key!,
            "sessionId": issueInvitiesDetails.sessionId!,
            "eventCode": issueInvitiesDetails.eventCode!,
            "source" : issueInvitiesDetails.source!,
            "type" : issueInvitiesDetails.type!,
            "passType" : issueInvitiesDetails.passType!,
            "entryType" : issueInvitiesDetails.entryType!
        ]
    }
}
