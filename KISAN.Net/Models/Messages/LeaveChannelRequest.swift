//
//  LeaveChannelRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 11/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class LeaveChannelRequest {
    public var operation : String?
    public var communityJabberId : String?
    public var new_admin_user_id : String?
    
    init(operation: String, communityJabberId: String, new_admin_user_id: String) {
        self.operation = operation
        self.communityJabberId = communityJabberId
        self.new_admin_user_id = new_admin_user_id
    }
    
    class func convertToDictionary(leaveChannelDetails:LeaveChannelRequest) -> Dictionary<String, Any> {
        return [
            "operation": leaveChannelDetails.operation!,
            "communityJabberId": leaveChannelDetails.communityJabberId!,
            "new_admin_user_id": leaveChannelDetails.new_admin_user_id!
        ]
    }
}
