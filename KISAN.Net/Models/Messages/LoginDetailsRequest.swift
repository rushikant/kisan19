//
//  LoginRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class LoginDetailsRequest {
    public var facebookToken : String?
    public var resource : String?
    public var fcmDeviceId : String?
    public var app : String?
    public var source : String?
    public var verificationCode: String?

    init(facebookToken: String, resource: String, fcmDeviceId: String, app:String, source:String,verificationCode:String) {
        self.facebookToken = facebookToken
        self.resource = resource
        self.fcmDeviceId = fcmDeviceId
        self.app = app
        self.source = source
        self.verificationCode = verificationCode
    }
    
    class func convertToDictionary(loginDetails:LoginDetailsRequest) -> Dictionary<String, Any> {
        return [
            "facebookToken": loginDetails.facebookToken!,
            "resource": loginDetails.resource!,
            "fcmDeviceId": loginDetails.fcmDeviceId!,
            "app" : loginDetails.app!,
            "source" : loginDetails.source!,
            "verificationCode" : loginDetails.verificationCode!
        ]
    }
}
