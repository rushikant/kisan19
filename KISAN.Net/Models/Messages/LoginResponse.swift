//
//  LoginResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class LoginResponse: NSObject {

    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var token: String?
    public var user_details: [User]
    public var country: String?
    public var mobile: String?
    public var registrationToken:String?
    public var middlewareToken:String?
    public var oAuthToken:String?
    
    init(token: String?,
         user_details: [User],success: Bool?,
         message: String,
         responseCode: Int,country: String,mobile:String,registrationToken:String,middlewareToken:String,oAuthToken:String){
        self.token = token
        self.user_details = user_details
        self.country = country
        self.mobile = mobile
        self.registrationToken = registrationToken
        self.middlewareToken = middlewareToken
        self.oAuthToken = oAuthToken
        self.success = success
        self.message = message
    }
    
    
    class func initializeLoginResponse() -> LoginResponse {
        let response = LoginResponse(token: "",user_details: [],success: false, message: "",responseCode: -1,country:"",mobile:"",registrationToken:"",middlewareToken:"",oAuthToken:"")
        return response
    }
    
    class func getDictionaryFromLoginResponse(_ response: LoginResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getLoginResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> LoginResponse {
        let response = initializeLoginResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.country = dictionary["country"] as? String
        response.token = dictionary["token"] as? String
        response.middlewareToken = dictionary["middlewareToken"] as? String
        response.oAuthToken = dictionary["oAuthToken"] as? String
        if response.responseCode == ResponseCode.loginResponseCode || response.responseCode == 0{
            let userDetails = dictionary["user"] as? [String: AnyObject?]
            response.user_details  = [User.getUserDetailsFromDictionary(userDetails!)]
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.mobile = dictionary["mobile"] as? String
        response.registrationToken = dictionary["registrationToken"] as? String
        return response as LoginResponse
    }
 
}
