//
//  LogoutFromAppRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 22/08/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
//
//  EditChannelProfileRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class LogOutFromAppRequest {

    public var userId:String!
    
    init(userID: String) {
        self.userId = userID
    }
    
    class func convertToDictionary(louOutUser:LogOutFromAppRequest) -> Dictionary<String, Any> {
        return [
            "userId": louOutUser.userId!,
        ]
    }
}
