//
//  MessageRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 13/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation


class MessageRequest {
  
    public var id : String?
    public var mediaType : String?
    public var messageType : String?
    public var contentField1 : String?
    public var contentField2 : String?
    public var contentField3 : String?
    public var contentField4 : String?
    public var contentField5 : String?
    public var contentField6 : String?
    public var contentField7 : String?
    public var contentField8 : String?
    public var contentField9 : String?
    public var contentField10 : String?
    public var senderId : String?
    public var communityId : String?
    public var jabberId : String?
    public var createdDatetime : String?
    public var hiddenDatetime : String?
    public var deletedDatetime : String?
    public var hiddenbyUserId : String?
    public var deletedbyUserId : String?
    public var messageStatus : String?
    public var communityName : String?
    public var communityProfileUrl : String?
    
    init( id: String?, mediaType: String?, messageType: String?, contentField1: String?, contentField2: String?, contentField3:String?, contentField4: String?, contentField5: String?, contentField6: String?, contentField7: String?, contentField8: String?, contentField9:String?, contentField10: String?, senderId: String?, communityId: String?, jabberId: String?, createdDatetime:String?,hiddenDatetime:String?, deletedDatetime: String?,hiddenbyUserId:String?,deletedbyUserId:String?,messageStatus:String?,communityName:String?,communityProfileUrl:String?) {
        
        self.id = id
        self.mediaType = mediaType
        self.messageType = messageType
        self.contentField1 = contentField1
        self.contentField2 = contentField2
        self.contentField3 = contentField3
        self.contentField4 = contentField4
        self.contentField5 = contentField5
        self.contentField6 = contentField6
        self.contentField7 = contentField7
        self.contentField8 = contentField8
        self.contentField9 = contentField9
        self.contentField10 = contentField10
        self.senderId = senderId
        self.communityId = communityId
        self.jabberId = jabberId
        self.createdDatetime = createdDatetime
        self.hiddenDatetime = hiddenDatetime
        self.deletedDatetime = deletedDatetime
        self.hiddenbyUserId = hiddenbyUserId
        self.deletedbyUserId = deletedbyUserId
        self.messageStatus = messageStatus
        self.communityName = communityName
        self.communityProfileUrl = communityProfileUrl
    }

    class func convertToDictionary(sendMessage:MessageRequest) -> Dictionary<String, Any> {
        
        return [
            "id" : sendMessage.id ?? String(),
            "mediaType" : sendMessage.mediaType ?? String(),
            "messageType" : sendMessage.messageType ?? String(),
            "contentField1" : sendMessage.contentField1 ?? String(),
            "contentField2" : sendMessage.contentField2 ?? String(),
            "contentField3" : sendMessage.contentField3 ?? String(),
            "contentField4" : sendMessage.contentField4 ?? String(),
            "contentField5" : sendMessage.contentField5 ?? String(),
            "contentField6" : sendMessage.contentField6 ?? String(),
            "contentField7" : sendMessage.contentField7 ?? String(),
            "contentField8" : sendMessage.contentField8 ?? String(),
            "contentField9" : sendMessage.contentField9 ?? String(),
            "contentField10" : sendMessage.contentField10 ?? String(),
            "senderId" : sendMessage.senderId ?? String(),
            "communityId" : sendMessage.communityId ?? String(),
            "jabberId" : sendMessage.jabberId ?? String(),
            "createdDatetime" : sendMessage.createdDatetime ?? String(),
            "hiddenDatetime" : sendMessage.hiddenDatetime ?? String(),
            "deletedDatetime" : sendMessage.deletedDatetime ?? String(),
            "hiddenbyUserId" : sendMessage.hiddenbyUserId ?? String(),
            "deletedbyUserId" : sendMessage.deletedbyUserId ?? String(),
            "messageStatus" : sendMessage.messageStatus ?? String(),
            "communityName" : sendMessage.communityName ?? String(),
            "communityProfileUrl" : sendMessage.communityProfileUrl ?? String()
        ]
    }
    
}
