//
//  MuteUnMuteRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 22/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class MuteUnMuteRequest {
    public var muteStatus : String?
    public var communityKey : String?

    init(muteStatus: String, communityKey: String) {
        self.muteStatus = muteStatus
        self.communityKey = communityKey
    }
    class func convertToDictionary(muteUnMuteDetails:MuteUnMuteRequest) -> Dictionary<String, Any> {
        return [
            "muteStatus": muteUnMuteDetails.muteStatus!,
            "communityKey": muteUnMuteDetails.communityKey!
        ]
    }
}
