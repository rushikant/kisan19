//
//  MuteUnMuteResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 22/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MuteUnMuteResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var httpStatusCode: Int?
    public var requestId: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var recordsAffected: Int?

    init(success: Bool?,
         message: String?,
         httpStatusCode: Int?,requestId:String?,responseCode: Int?,timeInMillis: Int?,recordsAffected: Int?){
        self.success = success
        self.message = message
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
        self.recordsAffected = recordsAffected
    }

    class func initializeMuteUnMuteResponse() -> MuteUnMuteResponse {
        let response = MuteUnMuteResponse(success: false, message: "",httpStatusCode: -1,requestId: "",responseCode: -1,timeInMillis: -1,recordsAffected: -1)
        return response
    }
    
    class func getDictionaryFromMuteUnMuteResponse(_ response: MuteUnMuteResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getMuteUnMuteResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> MuteUnMuteResponse {
        let response = initializeMuteUnMuteResponse()
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        response.responseCode = dictionary["responseCode"] as? Int
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.recordsAffected = dictionary["recordsAffected"] as? Int
        return response as MuteUnMuteResponse
    }
    
    
}
