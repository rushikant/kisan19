//
//  MyChatListRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation

class MyChatListRequest {
    public var userName : String!
    public var token : String!
    public var resource : String!
    public var appKey:String!
    public var appSecret:String!
  
    init(userName: String,token: String,resource:String,appKey:String,appSecret:String) {
        self.userName = userName
        self.token = token
        self.resource = resource
        self.appKey = appKey
        self.appSecret = appSecret
    }
    
    class func convertToDictionary(myChatListDetails:MyChatListRequest) -> Dictionary<String, Any> {
        return [
            "userName": myChatListDetails.userName!,
            "token": myChatListDetails.token!,
            "resource": myChatListDetails.resource,
            "appKey": myChatListDetails.appKey,
            "appSecret": myChatListDetails.appSecret
        ]
    }
    
}
