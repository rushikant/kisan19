//
//  MyChatListResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MyChatListResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var timeInMillis: Int?
    public var httpStatusCode: Int?
    public var communityKey: String?
    
    init(success: Bool?,
         message: String,
         response_code: Int,timeInMillis:Int,httpStatusCode:Int,communityKey:String){
        self.success = success
        self.message = message
        self.response_code = response_code
        self.timeInMillis = timeInMillis
        self.httpStatusCode = httpStatusCode
        self.communityKey = communityKey
    }
    
    
    class func initializeMyChatListResponse() -> MyChatListResponse {
        let response = MyChatListResponse(success: false, message: "",response_code: -1,timeInMillis: -1,httpStatusCode: -1,communityKey: "")
        return response
    }
    
    class func getDictionaryFromMyChatListResponse(_ response: MyChatListResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    // Call this function for getMyChatList form Dashboard
    class func getMyChatListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> MyChatListResponse {
        let response = initializeMyChatListResponse()
        response.response_code = dictionary["response_code"] as? Int
        var myChatDetails = dictionary["myChats"] as! [AnyObject]
        //myChatDetails = (myChatDetails as NSArray).sortedArray(using: [NSSortDescriptor(key: "last_activity_datetime", ascending: false)]) as! [[String:AnyObject]] as [AnyObject]
        for  j in (0..<myChatDetails.count){
            var myChatList = myChatDetails[j] as! Dictionary<String, Any>
            let communityKey = myChatList["communityKey"] as? String
            let communityName = myChatList["communityName"] as? String
            print("communityName",communityName as Any)
            let id = myChatList["id"] as? String
            print("id",id as Any)
            let isSponsoredMessage = String(describing: myChatList["isSponsoredMessage"]!) //myChatList["isSponsoredMessage"] as? String
           
            if isSponsoredMessage == StringConstants.ZERO{
                //Normal follwed channel - sponsored message value 0
                let checkCommunityKeyWithisSponsoredFlag = MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey!, isSponsoredMessage: isSponsoredMessage)
                //let communityDetailsCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey!)
                if(checkCommunityKeyWithisSponsoredFlag.count == 0){
                    MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: myChatList, isFirstTime: true,iSCommingFromOneTOneinitiate:false)
                }else {
                    MyChatServices.sharedInstance.updateAtMyChatTableForiSSponsoredValueZero(communityDetail: myChatList)
                }
            }else{
                //sponsored message - value 1
                let checkCommunityKeyWithisSponsoredFlag = MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdTrue(communityKey: communityKey!, isSponsoredMessage: isSponsoredMessage, id: id!)
                if(checkCommunityKeyWithisSponsoredFlag.count == 0){
                    MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: myChatList, isFirstTime: true,iSCommingFromOneTOneinitiate:false)
                }else{
                    MyChatServices.sharedInstance.updateAtOnlyUIONMyChatTableForiSSponsoredValueOne(communityDetail: myChatList)
                }
            }
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as MyChatListResponse
    }
    
    //Call this function for getMyChat from create oneToOneChat webservice
    class func getOneToOneMyChatListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> MyChatListResponse{
        let response = initializeMyChatListResponse()
        response.response_code = dictionary["responseCode"] as? Int
        var myChatList = dictionary["myChat"] as! Dictionary<String, Any>
        let communityJabberId = myChatList["communityJabberId"] as? String
        myChatList.updateValue(StringConstants.CommunityType.CHANNEL, forKey: "type")
        myChatList.updateValue(URLConstant.c_OneToOne, forKey: "featured")
        myChatList.updateValue(StringConstants.ONE, forKey: "isMember")
        myChatList.updateValue(StringConstants.CommunityPrivacy.PUBLIC, forKey: "privacy")
        myChatList.updateValue(myChatList["communityJabberId"] ?? String(), forKey: "communityKey")
        myChatList.updateValue(myChatList["communityJabberId"] ?? String(), forKey: "userCommunityJabberId")
        let communityDetailsCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityJabberId!)
        if(communityDetailsCommunityKey.count == 0){
            MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: myChatList, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
        }else if (communityDetailsCommunityKey.count == 1){
            MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: myChatList)
        }else{
            print("Not allowed")
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.communityKey = myChatList["communityJabberId"] as? String
        return response as MyChatListResponse
    }
    
    // Call this func for save OneToOne initiated user detail with mychat data
    class func getOneToOneUserInitiatedMyChatListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> MyChatListResponse{
        let response = initializeMyChatListResponse()
        response.response_code = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.httpStatusCode = dictionary["httpStatusCode"]  as? Int
        var myChatDetails = dictionary["myChats"] as! [AnyObject]
        for  j in (0..<myChatDetails.count){
            var myChatList = myChatDetails[j] as! Dictionary<String, Any>
            let communityJabberId = myChatList["communityJabberId"] as? String
            myChatList.updateValue(StringConstants.CommunityType.CHANNEL, forKey: "type")
            myChatList.updateValue(URLConstant.c_OneToOne, forKey: "featured")
            myChatList.updateValue(StringConstants.ONE, forKey: "isMember")
            myChatList.updateValue(StringConstants.CommunityPrivacy.PUBLIC, forKey: "privacy")
            //myChatList.updateValue(StringConstants.EMPTY, forKey: "messageAt")
            myChatList.updateValue(communityJabberId!, forKey: "communityKey")
            myChatList.updateValue(communityJabberId!, forKey: "userCommunityJabberId")
            let communityDetailsCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityJabberId!)
            if(communityDetailsCommunityKey.count == 0){
                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: myChatList, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
            }else if (communityDetailsCommunityKey.count == 1){
                MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: myChatList)
            }else{
                print("Not allowed")
            }
        }
        
        return response as MyChatListResponse
    }
}
