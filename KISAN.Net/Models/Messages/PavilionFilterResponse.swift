//
//  PavilionFilterResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 03/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class PavilionFilterResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var pavilion_list: [PavilionList] = []
    
    init(pavilion_list: [PavilionList],success: Bool?,
         message: String,
         response_code: Int){
        self.success = success
        self.pavilion_list = pavilion_list
        self.message = message
        self.response_code = response_code
    }
    
    
    class func initializePavilionFilterResponse() -> PavilionFilterResponse {
        let response = PavilionFilterResponse(pavilion_list: [],success: false, message: "",response_code: -1)
        return response
    }
    
    class func getDictionaryFromUserAreaOfInterestResponse(_ response: PavilionFilterResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUserAreaOfInterestResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> PavilionFilterResponse {
        let response = initializePavilionFilterResponse()
        response.response_code = dictionary["response_code"] as? Int
        let data = dictionary["data"] as? [String: AnyObject?]
        response.pavilion_list = PavilionList.getModelFromArray(data!["categories_list"] as! [AnyObject])
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as PavilionFilterResponse
    }

}
