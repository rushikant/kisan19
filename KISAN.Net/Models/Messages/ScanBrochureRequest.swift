//
//  ScanBrochureRequest.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import UIKit

/*
"{
  ""app_key"": ""nLyJTPX30kvsyuX5K7KvpQD2xB091F65"",
  ""eventCode"":  ""KISAN19"",
  ""sessionId"": ""cWg5WTFOUXNqV1hFeTg3aFlGUlVoQXFHdTBrMV8xNTQ2MjM5NTgyOTYx"",
  ""source"":'ios',
  ""data"": [{""shortcode"": ""455"",
  ""scanned_date_time"": ""2019-11-05 11:10:54""
  },{""shortcode"": ""877"",
  ""scanned_date_time"": ""2019-11-05 12:09:54""
  }]
}"
*/
class ScanBrochureRequest: NSObject {
    
    var   app_key : String?
    var   eventCode: String?
    var   sessionId: String?
    var   source: String?
    var   data: [ScanData]?
    
    
    init (app_key : String?,eventCode : String?,sessionId: String?,source: String?,data:[ScanData]){
        self.app_key = app_key
        self.eventCode = eventCode
        self.sessionId = sessionId
        self.source = source
        self.data = data
    }
    
    class func convertToDictionary(request:ScanBrochureRequest) -> Dictionary<String, Any> {
        return [
            "app_key": request.app_key!,
            "eventCode": request.eventCode!,
            "sessionId": request.sessionId!,
            "source": request.source!,
            "data" : ScanBrochureRequest.getScanData(data: request.data!)
        ]
    }
    
    class func getScanBrochureRequestFromDictionary(_ dictionary: [String: AnyObject?]) -> ScanBrochureRequest {
        let response = ScanBrochureRequest(app_key: "", eventCode: "", sessionId: "", source: "", data: [])
        response.app_key = dictionary["app_key"]  as? String
        response.eventCode = dictionary["eventCode"]  as? String
        response.sessionId = dictionary["sessionId"]  as? String
        response.source = dictionary["source"]  as? String
        var codes = [ScanData]()
        if let dict = dictionary["data"] {
            
            if (dict as! [[String:AnyObject]]).count > 0 {
                let scanObject = (dict as! [[String:AnyObject]])[0]
                let shortCode = scanObject["shortcode"]  as? String
                let scanned_date_time = scanObject["scanned_date_time"]  as? String
                let code = ScanData(shortcode: shortCode, scanned_date_time:scanned_date_time)
                codes.append(code)
            }
        }
        response.data = codes
        return response
    }
    
    class func getScanData(data:[ScanData]) -> [[String:Any]] {
        var response = [[String:Any]] ()
        for scan in data {
            response.append(scan.convertToDictionary())
        }
        return response
    }
        
}

/*
{""shortcode"": ""455"",
""scanned_date_time"": ""2019-11-05 11:10:54""
}
*/
class ScanData: NSObject {
    var   shortcode : String?
    var   scanned_date_time: String?
    
    init (shortcode : String?,scanned_date_time : String?){
        self.shortcode = shortcode
        self.scanned_date_time = scanned_date_time
    }
    
    func convertToDictionary() -> Dictionary<String, Any> {
        return [
            "shortcode": self.shortcode!,
            "scanned_date_time": self.scanned_date_time!
        ]
    }
}
