//
//  SelectStateDistrictResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 23/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SelectStateDistrictResponse: NSObject {
    var item:[String: AnyObject]!
    init(item:[String: AnyObject]) {
    }
    
    class func initializeSelectStateDistrictResponse() -> SelectStateDistrictResponse {
        let model = SelectStateDistrictResponse(item:[:])
        return model
    }
    
    class func getDictionarySelectStateDistrictResponse(_ model: SelectStateDistrictResponse) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }
    
    class func getSelectStateDistrictResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> SelectStateDistrictResponse {
        let response = initializeSelectStateDistrictResponse()
        let data = dictionary["string-array"] as? [String: AnyObject?]
        response.item = data! as [String : AnyObject]
        return response as SelectStateDistrictResponse
    }

    
}
