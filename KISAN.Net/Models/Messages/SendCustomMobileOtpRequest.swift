//
//  SendCustomMobileOtpRequest.swift
//  KISAN.Net
//
//  Created by Rushikant on 26/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

class SendCustomMobileOtpRequest: NSObject {
        
        var client_id :String?
        var client_secret :String?
        var mobile :String?
        var account_type :Int
        var source :String
        
        
        
    init( client_id : String?,
          client_secret :String?,
          mobile :String?,
          account_type:Int,
          source:String){
            
            self.client_id = client_id
            self.client_secret = client_secret
            self.mobile = mobile
            self.account_type = account_type
            self.source = source
            
        }
        

        
        class func convertToDictionary(SendCustomMobileOtpRequest:SendCustomMobileOtpRequest) -> Dictionary<String, Any> {
            return [
                "client_id": SendCustomMobileOtpRequest.client_id!,
                "client_secret": SendCustomMobileOtpRequest.client_secret!,
                "mobile" : SendCustomMobileOtpRequest.mobile!,
                "account_type": SendCustomMobileOtpRequest.account_type,
                "source": SendCustomMobileOtpRequest.source
            ]
        }
}
