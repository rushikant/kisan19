//
//  SendCustomMobileOtpResponse.swift
//  KISAN.Net
//
//  Created by Rushikant on 26/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

class SendCustomMobileOtpResponse: NSObject {
   
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var data: Dictionary<String, Any>
    
    
    init(success: Bool?,message: String?,responseCode: Int?,data: Dictionary<String, Any>) {
        
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.data = data
    }
    
    class func initializeSendCustomMobileOtpResponse() -> SendCustomMobileOtpResponse {
        let response = SendCustomMobileOtpResponse(success: false, message: "",responseCode: -1,data:[:])
        return response
    }
    
    class func getDictionaryFromSendCustomMobileOtpResponse(_ response: SendCustomMobileOtpResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getSendCustomMobileOtpResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> SendCustomMobileOtpResponse {
        let response = initializeSendCustomMobileOtpResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.data = dictionary["data"] as! Dictionary<String, Any>
        
        return response as SendCustomMobileOtpResponse
    }
}
