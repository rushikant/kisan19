//
//  SendInvitationDetailsRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class SendInvitationDetailsRequest {
    
    public var appLanguage : String?
    public var communityKey : String?
    public var initiated : String?
    public var invitationType : String?
    public var purposeCode : String?
    public var receiver : [String]?
    public var sendBy : String?
    public var sender : String?
    public var sentOn : String?
    public var status : String?
   
    init(appLanguage: String, communityKey: String, initiated: String,invitationType:String,purposeCode:String,receiver:[String],sendBy:String,sender:String,sentOn:String,status:String){
        self.appLanguage = appLanguage
        self.communityKey = communityKey
        self.initiated = initiated
        self.invitationType = invitationType
        self.purposeCode = purposeCode
        self.receiver = receiver
        self.sendBy = sendBy
        self.sender = sender
        self.sentOn = sentOn
        self.status = status
    }
    
    class func convertToDictionary(sendInvitationDetails:SendInvitationDetailsRequest) -> Dictionary<String, Any> {
        return [
            "appLanguage": sendInvitationDetails.appLanguage!,
            "communityKey": sendInvitationDetails.communityKey!,
            "initiated": sendInvitationDetails.initiated!,
            "invitationType": sendInvitationDetails.invitationType!,
            "purposeCode": sendInvitationDetails.purposeCode!,
            "receiver": sendInvitationDetails.receiver!,
            "sendBy": sendInvitationDetails.sendBy!,
            "sender": sendInvitationDetails.sender!,
            "sentOn": sendInvitationDetails.sentOn!,
            "status": sendInvitationDetails.status!
        ]
    }
}
