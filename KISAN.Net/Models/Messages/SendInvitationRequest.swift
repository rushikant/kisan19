//
//  SendInvitationRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation


class SendInvitationRequest {
    
    public var invitation: Dictionary<String, Any>!
   
    init(invitation: Dictionary<String, Any>) {
        self.invitation = invitation
    }
    
    class func convertToDictionary(sendInvitationRequest:SendInvitationRequest) -> Dictionary<String, Any>{
        return [
            "invitation": sendInvitationRequest.invitation!
        ]
    }
}
