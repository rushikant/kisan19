//
//  SendInvitationResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SendInvitationResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    public var httpStatusCode: Int?
    public var requestId: String?
    
    init(success: Bool?,
         message: String,
         responseCode: Int,timeInMillis:Int,httpStatusCode:Int,requestId:String){
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
    }
    
    
    class func initializeSendInvitationResponse() -> SendInvitationResponse {
        let response = SendInvitationResponse(success: false, message: "",responseCode: -1,timeInMillis:-1,httpStatusCode:-1,requestId:"")
        return response
    }
    
    class func getDictionaryFromSendInvitationResponse(_ response: SendInvitationResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getSendInvitationResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> SendInvitationResponse {
        let response = initializeSendInvitationResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        return response as SendInvitationResponse
    }
    
}
