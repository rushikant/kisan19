//
//  SendMessageInDbRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation


class SendMessageInDbRequest {
    
    public var message: Dictionary<String, Any>!
    
    init(message: Dictionary<String, Any>) {
        self.message = message
    }
    
    class func convertToDictionary(sendInvitationRequest:SendMessageInDbRequest) -> Dictionary<String, Any>{
        return [
            "message": sendInvitationRequest.message!
        ]
    }
}
