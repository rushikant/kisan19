//
//  SendMessageInDbResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 26/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SendMessageInDbResponse: NSObject {
   
    public var httpStatusCode : Int?
    public var message : String?
    public var requestId : String?
    public var responseCode: Int?
    public var success : Int?
    
    init(httpStatusCode: Int?,message: String?,
         requestId: String?,
         responseCode: Int?,success:Int?){
        self.httpStatusCode = httpStatusCode
        self.requestId = requestId
        self.responseCode = responseCode
    }
    
    
    class func initializeSendMessageInDbResponse() -> SendMessageInDbResponse {
        let response = SendMessageInDbResponse(httpStatusCode:-1,message:"", requestId: "",responseCode: -1,success: -1)
        return response
    }
    
    class func getDictionaryFromSendMessageInDbResponse(_ response: SendMessageInDbResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getSendMessageInDbResponseResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> SendMessageInDbResponse {
        let response = initializeSendMessageInDbResponse()
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.message = dictionary["message"] as? String
        response.requestId = dictionary["requestId"] as? String
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Int
        return response as SendMessageInDbResponse
    }
    
    
}
