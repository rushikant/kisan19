//
//  ShareGreenPassTrackingRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ShareGreenPassTrackingRequest: NSObject {
    var app_key :String?
    var toEmails :String?
    var eventCode :String?
    var sessionId :String
    var barcode : String?
    var activity :String
    var source :String
    
    
    
    init( app_key : String?,toEmails :String?,eventCode :String?,sessionId:String,barcode:String?,activity:String,source:String){
        
        self.app_key = app_key
        self.toEmails = toEmails
        self.barcode = barcode
        self.eventCode = eventCode
        self.sessionId = sessionId
        self.activity = activity
        self.source = source
        
    }
    

    
    class func convertToDictionary(shareGreenPassTrackingRequest:ShareGreenPassTrackingRequest) -> Dictionary<String, Any> {
        return [
            "app_key": shareGreenPassTrackingRequest.app_key!,
            "toEmails": shareGreenPassTrackingRequest.toEmails!,
            "barcode" : shareGreenPassTrackingRequest.barcode!,
            "eventCode": shareGreenPassTrackingRequest.eventCode!,
            "activity": shareGreenPassTrackingRequest.activity,
            "sessionId": shareGreenPassTrackingRequest.sessionId,
            "source": shareGreenPassTrackingRequest.source
        ]
    }
}
