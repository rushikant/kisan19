//
//  ShareGreenPasstrackingResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ShareGreenPasstrackingResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    
    
    init(success: Bool?,message: String?,responseCode: Int?,timeInMillis: Int?) {
        
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    class func initializeShareGreenPasstrackingResponse() -> ShareGreenPasstrackingResponse {
        let response = ShareGreenPasstrackingResponse(success: false, message: "",responseCode: -1,timeInMillis:-1)
        return response
    }
    
    class func getDictionaryFromShareGreenPasstrackingResponse(_ response: ShareGreenPasstrackingResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getShareGreenPasstrackingResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ShareGreenPasstrackingResponse {
        let response = initializeShareGreenPasstrackingResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        
        return response as ShareGreenPasstrackingResponse
    }
}
