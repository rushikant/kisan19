//
//  ShareInvitewithFriendsResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ShareInvitewithFriendsResponse: NSObject {
   
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    
    
    init(success: Bool?,message: String?,responseCode: Int?,timeInMillis: Int?) {
        
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    class func initializeShareInvitewithFriendsResponse() -> ShareInvitewithFriendsResponse {
        let response = ShareInvitewithFriendsResponse(success: false, message: "",responseCode: -1,timeInMillis:-1)
        return response
    }
    
    class func getDictionaryFromShareInvitewithFriendsResponse(_ response: ShareInvitewithFriendsResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getShareInvitewithFriendsResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ShareInvitewithFriendsResponse {
        let response = initializeShareInvitewithFriendsResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int

        return response as ShareInvitewithFriendsResponse
    }
}


