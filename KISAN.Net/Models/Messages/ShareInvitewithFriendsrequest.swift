//
//  ShareInvitewithFriendsrequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ShareInvitewithFriendsrequest {
    var app_key :String?
    var sessionId :String?
    var language :String?
    var type : String?
    var eventCode :String?
    var source : String?
    var mobiles :[String]
    
    
    
    init( app_key : String?,sessionId :String?,language :String?,type:String?,eventCode :String?,source:String?,mobiles:[String]){
        
        self.app_key = app_key
        self.sessionId = sessionId
        self.language = language
        self.type = type
        self.mobiles = mobiles
        self.eventCode = eventCode
        self.source = source
    }
    
    class func convertToDictionary(shareInvitewithFriendsrequest:ShareInvitewithFriendsrequest) -> Dictionary<String, Any> {
        return [
            "app_key": shareInvitewithFriendsrequest.app_key!,
            "sessionId": shareInvitewithFriendsrequest.sessionId!,
            "language" : shareInvitewithFriendsrequest.language!,
            "type": shareInvitewithFriendsrequest.type!,
            "mobiles": shareInvitewithFriendsrequest.mobiles,
            "eventCode": shareInvitewithFriendsrequest.eventCode!,
            "source": shareInvitewithFriendsrequest.source!

        ]
    }

}
