//
//  ShowGuestListResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class ShowGuestListResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var exhinfo: [ExhbitorInfo]
    

    init( success: Bool?,
          message: String?,
          responseCode: Int?,
          exhinfo: [ExhbitorInfo],
          successRecord: Bool?){
        self.success = success
        self.message =  message
        self.responseCode = responseCode
        self.exhinfo = exhinfo
        
    }
    
    
    class func initializeShowGuestListResponse() -> ShowGuestListResponse{
        let response = ShowGuestListResponse(success: false, message: "",responseCode: -1,exhinfo:[],successRecord : false)
        return response
    }
    
    
    
    class func showGuestListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ShowGuestListResponse {
        let response = initializeShowGuestListResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.exhinfo = [ExhbitorInfo.genInviteGuestListDetailsFromDictionary((dictionary["exhinfo"] as? [String:AnyObject])!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as ShowGuestListResponse
    }

}
