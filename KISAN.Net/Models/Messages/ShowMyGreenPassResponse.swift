//
//  ShowMyGreenPassResponse.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 12/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class ShowMyGreenpassResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var myGreenpassList: [MyGreenpassListDetails] = []
    public var couponInfo: [CouponInfo]
    
    
    init( success: Bool?,
          message: String?,
          responseCode: Int?,
          myGreenpassList: [MyGreenpassListDetails],
          successRecord: Bool?,
          couponInfo:[CouponInfo]){
        self.success = success
        self.message =  message
        self.responseCode = responseCode
        self.myGreenpassList = myGreenpassList
        self.couponInfo = couponInfo
    }
    
    
    class func initializeShowMyGreenpassResponse() -> ShowMyGreenpassResponse{
        let response = ShowMyGreenpassResponse(success: false, message: "",responseCode: -1,myGreenpassList:[],successRecord : false,couponInfo:[])
        return response
    }
    
    
    
    class func showMyGreenpassResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> ShowMyGreenpassResponse {
        let response = initializeShowMyGreenpassResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.myGreenpassList = MyGreenpassListDetails.getModelFromArray(dictionary["greenpasses"] as! [AnyObject])
        response.couponInfo = CouponInfo.getModelFromArray([dictionary["couponInfo"] as! [String:AnyObject]]as! [AnyObject])
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as ShowMyGreenpassResponse
    }
    
}
