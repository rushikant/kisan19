//
//  SignUpRequest.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 01/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
class SignUpRequest {
    
    public var firstName : String!
    public var lastName : String!
    public var pinCode : String!
    public var loginType : String!
    public var registrationId : String!
    public var email : String!
    public var resource : String!
    public var fcmDeviceId : String!
    public var imageBlob : String!
    public var app : String?
    public var source : String?
    
    init(firstName: String,  lastName: String, pinCode: String,loginType: String, registrationId: String,email: String,resource:String,fcmDeviceId:String,imageBlob:String,app:String,source:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.pinCode = pinCode
        self.loginType = loginType
        self.registrationId = registrationId
        self.email = email
        self.resource = resource
        self.fcmDeviceId = fcmDeviceId
        self.imageBlob = imageBlob
        self.app = app
        self.source = source
    }
    
    class func convertToDictionary(signUpDetails:SignUpRequest) -> Dictionary<String, Any> {
        return [
            "firstName": signUpDetails.firstName!,
            "lastName": signUpDetails.lastName!,
            "pinCode": signUpDetails.pinCode!,
            "loginType": signUpDetails.loginType!,
            "registrationId": signUpDetails.registrationId!,
            "email": signUpDetails.email!,
            "resource": signUpDetails.resource!,
            "fcmDeviceId": signUpDetails.fcmDeviceId!,
            "imageBlob": signUpDetails.imageBlob!,
            "app" : signUpDetails.app!,
            "source" : signUpDetails.source!
        ]
    }
}
