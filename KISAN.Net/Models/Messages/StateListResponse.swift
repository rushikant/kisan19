//
//  StateListResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class StateListResponse: NSObject {
     var item:[Any]!

    init(item:[Any]) {
    }

    class func initializeStateListResponse() -> StateListResponse {
        let model = StateListResponse(item:[])
        return model
    }

    class func getDictionaryStateListResponse(_ model: StateListResponse) -> [String: AnyObject?] {
        return model.propertyDictionary()
    }

    class func getStateListResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> StateListResponse {
        let response = initializeStateListResponse()
        let data = dictionary["string-array"] as? [String: AnyObject?]
        response.item = data!["item"] as? [Any]
        return response as StateListResponse
    }

}


