//
//  UpdateChannelResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class UpdateChannelResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var requestId: String?
    public var timeInMillis: Int?
    public var httpStatusCode: Int?
    public var recordsAffected: Int?
    
    init(success: Bool?,
         message: String,requestId: String,
         responseCode: Int,timeInMillis:Int,httpStatusCode:Int,recordsAffected:Int){
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.requestId = requestId
        self.timeInMillis = timeInMillis
        self.httpStatusCode = httpStatusCode
        self.recordsAffected = recordsAffected
    }
    
    
    class func initializeUpdateChannelResponse() -> UpdateChannelResponse {
        let response = UpdateChannelResponse(success: false, message: "",requestId: "",responseCode: -1,timeInMillis:-1,httpStatusCode:-1,recordsAffected:-1)
        return response
    }
    
    class func getDictionaryFromUpdateChannelResponse(_ response: UpdateChannelResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUpdateChannelResponseFromDictionary(_ dictionary: [String: AnyObject?],postData: [String: AnyObject]) -> UpdateChannelResponse {
        let response = initializeUpdateChannelResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        response.httpStatusCode = dictionary["httpStatusCode"] as? Int
        response.requestId = dictionary["requestId"] as? String
        response.recordsAffected = dictionary["recordsAffected"] as? Int
        let communityDetails = postData["communityDetails"]
        var updatedCommunityDetails = communityDetails as! Dictionary<String, Any>
        updatedCommunityDetails.updateValue(StringConstants.DashboardViewController.updateChannel, forKey: "messageText")
        updatedCommunityDetails.updateValue(StringConstants.MessageType.banner, forKey: "messageType")
        updatedCommunityDetails.updateValue(StringConstants.MessageType.text, forKey: "mediaType")

        let currentTime = DateTimeHelper.getCurrentMillis()
        updatedCommunityDetails.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
        updatedCommunityDetails.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.last_activity_datetime)
        MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: updatedCommunityDetails )
        CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: updatedCommunityDetails)
        return response as UpdateChannelResponse
    }
    
}
