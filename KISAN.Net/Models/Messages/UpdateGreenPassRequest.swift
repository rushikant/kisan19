//
//  UpdateGreenPassRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class UpdateGreenPassRequest {
   
    var app_key :String?
    var imageurl :String?
    var eventCode :String?
    var sessionId :String
    var barcode : String?
    var first_name :String
    var last_name :String
    var source :String



    init( app_key : String?,imageurl :String?,eventCode :String?,sessionId:String,barcode:String?,first_name:String,last_name:String,source:String){
        
        self.app_key = app_key
        self.imageurl = imageurl
        self.barcode = barcode
        self.eventCode = eventCode
        self.sessionId = sessionId
        self.first_name = first_name
        self.last_name = last_name
        self.source = source

    }
    
    class func convertToDictionary(updateGreenPassRequest:UpdateGreenPassRequest) -> Dictionary<String, Any> {
        return [
            "app_key": updateGreenPassRequest.app_key!,
            "imageurl": updateGreenPassRequest.imageurl!,
            "barcode" : updateGreenPassRequest.barcode!,
            "eventCode": updateGreenPassRequest.eventCode!,
            "first_name": updateGreenPassRequest.first_name,
            "last_name": updateGreenPassRequest.last_name,
            "sessionId": updateGreenPassRequest.sessionId,
            "source": updateGreenPassRequest.source


        ]
    }
}
