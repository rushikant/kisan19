//
//  UpdateInviteSummaryResponse.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class UpdateInviteSummaryResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var timeInMillis: Int?
    
    init(success: Bool?,
         message: String,
         responseCode: Int,timeInMillis:Int){
        self.success = success
        self.message = message
        self.responseCode = responseCode
        self.timeInMillis = timeInMillis
    }
    
    
    class func initializeUpdateInviteSummaryResponse() -> UpdateInviteSummaryResponse {
        let response = UpdateInviteSummaryResponse(success: false, message: "",responseCode: -1,timeInMillis:-1)
        return response
    }
    
    class func getDictionaryFromUpdateInviteSummaryResponse(_ response: SendInvitationResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUpdateInviteSummaryResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> UpdateInviteSummaryResponse {
        let response = initializeUpdateInviteSummaryResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        response.timeInMillis = dictionary["timeInMillis"] as? Int
        return response as UpdateInviteSummaryResponse
    }
    
}
