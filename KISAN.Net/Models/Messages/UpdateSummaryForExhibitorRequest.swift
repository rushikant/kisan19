//
//  UpdateSummaryForExhibitorRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class UpdateSummaryForExhibitorRequest {
    
    public var app_key : String!
    public var community_logo_big_thumb_url : String!
    public var community_logo_small_thumb_url : String!
    public var community_logo_url : String!
    public var community_name : String!
    public var community_theme_max_color : String!
    public var community_theme_min_color : String!
    public var eventCode : String?
    public var media_link : String!
    public var media_thumbnail : String!
    public var media_type : String!
    public var sessionId : String!
    public var source : String!
    public var type : String!

    init(app_key: String,  community_logo_big_thumb_url: String, community_logo_small_thumb_url: String,community_logo_url: String, community_name: String,community_theme_max_color: String,community_theme_min_color:String,eventCode:String,media_link:String,media_thumbnail:String,media_type:String,sessionId:String,source:String,type:String) {
        self.app_key = app_key
        self.community_logo_big_thumb_url = community_logo_big_thumb_url
        self.community_logo_small_thumb_url = community_logo_small_thumb_url
        self.community_logo_url = community_logo_url
        self.community_name = community_name
        self.community_theme_max_color = community_theme_max_color
        self.community_theme_min_color = community_theme_min_color
        self.eventCode = eventCode
        self.media_link = media_link
        self.media_thumbnail = media_thumbnail
        self.media_type = media_type
        self.sessionId = sessionId
        self.source = source
        self.type = type
    }
    
    class func convertToDictionary(updateSummaryForExhibitorRequest:UpdateSummaryForExhibitorRequest) -> Dictionary<String, Any> {
        return [
            "app_key": updateSummaryForExhibitorRequest.app_key!,
            "community_logo_big_thumb_url": updateSummaryForExhibitorRequest.community_logo_big_thumb_url!,
            "community_logo_small_thumb_url": updateSummaryForExhibitorRequest.community_logo_small_thumb_url!,
            "community_logo_url": updateSummaryForExhibitorRequest.community_logo_url!,
            "community_name": updateSummaryForExhibitorRequest.community_name!,
            "community_theme_max_color": updateSummaryForExhibitorRequest.community_theme_max_color!,
            "community_theme_min_color": updateSummaryForExhibitorRequest.community_theme_min_color!,
            "eventCode": updateSummaryForExhibitorRequest.eventCode!,
            "media_link" : updateSummaryForExhibitorRequest.media_link!,
            "media_thumbnail" : updateSummaryForExhibitorRequest.media_thumbnail!,
            "media_type" : updateSummaryForExhibitorRequest.media_type!,
            "sessionId" : updateSummaryForExhibitorRequest.sessionId!,
            "source": updateSummaryForExhibitorRequest.source!,
            "type" : updateSummaryForExhibitorRequest.type!
        ]
    }
}


