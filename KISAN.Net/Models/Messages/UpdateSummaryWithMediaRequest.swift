//
//  UpdateSummaryWithMediaRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class UpdateSummaryWithMediaRequest {
    
    public var app_key : String!
    public var eventCode : String!
    public var media_link : String!
    public var media_thumbnail : String!
    public var media_type : String!
    public var sessionId : String!
    public var source : String!
    public var type : String?
    public var channel_big_thumb : String!
    public var channel_max_color : String!
    public var channel_min_color : String!
    public var channel_name : String!
    //public var skip_media : String!
    
    init(app_key: String,  channel_big_thumb: String, channel_max_color: String,channel_min_color: String, channel_name: String,eventCode: String,/*skip_media:String,*/sessionId:String,source:String,type:String,media_link:String,media_thumbnail:String,media_type:String) {
        self.app_key = app_key
        self.channel_big_thumb = channel_big_thumb
        self.channel_max_color = channel_max_color
        self.channel_min_color = channel_min_color
        self.channel_name = channel_name
        self.eventCode = eventCode
        //self.skip_media = skip_media
        self.sessionId = sessionId
        self.source = source
        self.type = type
        self.media_link = media_link
        self.media_thumbnail = media_thumbnail
        self.media_type = media_type
    }
    
    class func convertToDictionary(updateSummaryWithMediaRequest:UpdateSummaryWithMediaRequest) -> Dictionary<String, Any> {
        return [
            "app_key": updateSummaryWithMediaRequest.app_key!,
            "channel_big_thumb": updateSummaryWithMediaRequest.channel_big_thumb!,
            "channel_max_color": updateSummaryWithMediaRequest.channel_max_color!,
            "channel_min_color": updateSummaryWithMediaRequest.channel_min_color!,
            "channel_name": updateSummaryWithMediaRequest.channel_name!,
            "eventCode": updateSummaryWithMediaRequest.eventCode!,
           //"skip_media": updateSummaryWithMediaRequest.skip_media!,
            "sessionId": updateSummaryWithMediaRequest.sessionId!,
            "source": updateSummaryWithMediaRequest.source!,
            "type" : updateSummaryWithMediaRequest.type!,
            "media_link" : updateSummaryWithMediaRequest.media_link!,
            "media_thumbnail" : updateSummaryWithMediaRequest.media_thumbnail!,
            "media_type" : updateSummaryWithMediaRequest.media_type!
        ]
    }
}

