//
//  UpdateSummaryWithoutMediaRequest.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 05/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

class UpdateSummaryWithoutMediaRequest {
    
    public var app_key : String!
    public var channel_big_thumb : String!
    public var channel_max_color : String!
    public var channel_min_color : String!
    public var channel_name : String!
    public var eventCode : String!
    public var skip_media : String!
    public var sessionId : String!
    public var source : String!
    public var type : String?
    
    
    init(app_key: String,  channel_big_thumb: String, channel_max_color: String,channel_min_color: String, channel_name: String,eventCode: String,skip_media:String,sessionId:String,source:String,type:String) {
        self.app_key = app_key
        self.channel_big_thumb = channel_big_thumb
        self.channel_max_color = channel_max_color
        self.channel_min_color = channel_min_color
        self.channel_name = channel_name
        self.eventCode = eventCode
        self.skip_media = skip_media
        self.sessionId = sessionId
        self.source = source
        self.type = type
    }
    
    class func convertToDictionary(updateSummaryWithoutMediaDetails:UpdateSummaryWithoutMediaRequest) -> Dictionary<String, Any> {
        return [
            "app_key": updateSummaryWithoutMediaDetails.app_key!,
            "channel_big_thumb": updateSummaryWithoutMediaDetails.channel_big_thumb!,
            "channel_max_color": updateSummaryWithoutMediaDetails.channel_max_color!,
            "channel_min_color": updateSummaryWithoutMediaDetails.channel_min_color!,
            "channel_name": updateSummaryWithoutMediaDetails.channel_name!,
            "eventCode": updateSummaryWithoutMediaDetails.eventCode!,
            "skip_media": updateSummaryWithoutMediaDetails.skip_media!,
            "sessionId": updateSummaryWithoutMediaDetails.sessionId!,
            "source": updateSummaryWithoutMediaDetails.source!,
            "type" : updateSummaryWithoutMediaDetails.type!,
        ]
    }
}
