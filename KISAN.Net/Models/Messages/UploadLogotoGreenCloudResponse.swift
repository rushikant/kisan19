//
//  UploadLogotoGreenCloudResponse.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 30/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class UploadLogotoGreenCloudResponse: NSObject {
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var imageDetails: [ImageDetailsFromGreenCloud?]
    
    
    
    init( success: Bool?,
          message: String?,
          responseCode: Int?,
          imageDetails: [ImageDetailsFromGreenCloud],
          successRecord: Bool?){
        self.success = success
        self.message =  message
        self.responseCode = responseCode
        self.imageDetails = imageDetails
        
    }
    
    
    class func initializeUploadLogotoGreenCloudResponse() -> UploadLogotoGreenCloudResponse{
        let response = UploadLogotoGreenCloudResponse(success: false, message: "",responseCode: -1,imageDetails:[],successRecord : false)
        return response
    }
    
    
    
    class func uploadLogotoGreenCloudResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> UploadLogotoGreenCloudResponse {
        let response = initializeUploadLogotoGreenCloudResponse()
        response.responseCode = dictionary["responseCode"] as? Int
        response.imageDetails = [ImageDetailsFromGreenCloud.genImageDetailsFromGreenCloud((dictionary["imageDetails"] as? [String:AnyObject])!)]
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as UploadLogotoGreenCloudResponse
    }


}
