//
//  UploadMediaToGreenCloudRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 04/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class UploadMediaToGreenCloudRequest{
    var app_key : String?
    var sessionId : String?
    var type: String?
    var url : String?

    init(  app_key : String?,sessionId : String?,type: String?,url : String?){
        self.app_key = app_key
        self.sessionId  = sessionId
        self.type = type
        self.url  = url
    }
    
    class func convertToDictionary(uploadMediaToGreenCloudRequest:UploadMediaToGreenCloudRequest) -> Dictionary<String, Any> {
        return [
            "app_key": uploadMediaToGreenCloudRequest.app_key!,
            "sessionId": uploadMediaToGreenCloudRequest.sessionId!,
            "type" : uploadMediaToGreenCloudRequest.type!,
            "url" : uploadMediaToGreenCloudRequest.url!,
        ]
    }
    

    
}
