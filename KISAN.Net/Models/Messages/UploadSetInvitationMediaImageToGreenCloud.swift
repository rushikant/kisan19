//
//  UploadSetInvitationMediaImageToGreenCloud.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 03/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class UploadSetInvitationMediaImageToGreenCloud: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var responseCode: Int?
    public var imageDetails: [SetInvitationMediaImageDetails?]
    
    init( success: Bool?,
          message: String?,
          responseCode: Int?,
          imageDetails: [SetInvitationMediaImageDetails],
          successRecord: Bool?){
        self.success = success
        self.message =  message
        self.responseCode = responseCode
        self.imageDetails = imageDetails
        
    }
    
    
    class func initializeUploadSetInvitationMediaImageToGreenCloud() -> UploadSetInvitationMediaImageToGreenCloud{
        let response = UploadSetInvitationMediaImageToGreenCloud(success: false, message: "",responseCode: -1,imageDetails:[],successRecord : false)
        return response
    }
    
    
    
    class func uploadSetInvitationMediaImageToGreenCloudFromDictionary(_ dictionary: [String: AnyObject?]) -> UploadSetInvitationMediaImageToGreenCloud {
        let response = initializeUploadSetInvitationMediaImageToGreenCloud()
        response.responseCode = dictionary["responseCode"] as? Int
        //response.imageDetails = [SetInvitationMediaImageDetails.genImageDetailsFromGreenCloud((dictionary["imageDetails"] as? [String:AnyObject])!)]
        if let imageData = dictionary["imageDetails"] {
            response.imageDetails = [SetInvitationMediaImageDetails.genSetInvitationMediaImageDetails((imageData as? [String:AnyObject])!)]
        }else{
            response.imageDetails = []
        }
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as UploadSetInvitationMediaImageToGreenCloud
    }

}
