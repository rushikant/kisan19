//
//  UserAreaOfInterestResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 01/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class UserAreaOfInterestResponse: NSObject {
    
    public var success: Bool?
    public var message: String?
    public var response_code: Int?
    public var categories_list: [CategoriesList] = []
    
    init(categories_list: [CategoriesList],success: Bool?,
         message: String,
         response_code: Int){
        self.success = success
        self.categories_list = categories_list
        self.message = message
        self.response_code = response_code
    }
    
    
    class func initializeUserAreaOfInterestResponse() -> UserAreaOfInterestResponse {
        let response = UserAreaOfInterestResponse(categories_list: [],success: false, message: "",response_code: -1)
        return response
    }
    
    class func getDictionaryFromUserAreaOfInterestResponse(_ response: UserAreaOfInterestResponse) -> [String: AnyObject?] {
        return response.propertyDictionary()
    }
    
    class func getUserAreaOfInterestResponseFromDictionary(_ dictionary: [String: AnyObject?]) -> UserAreaOfInterestResponse {
        let response = initializeUserAreaOfInterestResponse()
        response.response_code = dictionary["response_code"] as? Int
        let data = dictionary["data"] as? [String: AnyObject?]
        response.categories_list = CategoriesList.getModelFromArray(data!["categories_list"] as! [AnyObject])
        response.success = dictionary["success"] as? Bool
        response.message = dictionary["message"] as? String
        return response as UserAreaOfInterestResponse
    }

}
