//
//  XmppBroadCastMessageReceiveResponse.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import XMPPFramework
class XmppBroadCastMessageReceiveResponse: NSObject {

    class func adminBroadCastMsgReceiveResponse(message:XMPPMessage) -> (Dictionary<String, Any>,String){
        let body: XMLElement? = message
        let messageb: XMLElement? = body?.forName("event", xmlns: "http://jabber.org/protocol/pubsub#event")
        let items:XMLElement? = messageb?.forName("items")
        let item: XMLElement? = (items?.forName("item"))!
        let book: XMLElement = (item!.forName("book", xmlns: "pubsub:test:book"))!
        let title =  String(describing: book.forName("title"))
        let from = String(describing:book.forName("from"))
        let newTitle = String(title.dropFirst(9))
        let finalTitle = String(newTitle.dropLast())
        let  strTitle = finalTitle.replacingOccurrences(of: "(<[^>]+>)", with: "", options: .regularExpression, range: nil)
        let newFrom = String(from.dropFirst(9))
        let finalFrom = String(newFrom.dropLast())
        let  strFrom = finalFrom.replacingOccurrences(of: "(<[^>]+>)", with: "", options: .regularExpression, range: nil)
        let createSendMessageRequest = self.convertToDictionary(text:strTitle )
        return (createSendMessageRequest,strFrom)
    }
    
  class func convertToDictionary(text: String)  -> Dictionary<String, Any> {
        if let data = text.data(using: .utf8) {
            do {
                return (try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>)!
            } catch {
                print(error.localizedDescription)
            }
        }
        return ["":""]
    }
}
