//
//  getGuestListRequest.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 07/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class getGuestListRequest {
  
    var app_key :String?
    var sessionId :String?
    var eventCode :String?
    var source : String?
    var type : String?
    var pagesize : Int?
    var currentpage : Int?
    var search  : String?
    var passtype : String?
    var status : String?
    var filter :[String:AnyObject]


    init( app_key : String?,sessionId :String?,eventCode :String?,source :String?,type :String?,pagesize : Int?,currentpage :Int?,search :String?,passtype : String?,status :String?,filter:[String:AnyObject]){

        self.app_key = app_key
        self.sessionId = sessionId
        self.eventCode = eventCode
        self.source = source
        self.type = type
        self.pagesize = pagesize
        self.currentpage = currentpage
        self.search = search
        self.passtype  = passtype
        self.status = status
        self.filter = filter
    }

    class func convertToDictionary(getGuestListRequest:getGuestListRequest) -> Dictionary<String, Any> {
        return [
            "app_key": getGuestListRequest.app_key!,
            "sessionId": getGuestListRequest.sessionId!,
            "eventCode" : getGuestListRequest.eventCode!,
            "source" : getGuestListRequest.source!,
            "type": getGuestListRequest.type!,
            "pagesize": getGuestListRequest.pagesize!,
            "currentpage": getGuestListRequest.currentpage!,
            "search": getGuestListRequest.search!,
            "passtype" : getGuestListRequest.passtype!,
            "status" : getGuestListRequest.status!,
            "filter" : getGuestListRequest.filter
        ]
    }
    
}
