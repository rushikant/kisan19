//
//  PIPVideoPlayer.swift
//  PiPVideoPlayer
//
//  Created by Alfian Losari on 1/19/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import AVFoundation

class PIPVideoPlayer: NSObject {
    
    private var currentView: UIView!
    public var currentURL: URL?
    private var avplayer: AVPlayer!
    private var avPlayerLayer: AVPlayerLayer!
    public var closeButton: UIButton?
    var videoView: UIView!
    public var playerView: PlayerView!

    private var fullFrame: CGRect? {
        guard let keyWindow = UIApplication.shared.keyWindow else {
            return nil
        }
        
        let width = keyWindow.frame.width / 1.25
        let height = width * 9 / 16
        let fullFrame = CGRect(x: keyWindow.frame.width -  width - 32.0, y: keyWindow.frame.height - height - 32.0, width: width, height: height)
        return fullFrame
    }
    
    func showVideo(url: URL, in view: UIView) {
        //setupVideoView()
        
        if let currentURL = currentURL, currentURL == url {
            return
        }
        
        self.currentURL = url
        self.currentView = view
        
        self.avplayer.pause()
        self.avplayer.replaceCurrentItem(with: AVPlayerItem(url: url))
        self.avplayer.play()
        if self.videoView.isHidden {
            self.showVideoView()
        }
    }
  
    func showVideoUrl(in view: UIView,youTubeId:String){
        setupVideoView(youTubeId:youTubeId)
        self.currentView = view
        if self.videoView.isHidden {
            //hideVideoView()
            self.showVideoView()
        }
    }
    
     func addPlayerView(in view: UIView){
        playerView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        //playerView.bringSubviewToFront(closeButton ?? UIButton())
        playerView.bringSubview(toFront: playerView.btnCloseClick )
        playerView.addSubview(playerView.btnCloseClick)
        view.addSubview(playerView)
    }
    
    private func setupVideoView(youTubeId:String) {

        guard let keyWindow = UIApplication.shared.keyWindow else {
            return
        }
        let videoView = UIView()
        videoView.backgroundColor = .clear
        videoView.layer.cornerRadius = 10.0
        videoView.layer.shadowColor = UIColor.clear.cgColor
        videoView.layer.shadowOpacity = 1
        videoView.layer.shadowOffset = .zero
        videoView.layer.shadowRadius = 5
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.videoId = youTubeId //"14Ixg0crBB4"
        addPlayerView(in: videoView)

        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleDrag(_:)))
        recognizer.maximumNumberOfTouches = 1
        videoView.addGestureRecognizer(recognizer)
        playerView.btnCloseClick.addTarget(self, action: #selector(self.handleCloseTapped(_:)), for: .touchUpInside)
        videoView.isHidden = true
        if currentView != nil{
            self.videoView.isHidden = true
            self.playerView.ytPlayerView.stopVideo()
            self.currentURL = nil
            self.videoView.removeFromSuperview()
            keyWindow.addSubview(videoView)
            self.videoView = videoView
        }else{
            keyWindow.addSubview(videoView)
            self.videoView = videoView
        }

    }
    
    func hideCloseButton(){
         playerView.btnCloseClick?.isHidden = true
         playerView.btnCloseClick?.removeFromSuperview()
    }
    
    private func showVideoView(completion: ((Bool) -> Void)? = nil) {
        guard let fullFrame = self.fullFrame else {
            return
        }
        self.videoView.isHidden = false
        self.videoView.frame = CGRect(x: 10, y: 10, width: 10, height: 10)
         playerView.btnCloseClick.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.videoView.frame = fullFrame
            self.playerView.btnCloseClick.alpha = 1.0
        }, completion: completion)
    }
    
    private func hideVideoView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.videoView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.playerView.btnCloseClick.alpha = 0
            self.playerView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        }, completion: { (_) in
            self.videoView.isHidden = true
            self.playerView.ytPlayerView.stopVideo()
            self.currentURL = nil
        })
        guard let rootVC = UIApplication.shared.delegate?.window??.rootViewController else {
            return
        }
        rootVC.presentedViewController?.dismiss(animated: false, completion: nil)
    }
    
    @objc func handleCloseTapped(_ sender: Any) {
        self.hideVideoView()
    }
    
    @objc func handleDrag(_ sender: UIPanGestureRecognizer) {
        let screenBounds = UIScreen.main.bounds
        
        let translation = sender.translation(in: videoView)
        videoView.center = CGPoint(x: videoView.center.x + translation.x, y: videoView.center.y + translation.y)
        sender.setTranslation(CGPoint(x: 0, y: 0), in: videoView)
        
        if sender.state == .ended {
            var finalPoint = CGPoint(x: 0, y: 0)
            
            finalPoint.x = max(32 + videoView.frame.size.width, min(videoView.frame.maxX, screenBounds.size.width - 32.0))
            finalPoint.y = max(32 + videoView.frame.size.height ,min(videoView.frame.maxY, screenBounds.size.height - 32.0))
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.videoView.frame.origin = CGPoint(x: finalPoint.x - self.videoView.frame.size.width, y: finalPoint.y - self.videoView.frame.size.height)
            })
        }
    }
    
}
