//
//  PavilionFilterService.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 03/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class PavilionFilterService: Repository {

    func getPavilionFilterlist(_ baseUrl: String,
                     postData: [String: AnyObject],
                     withSuccessHandler success: CompleteionHandler?,
                     withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetPavilionFilterList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetPavilionFilterList {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? String == "true" {
                    var userAreaRespo: PavilionFilterResponse = PavilionFilterResponse.initializePavilionFilterResponse()
                    userAreaRespo = PavilionFilterResponse.getUserAreaOfInterestResponseFromDictionary(responseDict)
                    print("userAreaRespo",userAreaRespo)
                    if let success = self.completeionBlock {
                        success(userAreaRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
    
    
    
}
