
//  PostMesaageViewCell.swift
//  KISAN.Net
//  Created by KISAN TEAM on 16/01/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.

import UIKit

class PostMesaageViewCell: UITableViewCell {
    @IBOutlet var txtviewOfPostDescription: UITextView!
    @IBOutlet var ViewOncell: UIView!
    @IBOutlet var lblOfDateTime: UILabel!
    @IBOutlet var lblPOstTitl: UILabel!
    @IBOutlet var imgpostimage: UIImageView!
    @IBOutlet var imgChannelProfile: UIImageView!
    @IBOutlet weak var lblOfChannelName: UILabel!
    @IBOutlet weak var videIcon: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet var btnPromotePost: UIButton!
    @IBOutlet var lblsponsored: UILabel!
    @IBOutlet weak var heightConstantOfBtnPromoPost: NSLayoutConstraint!
    @IBOutlet weak var lblOfSeprator: UILabel!
    @IBOutlet var heightConstraintOfDescriptionView: NSLayoutConstraint!
        @IBOutlet weak var HeightConstraintForPostimage: NSLayoutConstraint!
    @IBOutlet var heightConstraintOfLblSponsored: NSLayoutConstraint!
    @IBOutlet var SpaceConstraintbetweenImageAndDesc: NSLayoutConstraint!
    @IBOutlet weak var heightOfViewOfBackRequestCallBack: NSLayoutConstraint!
    @IBOutlet weak var viewOfBackRequestCallBack: UIView!
    @IBOutlet weak var btnRequestCallBack: UIButton!
    
    @IBOutlet weak var lblOfThankYou: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var imgViewOfRequestCallBackIcon: UIImageView!
    
    @IBOutlet weak var lblRequestCallBackTitle: UILabel!
    
    @IBOutlet weak var lblRequestCallBackDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
