//
//  BrochureDetailsViewController.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD

class BrochureDetailsViewController: UIViewController {
    
    @IBOutlet weak var exhibitorSmallThubnail: UIImageView!
    @IBOutlet weak var exhibitorName: UILabel!
    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var mediaName: UILabel!
    @IBOutlet weak var mediaType: UIImageView!
    @IBOutlet weak var followHint: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var mediaButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    
    var brochureObject: ExihibitorScanBrochure!
    var videoPlayer = PIPVideoPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationbar()
        self.setupInitialView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.containerView.layer.cornerRadius = 10.0
        self.containerView.layer.shadowColor = UIColor.clear.cgColor
        self.containerView.layer.shadowOpacity = 1
        self.containerView.layer.shadowOffset = .zero
        self.containerView.layer.shadowRadius = 5
    }
    
    func setupNavigationbar() {
        let backButton = UIButton(type:.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let listButton = UIButton(type:.custom)
        listButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        listButton.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        backButton.contentHorizontalAlignment = .right
        listButton.addTarget(self, action: #selector(listAction), for: .touchUpInside)
        
        let back = UIBarButtonItem(customView: backButton)
        let backText = UIBarButtonItem(title: "scanned_result".localized, style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItems = [back, backText]
        
        let list = UIBarButtonItem(customView: listButton)
        let listText = UIBarButtonItem(title: "list".localized, style: .plain, target: self, action: #selector(listAction))
        self.navigationItem.rightBarButtonItems = [list, listText]
        
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    
    func setupInitialView() {
        
        //set Logo
        if self.brochureObject != nil {
            let urlString = self.brochureObject.community_logo_url!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            self.exhibitorSmallThubnail.sd_setImage(with: URL(string:urlString!), placeholderImage:#imageLiteral(resourceName: "defaultChannel"))
            self.exhibitorSmallThubnail.layer.cornerRadius = self.exhibitorSmallThubnail.frame.size.width / 2
            self.exhibitorSmallThubnail.layer.borderColor = UIColor.lightGray.cgColor
            self.exhibitorSmallThubnail.layer.borderWidth = 1
            self.exhibitorSmallThubnail.clipsToBounds = true
            
            
            self.exhibitorName.font = UIFont.boldSystemFont(ofSize: 18)
            self.exhibitorName.textColor = UIColor.darkGray
            self.exhibitorName.text = self.brochureObject.community_name!
            
            self.mediaName.font = UIFont.systemFont(ofSize: 16)
            self.mediaName.textColor = UIColor.darkGray
                 
            let mediaString = self.brochureObject.media_thumbnail!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            self.mediaImage.sd_setImage(with: URL(string:mediaString!), placeholderImage:UIImage())
            
            self.followHint.font = UIFont.systemFont(ofSize: 14)
            self.followHint.textColor = UIColor.black
            var follow_hint = "follow_hint".localized
            follow_hint = follow_hint.replacingOccurrences(of: "{{VALUE}}", with: self.brochureObject.community_name!)
            self.followHint.text = follow_hint
            
            let scanButtonTitle = "scan_more_brochures".localized
            let textRange = NSMakeRange(0, scanButtonTitle.count)
            let attributedText = NSMutableAttributedString(string: scanButtonTitle)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
            
            self.seeMoreButton.setAttributedTitle(attributedText, for: .normal)
            self.seeMoreButton.setTitleColor(UIColor.init(hexString: ColorConstants.colorPrimaryString), for: .normal)
            self.seeMoreButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
            self.seeMoreButton.addTarget(self, action: #selector(seeMoreBrochureActions), for: .touchUpInside)
            
            self.downloadButton.addTarget(self, action: #selector(downloadMedia), for: .touchUpInside)
            
            switch self.brochureObject.media_type {
            case StringConstants.MediaType.document:
                    self.mediaType.image = #imageLiteral(resourceName: "Document small")
                    self.mediaButton.addTarget(self, action: #selector(openDocument), for: .touchUpInside)
                    self.mediaButton.setImage(UIImage(), for: .normal)
                    let theFileName = (self.brochureObject.media_link! as NSString).lastPathComponent
                    self.mediaName.text = theFileName
                    self.downloadButton.isHidden = false
                case StringConstants.MediaType.image:
                    self.mediaType.image = #imageLiteral(resourceName: "upload_image_gray")
                    self.mediaButton.addTarget(self, action: #selector(openImage), for: .touchUpInside)
                    self.mediaButton.setImage(UIImage(), for: .normal)
                    let theFileName = (self.brochureObject.media_link! as NSString).lastPathComponent
                    self.mediaName.text = theFileName
                    self.downloadButton.isHidden = false
                case StringConstants.MediaType.externallink:
                    self.mediaType.image = #imageLiteral(resourceName: "upload_video_gray")
                    self.mediaButton.addTarget(self, action: #selector(openVideo), for: .touchUpInside)
                    self.mediaButton.setImage(#imageLiteral(resourceName: "ic_play_circle_outline_white"), for: .normal)
                    self.mediaName.text = self.brochureObject.media_link!
                    self.downloadButton.isHidden = true
                default:
                    self.mediaType.image = #imageLiteral(resourceName: "upload_image_gray")
                    self.mediaButton.setImage(UIImage(), for: .normal)
                    self.downloadButton.isHidden = true
            }
        }
    }
    
    @objc func openImage() {
        let mediaCtrl = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "MediaViewerController") as! MediaViewerController
        mediaCtrl.brochureObject = brochureObject
        let navigationController = UINavigationController(rootViewController: mediaCtrl)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //@objc func openVideo()
    @objc func openVideo(){
        let mediaString = self.brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        if let youTubeId = self.getYoutubeId(youtubeUrl:mediaString!) {
            //call API
            videoPlayer.showVideoUrl(in: self.view,youTubeId:youTubeId)
            videoPlayer.closeButton?.addTarget(self, action: #selector(closePlayer), for: .touchUpInside)
            videoPlayer.playerView.btnCloseClick.addTarget(self, action: #selector(closePlayer), for: .touchUpInside)
            videoPlayer.playerView.btnFullScreenClicked.addTarget(self, action: #selector(reopenPIP), for: .touchUpInside)
        }
    }
    
    @objc func closePlayer() {
        videoPlayer.playerView.removeFromSuperview()
    }
    
    @objc func reopenPIP() {
        self.closePlayer()
        self.openVideo()
    }
    
    @objc func openDocument() {
        let mediaCtrl = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "MediaViewerController") as! MediaViewerController
        mediaCtrl.brochureObject = brochureObject
        let navigationController = UINavigationController(rootViewController: mediaCtrl)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        let url = youtubeUrl
        var youtubrID = String()
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            if ((match) != nil){
                let range = match?.range(at: 0)
                youtubrID = (url as NSString).substring(with: range!)
                print(youtubrID)
            }else{
                youtubrID = StringConstants.EMPTY
            }
        } catch {
            youtubrID = StringConstants.EMPTY
        }
        return youtubrID
    }
    
}

// Actions on buttons
extension BrochureDetailsViewController {
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func listAction() {
        let brochureList = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "BrochureListViewController") as! BrochureListViewController
        self.navigationController?.pushViewController(brochureList, animated: true)
    }
    
    @objc func seeMoreBrochureActions() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func downloadMedia() {
        let urlString = self.brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString!)
        
        switch brochureObject.media_type {
        case StringConstants.MediaType.document:
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            FileDownloader.loadFileAsync(url: url!) { (path, error) in
                if error != nil {
                    let ac = UIAlertController(title: "Download error", message: error?.localizedDescription, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                    MBProgressHUD.hide(for: self.view, animated: true);
                } else {
                    print("path: \(path ?? "invalid")")
                    let localUrl = URL(string: path!)
                    let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let destinationUrl = documentsUrl.appendingPathComponent(localUrl!.lastPathComponent)
                    
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: destinationUrl.path){
                        DispatchQueue.main.async {
                            let documento = NSData(contentsOfFile: destinationUrl.path)
                            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView=self.view
                            self.present(activityViewController, animated: true, completion: nil)
                        }
                    }
                    else {
                        print("document was not found")
                    }
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true);
                    }
                    
                }
            }
        case StringConstants.MediaType.image:
            UIImageWriteToSavedPhotosAlbum(mediaImage.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        case StringConstants.MediaType.externallink: break
            
        default: break
            //do nothing
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            let ac = UIAlertController(title: "Download error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Image Downloaded!", message: "image_download_success_message".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
}
