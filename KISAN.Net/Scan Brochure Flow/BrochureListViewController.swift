//
//  BrochureListViewController.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import Photos

class BrochureListViewController: UIViewController {
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var syncButton: UIButton!
    
    var isFetchingData = false
    var page = 0
    var brochureList = [ExihibitorScanBrochure]()
    var unsyncedBrochures = [ScanBrochureRequest]()
    
    var isSearching = false
    var searchPage = 0
    var isSearchingData = false
    var searchedBrochureList = [ExihibitorScanBrochure]()
    var videoPlayer = PIPVideoPlayer()
    var isEventOngoing = true
    var reachability: Reachability!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupObservers()
        self.setupNavigationbar()
        self.setupView()
        self.checkForUnSyncedRecords()
    }
    
    func setupObservers() {
        self.reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(internetIsAvailable), name: NSNotification.Name.reachabilityChanged, object: nil)
    }
    
    func setupNavigationbar() {
        
        var title = "list_record_values".localized
        if self.isSearching {
            title = title.replacingOccurrences(of: "{{VALUE}}", with: "\(self.searchedBrochureList.count)")
        } else {
            title = title.replacingOccurrences(of: "{{VALUE}}", with: "\(self.brochureList.count)")
        }
        self.backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        self.backButton.setTitle("  \(title)", for: .normal)
        self.backButton.contentHorizontalAlignment = .left
        self.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        if self.isEventOngoing {
            self.listButton.setImage(#imageLiteral(resourceName: "barcode"), for: .normal)
            self.listButton.setTitle("  \("scan".localized)", for: .normal)
            self.listButton.contentHorizontalAlignment = .right
            self.listButton.addTarget(self, action: #selector(listAction), for: .touchUpInside)
            
            self.syncButton.setImage(#imageLiteral(resourceName: "refreshIcon"), for: .normal)
            self.syncButton.setTitle("  \("sync".localized)", for: .normal)
            self.syncButton.contentHorizontalAlignment = .right
            self.syncButton.addTarget(self, action: #selector(checkForUnSyncedRecords), for: .touchUpInside)
        } else {
             self.listButton.isHidden = true
             //self.syncButton.isHidden = true
            self.syncButton.addTarget(self, action: #selector(checkForUnSyncedRecords), for: .touchUpInside)

        }
    }
    
    func setupView() {
        self.searchBar.placeholder = "brochurelist_searchbar_placeholder".localized
        self.searchBar.delegate = self
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func internetIsAvailable() {
        self.checkForUnSyncedRecords()
    }
}

// UISearchBar delegate
extension BrochureListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.searchBar.text!.isEmpty {
            self.isEditing = false
            self.searchBar.resignFirstResponder()
            self.stopSearch()
            self.tableView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool  {
        var searchString = ""
        if text.isEmpty {
            searchString = String(searchString.dropLast())
        } else {
            searchString = searchBar.text! + text
        }
        self.isSearching = true
        self.searchedBrochureList.removeAll()
        searchString = searchString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.searchPage = 0
        self.searchBrochures(searchString)
        self.tableView.reloadData()
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.stopSearch()
        self.tableView.reloadData()
    }
    
    func stopSearch() {
        self.searchPage = 0
        self.searchBar.text = ""
        self.isSearching = false
        self.isSearchingData = false
        self.searchedBrochureList.removeAll()
        self.setupNavigationbar()
    }
}

// API Calls
extension BrochureListViewController {
    
    func getBrochures() {
        if self.page == 0 {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
        }
        self.isFetchingData = true
        
        let app_key = URLConstant.app_key
        let eventCode = URLConstant.eventCode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let request = BrochureListRequest(app_key: app_key, eventCode: eventCode, sessionId: sessionId, pagesize: 10, currentpage: self.page, search: "")
        let param = BrochureListRequest.convertToDictionary(request: request)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getScannedBrochures
        
        _ = ScanExhibitorBrochureService().getScannedBrochuersByvisitor(apiURL, postData: param as [String : AnyObject], withSuccessHandler: { (records) in
            if (records as! [ExihibitorScanBrochure]).count == 0 {
                self.isFetchingData = true
            } else {
                self.brochureList.append(contentsOf: records as! [ExihibitorScanBrochure])
                self.tableView.reloadData()
                self.page = self.page + 1
                self.isFetchingData = false
            }
            self.setupNavigationbar()
            MBProgressHUD.hide(for: self.view, animated: true);
        }, withFailureHandlere: { (error) in
            MBProgressHUD.hide(for: self.view, animated: true);
        })
    }
    
    func searchBrochures(_ searchString: String) {
        if self.searchPage == 0 {
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            self.searchedBrochureList.removeAll()
        }
        self.isSearchingData = true
        
        let app_key = URLConstant.app_key
        let eventCode = URLConstant.eventCode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let request = BrochureListRequest(app_key: app_key, eventCode: eventCode, sessionId: sessionId, pagesize: 10, currentpage: self.searchPage, search: searchString)
        let param = BrochureListRequest.convertToDictionary(request: request)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.getScannedBrochures
        _ = ScanExhibitorBrochureService().getScannedBrochuersByvisitor(apiURL, postData: param as [String : AnyObject], withSuccessHandler: { (records) in
            MBProgressHUD.hide(for: self.view, animated: true);
            if (records as! [ExihibitorScanBrochure]).count == 0 {
                self.isSearchingData = true
            } else {
                self.searchedBrochureList.append(contentsOf: records as! [ExihibitorScanBrochure])
                self.searchPage = self.searchPage + 1
                self.isSearchingData = false
                self.tableView.reloadData()
                
            }
            self.setupNavigationbar()
            MBProgressHUD.hide(for: self.view, animated: true);
        }, withFailureHandlere: { (error) in
            MBProgressHUD.hide(for: self.view, animated: true);
        })
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.isSearching {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                if self.isSearchingData == true {
                    
                } else {
                    self.isSearchingData = true
                    self.searchBrochures(self.searchBar!.text!)
                }
            }
        } else {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                if self.isFetchingData == true {
                    
                } else {
                    self.isFetchingData = true
                    self.getBrochures()
                }
            }
        }
    }
}

//TableviewDelegate and Data source methods
extension BrochureListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = .clear
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.unsyncedBrochures.count
        } else {
            if self.isSearching {
                return self.searchedBrochureList.count
            } else {
                return self.brochureList.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let brochure:ScanBrochureRequest = self.unsyncedBrochures[indexPath.row]
            let data:ScanData = brochure.data![0]
            var cell: BrochureUnsyncedCell? = nil
            cell = self.tableView.dequeueReusableCell(withIdentifier: "BrochureUnsyncedCell") as? BrochureUnsyncedCell
            if cell == nil {
                let cellnib = BrochureUnsyncedCell(style: .default, reuseIdentifier: "BrochureUnsyncedCell") as BrochureUnsyncedCell
                cell = cellnib
            }
            cell?.exhibitorName.text = data.shortcode
            cell?.selectionStyle = .none
            return cell!
        } else {
            var brochure: ExihibitorScanBrochure!
            if self.isSearching {
                brochure = self.searchedBrochureList[indexPath.row]
            } else {
                brochure = self.brochureList[indexPath.row]
            }
            
            var cell: BrochureListCell? = nil
            cell = self.tableView.dequeueReusableCell(withIdentifier: "BrochureListCell") as? BrochureListCell
            if cell == nil {
                let cellnib = BrochureListCell(style: .default, reuseIdentifier: "BrochureListCell") as BrochureListCell
                cell = cellnib
            }
            cell?.setupCell(brochure: brochure)
            cell?.mediaButton.tag = indexPath.row
            cell?.mediaButton.addTarget(self, action: #selector(openMedia(sender:)), for: .touchUpInside)
            cell?.downloadButton.tag = indexPath.row
            cell?.downloadButton.addTarget(self, action: #selector(downloadFile(sender:)), for: .touchUpInside)
            cell?.selectionStyle = .none
            return cell!
        }
    }
}

extension BrochureListViewController {
    
    @objc func openMedia(sender:UIButton) {
        let tag = sender.tag
        let brochureObject: ExihibitorScanBrochure!
        if self.isSearching {
            brochureObject = self.searchedBrochureList[tag]
        } else {
            brochureObject = self.brochureList[tag]
        }
        
        switch brochureObject.media_type {
            case StringConstants.MediaType.document:
                self.openDocument(brochureObject: brochureObject)
            case StringConstants.MediaType.image:
                self.openImage(brochureObject: brochureObject)
            case StringConstants.MediaType.externallink:
                self.openVideo(brochureObject: brochureObject)
            default: break
                //do nothing
        }
    }
    
    @objc func openImage(brochureObject: ExihibitorScanBrochure) {
        let mediaCtrl = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "MediaViewerController") as! MediaViewerController
        mediaCtrl.brochureObject = brochureObject
        let navigationController = UINavigationController(rootViewController: mediaCtrl)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //@objc func openVideo()
    @objc func openVideo(brochureObject: ExihibitorScanBrochure) {
        let urlString = brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let youTubeId = self.getYoutubeId(youtubeUrl: urlString!) {
            //call API
            videoPlayer.showVideoUrl(in: self.view,youTubeId:youTubeId)
            videoPlayer.closeButton?.addTarget(self, action: #selector(closePlayer), for: .touchUpInside)
            videoPlayer.playerView.btnCloseClick.addTarget(self, action: #selector(closePlayer), for: .touchUpInside)
            //videoPlayer.playerView.btnFullScreen.addTarget(self, action: #selector(reopenPIP), for: .touchUpInside)
            videoPlayer.playerView.btnFullScreenClicked.addTarget(self, action: #selector(reopenPIP), for: .touchUpInside)
        }
    }
    
    @objc func closePlayer() {
        videoPlayer.playerView.removeFromSuperview()
    }
    
    @objc func reopenPIP() {
        self.closePlayer()
    }
    
    @objc func openDocument(brochureObject: ExihibitorScanBrochure) {
        let mediaCtrl = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "MediaViewerController") as! MediaViewerController
        mediaCtrl.brochureObject = brochureObject
        let navigationController = UINavigationController(rootViewController: mediaCtrl)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        let url = youtubeUrl
        var youtubrID = String()
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            if ((match) != nil){
                let range = match?.range(at: 0)
                youtubrID = (url as NSString).substring(with: range!)
                print(youtubrID)
            }else{
                youtubrID = StringConstants.EMPTY
            }
        } catch {
            youtubrID = StringConstants.EMPTY
        }
        return youtubrID
    }
    
    
    @objc func downloadFile(sender: UIButton) {
        let tag = sender.tag
        let brochureObject: ExihibitorScanBrochure!
        if self.isSearching {
            brochureObject = self.searchedBrochureList[tag]
        } else {
            brochureObject = self.brochureList[tag]
        }
        let mediaString = brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: mediaString!)
        
        switch brochureObject.media_type {
            case StringConstants.MediaType.document:
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                
                FileDownloader.loadFileAsync(url: url!) { (path, error) in
                    if error != nil {
                        let ac = UIAlertController(title: "Download error", message: error?.localizedDescription, preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                        MBProgressHUD.hide(for: self.view, animated: true);
                    } else {
                        print("path: \(path ?? "invalid")")
                        //                        DispatchQueue.main.async {
                        //                            FileDownloader.copyDocumentsToiCloudDirectory(file: path!)
                        //                        }
                        let localUrl = URL(string: path!)
                        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        let destinationUrl = documentsUrl.appendingPathComponent(localUrl!.lastPathComponent)
                        
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: destinationUrl.path){
                            
                            DispatchQueue.main.async {
                                let documento = NSData(contentsOfFile: destinationUrl.path)
                                let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
                                activityViewController.popoverPresentationController?.sourceView=self.view
                                self.present(activityViewController, animated: true, completion: nil)
                            }
                        }
                        else {
                            print("document was not found")
                        }
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                    }
                }
            case StringConstants.MediaType.image:
                let cell = self.tableView.cellForRow(at: IndexPath(row: tag, section: 1)) as? BrochureListCell
                UIImageWriteToSavedPhotosAlbum((cell?.mediaImage.image!)!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                
            case StringConstants.MediaType.externallink: break
                
            default: break
                //do nothing
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            let ac = UIAlertController(title: "Download error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Image Downloaded!", message: "image_download_success_message".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
}

// Actions on buttons
extension BrochureListViewController {
    @objc func backAction() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    @objc func listAction() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

//offline operations
extension BrochureListViewController {
    @objc func checkForUnSyncedRecords(){
        self.unsyncedBrochures = UserDefaults.getUnsyncedBrochureRecord()
        if self.unsyncedBrochures.count > 0 {
            if isInternetAvailableHelper.isInternetAvailable() {
                self.callunsyncedData()
            } else {
                self.tableView.reloadData()
            }
        } else {
            if self.brochureList.count == 0 {
                self.getBrochures()
            }
        }
    }
    
    func callunsyncedData() {
        
        let request:ScanBrochureRequest = UserDefaults.getUnsyncedSubmissionRequest()
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        print("scanRequest: \(request)")
        let params = ScanBrochureRequest.convertToDictionary(request: request)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.scanBrochureCode
        
        _ = ScanExhibitorBrochureService().scanExhibitorBarcode(apiURL,postData: params as [String : AnyObject], withSuccessHandler: { (scannedObject) in
            MBProgressHUD.hide(for: self.view, animated: true);
            if (scannedObject as! [AnyObject]).count > 0 {
                
                for dict in (scannedObject as! [[String: Any]]) {
                    if dict.keys.contains("data") {
                        //invalid code
                    } else {
                        let array : [ExihibitorScanBrochure] = ExihibitorScanBrochure.getModelFromArray([dict] as [AnyObject])
                        print("array: \(array)")
                        if array.count > 0 {
                            let brochure = array[0]
                            self.getProfileData(channelKey: brochure.community_key!)
                        } else {
                            //self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                        
                        }
                    }
                }
                if UserDefaults.deleteUnsyncedBrochureRecord() {
                    self.unsyncedBrochures.removeAll()
                }
            }
            if self.brochureList.count == 0 {
                self.getBrochures()
            }
        }, withFailureHandlere: { (error: String) in
            MBProgressHUD.hide(for: self.view, animated: true);
            print("error",error)
            if self.brochureList.count == 0 {
                self.getBrochures()
            }
        })
    }
    
    func getProfileData(channelKey:String){
        let params = ["":""]
        let currentTime = DateTimeHelper.getCurrentMillis()
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.getChannelProfile + channelKey + URLConstant.getChannelProfileTimeStamp + String(currentTime)
        
        print("apiURL",apiURL)
        _ = ChannelProfileService().getChannelProfile(apiURL,
                                                      postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let communityDetails = userModel as! CommunityDetails
                                                        //communityDetails.communityJabberId
                                                        DispatchQueue.main.async {
                                                            self.followChannelApi(communityKey: channelKey, communityJabberId: communityDetails.communityJabberId!,communityDetails:communityDetails)
                                                        }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))
                ], actions:[{action1 in
                    },{action2 in
                    }, nil])
        })
        
    }
    
    func followChannelApi(communityKey:String,communityJabberId:String,communityDetails:CommunityDetails){
        updateIsMemberInLocalDB(communityKey: communityKey, CommunityDetails: communityDetails)
        
        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
                self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])}
        })
        
    }
    
    func updateIsMemberInLocalDB(communityKey:String,CommunityDetails:CommunityDetails){
        
        let  dict1  = ConvertToDictOfChannelProfileObj.convertToDictOfChannelProfileObj(myChatDetails: [CommunityDetails])
        let  myChatListRespo = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict1)
        var myChats = [MyChatsTable]()
        myChats.append(myChatListRespo)
        //---------------- Update isMember  value as true for follow channel & also at database----------//
        myChats.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
        myChats.filter({$0.communityKey == communityKey}).first?.messageText = NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
        
        var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: myChats)
        CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)
        let currentTime = DateTimeHelper.getCurrentMillis()
        dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
        dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
        dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
        
        let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
        if checkCommunityKeyWithisNormalMychatFlag.count == 0{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
            dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
            MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
        }else{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
        }
        
    }
}

//List cell
class BrochureListCell: UITableViewCell {
    
    @IBOutlet weak var exhibitorSmallThubnail: UIImageView!
    @IBOutlet weak var exhibitorName: UILabel!
    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var mediaTypeImage: UIImageView!
    @IBOutlet weak var mediaName: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mediaButton: UIButton!
    
    private var shadowLayer: CAShapeLayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.exhibitorName.font = UIFont.boldSystemFont(ofSize: 16)
        self.exhibitorName.textColor = UIColor.darkGray
        
        self.mediaName.font = UIFont.systemFont(ofSize: 14)
        self.mediaName.textColor = UIColor.darkGray
    }
    
    func setupCell(brochure:ExihibitorScanBrochure) {
        let urlString = brochure.community_logo_url!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.exhibitorSmallThubnail.sd_setImage(with: URL(string:urlString!), placeholderImage:#imageLiteral(resourceName: "defaultChannel"))
        self.exhibitorName.text = brochure.community_name!
        
        let mediaString = brochure.media_thumbnail!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.mediaImage.sd_setImage(with: URL(string:mediaString!), completed: nil)
        
        switch brochure.media_type {
            case StringConstants.MediaType.document:
                self.mediaTypeImage.image = #imageLiteral(resourceName: "Document small")
                self.mediaButton.setImage(UIImage(), for: .normal)
                let theFileName = (brochure.media_link! as NSString).lastPathComponent
                self.mediaName.text = theFileName
                self.downloadButton.isHidden = false
            case StringConstants.MediaType.image:
                self.mediaTypeImage.image = #imageLiteral(resourceName: "upload_image_gray")
                self.mediaButton.setImage(UIImage(), for: .normal)
                let theFileName = (brochure.media_link! as NSString).lastPathComponent
                self.mediaName.text = theFileName
                self.downloadButton.isHidden = false
            case StringConstants.MediaType.externallink:
                self.mediaTypeImage.image = #imageLiteral(resourceName: "upload_video_gray")
                self.mediaButton.setImage(#imageLiteral(resourceName: "ic_play_circle_outline_white"), for: .normal)
                self.mediaName.text = brochure.media_link!
                self.downloadButton.isHidden = true
            default:
                self.mediaTypeImage.image = #imageLiteral(resourceName: "upload_image_gray")
                self.mediaButton.setImage(UIImage(), for: .normal)
                self.mediaName.text = brochure.media_link!
                self.downloadButton.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.exhibitorSmallThubnail.layer.cornerRadius = self.exhibitorSmallThubnail.frame.size.width / 2
        self.exhibitorSmallThubnail.layer.borderColor = UIColor.lightGray.cgColor
        self.exhibitorSmallThubnail.layer.borderWidth = 1
        self.exhibitorSmallThubnail.clipsToBounds = true
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: CGRect(x: self.containerView.frame.origin.x, y: self.containerView.frame.origin.y + 1, width: self.containerView.bounds.size.width, height: self.containerView.bounds.size.height + 1), cornerRadius: 6).cgPath
            shadowLayer.fillColor = UIColor.white.cgColor
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width:1, height:1)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 6
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
}


class BrochureUnsyncedCell: UITableViewCell {
    
    @IBOutlet weak var exhibitorSmallThubnail: UIImageView!
    @IBOutlet weak var exhibitorName: UILabel!
    @IBOutlet weak var mediaName: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    private var shadowLayer: CAShapeLayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.exhibitorName.font = UIFont.systemFont(ofSize: 16)
        self.exhibitorName.textColor = UIColor.red
        
        self.mediaName.font = UIFont.systemFont(ofSize: 14)
        self.mediaName.textColor = UIColor.black
        self.mediaName.text = "unsynced_hint".localized
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.exhibitorSmallThubnail.layer.cornerRadius = self.exhibitorSmallThubnail.frame.size.width / 2
        self.exhibitorSmallThubnail.layer.borderColor = UIColor.clear.cgColor
        self.exhibitorSmallThubnail.layer.borderWidth = 1
        self.exhibitorSmallThubnail.clipsToBounds = true
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: CGRect(x: self.containerView.frame.origin.x, y: self.containerView.frame.origin.y + 1, width: self.containerView.bounds.size.width, height: self.containerView.bounds.size.height + 1), cornerRadius: 6).cgPath
            shadowLayer.fillColor = UIColor.white.cgColor
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width:1, height:1)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 6
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
}

class FileDownloader {
    
//    static func loadFileSync(url: NSURL, completion:(_ path:String, _ error:NSError?) -> Void) {
//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent!)
//        if FileManager().fileExists(atPath: (destinationUrl?.path)!) {
//            completion((destinationUrl?.path)!, nil)
//        } else if let dataFromURL = NSData(contentsOf: url as URL){
//            if dataFromURL.write(to: destinationUrl!, atomically: true) {
//                completion(destinationUrl?.path ?? "", nil)
//            } else {
//                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
//                completion(destinationUrl!.path, error)
//            }
//        } else {
//            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
//            completion(destinationUrl?.path ?? "", error)
//        }
//    }
    
    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
    
    //Create iCloud Drive directory
    static func createDirectory() -> Bool {
        if isICloudContainerAvailable() {
            if let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents") {
                if (!FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: nil)) {
                    do {
                        try FileManager.default.createDirectory(at: iCloudDocumentsURL, withIntermediateDirectories: true, attributes: nil)
                        return true
                    }
                    catch {
                        //Error handling
                        print("Error in creating doc")
                        return false
                    }
                } else {
                    return true
                }
            } else {
                return false
            }
        }
        else {
            if let topController = UIApplication.topViewController() {
                let ac = UIAlertController(title: "Unable to store file", message: "You are not logged into iCloud".localized, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                topController.present(ac, animated: true)
            }
            return false
        }
        return false
    }
    
    
    //Copy files from local directory to iCloud Directory
    static func copyDocumentsToiCloudDirectory(file: String) {
        
        if FileDownloader.createDirectory() {
            if isICloudContainerAvailable() {
                guard let localDocumentsURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: .userDomainMask).last else { return }
                let localUrl = URL(string: file)
                let fileURL = localDocumentsURL.appendingPathComponent(localUrl!.lastPathComponent)
                
                guard let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents").appendingPathComponent(fileURL.lastPathComponent) else { return }
                
                var isDir:ObjCBool = false
                
                if FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: &isDir) {
                    do {
                        try FileManager.default.removeItem(at: iCloudDocumentsURL)
                    }
                    catch {
                        //Error handling
                        if let topController = UIApplication.topViewController() {
                            MBProgressHUD.hide(for: topController.view, animated: true);
                            let ac = UIAlertController(title: "Download error", message: "Unable to store the document", preferredStyle: .alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .default))
                            topController.present(ac, animated: true)
                        }
                    }
                }
                
                do {
                    try FileManager.default.copyItem(at: fileURL, to: iCloudDocumentsURL)
                    if let topController = UIApplication.topViewController() {
                        MBProgressHUD.hide(for: topController.view, animated: true);
                        let ac = UIAlertController(title: "Document Downloaded!", message: "document_download_success_message".localized, preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        topController.present(ac, animated: true)
                    }
                }
                catch {
                    //Error handling
                    if let topController = UIApplication.topViewController() {
                        MBProgressHUD.hide(for: topController.view, animated: true);
                        let ac = UIAlertController(title: "Download error", message: "Unable to store the document", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        topController.present(ac, animated: true)
                    }
                }
            }
            else {
                //Message Not logged into iCloud
                if let topController = UIApplication.topViewController() {
                    MBProgressHUD.hide(for: topController.view, animated: true);
                    let ac = UIAlertController(title: "Unable to store file", message: "You are not logged into iCloud".localized, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    topController.present(ac, animated: true)
                }
            }
        }
    }
    
    
    //To check user logged in to iCloud
   static  func isICloudContainerAvailable() -> Bool {
        if FileManager.default.ubiquityIdentityToken != nil {
            return true
        }
        else {
            return false
        }
    }
}

