//
//  PDFViewerController.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import WebKit

class MediaViewerController: UIViewController {
    
    var brochureObject: ExihibitorScanBrochure!
    var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupNavigationBar(title:String) {
        let backButton = UIButton(type:.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        backButton.setImage(#imageLiteral(resourceName: "ic_navigate_before_white_36pt"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        backButton.addTarget(self, action: #selector(cancelView), for: .touchUpInside)
        let back = UIBarButtonItem(customView: backButton)
        let backText = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(cancelView))
        self.navigationItem.leftBarButtonItems = [back, backText]
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    
    func setupView() {
        switch self.brochureObject.media_type {
            case StringConstants.MediaType.document:
                self.setupNavigationBar(title:StringConstants.MediaType.pdf.uppercased())
                self.showDocument()
            case StringConstants.MediaType.image:
                self.setupNavigationBar(title:StringConstants.MediaType.image.uppercased())
                self.showImage()
            case StringConstants.MediaType.externallink:
                self.setupNavigationBar(title:StringConstants.MediaType.video.uppercased())
                self.showYouTubeVideo()
            default: break
                
        }
    }
    
    func showLoading() {
       if #available(iOS 13.0, *) {
            self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
        } else {
            // Fallback on earlier versions
            self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
        }
        self.activityIndicatorView.tintColor = .white
        self.activityIndicatorView.startAnimating()
        
        let activity = UIBarButtonItem(customView: self.activityIndicatorView)
        self.navigationItem.rightBarButtonItem = activity
    }
    
    func removeLoading() {
        self.activityIndicatorView.stopAnimating()
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func showDocument() {
        self.showLoading()
        let webview = WKWebView(frame: UIScreen.main.bounds)
        self.view.addSubview(webview)
        webview.navigationDelegate = self
        let mediaString = self.brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        webview.load(URLRequest(url: URL(string: mediaString!)!))
    }
    
    func showImage() {
        self.showLoading()
        let mediaString = self.brochureObject.media_link!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let imageView = UIImageView(frame: UIScreen.main.bounds)
        imageView.sd_setImage(with: URL(string:mediaString!)) { (image, error, type, url) in
            self.removeLoading()
        }
        imageView.contentMode = .scaleAspectFit
        self.view.addSubview(imageView)
    }
    
    func showYouTubeVideo() {
        
    }
}

extension MediaViewerController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
        self.removeLoading()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.removeLoading()
    }
}

extension MediaViewerController {
    @objc func cancelView() {
        self.dismiss(animated: true) {
            
        }
    }
}

