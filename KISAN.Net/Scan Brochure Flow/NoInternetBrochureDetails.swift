//
//  NoInternetBrochureViewController.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 05/12/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class NoInternetBrochureDetails: UIViewController {

    @IBOutlet weak var noInternetHint: UILabel!
    @IBOutlet weak var noInternetTitle: UILabel!
    @IBOutlet weak var backToScanButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupNavigationbar()
        self.setupInitialView()
    }
    
    func setupNavigationbar() {
        let backButton = UIButton(type:.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let listButton = UIButton(type:.custom)
        listButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        listButton.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        backButton.contentHorizontalAlignment = .right
        listButton.addTarget(self, action: #selector(listAction), for: .touchUpInside)
        
        let back = UIBarButtonItem(customView: backButton)
        let backText = UIBarButtonItem(title: "scanned_result".localized, style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItems = [back, backText]
        
        let list = UIBarButtonItem(customView: listButton)
        let listText = UIBarButtonItem(title: "list".localized, style: .plain, target: self, action: #selector(listAction))
        self.navigationItem.rightBarButtonItems = [list, listText]
        
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    
    func setupInitialView() {
        self.noInternetTitle.text = "no_internet_brochure_title".localized
        self.noInternetTitle.font = UIFont.boldSystemFont(ofSize: 24)
        self.noInternetHint.text = "no_internet_brochure_hint".localized
        self.noInternetHint.font = UIFont.systemFont(ofSize: 18)
        self.backToScanButton.setTitle("no_internet_brochure_scanner_button".localized, for: .normal)
        self.backToScanButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        self.backToScanButton.addTarget(self, action: #selector(goToScanView), for: .touchUpInside)
    }
    
    @objc func backAction() {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    @objc func listAction() {
        let brochureList = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "BrochureListViewController") as! BrochureListViewController
        self.navigationController?.pushViewController(brochureList, animated: true)
    }
    
    @objc func goToScanView() {
        self.navigationController?.popViewController(animated: true)
    }
}
