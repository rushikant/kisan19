//
//  ScanBarcodeViewController.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVFoundation

class ScanBarcodeViewController: UIViewController {
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var upperHorizontal: UIView!
    @IBOutlet weak var bottomHorizontal: UIView!
    @IBOutlet weak var leftVertical: UIView!
    @IBOutlet weak var rightVertical: UIView!
    @IBOutlet weak var redLine: UILabel!
    @IBOutlet weak var scanAlert: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    let avDevice = AVCaptureDevice.default(for: AVMediaType.video)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "scan".localized
        self.setupNavigationbar()
        self.flashButton.addTarget(self, action: #selector(didTouchFlashButton(_:)), for: .touchUpInside)
        self.cameraView.backgroundColor = UIColor.clear
        self.view.bringSubview(toFront: cameraView)
        self.scannerView.areaOfInterest = cameraView.frame
        self.scannerView.setupScanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        if !self.scannerView.isRunning {
            self.scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !self.scannerView.isRunning {
            self.scannerView.stopScanning()
            self.stopFlash()
        }
    }
    
    func setupNavigationbar() {
        
        let backButton = UIButton(type:.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        
        let listButton = UIButton(type:.custom)
        listButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        listButton.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        backButton.contentHorizontalAlignment = .right
        listButton.addTarget(self, action: #selector(listAction), for: .touchUpInside)
        
        
        let back = UIBarButtonItem(customView: backButton)
        let backText = UIBarButtonItem(title: "back".localized, style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItems = [back, backText]
        
        let list = UIBarButtonItem(customView: listButton)
        let listText = UIBarButtonItem(title:"list".localized, style: .plain, target: self, action: #selector(listAction))
        self.navigationItem.rightBarButtonItems = [list, listText]
        
        self.navigationController?.navigationBar.tintColor = .white
    }
    
}

// Scan Delegates
extension ScanBarcodeViewController: QRScannerViewDelegate {
    
    func qrScanningDidStop() {
        self.scanAlert.setTitle("", for: .normal)
        self.scanAlert.setImage(UIImage(), for: .normal)
    }
    
    func qrScanningDidFail() {
        self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
        self.scanAlert.titleLabel?.numberOfLines = 1
        self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
        self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
        self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
        self.clearValues()
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        if str!.isEmpty {
            self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
            self.scanAlert.titleLabel?.numberOfLines = 1
            self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
            self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
            self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
            self.clearValues()
        } else {
            if str!.isNumeric {
                if str?.length == 3 {
                    self.submitScan(code: str!)
                } else {
                    self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                    self.scanAlert.titleLabel?.numberOfLines = 1
                    self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
                    self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
                    self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
                    self.clearValues()
                }
            } else {
                self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                self.scanAlert.titleLabel?.numberOfLines = 1
                self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
                self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
                self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
                self.clearValues()
            }
        }
    }
    
    @objc func clearValues() {
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) {timer in
            timer.invalidate()
            self.scanAlert.setTitle("", for: .normal)
            self.scanAlert.setImage(UIImage(), for: .normal)
            self.scannerView.startScanning()
        }
    }
    
    @objc func onScanSuccess(brochureObject: ExihibitorScanBrochure) {
        self.scanAlert.setTitle("scan_success_message".localized, for: .normal)
        self.scanAlert.titleLabel?.numberOfLines = 1
        self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
        self.scanAlert.setImage(#imageLiteral(resourceName:"check-symbol"), for: .normal)
        self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) {timer in
            timer.invalidate()
            self.scanAlert.setTitle("", for: .normal)
            self.scanAlert.setImage(UIImage(), for: .normal)
            self.scanDetailsAction(brochureObject:brochureObject)
        }
    }
    
    @objc func scanAlertAction() {}
    
    @objc func didTouchFlashButton(_ sender: Any) {
        if (avDevice!.hasTorch) {
            do {
                try avDevice!.lockForConfiguration()
            } catch {
                print("aaaa")
            }
            if avDevice!.isTorchActive {
                self.stopFlash()
            } else {
                self.startFlash()
            }
        }
        // unlock your device
        avDevice!.unlockForConfiguration()
    }
    
    func startFlash() {
        if (avDevice!.hasTorch) {
            do {
                try avDevice!.lockForConfiguration()
            } catch {
                print("aaaa")
            }
            avDevice!.torchMode = AVCaptureDevice.TorchMode.on
        }
        // unlock your device
        avDevice!.unlockForConfiguration()
    }
    
    func stopFlash() {
        if (avDevice!.hasTorch) {
            do {
                try avDevice!.lockForConfiguration()
            } catch {
                print("aaaa")
            }
            avDevice!.torchMode = AVCaptureDevice.TorchMode.off
        }
        // unlock your device
        avDevice!.unlockForConfiguration()
    }
}

// API Handler
extension ScanBarcodeViewController {
    
    @objc func submitScan(code:String) {
        let app_key = URLConstant.app_key
        let eventCode = URLConstant.eventCode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
            source = RequestName.loginExhibitorSourceName
        }
        let scanData = ScanData(shortcode:code, scanned_date_time: DateTimeHelper.getCurrentDateTime())
        let scanRequest = ScanBrochureRequest(app_key: app_key, eventCode: eventCode, sessionId: sessionId, source: source, data: [scanData])
        
        if isInternetAvailableHelper.isInternetAvailable() {
            //ScanBrochureRequest
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            print("scanRequest: \(scanRequest)")
            let params = ScanBrochureRequest.convertToDictionary(request: scanRequest)
            let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.scanBrochureCode
            
            _ = ScanExhibitorBrochureService().scanExhibitorBarcode(apiURL,postData: params as [String : AnyObject], withSuccessHandler: { (scannedObject) in
                MBProgressHUD.hide(for: self.view, animated: true);
                if (scannedObject as! [AnyObject]).count > 0 {
                    let dict =  (scannedObject as! [[String:Any]])[0]
                    if dict.keys.contains("data") {
                        self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                        self.scanAlert.titleLabel?.numberOfLines = 1
                        self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
                        self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
                        self.scanAlert.addTarget(self, action:#selector(self.scanAlertAction), for: .allEvents)
                        self.clearValues()
                    } else {
                        let array : [ExihibitorScanBrochure] = ExihibitorScanBrochure.getModelFromArray([dict] as [AnyObject])
                        print("array: \(array)")
                        if array.count > 0 {
                            let brochure = array[0]
                            self.onScanSuccess(brochureObject: brochure)
                            self.getProfileData(channelKey: brochure.community_key!)
                        } else {
                            self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                            self.scanAlert.titleLabel?.numberOfLines = 1
                            self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
                            self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
                            self.scanAlert.addTarget(self, action:#selector(self.scanAlertAction), for: .allEvents)
                            self.clearValues()
                        }
                    }
                }
            }, withFailureHandlere: { (error: String) in
                MBProgressHUD.hide(for: self.view, animated: true);
                print("error",error)
                self.scanAlert.setTitle("scan_failed_message".localized, for: .normal)
                self.scanAlert.titleLabel?.numberOfLines = 1
                self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
                self.scanAlert.setImage(#imageLiteral(resourceName: "ic_close_white_24dp-1"), for: .normal)
                self.scanAlert.addTarget(self, action:#selector(self.scanAlertAction), for: .allEvents)
                self.clearValues()
            })
        } else {
            UserDefaults.storeUnsyncedBrochureRecord(record: scanRequest)
            self.scanAlert.setTitle("scan_success_message".localized, for: .normal)
            self.scanAlert.titleLabel?.numberOfLines = 1
            self.scanAlert.titleLabel?.minimumScaleFactor = 0.5
            self.scanAlert.setImage(#imageLiteral(resourceName:"check-symbol"), for: .normal)
            self.scanAlert.addTarget(self, action:#selector(scanAlertAction), for: .allEvents)
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) {timer in
                timer.invalidate()
                self.scanAlert.setTitle("", for: .normal)
                self.scanAlert.setImage(UIImage(), for: .normal)
                self.goToNoInternetView()
            }
        }
    }
    
    func getProfileData(channelKey:String){
        let params = ["":""]
        let currentTime = DateTimeHelper.getCurrentMillis()
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.getChannelProfile + channelKey + URLConstant.getChannelProfileTimeStamp + String(currentTime)
        
        print("apiURL",apiURL)
        _ = ChannelProfileService().getChannelProfile(apiURL,
                                                      postData: params as [String : AnyObject],
                                                      withSuccessHandler: { (userModel) in
                                                        let communityDetails = userModel as! CommunityDetails
                                                        //communityDetails.communityJabberId
                                                        DispatchQueue.main.async {
                                                            self.followChannelApi(communityKey: channelKey, communityJabberId: communityDetails.communityJabberId!,communityDetails:communityDetails)
                                                        }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
           /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))
                ], actions:[{action1 in
                    },{action2 in
                    }, nil])*/
        })
        
    }
    
    func followChannelApi(communityKey:String,communityJabberId:String,communityDetails:CommunityDetails){
        updateIsMemberInLocalDB(communityKey: communityKey, CommunityDetails: communityDetails)
        
        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            
                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
               /* self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])*/
                
            }
        })
        
    }
    
    func updateIsMemberInLocalDB(communityKey:String,CommunityDetails:CommunityDetails){
        
        let  dict1  = ConvertToDictOfChannelProfileObj.convertToDictOfChannelProfileObj(myChatDetails: [CommunityDetails])
        let  myChatListRespo = MyChatServices.sharedInstance.convertCommunityDetailsToMyChat(communityDetail: dict1)
        var myChats = [MyChatsTable]()
        myChats.append(myChatListRespo)
        //---------------- Update isMember  value as true for follow channel & also at database----------//
        myChats.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
        myChats.filter({$0.communityKey == communityKey}).first?.messageText = NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
        
        var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: myChats)
        CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)
        let currentTime = DateTimeHelper.getCurrentMillis()
        dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
        dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
        dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
        
        let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
        if checkCommunityKeyWithisNormalMychatFlag.count == 0{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
            dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
            MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
        }else{
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
        }
        
    }
}


// Actions on buttons
extension ScanBarcodeViewController {
    
    @objc func backAction() {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    @objc func listAction() {
        let brochureList = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "BrochureListViewController") as! BrochureListViewController
        self.navigationController?.pushViewController(brochureList, animated: true)
    }
    
    @objc func scanDetailsAction(brochureObject: ExihibitorScanBrochure) {
        let brochureDetails = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "BrochureDetailsViewController") as! BrochureDetailsViewController
        brochureDetails.brochureObject = brochureObject
        self.navigationController?.pushViewController(brochureDetails, animated: true)
    }
    
    func goToNoInternetView() {
        let noInternetView = UIStoryboard.BarcodeFlow().instantiateViewController(withIdentifier: "NoInternetBrochureDetails") as! NoInternetBrochureDetails
        self.navigationController?.pushViewController(noInternetView, animated: true)
    }
}

