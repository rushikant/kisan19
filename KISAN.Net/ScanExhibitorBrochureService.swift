//
//  ScanExibitorBrochureService.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 15/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import Alamofire

class ScanExhibitorBrochureService: Repository {
  
    func scanExhibitorBarcode(_ baseUrl: String,
                      postData: [String: AnyObject],
                      withSuccessHandler success: CompleteionHandler?,
                      withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.scanExhibitorBrochureCode)
    }
    
    func getScannedBrochuersByvisitor(_ baseUrl: String,
                      postData: [String: AnyObject],
                      withSuccessHandler success: CompleteionHandler?,
                      withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.getScannedBrochuersByvisitor)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.scanExhibitorBrochureCode {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    
                    let dataArray = responseDict["data"] as! [AnyObject]
                    
                    if dataArray.count > 0  {
                        if let success = self.completeionBlock {
                            success(dataArray)
                        }
                    } else {
                        if let failure = self.failureBlock {
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                } else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        } else if requestName == RequestName.getScannedBrochuersByvisitor {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    
                    if responseDict["data"] == nil {
                        let response = [ExihibitorScanBrochure] ()
                        if let success = self.completeionBlock {
                            success(response as [ExihibitorScanBrochure])
                        } else {
                           if let failure = self.failureBlock {
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    } else {
                        let response : [ExihibitorScanBrochure] = ExihibitorScanBrochure.getModelFromArray(responseDict["data"] as! [AnyObject])
                        print("appStarterResponse",response)
                        if let success = self.completeionBlock {
                            success(response as [ExihibitorScanBrochure])
                        } else {
                            if let failure = self.failureBlock {
                                if let message  = responseDict["message"] as? String{
                                    failure(message)
                                }else{
                                    failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                                }
                            }
                        }
                    }
                    
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
