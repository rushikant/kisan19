//
//  ChannelProfileService.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 28/09/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class ChannelProfileService: Repository {
    
    func getChannelProfile(_ baseUrl: String,
                               postData: [String: AnyObject],
                               withSuccessHandler success: CompleteionHandler?,
                               withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetChannelProfileDetails)
        
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetChannelProfileDetails {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var channelProfileRespo: CommunityDetails = CommunityDetails.initializeCommunityDetails()
                    channelProfileRespo = CommunityDetails.getCommunityDetailsFromDictionary(responseDict["communityDetails"] as! [String : AnyObject?])

                    if let success = self.completeionBlock {
                        success(channelProfileRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure("Something went wrong")
                    }
                }
            }
        }
    }
    
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
