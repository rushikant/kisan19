//
//  CreateCommunityServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 09/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class CreateCommunityServices: Repository {
    
    func createCommunity(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData as [String: AnyObject], requestName:  RequestName.createCommunity)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.createCommunity {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var createChannelRespo: CreateChannelResponse = CreateChannelResponse.initializeCreateChannelResponse()
                    createChannelRespo = CreateChannelResponse.getCreateChannelResponseFromDictionary(responseDict,postData:postData)
                    print("createChannelRespo",createChannelRespo)
                    if let success = self.completeionBlock {
                        success(createChannelRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
