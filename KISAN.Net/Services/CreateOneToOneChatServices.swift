//
//  CreateOneToOneChatServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 10/04/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class CreateOneToOneChatServices: Repository {

    func createOneToOneChat(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData as [String: AnyObject], requestName:  RequestName.createOneToOneChat)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.createOneToOneChat {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Bool  == true {
                    var myChatListRespo: MyChatListResponse = MyChatListResponse.initializeMyChatListResponse()
                    myChatListRespo = MyChatListResponse.getOneToOneMyChatListResponseFromDictionary(responseDict)
                    print("myChatListRespo",myChatListRespo)
                    if let success = self.completeionBlock {
                        success(myChatListRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    if let message  = responseDict["message"] as? String{
                        if let failure = self.failureBlock {
                            failure(message)
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
    
}
