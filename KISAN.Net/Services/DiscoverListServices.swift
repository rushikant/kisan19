//
//  DiscoverListServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class DiscoverListServices: Repository {
    
    func getDiscoverList(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        //return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetDiscoverList)
        return self.GET_FROM_PARAMETERWITHURL_API(baseUrl,postData: postData, requestName: RequestName.GetDiscoverList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        print("postData",postData)
        var isSearchig = String()
        if let value = postData["isSearchig"]{
            isSearchig = value as! String
        }

        if isSearchig == String(StringConstants.TRUE) {
            if requestName == RequestName.GetDiscoverList {
                if let responseDict =  response as? [String: AnyObject] {
                    if responseDict["success"] as? Int == 1 {
                        // var discoverChannelRespo: DiscoverChannelResponse = DiscoverChannelResponse.initializeDiscoverChannelResponse()
                        //  discoverChannelRespo = DiscoverChannelResponse.getDiscoverChannelResponseFromDictionary(responseDict)
                        let  discoverChannelRespo = DiscoverChannelResponse.getDiscoverChannelResponseFromSearching(responseDict)
                        print("discoverChannelRespo",discoverChannelRespo)
                        if let success = self.completeionBlock {
                            success(discoverChannelRespo)
                        }else{
                            let message  = responseDict["message"] as? String
                            if let failure = self.failureBlock {
                                failure(message!)
                            }
                        }
                    }
                    else {
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
            }
        }else{
            if requestName == RequestName.GetDiscoverList {
                if let responseDict =  response as? [String: AnyObject] {
                    if responseDict["success"] as? Int == 1 {
                        var discoverChannelRespo: DiscoverChannelResponse = DiscoverChannelResponse.initializeDiscoverChannelResponse()
                        discoverChannelRespo = DiscoverChannelResponse.getDiscoverChannelResponseFromDictionary(responseDict)
                        print("discoverChannelRespo",discoverChannelRespo)
                        if let success = self.completeionBlock {
                            success(discoverChannelRespo)
                        }else{
                            let message  = responseDict["message"] as? String
                            if let failure = self.failureBlock {
                                failure(message!)
                            }
                        }
                    }
                    else {
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
            }
        }
    }
    
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
