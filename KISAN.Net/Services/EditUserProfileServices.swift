//
//  EditUserProfileServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
class EditUserProfileServices: Repository {
    
    func editUserProfileServices(_ baseUrl: String,
                                 postData: [String: AnyObject],
                                 withSuccessHandler success: CompleteionHandler?,
                                 withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.PUT(baseUrl, postData: postData, requestName:  RequestName.EditUserProfile)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.EditUserProfile {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var editUserProfileRespo: EditUserProfileResponse = EditUserProfileResponse.initializeEditUserProfileResponse()
                    editUserProfileRespo = EditUserProfileResponse.getEditUserProfileResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(editUserProfileRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
