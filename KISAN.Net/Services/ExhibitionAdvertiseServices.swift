//
//  ExhibitorAdvertiseServices.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 20/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class ExhibitionAdvertiseServices: Repository {
    
    func exhibitionAdvertise(_ baseUrl: String,
                        postData: [String: AnyObject],
                        withSuccessHandler success: CompleteionHandler?,
                        withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.ExhibitionAdvertise)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.ExhibitionAdvertise {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    var exhibiAdvRespo: ExhibitionAdvertiseResponse = ExhibitionAdvertiseResponse.initializeExhibitionAdvertiseResponse()
                    exhibiAdvRespo = ExhibitionAdvertiseResponse.getExhibitionAdvertiseResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(exhibiAdvRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
