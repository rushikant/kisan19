//
//  FollowChannelServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 16/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class FollowChannelServices: Repository {
    
    func followChannelServices(_ baseUrl: String,
                               postData: [String: AnyObject],
                               withSuccessHandler success: CompleteionHandler?,
                               withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.followChannel)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.followChannel {
            if let responseDict =  response as? [String: AnyObject] {
                var success = String()
                if let successValue = responseDict["success"]{
                    success = String(describing: successValue)
                }
                if success == "1" {
                    var followChannelRespo: FollowChannelResponse = FollowChannelResponse.initializeFollowChannelResponse()
                    followChannelRespo = FollowChannelResponse.getFollowChannelResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(followChannelRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    //let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        //failure(message!)
                        failure("channel doesn't exit")
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
