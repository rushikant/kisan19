//
//  GetCommonChannelListServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 08/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class GetCommonChannelListServices: Repository {

    func getCommonChannelList(_ baseUrl: String,
                                 postData: [String: AnyObject],
                                 withSuccessHandler success: CompleteionHandler?,
                                 withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetCommonChannelList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetCommonChannelList {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var commonChannelListRespo: CommonChannelListResponse =  CommonChannelListResponse.initializeCommonChannelListResponse()
                    commonChannelListRespo = CommonChannelListResponse.getCommonChannelListResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(commonChannelListRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
