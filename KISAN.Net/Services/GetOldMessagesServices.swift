//
//  GetOldMessagesServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class GetOldMessagesServices: Repository {
    
    func getOldMessagesList(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl,postData: postData, requestName: RequestName.GetMessageList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetMessageList {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var getOldMessageResponse: GetOldMessageResponse = GetOldMessageResponse.initializeGetOldMessageResponse()
                    getOldMessageResponse = GetOldMessageResponse.getGetOldMessageResponseFromDictionary(responseDict,postData:postData)
                    print("getOldMessageResponse",getOldMessageResponse)
                    if let success = self.completeionBlock {
                        success(getOldMessageResponse)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String ?? ""
                    if let failure = self.failureBlock {
                        failure(message)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
