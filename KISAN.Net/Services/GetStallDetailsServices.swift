//
//  GetStallDetailsServices.swift
//  KISAN.Net
//
//  Created by Rushikant on 20/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import Alamofire

class GetStallDetailsServices: Repository {
   
    func getStallDetails(_ baseUrl: String,
                           postData: [String: AnyObject],
                           withSuccessHandler success: CompleteionHandler?,
                           withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.GetStallPasses)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.GetStallPasses {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    var getStallDetailsResponse: GetStallDetailsResponse =  GetStallDetailsResponse.initializeGetStallDetailsResponse()
                    getStallDetailsResponse = GetStallDetailsResponse.getStallDetailsResponseFromDictionary(responseDict)
                    print("callBackApiResponse",getStallDetailsResponse)
                    if let success = self.completeionBlock {
                        success(getStallDetailsResponse)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}


