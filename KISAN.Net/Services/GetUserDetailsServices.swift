//
//  GetUserDetailsServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class GetUserDetailsServices: Repository {
    
    func getUserDetailsServices(_ baseUrl: String,
                                 postData: [String: AnyObject],
                                 withSuccessHandler success: CompleteionHandler?,
                                 withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_PARAMETERWITHURL_API(baseUrl,postData: postData, requestName: RequestName.GetUserDetails)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetUserDetails {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var communityMembersListRespo: GetUserDetailsResponse = GetUserDetailsResponse.initializeGetUserDetailsResponse()
                    communityMembersListRespo = GetUserDetailsResponse.getGetUserDetailsResponseFromDictionary(responseDict,postData:postData)
                    if let success = self.completeionBlock {
                        success(communityMembersListRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
