//
//  GetYoutubeurlDataService.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/02/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class GetYoutubeurlDataService: Repository {
  
    func getyoutubeurldata(_ baseUrl: String,
                     postData: [String: AnyObject],
                     withSuccessHandler success: CompleteionHandler?,
                     withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_WITHOUT_HEADER_API(baseUrl, postData: postData, requestName: RequestName.GetYoutubeurlData)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetYoutubeurlData {
            if let responseDict =  response as? [String: AnyObject] {
                var youtubedataresponce: GetYoutubeurlDataResponse = GetYoutubeurlDataResponse.initializeGetYoutubeurlDataResponse()
                youtubedataresponce = GetYoutubeurlDataResponse.getGetYoutubeurlDataResponseFromDictionary(responseDict)
                print("youtubedataurl",youtubedataresponce)
                
                if let success = self.completeionBlock {
                    success(youtubedataresponce)
                }else{
                    let message  = "Unable to get data"
                    if let failure = self.failureBlock {
                        failure(message)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
