//
//  LeaveChannelServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 11/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
class LeaveChannelServices: Repository {
    
    func leaveChannelServices(_ baseUrl: String,
                              postData: [String: AnyObject],
                                 withSuccessHandler success: CompleteionHandler?,
                                 withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.DELETE(baseUrl, deleteData: postData as NSDictionary, requestName: RequestName.leaveChannel)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.leaveChannel {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var leaveChannelResponse: LeaveChannelResponse = LeaveChannelResponse.initializeLeaveChannelResponse()
                    leaveChannelResponse = LeaveChannelResponse.getLeaveChannelResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(leaveChannelResponse)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
