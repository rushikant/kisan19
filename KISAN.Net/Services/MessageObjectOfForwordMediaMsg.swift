//
//  MessageObjectOfForwordMediaMsg.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 03/11/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MessageObjectOfForwordMediaMsg: NSObject,PassMultiMediaResponse {
    func PassMultiMediaResponse(_ mediaSource: String, _ imagePath: String, _ callAgain: Bool) {
        
    }
    
    func PassMultiMediaFail(_ errorMsg: String) {
        
    }
    
    var imageData = NSData()
    var messageKeyForSendMultiMedia = String()
    var messageTimeForSendMultiMedia = String()
    var originalMediaBucketName = String()
    var S3UploadKeyNameWithImagePrefix = String()
    let documentDirectoryHelper = DocumentDirectoryHelper()

    func setMultiMediaPart(medeiaType:String,forwordMsgData:Messages,communityKey:String){
        
        let currentTime = DateTimeHelper.getCurrentMillis()
        let strUUID = UUIDHelper.getUUID()
        let size = CGSize(width: 0, height: 0)
        if medeiaType == StringConstants.MediaType.image{
            //let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            //spinnerActivity.isUserInteractionEnabled = false
            // Compress image
            //let imageHelper = ImageHelper()
           // let imageData = imageHelper.compressImage(image: appDelegate.selctedImage, compressQuality: 0.4)!
            //imageData = NSData(contentsOf: contentField2! as URL)
            
            let url = URL(string:forwordMsgData.contentField2!)
            if let data = try? Data(contentsOf: url!){
                imageData = data as NSData
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original image to Amzon S3
                let  originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + images + "/" + messageKeyForSendMultiMedia
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save Image at docoument directory
                S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + imagePrefix + String(messageTimeForSendMultiMedia)
                let imageName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".png"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: imageName , sourceType: StringConstants.MediaType.image,imageData:imageData as NSData)
                uploadMediaPartToS3.uploadImage(with: imageData as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.image,callAgain:true,contentType:imageContentType,uploadKeyName:S3UploadKeyNameWithImagePrefix)
            }

            
            
            
        }/*else if appDelegate.selectedVideoURL != nil{
            
            let imageHelper = ImageHelper()
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4")
            imageHelper.compressVideo(inputURL: (appDelegate.selectedVideoURL)!, outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {
                        return
                    }
                    print("File size after compression: \(Double(compressedData.length / 1024)) kb")
                    
                    DispatchQueue.main.async {
                        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                        spinnerActivity.isUserInteractionEnabled = false
                        //let video = try NSData(contentsOf: self.appDelegate.selectedVideoURL!, options: .mappedIfSafe)
                        let video = compressedData
                        // Total Size in KB
                        self.mediaSize = Double(video.length) / 1024.0
                        print("mediaSize",self.mediaSize)
                        let imageThumb = imageHelper.getThumbnailImage(forUrl: self.appDelegate.selectedVideoURL!)
                        self.thumbVideoData = imageHelper.compressImage(image: imageThumb!, compressQuality:  0.25)!
                        self.messageKeyForSendMultiMedia = strUUID
                        self.messageTimeForSendMultiMedia = String(currentTime)
                        //Send original image to Amzon S3
                        self.originalMediaBucketName = S3BucketName + channelMedia + self.communityKey + "/" + videos + "/" + self.messageKeyForSendMultiMedia
                        let uploadMediaPartToS3  = UploadMediaPartToS3()
                        uploadMediaPartToS3.delegate = self
                        //Save Image at docoument directory
                        self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + videoPrefix + String(self.messageTimeForSendMultiMedia)
                        let videoNameThumbName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + self.S3UploadKeyNameWithImagePrefix + String(self.messageTimeForSendMultiMedia) + ".mp4"
                        self.documentDirectoryHelper.saveImageDocumentDirectory(imageName: videoNameThumbName , sourceType: StringConstants.MediaType.video,imageData:video as NSData)
                        uploadMediaPartToS3.uploadImage(with: video as Data, bucketName:self.originalMediaBucketName,mediaSource:StringConstants.MediaType.video,callAgain:true,contentType:videoContentType,uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
                    }
                    
                case .failed:
                    break
                case .cancelled:
                    break
                }
            }
            
        }else if appDelegate.captureAudioUrl != nil {
            
            do {
                let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.isUserInteractionEnabled = false
                let audio = try NSData(contentsOf: appDelegate.captureAudioUrl!, options: .mappedIfSafe)
                // Total Size in KB
                mediaSize = Double(audio.length) / 1024.0
                let formatter = NumberFormatter()
                formatter.numberStyle = .none
                formatter.maximumFractionDigits = 0
                let formattedAmount = formatter.string(from: mediaSize as NSNumber)!
                print(mediaSize)
                mediaSize = Double(formattedAmount)!
                
                print(mediaSize)
                messageKeyForSendMultiMedia = strUUID
                messageTimeForSendMultiMedia = String(currentTime)
                //Send original audio file to Amzon S3
                originalMediaBucketName = S3BucketName + channelMedia + communityKey + "/" + audios + "/" + messageKeyForSendMultiMedia
                let uploadMediaPartToS3  = UploadMediaPartToS3()
                uploadMediaPartToS3.delegate = self
                //Save audio file at docoument directory
                self.S3UploadKeyNameWithImagePrefix =  S3UploadKeyName + audioPrefix + String(self.messageTimeForSendMultiMedia)
                let audioFileName = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyNameWithImagePrefix + String(messageTimeForSendMultiMedia) + ".mp3"
                documentDirectoryHelper.saveImageDocumentDirectory(imageName: audioFileName , sourceType: StringConstants.MediaType.audio,imageData:audio as NSData)
                uploadMediaPartToS3.uploadImage(with: audio as Data, bucketName:originalMediaBucketName,mediaSource:StringConstants.MediaType.audio,callAgain:false,contentType:audioContentType, uploadKeyName: self.S3UploadKeyNameWithImagePrefix)
            } catch {
                print("Error")
            }
            
        }else if !(appDelegate.locationTitleAddress.isEmpty) && !appDelegate.locationDetailddress.isEmpty{
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            let strUUID = UUIDHelper.getUUID()
            let messageKey = strUUID
            let createSendMessageRequest  = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.sent)
            print("createSendMessageRequest",createSendMessageRequest)
            
            createSendMessageRequestInDB = buildRequestForSendMessage(id: messageKey, mediaType: StringConstants.EMPTY, messageType: StringConstants.MessageType.mapLocation, contentField1: StringConstants.EMPTY, contentField2: appDelegate.latitudeOfSendLocation, contentField3: appDelegate.longitudeOfSendLocation, contentField4: appDelegate.locationTitleAddress, contentField5: appDelegate.locationDetailddress, contentField6: StringConstants.EMPTY, contentField7: StringConstants.EMPTY, contentField8: StringConstants.EMPTY, contentField9: StringConstants.EMPTY, contentField10: StringConstants.EMPTY, senderId: userId, communityId: communityKey, jabberId: myChatsData.communityJabberId!, createdDatetime: String(currentTime), hiddenDatetime: StringConstants.EMPTY, deletedDatetime: StringConstants.EMPTY, hiddenbyUserId: StringConstants.EMPTY, deletedbyUserId: StringConstants.EMPTY, messageStatus: StringConstants.Status.pending)
            
            
            // Show message on same screen
            var  messages = Messages()
            let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
            if checkInternetConnection == true{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequest)
            }else{
                messages = MessagesServices.sharedInstance.crateMessageInBaseType(messageDetail: createSendMessageRequestInDB)
            }
            
            //  messageList.append(messages)
            messageList.insert(messages, at: 0)
            messageListTemp = messageList
            setTblScrollAtBottom()
            tblChannelSendBroadcastMsgList.reloadData()
            //Save message in core data as local DB with pending status
            let messagesDB = MessagesServices.sharedInstance.sendMessageAtMessageTable(messageDetail: createSendMessageRequestInDB, saveDataAtDB: true)
            MyChatServices.sharedInstance.updateLatestMsgAtMyChatTable(latestMsg: messagesDB.contentField1!, latestMessageTime: messagesDB.messageStatusTime!, communityKey: messagesDB.communityId!,mediaType:messagesDB.mediaType!,messageType:messagesDB.messageType!, lastMessageId: messagesDB.id!)
            
            if checkInternetConnection == true{
                self.groupChat(createSendMessageRequest: createSendMessageRequest, messageGuid: strUUID, communityJabberId: myChatsData.communityJabberId!)
            }
            txtViewOfChatMessage.text = StringConstants.EMPTY
            
            // Make nill object
            appDelegate.strCaptionText = ""
            appDelegate.selctedImage = UIImage()
            appDelegate.selectedVideoURL = nil
            appDelegate.captureAudioUrl = nil
            appDelegate.locationTitleAddress = ""
            appDelegate.locationDetailddress = ""
            appDelegate.longitudeOfSendLocation = ""
            appDelegate.latitudeOfSendLocation = ""
        }*/
    }
    

}

