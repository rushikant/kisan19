//
//  MuteUnMuteServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 22/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
class MuteUnMuteServices: Repository {
    
    func muteUnMuteServices(_ baseUrl: String,
                                 postData: [String: AnyObject],
                                 withSuccessHandler success: CompleteionHandler?,
                                 withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.PUT(baseUrl, postData: postData, requestName:  RequestName.EditUserProfile)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.EditUserProfile {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var muteUnMuteRespo: MuteUnMuteResponse = MuteUnMuteResponse.initializeMuteUnMuteResponse()
                    muteUnMuteRespo = MuteUnMuteResponse.getMuteUnMuteResponseFromDictionary(responseDict)
                    if let success = self.completeionBlock {
                        success(muteUnMuteRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
