//
//  OneToOneMyChatListService.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class OneToOneMyChatListService: Repository {
    
    func getOneToOneMyChatList(_ baseUrl: String,
                       postData: [String: AnyObject],
                       withSuccessHandler success: CompleteionHandler?,
                       withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetOneToOneMyChatList)
        
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetOneToOneMyChatList {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var myChatListRespo: MyChatListResponse = MyChatListResponse.initializeMyChatListResponse()
                    myChatListRespo = MyChatListResponse.getOneToOneUserInitiatedMyChatListResponseFromDictionary(responseDict)
                    print("myChatListRespo",myChatListRespo)
                    if let success = self.completeionBlock {
                        success(myChatListRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
