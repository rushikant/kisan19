//
//  SelectDistrictServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 23/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
class SelectDistrictServices: Repository {
    
    func getDistrictList(_ baseUrl: String,
                      postData: [String: AnyObject],
                      withSuccessHandler success: CompleteionHandler?,
                      withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl, postData: postData, requestName: RequestName.GetDistrictList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetDistrictList {
            if let responseDict =  response as? [String: AnyObject] {
                var selectStateDistrictResponse: SelectStateDistrictResponse = SelectStateDistrictResponse.initializeSelectStateDistrictResponse()
                selectStateDistrictResponse = SelectStateDistrictResponse.getSelectStateDistrictResponseFromDictionary(responseDict)
                if let success = self.completeionBlock {
                    success(selectStateDistrictResponse)
                }else{
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
                
                
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
