//
//  SelectStateServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 05/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
class SelectStateServices: Repository {
    
    func getStateList(_ baseUrl: String,
                      postData: [String: AnyObject],
                      withSuccessHandler success: CompleteionHandler?,
                      withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.GET_FROM_WIRHOUT_PARAMETER_API(baseUrl,postData: postData, requestName: RequestName.GetStateList)
    }
    
    override func successCallback(_ response: AnyObject, requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.GetStateList {
            if let responseDict =  response as? [String: AnyObject] {
                var stateListResponse: StateListResponse = StateListResponse.initializeStateListResponse()
                stateListResponse = StateListResponse.getStateListResponseFromDictionary(responseDict)
                if let success = self.completeionBlock {
                    success(stateListResponse)
                }else{
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
                
                
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
