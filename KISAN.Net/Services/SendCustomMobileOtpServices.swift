//
//  SendCustomMobileOtpServices.swift
//  KISAN.Net
//
//  Created by Rushikant on 26/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class SendCustomMobileOtpServices: Repository {
    
    func sendOtp(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData as [String: AnyObject], requestName:  RequestName.sendOtp)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.sendOtp {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? String == "true" {
                    var sendCustomMobileOtpResponse: SendCustomMobileOtpResponse = SendCustomMobileOtpResponse.initializeSendCustomMobileOtpResponse()
                    sendCustomMobileOtpResponse = SendCustomMobileOtpResponse.getSendCustomMobileOtpResponseFromDictionary(responseDict)
                    print("sendCustomMobileOtpResponse",sendCustomMobileOtpResponse)
                    if let success = self.completeionBlock {
                        success(sendCustomMobileOtpResponse)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}

