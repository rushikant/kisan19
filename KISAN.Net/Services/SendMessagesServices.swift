//
//  SendMessagesServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 02/08/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import XMPPFramework

class SendMessagesServices: NSObject {
    var appDelegate = AppDelegate()

    func groupChat(createSendMessageRequest:Dictionary<String, Any>,messageGuid:String,communityJabberId:String) {
        
        let host = URLConstant.hostName
        let serviceJID = XMPPJID(string: "pubsub.\(host)")
        print("serviceJID",serviceJID ?? String())
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        let xmppPubSub = XMPPPubSub(serviceJID: serviceJID, dispatchQueue: DispatchQueue.main)
        xmppPubSub.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppPubSub.activate(appDelegate.xmppController.xmppStream)
        let body = XMLElement.element(withName: "title") as? XMLElement // New
        let from = XMLElement.element(withName: "from") as? XMLElement
        
        var userString = String()
        do{
            if let userOneData = try JSONSerialization.data(withJSONObject: createSendMessageRequest, options: JSONSerialization.WritingOptions.prettyPrinted) as Optional {
                let  jsonString = NSString(data: userOneData , encoding: String.Encoding.utf8.rawValue)! as String
                userString = jsonString.replacingOccurrences(of: "\\/", with: "/"
                    , options:  NSString.CompareOptions.caseInsensitive, range: nil)
                print("userString",userString)
            } }catch {
                print(error.localizedDescription)
        }
        
        let resource = RequestName.resourceName
        body?.stringValue = userString
        from?.stringValue = resource
        let messageBody = XMLElement.element(withName: "book") as? XMLElement
        messageBody?.setXmlns("pubsub:test:book") //New
        messageBody?.addChild(body ?? XMLNode())
        messageBody?.addChild(from ?? XMLNode())
        print("messageBody",messageBody)
        xmppPubSub.publish(toNode: communityJabberId, entry: messageBody!, withItemID: messageGuid, options: ["pubsub#access_model": "open"])
        
        // Message status  Use this code for acknowledge
        let xmppMessageDeliveryRecipts = XMPPMessageDeliveryReceipts(dispatchQueue: DispatchQueue.main)
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = true
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = true
        xmppMessageDeliveryRecipts.activate(appDelegate.xmppController.xmppStream)
    }

}
