//
//  ShowMyGreenpassService.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 12/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import Alamofire

class ShowMyGreenpassService: Repository {
    func getMyGreenpassList(_ baseUrl: String,
                               postData: [String: AnyObject],
                               withSuccessHandler success: CompleteionHandler?,
                               withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.GetMyGreenpassList)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.GetMyGreenpassList {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    var ShowMyGreenpasssRepo: ShowMyGreenpassResponse = ShowMyGreenpassResponse.initializeShowMyGreenpassResponse()
                    ShowMyGreenpasssRepo = ShowMyGreenpassResponse.showMyGreenpassResponseFromDictionary(responseDict)
                    print("ShowPendingInvitesRepo",ShowMyGreenpasssRepo)
                    if let success = self.completeionBlock {
                        success(ShowMyGreenpasssRepo)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
