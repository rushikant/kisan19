//
//  ShowPendingInvitesService.swift
//  KISAN.Net
//
//  Created by MacMini on 10/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation
import Alamofire

class ShowPendingInvitesService: Repository {
    func getPendingInvitetList(_ baseUrl: String,
                      postData: [String: AnyObject],
                      withSuccessHandler success: CompleteionHandler?,
                      withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName:  RequestName.GetPendingInviteList)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        
        if requestName == RequestName.GetPendingInviteList {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    var ShowPendingInvitesRepo: ShowPendingInvitesResponse = ShowPendingInvitesResponse.initializeShowPendingInviteListResponse()
                    ShowPendingInvitesRepo = ShowPendingInvitesResponse.showPendingInviteListResponseFromDictionary(responseDict)
                    print("ShowPendingInvitesRepo",ShowPendingInvitesRepo)
                    if let success = self.completeionBlock {
                        success(ShowPendingInvitesRepo)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }
}
