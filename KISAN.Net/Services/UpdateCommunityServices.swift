//
//  UpdateCommunityServices.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 06/06/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class UpdateCommunityServices: Repository {
    
    func updateCommunity(_ baseUrl: String,
                         postData: [String: AnyObject],
                         withSuccessHandler success: CompleteionHandler?,
                         withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.PUT(baseUrl, postData: postData, requestName:  RequestName.updateCommunity)
    }
    
    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {
        if requestName == RequestName.updateCommunity {
            if let responseDict =  response as? [String: AnyObject] {
                if responseDict["success"] as? Int == 1 {
                    var updateChannelRespo: UpdateChannelResponse = UpdateChannelResponse.initializeUpdateChannelResponse()
                    updateChannelRespo = UpdateChannelResponse.getUpdateChannelResponseFromDictionary(responseDict, postData: postData)
                    print("updateChannelRespo",updateChannelRespo)
                    if let success = self.completeionBlock {
                        success(updateChannelRespo)
                    }else{
                        let message  = responseDict["message"] as? String
                        if let failure = self.failureBlock {
                            failure(message!)
                        }
                    }
                }
                else {
                    let message  = responseDict["message"] as? String
                    if let failure = self.failureBlock {
                        failure(message!)
                    }
                }
            }
        }
    }
    
    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }

}
