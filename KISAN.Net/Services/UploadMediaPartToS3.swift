//
//  UploadMediaPartToS3.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 27/03/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit
import AWSS3

//MARK: step 1 Add Protocol here
protocol PassMultiMediaResponse: class {
    func PassMultiMediaResponse(_ mediaSource: String,_ imagePath:String,_ callAgain: Bool)
    func PassMultiMediaFail(_ errorMsg: String)
}

class UploadMediaPartToS3:NSObject {
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    let transferUtility = AWSS3TransferUtility.default()
    weak var delegate: PassMultiMediaResponse?
    
    func uploadImage(with data: Data,bucketName:String,mediaSource:String,callAgain:Bool,contentType:String,uploadKeyName:String) {
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    print("Failed with error: \(error)")
                    self.delegate?.PassMultiMediaFail(error.localizedDescription)
                }
                else{
                    print("Success")
                    self.delegate?.PassMultiMediaResponse(mediaSource, bucketName, callAgain)
                }
            })
        }
        let expression = AWSS3TransferUtilityUploadExpression()
        transferUtility.uploadData(
            data,
            bucket: bucketName,
            key: uploadKeyName,
            contentType: contentType,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> Bool? in
                if let error = task.error {
                    print("Error: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Failed"
                        self.delegate?.PassMultiMediaFail(error.localizedDescription)
                    }
                }
                if let _ = task.result {
                    DispatchQueue.main.async {
                        print("Upload Starting!")
                    }
                }
                return nil
        }
    }
}
