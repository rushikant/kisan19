//
//  SetInviteSummaryViewController.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 26/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
import MBProgressHUD

class SetInviteSummaryViewController: UIViewController,PassSelectedColor,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIViewControllerTransitioningDelegate,UIDocumentPickerDelegate {
   
    var appDelegate = AppDelegate()

    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    @IBOutlet weak var txtChangeColor: UILabel!
    
    @IBOutlet weak var lblInvitationPreview: UILabel!
    
    @IBOutlet weak var viewUploadMedia: UIView!
    
    @IBOutlet weak var viewSetInvite: UIView!
    
    @IBOutlet weak var viewInvitationPreview: UIView!
    
    @IBOutlet weak var lblCompanyNameCount: UILabel!
    
    @IBOutlet weak var txtCompanyName: UITextField!
    
    @IBOutlet weak var imgMedia: UIImageView!
    
    @IBOutlet weak var btnEditMEdia: UIButton!
    
    @IBOutlet weak var lblInviteBtnText: UILabel!
    
    //media PopUp
    @IBOutlet weak var viewUploadImage: UIView!

    @IBOutlet weak var txtYoutubeLink: UITextField!
    
    @IBOutlet weak var viewOfBackPopUp: UIView!
    
    @IBOutlet weak var viewYoutubeExpanded: UIView!
    
    @IBOutlet weak var viewYoutubeSmall: UIView!
    
    @IBOutlet weak var viewUploadDoc: UIView!
    
    @IBOutlet weak var viewSelectMEdiPopup: UIView!
    
    @IBOutlet weak var imgUploadImage: UIImageView!
    
    @IBOutlet weak var imgUploadDoc: UIImageView!
    
    @IBOutlet weak var imgUploadVideo: UIImageView!
    
    @IBOutlet weak var lbluploadImage: UILabel!
    
    @IBOutlet weak var lblUploadDoc: UILabel!
    
    @IBOutlet weak var lblUploadVideo: UILabel!
    
    @IBOutlet weak var heightConstraintlayoutofMEdiaPopup: NSLayoutConstraint!
    
    @IBOutlet weak var lblKnowMore: UILabel!
    
    @IBOutlet weak var topConstarintComnstantForMEdiaPopup: NSLayoutConstraint!
    
    @IBOutlet weak var btnKnowMore: UIButton!
    
    @IBOutlet weak var imgVideoIcon: UIImageView!
    
    @IBOutlet weak var btnSkip: UIButton!
    private let picker = UIImagePickerController()
    var overlayView: InvitationPreviewOverlay!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leftConstantOfViewOfSend100GreenPassText: NSLayoutConstraint!
    @IBOutlet weak var rightConstantOfViewSend100GreenPassText: NSLayoutConstraint!
    @IBOutlet weak var lblissueGreenpassDate: UILabel!
    @IBOutlet weak var scrollviewPopUpView: UIScrollView!
    var profileImage: UIImage!
    var base64String:String =  ""
    var perofileImgName: String?
    var channelname: String?
    var ChannelColor: String?
    var mediaLink: String?
    var MediaThumb: String?
    var imgChannelProfile: String?
    var mediaType: String?
    var flagImageType: String?
    var skip_media: String?
    var channel_min_color : String?
    var imageurl : String?
    var issueInviteLastDate : String?


    @IBOutlet weak var btnopenMedia: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initialDesign()
        setNavigation()
        setData()
        getInviteSummaryAPI()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        NotificationCenter.default.addObserver(self,selector:#selector(applicationWillEnterForeground(_:)),name:NSNotification.Name.UIApplicationWillEnterForeground,object: nil)

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden  = false
        scrollviewPopUpView.isHidden = true
        setNavigation()
        lblissueGreenpassDate.layer.removeAllAnimations()
        lblissueGreenpassDate.alpha = 1
        ButtonAnimationHelper().blinkView(view: lblissueGreenpassDate)
    }
    
   @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        lblissueGreenpassDate.layer.removeAllAnimations()
        lblissueGreenpassDate.alpha = 1
        ButtonAnimationHelper().blinkView(view: lblissueGreenpassDate)
    }
    
    func setNavigation(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
        
        let lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.textColor = UIColor.white
        
        if UIScreen.main.bounds.size.height <= 568{
            lblOfTitle.font = UIFont.boldSystemFont(ofSize: 15.0)
        }else{
            lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        }

        lblOfTitle.text =  (NSLocalizedString(StringConstants.NavigationTitle.seyInviteSummary, comment: StringConstants.EMPTY))
        navigationItem.titleView = lblOfTitle
        
        navigationController?.navigationBar.barTintColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }

   
    @IBAction func btnUploadPRofileImageClicked(_ sender: Any) {
        flagImageType  =  StringConstants.flags.profileTypeImage
        openImagePicker()
    }
    

    @IBAction func btnOpenMediaClicked(_ sender: Any) {
        
        if(appDelegate.mediaType == StringConstants.MediaType.image){
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            let imgurl = appDelegate.mediaUrl
            let imageUrlByRemovingSpace = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            fullViewOfImageFromMessagesVC.imageDataFromServer = imageUrlByRemovingSpace
            self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
        }else if(appDelegate.mediaType == StringConstants.MediaType.externallink) {
            
            let youtubeUrl = NSURL(string:appDelegate.mediaUrl!)!
            if UIApplication.shared.canOpenURL(youtubeUrl as URL){
                UIApplication.shared.openURL(youtubeUrl as URL)
            } else{
                let youtubeUrl = NSURL(string:appDelegate.mediaUrl!)!
                UIApplication.shared.openURL(youtubeUrl as URL)
            }
        }else{
            let docUrl = appDelegate.mediaUrl
            let finalUrl = docUrl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let url = URL(string: finalUrl)
            let controller = SVWebViewController(url: url)
            controller?.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        
    }
    
    func openImagePicker(){
    let picker = UIImagePickerController()
    picker.delegate = self
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.popUpMenuName.takeApicture, comment: StringConstants.EMPTY)), style: .default, handler: {
    action in
    picker.sourceType = .camera
    self.present(picker, animated: true, completion: nil)
    }))
    alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.photoLibrary, comment: StringConstants.EMPTY)), style: .default, handler: {
    action in
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
    }))
    alert.addAction(UIAlertAction(title: (NSLocalizedString(StringConstants.TextOnAllScreen.cancel, comment: StringConstants.EMPTY)), style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
    }
    
    
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if(flagImageType == StringConstants.flags.profileTypeImage){
               imgProfilePic.contentMode = .scaleAspectFit
               imgProfilePic.image = pickedImage
            }else{
                imgMedia.contentMode = .scaleAspectFit
                imgMedia.image = pickedImage
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSend100FreenGreenPassClick(_ sender: Any) {
        updateInviteSummary()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImage = image.fixOrientation()
            dismiss(animated: true) { [unowned self] in
                if(self.flagImageType == StringConstants.flags.profileTypeImage){
                  self.openEditor(nil)
                }else{
                    self.imgMedia.image = image
                    let imageHelper = ImageHelper()
                    let imageData = imageHelper.compressImage(image: image, compressQuality: 0.4)!
                   self.uploadMediaImagePdfToGreenCloud(isImageSelect: true, imagetoSend: image, imageData: imageData as NSData, isImageOrPDF: StringConstants.MediaType.image)
                }
            }
        }
    }
    
    
    
    @IBAction func openEditor(_ sender: UIBarButtonItem?) {
        guard let image = profileImage else {
            return
        }
      
        let cropper = UIImageCropper(cropRatio: 2/2)
        cropper.delegate = self
        cropper.picker = nil
        cropper.image = image
        cropper.cancelButtonText = "Cancel"
        self.present(cropper, animated: true, completion: nil)
    }
    
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller is DashboardViewController {
                    let _ = nav.popToViewController(controller as UIViewController, animated: false)
                    break
                }
            }
            nav.pushViewController(vc, animated: false)
        }
    }
    
    
    func initializeOverlayView()  {
        
        if !(imgChannelProfile!.isEmpty) && !(txtCompanyName.text!.isEmpty) {
        let dashboardStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil)
        let vc = dashboardStoryboard.instantiateViewController(withIdentifier: "InvitationPreviewPopupVc") as! InvitationPreviewPopupVc
        vc.modalPresentationStyle = .overCurrentContext
        let backgroundScreenShot = captureScreen()
        appDelegate.channelMaxColor = ChannelColor!
        appDelegate.companyName = (txtCompanyName.text)!
        appDelegate.imgprofileLogo = imgChannelProfile!
        vc.backroundImage = backgroundScreenShot!
        self.navigationController?.pushViewController(vc, animated: false)
        //self.present(vc, animated: false, completion: nil)
        }else{
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.All_feildsare_mandatory, comment: StringConstants.EMPTY)))
        }
    }

    func captureScreen() -> UIImage? {
        var window: UIWindow? = UIApplication.shared.keyWindow
        window = UIApplication.shared.windows[0] 
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.isOpaque, 0.0)
        window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
    @objc func btnCloseClicked(sender:UIButton){
        self.overlayView.isHidden = true
    }
    
    @IBAction func btnClourPickerClicked(_ sender: Any) {
      
        let mainStoryboard = UIStoryboard(name: StringConstants.StoryBoardName.main, bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ColorPickerViewController") as! ColorPickerViewController
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func passSelectedColor(_ color: String) {
        print("color123",color)
        viewSetInvite.backgroundColor = hexStringToUIColor(hex: color)
        ChannelColor = color
    }
    
    func initialDesign(){
        self.navigationController?.navigationBar.isHidden  = false

        viewSetInvite.layer.cornerRadius = 8
        viewSetInvite.clipsToBounds = true
    
        viewInvitationPreview.layer.cornerRadius = 4
        viewInvitationPreview.clipsToBounds = true

        viewUploadMedia.layer.cornerRadius = 8
        viewUploadMedia.clipsToBounds = true
        
        
        //MEdia popup
        heightConstraintlayoutofMEdiaPopup.constant = 350
        topConstarintComnstantForMEdiaPopup.constant = 170
        
        viewYoutubeSmall.isHidden = false
        viewYoutubeExpanded.isHidden = true
        
        viewSelectMEdiPopup.layer.cornerRadius = 8
        viewSelectMEdiPopup.clipsToBounds = true

        imgMedia.layer.cornerRadius = 8
        imgMedia.clipsToBounds = true

        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.width/2
        imgProfilePic.clipsToBounds = true
        
        txtYoutubeLink.layer.cornerRadius = 4
        txtYoutubeLink.layer.borderWidth = 1
        
        txtYoutubeLink.layer.borderColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary).cgColor
        
        let attributedString = NSMutableAttributedString.init(string: "Invitation Preview")
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        lblInvitationPreview.attributedText = attributedString

        let attributedString2 = NSMutableAttributedString.init(string: "Change colour")
        attributedString2.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString2.length))
        txtChangeColor.attributedText = attributedString2
        
        
        let attributedString3 = NSMutableAttributedString.init(string: "Know More")
        attributedString3.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString3.length))
        lblKnowMore.attributedText = attributedString3

        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.uploadImageClicked(_:)))
        tap1.cancelsTouchesInView = false
        self.viewUploadImage.addGestureRecognizer(tap1)

        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.uploadDocumentClicked(_:)))
        tap2.cancelsTouchesInView = false
        self.viewUploadDoc.addGestureRecognizer(tap2)

        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.UploadVideoClicked(_:)))
        tap3.cancelsTouchesInView = false
        self.viewYoutubeSmall.addGestureRecognizer(tap3)
        
        if UIScreen.main.bounds.size.height <= 568{
            leftConstantOfViewOfSend100GreenPassText.constant = 10
            rightConstantOfViewSend100GreenPassText.constant = 10
        }else{
            
            leftConstantOfViewOfSend100GreenPassText.constant = 34
            rightConstantOfViewSend100GreenPassText.constant = 34
        }

    }
    
    
    func closePopUp(){
        viewOfBackPopUp.isHidden = true
        scrollviewPopUpView.isHidden = true
        
        UIView.transition(with: viewSelectMEdiPopup,
                          duration: 0.5,
                          options: [.showHideTransitionViews],
                          animations: {
                            self.viewSelectMEdiPopup.isHidden = true},
                          completion: nil)
        
        
        imgUploadDoc.image = UIImage.init(named: StringConstants.ImageNames.docGray)
        lblUploadDoc.textColor = UIColor.darkGray

        imgUploadImage.image = UIImage.init(named: StringConstants.ImageNames.imageGray)
        lbluploadImage.textColor = UIColor.darkGray
        
        //make All others deselecct
        viewYoutubeSmall.isHidden = false
        viewYoutubeExpanded.isHidden = true
        heightConstraintlayoutofMEdiaPopup.constant = 350
        topConstarintComnstantForMEdiaPopup.constant = 170
        
        txtYoutubeLink.text = StringConstants.EMPTY

    }
    
    
    
    @objc func uploadImageClicked(_ sender: UITapGestureRecognizer) {
        print("ImageTapped")
     
        //when select
        imgUploadImage.image = UIImage.init(named: StringConstants.ImageNames.imageGreen)
        lbluploadImage.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        //make All others deselecct
        viewYoutubeSmall.isHidden = false
        viewYoutubeExpanded.isHidden = true
        heightConstraintlayoutofMEdiaPopup.constant = 350
        topConstarintComnstantForMEdiaPopup.constant = 170

        imgUploadDoc.image = UIImage.init(named: StringConstants.ImageNames.docGray)
        lblUploadDoc.textColor = UIColor.darkGray
        
        //open image picker
        openImagePicker()
        flagImageType  =  StringConstants.flags.MediaTypeImage
        closePopUp()
    }
    
    
    
    @objc func uploadDocumentClicked(_ sender: UITapGestureRecognizer) {
        print("DocumentTapped")
        //when select
        imgUploadDoc.image = UIImage.init(named: StringConstants.ImageNames.docGreen)
        lblUploadDoc.textColor = UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)
        
        //make All others deselecct
        viewYoutubeSmall.isHidden = false
        viewYoutubeExpanded.isHidden = true
        heightConstraintlayoutofMEdiaPopup.constant = 350
        topConstarintComnstantForMEdiaPopup.constant = 170

        imgUploadImage.image = UIImage.init(named: StringConstants.ImageNames.imageGray)
        lbluploadImage.textColor = UIColor.darkGray
       
        closePopUp()
        
        
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true {
            let pdf                                 = String(kUTTypePDF)
            //  let docs                                = String(kUTTypeCompositeContent)
            let types                               = [pdf]
            let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = .formSheet
            self.present(documentPicker, animated: true, completion: nil)
        }else{
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.noInterNet, comment: StringConstants.EMPTY)))
        }

    }
    
    @objc func UploadVideoClicked(_ sender: UITapGestureRecognizer) {
        print("video tapped")
        heightConstraintlayoutofMEdiaPopup.constant = 395
        topConstarintComnstantForMEdiaPopup.constant = 128
        viewYoutubeSmall.isHidden = true
        viewYoutubeExpanded.isHidden = false
        
        //set Anotherview All images and text Gray Color
        imgUploadImage.image = UIImage.init(named: StringConstants.ImageNames.imageGray)
        imgUploadDoc.image = UIImage.init(named: StringConstants.ImageNames.docGray)
        
        lblUploadDoc.textColor = UIColor.darkGray
        lbluploadImage.textColor = UIColor.darkGray
    }

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        do{
            
           // let pdfData = try! Data(contentsOf: myURL.asURL())
            //let data : Data = pdfData
            let pdfDocData = try NSData(contentsOf: myURL, options: .mappedIfSafe) as Data
            uploadMediaImagePdfToGreenCloud(isImageSelect:false,imagetoSend:UIImage(),imageData : pdfDocData as NSData, isImageOrPDF:StringConstants.MediaType.document)
            
        }catch{
        }

    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func setData(){
        
        channelname = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.channelName)
        mediaLink = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_link)
        MediaThumb = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.mediathumb)
        ChannelColor = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.channelColour)
        imgChannelProfile = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.channelBigThumb)
        mediaType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.media_type)
        skip_media = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.skip_media)
       channel_min_color = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.channel_min_color)
        imageurl = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.channelBigThumb)
        issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
        let  standard = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.standard)
        let  extra = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.extra)
        let sent = UserDefaults.standard.integer(forKey: StringConstants.NSUserDefauluterKeys.sent)
        let remaining = (standard + extra) - sent
        let intRemainingString = remaining.formattedWithSeparator

        lblInviteBtnText.text = StringConstants.setInviteSummaryViewController.send + StringConstants.singleSpace + intRemainingString + StringConstants.singleSpace + StringConstants.setInviteSummaryViewController.FreeGreenPass
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        
        if let issueInviteLastDaTe = issueInviteLastDate  {
            if !(issueInviteLastDaTe.isEmpty){
                let date: NSDate? = dateFormatterGet.date(from: issueInviteLastDaTe)! as NSDate
                print(dateFormatterPrint.string(from: date! as Date))
            }
        }
        
        
       // issueInviteLastDate = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
        
        if let issueInviteLastDATE = issueInviteLastDate {
            if !(issueInviteLastDATE.isEmpty){
                getDateInFormat(dateStr: issueInviteLastDate!)
            }
        }
        txtCompanyName.text = channelname
        lblCompanyNameCount.text = "(" + String(channelname!.count) + "/25)"

//        if let url = imageurl { //imgChannelProfile
//        let rep2 = url.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
//        let url = NSURL(string: rep2)
//        imgProfilePic.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
//        }
        if let imgProfile = imgChannelProfile  {
            if imgProfile.isEmpty{
                if let imgProfileUrl = imageurl  {
                    imgChannelProfile = imgProfileUrl
                    imgProfilePic.sd_setImage(with: URL(string: imgProfileUrl), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg))
                }
            }else{
                imgProfilePic.sd_setImage(with: URL(string: imgProfile), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg))
            }
        }else{
            if let imgProfile = imageurl  {
                imgChannelProfile = imgProfile
                imgProfilePic.sd_setImage(with: URL(string: imgProfile), placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg))
            }
        }

        viewSetInvite.backgroundColor = hexStringToUIColor(hex: ChannelColor!)

        if mediaLink != nil  && !(mediaLink?.isEmpty)!{
            viewUploadMedia.isHidden = true
            imgMedia.isHidden = false
            btnEditMEdia.isHidden = false
            btnopenMedia.isHidden = false
            let rep2 = MediaThumb!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let url = NSURL(string: rep2)
            imgMedia.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
            
            self.appDelegate.mediThumbUrl = MediaThumb!
            self.appDelegate.mediaUrl = mediaLink
            self.appDelegate.mediaType = mediaType!
            
        }else{
            viewUploadMedia.isHidden = false
            imgMedia.isHidden = true
            btnopenMedia.isHidden = true
            btnEditMEdia.isHidden = true
            self.appDelegate.mediThumbUrl = StringConstants.EMPTY
            self.appDelegate.mediaUrl = StringConstants.EMPTY
            self.appDelegate.mediaType = StringConstants.EMPTY
        }
        
        if let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType) {
            if accountType == StringConstants.TWO{
                btnSkip.isHidden = true
            }else{
                btnSkip.isHidden = false
            }
        }
        
        if(mediaType == StringConstants.MediaType.externallink){
            imgVideoIcon.isHidden = false
        }else{
            imgVideoIcon.isHidden = true
        }
    }
    
    func setdataAfterUpdate(){
        if(!(ChannelColor?.isEmpty)! && !(mediaLink?.isEmpty)!){
           lblDescription.text = "We will send the invitation SMS on your behalf. Set the name and profile photo for invitation purpose."
        }
    }
    
    @IBAction func btnInvitationPreviewClick(_ sender: Any) {
        initializeOverlayView()
    }
    
    @IBAction func btnOpenInvitationPreviewClick(_ sender: Any) {
        print("btnOpenInvitationPreviewClick")
        initializeOverlayView()
    }
    
    @IBAction func btnUploadYoutubeLinkClicked(_ sender: Any) {
        if((txtYoutubeLink.text?.isEmpty)!){
          self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.EmptyText, comment: StringConstants.EMPTY)))

        }else{

            let text = txtYoutubeLink.text
            let types: NSTextCheckingResult.CheckingType = .link
            let detector = try? NSDataDetector(types: types.rawValue)
            guard let detect = detector else {
                return
            }
            let matches = detect.matches(in: text!, options: .reportCompletion, range: NSMakeRange(0, text!.characters.count))
            if matches.count > 0{
                var urlString = String()
                for match in matches {
                    print(match.url!)
                    urlString = match.url!.absoluteString
                }
                if let youTubeId = getYoutubeId(youtubeUrl: urlString) {
                    //call API
                    if youTubeId == StringConstants.EMPTY{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.validYoutubeLink, comment: StringConstants.EMPTY)))
                    }else{
                        uploadYoutubeeUrlToGreenCloud(youtubeUrl: txtYoutubeLink.text!)
                        closePopUp()
                    }
                }else{
               self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.validYoutubeLink, comment: StringConstants.EMPTY)))
                }
            }else{
            self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.Validation.validYoutubeLink, comment: StringConstants.EMPTY)))
            }
        }
    }
    
    
    
    func getDateInFormat(dateStr:String){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        let date: Date? = dateFormatterGet.date(from: dateStr)! as Date
        
        dateFormatterPrint.dateFormat = date!.dateFormatWithSuffix()
        print(dateFormatterPrint.string(from: date!))
        
        lblissueGreenpassDate.text = StringConstants.kisanMitra.before + StringConstants.singleSpace + dateFormatterPrint.string(from: date! as Date)
        
    }
    
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        let url = youtubeUrl
        var youtubrID = String()
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            if ((match) != nil){
                let range = match?.range(at: 0)
                youtubrID = (url as NSString).substring(with: range!)
                print(youtubrID)
            }else{
                youtubrID = StringConstants.EMPTY
            }
        } catch {
            youtubrID = StringConstants.EMPTY
        }
        return youtubrID
    }
    
    
    
    @IBAction func btnKnowMoreCicked(_ sender: Any) {
       
        let knowMoreVc = self.storyboard?.instantiateViewController(withIdentifier: "KnowMoreViewController") as! KnowMoreViewController
        self.navigationController?.pushViewController(knowMoreVc, animated: true)
        
    }

    
    @IBAction func btnEditMediaClicked(_ sender: Any) {
        viewOfBackPopUp.isHidden = false
        scrollviewPopUpView.isHidden = false

        UIView.transition(with: viewSelectMEdiPopup,
                          duration: 0.5,
                          options: [.curveEaseInOut],
                          animations: {
                            self.viewSelectMEdiPopup.isHidden = false},
                          completion: nil)
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
         updateInviteSummary()
    }
    
    func updateInviteSummary(){
        
        txtCompanyName.text =  txtCompanyName.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        channelname = txtCompanyName.text
        let accountType = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.accountType)
        let account_Type = Int(accountType)
        if account_Type == 1{
            if mediaLink!.isEmpty{
                if !txtCompanyName.text!.isEmpty && imgProfilePic.image != UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg) && !(self.imgChannelProfile?.isEmpty)!{
                    //Kisan mitra without media
                    let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
                    let params = UpdateSummaryWithoutMediaRequest.convertToDictionary(updateSummaryWithoutMediaDetails: UpdateSummaryWithoutMediaRequest.init( app_key: URLConstant.GREENPASS_APPKEY, channel_big_thumb: imgChannelProfile!, channel_max_color: ChannelColor!, channel_min_color: channel_min_color!, channel_name: channelname!, eventCode: URLConstant.eventCode, skip_media: StringConstants.ONE, sessionId: sessionId, source: RequestName.loginSourceName, type: RequestName.kisanMita))
                    UpdateInviteSummaryWithoutMediaRequest(params:params)
                }else{
                    self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.All_feildsare_mandatory, comment: StringConstants.EMPTY)))
                }
            }else{
                //kisan mitra media
                if !txtCompanyName.text!.isEmpty && imgProfilePic.image != UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg) && !(self.imgChannelProfile?.isEmpty)!{
                let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
                //UpdateSummaryWithMediaRequest
                let params = UpdateSummaryWithMediaRequest.convertToDictionary(updateSummaryWithMediaRequest: UpdateSummaryWithMediaRequest.init(app_key: URLConstant.GREENPASS_APPKEY, channel_big_thumb: imgChannelProfile!, channel_max_color: ChannelColor!, channel_min_color: channel_min_color!, channel_name: channelname!, eventCode: URLConstant.eventCode, /*skip_media: "",*/ sessionId: sessionId, source: RequestName.loginSourceName, type: RequestName.kisanMita, media_link: mediaLink!, media_thumbnail: MediaThumb!, media_type: mediaType!))

                UpdateInviteSummaryWithoutMediaRequest(params:params)
            }else{
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.All_feildsare_mandatory, comment: StringConstants.EMPTY)))
            }
            }
        }else{
            //for exhibitor
            
            if !txtCompanyName.text!.isEmpty && imgProfilePic.image != UIImage(named: "defaultChannel")! && !mediaLink!.isEmpty && !(self.imgChannelProfile?.isEmpty)!{

            let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
            //UpdateSummaryWithMediaRequest
            let params = UpdateSummaryForExhibitorRequest.convertToDictionary(updateSummaryForExhibitorRequest: UpdateSummaryForExhibitorRequest.init(app_key: URLConstant.GREENPASS_APPKEY, community_logo_big_thumb_url: imgChannelProfile!, community_logo_small_thumb_url: imageurl!, community_logo_url: imageurl!, community_name: channelname!, community_theme_max_color: ChannelColor!, community_theme_min_color: channel_min_color!, eventCode: URLConstant.eventCode, media_link: mediaLink!, media_thumbnail: MediaThumb!, media_type: mediaType!, sessionId: sessionId, source: RequestName.loginSourceExhibitorName, type: RequestName.ExhibitorInvite))
            UpdateInviteSummaryWithoutMediaRequest(params:params)
            }else{
                self.navigationController?.view.makeToast((NSLocalizedString(StringConstants.ToastViewComments.All_feildsare_mandatory, comment: StringConstants.EMPTY)))
            }
        }
    }
    
    func UpdateInviteSummaryWithoutMediaRequest(params : Dictionary<String, Any>){
       print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.updateinvitesummaryinfo
        _ = UpdateInviteSummaryServices().updateInviteSummary(apiURL,
                                      postData: params as [String : AnyObject],
                                      withSuccessHandler: { (userModel) in
                                        let model = userModel as! UpdateInviteSummaryResponse
                                        if model.success == true {
                                            let model = userModel as! UpdateInviteSummaryResponse
                                            print("message",model.message!)
                                            let  inviteFriendBaseVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendBaseVC") as! InviteFriendBaseVC
                                            self.navigationController?.pushViewController(inviteFriendBaseVC, animated: true)
                                        }else{
                                            MBProgressHUD.hide(for: self.view, animated: true);
                                            self.popupAlert(title: "", message: StringConstants.AlertMessage.errorOccured, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                                                },{action2 in
                                                }, nil])
                                        }
                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [StringConstants.AlertMessage.btnOK], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    @IBAction func btnUploadMediaClicked(_ sender: Any) {
        
        viewOfBackPopUp.isHidden = false
        scrollviewPopUpView.isHidden = false
        UIView.transition(with: viewSelectMEdiPopup,
                          duration: 0.5,
                          options: [.showHideTransitionViews],
                          animations: {
                            self.viewSelectMEdiPopup.isHidden = false},
                          completion: nil)
    }
    
    
    
    @IBAction func btnClosClicked(_ sender: Any) {
         closePopUp()
    }

    
    func UploadUserProfileImageToGreenCloud(isImageSelect:Bool,imagetoSend:UIImage){
      
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let appKey =  URLConstant.app_key
        
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadProfilePicToGreenCloud1 + URLConstant.uploadProfilePicToGreenCloud2 + appKey + URLConstant.uploadProfilePicToGreenCloud3 + sessionId + URLConstant.uploadProfilePicToGreenCloud4 + StringConstants.MediaType.image
        print("ApiUrl",apiURL)
        let params = ["":""]
        let fileName = StringConstants.file
        _ = Repository().requestPostURLForUploadImage(apiURL, isImageSelect: true,
                                                      fileName:fileName,
                                                      params: params as [String : AnyObject],
                                                      image: imagetoSend,
                                                      successHandler: { (uploadResponse) in
                                                        print(uploadResponse)
                                                        let imageDetails = uploadResponse.imageDetails
                                                        let imageDetailValue = imageDetails[0]
                                                        print("ImageLink",imageDetailValue?.imgFullUrl as Any)
                                                       
                                                        if let url = imageDetailValue?.imgFullUrl{
                                                            let rep2 = url.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                                                            let url = NSURL(string: rep2)
                                                            self.imgProfilePic.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                                                        }
                                                        
                                                        
                                                        MBProgressHUD.hide(for: self.view, animated: true);
                                                        self.appDelegate.imgprofileLogo = (imageDetailValue?.imgFullUrl)!
                                                        self.appDelegate.mediaUrl = imageDetailValue?.imgMaxColor
                                                        self.viewSetInvite.backgroundColor = hexStringToUIColor(hex: (imageDetailValue?.imgMaxColor)!)
                                                        
                                                        self.imgChannelProfile = (imageDetailValue?.imgFullUrl)!
                                                        self.ChannelColor = imageDetailValue?.imgMaxColor

                                                        
        }, failureHandler: { (true,error) in
            print("error",error)
             MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
        
    }
    

    func uploadMediaImagePdfToGreenCloud(isImageSelect:Bool,imagetoSend:UIImage,imageData : NSData, isImageOrPDF:String){
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false

       let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
       let appKey =  URLConstant.app_key
        var mediatype = String()
        if(isImageOrPDF ==  StringConstants.MediaType.image){
            mediatype = StringConstants.MediaType.image
        }else{
            mediatype = StringConstants.MediaType.document
        }
       let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadMediaToGreenCloud1 + URLConstant.uploadMediaToGreenCloud2 + appKey + URLConstant.uploadMediaToGreenCloud3 + sessionId + URLConstant.uploadMediaToGreenCloud4 + mediatype
        
        print("ApiUrl",apiURL)
        let params = ["":""]
        let fileName = StringConstants.file
        _ = Repository().requestPostURLForUploadMediaImageToGreenCloud(apiURL, isImageSelect: isImageSelect,
                                                      fileName:fileName,
                                                      params: params as [String : AnyObject],
                                                      image: imagetoSend, imageData: imageData,
                                                      successHandler: { (uploadResponse) in
                                                        print(uploadResponse)
                                                        let imageDetails = uploadResponse.imageDetails
                                                        let imageDetailValue = imageDetails[0]
                                                        self.mediaLink = imageDetailValue?.mediaUrl
                                                        self.MediaThumb = imageDetailValue?.mediaThumbUrl
                                                        self.mediaType = imageDetailValue?.mediaType
                                                        print("ImageLink",imageDetailValue?.mediaUrl as Any)
                                                        MBProgressHUD.hide(for: self.view, animated: true);

                                                        self.appDelegate.mediThumbUrl = (imageDetailValue?.mediaThumbUrl)!
                                                        self.appDelegate.mediaUrl = imageDetailValue?.mediaUrl
                                                        self.appDelegate.mediaType = (imageDetailValue?.mediaType)!
                                                        
                                                        if let url = imageDetailValue?.mediaThumbUrl{
                                                            let rep2 = url.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                                                            let url = NSURL(string: rep2)
                                                            self.imgMedia.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                                                        }
                                                        self.imgVideoIcon.isHidden = true
                                                        self.UpdateUI()

                                                        
        }, failureHandler: { (true,error) in
            print("error",error)
             MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
    
    
    
    
    //API call to Upload Youtube Video
    func uploadYoutubeeUrlToGreenCloud(youtubeUrl:String){
        
        let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.isUserInteractionEnabled = false

        let mediatype = StringConstants.MediaType.externallink
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let appKey =  URLConstant.app_key

        let params = UploadMediaToGreenCloudRequest.convertToDictionary(uploadMediaToGreenCloudRequest: UploadMediaToGreenCloudRequest.init(app_key: appKey, sessionId: sessionId, type: mediatype, url: youtubeUrl))
        
         print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.uploadMediaToGreenCloud1
        _ = UploadMediaToGreenCloudServices().uploadMediaToGreenCloud(apiURL,postData: params as [String :AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! UploadSetInvitationMediaImageToGreenCloud
                                                        let imageDetails = model.imageDetails
                                                        let imageDetailValue = imageDetails[0]
                                                        self.mediaLink = imageDetailValue?.mediaUrl
                                                        self.MediaThumb = imageDetailValue?.mediaThumbUrl
                                                        self.mediaType = imageDetailValue?.mediaType
                                                        
                                                        MBProgressHUD.hide(for: self.view, animated: true);

                                                        self.appDelegate.mediThumbUrl = (imageDetailValue?.mediaThumbUrl)!
                                                        self.appDelegate.mediaUrl = imageDetailValue?.mediaUrl
                                                        self.appDelegate.mediaType = (imageDetailValue?.mediaType)!
                                                        
                                                        if let url = imageDetailValue?.mediaThumbUrl{
                                                            let rep2 = url.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                                                            let url = NSURL(string: rep2)
                                                            self.imgMedia.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                                                        }
                                                        
                                                        self.imgVideoIcon.isHidden = false
                                                        self.UpdateUI()

        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);

        })
    }

    
    func UpdateUI(){
        viewUploadMedia.isHidden = true
        imgMedia.isHidden = false
        btnEditMEdia.isHidden = false
        btnopenMedia.isHidden = false
        self.setdataAfterUpdate()
    }

    //API call to get Invite Summary
    func getInviteSummaryAPI(){
        
        let userName = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.username)
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)

        //set Source and filter for both exhibitor and Kisan Mitra
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var resource = String()
        var  source = String()
        
        if accountType == StringConstants.ONE {
            resource = RequestName.kisanMita
            source = RequestName.loginSourceName
        }else{
            resource = RequestName.ExhibitorInvite
            source = RequestName.loginSourceExhibitorName
        }
        
        let filter = [URLConstant.filter1: resource, URLConstant.filter2: userName]
        let params = InviteSummuryRequest.convertToDictionary(inviteSummuryRequest:InviteSummuryRequest.init(eventCode:URLConstant.eventCode , source: source, pagesize: 3, currentpage: 0, sort_col: "", app_key: URLConstant.app_key, sessionId: sessionId, filter: filter as [String : AnyObject]))
        print("params",params)
        let apiURL = URLConstant.GREENPASS_BASE_URL + URLConstant.inviteSummary
        _ = InviteSummaryServices().getInviteSummary(apiURL,postData: params as [String : AnyObject],
                                                     withSuccessHandler: { (userModel) in
                                                        let model = userModel as! InviteSummaryResponse
                                                        print("model",model)
                                                        
                                                        let records = model.records
                                                        let inviteSummary = records[0]
                                                        
                                                        let settings = model.settings
                                                        let inviteSetting = settings[0]

                                                        var standard =  Int()
                                                        var extra = Int()
                                                        var sent = Int()
                                                        
                                                        let medialink =  inviteSummary.media_link
                                                        let mediatype = inviteSummary.media_type
                                                        let skip_media = inviteSummary.skip_media
                                                        let channelName = inviteSummary.channel_name
                                                        let channelColour = inviteSummary.channel_max_color
                                                        let channel_min_color = inviteSummary.channel_min_color
                                                        let mediathumb = inviteSummary.media_thumbnail
                                                        let imagechannelProfile = inviteSummary.channel_big_thumb
                                                        let imageurl = inviteSummary.imageurl
                                                        let issueGreenpasslastDate = inviteSetting.issue_greenpass_last_date
                                                        let greenpassAcceptLast_date = inviteSetting.greenpass_accept_last_date
                                                        
                                                        if(accountType == StringConstants.TWO){
                                                            if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 1) {
                                                                standard =  inviteSummary.unlimited_standard!
                                                                extra = inviteSummary.unlimited_extra!
                                                                sent = inviteSummary.unlimited_sent!
                                                            }else if (inviteSetting.show_issue_greenpass == 1 && inviteSetting.show_issue_unlimited_greenpass == 0){
                                                                standard =  inviteSummary.quota_standard!
                                                                extra = inviteSummary.quota_extra!
                                                                sent = inviteSummary.quota_sent!
                                                            }
                                                        }else{
                                                            standard =  inviteSummary.unlimited_standard!
                                                            extra = inviteSummary.unlimited_extra!
                                                            sent = inviteSummary.unlimited_sent!
                                                        }
                                                        
                                                        let show_issue_greenpass = inviteSetting.show_issue_greenpass
                                                        
                                                        UserDefaults.standard.set(show_issue_greenpass, forKey: StringConstants.NSUserDefauluterKeys.show_issue_greenpass)
                                                        
                                                        if inviteSetting.show_issue_unlimited_greenpass != nil {
                                                            UserDefaults.standard.set(inviteSetting.show_issue_unlimited_greenpass!, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                        }else{
                                                            UserDefaults.standard.set(StringConstants.EMPTY, forKey: StringConstants.NSUserDefauluterKeys.show_issue_unlimited_greenpass)
                                                        }

                                                        
                                                        UserDefaults.standard.set(standard, forKey: StringConstants.NSUserDefauluterKeys.standard)
                                                        
                                                        UserDefaults.standard.set(extra, forKey:
                                                            StringConstants.NSUserDefauluterKeys.extra)
                                                        
                                                        UserDefaults.standard.set(sent, forKey:
                                                            StringConstants.NSUserDefauluterKeys.sent)
                                                        
                                                        
                                                        UserDefaults.standard.set(medialink, forKey: StringConstants.NSUserDefauluterKeys.media_link)
                                                        
                                                        UserDefaults.standard.set(mediatype, forKey: StringConstants.NSUserDefauluterKeys.media_type)
                                                        
                                                        UserDefaults.standard.set(skip_media, forKey: StringConstants.NSUserDefauluterKeys.skip_media)
                                                        
                                                        UserDefaults.standard.set(issueGreenpasslastDate, forKey: StringConstants.NSUserDefauluterKeys.issueGreenpasslastDate)
                                                        UserDefaults.standard.set(greenpassAcceptLast_date, forKey: StringConstants.NSUserDefauluterKeys.greenpassAcceptLast_date)
                                                        
                                                        UserDefaults.standard.set(channelName, forKey: StringConstants.NSUserDefauluterKeys.channelName)
                                                        
                                                        UserDefaults.standard.set(channelColour, forKey: StringConstants.NSUserDefauluterKeys.channelColour)
                                                        
                                                        UserDefaults.standard.set(mediathumb, forKey: StringConstants.NSUserDefauluterKeys.mediathumb)
                                                        
                                                        UserDefaults.standard.set(imagechannelProfile, forKey: StringConstants.NSUserDefauluterKeys.channelBigThumb)
                                                        
                                                        UserDefaults.standard.set(channel_min_color, forKey: StringConstants.NSUserDefauluterKeys.channel_min_color)
                                                        
                                                        UserDefaults.standard.set(imageurl, forKey: StringConstants.NSUserDefauluterKeys.imageurl)


                                                        self.setData()
                                                        
        }, withFailureHandlere: { (error: String) in
            print("error",error)
        })
    }

}


extension SetInviteSummaryViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? StringConstants.EMPTY
        //guard let stringRange = Range(range, in: currentText) else { return false }
        //        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        //        lblCompanyNameCount.text = "(" + String(updatedText.count) + "/25)"
        var updatedText = String()
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            updatedText = currentText.replacingCharacters(in: textRange, with: string)
            lblCompanyNameCount.text = "(" + String(updatedText.count) + "/25)"
        }
        
        //Not allowed type emoji
        if #available(iOS 7, *){
            if textField.isFirstResponder {
                if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                    // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
                    return false
                }
            }
        }
        
        let validString = NSCharacterSet(charactersIn: "!@#$%^*_+{}[]|\"<>,~`/:;?-=\\¥£•¢₹")
        if let range = string.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            return false
        }
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
            return false
        } else {
            
            guard range.location == 0 else {
                return updatedText.count <= 24
            }
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
            
            if newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location == 0{
                return false
            }else{
                return updatedText.count <= 24
            }
        }
    }
    
}


extension SetInviteSummaryViewController: UIImageCropperProtocol {
    
    func didCropImage(originalImage: UIImage?, croppedImage: UIImage?) {
        imgProfilePic.image = croppedImage
        let _:NSData = UIImageJPEGRepresentation(imgProfilePic.image!, 0.7)! as NSData
        UploadUserProfileImageToGreenCloud(isImageSelect: true, imagetoSend: croppedImage!)
        picker.dismiss(animated: true, completion: nil)
    }
    
    //optional
    func didCancel() {
        picker.dismiss(animated: true, completion: nil)
        print("did cancel")
    }
}
