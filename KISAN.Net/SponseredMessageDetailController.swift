//
//  SponseredMessageDetailController.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 29/01/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVFoundation
import AVKit
import MediaPlayer

class SponseredMessageDetailController: UIViewController {

    @IBOutlet var lblReceiveUpdate: UILabel!
    @IBOutlet var tblSponseredMessages: UITableView!
    var myChatsData = MyChatsTable()
    var lblOfTitle = UILabel()
    @IBOutlet var btnfFollowChannel: UIButton!
    
    @IBOutlet weak var heightOfFollowChannelViewConstant: NSLayoutConstraint!
    let attrs = [
        NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13.0),
        NSAttributedStringKey.foregroundColor : UIColor.black,
        NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any] as [NSAttributedStringKey : Any]
    let attrs1 = [NSAttributedStringKey.foregroundColor : UIColor.gray]
    let documentDirectoryHelper = DocumentDirectoryHelper()
    var userId = String()
    @IBOutlet weak var viewOfFollowBtn: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblSponseredMessages.register(UINib(nibName: "PostMesaageViewCell", bundle:nil), forCellReuseIdentifier: "PostMesaageViewCell")
        tblSponseredMessages.rowHeight = UITableViewAutomaticDimension
        tblSponseredMessages.estimatedRowHeight = 30
        userId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.userId)
        btnfFollowChannel.setTitle(NSLocalizedString(StringConstants.CreatePostViewController.FollowChannel, comment: StringConstants.EMPTY), for: .normal)
        lblReceiveUpdate.text = NSLocalizedString(StringConstants.CreatePostViewController.receiveUpdateandStartDialog, comment: StringConstants.EMPTY)
       
        if (myChatsData.isMember ==  StringConstants.ONE){
            viewOfFollowBtn.isHidden = true
            heightOfFollowChannelViewConstant.constant = 0
        }else{
            viewOfFollowBtn.isHidden = false
            heightOfFollowChannelViewConstant.constant = 74
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarAtScreenLoadTime()
        self.view.backgroundColor = UIColor.lightGray
    }
    
    
    @IBAction func followChannelClicked(_ sender: Any) {
        
        //let followingDetails = self.communityList[buttonRow]
        let communityKey = myChatsData.communityKey!
        let checkInternetConnection = isInternetAvailableHelper.isInternetAvailable()
        if checkInternetConnection == true{
            let spinnerActivity =  MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.isUserInteractionEnabled = false
            var myChats = [MyChatsTable]()
            myChats.append(myChatsData)
            //---------------- Update isMember  value as true for follow channel & also at database----------//
            myChats.filter({$0.communityKey == communityKey}).first?.isMember = StringConstants.ONE
            myChats.filter({$0.communityKey == communityKey && $0.isSponsoredMessage == StringConstants.ZERO}).first?.messageText = NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: "")
            
            // Update MyChat List
            //let followingData = myChats[0]
            var  dict  = ConvertToDictOfMyChatObj.ConvertToDictOfMyChatObj(myChatDetails: myChats)
            // Set follower count after following channel
            
            
           // let myChatCommunityKey = MyChatServices.sharedInstance.checkMyChatCommunityKey(communityKey: communityKey)
            // dict.updateValue(followingData.communityWithRssFeed ?? String(), forKey: "feed")
            
            let currentTime = DateTimeHelper.getCurrentMillis()
            dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.messageAt)
            dict.updateValue(String(currentTime), forKey: StringConstants.NSUserDefauluterKeys.last_activity_datetime)
            CommunityDetailService.sharedInstance.updateAtCommunityDetailTable(communityDetail: dict)

            let checkCommunityKeyWithisSponsoredFlag = MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdTrue(communityKey: communityKey, isSponsoredMessage: myChatsData.isSponsoredMessage!, id: myChatsData.id!)
            let checkCommunityKeyWithisNormalMychatFlag =  MyChatServices.sharedInstance.checkMychatCommunityKeyAndiSSponserdFalse(communityKey: communityKey, isSponsoredMessage: StringConstants.ZERO)
            
            if(checkCommunityKeyWithisSponsoredFlag.count > 0) && (checkCommunityKeyWithisNormalMychatFlag.count == 0){
                dict.updateValue(StringConstants.MessageType.chatMessage, forKey: "messageType")
                dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")
                dict.updateValue(StringConstants.MessageType.text, forKey: "mediaType")

               // MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
            MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
                dict.updateValue(StringConstants.ZERO, forKey: "isSponsoredMessage")
                dict.updateValue(NSLocalizedString(StringConstants.DashboardViewController.follwStatus, comment: ""), forKey: "messageText")
                MyChatServices.sharedInstance.sendMessageAtMyChatTable(communityDetail: dict, isFirstTime: false,iSCommingFromOneTOneinitiate:false)
            }else{
                
                     //MyChatServices.sharedInstance.updateAtMyChatTable(communityDetail: dict)
                MyChatServices.sharedInstance.updateMyChatTableForSponsoredMessages(latestMsg:StringConstants.DashboardViewController.follwStatus,latestMessageTime:String(currentTime),communityKey:communityKey,isMember:StringConstants.ONE)
                
            }
            self.myChatsData = myChats[0]
            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi()
            }
            
        }else{
            //call API for Following channel
            DispatchQueue.global(qos: .background).async {
                self.followChannelApi()}
        }
        
  
    }

    func setNavigationBarAtScreenLoadTime(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.navigationBar.backItem?.title = StringConstants.NavigationTitle.back
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:StringConstants.ImageNames.backArrowImage), style: .plain, target: self, action: #selector(btnBackTapped))
        lblOfTitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblOfTitle.textColor = UIColor.white
        lblOfTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        navigationItem.titleView = lblOfTitle
        lblOfTitle.text = myChatsData.communityName
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        if myChatsData.feed == StringConstants.ONE{
            navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }else{
            if let communityDominantColour = myChatsData.communityDominantColour{
                navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
        }
    }
    
    @objc func btnBackTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func labelAction(gesture: UITapGestureRecognizer)
    {
        let indexPath = IndexPath(row: gesture.view!.tag, section: 0)
        let cell = tblSponseredMessages.cellForRow(at: indexPath) as! PostMesaageViewCell
        if (myChatsData.lastMessageContentField2?.length)! > 60{
            if cell.heightConstraintOfDescriptionView.constant == 55{
                tblSponseredMessages.beginUpdates()
                cell.heightConstraintOfDescriptionView.priority = UILayoutPriority(rawValue: 250)
                let attributedString2 = NSMutableAttributedString(string:NSLocalizedString(StringConstants.less, comment: StringConstants.EMPTY), attributes:attrs)
                let strDescrption = myChatsData.lastMessageContentField2! + StringConstants.EMPTY
                let attrString = NSMutableAttributedString(string:strDescrption)
                attrString.addAttributes(attrs1, range: NSMakeRange(0, attrString.length))
                attributedString2.addAttributes(attrs, range: NSMakeRange(0, attributedString2.length))
                attrString.append(attributedString2)
                cell.txtviewOfPostDescription.attributedText = attrString
                cell.heightConstraintOfDescriptionView.constant = cell.txtviewOfPostDescription.frame.height
                tblSponseredMessages.endUpdates()
            }else{
                tblSponseredMessages.beginUpdates()
                cell.heightConstraintOfDescriptionView.priority = UILayoutPriority(rawValue: 999)
                let attributedString2 = NSMutableAttributedString(string:NSLocalizedString(StringConstants.More, comment: StringConstants.EMPTY), attributes:attrs)
                let strDescrption = myChatsData.lastMessageContentField2! + StringConstants.ShowMore
                var substring1 = String()
                if UIScreen.main.bounds.size.width <= 320{
                    let index1 = strDescrption.index(strDescrption.startIndex, offsetBy: 35)
                    substring1 = String(strDescrption[..<index1])
                }else{
                    let index1 = strDescrption.index(strDescrption.startIndex, offsetBy: 60)
                    substring1 = String(strDescrption[..<index1])
                }
                substring1 = substring1 + StringConstants.ShowMore
                let attrString = NSMutableAttributedString(string:String(substring1))
                attrString.addAttributes(attrs1, range: NSMakeRange(0, attrString.length))
                attrString.append(attributedString2)
                cell.txtviewOfPostDescription.attributedText = attrString
                cell.heightConstraintOfDescriptionView.constant = 55
                tblSponseredMessages.endUpdates()
            }
            self.view.layoutIfNeeded()
        }else{
            //when text length is leass than 30
        }
    }
    
    func followChannelApi (){
        
        let followingDetails = self.myChatsData
        let communityKey = followingDetails.communityKey!
        let communityJabberId = followingDetails.communityJabberId!
        
        let params = FollowChannelRequest.convertToDictionary(followChannelDetails: FollowChannelRequest.init(user_role: "member", i_jabber_id:communityJabberId))
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.followCommunity1 + communityKey + URLConstant.followCommunity2
        _ = FollowChannelServices().followChannelServices(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (followChannelRespo) in
                                                            let model = followChannelRespo as! FollowChannelResponse
                                                            print("model",model)
                                                            
                                                            //---------------------------------------------------------------------------//
                                                            // Get old messages after follow channel
                                                            DispatchQueue.global(qos: .background).async {
                                                                self.getOldMessage(communityKey:communityKey)
                                                            }
                                                           
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
                MBProgressHUD.hide(for: self.view, animated: true);
                self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])}
        })
        
    }
    
    
    //Request Callback API
    func requestCallBackAPI (sender:UIButton,messageId:String, SponsoredMessgeId:String){
        let followingDetails = self.myChatsData
        let params = ["":""]
        print("params",params)
        let apiURL = URLConstant.BASE_URL + URLConstant.callback1 + messageId + URLConstant.callback2 + SponsoredMessgeId
        _ = CallBackAPIServices().getAppStarterData(apiURL,
                                                          postData: params as [String : AnyObject],
                                                          withSuccessHandler: { (callBackApiRepo) in
                                                            let model = callBackApiRepo as! CallBackApiResponse
                                                            print("model",model)
                                                            
                                                            //Update UI
                                                            let point = self.tblSponseredMessages.convert(sender.center, from:  sender.superview)
                                                            let indexPath = self.tblSponseredMessages.indexPathForRow(at: point)
                                                            print("indexPath ==",indexPath?.row ?? AnyObject.self)
                                                            let messageInfo = self.myChatsData
                                                            
                                                            // let cell = sender.superview?.superview as? PostMesaageViewCell
                                                            let cell = self.tblSponseredMessages!.cellForRow(at: indexPath!) as? PostMesaageViewCell
                                                            cell?.lblOfThankYou.isHidden = false
                                                            cell?.lblTime.isHidden = false
                                                            cell?.imgViewOfRequestCallBackIcon.isHidden = true
                                                            cell?.lblRequestCallBackTitle.isHidden = true
                                                            cell?.lblRequestCallBackDescription.isHidden = true
                                                            cell?.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
                                                            cell?.btnRequestCallBack.isEnabled = false
                                                            cell?.lblOfThankYou.text = StringConstants.CreatePostViewController.thankYouMsg  + messageInfo.communityName!
                                                            cell?.lblTime.text = DateTimeHelper.getTimeAgo(time:String(DateTimeHelper.getCurrentMillis()))
                                                            let id =  model.callbackRequestId!
                                                       
                                                         MyChatServices.sharedInstance.updatemessageIdForSponsoredMessageCallmefunctionality(id: id, communityKey: followingDetails.communityKey!,messageId :messageInfo.id!)


                                                            
                                                            
        }, withFailureHandlere: { (error: String) in
            DispatchQueue.main.async {
                print("error",error)
                MBProgressHUD.hide(for: self.view, animated: true);
                self.popupAlert(title: "", message: StringConstants.AlertMessage.alreadySent, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                    },{action2 in
                    }, nil])}
        })
        
    }
    
    
  
    func getOldMessage (communityKey:String){
        let currentTime = DateTimeHelper.getCurrentMillis()
        let apiURL = URLConstant.BASE_URL + URLConstant.getOldMessages1 + communityKey + URLConstant.getOldMessages2 + "10" + URLConstant.getOldMessages3 + "1" + URLConstant.getOldMessageOfBroadcast  + String(currentTime)
        let params = ["communityKey":communityKey]
        _ = GetOldMessagesServices().getOldMessagesList(apiURL,
                                                        postData: params as [String : AnyObject],
                                                        withSuccessHandler: { (userModel) in
                                                            let model = userModel as! GetOldMessageResponse
                                                            print("model",model)
                                                            MBProgressHUD.hide(for: self.view, animated: true);
                                                            DispatchQueue.main.async {
                                                                if self.myChatsData.isMember! == StringConstants.ONE{
                                                                    let channelSendBroadcastMsgVC = self.storyboard?.instantiateViewController(withIdentifier: "ChannelSendBroadcastMsgVC") as! ChannelSendBroadcastMsgVC
                                                                    //channelSendBroadcastMsgVC.ownerId = myChats.ownerId!
                                                                    channelSendBroadcastMsgVC.communityName = self.myChatsData.communityName!
                                                                    channelSendBroadcastMsgVC.communityKey = self.myChatsData.communityKey!
                                                                    channelSendBroadcastMsgVC.communityImageSmallThumbUrl = self.myChatsData.communityImageSmallThumbUrl!
                                                                    channelSendBroadcastMsgVC.myChatsData = self.myChatsData
                                                                    channelSendBroadcastMsgVC.commingFrom = StringConstants.flags.SponseredDetailCOntroller
                                                                    // Change unreadMessagesCount to zero
                                                                    MyChatServices.sharedInstance.unreadMessagesCountAtMyChatTable(unreadMessagesCount: StringConstants.ZERO, communityKey: self.myChatsData.communityKey!)
                                                                    self.navigationController?.pushViewController(channelSendBroadcastMsgVC, animated: true)
                                                                }else{
                                                                    
                                                                }
                                                            }
        }, withFailureHandlere: { (error: String) in
            print("error",error)
            MBProgressHUD.hide(for: self.view, animated: true);
            self.popupAlert(title: "", message: error, actionTitles: [(NSLocalizedString(StringConstants.AlertMessage.btnOK, comment: StringConstants.EMPTY))], actions:[{action1 in
                },{action2 in
                }, nil])
        })
    }
}


extension SponseredMessageDetailController :UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostMesaageViewCell", for: indexPath) as! PostMesaageViewCell
        cell.heightConstraintOfDescriptionView.priority = UILayoutPriority(rawValue: 999)
        cell.heightConstraintOfDescriptionView.constant = 55
        cell.heightConstraintOfLblSponsored.constant = 18
        cell.lblsponsored.text = NSLocalizedString(StringConstants.Sponsered, comment: StringConstants.EMPTY)
        cell.ViewOncell.backgroundColor = UIColor.white
        if (myChatsData.lastMessageContentField2?.length)! >= 60{

            cell.heightConstraintOfDescriptionView.priority = UILayoutPriority(rawValue: 999)
            let attributedString1 = NSMutableAttributedString(string:StringConstants.EMPTY, attributes:attrs)
            let attributedString2 = NSMutableAttributedString(string:NSLocalizedString(StringConstants.More, comment: StringConstants.EMPTY), attributes:attrs)
            let strDescrption = myChatsData.lastMessageContentField2! + StringConstants.ShowMore
            var substring1 = String()
            if UIScreen.main.bounds.size.width <= 320{
                let index1 = strDescrption.index(strDescrption.startIndex, offsetBy: 40)
                substring1 = String(strDescrption[..<index1])
            }else{
                let index1 = strDescrption.index(strDescrption.startIndex, offsetBy: 60)
                substring1 = String(strDescrption[..<index1])
            }
            substring1 = substring1 + StringConstants.ShowMore
            let attrString = NSMutableAttributedString(string:String(substring1))
            attrString.addAttributes(attrs1, range: NSMakeRange(0, attrString.length))
            attrString.append(attributedString1)
            attrString.append(attributedString2)
            cell.txtviewOfPostDescription.attributedText = attrString
            cell.heightConstraintOfDescriptionView.constant = 55
            self.view.layoutIfNeeded()
        }else{
            cell.txtviewOfPostDescription.text = myChatsData.lastMessageContentField2
        }
       // cell.heightConstantOfBtnPromoPost.constant = 0
        //heightOfViewOfBackRequestCallBack.constant
        cell.videIcon.isHidden = true
        cell.btnPromotePost.isHidden = true
        cell.viewOfBackRequestCallBack.isHidden = false
        
        cell.lblPOstTitl.text = myChatsData.messageText
        let time = DateTimeHelper.getTimeAgo(time: String(describing: myChatsData.messageAt!))
        cell.lblOfDateTime.text  = time
        
        //set channel profile Image
        cell.lblOfChannelName.text = myChatsData.messageText
        let imgurl = myChatsData.communityImageBigThumbUrl
        let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
        let url = NSURL(string: rep2)
        cell.imgChannelProfile.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
        if let communityName = myChatsData.communityName, !communityName.isEmpty {
            cell.lblOfChannelName.text = communityName
        }
        
        cell.imgChannelProfile.layer.cornerRadius = cell.imgChannelProfile.frame.size.width / 2
        cell.imgChannelProfile.clipsToBounds = true
        cell.selectionStyle = .none
        
        //postimageOrVideo
        if myChatsData.mediaType == StringConstants.MediaType.video{
            cell.videIcon.isHidden = false
            cell.btnPlayVideo.isHidden = false
            if let imgurl = myChatsData.lastMessageContentField8 {
                let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                let url = NSURL(string: rep2)
                //cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                let imageView = UIImageView()
                imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                let size = CGSize(width: 248, height: 248)
                let imageHelper = ImageHelper()
                cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
            }else{
                cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg)
            }
        }else{
            cell.videIcon.isHidden = true
            cell.btnPlayVideo.isHidden = true
            if let imgurl = myChatsData.lastMessageContentField3 {
                let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                let url = NSURL(string: rep2)
               // cell.imgpostimage.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                let imageView = UIImageView()
                imageView.sd_setImage(with: url! as URL, placeholderImage:UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg), options: .refreshCached)
                let size = CGSize(width: 248, height: 248)
                let imageHelper = ImageHelper()
                cell.imgpostimage.image = imageHelper.imageCenterCrop(with: imageView.image, scaledToFill: size)
            }else{
                cell.imgpostimage.image = UIImage(named: StringConstants.ImageNames.imageDefaultMediaPartPlaceHolderImg)
            }
        }
        // cell.HeightConstraintForPostimage.constant = 200
        cell.btnPromotePost.isHidden = true
        cell.lblOfSeprator.isHidden = true
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.labelAction(gesture:)))
        cell.txtviewOfPostDescription.addGestureRecognizer(tap)
        cell.txtviewOfPostDescription.isUserInteractionEnabled = true
        cell.txtviewOfPostDescription.tag = indexPath.row
        cell.ViewOncell.layer.cornerRadius = 8
        cell.ViewOncell.clipsToBounds =  true
        cell.selectionStyle = .none
        cell.btnPlayVideo.isHidden = false
        // ------------------------- Download & Open image and Download video & Play video --------------- //
        cell.btnPlayVideo.tag = indexPath.row + 10
        cell.btnPlayVideo.addTarget(self,action:#selector(btnPlayVideo(sender:)), for: .touchUpInside)
        // -----------------------------------------//----------------------------------------------- //
        
       /* if let communityDominantColour = myChatsData.communityDominantColour{
            cell.contentView.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            tableView.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
        }else{
            cell.contentView.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            tableView.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
        }*/
        
        
        cell.contentView.backgroundColor = UIColor.lightGray
        tableView.backgroundColor = UIColor.lightGray

        tap.delegate = self
        if  UIScreen.main.bounds.size.height <= 568{
            cell.HeightConstraintForPostimage.constant = 200
            
            cell.lblRequestCallBackTitle.font = UIFont.systemFont(ofSize: 15)
            cell.lblRequestCallBackDescription.font = UIFont.systemFont(ofSize: 6)
            cell.lblOfThankYou.font = UIFont.systemFont(ofSize: 13)
            cell.lblTime.font = UIFont.systemFont(ofSize: 10)

        }
        
        cell.btnRequestCallBack.tag = indexPath.row + 20
        cell.btnRequestCallBack.addTarget(self,action:#selector(btnRequestCallBack(sender:)), for: .touchUpInside)

        //handle show hide of call me button according to status
        let followingDetails = self.myChatsData
        let messageId = followingDetails.id!
        let strArray = messageId.components(separatedBy: ":")
        
        if ((self.myChatsData.request_callback_id?.isEmpty)!){
            cell.lblOfThankYou.isHidden = true
            cell.lblTime.isHidden = true
            cell.imgViewOfRequestCallBackIcon.isHidden = false
            cell.lblRequestCallBackTitle.isHidden = false
            cell.lblRequestCallBackDescription.isHidden = false
            // cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            if let communityDominantColour = myChatsData.communityDominantColour{
                cell.viewOfBackRequestCallBack.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }
            
            cell.btnRequestCallBack.isEnabled = true
        }else{
            cell.lblOfThankYou.isHidden = false
            cell.lblTime.isHidden = false
            cell.imgViewOfRequestCallBackIcon.isHidden = true
            cell.lblRequestCallBackTitle.isHidden = true
            cell.lblRequestCallBackDescription.isHidden = true
            cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            cell.lblTime.text = DateTimeHelper.getTimeAgo(time: myChatsData.request_callback_time!)
            cell.btnRequestCallBack.isEnabled = false
            cell.lblOfThankYou.text = StringConstants.CreatePostViewController.thankYouMsg + followingDetails.communityName!
        }
       /* if(strArray.count == 3){
            cell.lblOfThankYou.isHidden = false
            cell.lblTime.isHidden = false
            cell.imgViewOfRequestCallBackIcon.isHidden = true
            cell.lblRequestCallBackTitle.isHidden = true
            cell.lblRequestCallBackDescription.isHidden = true
            cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            cell.btnRequestCallBack.isEnabled = false
            cell.lblOfThankYou.text = StringConstants.CreatePostViewController.thankYouMsg + followingDetails.communityName!
        }else{
            cell.lblOfThankYou.isHidden = true
            cell.lblTime.isHidden = true
            cell.imgViewOfRequestCallBackIcon.isHidden = false
            cell.lblRequestCallBackTitle.isHidden = false
            cell.lblRequestCallBackDescription.isHidden = false
           // cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            if let communityDominantColour = myChatsData.communityDominantColour{
                cell.viewOfBackRequestCallBack.backgroundColor = hexStringToUIColor(hex: communityDominantColour)
            }else{
                cell.viewOfBackRequestCallBack.backgroundColor = UIColor.init(hexString: ColorConstants.colorPrimaryString)
            }

            cell.btnRequestCallBack.isEnabled = true

        }*/
        return cell
    }
    
    @objc func btnRequestCallBack(sender:UIButton){
        
        /*let point = self.tblSponseredMessages.convert(sender.center, from: sender.superview)
        let indexPath = self.tblSponseredMessages.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = self.myChatsData
        
        // let cell = sender.superview?.superview as? PostMesaageViewCell
        let cell = self.tblSponseredMessages!.cellForRow(at: indexPath!) as? PostMesaageViewCell
        cell?.lblOfThankYou.isHidden = false
        cell?.lblTime.isHidden = false
        cell?.imgViewOfRequestCallBackIcon.isHidden = true
        cell?.lblRequestCallBackTitle.isHidden = true
        cell?.lblRequestCallBackDescription.isHidden = true
        cell?.viewOfBackRequestCallBack.backgroundColor = UIColor.init(displayP3Red: 251.0/255.0, green: 255.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        cell?.btnRequestCallBack.isEnabled = false
        cell?.lblOfThankYou.text = StringConstants.CreatePostViewController.thankYouMsg + "<" + messageInfo.communityName! + ">"*/
        
        let followingDetails = self.myChatsData
        let messageId = followingDetails.id!
        var strArray = messageId.components(separatedBy: ":")
        if(strArray.count == 2){
          let sponsoredMessageId = strArray[0]
          let messageId = strArray[1]
          self.requestCallBackAPI(sender: sender,messageId: messageId,SponsoredMessgeId: sponsoredMessageId)
        }
    }
    
    
    @objc func btnPlayVideo(sender:UIButton){
        let point = tblSponseredMessages.convert(sender.center, from: sender.superview)
        let indexPath = tblSponseredMessages.indexPathForRow(at: point)
        print("indexPath ==",indexPath?.row ?? AnyObject.self)
        let messageInfo = myChatsData
        
        if messageInfo.mediaType == StringConstants.MediaType.image{
            let fullViewOfImageFromMessagesVC = UIStoryboard(name: StringConstants.StoryBoardName.dashboard, bundle: nil).instantiateViewController(withIdentifier: "FullViewOfImageFromMessagesVC") as! FullViewOfImageFromMessagesVC
            
            
            let messageId = messageInfo.id
            let imageName1 = "/" + StringConstants.documentDirectory.documentDirectoryName + "/" + S3UploadKeyName + String(describing: messageId!) + ".png"
            let isExist = documentDirectoryHelper.checkImageORVideoIsPresent(imageName: imageName1)
            print(isExist)
            //fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.lastMessageContentField3!
            
            if let contentField3 = messageInfo.lastMessageContentField3{
                fullViewOfImageFromMessagesVC.imageDataFromServer = contentField3
            }
            
            if isExist{
                fullViewOfImageFromMessagesVC.imageDataFromLocalDB = imageName1
                self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
            }else{
                if userId == messageInfo.ownerId{
                    fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.lastMessageContentField3!
                    fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                }else{
                    if messageInfo.messageType != StringConstants.MediaType.post{
                        fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                    }else{
                        fullViewOfImageFromMessagesVC.imageDataFromServer = messageInfo.lastMessageContentField3!
                        fullViewOfImageFromMessagesVC.imageDataFromLocalDB = StringConstants.EMPTY
                        self.navigationController?.pushViewController(fullViewOfImageFromMessagesVC, animated: false)
                    }
                }
            }
            
        }else{
            let rep2 = myChatsData.lastMessageContentField3!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
            let videoURL = NSURL(string:rep2)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
}
