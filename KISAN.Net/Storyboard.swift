//
//  Storyboard.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 12/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

extension UIStoryboard {
    
    //Main
    static func Main() -> UIStoryboard {
        return UIStoryboard(name:"Main", bundle: Bundle.main)
    }
    
    //BarcodeFlow
    static func BarcodeFlow() -> UIStoryboard {
        return UIStoryboard(name:"BarcodeFlow", bundle: Bundle.main)
    }
}
