//
//  UIView+Extension.swift
//  Find A Fix Provider
//
//  Created by Lemo-ssd on 10/04/18.
//  Copyright © 2018 Lemosys. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var cornerRound: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
//            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor{
        
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius:CGFloat{
        get {
            return layer.shadowRadius
        }set{
            layer.shadowColor = UIColor.darkGray.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
       func applyBorder(colours: UIColor) -> Void {
            self.layer.borderColor = colours.cgColor
            self.layer.borderWidth = 1.5
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        }
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        
        
    }
    
    func cornerRadiusWithShadow(shadowRadius: CGFloat, cornerRadius: CGFloat) {
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
//        shadowLayer.fillColor = self.layer.backgroundColor
        
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = shadowRadius
        
        self.layer.insertSublayer(shadowLayer, at: 0)
//        self.layer.backgroundColor = UIColor.clear.cgColor
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        self.layer.shadowOpacity = 0.4
//        self.layer.shadowRadius = shadowRadius
//
//        // set the cornerRadius of the containerView's layer
//        let containerView = UIView()
//        containerView.frame=self.layer.frame
//        containerView.layer.cornerRadius = cornerRadius
//        containerView.layer.masksToBounds = true
//
//        self.addSubview(containerView)
//
//        //
//        // add additional views to the containerView here
//        //
//
//        // add constraints
//        containerView.translatesAutoresizingMaskIntoConstraints = false
//
//        // pin the containerView to the edges to the view
//        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
//
//
    }
}
