//
//  VerifyUoW.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 15/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import Foundation
public class VerifyUoW {
   
     var baseRepository: BaseRepository!
    
    init() {
        
    }
    
    class func verify(token:String) -> Dictionary<String, Any> {
        return VerifyRepository.verifyRepository(token:token)
    }
    
}
