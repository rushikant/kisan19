//
//  "sending back from webview to specific view controller in iOS" "sending back from webview to specific view controller in iOS" UploadProfileImageToGreenCloudServices.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 27/09/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import Alamofire

class UploadMediaToGreenCloudServices: Repository {
    

    func uploadMediaToGreenCloud(_ baseUrl: String,
                             postData: [String: AnyObject],
                             withSuccessHandler success: CompleteionHandler?,
                             withFailureHandlere failure: FailureHandler?) -> DataRequest? {
        self.completeionBlock = success
        self.failureBlock = failure
        return self.POST(baseUrl, postData: postData, requestName: RequestName.UploadLogoToGreenCloud)
    }


    override func successCallback(_ response: AnyObject, headers: [AnyHashable : Any], requestName: String,postData: [String: AnyObject]) {

        if requestName == RequestName.UploadLogoToGreenCloud {
            if let responseDict =  response as? [String: AnyObject] {
                print("success",responseDict["success"] ?? [String: AnyObject]())
                if responseDict["success"] as? Int == 1 {
                    
                    var mediaUploadRespo: UploadSetInvitationMediaImageToGreenCloud = UploadSetInvitationMediaImageToGreenCloud.initializeUploadSetInvitationMediaImageToGreenCloud()
                    mediaUploadRespo = UploadSetInvitationMediaImageToGreenCloud.uploadSetInvitationMediaImageToGreenCloudFromDictionary(responseDict)

                    print("GrrenCloudmediaUploadResponse",mediaUploadRespo)
                    if let success = self.completeionBlock {
                        success(mediaUploadRespo)
                    }else{
                        if let failure = self.failureBlock {
                            if let message  = responseDict["message"] as? String{
                                failure(message)
                            }else{
                                failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                            }
                        }
                    }
                }
                else {
                    if let failure = self.failureBlock {
                        if let message  = responseDict["message"] as? String{
                            failure(message)
                        }else{
                            failure((NSLocalizedString(StringConstants.ToastViewComments.failureMsg, comment: StringConstants.EMPTY)))
                        }
                    }
                }
            }
        }
    }

    override func failureCallback(_ error: NSError, requestName: String) {
        if let failure = self.failureBlock {
            failure(error.localizedDescription)
        }
    }

}
