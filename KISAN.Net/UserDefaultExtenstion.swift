//
//  UserDefaultExtenstion.swift
//  KISAN.Net
//
//  Created by iariana macbook3 on 05/12/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static var UNSYNCED_BROCHURE_RECORD_KEY = "UnsyncedBrochureRecord"
    
    static func storeUnsyncedBrochureRecord(record: ScanBrochureRequest) {
        // Retrieve the records first
        var brochures = getUnsyncedBrochureRecord()
        if UserDefaults.standard.value(forKey: UNSYNCED_BROCHURE_RECORD_KEY) != nil {
            UserDefaults.standard.removeObject(forKey: UNSYNCED_BROCHURE_RECORD_KEY)
        }
        brochures.append(record)
        
        var records = [[String: AnyObject]]()
        for brochure in brochures {
            let param = ScanBrochureRequest.convertToDictionary(request: brochure)
            records.append(param as [String : AnyObject])
        }
        UserDefaults.standard.set(records, forKey: UNSYNCED_BROCHURE_RECORD_KEY)
        UserDefaults.standard.synchronize()
    }
    
    static func getUnsyncedBrochureRecord() -> [ScanBrochureRequest] {
        var brochures = [ScanBrochureRequest]()
        
        if (UserDefaults.standard.value(forKeyPath: UNSYNCED_BROCHURE_RECORD_KEY) != nil) {
            let list : [[String:AnyObject]] = UserDefaults.standard.value(forKeyPath: UNSYNCED_BROCHURE_RECORD_KEY) as! [[String : AnyObject]]
            for item in list {
                let brochure = ScanBrochureRequest.getScanBrochureRequestFromDictionary(item)
                brochures.append(brochure)
            }
        }
        return brochures
    }
    
    static func getUnsyncedSubmissionRequest() -> ScanBrochureRequest {
        let app_key = URLConstant.app_key
        let eventCode = URLConstant.eventCode
        let sessionId = SessionManagerForSaveData.retriveDataFromSessionManager(keyOfRetriveData: StringConstants.NSUserDefauluterKeys.sessionId)
        let accountType = UserDefaults.standard.string(forKey: StringConstants.NSUserDefauluterKeys.accountType)
        var source = String()
        if accountType == StringConstants.ONE {
            source = RequestName.loginSourceName
        }else{
            source = RequestName.loginExhibitorSourceName
        }
        let brochures = getUnsyncedBrochureRecord()
        var scans = [ScanData]()
        for brochure in brochures {
            let data = brochure.data
            for scan in data! {
                let scanData = ScanData(shortcode:scan.shortcode, scanned_date_time: scan.scanned_date_time)
                scans.append(scanData)
            }
        }
        let scanRequest = ScanBrochureRequest(app_key: app_key, eventCode: eventCode, sessionId: sessionId, source: source, data: scans)
        return scanRequest
    }
    
    static func deleteUnsyncedBrochureRecord() -> Bool {
        // remove privious obj
        if UserDefaults.standard.value(forKey: UNSYNCED_BROCHURE_RECORD_KEY) != nil {
            UserDefaults.standard.removeObject(forKey: UNSYNCED_BROCHURE_RECORD_KEY)
        }
        UserDefaults.standard.synchronize()
        return true
    }
}
