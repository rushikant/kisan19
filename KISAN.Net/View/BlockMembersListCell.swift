//
//  BlockMembersListCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 01/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class BlockMembersListCell: UITableViewCell {

    @IBOutlet weak var imageViewOfBlockMemProfile: UIImageView!
    @IBOutlet weak var lblBlockMemName: UILabel!
        @IBOutlet weak var btnUnBlock: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
