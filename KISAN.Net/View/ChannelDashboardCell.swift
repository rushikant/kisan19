//
//  ChannelDashboardCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 17/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class ChannelDashboardCell: UITableViewCell {
    @IBOutlet weak var imageViewOfFollower: UIImageView!
    @IBOutlet weak var lblNameOfFollower: UILabel!
    @IBOutlet weak var lblTextMessage: UILabel!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var lblOnCell: UILabel!
    @IBOutlet weak var viewOnCell: UIView!
    @IBOutlet weak var imageOfShowMessageTypeLatestMsg: UIImageView!
    @IBOutlet weak var leadingOflblLatestMsg: NSLayoutConstraint!
    @IBOutlet var widtContraintForMediaImg: NSLayoutConstraint!
    @IBOutlet var heightconstraintforMediaimg: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
