//
//  ChannelFollowerListCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 31/01/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ChannelFollowerListCell: UITableViewCell {

    @IBOutlet weak var lblOfFollowerName: UILabel!
    @IBOutlet weak var imageViewOfFollowerProfile: UIImageView!
    @IBOutlet var btnInitializeOnetoOneChat: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
