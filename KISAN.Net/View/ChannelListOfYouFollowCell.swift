//
//  ChannelListOfYouFollowCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 13/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ChannelListOfYouFollowCell: UITableViewCell {

    @IBOutlet weak var imageviewOfUserProfile: UIImageView!
    @IBOutlet weak var lblUserRole: UILabel!
    @IBOutlet weak var lblChannelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
