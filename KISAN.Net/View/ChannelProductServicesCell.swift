//
//  ChannelProductServicesCell.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 27/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class ChannelProductServicesCell: UICollectionViewCell {
    @IBOutlet weak var imageViewOfShowPlayIcon: UIImageView!
    @IBOutlet weak var imageViewOfShowThumbImg: UIImageView!
    @IBOutlet weak var btnOpenMediaClicked: UIButton!
}
