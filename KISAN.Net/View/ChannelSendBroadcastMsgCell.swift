//
//  ChannelSendBroadcastMsgCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 18/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class ChannelSendBroadcastMsgCell: UITableViewCell {

    @IBOutlet weak var txtViewOfMessageText: UITextView!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var widthOfStatusImageView: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfStatus: UIImageView!
    @IBOutlet weak var leftConstantOfViewBackChatMsg: NSLayoutConstraint!
    @IBOutlet weak var bubbleImgeView: UIImageView!
    @IBOutlet weak var rightConstantOfViewBackChatMsg: NSLayoutConstraint!
    @IBOutlet weak var heightOfViewOnCell: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
