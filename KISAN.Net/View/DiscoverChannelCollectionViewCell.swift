//
//  DiscoverChannelCollectionViewCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 10/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class DiscoverChannelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewOfBackChannelImg: UIView!
    @IBOutlet weak var viewOnCell: UIView!
    @IBOutlet weak var viewOfBaner: UIView!
    @IBOutlet weak var imageViewOfChannel: UIImageView!
    @IBOutlet weak var lblChannelName: UILabel!
    @IBOutlet weak var lblFollwersCount: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblOfOwnership: UILabel!
}
