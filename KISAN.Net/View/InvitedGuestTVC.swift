//
//  InvitedGuestTVC.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 16/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class InvitedGuestTVC: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    
    @IBOutlet weak var contactImg: UIImageView!
    
    @IBOutlet weak var statusImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
