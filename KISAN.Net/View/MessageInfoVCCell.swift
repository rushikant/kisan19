//
//  MessageInfoVCCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 12/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MessageInfoVCCell: UITableViewCell {

    @IBOutlet weak var imageViewOfProfile: UIImageView!
    @IBOutlet weak var lblMessageSendStatus: UILabel!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var lblNameOfMessageSender: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
