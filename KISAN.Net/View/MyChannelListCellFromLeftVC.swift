//
//  MyChannelListCellFromLeftVC.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 10/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class MyChannelListCellFromLeftVC: UITableViewCell {

    @IBOutlet weak var imageViewOfChannel: UIImageView!
    @IBOutlet weak var lblOfChannelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
