//
//  MyChatListTblCell.swift
//  KisanChat
//
//  Created by Kisan-MAC on 09/08/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class MyChatListTblCell: UITableViewCell {

    @IBOutlet weak var imgCommunityProfile: UIImageView!
    @IBOutlet weak var lblCommunityType: UILabel!
    @IBOutlet weak var lblCommunityName: UILabel!
    @IBOutlet weak var lblMessageDateTime: UILabel!
    @IBOutlet weak var lblLatestMessage: UILabel!
    @IBOutlet weak var leadingOflblLatestMsg: NSLayoutConstraint!
    @IBOutlet weak var imageOfShowMessageTypeLatestMsg: UIImageView!
    @IBOutlet weak var imgOfMsgUnReadIndicator: UIImageView!
    @IBOutlet var leftConstantOfLatestMsgTypeImgView: NSLayoutConstraint!
    @IBOutlet var widthOfLatestMsgTypeImgView: NSLayoutConstraint!
    
    @IBOutlet weak var topOfLatestMesgLabel: NSLayoutConstraint!
    @IBOutlet var heightOfLatestMsgTypeImgView: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
