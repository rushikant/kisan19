//
//  MyGreenPassTVC.swift
//  KISAN.Net
//
//  Created by MacMini on 11/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class MyGreenPassTVC: UITableViewCell {
    
    @IBOutlet weak var imgUpdatepic: UIImageView!
    @IBOutlet weak var btnEditPhoto: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var qrCodeLbl: UILabel!
    @IBOutlet weak var qrCodeImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var multientryImgView: UIView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var bottonLineLbl: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var smallView: UIView!
    
    @IBOutlet weak var imgStamp: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
