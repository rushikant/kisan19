//
//  MyGreenPassTVC2.swift
//  KISAN.Net
//
//  Created by Mac 3.0 on 14/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class MyGreenPassTVC2: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var qrCodeLbl: UILabel!
    @IBOutlet weak var qrCodeImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var typeCView: UIView!
    @IBOutlet weak var btnEditPhoto: UIButton!
    
    @IBOutlet weak var imgUpdatePic: UIImageView!
    
    @IBOutlet weak var imgStamp: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
