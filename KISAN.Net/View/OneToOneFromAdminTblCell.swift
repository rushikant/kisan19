//
//  OneToOneFromAdminTblCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 05/09/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class OneToOneFromAdminTblCell: UITableViewCell {

    @IBOutlet weak var bubbleImgeView: UIImageView!
    @IBOutlet weak var viewOfBackTextChatMsg: UIView!
    //@IBOutlet weak var lblOfChatMessage: UILabel!
    @IBOutlet weak var txtViewOfChatMessage: UITextView!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var imageviewOfStatus: UIImageView!
    @IBOutlet weak var rightConstantOfViewOfChatTxtMsg: NSLayoutConstraint!
    @IBOutlet weak var leftConstantOfViewOfChatTxtMsg: NSLayoutConstraint!
    @IBOutlet var widthOfStatusImageView: NSLayoutConstraint!
    @IBOutlet weak var rightOcnstantOflblDateTime: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
