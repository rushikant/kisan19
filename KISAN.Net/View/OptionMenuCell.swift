//
//  OprtionMenuCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 20/12/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class OptionMenuCell: UITableViewCell {
   
    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet weak var imageViewOfOption: UIImageView!
    @IBOutlet weak var widthOfMenuImageView: NSLayoutConstraint!
    @IBOutlet weak var leadingOfOptinMenuImgView: NSLayoutConstraint!
    @IBOutlet weak var heightOfOptionMenuImg: NSLayoutConstraint!
    @IBOutlet weak var topOfOptionImageView: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
