//
//  PIPViewCell.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 06/02/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class PIPViewCell: UITableViewCell {

    @IBOutlet weak var viewOnCell: UIView!
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var txtViewOfLink: UITextView!
    @IBOutlet weak var lblOfDescription: UILabel!
    @IBOutlet weak var lblOfTitle: UILabel!
    @IBOutlet weak var imageViewOfThumb: UIImageView!
    @IBOutlet weak var trailingOfViewOnCell: NSLayoutConstraint!
    @IBOutlet weak var leadingOfViewOnCell: NSLayoutConstraint!
    @IBOutlet weak var trailingOfthumbImage: NSLayoutConstraint!
    @IBOutlet weak var leadingOfThumbImg: NSLayoutConstraint!
    @IBOutlet weak var leadingOfDescriptionView: NSLayoutConstraint!
    @IBOutlet weak var btnOpenVideoInPipModeClick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
