//
//  PendingInvitesTVC.swift
//  KISAN.Net
//
//  Created by MacMini on 11/10/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class PendingInvitesTVC: UITableViewCell {
    
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var termConditionBtn: UIButton!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var invitedNameLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    @IBOutlet weak var infoLbl1: UILabel!
    @IBOutlet weak var infoLbl2: UILabel!
    @IBOutlet weak var heightConstraintConstantOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintConstantOfCenterView: NSLayoutConstraint!
    @IBOutlet weak var lblAcceptDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
