//
//  RSSFeedTblCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 25/07/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class RSSFeedTblCell: UITableViewCell {

    @IBOutlet weak var lblFeedTitle: UILabel!
    @IBOutlet weak var imageViewOfFeed: UIImageView!
    @IBOutlet weak var lblFeedMsg: UILabel!
    @IBOutlet weak var lblFeedTime: UILabel!
    @IBOutlet var viewOnCell: UIView!
    @IBOutlet var lblSourceName: UILabel!
    @IBOutlet var imgSourceIMage: UIImageView!
    @IBOutlet var btnreadMore: UIButton!
    @IBOutlet var lblReadMore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
