//
//  SelectLanguageTblCell.swift
//  KisanChat
//
//  Created by Niranjan Deshpande on 12/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class SelectLanguageTblCell: UITableViewCell {

    @IBOutlet weak var lblOfLanguages: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
