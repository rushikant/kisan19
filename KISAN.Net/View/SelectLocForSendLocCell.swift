//
//  SelectLocForSendLocCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SelectLocForSendLocCell: UITableViewCell {
    @IBOutlet weak var lblOfDetailAddress: UILabel!
    @IBOutlet weak var lblOfTitleAddress: UILabel!
    @IBOutlet weak var imageViewOfAddressIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
