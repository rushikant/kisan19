//
//  SelectMultiMediaPopUpCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 15/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SelectMultiMediaPopUpCell: UITableViewCell {

    @IBOutlet weak var imageViewOfMedia: UIImageView!
    @IBOutlet weak var lblOfMediaName: UILabel!
    @IBOutlet var imgWidthconstant: NSLayoutConstraint!
    @IBOutlet var imgHeightConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
