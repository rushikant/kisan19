//
//  SetAudioMediaPartCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 19/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SetAudioMediaPartCell: UITableViewCell {
    @IBOutlet weak var imageViewOfStatus: UIImageView!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var btnAudioPlay: UIButton!
    @IBOutlet weak var leftConstantOfBackViewOfBubleImage: NSLayoutConstraint!
    @IBOutlet weak var widthConstantOfStatusImg: NSLayoutConstraint!
    @IBOutlet weak var rightConstantOfBackViewOfBubleImage: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfShowMediaStatus: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
