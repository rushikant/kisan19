//
//  SetCameraMediaPartCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 17/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SetCameraMediaPartCell: UITableViewCell {
    @IBOutlet weak var ViewOnCell: UIView!
    @IBOutlet weak var bubbleImgeView: UIImageView!
    @IBOutlet weak var imageViewOfSatus: UIImageView!
    @IBOutlet weak var lblCaptionText: UILabel!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var imgViewOfSelectedImage: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var leftConstantOfBubbleImgView: NSLayoutConstraint!
    @IBOutlet weak var rightConstantOfBubbleImgView: NSLayoutConstraint!
    @IBOutlet weak var widthOfStatusImg: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfShowMediaStatus: UIImageView!
    @IBOutlet weak var imageViewOfShowMediaType: UIImageView!
    @IBOutlet weak var rightConstantOfSelectedImage: NSLayoutConstraint!
    @IBOutlet weak var leftConstantOfSelectedImage: NSLayoutConstraint!
    @IBOutlet weak var leadingOfBackViewOfSelectedImage: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
