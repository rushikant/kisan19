//
//  SetLocationPartCell.swift
//  KISAN.Net
//
//  Created by KISAN TEAM on 20/02/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SetLocationPartCell: UITableViewCell {
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var lblDetailAddress: UILabel!
    @IBOutlet weak var lblTitleAddress: UILabel!
    @IBOutlet weak var rightConstantViewOfBackBubbleImg: NSLayoutConstraint!
    @IBOutlet weak var leftConstantViewOfBackBubbleImg: NSLayoutConstraint!
    @IBOutlet weak var widthOfStatusImage: NSLayoutConstraint!
    @IBOutlet weak var imageViewOfStastus: UIImageView!
    @IBOutlet weak var btnOpenGoogleMap: UIButton!
    @IBOutlet weak var lblOfDateTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
