//
//  SetPDFDOCCell.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 30/10/18.
//  Copyright © 2018 KISAN TEAM. All rights reserved.
//

import UIKit

class SetPDFDOCCell: UITableViewCell {

    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var leftConstatntOfbackViewOfBubbleImg: NSLayoutConstraint!
    @IBOutlet weak var rightConstantOfBackViewOfBubbleImg: NSLayoutConstraint!
    @IBOutlet weak var rightConstantOfMediaTypeImage: NSLayoutConstraint!
    @IBOutlet weak var leftConstantOfMediaTypeImage: NSLayoutConstraint!
    @IBOutlet weak var btnofDownloadMedia: UIButton!
    @IBOutlet weak var imgeViewOfDownloadIcon: UIImageView!
    @IBOutlet weak var lblOfDateTime: UILabel!
    @IBOutlet weak var lblOfMediaName: UILabel!
    @IBOutlet weak var imageViewOfThumbOfMedia: UIImageView!
    @IBOutlet weak var viewOnCell: UIView!
    @IBOutlet weak var documentTypeImageView: UIImageView!
    @IBOutlet weak var documentTypeImageOnBanerImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
