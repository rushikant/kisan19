//
//  StallListCell.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 19/11/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class StallListCell: UITableViewCell {

    @IBOutlet weak var imageViewIfRedioBtn: UIImageView!
    @IBOutlet weak var lblStallName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
