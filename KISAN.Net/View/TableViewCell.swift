//
//  TableViewCell.swift
//  FeteApp
//
//  Created by MacMini104 on 03/06/19.
//  Copyright © 2019 Lemosys. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    
    @IBOutlet weak var contactImg: UIImageView!
    
    @IBOutlet var checkBoxBtn: UIButton!
    
    @IBOutlet var crossBtn: UIButton!
    
    @IBOutlet weak var heightConstraintConstantOfErrorLbl: NSLayoutConstraint!
    
    @IBOutlet weak var lblErrorMessage: UILabel!
    
    @IBOutlet weak var viewBackgroudBoreder: UIView!
    
    @IBOutlet weak var imgAlreadySent: UIImageView!
    
    @IBOutlet weak var imgSingleEntry: UIImageView!
    
    @IBOutlet weak var heightOfImgAlreadySentInvitation: NSLayoutConstraint!
    @IBOutlet weak var widthOfImgAlreadySentInvitation: NSLayoutConstraint!
    @IBOutlet weak var topOfImgImgAlreadySentInvitation: NSLayoutConstraint!
    @IBOutlet weak var trailingOfImgAlreadySentInvitaion: NSLayoutConstraint!
    
    @IBOutlet weak var widthOfImageOfPassType: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}
