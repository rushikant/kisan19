//
//  UserAreaInterestTblCell.swift
//  KisanChat
//
//  Created by KISAN TEAM on 25/07/17.
//  Copyright © 2017 Niranjan Deshpande. All rights reserved.
//

import UIKit

class UserAreaInterestTblCell: UITableViewCell {

    @IBOutlet weak var viewOnContentView: UIView!
    @IBOutlet weak var imageViewOfInterest: UIImageView!
    @IBOutlet weak var lblofDetailsTxt: UILabel!
    @IBOutlet weak var imageViewOfCheckBox: UIImageView!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
