//
//  NotificationViewController.swift
//  Notification Extention
//
//  Created by Kisan Forum on 08/01/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {
    @IBOutlet var label: UILabel?
    @IBOutlet weak var imageViewOfNotificationTypeIcon: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var ImageVewOfChannelProfile: UIImageView!
    @IBOutlet weak var imageViewOfProduct: UIImageView!
    @IBOutlet weak var viewOfBackMessageNotification: UIView!
    @IBOutlet weak var viewOfBckSponserdNotification: UIView!
    @IBOutlet weak var bottomOfBackMessagenotifsctionView: NSLayoutConstraint!
    
    @IBOutlet weak var viewOfOverlayChatMessages: UIView!
    @IBOutlet var lblCompanyNameForlink: UILabel!
    @IBOutlet var lblDescriptionoflink: UILabel!
    @IBOutlet var lblUpdate: UILabel!
    @IBOutlet var imgChannelProfilelink: UIImageView!
    @IBOutlet weak var imageViewOfPlayVideo: UIImageView!
    @IBOutlet weak var viewofBackLinkTextNoti: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
        ImageVewOfChannelProfile.layer.cornerRadius = ImageVewOfChannelProfile.frame.size.width / 2
        ImageVewOfChannelProfile.clipsToBounds = true
        imgChannelProfilelink.layer.cornerRadius = imgChannelProfilelink.frame.size.width / 2
        imgChannelProfilelink.clipsToBounds = true

    }
    
    @available(iOSApplicationExtension 10.0, *)
    func didReceive(_ notification: UNNotification) {
        let content = notification.request.content
        if let flagValue = content.userInfo[StringConstants.PushNotificationAction.pushNotificationaAtion] as? String {
            if flagValue == StringConstants.PushNotificationAction.Followchannel {
                 showCustomnotificationwithimage(notification:notification)
            }else if flagValue == StringConstants.PushNotificationAction.sponsored {
                showCustoNotificationOfSponsoredMsg(notification:notification)
            }else if flagValue == StringConstants.PushNotificationAction.link{
                if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.mediaUrl] as? String {
                    if urlImageString == StringConstants.EMPTY{
                        showCustomNotificationWithoutimage(notification:notification)
                    }else{
                        showCustomnotificationwithimage(notification:notification)
                        print(urlImageString)
                    }
                }else{
                    showCustomNotificationWithoutimage(notification:notification)
                }
            }else if flagValue == StringConstants.PushNotificationAction.upgrade{
                 bottomOfBackMessagenotifsctionView.constant = 0
                viewOfBckSponserdNotification.isHidden = true
                viewOfBackMessageNotification.isHidden = false
                lblCompanyNameForlink.text = StringConstants.NavigationTitle.dashboard
               if let companyDescription = content.userInfo[StringConstants.PushNotificationAction.description] as? String {
                    lblDescriptionoflink.text = companyDescription
                }
            }else if  flagValue == StringConstants.PushNotificationAction.message{
                viewOfBckSponserdNotification.isHidden = true
                viewOfBackMessageNotification.isHidden = true
              
                
                //showOverlayNotificationforPostRssFeedMsges(notification:notification)

                //*********************For Post Messages & RSS Feed & image & Video**************
                if (content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.audio) {
                    showOverlayAudioNotification(notification:notification)
                }else if (content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.post) ||  (content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.feed){
                    showOverlayNotificationforPostRssFeedMsges(notification:notification)
                }else if ((content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.image) || (content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.video)){
                    showOverlayVideoImageNotification(notification:notification)
                }else if  ((content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.document) || (content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.mapLocation)){
                    showOverlayDocumentLocationNotification(notification:notification)
                }else if ((content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.chatMessage) || (content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.banner)) && ((content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.EMPTY) || (content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.text) || (content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.link)) {
                    showOverlayTextAndLinkNotification(notification:notification)
                }
            }
        }
    }
    
    
    
    //Show Audio
    func showOverlayAudioNotification(notification:UNNotification){
        let size = view.bounds.size
        preferredContentSize = CGSize(width: size.width, height: (size.height / 3 - 30) )
        let content = notification.request.content
        if let customView = Bundle.main.loadNibNamed("OverlayNotificationAudio", owner: self, options: nil)!.first as? OverlayNotificationAudio {
            customView.lblChannelName.text = content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String
            if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileUrl] as? String {
                if let url = URL(string: urlImageString) {
                    URLSession.downloadImage(atURL: url) { (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            customView.imgChannelProfile.image = UIImage(data: data)
                        }
                    }
                }
            }
            customView.frame = self.viewOfOverlayChatMessages.frame
            viewOfOverlayChatMessages.addSubview(customView)
        }
    }

    //Show Video And Image
    func showOverlayVideoImageNotification(notification:UNNotification){
        let size = view.bounds.size
        preferredContentSize = CGSize(width: size.width, height: (size.height / 2 + 50) )
        let content = notification.request.content
        if let customView = Bundle.main.loadNibNamed("OverlayNotificationVideoImage", owner: self, options: nil)!.first as? OverlayNotificationVideoImage {
            customView.lblCommunityName.text = content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String
            if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileUrl] as? String {
                let imgurl = urlImageString
                let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                if let url = URL(string: rep2) {
                    URLSession.downloadImage(atURL: url) { (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            customView.imgChannelProfile.image = UIImage(data: data)
                        }
                    }
                }
            }
            
            if  content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.image{
                if content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String == StringConstants.EMPTY{
                    customView.viewOfImageCaption.isHidden = true
                    customView.heighConstraintConstantOfViewCAption.constant = 0
                }else{
                    customView.viewOfImageCaption.isHidden = false
                    customView.lblicon.text = "📷"
                    customView.lblCaption.text = content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                }
                customView.imgVideoIcon.isHidden = true
                if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.contentField2] as? String {
                    let imgurl = urlImageString
                    let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                    if let url = URL(string: rep2) {
                        URLSession.downloadImage(atURL: url) {(data, error) in
                            if let _ = error {
                                return
                            }
                            guard let data = data else {
                                return
                            }
                            DispatchQueue.main.async {
                                customView.imgReceivedMedia.image = UIImage(data: data)
                            }
                        }
                    }
                }
                
            }else{
                customView.imgVideoIcon.isHidden = false
                if content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String == StringConstants.EMPTY{
                    customView.viewOfImageCaption.isHidden = true
                    customView.heighConstraintConstantOfViewCAption.constant = 0
                }else{
                    customView.viewOfImageCaption.isHidden = false
                    customView.lblicon.text = "📹"
                    customView.lblCaption.text = content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                }

                if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.contentField3] as? String {
                    let imgurl = urlImageString
                    let rep2 = imgurl.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
                    if let url = URL(string: rep2) {
                        URLSession.downloadImage(atURL: url) {(data, error) in
                            if let _ = error {
                                return
                            }
                            guard let data = data else {
                                return
                            }
                            DispatchQueue.main.async {
                                customView.imgReceivedMedia.image = UIImage(data: data)
                            }
                        }
                    }
                }
            }
            
            customView.frame = self.viewOfOverlayChatMessages.frame
            viewOfOverlayChatMessages.addSubview(customView)
        }
    }
    
    func showOverlayTextAndLinkNotification(notification:UNNotification){
        let content = notification.request.content
        let size = view.bounds.size
        let strmessage = content.userInfo[StringConstants.PushNotificationAction.contentField1] as! String
        if let customView = Bundle.main.loadNibNamed("OverlayNotificationTextAndLink", owner: self, options: nil)!.first as? OverlayNotificationTextAndLink {
            if content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.link{
                let attrs = [
                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14.0),
                    NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
                let attrs1 = [
                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14.0),
                    NSAttributedString.Key.foregroundColor : UIColorFromRGB.init(rgb: ColorConstants.colorPrimary)] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
                let strnewLine = " - "
                let strUrlName =  "🔗 " + ((content.userInfo[StringConstants.PushNotificationAction.contentField3] as? String)!) as String + strnewLine as String
                let attributedString2 = NSMutableAttributedString(string:strUrlName, attributes:attrs)
                let attrString = NSMutableAttributedString(string:(content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String)!)
                attrString.addAttributes(attrs1, range: NSMakeRange(0, attrString.length))
                attributedString2.addAttributes(attrs, range: NSMakeRange(0, attributedString2.length))
                attributedString2.append(attrString)
                customView.txtMessage.attributedText =  attributedString2
                preferredContentSize = CGSize(width: size.width, height: (size.height / 3))

            }else{
                
                customView.txtMessage.text = content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                if (strmessage.count > 100){
                    preferredContentSize = CGSize(width: size.width, height: (size.height / 3))
                }else{
                    preferredContentSize = CGSize(width: size.width, height: (size.height / 3) - 50)
                }
                
                //customView.txtMessage.becomeFirstResponder()
               // preferredContentSize = CGSize(width: size.width, height: customView.viewOnOverLayTextAndLinkView.frame.height)
               
                //Count number of lines of  customView.txtMessage
                let numberOfLines = getNumberOfLinesOfTextView(txtView: customView.txtMessage)
                if numberOfLines >= 5  {
                    customView.heightOfViewBackNoti.priority = UILayoutPriority(rawValue: 999)
                    customView.heightOfTextView.priority = UILayoutPriority(rawValue: 999)
                    customView.heightOfViewBackNoti.constant = 140
                    customView.heightOfTextView.constant = 99
                } else {
                    customView.heightOfViewBackNoti.priority = UILayoutPriority(rawValue: 250)
                    customView.heightOfTextView.priority = UILayoutPriority(rawValue: 250)
                }
                
            }
            customView.lblChannelName.text = content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String
           
            if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileUrl] as? String {
                if let url = URL(string: urlImageString) {
                    URLSession.downloadImage(atURL: url) { (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            customView.imgChannelProfile.image = UIImage(data: data)
                        }
                    }
                }
            }
            customView.frame = self.viewOfOverlayChatMessages.frame
            viewOfOverlayChatMessages.addSubview(customView)
//            customView.frame = customView.viewOnOverLayTextAndLinkView.frame
//            viewofBackLinkTextNoti.addSubview(customView)
        }
        
    }
    
    func getNumberOfLinesOfTextView(txtView : UITextView)-> Int{
        let layoutManager = txtView.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var lineRange: NSRange = NSMakeRange(0, 1)
        var index = 0
        var numberOfLines = 0
        
        while index < numberOfGlyphs {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        return numberOfLines
    }
    
    
    
    func showOverlayDocumentLocationNotification(notification:UNNotification){
        
        let size = view.bounds.size
        preferredContentSize = CGSize(width: size.width, height: (size.height / 3) + 10)
        let content = notification.request.content
        if let customView = Bundle.main.loadNibNamed("OverlayNotificationLocDoc", owner: self, options: nil)!.first as? OverlayNotificationLocDoc {
            customView.lblCommunityName.text = content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String
            if (content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String == StringConstants.MediaType.document){
                customView.lblLocationDocName.text = content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                customView.imgLcationDoclogo.image = UIImage(named: StringConstants.PushNotificationAction.imgDocLogo)
            }else{
                customView.imgLcationDoclogo.image = UIImage(named: StringConstants.PushNotificationAction.imgLocationLogo)
                let attrs = [
                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18.0),
                    NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
                let attrs1 = [
                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14.0),
                    NSAttributedString.Key.foregroundColor : UIColor.gray] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
                let strnewLine = "\n"
                let strDescrption =   ((content.userInfo[StringConstants.PushNotificationAction.contentField4] as? String)!) as String + strnewLine as String
                let attributedString2 = NSMutableAttributedString(string:strDescrption, attributes:attrs)
                let attrString = NSMutableAttributedString(string:(content.userInfo[StringConstants.PushNotificationAction.contentField5] as? String)!)
                attrString.addAttributes(attrs1, range: NSMakeRange(0, attrString.length))
                attributedString2.addAttributes(attrs, range: NSMakeRange(0, attributedString2.length))
                attributedString2.append(attrString)
                customView.lblLocationDocName.attributedText = attributedString2/*content.userInfo[StringConstants.PushNotificationAction.contentField5] as? String*/
                if let strlocation = content.userInfo[StringConstants.PushNotificationAction.contentField4]{
                    customView.lblLocationDocName.text = content.userInfo[StringConstants.PushNotificationAction.contentField5] as? String
                }
            }
            if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileUrl] as? String {
                if let url = URL(string: urlImageString) {
                    URLSession.downloadImage(atURL: url) { (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            customView.imgCommunityProfile.image = UIImage(data: data)
                        }
                    }
                }
            }
           customView.frame = self.viewOfOverlayChatMessages.frame
            viewOfOverlayChatMessages.addSubview(customView)
            
        }

    }

    func showOverlayNotificationforPostRssFeedMsges(notification:UNNotification){
        
        let content = notification.request.content
        viewOfBckSponserdNotification.isHidden = false
        viewOfBackMessageNotification.isHidden = true
        viewOfOverlayChatMessages.isHidden = true
        
        
        if let companyName = content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String {
            lblCompanyName.text = companyName
        }
        if let productName = content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String {
            lblProductName.text = productName
        }
        if let productDesc = content.userInfo[StringConstants.PushNotificationAction.contentField2] as? String {
            lblDescription.text = productDesc
        }
        
        if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileUrl] as? String {
            if let url = URL(string: urlImageString) {
                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.ImageVewOfChannelProfile.image = UIImage(data: data)
                    }
                }
            }
        }
        
        if content.userInfo[StringConstants.PushNotificationAction.messageType] as? String == StringConstants.MessageType.feed{
            imageViewOfPlayVideo.isHidden = true
            if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.contentField3]{
                self.imageViewOfProduct.contentMode = .scaleAspectFit
                if urlImageString as! String == StringConstants.EMPTY{
                    self.imageViewOfProduct.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
                }else{
                    if let url = URL(string: urlImageString as! String ) {
                        URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                            if let _ = error {
                                return
                            }
                            guard let data = data else {
                                return
                            }
                            DispatchQueue.main.async {
                                self?.imageViewOfProduct.image = UIImage(data: data)
                            }
                        }
                    }else{
                        self.imageViewOfProduct.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
                    }
                }
                
            }else{
                self.imageViewOfProduct.image = UIImage(named: StringConstants.ImageNames.rssFeedDefault)
            }
        }else{
            if let image = content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String{
                var urlImageString = String()
                if image == StringConstants.MediaType.image{
                    urlImageString = content.userInfo[StringConstants.PushNotificationAction.contentField3] as! String
                    imageViewOfPlayVideo.isHidden = true
                }else{
                    urlImageString = content.userInfo[StringConstants.PushNotificationAction.contentField8] as! String
                    imageViewOfPlayVideo.isHidden = false
                }
                if let url = URL(string: urlImageString ) {
                    URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            self?.imageViewOfProduct.image = UIImage(data: data)
                        }
                    }
                }
            }
        }
    }

    func showCustoNotificationOfSponsoredMsg(notification:UNNotification){
    
        let content = notification.request.content
        viewOfBckSponserdNotification.isHidden = false
        viewOfBackMessageNotification.isHidden = true
        viewOfOverlayChatMessages.isHidden = true
        
        if let companyName = content.userInfo[StringConstants.PushNotificationAction.sponsoredMsgcompanyName] as? String {
            lblCompanyName.text = companyName
        }
        if let productName = content.userInfo[StringConstants.PushNotificationAction.sponsoredMessageTitle] as? String {
            lblProductName.text = productName
        }
        if let productDesc = content.userInfo[StringConstants.PushNotificationAction.sponsoredMessageDescription] as? String {
            lblDescription.text = productDesc
        }
        if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityImageBigThumbUrl] as? String {
            if let url = URL(string: urlImageString) {
                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.ImageVewOfChannelProfile.image = UIImage(data: data)
                    }
                }
            }
        }
        
        if let image = content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String{
            if image == StringConstants.MediaType.image{
                let urlImageString = content.userInfo[StringConstants.PushNotificationAction.lastMessageContentField3]
                imageViewOfPlayVideo.isHidden = true
                if let url = URL(string: urlImageString as! String) {
                    URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            self?.imageViewOfProduct.image = UIImage(data: data)
                        }
                    }
                }
            }
            else{
                let urlImageString = content.userInfo[StringConstants.PushNotificationAction.lastMessageContentField8]
                imageViewOfPlayVideo.isHidden = false
                if let url = URL(string: urlImageString as! String) {
                    URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                        if let _ = error {
                            return
                        }
                        guard let data = data else {
                            return
                        }
                        DispatchQueue.main.async {
                            self?.imageViewOfProduct.image = UIImage(data: data)
                        }
                    }
                }
            }
        }
    }
    
    func showCustomnotificationwithimage(notification:UNNotification)
    {
        let content = notification.request.content
        viewOfBckSponserdNotification.isHidden = false
        viewOfBackMessageNotification.isHidden = true
        imageViewOfPlayVideo.isHidden = true
        
        if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.mediaUrl] as? String {
            if let url = URL(string: urlImageString) {
                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.imageViewOfProduct.image = UIImage(data: data)
                    }
                }
            }
        }
        if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileImageUrl] as? String {
            if let url = URL(string: urlImageString) {
                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.ImageVewOfChannelProfile.image = UIImage(data: data)
                    }
                }
            }
        }
        
        if let companyName = content.userInfo[StringConstants.PushNotificationAction.companyName] as? String {
            lblCompanyName.text = companyName
        }
        if let productName = content.userInfo[StringConstants.PushNotificationAction.title] as? String {
            lblProductName.text = productName
        }
        if let productDesc = content.userInfo[StringConstants.PushNotificationAction.description] as? String {
            lblDescription.text = productDesc
        }
    }
    
     func showCustomNotificationWithoutimage(notification:UNNotification)
     {
        let content = notification.request.content
        let size = view.bounds.size
        preferredContentSize = CGSize(width: size.width, height: size.height / 3.0)
        bottomOfBackMessagenotifsctionView.constant = 0
        viewOfBckSponserdNotification.isHidden = true
        viewOfBackMessageNotification.isHidden = false
        lblUpdate.isHidden = true
        if let companyname = content.userInfo[StringConstants.PushNotificationAction.companyName] as? String {
            lblCompanyNameForlink.text = companyname
        }
        if let companyDescription = content.userInfo[StringConstants.PushNotificationAction.description] as? String {
            lblDescriptionoflink.text = companyDescription
        }
        if let urlImageString = content.userInfo[StringConstants.PushNotificationAction.communityProfileImageUrl] as? String {
            if let url = URL(string: urlImageString) {
                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.imgChannelProfilelink.image = UIImage(data: data)
                    }
                }
            }
        }
    }
}


extension URLSession {
    class func downloadImage(atURL url: URL, withCompletionHandler completionHandler: @escaping (Data?, NSError?) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            completionHandler(data, nil)
        }
        dataTask.resume()
    }

}
