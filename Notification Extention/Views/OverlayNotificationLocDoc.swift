//
//  OverlayNotificationLocDoc.swift
//  KISAN.Net
//
//  Created by Kisan Forum on 24/04/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit



class OverlayNotificationLocDoc: UIView {

    @IBOutlet weak var lblCommunityName: UILabel!
    @IBOutlet weak var imgCommunityProfile: UIImageView!
    @IBOutlet weak var lblLocationDocName: UILabel!
    @IBOutlet weak var imgLcationDoclogo: UIImageView!
    

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        imgCommunityProfile.layer.cornerRadius = self.imgCommunityProfile.frame.width/2
        imgCommunityProfile.clipsToBounds = true

    }
    

}
