//
//  OverlayNotificationTextAndLink.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 03/05/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit



class OverlayNotificationTextAndLink: UIView {
    @IBOutlet weak var imgChannelProfile: UIImageView!
    @IBOutlet weak var lblChannelName: UILabel!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var viewOnOverLayTextAndLinkView: UIView!
    @IBOutlet weak var heightOfViewBackNoti: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfTextView: NSLayoutConstraint!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        imgChannelProfile.layer.cornerRadius = self.imgChannelProfile.frame.width/2
        imgChannelProfile.clipsToBounds = true

    }
    

}
