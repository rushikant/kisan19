//
//  OverlayNotificationVideoImage.swift
//  KISAN.Net
//
//  Created by Niranjan Deshpande on 24/04/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UIKit

class OverlayNotificationVideoImage: UIView {
    @IBOutlet weak var imgChannelProfile: UIImageView!
    @IBOutlet weak var lblCommunityName: UILabel!
    @IBOutlet weak var imgReceivedMedia: UIImageView!
    @IBOutlet weak var imgVideoIcon: UIImageView!
    @IBOutlet weak var viewOfImageCaption: UIView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var heighConstraintConstantOfViewCAption: NSLayoutConstraint!
    @IBOutlet weak var lblicon: UILabel!
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        imgChannelProfile.layer.cornerRadius = self.imgChannelProfile.frame.width/2
        imgChannelProfile.clipsToBounds = true
    }
    

}
