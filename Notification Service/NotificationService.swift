//
//  NotificationService.swift
//  Notification Service
//
//  Created by Niranjan Deshpande on 23/04/19.
//  Copyright © 2019 KISAN TEAM. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            
            bestAttemptContent.title = (request.content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String)!
            let mediaType = request.content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String
            let messageType = request.content.userInfo[StringConstants.PushNotificationAction.messageType] as? String
            
            if messageType == StringConstants.MessageType.post{
                if mediaType == StringConstants.MediaType.image{
                    bestAttemptContent.body = "📷" + StringConstants.singleSpace + StringConstants.PushNotificationAction.post
                }else if mediaType == StringConstants.MediaType.video{
                    bestAttemptContent.body = "📹" + StringConstants.singleSpace + StringConstants.PushNotificationAction.post
                }
                self.showMediainNotificatio(messageData: request)
            }else if mediaType == StringConstants.MediaType.image{
                let imageCaption = request.content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                if imageCaption == StringConstants.EMPTY{
                    bestAttemptContent.body = "📷" + StringConstants.singleSpace + StringConstants.PushNotificationAction.photo
                }else{
                    bestAttemptContent.body = "📷" + StringConstants.singleSpace + imageCaption!
                }
                self.showMediainNotificatio(messageData: request)
            }else if mediaType == StringConstants.MediaType.video{
                let imageCaption = request.content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String
                if imageCaption == StringConstants.EMPTY{
                    bestAttemptContent.body = "📹" + StringConstants.singleSpace + StringConstants.PushNotificationAction.video
                }else{
                    bestAttemptContent.body = "📹" + StringConstants.singleSpace + imageCaption!
                }
                self.showMediainNotificatio(messageData: request)
            }else if mediaType == StringConstants.MediaType.audio{
                bestAttemptContent.body = "🎵" + StringConstants.singleSpace + request.content.body
            }else if messageType == StringConstants.MessageType.mapLocation{
                bestAttemptContent.body = "📍" + StringConstants.singleSpace + request.content.body
            }else if (messageType == StringConstants.MessageType.chatMessage || messageType == StringConstants.MessageType.banner) && (mediaType  == StringConstants.EMPTY || mediaType  == StringConstants.MessageType.text || mediaType  == StringConstants.MediaType.link ) {
                if mediaType == StringConstants.MediaType.link{
                    bestAttemptContent.body =  "🔗" + StringConstants.singleSpace + (request.content.userInfo[StringConstants.PushNotificationAction.contentField3] as? String)! + " - " + (request.content.userInfo[StringConstants.PushNotificationAction.contentField1] as? String)!
                }else{
                    bestAttemptContent.body =  request.content.body
                }
            }else if (mediaType == StringConstants.MediaType.document ) {
                bestAttemptContent.body = "📄" + StringConstants.singleSpace + request.content.body
            }else if (mediaType == StringConstants.MediaType.link ) {
                bestAttemptContent.body = "🔗" + StringConstants.singleSpace + request.content.body
            }else if messageType == StringConstants.MessageType.feed {
                bestAttemptContent.body =  "🌐" + StringConstants.singleSpace + request.content.body
                //self.showMediainNotificatio(messageData: request)
            }
            //For groping of message as per the community when no of messages count greater than 4
            bestAttemptContent.threadIdentifier = (request.content.userInfo[StringConstants.PushNotificationAction.communityId] as? String)!
            bestAttemptContent.summaryArgument = (request.content.userInfo[StringConstants.PushNotificationAction.communtyName] as? String)!
            contentHandler(bestAttemptContent)
        }
        
    }
    
    
    func showMediainNotificatio(messageData: UNNotificationRequest){
        var urlString:String? = nil
        let mediaType = messageData.content.userInfo[StringConstants.PushNotificationAction.mediaType] as? String
        let messageType = messageData.content.userInfo[StringConstants.PushNotificationAction.messageType] as? String
        if messageType == StringConstants.MessageType.post{
            if mediaType == StringConstants.MediaType.image{
                if let urlImageString = messageData.content.userInfo[StringConstants.PushNotificationAction.contentField3] as? String {
                    urlString = urlImageString
                }
            }else{
                if let urlImageString = messageData.content.userInfo[StringConstants.PushNotificationAction.contentField8] as? String {
                    urlString = urlImageString
                }
            }
        }else if mediaType == StringConstants.MediaType.image{
            if let urlImageString = messageData.content.userInfo[StringConstants.PushNotificationAction.contentField2] as? String {
                urlString = urlImageString
            }
        }else if mediaType == StringConstants.MediaType.video{
            if let urlImageString = messageData.content.userInfo[StringConstants.PushNotificationAction.contentField3] as? String {
                urlString = urlImageString
            }
        }
        
        let imgurl = urlString
        let rep2 = imgurl!.replacingOccurrences(of:StringConstants.singleSpace, with: StringConstants.urlRemoveSpace)
        if rep2 != nil, let fileUrl = URL(string: rep2) {
            print("fileUrl: \(fileUrl)")
            
            guard let imageData = NSData(contentsOf: fileUrl) else {
                contentHandler!(bestAttemptContent!)
                return
            }
            guard let attachment = UNNotificationAttachment.saveImageToDisk(fileIdentifier: "image.jpg", data: imageData, options: nil) else {
                print("error in UNNotificationAttachment.saveImageToDisk()")
                contentHandler!(bestAttemptContent!)
                return
            }
            //,, let attachment = UNNotificationAttachment.
            bestAttemptContent!.attachments = [ attachment]
        }
    }
    
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}

@available(iOSApplicationExtension 10.0, *)
extension UNNotificationAttachment {
    
    static func saveImageToDisk(fileIdentifier: String, data: NSData, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let folderName = ProcessInfo.processInfo.globallyUniqueString
        let folderURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(folderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: folderURL!, withIntermediateDirectories: true, attributes: nil)
            let fileURL = folderURL?.appendingPathComponent(fileIdentifier)
            try data.write(to: fileURL!, options: [])
            let attachment = try UNNotificationAttachment(identifier: fileIdentifier, url: fileURL!, options: options)
            return attachment
        } catch let error {
            print("error \(error)")
        }
    
        return nil
    }
}
